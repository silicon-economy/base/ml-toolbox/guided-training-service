# Guided Training Service
The Guided Training Service (GTS) is a web application for developing ML-based computer vision models and applications.
It bundles all important steps to create and manage training data, as well as to train, manage and deploy
computer vision models. It is designed for ML-developers as well as non-ML-experienced technical experts
(e.g. engineers and logisticians) who want to develop computer vision applications for their own use cases.

For a detailed description of the GTS see the documentation.

# Quick Start
[comment]: <> (TODO: Fix links to main branch)
- [Ar42 Documentation](https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/guided-training-service/-/blob/main/documentation/index.adoc)
- [Installation guide](https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/guided-training-service/-/blob/main/documentation/12_tutorial.adoc?ref_type=heads#user-content-setup-local-environment)
- [Usage Tutorials](https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/guided-training-service/-/blob/main/documentation/12_tutorial.adoc?ref_type=heads#user-content-usage-tutorials)


# Components of the Guided Training Service

| Service                     | Description                                                  |
|-----------------------------|--------------------------------------------------------------|
| (GTS-)Frontend              | Web interface to control the Guided-Training-Service         |
| (GTS-)Backend               | The API that manages the models, the training and other data |
| MLflow Server               | Web interface and server for MLflow                          |
| Label Studio                | Web interface to label datasets                              |
| PostreSQL-DB                | Database to store metadata for the models, datasets, etc.    |
| S3 storage                  | Database to store files, artifacts, images, etc.             |
| Training/ Prediction server | Server to execute trainings and prediction tasks             |
| Edge Device                 | Device for model deployment and runtime                      |

# Current state of the project
This project is currently under active development. Following functionality is already provided:
- Data Management (incl. creation, annotation and export of datasets)
- Model Training (i.e. training of object detection models)
- Model Testing (i.e. in-app inference of trained models for testing purposes)
- Model Management (i.e. management of previously trained models)

Following functionality is still under active development and will be added in the future:
- Model deployment (i.e. deployment of trained models to edge devices)
- Model Training to support classification models
- Model Training to support a wider range of configurable training parameters

# Contact information
Maintainers:
- Maximilian Otten <a href="mailto:maximilian.otten@iml.fraunhofer.de?">maximilian.otten@iml.fraunhofer.de</a>

# License
Copyright Open Logistics Foundation
Licensed under the Open Logistics Foundation License 1.3.

For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
