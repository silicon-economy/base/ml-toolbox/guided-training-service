#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=cvonedge-staging}"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -i label-studio . || exit 1
