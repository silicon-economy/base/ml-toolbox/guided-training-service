#!/bin/bash

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=cvonedge}"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -i cvonedge-gts-angular-frontend . || exit 1
# Ensure image stream picks up the new docker image right away
oc import-image cvonedge-gts-angular-frontend