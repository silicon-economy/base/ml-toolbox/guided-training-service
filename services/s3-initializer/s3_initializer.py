# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Provide the S3Initializer module."""

import json
import logging
import os.path
import urllib.request
from re import compile

import boto3

logger = logging.getLogger(__file__)


def replace_with_os_env(value: str) -> str:
    """
    Takes the given value and replaces all occurrences that match the
    given pattern ${...}, which corresponds to a specific os environment
    variable.

    Args:
        value: The value that should be checked for occurrences of os environment
               variables marked with ${...}

    Returns:
        The value with replaced occurrences of environment variables
    """
    # Pattern matches all occurrences of ${...} in the given value
    pattern = compile(r".*?\${(\w+)}.*?")
    matched_patterns = pattern.findall(value)

    for match in matched_patterns:
        os_value = os.environ.get(match)
        if os_value is not None:
            logger.debug(
                f"Replace os-environment '${{{match}}}' "
                f"in value '{value}' "
                f"with value '{os_value}'"
            )
            value = value.replace(f"${{{match}}}", os_value)

    return value


class S3Initializer:
    """
    Initialize the S3 storage of the Guided Training Service with
    all checkpoints that are needed for Transfer Learning.
    """

    def __init__(self) -> None:
        self.media_dir_env_name = "MEDIA_DIR"

        # Set the MEDIA_DIR environment variable if it is not set
        if self.media_dir_env_name not in os.environ:
            os.environ[self.media_dir_env_name] = "/data"

        self.client = boto_client = boto3.client(
            service_name="s3",
            region_name=os.environ.get("AWS_DEFAULT_REGION"),
            endpoint_url=os.environ.get("S3_ENDPOINT_EXTERNAL"),
            aws_access_key_id=os.environ.get("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.environ.get("AWS_SECRET_ACCESS_KEY"),
        )
        self.bucket_name = os.environ.get("BUCKET_NAME")
        self.mlflow_bucket_name = os.environ.get("MLFLOW_BUCKET_NAME")
        self.device_bucket_name = os.environ.get("DEVICE_BUCKET_NAME")

    @staticmethod
    def __download_file(url: str, file_path: str) -> None:
        """Download a file from a given url and save it to the local directory.

        Args:
            url: The url to download the file from.
            file_path: The name of the file to save the downloaded content to.
        """
        logger.info("Downloading '%s'...", url)
        os.makedirs(name=os.path.dirname(file_path), exist_ok=True)
        with urllib.request.urlopen(url) as response, open(file_path, "wb") as out_file:
            data = response.read()
            out_file.write(data)
            logger.info("Successfully downloaded checkpoint to '%s'.", file_path)

    def __upload_to_s3(self, local_path: str, backend_name: str) -> None:
        """Upload a file to S3.

        Args:
            local_path: The local path of the file to upload.
            backend_name: The name of the file to save the uploaded content to.
        """
        logger.info(
            "Upload file '%s' for '%s' for upload to S3." % (local_path, backend_name)
        )

        self.client.upload_file(
            Bucket=self.bucket_name,
            # Directory where the initial checkpoints should be stored, as defined in
            # python-backend/gts_backend/services/jobs/training/structs.DataRootDirs
            Key=f"initial-checkpoints/{backend_name}",
            Filename=local_path,
        )
        logger.info("Successfully uploaded '%s' to S3.", local_path)

    def __create_buckets(self) -> None:
        """Create the default buckets if they do not exist."""
        buckets = self.client.list_buckets()["Buckets"]
        has_default_bucket = False
        has_ml_flow_bucket = False
        has_deployment_bucket = False

        for bucket in buckets:
            if bucket["Name"] == self.bucket_name:
                has_default_bucket = True
            elif bucket["Name"] == self.mlflow_bucket_name:
                has_ml_flow_bucket = True
            elif bucket["Name"] == self.device_bucket_name:
                has_deployment_bucket = True

        if not has_default_bucket:
            self.client.create_bucket(Bucket=self.bucket_name)
            logger.info("Created default bucket")

        if not has_ml_flow_bucket:
            self.client.create_bucket(Bucket=self.mlflow_bucket_name)
            logger.info("Created mlflow bucket")

        if not has_deployment_bucket:
            self.client.create_bucket(Bucket=self.device_bucket_name)
            logger.info("Created device bucket")

    def run(self) -> None:
        """Run the S3 initializer."""
        self.__create_buckets()
        with open("checkpoints.json") as file:
            checkpoints = json.load(file)["checkpoints"]
            for checkpoint in checkpoints:
                name = checkpoint["name"]
                backend_name = checkpoint["backend_name"]
                download_url = checkpoint["original_url"]
                storage_location = replace_with_os_env(checkpoint["storage_location"])

                matches = self.client.list_objects(
                    Bucket=self.bucket_name,
                    Prefix=f"initial-checkpoints/{backend_name}",
                )

                if "Contents" in matches:
                    # file is already downloaded and in the correct location
                    logger.info("'%s' already exists in S3.", name)
                    continue

                local_storage_location = str(
                    os.path.join(
                        storage_location,
                        os.path.basename(download_url),
                    )
                )

                # If file does not exist, download it
                if not os.path.isfile(local_storage_location):
                    self.__download_file(download_url, local_storage_location)

                self.__upload_to_s3(local_storage_location, backend_name)

        logger.info("Finished loading of checkpoints.")


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(levelname)-7.7s: "
        "[%(name)-30.30s]"
        "[%(threadName)-11.11s]"
        "[%(filename)s:%(lineno)s - %(funcName)20s()] "
        "%(message)s",
        level=logging.INFO,
    )

    client = S3Initializer()
    client.run()
