# This template file defines jobs related to npm projects using Angular.

variables:
  # IMPORTANT: Variables that are defined using variable expansion cannot be use in `rules:if` and
  # `rules:changes` clauses. Therefore, variables defined in this template file and used in
  # `rules:if` or `rules:changes` clauses, among others, explicitly don't make use of variable
  # expansion. These variables are marked with [RULES]. Care must be taken when overriding these
  # variables. Otherwise, some predefined rules may not work as intended.

  ##################################################################################################
  # Definition of variables for general configuration of the pipeline jobs and the tools used
  # with them.
  # These variables are not intended to be overridden by jobs extending the job templates.
  ##################################################################################################
  NPM_ANGULAR_DOCKER_IMAGE: trion/ng-cli-karma:17.3.4

  ##################################################################################################
  # Definition of variables for project-specific configuration of the pipeline jobs.
  # These variables can be overridden as needed by jobs extending the job templates.
  ##################################################################################################
  # Path to the project directory (relative to ${CI_PROJECT_DIR}).
  # Can either be empty or has to end with a `/`.
  PROJECT_DIR: ""
  # [RULES] Path to the project's `package.json` file (relative to ${CI_PROJECT_DIR}).
  PACKAGE_JSON_PATH: "package.json"
  # The unique key to use for the project in SonarQube.
  # Hint: For Maven projects, this defaults to <groupId>:<artifactId>.
  SONAR_PROJECT_KEY: ""
  # The name to use for the project in SonarQube.
  SONAR_PROJECT_NAME: ""
  # The SonarQube server URL.
  SONAR_HOST_URL: "https://sonar.apps.sele.iml.fraunhofer.de"
  # Path to the directory containing coverage report files (relative to ${PROJECT_DIR}).
  COVERAGE_OUTPUT_RELATIVE_PATH: "coverage"
  # Comma-SEPARATED list of paths to LCOV coverage report files. Paths may be absolute or
  # relative to ${PROJECT_DIR}.
  LCOV_REPORT_PATHS: "${COVERAGE_OUTPUT_RELATIVE_PATH}/lcov.info"
  # Path to ESLint report file (relative to ${PROJECT_DIR}).
  ESLINT_REPORT_FILE: "eslint-report.json"
  # Whether to enforce successful quality jobs (e.g. sonar analysis) on the default branch. A value
  # of `true` effectively doesn't allow quality jobs to fail on the default branch.
  ENFORCE_QA_ON_DEFAULT_BRANCH: "false"
  # Path to the reference file containing all third-party licenses (relative to ${PROJECT_DIR}).
  REFERENCE_LICENSES_FILE: "third-party-licenses/third-party-licenses.csv"
  # Path to the generated file containing all third-party licenses - i.e. the output of the
  # `license-checker` module (relative to ${PROJECT_DIR}).
  GENERATED_LICENSES_FILE: "generated/third-party-licenses.csv"

# Job template for compiling the source code.
.npm-angular-build:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - npm --registry="${NEXUS_NPM_CACHE_URL}" install
    - npm run build
  artifacts:
    # Save the files produced by this job as job artifacts and allow jobs in subsequent stages to
    # download them.
    paths:
      - "${PROJECT_DIR}dist/"
    expire_in: 1 week
  rules:
    # The build is never allowed to fail.
    - when: on_success
      allow_failure: false

# Job template for running the linter.
.npm-angular-lint:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - npm --registry="${NEXUS_NPM_CACHE_URL}" install
    # Run the linter and ignore any errors since the result of the linter run will be evaluated by
    # SonarQube.
    - npm run lint -- -o "${ESLINT_REPORT_FILE}" -f json || true
  artifacts:
    # Save the files produced by this job as job artifacts and allow jobs in subsequent stages to
    # download them.
    paths:
      - "${PROJECT_DIR}${ESLINT_REPORT_FILE}"
    expire_in: 1 week

# Job template for running unit and integration tests.
.npm-angular-test:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - npm --registry="${NEXUS_NPM_CACHE_URL}" install
    - npm run test -- --code-coverage=true --progress=false --watch=false
  coverage: '/Lines \W+: (\d+\.\d+)%.*/'
  artifacts:
    # Save the files produced by this job as job artifacts and allow jobs in subsequent stages to
    # download them.
    paths:
      - "${PROJECT_DIR}${COVERAGE_OUTPUT_RELATIVE_PATH}"
    expire_in: 1 week
  rules:
    # Tests are never allowed to fail.
    - when: on_success
      allow_failure: false

# Job template for running tests and performing static code analysis via SonarQube.
.npm-angular-sonarqube-check:
  variables:
    # This tells git to fetch all branches of the project, which is required for the analysis task.
    GIT_DEPTH: "0"
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  before_script:
    # Workaround for `GIT_DEPTH: "0"` not being enough in some situations: SonarQube requires an
    # unshallow clone, otherwise some files will miss SCM information. This would e.g. affect
    # features like auto-assignment of issues.
    - git fetch --unshallow || true
    - npm --registry="${NEXUS_NPM_CACHE_URL}" install -g sonarqube-scanner
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "COVERAGE_OUTPUT_RELATIVE_PATH=${COVERAGE_OUTPUT_RELATIVE_PATH}"
    - echo "ESLINT_REPORT_PATHS=${ESLINT_REPORT_PATHS}"
    - echo "LCOV_REPORT_PATHS=${LCOV_REPORT_PATHS}"
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - echo "SONAR_HOST_URL=${SONAR_HOST_URL}"
    - echo "SONAR_PROJECT_KEY=${SONAR_PROJECT_KEY}"
    - echo "SONAR_PROJECT_NAME=${SONAR_PROJECT_NAME}"
    - cd "${PROJECT_DIR}"
    - >
      sonar-scanner
      -Dsonar.projectKey=${SONAR_PROJECT_KEY}
      -Dsonar.projectName=${SONAR_PROJECT_NAME}
      -Dsonar.host.url=${SONAR_HOST_URL}
      -Dsonar.login=${SONAR_TOKEN}
      -Dsonar.sourceEncoding=UTF-8
      -Dsonar.sources=src/app
      -Dsonar.exclusions=**/node_modules/**,**/*.spec.ts
      -Dsonar.cpd.exclusions=**/*.html
      -Dsonar.tests=src/app
      -Dsonar.test.inclusions=**/*.spec.ts
      -Dsonar.javascript.lcov.reportPaths="${LCOV_REPORT_PATHS}"
      -Dsonar.eslint.reportPaths="${ESLINT_REPORT_FILE}"
      -Dsonar.qualitygate.wait=true
  rules:
    # Sonar analysis must succeed (i.e. the quality gate must be passed) in MRs. However, on the
    # default branch, sonar analysis is allowed to fail by default. This allows merges into the
    # default branch and thus the deployment of new versions even if the sonar analysis fails.
    - if: (($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH) && ($ENFORCE_QA_ON_DEFAULT_BRANCH == "true")) || $CI_MERGE_REQUEST_IID
      allow_failure: false
    # In all other cases, failure is permitted.
    - when: on_success
      allow_failure: true

# Job template for checking for changed licenses of third-party dependencies.
# Note: This job uses the `license-checker` module to evaluate the project's third-party licenses.
.npm-angular-third-party-license-check:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - echo "REFERENCE_LICENSES_FILE=${REFERENCE_LICENSES_FILE}"
    - echo "GENERATED_LICENSES_FILE=${GENERATED_LICENSES_FILE}"
    - cd "${PROJECT_DIR}"
    # `npx license-checker` traverses the `node_modules` folder to evaluate the project's licenses,
    # therefore "npm install" is needed.
    - npm --registry="${NEXUS_NPM_CACHE_URL}" install --omit=dev
    - npx license-checker --unknown --csv --out "${GENERATED_LICENSES_FILE}"
    # Ensure a newline on each of the license files to equalise them
    - sed -i -e '$a\' "${GENERATED_LICENSES_FILE}"; sed -i -e '$a\' "${REFERENCE_LICENSES_FILE}"
    - cmp --silent "${REFERENCE_LICENSES_FILE}" "${GENERATED_LICENSES_FILE}" || export LICENSES_CHANGED=true
    - 'if [ ! -z ${LICENSES_CHANGED} ]; then
        echo Some licenses used by the third-party dependencies have changed.;
        echo Please refer to the README and generate/update them accordingly.;
        git diff --no-index --unified=0 "${REFERENCE_LICENSES_FILE}" "$GENERATED_LICENSES_FILE";
      fi'
  rules:
    # License check must succeed in default branches and MRs.
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH) || $CI_MERGE_REQUEST_IID
      allow_failure: false
    # In all other cases, failure is permitted.
    - when: on_success
      allow_failure: true

# Job template for deploying project artifacts to a package registry.
.npm-angular-archive:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  before_script:
    - echo "//${NEXUS_NPM_REPO}:_authToken=NpmToken.${NEXUS_NPM_AUTH}" > ~/.npmrc
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - npm publish
  rules:
    # Only archive if the project's `package.json` file has changed. Usually this should be the
    # case, among other things, when there is a new version.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - "${PACKAGE_JSON_PATH}"
      # This is allowed to fail so that default branch pipelines can be repeated
      allow_failure: true
