# This template file defines jobs related to Docker.

variables:
  # IMPORTANT: Variables that are defined using variable expansion cannot be use in `rules:if` and
  # `rules:changes` clauses. Therefore, variables defined in this template file and used in
  # `rules:if` or `rules:changes` clauses, among others, explicitly don't make use of variable
  # expansion. These variables are marked with [RULES]. Care must be taken when overriding these
  # variables. Otherwise, some predefined rules may not work as intended.

  ##################################################################################################
  # Definition of variables for general configuration of the pipeline jobs and the tools used
  # with them.
  # These variables are not intended to be overridden by jobs extending the job templates.
  ##################################################################################################
  # Specify to Docker where to create the certificates. Docker will create them automatically on
  # boot, and will create `/certs/client` that will be shared between the service and job
  # container, thanks to volume mount from `config.toml`.
  DOCKER_TLS_CERTDIR: "/certs"

  ##################################################################################################
  # Definition of variables for project-specific configuration of the pipeline jobs.
  # These variables can be overridden as needed by jobs extending the job templates.
  ##################################################################################################
  # [RULES] Path to the project directory (relative to ${CI_PROJECT_DIR}).
  # Can either be empty or has to end with a `/`.
  PROJECT_DIR: ""
  # Path to the `Dockerfile` to use (relative to ${PROJECT_DIR}).
  DOCKERFILE_RELATIVE_PATH: "Dockerfile"
  # The name of the image to build and push (including any prefixes, e.g. "my/org/image-name").
  IMAGE_NAME: ""
  # The complete name of the image (including the registry's host and port).
  REGISTRY_IMAGE: "${NEXUS_DOCKER_REGISTRY}:${NEXUS_DOCKER_REGISTRY_PORT}/${IMAGE_NAME}"

# Job template for building a Docker image for a project and pushing it to a registry.
.docker-build-and-push:
  image: docker:dind
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - echo ${NEXUS_PASS} | docker login -u ${NEXUS_USER} --password-stdin ${NEXUS_DOCKER_REGISTRY}:${NEXUS_DOCKER_REGISTRY_PORT}
    - rm -rf ~/.docker/manifests || true
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "DOCKER_TLS_CERTDIR=${DOCKER_TLS_CERTDIR}"
    - echo "DOCKERFILE_RELATIVE_PATH=${DOCKERFILE_RELATIVE_PATH}"
    - echo "IMAGE_NAME=${IMAGE_NAME}"
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - echo "REGISTRY_IMAGE=${REGISTRY_IMAGE}"
    # Change directory only if the variable is not empty (which would otherwise lead to a change to
    # the home directory).
    - '[ ! -z "${PROJECT_DIR}" ] && cd "${PROJECT_DIR}"'
    # CI_COMMIT_TAG is available only in tag pipelines and CI_COMMIT_BRANCH is only available in
    # branch pipelines (i.e. not available in tag or merge request pipelines). So, considering the
    # job rules, we either build an image for a commit tag or for the latest commit.
    - IMAGE_TAG=${CI_COMMIT_TAG:-latest}
    - docker pull ${REGISTRY_IMAGE}:latest || true
    - docker build --pull --cache-from ${REGISTRY_IMAGE}:latest --tag ${REGISTRY_IMAGE}:${IMAGE_TAG} -f "${DOCKERFILE_RELATIVE_PATH}" .
    - docker push ${REGISTRY_IMAGE}:${IMAGE_TAG}
  tags:
    - docker
  rules:
    # Build and push images whenever there is a change in any file in the project directory.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - "${PROJECT_DIR}**/*"
    # Build and push images for tags.
    - if: $CI_COMMIT_TAG
      when: on_success
