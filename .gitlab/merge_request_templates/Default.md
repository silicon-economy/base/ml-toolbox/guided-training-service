## Related issues

<!-- Issues that are closed by or related to this merge request, e.g. "Closes XXX" or "Related to XXX" -->

...

## Acceptance criteria

<!-- Criteria for the MR to be considered mergeable -->

- [ ] Implemented code changes don't diverge from the related issue.
- [ ] Pipeline must be green.
- [ ] No Sonar issues and warnings.
- [ ] The documentation is up-to-date.
- [ ] There are no critical bugs open.
- Version number is incremented according to [SemVer](https://semver.org/) (Snapshot versions are not merged/released.). Update in:
  - Frontend
    - [ ] angular-frontend/package.json
    - [ ] angular-frontend/package-lock.json
    - [ ] angular-frontend/CHANGELOG.md
  - Backend
    - [ ] python-backend/gts_backend/__init__.py
    - [ ] python-backend/CHANGELOG.md
- [ ] Only released versions of dependencies are used (i.e., no snapshot versions).
- [ ] The changelog contains entries reflecting all changes of this MR (and their reasons).

**Important:**
- [ ] Final check, when the MR is "ready to merge": The new feature has been tested in the staging environment.

## Proposed squash commit message

<!--
A proposed message for the eventual squashed commit.
Please stick to the following pattern:

- A short one-line summary (max. 50 characters).
- A blank line.
- A detailed explanation of the changes introduced by this merge request.
  Each line should not exceed 72 characters.
- Two blank lines
- A list of co-authors, each starting with Co-authored-by:
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
```
A short one-line summary (max. 50 characters)

* A more detailed explanation of the changes introduced by this merge
  request.
* Each line should not exceed 72 characters.


Co-authored-by: NAME1 <EMAIL1>
Co-authored-by: NAME2 <EMAIL2>
```
<!--
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
