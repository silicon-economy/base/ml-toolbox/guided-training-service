== System Scope and Context

This section describes the environment and scope of the GTS.
Who are its users, and with which other systems does it interact.
It thereby specifies the external interface (domain interfaces and technical interfaces to communication partners).
The GTS instance is a black box here.

=== Business and technical context

Stakeholders of an GTS instance need to understand which data is exchanged with the environment.
All communication partners (users, IT-systems, …) of GTS are described below.
It specifies (the interfaces of) all communication partners in business/domain and technical view.
The following table explains all communication partners, their inputs and outputs or interfaces, which may include domain-specific formats or communication protocols.
For details regarding the technical context see <<Deployment View>>.

[cols="6",options="header"]
|===
|Communication Partner
|Kind
|Domain Inputs
|Domain Outputs
|Protocol
|Data format

|IoT Device Admin
|User
|Device configuration, Device management
|
|HTTP (Web Frontend)
|

|Service User
|User
|Image data (+ anotations), Dataset management, Training configuration, Model management
|
|HTTP (Web Frontend)
|

|MLCVZoo Service (Training, Prediction)
|IT System / SE Service
|Configuration (Training/ Prediction)
|Trained model and evaluated metrics (Training), Predictions (Prediction)
|REST/HTTP
|JSON

|MLflow
|IT System
|Training metadata, information from model registry
|Training metadata
|REST/HTTP
|JSON

|LabelStudio
|IT System
|Image data (+ anotations), Dataset
|Dataset metadata
|REST/HTTP
|JSON
|===