== Solution Strategy

This chapter gives a short summary and explanation of the fundamental decisions and solution strategies, that shape the architecture of the Guided Training Service.
They are based on the problem statement, the quality goals and the key constraints from chapters 1 and 2.

The GTS is integrated into the ML Toolbox, which is developed in the context of the Silicon Economy. While the ML Toolbox provides a wide range of tools supporting developers in the development process of computer vision models, so far it lacks a central component unifying central steps in this process into a single application. In addition, the ML Toolbox is aimed more at developers and is comparatively difficult to use, especially for non-ML-experienced beginners.

To overcome this omission, the GTS was developed. It unites the most important aspects in the process of developing ML-based computer vision models, namely data management, model management (incl. training) and deployment. In particular, it aims to make the entire development process so easily accessible that even non-ML developers can develop their own computer vision models.

Against this background, the architecture of the GTS was concretely influenced by the following aspects:

First, the architecture should be designed to represent the typical work flow or all the tasks typically encountered in the development process of a CV model. This means that the three central tasks of data management, model management and training, and model implementation are taken into account in the architecture.

Secondly, it should be avoided that certain components, which are needed again and again in the development process, are redeveloped several times and recurrently. The aim is for a component to be developed once and then used as often as desired.

Third, the architecture should take into account that the research field in ML-based computer vision is developing rapidly. Therefore, it can be assumed that the GTS will be constantly extended by new model families.

=== Quality

The quality goals as summarized in <<Quality Goals>> and their matching architecture approaches and means for quality assurance are described in detail in <<Quality Requirements>>.
The key to assure quality is to use quality assurance tools like Sonar and automated tests in the CI/CD Pipeline.

=== Common SE Reference Architecture

All SE components follow a common SE reference architecture.

.SE Reference Architecture
image::images/04_se_reference_architecture.jpg[alt="SE Reference Architecture"]

This architecture is determined by the following elements:

A macro-architecture, currently consisting of the IDS infrastructure with its Connectors and the three types of brokers (Logistic, IoT, Blockchain).
The macro-architecture can be extended by further basic components, e.g. concerning 5G.
a micro-architecture for the individual SE services and brokers, developed independently of each other, which plug into the macro-architecture and which themselves consist of microservices.
These SE services are Self-Contained Systems (SCS) and gradually enrich the SE architecture in conformity with the SE reference architecture.

Roughly, three layers can be identified in and across all SE services and across the SE architecture:

The user interface (U) is composed of the web UIs and mobile UIs of the individual SE services, which all follow a common style and may refer to each other the business logic (S) is realized via multi-tier microservices.
The services are orchestrated using the most appropriate means, from rigid to AI-driven.
the persistence layer (P) is analogously composed of the heterogeneous data stores of the individual SE services, which can also refer to each other.

=== Decomposition

The fundamental decisions are to compose SE services and brokers from microservices, in accordance with the reference architecture.
Every Micro Service is executed in its own application container, and loose coupling by event-based communication is used whenever appropriate.
See <<Building Block View>> for details.

=== Technology

As detailed in <<Architecture Constraints>> we use containers running on OKD/Kubernetes, communicating via HTTP/REST, MQTT, AMQP, WebSockets etc.

=== Organizational

See <<Architecture Constraints>> for organisational constraints and political constraints that apply.
We use agile development (Scrum) to fulfill these constraints, organised in separate projects for all SE projects and brokers.
All third-party software used must be open source.
Moreover, the development of the DataSpace Connector and the Wrapper is fulfilled by another Silicon Economy base component project "IDS".
The same applies to Blockchain technology from Blockchain Europe.