#!/bin/sh

# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# This script is intended to be executed from the root of the project

FAILED=0

scripts/complementary_license_check.sh "third-party-licenses/third-party-licenses.csv" "third-party-licenses/third-party-licenses-complementary.csv" || FAILED=1

exit $FAILED