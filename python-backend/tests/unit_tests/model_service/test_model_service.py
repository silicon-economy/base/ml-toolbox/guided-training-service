# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the ModelService."""
from dataclasses import asdict
from unittest.mock import MagicMock

import pytest
from mlcvzoo_util.adapters.mlflow.adapter import ModelInfo
from mlcvzoo_util.adapters.s3.data_location import S3Data
from mlflow.exceptions import RestException

from gts_backend.services.models.model_service import ModelNotFoundError, ModelService


@pytest.fixture(name="object_detection_model")
def object_detection_model_fixture():
    """Fixture for an object detection model."""
    return ModelInfo(
        run_id="123",
        description="dummy_description",
        name="A model",
        baseline_model="object_detection_xyz_model",
        model_type_name="object_detection_model",
        epochs=42,
        batch_size=2,
        score=0.9,
        datasets=["dummy dataset", "test_dataset"],
        evaluation_datasets=["dummy eval dataset"],
        training_state="finished",
        algorithm_type="object_detection",
        creation_date="2023-08-02 00:00:00",
    )


@pytest.fixture(name="segmentation_model")
def segmentation_model_fixture():
    """Fixture for a segmentation model."""
    return ModelInfo(
        run_id="456",
        description="dummy_description",
        name="B model",
        baseline_model="segmentation_abc_model",
        model_type_name="segmentation_model",
        epochs=3,
        batch_size=1,
        score=0.0,
        datasets=["test other dataset", "test_123_dataset"],
        evaluation_datasets=["test eval dataset"],
        training_state="running",
        algorithm_type="segmentation",
        creation_date="2023-08-02 00:00:00",
    )


@pytest.fixture(name="classification_model")
def classification_model_fixture():
    """Fixture for a classification model."""
    return ModelInfo(
        run_id="789",
        description="",
        name="C model",
        baseline_model="classification_123_model",
        model_type_name="classification_model",
        epochs=10,
        batch_size=5,
        score=0.7,
        datasets=["dataset", "second_dataset"],
        evaluation_datasets=["eval dataset"],
        training_state="crashed",
        algorithm_type="classification",
        creation_date="2023-08-02 00:00:00",
    )


@pytest.fixture(name="registered_models")
def registered_models_fixture(object_detection_model, segmentation_model, classification_model):
    """Fixture to get registered models."""
    return [object_detection_model, segmentation_model, classification_model]


@pytest.fixture(name="registered_fifty_models")
def registered_fifty_models_fixture():
    """Fixture to get fifty registered models."""
    models = []
    for i in range(50):
        models.append(
            ModelInfo(
                run_id=f"{i}",
                description="",
                name="static name",
                baseline_model="",
                model_type_name="",
                epochs=0,
                batch_size=0,
                score=0.0,
                datasets=[""],
                evaluation_datasets=[""],
                training_state="",
                algorithm_type="",
                creation_date="2023-08-02 00:00:00",
            )
        )
    return models


@pytest.fixture(name="get_presigned_url")
def get_presigned_url_fixture():
    """Fixture to get a presigned url."""
    return ["https://test.com/presigned_url_1", "https://test.com/presigned_url_2"]


@pytest.fixture(name="get_registered_model")
def get_registered_model_fixture():
    """Fixture to get a registered model."""
    model = MagicMock()
    latest_version = MagicMock()
    latest_version.source = "s3://endpoint.com/test_bucket/test_model_prefix/model.pth"
    model.latest_versions = [latest_version]
    return model


@pytest.fixture(name="get_model_config_location")
def get_model_config_location_fixture():
    """Fixture to get the model config location."""
    return S3Data(uri="s3://endpoint.com/test_bucket/test_model/config.yaml")


@pytest.fixture(name="mlflow_adapter")
def mlflow_adapter_fixture(get_registered_model, get_model_config_location, get_presigned_url):
    """Fixture for the MlflowAdapter."""
    mlflow_adapter_mock = MagicMock()
    mlflow_adapter_mock.get_model_config_dict.return_value = {
        "class_mapping": {
            "model_classes": [
                {"class_name": "test_A", "class_id": 0},
                {"class_name": "test_B", "class_id": 1},
            ]
        }
    }
    mlflow_adapter_mock.get_model_artifact_location.return_value = get_model_config_location
    mlflow_adapter_mock.s3_adapter.generate_presigned_urls.return_value = get_presigned_url
    return mlflow_adapter_mock


@pytest.fixture(name="mlflow_adapter_all_models")
def mlflow_adapter_all_models_fixture(registered_models):
    """Fixture for the MlflowAdapter with all models."""
    mlflow_adapter_mock = MagicMock()
    mlflow_adapter_mock.get_model_infos_from_registry.return_value = registered_models
    return mlflow_adapter_mock


@pytest.fixture(name="mlflow_adapter_single_models")
def mlflow_adapter_single_fixture(object_detection_model):
    """Fixture for the MlflowAdapter with a single model."""
    mlflow_adapter_mock = MagicMock()
    mlflow_adapter_mock.get_model_infos_from_registry.return_value = [object_detection_model]
    return mlflow_adapter_mock


@pytest.fixture(name="mlflow_adapter_no_model")
def mlflow_adapter_no_model_fixture():
    """Fixture for the MlflowAdapter with no model."""
    mlflow_adapter_mock = MagicMock()
    mlflow_adapter_mock.get_model_infos_from_registry.return_value = []
    return mlflow_adapter_mock


@pytest.fixture(name="mlflow_adapter_rest_error")
def mlflow_adapter_rest_error_fixture():
    """Fixture for the MlflowAdapter to raise an MLflow error."""
    mlflow_adapter_mock = MagicMock()
    mlflow_adapter_mock.get_model_infos_from_registry.side_effect = RestException(
        json={"message": "Model not found"}
    )
    return mlflow_adapter_mock


@pytest.fixture(name="mlflow_adapter_all_fifty_models")
def mlflow_adapter_fifty_models_fixture(registered_fifty_models):
    """Fixture for the MlflowAdapter with fifty models."""
    mlflow_adapter_mock = MagicMock()
    mlflow_adapter_mock.get_model_infos_from_registry.return_value = registered_fifty_models
    return mlflow_adapter_mock


def test_get_fp_fn_images(mlflow_adapter):
    """Test get_fp_fn_images method.

    Expected behaviour:
        - Call the correct methods from the mlflow and s3 connectors with the expected parameters
        - Return the expected results
    """
    service = ModelService(mlflow_adapter=mlflow_adapter)
    result = service.get_fp_fn_images("test_model")

    # check results
    expected_urls = {
        "0_test_A": ["https://test.com/presigned_url_1", "https://test.com/presigned_url_2"],
        "1_test_B": ["https://test.com/presigned_url_1", "https://test.com/presigned_url_2"],
    }
    assert result == expected_urls


def test_parse_fp_fn_image_prefixes(mlflow_adapter):
    """Test get_fp_fn_images method.

    Expected behaviour:
        - Call the correct methods from the mlflow and s3 connectors with the expected parameters
        - Return the expected results
    """
    service = ModelService(mlflow_adapter=mlflow_adapter)

    # Check that model_prefix is correctly combined with no double "//"
    image_prefixes = service._ModelService__parse_fp_fn_image_prefixes(
        model_prefix="test_bucket/test_model/",
        model_classes=[
            {"class_id": 0, "class_name": "test_A"},
            {"class_id": 1, "class_name": "test_B"},
        ],
    )

    assert [
        ("test_bucket/test_model/fp_fn_images/0_test_A", "0_test_A"),
        ("test_bucket/test_model/fp_fn_images/1_test_B", "1_test_B"),
    ] == image_prefixes

    # Check that model_prefix is correctly combined with no double "//"
    image_prefixes = service._ModelService__parse_fp_fn_image_prefixes(
        model_prefix="test_bucket/test_model",
        model_classes=[
            {"class_id": 0, "class_name": "test_A"},
            {"class_id": 1, "class_name": "test_B"},
        ],
    )

    assert [
        ("test_bucket/test_model/fp_fn_images/0_test_A", "0_test_A"),
        ("test_bucket/test_model/fp_fn_images/1_test_B", "1_test_B"),
    ] == image_prefixes


def test_load_models_unfiltered(registered_models, mlflow_adapter_all_models):
    """Test load_models method without filtering."""
    service = ModelService(mlflow_adapter=mlflow_adapter_all_models)
    result = service.load_models()
    models = result["models"]

    assert result["total_items"] == len(registered_models)

    first = asdict(registered_models[0])
    first["dataset"] = first["datasets"]
    first["accuracy"] = first["score"]

    second = asdict(registered_models[1])
    second["dataset"] = second["datasets"]
    second["accuracy"] = second["score"]

    third = asdict(registered_models[2])
    third["dataset"] = third["datasets"]
    third["accuracy"] = third["score"]

    assert models[0] == first
    assert models[1] == second
    assert models[2] == third


@pytest.mark.parametrize(
    "order, descending, expected_order",
    [
        ("name", True, ["C model", "B model", "A model"]),
        ("epochs", True, [42, 10, 3]),
        ("batch_size", False, [1, 2, 5]),
        ("training_state", False, ["crashed", "finished", "running"]),
        ("algorithm_type", False, ["classification", "object_detection", "segmentation"]),
    ],
)
def test_load_models(mlflow_adapter_all_models, order, descending, expected_order):
    """Test load models with different order and descending parameters."""
    service = ModelService(mlflow_adapter=mlflow_adapter_all_models)
    result = service.load_models(order=order, descending=descending)
    models = result["models"]

    assert [model[order] for model in models] == expected_order


def test_load_models_five_per_page(mlflow_adapter_all_fifty_models):
    """Test if 5 models are returned."""
    service = ModelService(mlflow_adapter=mlflow_adapter_all_fifty_models)
    result = service.load_models(per_page=5, page=1)
    models = result["models"]

    assert len(models) == 5

    for ind, model in enumerate(models):
        assert model["run_id"] == str(ind)


@pytest.mark.parametrize(
    "page, per_page",
    [(1, 10), (3, 10), (5, 10), (2, 5), (4, 5), (6, 5), (10, 1), (10, 3), (10, 5)],
)
def test_load_models_ten_per_page(mlflow_adapter_all_fifty_models, page, per_page):
    """Test load_models with different per_page and page parameters."""
    service = ModelService(mlflow_adapter=mlflow_adapter_all_fifty_models)
    result = service.load_models(per_page=per_page, page=page)
    models = result["models"]

    assert len(models) == per_page
    for ind, model in enumerate(models):
        assert model["run_id"] == str(ind + per_page * (page - 1))


def test_load_model(mlflow_adapter_single_models, object_detection_model):
    """Test load_model method."""
    service = ModelService(mlflow_adapter=mlflow_adapter_single_models)
    result = service.load_model("test_model")

    expected_model = asdict(object_detection_model)
    expected_model["dataset"] = expected_model["datasets"]
    expected_model["accuracy"] = expected_model["score"]

    assert result["total_items"] == 1
    assert result["models"][0] == expected_model


def test_load_model_not_found(mlflow_adapter_no_model):
    """Test load_model method with a model that is not found."""
    service = ModelService(mlflow_adapter=mlflow_adapter_no_model)
    with pytest.raises(ModelNotFoundError):
        service.load_model("not_found_model")


def test_load_model_rest_error(mlflow_adapter_rest_error):
    """Test load_model method with a model that raises an MLflow error."""
    service = ModelService(mlflow_adapter=mlflow_adapter_rest_error)
    with pytest.raises(ModelNotFoundError):
        service.load_model("test_model")
