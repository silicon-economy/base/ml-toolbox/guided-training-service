# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the EdgeDeviceService."""
import json
from datetime import datetime
from unittest import mock

import pytest
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from requests.exceptions import HTTPError

from gts_backend.model.edge_device import EdgeDeviceModelSQL
from gts_backend.services.dbmanager import db
from gts_backend.services.edge_devices.edge_device_service import (
    EdgeDeviceNotFoundError,
    EdgeDeviceNotRecordingError,
    EdgeDeviceService,
)


@pytest.fixture(name="single_edge_device")
def single_edge_device_fixture():
    """Fixture for a single edge device."""
    return EdgeDeviceModelSQL(
        uri="test_uri",
        name="test_name",
        device_type="test_device_type",
        creation_date=datetime.now(),
    )


@pytest.fixture(name="multiple_edge_devices")
def multiple_edge_devices_fixture(single_edge_device):
    """Fixture for multiple edge devices."""
    return [
        single_edge_device,
        EdgeDeviceModelSQL(
            uri="test_uri_2",
            name="test_name_2",
            device_type="test_device_type_2",
            creation_date=datetime.now(),
        ),
    ]


@pytest.fixture(name="mlflow_credentials")
def mlflow_credentials_fixture():
    """Fixture for MLFlow credentials."""
    return MlflowCredentials(
        server_uri="test_server_uri",
        s3_region_name="test_s3_region_name",
        s3_endpoint_url="test_s3_endpoint_url",
        s3_artifact_access_key="test_s3_artifact_access_key",
        s3_artifact_access_id="test_s3_artifact_access_id",
        run_id=None,
    )


@pytest.fixture(name="mlflow_credentials_mock")
def mlflow_credentials_mock_fixture(mocker, mlflow_credentials):
    """Fixture for MLFlow credentials."""
    mocker.patch(
        "gts_backend.services.edge_devices.edge_device_service.MlflowCredentials.init_from_os_environment",
        return_value=mlflow_credentials,
    )


@pytest.fixture(name="s3_credentials")
def s3_credentials_fixture():
    """Fixture for MLFlow credentials."""
    return S3Credentials(
        s3_region_name="test_region",
        s3_endpoint_url="test_s3_endpoint_url",
        s3_artifact_access_key="test_s3_artifact_access_key",
        s3_artifact_access_id="test_s3_artifact_access_id",
    )


@pytest.fixture(name="s3_credentials_mock")
def s3_credentials_mock_fixture(mocker, s3_credentials):
    """Fixture for MLFlow credentials."""
    mocker.patch(
        "gts_backend.services.edge_devices.edge_device_service.S3Credentials.init_from_os_environment",
        return_value=s3_credentials,
    )


@pytest.fixture(name="test_app")
def test_app_fixture(single_edge_device):
    """Fixture for a test app."""
    from gts_backend.app import Server

    app = Server.create_app(config="test")

    yield app.app_context()

    del app


@pytest.fixture(name="sql_one_device_query")
def sql_one_device_query_fixture(mocker, single_edge_device):
    """Fixture for a query for one edge device."""
    mocker.patch(
        "gts_backend.services.edge_devices.edge_device_service.EdgeDeviceModelSQL.get",
        return_value=single_edge_device,
    )


@pytest.fixture(name="sql_all_device_query")
def sql_multi_device_query_fixture(mocker, multiple_edge_devices):
    """Fixture for a query for all edge devices."""
    mocker.patch(
        "gts_backend.services.edge_devices.edge_device_service.EdgeDeviceModelSQL.query.with_entities.order_by.all",
        return_value=multiple_edge_devices_fixture,
    )


def test_load_edge_devices(multiple_edge_devices, test_app):
    """Test loading edge devices."""
    with test_app:
        first_device, second_device = multiple_edge_devices

        db.session.add(first_device)
        db.session.add(second_device)
        db.session.commit()
        device_1, device_2 = EdgeDeviceService.load_edge_devices()

        assert device_1.id == 1
        assert device_1.uri == first_device.uri
        assert device_1.name == first_device.name
        assert device_1.device_type == first_device.device_type
        assert device_1.creation_date == first_device.creation_date
        assert device_2.id == 2
        assert device_2.uri == second_device.uri
        assert device_2.name == second_device.name
        assert device_2.device_type == second_device.device_type
        assert device_2.creation_date == second_device.creation_date


def test_add_edge_device(single_edge_device, test_app):
    """Test adding an edge device."""
    with test_app:
        device = EdgeDeviceService.add_edge_device(
            uri=single_edge_device.uri,
            name=single_edge_device.name,
            device_type=single_edge_device.device_type,
        )

        assert device.id == 1
        assert device.uri == single_edge_device.uri
        assert device.name == single_edge_device.name
        assert device.device_type == single_edge_device.device_type


def test_load_single_edge_device(single_edge_device, test_app):
    """Test loading a single edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()
        device = EdgeDeviceService().load_edge_device(device_id=1)

        assert device["id"] == 1
        assert device["uri"] == single_edge_device.uri
        assert device["name"] == single_edge_device.name
        assert device["device_type"] == single_edge_device.device_type
        assert device["stream"] == f"{single_edge_device.uri}/stream/"
        assert device["is_recording"] is False


def test_load_single_edge_device_not_found(test_app):
    """Test if the expected error is raised if a specified edge device is not found."""
    with test_app:
        with pytest.raises(EdgeDeviceNotFoundError):
            EdgeDeviceService().load_edge_device(device_id=1)


def test_delete_edge_device(single_edge_device, test_app):
    """Test deleting an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        assert EdgeDeviceModelSQL.query.get(1) is not None

        EdgeDeviceService().delete_edge_device(device_id=1)

        assert EdgeDeviceModelSQL.query.get(1) is None


def test_delete_edge_device_error(single_edge_device, test_app, mocker):
    """Test error handling when deleting an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        # Mock the db.session.delete method to raise an exception
        mocker.patch(
            "gts_backend.services.dbmanager.db.session.delete",
            side_effect=Exception("Deletion error"),
        )

        with pytest.raises(Exception, match="Deletion error"):
            EdgeDeviceService().delete_edge_device(device_id=1)


def test_start_deployment_success(
    single_edge_device, test_app, mocker, mlflow_credentials, mlflow_credentials_mock
):
    """Test successful deployment on an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        # Mock the requests.post method to simulate a successful deployment
        mock_post = mocker.patch("requests.post", return_value=mocker.Mock(status_code=200))

        EdgeDeviceService().start_deployment(device_id=1, model_name="test_model")

        # Assert that the requests.post method was called with the correct URL and data
        expected_credentials = mlflow_credentials.to_dict()
        expected_credentials.pop("run_id")

        # data is formatted as json string
        expected_data = json.dumps(
            {"unique_name": "test_model", "mlflow_credentials": expected_credentials}
        )

        mock_post.assert_called_once_with(
            url=f"{single_edge_device.uri}/deploy/",
            data=expected_data,
            headers={"Content-Type": "application/json"},
        )


def test_start_deployment_fails(
    single_edge_device, test_app, mocker, mlflow_credentials, mlflow_credentials_mock
):
    """Test failed deployment on an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        # Mock the requests.post method to simulate a failed deployment
        mocker.patch("requests.post", side_effect=HTTPError("Deployment failed"))

        with pytest.raises(HTTPError, match="Deployment failed"):
            EdgeDeviceService().start_deployment(device_id=1, model_name="test_model")


def test_stop_deployment_success(single_edge_device, test_app, mocker):
    """Test successful stopping of deployment on an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        # Mock the requests.post method to simulate a successful deployment stop
        mock_post = mocker.patch("requests.post", return_value=mocker.Mock(status_code=200))

        EdgeDeviceService().stop_deployment(device_id=1)

        # Assert that the requests.post method was called with the correct URL
        mock_post.assert_called_once_with(url=f"{single_edge_device.uri}/deploy/stop")


def test_stop_deployment_fails(single_edge_device, test_app, mocker):
    """Test failed stopping of deployment on an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        # Mock the requests.post method to simulate a failed deployment stop
        mocker.patch("requests.post", side_effect=HTTPError("Deployment stop failed"))

        with pytest.raises(HTTPError, match="Deployment stop failed"):
            EdgeDeviceService().stop_deployment(device_id=1)


def test_start_recording_success(
    single_edge_device, test_app, mocker, s3_credentials_mock, s3_credentials
):
    """Test successful starting of recording on an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        # Mock the requests.post method to simulate a successful recording start
        mock_post = mocker.patch("requests.put", return_value=mocker.Mock(status_code=200))

        EdgeDeviceService().start_recording(device_id=1)

        expected_zip_location = {
            "uri": f"{s3_credentials.s3_endpoint_url}/edge-devices/1",
            "location_id": "",
            "credentials": s3_credentials.to_dict(),
        }
        expected_data = json.dumps(
            {"zip_location": expected_zip_location, "recording_interval": 1}
        )

        # Assert that the requests.post method was called with the correct URL
        mock_post.assert_called_once_with(
            url=f"{single_edge_device.uri}/data_recording/start",
            data=expected_data,
            headers={"Content-Type": "application/json"},
        )


def test_start_recording_fails(single_edge_device, test_app, mocker, s3_credentials_mock):
    """Test failed starting of recording on an edge device."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        # Mock the requests.post method to simulate a failed recording start
        mocker.patch("requests.put", side_effect=HTTPError("Recording start failed"))

        with pytest.raises(HTTPError, match="Recording start failed"):
            EdgeDeviceService().start_recording(device_id=1)


def test_stop_recording_success(single_edge_device, test_app, mocker, s3_credentials_mock):
    """Test successful stopping of recording on an edge device."""
    recording_device = single_edge_device
    recording_device.is_recording = True
    with test_app:
        db.session.add(recording_device)
        db.session.commit()

        # Mock the requests.post method to simulate a successful recording stop
        mock_post = mocker.patch(
            "requests.put",
            return_value=mocker.Mock(status_code=200, text="'s3://test_bucket/test_prefix'"),
        )

        # mock presigned url
        mock_s3_adapter = mock.MagicMock()
        mock_s3_adapter.generate_presigned_url.return_value = "test_presigned_url"
        mocker.patch(
            "gts_backend.services.edge_devices.edge_device_service.S3Adapter",
            return_value=mock_s3_adapter,
        )

        url = EdgeDeviceService().stop_recording(device_id=1)

        assert EdgeDeviceModelSQL.query.get(1).is_recording is False
        assert url == "test_presigned_url"
        mock_post.assert_called_once_with(url=f"{single_edge_device.uri}/data_recording/stop")


def test_stop_recording_not_recording(single_edge_device, test_app):
    """Test stopping of recording on an edge device that is not recording."""
    with test_app:
        db.session.add(single_edge_device)
        db.session.commit()

        with pytest.raises(EdgeDeviceNotRecordingError):
            EdgeDeviceService().stop_recording(device_id=1)
