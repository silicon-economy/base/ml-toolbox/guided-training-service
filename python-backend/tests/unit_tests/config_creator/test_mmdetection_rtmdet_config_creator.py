# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the MMDetectionConfigCreator used for building MLCVZoo specific configuration files within the mmdetection
framework.
"""
import copy
import unittest
from typing import Any

import yaml
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_mmdetection.configuration import (
    MMDetectionConfig,
    MMDetectionTrainArgparseConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
    ConfigCreatorDatasetConfig,
    ModelParameterConfig,
)
from gts_backend.services.configuration_creator.mmdetection.creator import (
    MMDetectionConfigCreator,
)
from gts_backend.services.configuration_creator.mmdetection.templates import (
    MMDETECTION_CONFIG_TEMPLATES,
)
from gts_backend.services.configuration_creator.structs import MMDetectionModels
from gts_backend.services.jobs.training.structs import DataRootDirs


class TestMMDetectionConfigCreator(unittest.TestCase):
    """
    Collection of tests for initializing MMDetectionConfigs from the MMDetectionConfigCreator class.
    """

    @classmethod
    def setUpClass(cls) -> None:
        cls.bucket_name: str = "test_bucket"
        cls.s3_prefixes: list[str] = ["test_prefix1", "test_prefix2"]
        cls.annotation_handler_config: AnnotationHandlerConfig = AnnotationHandlerConfig()
        # RTMDet l
        cls.configuration_base_rtmdet_l: ConfigCreatorConfig = ConfigCreatorConfig(
            parameters=ModelParameterConfig(
                epochs=20,
                batch_size=16,
                dataset_names=["test_dataset1", "test_dataset2"],
                model="mmdetection_object_detection__rtmdet_l_8xb32-300e_coco_20220719_112030-5a0be7c4",
                model_version="rtmdet_l_version",
                nms_threshold=0.3,
                score_threshold=0.5,
            ),
            dataset=ConfigCreatorDatasetConfig(
                class_mapping=ClassMappingConfig(
                    model_classes=[
                        ClassMappingModelClassesConfig(class_name=c, class_id=i)
                        for i, c in enumerate(["class1", "class2", "class3"])
                    ],
                    mapping=[
                        ClassMappingMappingConfig(
                            input_class_name="class1", output_class_name="class4"
                        )
                    ],
                    number_classes=3,
                )
            ),
        )

        # m
        cls.configuration_base_rtmdet_m = copy.deepcopy(cls.configuration_base_rtmdet_l)
        cls.configuration_base_rtmdet_m.parameters.model = (
            "mmdetection_object_detection__rtmdet_m_8xb32-300e_coco_20220719_112220-229f527c"
        )
        cls.configuration_base_rtmdet_m.parameters.model_version = "rtmdet_m_version"

        # x
        cls.configuration_base_rtmdet_x = copy.deepcopy(cls.configuration_base_rtmdet_l)
        cls.configuration_base_rtmdet_x.parameters.model = (
            "mmdetection_object_detection__rtmdet_x_8xb32-300e_coco_20220715_230555-cc79b9ae"
        )
        cls.configuration_base_rtmdet_x.parameters.model_version = "rtmdet_x_version"

        # s
        cls.configuration_base_rtmdet_s = copy.deepcopy(cls.configuration_base_rtmdet_l)
        cls.configuration_base_rtmdet_s.parameters.model = (
            "mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e"
        )
        cls.configuration_base_rtmdet_s.parameters.model_version = "rtmdet_s_version"

        # tiny based on s
        cls.configuration_base_rtmdet_tiny = copy.deepcopy(cls.configuration_base_rtmdet_s)
        cls.configuration_base_rtmdet_tiny.parameters.model = (
            "mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc"
        )
        cls.configuration_base_rtmdet_tiny.parameters.model_version = "rtmdet_tiny_version"


        # RTMdet l
        cls.creator_base_rtmdet_l: MMDetectionConfigCreator = MMDetectionConfigCreator(
            configuration=cls.configuration_base_rtmdet_l
        )
        # m
        cls.creator_base_rtmdet_m: MMDetectionConfigCreator = MMDetectionConfigCreator(
            configuration=cls.configuration_base_rtmdet_m
        )  # No Test function
        # x
        cls.creator_base_rtmdet_x: MMDetectionConfigCreator = MMDetectionConfigCreator(
            configuration=cls.configuration_base_rtmdet_x
        )
        # s
        cls.creator_base_rtmdet_s: MMDetectionConfigCreator = MMDetectionConfigCreator(
            configuration=cls.configuration_base_rtmdet_s
        )  # No Test function
        # tiny
        cls.creator_base_rtmdet_tiny: MMDetectionConfigCreator = MMDetectionConfigCreator(
            configuration=cls.configuration_base_rtmdet_tiny
        )  # No Test function
        # add additional MMDetectionConfigCreators for mmdetection models here

    def test_init_mmdetection_config_creator_base_rtmdet_l(self) -> None:
        """
        Test if a RTMDet MMDetectionConfig is correctly loaded with base parameters.
        """
        configuration: MMDetectionConfig = self.creator_base_rtmdet_l.create_model_config(
            bucket_name=self.bucket_name, s3_prefixes=self.s3_prefixes
        )
        self.assertEqual(
            configuration.unique_name, self.configuration_base_rtmdet_l.parameters.model_version
        )
        self.assertEqual(
            configuration.class_mapping, self.configuration_base_rtmdet_l.dataset.class_mapping
        )
        self.assertEqual(
            configuration.class_mapping.model_classes,
            self.configuration_base_rtmdet_l.dataset.class_mapping.model_classes,
        )
        self.assertEqual(
            configuration.class_mapping.number_model_classes,
            self.configuration_base_rtmdet_l.dataset.class_mapping.number_model_classes,
        )
        self.assertEqual(
            configuration.class_mapping.number_classes,
            self.configuration_base_rtmdet_l.dataset.class_mapping.number_classes,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["num_classes"],
            self.configuration_base_rtmdet_l.dataset.class_mapping.number_classes,
        )
        self.assertEqual(
            configuration.class_mapping.mapping,
            self.configuration_base_rtmdet_l.dataset.class_mapping.mapping,
        )
        self.assertEqual(
            configuration.inference_config.score_threshold,
            self.configuration_base_rtmdet_l.parameters.score_threshold,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_cfg"]["max_epochs"],
            self.configuration_base_rtmdet_l.parameters.epochs,
        )
        # Milestones specified in RTMDet as "begin", "end" and "T_max"
        self.assertEqual(
            configuration.mm_config.config_dict["param_scheduler"][1]["begin"],
            int(self.configuration_base_rtmdet_l.parameters.epochs // 2),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["param_scheduler"][1]["end"],
            int(self.configuration_base_rtmdet_l.parameters.epochs),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["param_scheduler"][1]["T_max"],
            int(self.configuration_base_rtmdet_l.parameters.epochs // 2),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["default_hooks"]["checkpoint"]["filename_tmpl"],
            str(self.configuration_base_rtmdet_l.parameters.model_version) + "_epoch_{:04d}.pth",
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["test_cfg"]["nms"]["iou_threshold"],
            self.configuration_base_rtmdet_l.parameters.nms_threshold,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_dataloader"]["batch_size"],
            self.configuration_base_rtmdet_l.parameters.batch_size,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["load_from"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__rtmdet_l_8xb32-300e_coco_20220719_112030-5a0be7c4.pth",
        )

    def test_init_mmdetection_config_creator_base_rtmdet_m(self) -> None:
        """
        Test if a RTMDet MMDetectionConfig is correctly loaded with base parameters.
        """
        configuration: MMDetectionConfig = self.creator_base_rtmdet_m.create_model_config(
            bucket_name=self.bucket_name, s3_prefixes=self.s3_prefixes
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["deepen_factor"],
            0.67,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["widen_factor"],
            0.75,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["in_channels"],
            [192, 384, 768],
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["out_channels"],
            192,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["num_csp_blocks"],
            2,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["in_channels"],
            192,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["feat_channels"],
            192,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["load_from"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__rtmdet_m_8xb32-300e_coco_20220719_112220-229f527c.pth",
        )

    def test_init_mmdetection_config_creator_base_rtmdet_x(self) -> None:
        """
        Test if a RTMDet MMDetectionConfig is correctly loaded with base parameters.
        """
        configuration: MMDetectionConfig = self.creator_base_rtmdet_x.create_model_config(
            bucket_name=self.bucket_name, s3_prefixes=self.s3_prefixes
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["deepen_factor"],
            1.33,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["widen_factor"],
            1.25,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["in_channels"],
            [320, 640, 1280],
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["out_channels"],
            320,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["num_csp_blocks"],
            4,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["in_channels"],
            320,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["feat_channels"],
            320,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["load_from"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__rtmdet_x_8xb32-300e_coco_20220715_230555-cc79b9ae.pth",
        )

    def test_init_mmdetection_config_creator_base_rtmdet_s(self) -> None:
        """
        Test if a RTMDet MMDetectionConfig is correctly loaded with base parameters.
        """
        configuration: MMDetectionConfig = self.creator_base_rtmdet_s.create_model_config(
            bucket_name=self.bucket_name, s3_prefixes=self.s3_prefixes
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_pipeline"][3]["ratio_range"],
            (0.5, 2.0),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_dataloader"]["dataset"]["pipeline"][3][
                "ratio_range"
            ],
            (0.5, 2.0),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["deepen_factor"],
            0.33,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["widen_factor"],
            0.5,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["init_cfg"],
            {
                "type": "Pretrained",
                "prefix": "backbone.",
                "checkpoint": f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
                f"/mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e.pth",
            },
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["in_channels"],
            [128, 256, 512],
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["out_channels"],
            128,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["num_csp_blocks"],
            1,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["in_channels"],
            128,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["feat_channels"],
            128,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["exp_on_reg"],
            False,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_pipeline_stage2"][2]["ratio_range"],
            (0.5, 2.0),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["custom_hooks"][1]["switch_pipeline"][2][
                "ratio_range"
            ],
            (0.5, 2.0),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["checkpoint"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e.pth",
        )
        self.assertEqual(
            configuration.mm_config.config_dict["load_from"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e.pth",
        )

    def test_init_mmdetection_config_creator_base_rtmdet_tiny(self) -> None:
        """
        Test if a RTMDet MMDetectionConfig is correctly loaded with base parameters.
        """
        configuration: MMDetectionConfig = self.creator_base_rtmdet_tiny.create_model_config(
            bucket_name=self.bucket_name, s3_prefixes=self.s3_prefixes
        )
        # this two are same as ... :
        self.assertEqual(
            configuration.mm_config.config_dict["train_pipeline"][2]["max_cached_images"],
            20,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_pipeline"][2]["random_pop"],
            False,
        )
        # ... same as this function, but here the nested dict is also controlled:
        self.assertEqual(
            configuration.mm_config.config_dict["train_pipeline"][2],
            {
                "type": "CachedMosaic",
                "img_scale": (640, 640),
                "pad_val": 114.0,
                "max_cached_images": 20,
                "random_pop": False,
            },
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_pipeline"][8],
            {
                "type": "CachedMixUp",
                "img_scale": (640, 640),
                "ratio_range": (1.0, 1.0),
                "max_cached_images": 10,
                "random_pop": False,
                "pad_val": (114, 114, 114),
                "prob": 0.5,
            },
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_dataloader"]["dataset"]["pipeline"][2],
            {
                "type": "CachedMosaic",
                "img_scale": (640, 640),
                "pad_val": 114.0,
                "max_cached_images": 20,
                "random_pop": False,
            },
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_dataloader"]["dataset"]["pipeline"][8],
            {
                "type": "CachedMixUp",
                "img_scale": (640, 640),
                "ratio_range": (1.0, 1.0),
                "max_cached_images": 10,
                "random_pop": False,
                "pad_val": (114, 114, 114),
                "prob": 0.5,
            },
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["deepen_factor"],
            0.167,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["widen_factor"],
            0.375,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["backbone"]["init_cfg"],
            {
                "type": "Pretrained",
                "prefix": "backbone.",
                "checkpoint": f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
                f"/mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc.pth",
            },
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["in_channels"],
            [96, 192, 384],
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["neck"]["out_channels"],
            96,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["in_channels"],
            96,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["bbox_head"]["feat_channels"],
            96,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["checkpoint"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc.pth",
        )
        self.assertEqual(
            configuration.mm_config.config_dict["load_from"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc.pth",
        )
