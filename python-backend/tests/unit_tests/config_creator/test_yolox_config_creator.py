# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the YOLOXConfigCreator used for creating MLCVZoo specific configuration files
"""
import unittest

from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingMappingConfig,
    ClassMappingModelClassesConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
    ConfigCreatorDatasetConfig,
    ModelParameterConfig,
)
from gts_backend.services.configuration_creator.yolox.creator import YOLOXConfigCreator
from gts_backend.services.jobs.training.structs import DataRootDirs


class TestYOLOXConfigCreator(unittest.TestCase):
    def setUp(self) -> None:
        self.configuration_base: ConfigCreatorConfig = ConfigCreatorConfig(
            parameters=ModelParameterConfig(
                epochs=20,
                batch_size=4,
                model="yolox__nano",
                model_version="yolox__nano_version",
                dataset_names=["test_dataset_1", "test_dataset_2"],
                nms_threshold=0.8,
                score_threshold=0.8,
            ),
            dataset=ConfigCreatorDatasetConfig(
                class_mapping=ClassMappingConfig(
                    model_classes=[
                        ClassMappingModelClassesConfig(class_name=c, class_id=i)
                        for i, c in enumerate(["cat", "mouse", "dog"])
                    ],
                    mapping=[
                        ClassMappingMappingConfig(
                            input_class_name="bird",
                            output_class_name="cat",
                        )
                    ],
                    number_classes=3,
                ),
            ),
        )

        self.creator = YOLOXConfigCreator(configuration=self.configuration_base)

    def test_creation_of_yolox_config_creator_instance(self):
        """
        Tests if instance of YOLOXConfigCreator is successfully initialized with associated
        config objects
        """

        self.assertIsInstance(self.creator, YOLOXConfigCreator)

    def test_correct_parsing_of_base_yolox_config_creator_config(self):
        """
        Tests if the parameters specified in the base test config are parsed correctly
        """

        bucket = "test_bucket"

        configuration = self.creator.create_model_config(
            bucket_name=bucket, s3_prefixes=["test_prefix_1", "test_prefix_2"]
        )

        self.assertEqual(configuration.experiment_config.attribute_overwrite["max_epoch"], 20)

        self.assertEqual(configuration.train_config.argparse_config.batch_size, 4)

        self.assertEqual(
            configuration.train_config.argparse_config.ckpt,
            f"BASELINE_MODEL_DIR/{bucket}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/yolox__nano.pth",
        )

        self.assertEqual(configuration.inference_config.nms_threshold, 0.8)

        self.assertEqual(configuration.inference_config.score_threshold, 0.8)

        self.assertEqual(
            configuration.train_config.train_annotation_handler_config.class_mapping.model_classes,
            [
                ClassMappingModelClassesConfig(class_name=c, class_id=i)
                for i, c in enumerate(["cat", "mouse", "dog"])
            ],
        )

        self.assertEqual(
            configuration.train_config.train_annotation_handler_config.class_mapping.number_classes,
            3,
        )

        self.assertEqual(
            configuration.train_config.train_annotation_handler_config.class_mapping.mapping,
            [
                ClassMappingMappingConfig(
                    input_class_name="bird",
                    output_class_name="cat",
                )
            ],
        )

    def test_parse_input_dim_for_yolox_nano(self):
        """
        Tests if correct input dimension is set, if yolox_nano model is specified
        """

        self.creator.configuration.parameters.model = "yolox__nano"

        configuration = self.creator.create_model_config(
            bucket_name="test_bucket", s3_prefixes=["test_prefix_1", "test_prefix_2"]
        )

        self.assertEqual(
            configuration.experiment_config.attribute_overwrite["input_size"],
            [416, 416],
        )

        self.assertEqual(
            configuration.experiment_config.attribute_overwrite["test_size"], [416, 416]
        )

    def test_parse_input_dim_for_yolox_x(self):
        """
        Tests if correct input dimension is set, if yolox_x model is specified
        """

        self.creator.configuration.parameters.model = "yolox__x"

        configuration = self.creator.create_model_config(
            bucket_name="test_bucket", s3_prefixes=["test_prefix_1", "test_prefix_2"]
        )

        self.assertEqual(
            configuration.experiment_config.attribute_overwrite["input_size"],
            [640, 640],
        )

        self.assertEqual(
            configuration.experiment_config.attribute_overwrite["test_size"], [640, 640]
        )


if __name__ == "__main__":
    unittest.main()
