# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the MMPretrainConfigCreator used for building MLCVZoo specific configuration files within the mmpretrain
framework.
"""
import unittest
from typing import Any

import yaml
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_mmpretrain.configuration import (
    MMPretrainConfig,
    MMPretrainTrainArgparseConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
    ConfigCreatorDatasetConfig,
    ModelParameterConfig,
)
from gts_backend.services.configuration_creator.mmpretrain.creator import (
    MMPretrainConfigCreator,
)
from gts_backend.services.configuration_creator.mmpretrain.templates import (
    MMPRETRAIN_CONFIG_TEMPLATES,
)
from gts_backend.services.configuration_creator.structs import MMPretrainModels
from gts_backend.services.jobs.training.structs import DataRootDirs


class TestMMPretrainConfigCreator(unittest.TestCase):
    """
    Collection of tests for initializing MMPretrainConfigs from the MMPretrainConfigCreator class.
    """

    def setUp(self) -> None:
        self.bucket_name: str = "test_bucket"
        self.s3_prefixes: list[str] = ["test_prefix1", "test_prefix2"]
        self.annotation_handler_config: AnnotationHandlerConfig = AnnotationHandlerConfig()
        self.configuration_base_efficientnet_b0: ConfigCreatorConfig = ConfigCreatorConfig(
            parameters=ModelParameterConfig(
                epochs=20,
                batch_size=16,
                dataset_names=["test_dataset1", "test_dataset2"],
                model="mmpretrain__efficientnet-b0_3rdparty_8xb32_in1k_20220119-a7e2a0b1",
                model_version="efficientnet_version",
                nms_threshold=0.3,
                score_threshold=0.5,
            ),
            dataset=ConfigCreatorDatasetConfig(
                class_mapping=ClassMappingConfig(
                    model_classes=[
                        ClassMappingModelClassesConfig(class_name=c, class_id=i)
                        for i, c in enumerate(["class1", "class2", "class3"])
                    ],
                    mapping=[
                        ClassMappingMappingConfig(
                            input_class_name="class1", output_class_name="class4"
                        )
                    ],
                    number_classes=3,
                )
            ),
        )

        self.creator_base_efficientnet_b0: MMPretrainConfigCreator = MMPretrainConfigCreator(
            configuration=self.configuration_base_efficientnet_b0
        )

    def test_init_mmpretrain_config_creator_efficientnet_b0_basic(self):
        """Test if an efficientnet b0 MMPretrainConfig is correctly loaded.

        Only the dynamically parsed parameters are tested here. Static parameters are not tested.
        """
        configuration: MMPretrainConfig = self.creator_base_efficientnet_b0.create_model_config(
            bucket_name=self.bucket_name,
            s3_prefixes=self.s3_prefixes,
            s3_eval_prefixes=self.s3_prefixes,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["default_hooks"]["checkpoint"]["filename_tmpl"],
            str(self.configuration_base_efficientnet_b0.parameters.model_version) + "_{:04d}.pth",
        )
        self.assertEqual(
            configuration.mm_config.config_dict["load_from"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmpretrain__efficientnet-b0_3rdparty_8xb32_in1k_20220119-a7e2a0b1.pth",
        )
        self.assertEqual(
            configuration.mm_config.config_dict["data_preprocessor"]["num_classes"],
            self.configuration_base_efficientnet_b0.dataset.class_mapping.number_classes,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["param_scheduler"][0]["milestones"],
            [
                int(self.configuration_base_efficientnet_b0.parameters.epochs * 0.8),
                int(self.configuration_base_efficientnet_b0.parameters.epochs * 0.9),
            ],
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_cfg"]["max_epochs"],
            self.configuration_base_efficientnet_b0.parameters.epochs,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_dataloader"]["batch_size"],
            self.configuration_base_efficientnet_b0.parameters.batch_size,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["auto_scale"]["base_batch_size"],
            self.configuration_base_efficientnet_b0.parameters.batch_size,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["val_evaluator"]["topk"],
            (1, self.configuration_base_efficientnet_b0.dataset.class_mapping.number_classes),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["test_evaluator"]["topk"],
            (1, self.configuration_base_efficientnet_b0.dataset.class_mapping.number_classes),
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["head"]["num_classes"],
            self.configuration_base_efficientnet_b0.dataset.class_mapping.number_classes,
        )
        # static value specific for efficientnet_b0
        self.assertEqual(configuration.mm_config.config_dict["model"]["head"]["in_channels"], 1280)

    def test_init_mmpretrain_config_creator_efficientnet_b4_basic(self):
        """Test if an efficientnet b4 MMPretrainConfig is correctly loaded.

        Only the specific parameters of efficientnet_b4 are tested here.
        Shared values with efficientnet_b0 are not tested.
        """
        b4_config = self.configuration_base_efficientnet_b0
        b4_config.parameters.model = (
            "mmpretrain__efficientnet-b4_3rdparty-ra-noisystudent_in1k_20221103-16ba8a2d"
        )
        config_creator = MMPretrainConfigCreator(configuration=b4_config)
        configuration: MMPretrainConfig = config_creator.create_model_config(
            bucket_name=self.bucket_name,
            s3_prefixes=self.s3_prefixes,
            s3_eval_prefixes=self.s3_prefixes,
        )

        # static value specific for efficientnet_b0
        self.assertEqual(configuration.mm_config.config_dict["model"]["head"]["in_channels"], 1792)

    def test_init_mmpretrain_config_creator_efficientnet_b8_basic(self):
        """Test if an efficientnet b8 MMPretrainConfig is correctly loaded.

        Only the specific parameters of efficientnet_b8 are tested here.
        Shared values with efficientnet_b0 are not tested.
        """
        b4_config = self.configuration_base_efficientnet_b0
        b4_config.parameters.model = (
            "mmpretrain__efficientnet-b8_3rdparty_8xb32-aa-advprop_in1k_20220119-297ce1b7"
        )
        config_creator = MMPretrainConfigCreator(configuration=b4_config)
        configuration: MMPretrainConfig = config_creator.create_model_config(
            bucket_name=self.bucket_name,
            s3_prefixes=self.s3_prefixes,
            s3_eval_prefixes=self.s3_prefixes,
        )

        # static value specific for efficientnet_b0
        self.assertEqual(configuration.mm_config.config_dict["model"]["head"]["in_channels"], 2816)
