# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the MMDetectionConfigCreator used for building MLCVZoo specific configuration files within the mmdetection
framework.
"""
import unittest
from typing import Any

import yaml
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_mmdetection.configuration import (
    MMDetectionConfig,
    MMDetectionTrainArgparseConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
    ConfigCreatorDatasetConfig,
    ModelParameterConfig,
)
from gts_backend.services.configuration_creator.mmdetection.creator import (
    MMDetectionConfigCreator,
)
from gts_backend.services.configuration_creator.mmdetection.templates import (
    MMDETECTION_CONFIG_TEMPLATES,
)
from gts_backend.services.configuration_creator.structs import MMDetectionModels
from gts_backend.services.jobs.training.structs import DataRootDirs


class TestMMDetectionConfigCreator(unittest.TestCase):
    """
    Collection of tests for initializing MMDetectionConfigs from the MMDetectionConfigCreator class.
    """

    def setUp(self) -> None:
        self.bucket_name: str = "test_bucket"
        self.s3_prefixes: list[str] = ["test_prefix1", "test_prefix2"]
        self.annotation_handler_config: AnnotationHandlerConfig = AnnotationHandlerConfig()
        self.configuration_base_htc: ConfigCreatorConfig = ConfigCreatorConfig(
            parameters=ModelParameterConfig(
                epochs=20,
                batch_size=16,
                dataset_names=["test_dataset1", "test_dataset2"],
                model="mmdetection_object_detection__htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco",
                model_version="htc_version",
                nms_threshold=0.3,
                score_threshold=0.5,
            ),
            dataset=ConfigCreatorDatasetConfig(
                class_mapping=ClassMappingConfig(
                    model_classes=[
                        ClassMappingModelClassesConfig(class_name=c, class_id=i)
                        for i, c in enumerate(["class1", "class2", "class3"])
                    ],
                    mapping=[
                        ClassMappingMappingConfig(
                            input_class_name="class1", output_class_name="class4"
                        )
                    ],
                    number_classes=3,
                )
            ),
        )

        self.creator_base_htc: MMDetectionConfigCreator = MMDetectionConfigCreator(
            configuration=self.configuration_base_htc
        )

    def test_init_mmdetection_config_creator_base_htc(self) -> None:
        """
        Test if a htc MMDetectionConfig is correctly loaded with base parameters.
        """
        configuration: MMDetectionConfig = self.creator_base_htc.create_model_config(
            bucket_name=self.bucket_name, s3_prefixes=self.s3_prefixes
        )
        self.assertEqual(
            configuration.unique_name, self.configuration_base_htc.parameters.model_version
        )
        self.assertEqual(
            configuration.class_mapping, self.configuration_base_htc.dataset.class_mapping
        )
        self.assertEqual(
            configuration.class_mapping.model_classes,
            self.configuration_base_htc.dataset.class_mapping.model_classes,
        )
        self.assertEqual(
            configuration.class_mapping.number_model_classes,
            self.configuration_base_htc.dataset.class_mapping.number_model_classes,
        )
        self.assertEqual(
            configuration.class_mapping.number_classes,
            self.configuration_base_htc.dataset.class_mapping.number_classes,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["roi_head"]["bbox_head"][0][
                "num_classes"
            ],
            self.configuration_base_htc.dataset.class_mapping.number_classes,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["roi_head"]["bbox_head"][1][
                "num_classes"
            ],
            self.configuration_base_htc.dataset.class_mapping.number_classes,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["roi_head"]["bbox_head"][2][
                "num_classes"
            ],
            self.configuration_base_htc.dataset.class_mapping.number_classes,
        )
        self.assertEqual(
            configuration.class_mapping.mapping,
            self.configuration_base_htc.dataset.class_mapping.mapping,
        )
        self.assertEqual(
            configuration.inference_config.score_threshold,
            self.configuration_base_htc.parameters.score_threshold,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_cfg"]["max_epochs"],
            self.configuration_base_htc.parameters.epochs,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["param_scheduler"][1]["milestones"],
            [
                int(self.configuration_base_htc.parameters.epochs * 0.8),
                int(self.configuration_base_htc.parameters.epochs * 0.9),
            ],
        )
        self.assertEqual(
            configuration.mm_config.config_dict["max_epochs"],
            self.configuration_base_htc.parameters.epochs,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["default_hooks"]["checkpoint"]["filename_tmpl"],
            str(self.configuration_base_htc.parameters.model_version) + "_{:04d}.pth",
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["train_cfg"]["rpn_proposal"]["nms"][
                "iou_threshold"
            ],
            self.configuration_base_htc.parameters.nms_threshold,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["test_cfg"]["rpn"]["nms"][
                "iou_threshold"
            ],
            self.configuration_base_htc.parameters.nms_threshold,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["model"]["test_cfg"]["rcnn"]["nms"][
                "iou_threshold"
            ],
            self.configuration_base_htc.parameters.nms_threshold,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["train_dataloader"]["batch_size"],
            self.configuration_base_htc.parameters.batch_size,
        )
        self.assertEqual(
            configuration.mm_config.config_dict["load_from"],
            f"BASELINE_MODEL_DIR/{self.bucket_name}/{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
            f"/mmdetection_object_detection__htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco.pth",
        )
