# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the internal server
"""

import os
import unittest
from unittest.mock import MagicMock, patch

from flask import Flask
from flask_socketio import SocketIO
from gevent import monkey

from gts_backend.app import Server


class TestServer(unittest.TestCase):
    SERVER_LOGGER = "gts_backend.app"

    def setUp(self):
        self.flaskAppMock = MagicMock
        self.flaskAppMock.wsgi_app = MagicMock
        self.Flask = MagicMock
        self.CORS = MagicMock
        self.server = Server()
        monkey.patch_all = MagicMock

    def test_run(self):
        """
        Testing Server's run method
        """

        with patch.object(SocketIO, "run", returnValue=None) as siomock:
            with self.assertLogs(self.SERVER_LOGGER, level="DEBUG") as cm:
                self.server.run(self.flaskAppMock, siomock)
            self.assertIn(
                "INFO:gts_backend.app:Running socketio on port 5000 and all interfaces 0.0.0.0",
                cm.output,
            )
            self.assertIn(
                "DEBUG:gts_backend.app:Running socketio in debug mode: False",
                cm.output,
            )

    def test_debug_call_run(self):
        """
        Testing Server's run method with backend debug
        """
        self.server.backend_debug = True

        with patch.object(SocketIO, "run", returnValue=None) as siomock:
            with self.assertLogs(self.SERVER_LOGGER, level="DEBUG") as cm:
                self.server.run(self.flaskAppMock, siomock)
            self.assertIn(
                "INFO:gts_backend.app:Running socketio on port 5000 and all interfaces 0.0.0.0",
                cm.output,
            )
            self.assertIn(
                "DEBUG:gts_backend.app:Running socketio in debug mode: True",
                cm.output,
            )

    def test_start(self):
        """
        Testing Server's start method
        """
        self.server.create_app = MagicMock()
        self.server.add_socketio = MagicMock()
        self.server.run = MagicMock()

        # Mock os.getenv call
        with patch.object(os, "getenv", returnValue=False), patch.object(
            Server, "configure_extensions", returnValue=MagicMock
        ):
            with self.assertLogs(self.SERVER_LOGGER, level="DEBUG") as cm:
                self.server.start()
            self.assertIn("INFO:gts_backend.app:Starting Python backend", cm.output)
        # test if last statement is reached and called once
        self.server.run.assert_called_once()

    def test_create_app(self):
        """
        Testing Server's create app method
        """

        # Mock os.getenv call
        with patch.object(Flask, "register_blueprint", returnValue=MagicMock), patch.object(
            Server, "configure_extensions", returnValue=MagicMock
        ):
            app = self.server.create_app(config="test")
            self.assertEqual(
                app.__class__,
                Flask,
                "create_app result is not as expected a Flask instance.",
            )


if __name__ == "__main__":
    unittest.main()
