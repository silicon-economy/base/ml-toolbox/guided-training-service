# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Some tests for the non REST endpoints of the HTTP webserver
"""

import unittest
from unittest.mock import MagicMock, patch

from gts_backend.app import Server


class TestHTTPServer(unittest.TestCase):
    """
    Tests the basic behaviour of the HTTP Server
    """

    def setUp(self):
        self.server = Server()
        with patch.object(Server, "configure_extensions", returnValue=MagicMock):
            self.app = self.server.create_app(config="test")
        self.app.config["TESTING"] = True

    def test_app_creation(self):
        """Verifies that the created app is not None"""
        self.assertIsNotNone(self.app)

    def test_index_route(self):
        """Tests that the root/index route is reachable"""
        client = self.app.test_client()
        response = client.get("/")
        self.assertEqual(200, response.status_code)


if __name__ == "__main__":
    unittest.main()
