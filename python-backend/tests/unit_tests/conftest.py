# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Definition of common pytest fixtures for testing."""


import os
from pathlib import Path

import pytest


@pytest.fixture(name="project_root")
def project_root_fixture() -> Path:
    """Provide the project root path."""

    this_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve()

    setup_path = this_dir
    while setup_path.exists() and setup_path.name != "python-backend":
        if setup_path == setup_path.parent:
            raise RuntimeError("Could not find setup_path!")
        else:
            setup_path = setup_path.parent

    return setup_path
