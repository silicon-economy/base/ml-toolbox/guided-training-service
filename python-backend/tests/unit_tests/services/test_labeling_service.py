# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the /datasets REST Endpoints
"""
import datetime
import json
import os
import shutil
import tempfile
import unittest
from datetime import timedelta
from hashlib import md5
from io import BytesIO
from pathlib import Path
from unittest import mock
from zipfile import ZipFile

import cv2
from botocore.response import StreamingBody
from gevent import monkey
from label_studio_sdk import Client, Project
from mypy_boto3_s3 import S3Client
from mypy_boto3_sts import STSClient

from gts_backend.model.structs import DatasetTaskType
from gts_backend.services.labeling.service import LabelingService
from gts_backend.utils.exceptions import (
    TooManyAnnotationFilesException,
    WrongFormatException,
)

monkey.patch_all = mock.MagicMock


class TestLabelingServiceConfig(unittest.TestCase):
    """
    Tests the configuration part of the labeling service
    """

    def setUp(self):
        self.labeling_service = LabelingService(
            mock.MagicMock(spec=Client),
            mock.MagicMock(spec=S3Client),
            mock.MagicMock(spec=STSClient),
            "guided-training-service",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

    def test_creates_object_detection_labeling_config(self):
        """
        Tests the creation of a Object Detection Labeling Config for Label Studio
        It verifies that the interface is configured with all requested classes
        """
        labels = ["class1", "class2"]
        config = self.labeling_service.create_config(task_type="object_detection", labels=labels)

        for label in labels:
            self.assertTrue(f'<Label value="{label}"/>' in config)
        self.assertTrue("RectangleLabels" in config)

    def test_creates_classification_labeling_config(self):
        """
        Tests the creation of a Classification Labeling Config for Label Studio
        It verifies that the interface is configured with the requested class
        """
        labels = ["class1", "class2"]
        config = self.labeling_service.create_config(task_type="classification", labels=labels)

        for label in labels:
            self.assertTrue(f'<Choice value="{label}"/>' in config)
        self.assertTrue("Choices" in config)

    def test_create_tracking_labeling_config(self):
        """Test if  a tracking task labeling config for LabelStudio is created.
        It verifies that the interface is configured with the requested class.

        Note: The template contains a placeholder for $framerate, which has to be determined dynamically
        when the first video file is uploaded to the tracking task LabelStudio project.
        """
        labels = ["class1", "class2"]
        config = self.labeling_service.create_config(task_type="object_tracking", labels=labels)

        for label in labels:
            self.assertTrue(f'<Label value="{label}"/>' in config)
        self.assertTrue("VideoRectangle" in config)


class TestLabelingServiceHelperMethods(unittest.TestCase):
    """
    Tests private helper methods of the labling service
    """

    def setUp(self):
        self.ls_client_mock = mock.MagicMock(spec=Client)
        self.s3_client_mock = mock.MagicMock(spec=S3Client)
        self.sts_client_mock = mock.MagicMock(spec=STSClient)
        self.labeling_service = LabelingService(
            self.ls_client_mock,
            self.s3_client_mock,
            self.sts_client_mock,
            "testingbucketname",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )
        script_dir = Path(os.path.dirname(os.path.abspath(__file__)))
        self.video_file_path = str(script_dir.joinpath("../../test_data/data/test_video.mp4"))

    def test_assert_correctness_of_s3_path_succeeds(self):
        """
        Tests that the method executes without throwing an error
        """

        s3_prefix = "test_prefix"
        image_path = f"s3://{self.labeling_service.s3_bucket_name}/{s3_prefix}/data/test123.jpg"
        s3_file_key = image_path[len("s3://" + self.labeling_service.s3_bucket_name + "/") :]

        self.labeling_service._LabelingService__assert_correctness_of_s3_path(
            s3_path=image_path, expected_s3_prefix=s3_prefix, s3_file_key=s3_file_key
        )

    def test_assert_correctness_of_s3_path_fails_case_1(self):
        """
        Tests that the method raises an AssertionError because the image path does start with 's3://'
        """

        s3_prefix = "test_prefix"
        image_path = f"~/{self.labeling_service.s3_bucket_name}/{s3_prefix}/data/test123.jpg"
        s3_file_key = image_path[len("s3://" + self.labeling_service.s3_bucket_name + "/") :]

        with self.assertRaises(AssertionError):
            self.labeling_service._LabelingService__assert_correctness_of_s3_path(
                s3_path=image_path,
                expected_s3_prefix=s3_prefix,
                s3_file_key=s3_file_key,
            )

    def test_assert_correctness_of_s3_path_fails_case_2(self):
        """
        Tests that the method raises an AssertionError because the image path does not point to file
        inside the s3 bucket
        """

        s3_prefix = "test_prefix"
        image_path = f"s3://not_a_s3_bucket/{s3_prefix}/data/test123.jpg"
        s3_file_key = image_path[len("s3://" + self.labeling_service.s3_bucket_name + "/") :]

        with self.assertRaises(AssertionError):
            self.labeling_service._LabelingService__assert_correctness_of_s3_path(
                s3_path=image_path,
                expected_s3_prefix=s3_prefix,
                s3_file_key=s3_file_key,
            )

    def test_assert_correctness_of_s3_path_fails_case_3(self):
        """
        Tests that the method raises an AssertionError because the s3 file key did not start with
        the correct prefix
        """

        s3_prefix = "test_prefix"
        image_path = (
            f"s3://{self.labeling_service.s3_bucket_name}/wrong_test_prefix/data/test123.jpg"
        )
        s3_file_key = image_path[len("s3://" + self.labeling_service.s3_bucket_name + "/") :]

        with self.assertRaises(AssertionError):
            self.labeling_service._LabelingService__assert_correctness_of_s3_path(
                s3_path=image_path,
                expected_s3_prefix=s3_prefix,
                s3_file_key=s3_file_key,
            )

    def test_parse_annotations_from_label_studio_json_min(self):
        """
        Tests if result is parsed correctly if export type is json_min annotation format
        """

        dummy_content = {
            "width": 788,
            "height": 462,
            "id": 0,
            "file_name": "s3://test_bucket/test_s3_prefix/data/test123.jpg",
        }

        expected_result = {"images": dummy_content}

        def make_request_mock(method, url):  # pylint: disable=unused-argument,redefined-outer-name
            if (
                method == "GET"
                and url
                == "/api/projects/0/export?exportType=JSON_MIN&download_resources=true&download_all_tasks=false"
            ):
                return mock.Mock(
                    status_code=200,
                    content=json.dumps(dummy_content, indent=2).encode("utf-8"),
                )
            else:
                raise NotImplementedError()

        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.make_request.side_effect = make_request_mock

        result = self.labeling_service._LabelingService__parse_annotations_from_labelstudio(
            export_type="JSON_MIN",
            project=ls_project_mock,
            project_id=0,
        )

        self.assertEqual(expected_result, result)

    def test_parse_annotations_from_label_studio_coco(self):
        """
        Tests if result is parsed correctly if export type is coco annotation format
        """
        zip_path = (
            Path(os.path.dirname(os.path.abspath(__file__)))
            / "../../test_data/label_studio_export/coco.zip"
        )

        with open(zip_path, "rb") as zip_file:
            dummy_zip = zip_file.read()

        expected_result = {
            "images": [
                {
                    "width": 788,
                    "height": 462,
                    "id": 0,
                    "file_name": "s3://testingbucketname/testing_s3_prefix/data/pallet2.jpg",
                },
                {
                    "width": 688,
                    "height": 584,
                    "id": 1,
                    "file_name": "s3://testingbucketname/testing_s3_prefix/data/pallet1.jpg",
                },
            ],
            "categories": [{"id": 0, "name": "box"}, {"id": 1, "name": "circle"}],
            "annotations": [
                {
                    "id": 0,
                    "image_id": 0,
                    "category_id": 0,
                    "segmentation": [],
                    "bbox": [215, 26, 528, 384],
                    "ignore": 0,
                    "iscrowd": 0,
                    "area": 202752,
                },
                {
                    "id": 1,
                    "image_id": 1,
                    "category_id": 0,
                    "segmentation": [],
                    "bbox": [12, 368, 636, 73],
                    "ignore": 0,
                    "iscrowd": 0,
                    "area": 46428,
                },
                {
                    "id": 2,
                    "image_id": 1,
                    "category_id": 1,
                    "segmentation": [],
                    "bbox": [18, 181, 627, 73],
                    "ignore": 0,
                    "iscrowd": 0,
                    "area": 45771,
                },
            ],
            "info": {
                "year": 2022,
                "version": "1.0",
                "description": "",
                "contributor": "Label Studio",
                "url": "",
                "date_created": "2022-08-29 23:17:54.258162",
            },
        }

        def make_request_mock(method, url):  # pylint: disable=unused-argument,redefined-outer-name
            if (
                method == "GET"
                and url
                == "/api/projects/0/export?exportType=COCO&download_resources=true&download_all_tasks=false"
            ):
                return mock.Mock(status_code=200, content=dummy_zip)
            else:
                raise NotImplementedError()

        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.make_request.side_effect = make_request_mock

        result = self.labeling_service._LabelingService__parse_annotations_from_labelstudio(
            export_type="COCO",
            project=ls_project_mock,
            project_id=0,
        )

        self.assertEqual(expected_result, result)

    def test_parse_annotations_from_label_studio_json(self):
        """
        Tests if result is parsed correctly if export type is coco annotation format
        """
        zip_path = (
            Path(os.path.dirname(os.path.abspath(__file__)))
            / "../../test_data/label_studio_export/ls_json.zip"
        )

        expected_result = [
            {
                "id": 11843,
                "annotations": [
                    {
                        "id": 2872,
                        "completed_by": 1,
                        "result": [
                            {
                                "id": "b64797afff",
                                "type": "rectanglelabels",
                                "value": {
                                    "x": 1.744186046511628,
                                    "y": 63.013698630136986,
                                    "width": 92.44186046511628,
                                    "height": 12.5,
                                    "rotation": 0,
                                    "rectanglelabels": ["box"],
                                },
                                "to_name": "image",
                                "from_name": "label",
                                "image_rotation": 0,
                                "original_width": 688,
                                "original_height": 584,
                            },
                            {
                                "id": "02b69cd8bf",
                                "type": "rectanglelabels",
                                "value": {
                                    "x": 2.616279069767442,
                                    "y": 30.993150684931507,
                                    "width": 91.13372093023256,
                                    "height": 12.5,
                                    "rotation": 0,
                                    "rectanglelabels": ["circle"],
                                },
                                "to_name": "image",
                                "from_name": "label",
                                "image_rotation": 0,
                                "original_width": 688,
                                "original_height": 584,
                            },
                        ],
                        "was_cancelled": False,
                        "ground_truth": False,
                        "created_at": "2024-12-11T10:46:48.194828Z",
                        "updated_at": "2024-12-11T10:46:48.194859Z",
                        "draft_created_at": None,
                        "lead_time": None,
                        "prediction": {},
                        "result_count": 0,
                        "unique_id": "ec372bbf-d80e-426a-878c-7f35bbb0e305",
                        "import_id": None,
                        "last_action": None,
                        "task": 11843,
                        "project": 119,
                        "updated_by": 1,
                        "parent_prediction": None,
                        "parent_annotation": None,
                        "last_created_by": None,
                    }
                ],
                "drafts": [],
                "predictions": [],
                "data": {"image": "s3://test-bucket/pallet1.jpg"},
                "meta": {},
                "created_at": "2024-12-11T10:46:28.695380Z",
                "updated_at": "2024-12-11T10:46:48.404807Z",
                "inner_id": 1,
                "total_annotations": 1,
                "cancelled_annotations": 0,
                "total_predictions": 0,
                "comment_count": 0,
                "unresolved_comment_count": 0,
                "last_comment_updated_at": None,
                "project": 119,
                "updated_by": 1,
                "comment_authors": [],
            },
            {
                "id": 11844,
                "annotations": [
                    {
                        "id": 2871,
                        "completed_by": 1,
                        "result": [
                            {
                                "id": "b5afdd4a17",
                                "type": "rectanglelabels",
                                "value": {
                                    "x": 27.284263959390863,
                                    "y": 5.627705627705628,
                                    "width": 67.00507614213198,
                                    "height": 83.11688311688312,
                                    "rotation": 0,
                                    "rectanglelabels": ["box"],
                                },
                                "to_name": "image",
                                "from_name": "label",
                                "image_rotation": 0,
                                "original_width": 788,
                                "original_height": 462,
                            }
                        ],
                        "was_cancelled": False,
                        "ground_truth": False,
                        "created_at": "2024-12-11T10:46:47.928541Z",
                        "updated_at": "2024-12-11T10:46:47.928566Z",
                        "draft_created_at": None,
                        "lead_time": None,
                        "prediction": {},
                        "result_count": 0,
                        "unique_id": "fd660060-9633-4835-8a61-21e658424a20",
                        "import_id": None,
                        "last_action": None,
                        "task": 11844,
                        "project": 119,
                        "updated_by": 1,
                        "parent_prediction": None,
                        "parent_annotation": None,
                        "last_created_by": None,
                    }
                ],
                "drafts": [],
                "predictions": [],
                "data": {"image": "s3://test-bucket/pallet2.jpg"},
                "meta": {},
                "created_at": "2024-12-11T10:46:28.724063Z",
                "updated_at": "2024-12-11T10:46:48.085467Z",
                "inner_id": 2,
                "total_annotations": 1,
                "cancelled_annotations": 0,
                "total_predictions": 0,
                "comment_count": 0,
                "unresolved_comment_count": 0,
                "last_comment_updated_at": None,
                "project": 1,
                "updated_by": 1,
                "comment_authors": [],
            },
        ]

        def make_request_mock(method, url):  # pylint: disable=unused-argument,redefined-outer-name
            if (
                method == "GET"
                and url
                == "/api/projects/1/export?exportType=JSON&download_resources=true&download_all_tasks=false"
            ):
                return mock.Mock(
                    status_code=200, content=json.dumps(expected_result).encode("utf-8")
                )
            else:
                raise NotImplementedError()

        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.make_request.side_effect = make_request_mock

        result = self.labeling_service._LabelingService__parse_annotations_from_labelstudio(
            export_type="JSON",
            project=ls_project_mock,
            project_id=1,
        )

        self.assertEqual(expected_result, result)

    def test_replace_whitespaces(self):
        """Test if whitespaces in filename are replaced by underscore."""
        file_name_old = "file name.txt"
        file_name_new = "file_name.txt"

        self.assertEqual(
            self.labeling_service._LabelingService__replace_whitespaces(file_name_old),
            file_name_new,
        )

    def test_get_framerate(self):
        """Test if the framerate of a loaded VideoCapture object can be retrieved."""
        video: cv2.VideoCapture = cv2.VideoCapture(self.video_file_path)
        self.assertEqual(self.labeling_service._get_framerate(video), 2)

    @mock.patch("requests.get")
    def test_load_video(self, mock_requests_get):
        """Test if a video can be loaded from a presigned url."""
        with open(self.video_file_path, "rb") as f:
            video = f.read()
            hash_md5 = md5()
            hash_md5.update(video)
            self.s3_client_mock.get_object.return_value = {"ETag": hash_md5.hexdigest()}
            mock_requests_get.return_value.content = video
            self.assertIsInstance(
                self.labeling_service._load_video(
                    "http://localhost:1234/bucket-name/data/project/file.mp4"
                ),
                cv2.VideoCapture,
            )


class TestLabelingServiceProject(unittest.TestCase):
    """
    Tests the project related part of the labeling service
    """

    def setUp(self):
        self.ls_client_mock = mock.MagicMock(spec=Client)
        self.s3_client_mock = mock.MagicMock(spec=S3Client)
        self.sts_client_mock = mock.MagicMock(spec=STSClient)
        self.labeling_service = LabelingService(
            self.ls_client_mock,
            self.s3_client_mock,
            self.sts_client_mock,
            "testingbucketname",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )
        script_dir = Path(os.path.dirname(os.path.abspath(__file__)))
        self.video_file_path = str(script_dir.joinpath("../../test_data/data/test_video.mp4"))

    def test_initializes_project(self):
        """
        Tests the initilization of a project.
        It verifies that the correct S3 prefix is computed
        and all cloud storage integrations are configured.
        """

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.connect_s3_import_storage.return_value = {"id": 123}
        ls_project_mock.connect_s3_export_storage.return_value = {"id": 124}
        self.ls_client_mock.start_project.return_value = ls_project_mock

        # Setup STS Client
        self.sts_client_mock.assume_role.return_value = {
            "Credentials": {
                "AccessKeyId": "someId",
                "SecretAccessKey": "someKey",
                "SessionToken": "someToken",
            }
        }

        # Act
        with mock.patch("uuid.uuid4") as uuid4_mock, mock.patch(
            "gts_backend.services.labeling.service.LabelingService.validate_s3_import_settings"
        ) as validate_import_settings_mock, mock.patch(
            "gts_backend.services.labeling.service.LabelingService.validate_s3_export_settings"
        ) as validate_export_settings_mock:
            uuid4_mock.return_value = "aa2f38eb-712d-4be2-896f-ce212cc16e70"
            validate_import_settings_mock.return_value = True
            validate_export_settings_mock.return_value = True

            ls_project, s3_prefix = self.labeling_service.initialize_project(
                dataset_name="Testing Dataset",
                task_type=DatasetTaskType.OBJECT_DETECTION.lower(),
                labels=[],
            )

        # Assert
        self.assertEqual(ls_project.id, ls_project_mock.id)
        self.assertEqual(ls_project.title, ls_project_mock.title)
        self.assertEqual(s3_prefix, "project_1__aa2f38eb-712d-4be2-896f-ce212cc16e70")
        self.ls_client_mock.start_project.assert_called_once()

        # assert bucket initilaized
        self.s3_client_mock.put_object.assert_has_calls(
            [
                mock.call(Bucket="testingbucketname", Body="", Key=s3_prefix + "/"),
                mock.call(Bucket="testingbucketname", Body="", Key=s3_prefix + "/data/"),
                mock.call(Bucket="testingbucketname", Body="", Key=s3_prefix + "/tasks/"),
            ]
        )

        self.sts_client_mock.assume_role.assert_called_once()
        ls_project_mock.make_request.assert_not_called()

        ls_project_mock.connect_s3_import_storage.assert_called_once()
        ls_project_mock.connect_s3_export_storage.assert_called_once()
        ls_project_mock.sync_storage.assert_has_calls(
            [mock.call("s3", 123), mock.call("export/s3", 124)], any_order=True
        )

    def test_update_import_export_s3_settings_for_temporary_user_no_settings_configured(
        self,
    ):
        """Test if import/export settings for S3 storage are created for an already initialized project."""
        # setup LabelStudio Client
        ls_project_id: int = 1
        s3_prefix = "s3-prefix"

        # Setup STS Client
        self.sts_client_mock.assume_role.return_value = {
            "Credentials": {
                "AccessKeyId": "someId",
                "SecretAccessKey": "someKey",
                "SessionToken": "someToken",
            }
        }
        url_import = "/api/storages/s3"
        url_export = "/api/storages/export/s3"
        params = {"project": ls_project_id}

        def make_request_mock(method, url, **kwargs):
            if method == "GET":
                return mock.MagicMock(status_code=200, json=mock.Mock(return_value=[]))

        self.ls_client_mock.make_request.side_effect = make_request_mock

        # Act
        self.assertTrue(
            self.labeling_service.update_import_export_s3_settings_for_temporary_user(
                s3_prefix=s3_prefix, ls_project_id=ls_project_id
            )
        )
        self.ls_client_mock.make_request.assert_has_calls(
            [
                mock.call(method="GET", url=url_import, params=params),
                mock.call(method="GET", url=url_export, params=params),
            ],
        )

    def test_update_import_export_s3_settings_for_temporary_user_overwrite_existing_settings(
        self,
    ):
        """Test if import/export settings for S3 storage are overwritten for an already initialized project."""
        # Setup Labelstudio Client
        ls_project_id = 1
        id_storage = 1
        s3_prefix = "s3-prefix"
        self.ls_client_mock.get_project = mock.MagicMock(Project)

        # Setup STS Client
        self.sts_client_mock.assume_role.return_value = {
            "Credentials": {
                "AccessKeyId": "someId",
                "SecretAccessKey": "someKey",
                "SessionToken": "someToken",
            }
        }
        url_import = "/api/storages/s3"
        url_export = "/api/storages/export/s3"
        params = {"project": ls_project_id}

        def make_request_mock(method, url, **kwargs):
            if method == "GET":
                return mock.MagicMock(
                    status_code=200,
                    json=mock.Mock(
                        return_value=[
                            {
                                "id": id_storage,
                                "type": "s3",
                                "presign": False,
                                "title": "S3 data storage",
                                "description": "A custom S3 raw data storage shared by the services.",
                                "created_at": "2023-09-26T11:30:59.003271Z",
                                "last_sync": "2023-09-26T11:30:59.611632Z",
                                "last_sync_count": 1,
                                "last_sync_job": None,
                                "bucket": "bucket-name",
                                "prefix": "project_1__6e871d64-ee3c-42df-9274-ecc3058dc2ec/data",
                                "regex_filter": None,
                                "use_blob_urls": True,
                                "aws_session_token": "123456",
                                "region_name": "eu-central-1",
                                "s3_endpoint": "http://localhost:9005",
                                "presign_ttl": 1,
                                "recursive_scan": False,
                                "project": ls_project_id,
                            }
                        ]
                    ),
                )
            elif method == "PATCH":
                return mock.MagicMock(status_code=200)

        self.ls_client_mock.make_request.side_effect = make_request_mock

        # Act
        self.assertTrue(
            self.labeling_service.update_import_export_s3_settings_for_temporary_user(
                s3_prefix=s3_prefix, ls_project_id=ls_project_id
            )
        )
        self.ls_client_mock.make_request.assert_has_calls(
            [
                mock.call(method="GET", url=url_import, params=params),
                mock.call(method="GET", url=url_export, params=params),
                mock.call(
                    method="PATCH",
                    url=url_import + "/" + str(id_storage),
                    json={
                        "aws_access_key_id": "someId",
                        "aws_secret_access_key": "someKey",
                        "aws_session_token": "someToken",
                    },
                ),
                mock.call(
                    method="PATCH",
                    url=url_export + "/" + str(id_storage),
                    json={
                        "aws_access_key_id": "someId",
                        "aws_secret_access_key": "someKey",
                        "aws_session_token": "someToken",
                    },
                ),
            ],
        )

    def test_update_import_export_s3_settings_for_temporary_user_exception(
        self,
    ):
        """Test if False is returned when an Exception occurs during the method call."""
        # Setup Labelstudio Client
        ls_project_id = 1
        id_storage = 1
        s3_prefix = "s3-prefix"
        self.ls_client_mock.get_project = mock.MagicMock(Project)

        # Setup STS Client
        self.sts_client_mock.assume_role.return_value = {
            "Credentials": {
                "AccessKeyId": "someId",
                "SecretAccessKey": "someKey",
                "SessionToken": "someToken",
            }
        }
        url_import = "/api/storages/s3"
        url_export = "/api/storages/export/s3"
        params = {"project": ls_project_id}

        def make_request_mock(method, url, **kwargs):
            if method == "GET":
                return mock.MagicMock(
                    status_code=200,
                    json=mock.Mock(
                        return_value=[
                            {
                                "id": id_storage,
                                "type": "s3",
                                "presign": False,
                                "title": "S3 data storage",
                                "description": "A custom S3 raw data storage shared by the services.",
                                "created_at": "2023-09-26T11:30:59.003271Z",
                                "last_sync": "2023-09-26T11:30:59.611632Z",
                                "last_sync_count": 1,
                                "last_sync_job": None,
                                "bucket": "bucket-name",
                                "prefix": "project_1__6e871d64-ee3c-42df-9274-ecc3058dc2ec/data",
                                "regex_filter": None,
                                "use_blob_urls": True,
                                "aws_session_token": "123456",
                                "region_name": "eu-central-1",
                                "s3_endpoint": "http://localhost:9005",
                                "presign_ttl": 1,
                                "recursive_scan": False,
                                "project": ls_project_id,
                            }
                        ]
                    ),
                )
            elif method == "PATCH":
                return mock.MagicMock(status_code=500)

        self.ls_client_mock.make_request.side_effect = make_request_mock

        # Act
        self.assertFalse(
            self.labeling_service.update_import_export_s3_settings_for_temporary_user(
                s3_prefix=s3_prefix, ls_project_id=ls_project_id
            )
        )
        self.ls_client_mock.make_request.assert_has_calls(
            [
                mock.call(method="GET", url=url_import, params=params),
                mock.call(method="GET", url=url_export, params=params),
                mock.call(
                    method="PATCH",
                    url=url_import + "/" + str(id_storage),
                    json={
                        "aws_access_key_id": "someId",
                        "aws_secret_access_key": "someKey",
                        "aws_session_token": "someToken",
                    },
                ),
                mock.call(
                    method="PATCH",
                    url=url_export + "/" + str(id_storage),
                    json={
                        "aws_access_key_id": "someId",
                        "aws_secret_access_key": "someKey",
                        "aws_session_token": "someToken",
                    },
                ),
            ],
        )

    def test_validate_import_settings_calls_endpoint(self):
        """
        Verifies that the correct validation endpoint is called
        for the valtidation of Cloud Storage import settings
        """

        def make_request_mock(
            method, endpoint, json
        ):  # pylint: disable=unused-argument,redefined-outer-name
            if method == "POST" and endpoint == "/api/storages/s3/validate":
                return mock.Mock(status_code=200)
            else:
                raise NotImplementedError()

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.make_request.side_effect = make_request_mock

        # Act
        self.labeling_service.validate_s3_import_settings(ls_project_mock, somekeyword="arg")

        # Assert
        ls_project_mock.make_request.assert_called_once_with(
            "POST",
            "/api/storages/s3/validate",
            json={"somekeyword": "arg", "project": 1, "presign_ttl": 1},
        )

    def test_validate_export_settings_calls_endpoint(self):
        """
        Verifies that the correct validation endpoint is called
        for the valtidation of Cloud Storage export settings
        """

        def make_request_mock(
            method, endpoint, json
        ):  # pylint: disable=unused-argument,redefined-outer-name
            if method == "POST" and endpoint == "/api/storages/export/s3/validate":
                return mock.Mock(status_code=200)
            else:
                raise NotImplementedError()

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.make_request.side_effect = make_request_mock

        # Act
        self.labeling_service.validate_s3_export_settings(ls_project_mock, somekeyword="arg")

        # Assert
        ls_project_mock.make_request.assert_called_once_with(
            "POST",
            "/api/storages/export/s3/validate",
            json={"somekeyword": "arg", "project": 1},
        )

    def test__extract_all_images_from_zip(self):
        """
        Verifies that all images from the uploaded zips are extracted
        and uploaded to the s3 bucket.
        """
        # test data
        s3_prefix = "test_project"
        zip_key = f"{s3_prefix}/data/upload_images.zip"
        files = {
            "Contents": [
                {"Key": zip_key},
            ],
        }

        script_dir = Path(os.path.dirname(os.path.abspath(__file__)))

        def download_file_from_s3_mock(
            Bucket, Key, Filename
        ):  # pylint: disable=unused-argument,invalid-name
            """
            Helper method to mock downloading of a file from s3
            to the local file system.
            We simply copy the file from our test data directory
            to the desired download location.
            """
            file_dir = script_dir.joinpath("../../test_data/data/")

            if "upload_images.zip" in Key:
                self.assertEqual(Key, zip_key)
                file_path = os.path.join(file_dir, "upload_images.zip")
            else:
                raise NotImplementedError()

            shutil.copy(file_path, Path(Filename))

        uploaded_files = []

        def upload_file_to_s3_mock(file, Bucket, Key):  # pylint: disable=invalid-name
            filename = str(file.name).split("/")[-1]
            uploaded_files.append((filename, Bucket, Key))

        # configure the mock S3 client
        self.s3_client_mock.list_objects_v2.return_value = files
        self.s3_client_mock.download_file.side_effect = download_file_from_s3_mock
        self.s3_client_mock.upload_fileobj.side_effect = upload_file_to_s3_mock

        # act
        self.labeling_service.extract_all_images_from_zips(s3_prefix)

        # assert
        self.s3_client_mock.list_objects_v2.assert_called_once()
        self.s3_client_mock.download_file.assert_called_once()

        # Assert that all image files from the zip were uploaded
        expected_upload = [
            ("pallet1.jpg", "testingbucketname", f"{s3_prefix}/data/pallet1.jpg"),
            ("pallet2.jpg", "testingbucketname", f"{s3_prefix}/data/pallet2.jpg"),
        ]
        self.assertEqual(set(uploaded_files), set(expected_upload))

    def test_sync_all_s3_cloud_storages(self):
        """
        Verifies that all cloud storages of type 's3' get synced.
        Tests cover both import and export storage types.
        """

        def json_func():
            return [
                {
                    "id": 1,
                    "type": "s3",
                },
                {
                    "id": 2,
                    "type": "s3",
                },
                {
                    "id": 3,
                    "type": "gcp",  # only s3 should be synced
                },
            ]

        def sync_storage_mock(storage_type: str, storage_id: int):
            if "export" not in storage_type:  # return count only for import
                return {"last_sync_count": 10 * storage_id}
            return {}

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.make_request.return_value = mock.Mock(status_code=200, json=json_func)
        ls_project_mock.sync_storage.side_effect = sync_storage_mock

        self.ls_client_mock.get_project.return_value = ls_project_mock

        # Act
        total_imported_task_count = self.labeling_service.sync_all_s3_cloud_storages(1)

        # Assert
        self.ls_client_mock.get_project.assert_called_once_with(1)
        ls_project_mock.make_request.assert_has_calls(
            [
                mock.call("GET", "/api/storages/s3", params={"project": 1}),
                mock.call("GET", "/api/storages/export/s3", params={"project": 1}),
            ],
            any_order=True,
        )
        ls_project_mock.sync_storage.assert_has_calls(
            [
                mock.call("s3", 1),
                mock.call("s3", 2),
                mock.call("export/s3", 1),
                mock.call("export/s3", 2),
            ],
            any_order=True,
        )
        self.assertEqual(total_imported_task_count, 30)

    def test_delete_files_from_s3_folder(self):
        """
        Verifies that all files are deleted from an S3 folder/directory
        """
        # Setup S3 client
        paginate_mock = mock.Mock()
        paginate_mock.search.return_value = [
            {"Key": "testingObjKey1"},
            {"Key": "testingObjKey2"},
        ]

        paginator_mock = mock.Mock()
        paginator_mock.paginate.return_value = paginate_mock

        self.s3_client_mock.get_paginator.return_value = paginator_mock

        # Act
        self.labeling_service.delete_files_from_s3_folder(
            s3_prefix="testingprefix", folder="somefolder"
        )

        # Assert
        paginator_mock.paginate.assert_called_once_with(
            Bucket="testingbucketname", Prefix="testingprefix/somefolder"
        )
        self.s3_client_mock.delete_objects.assert_called_once_with(
            Bucket="testingbucketname",
            Delete={"Objects": [{"Key": "testingObjKey1"}, {"Key": "testingObjKey2"}]},
        )

    def test_delete_project(self):
        """
        Verifies that a project is deleted with all files related to that project.
        """
        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        self.ls_client_mock.get_project.return_value = ls_project_mock

        # Setup S3 client
        self.s3_client_mock.list_objects.return_value = {
            "Contents": [
                {"Key": "testingObjKey1"},
                {"Key": "testingObjKey2"},
            ]
        }

        # Act
        with mock.patch(
            "gts_backend.services.labeling.service.LabelingService.delete_files_from_s3_folder"
        ) as delete_files_from_s3_folder_mock:
            self.labeling_service.delete_project(project_id=1, s3_prefix="testingprefix")

            # Assert folders were deleted
            delete_files_from_s3_folder_mock.assert_has_calls(
                [
                    mock.call(s3_prefix="testingprefix", folder="data"),
                    mock.call(s3_prefix="testingprefix", folder="tasks"),
                    mock.call(s3_prefix="testingprefix", folder="annotated"),
                ],
                any_order=True,
            )

        self.ls_client_mock.get_project.assert_called_once_with(1)
        self.s3_client_mock.list_objects.assert_called_once_with(
            Bucket="testingbucketname", Prefix="testingprefix"
        )
        self.s3_client_mock.delete_objects.assert_called_once_with(
            Bucket="testingbucketname",
            Delete={"Objects": [{"Key": "testingObjKey1"}, {"Key": "testingObjKey2"}]},
        )
        ls_project_mock.make_request.assert_called_once_with("DELETE", "/api/projects/1/")

    def test_check_annotation_file_for_errors(self):
        """Test that the latest uploaded annotation file is used."""
        # Setup s3 client
        self.s3_client_mock.list_objects_v2.return_value = {
            "Contents": [
                {
                    "Key": "testingprefix/coco_json.json",
                    "LastModified": datetime.datetime(1999, 1, 1),
                },
                {
                    "Key": "testingprefix/ls_json.json",
                    "LastModified": datetime.datetime(2022, 1, 1),
                },
            ]
        }

        # Act
        (
            annotation_file,
            annotation_format,
        ) = self.labeling_service._check_annotation_file_for_errors(s3_prefix="testingprefix")

        self.s3_client_mock.list_objects_v2.assert_called()
        self.assertTrue(annotation_format, "ls_json")
        self.assertTrue(annotation_file, "ls_json.json")

    def test_import_ls_annotation_wrong_format(self):
        """
        Verifies that a wrong annotation format is not allowed
        """
        # Setup s3 client
        self.s3_client_mock.list_objects_v2.return_value = {
            "Contents": [
                {"Key": "testingprefix/other.json"},
            ]
        }

        # Act
        with self.assertRaises(WrongFormatException) as ctx:
            self.labeling_service.import_ls_annotation(project_id=1, s3_prefix="testingprefix")

        self.s3_client_mock.list_objects_v2.assert_called()
        self.assertTrue("Wrong annotation format was provided." in str(ctx.exception))

    @mock.patch(
        "gts_backend.services.labeling.service.LabelingService.make_external_presigned_url"
    )
    def test_generate_s3_presigned_url_upload_valid(self, mock_make_external_presigned_url):
        """
        Test the generate_s3_presigned_url_upload method with valid input including a space in the filename.
        Check that the returned value is as expected and that the generate_presigned_url_upload
        and make_external_presigned_url are called with the correct parameters.
        """
        # Test valid input
        mock_make_external_presigned_url.return_value = "https://test_url.com"
        s3_prefix = "test-prefix"
        s3_folder = "test-folder"
        file_name = "test file.txt"
        expected_url = "https://test_url.com"
        expected_presigned_url = "https://presigned_test_url.com"
        self.s3_client_mock.generate_presigned_url.return_value = expected_presigned_url

        result = self.labeling_service.generate_s3_presigned_url_upload(
            s3_prefix, s3_folder, file_name
        )
        mock_make_external_presigned_url.assert_called_with(
            self.s3_client_mock.generate_presigned_url.return_value
        )
        self.s3_client_mock.generate_presigned_url.assert_called_with(
            ClientMethod="put_object",
            Params={
                "Bucket": "testingbucketname",
                "Key": f"{s3_prefix}/{s3_folder}/{file_name.replace(' ', '_')}",
            },
            ExpiresIn=int(timedelta(minutes=5).total_seconds()),
        )
        self.assertEqual(result, expected_url)
        mock_make_external_presigned_url.assert_called_with(expected_presigned_url)

    def test_generate_s3_presigned_url_upload_empty_args(self):
        """
        Test the generate_s3_presigned_url_upload method with invalid input including a space in the filename.
        A ValueError should be returned.
        """
        with self.assertRaises(ValueError):
            kwargs_tup = (
                dict(s3_prefix="", s3_folder="some_folder", file_name="file_name.txt"),
                dict(s3_prefix="s3_prefix", s3_folder="", file_name="file_name.txt"),
            )

            for kwargs in kwargs_tup:
                with self.subTest(**kwargs):
                    self.labeling_service.generate_s3_presigned_url_upload(**kwargs)

    @mock.patch(
        "gts_backend.services.labeling.service.LabelingService.make_external_presigned_url"
    )
    def test_generate_s3_presigned_url_get_object_valid(self, mock_make_external_presigned_url):
        """
        Test the generate_s3_presigned_url method with valid input including a space in the filename.
        Check that the returned value is as expected and that the generate_presigned_url_get_object
        and make_external_presigned_url are called with the correct parameters.
        """
        # Test valid input
        mock_make_external_presigned_url.return_value = "https://test_url.com"
        s3_prefix = "test-prefix"
        s3_folder = "test-folder"
        file_name = "test file.txt"
        expected_url = "https://test_url.com"
        expected_presigned_url = "https://presigned_test_url.com"
        self.s3_client_mock.generate_presigned_url.return_value = expected_presigned_url

        result = self.labeling_service.generate_s3_presigned_url_get_object(
            s3_prefix, s3_folder, file_name
        )
        mock_make_external_presigned_url.assert_called_with(
            self.s3_client_mock.generate_presigned_url.return_value
        )
        self.s3_client_mock.generate_presigned_url.assert_called_with(
            ClientMethod="get_object",
            Params={
                "Bucket": "testingbucketname",
                "Key": f"{s3_prefix}/{s3_folder}/{file_name.replace(' ', '_')}",
            },
            ExpiresIn=int(timedelta(minutes=5).total_seconds()),
        )
        self.assertEqual(result, expected_url)
        mock_make_external_presigned_url.assert_called_with(expected_presigned_url)

    def test_generate_s3_presigned_url_get_object_empty_args(self):
        """
        Test the generate_s3_presigned_url_upload method with invalid input including a space in the filename.
        A ValueError should be returned.
        """
        with self.assertRaises(ValueError):
            kwargs_tup = (
                dict(s3_prefix="", s3_folder="some_folder", file_name="file_name.txt"),
                dict(s3_prefix="s3_prefix", s3_folder="", file_name="file_name.txt"),
            )

            for kwargs in kwargs_tup:
                with self.subTest(**kwargs):
                    self.labeling_service.generate_s3_presigned_url_get_object(**kwargs)

    @mock.patch("label_studio_sdk.Project")
    @mock.patch("requests.get")
    def test_set_framerate_labelstudio_project_template(self, request_get_mock, ls_project_mock):
        """Test if the framerate can be dynamically set via video file's actual framerate in LabelStudio project
        template.
        """
        # setup
        label_config_old: str = self.labeling_service.create_config(
            task_type="object_tracking", labels=["Test1", "Test2"]
        )
        label_config_new: str = label_config_old.replace("$framerate", "2.0")
        ls_project_id: int = 1
        ls_project_mock.get_params.return_value = {
            "id": ls_project_id,
            "label_config": label_config_old,
        }
        self.ls_client_mock.get_project.return_value = ls_project_mock

        with open(self.video_file_path, "rb") as f:
            video = f.read()
            hash_md5 = md5()
            hash_md5.update(video)
            self.s3_client_mock.get_object.return_value = {"ETag": hash_md5.hexdigest()}
            request_get_mock.return_value.content = video
            ls_project_mock.get_params.return_value["label_config"] = label_config_new
            presigned_url: str = "http://localhost:1234:/bucket-name/data/project/video.mp4"
            self.labeling_service.set_framerate_labelstudio_project_template(
                project_id=ls_project_id, presigned_url_video_file=presigned_url
            )
            ls_project_mock.get_params.return_value["label_config"] = label_config_new
            # assert calls
            self.ls_client_mock.get_project.assert_called_once_with(id=ls_project_id)
            request_get_mock.assert_called_once_with(presigned_url)
            ls_project_mock.get_params.assert_called_once()
            ls_project_mock.make_request.assert_called_with(
                method="PATCH",
                url=f"/api/projects/{ls_project_id}/",
                params={"id": ls_project_id},
                data={"label_config": label_config_new},
            )

    def test_import_ls_annotation_too_many_tasks(self):
        """
        Verifies that tasks that do not have corresponding images get removed
        """

        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "label_config": "<View>"
                '<Header value="Select the label and fit a bounding box around every'
                ' matching object. Leave a very tiny margin around the actual object."/>'
                '<Image name="image" value="$image" zoom="true" zoomControl="true"/>'
                '<RectangleLabels name="label" toName="image">'
                '<Label value="testvalue1"/><Label value="testvalue2"/>'
                "</RectangleLabels>"
                "</View>",
            }

        # Setup s3 client
        self.s3_client_mock.list_objects_v2.side_effect = [
            {"Contents": [{"Key": "testingprefix/ls_json.json"}]},
            {
                "Contents": [
                    {"Key": "testingprefix/pallet1.jpg"},
                    {"Key": "testingprefix/"},
                ]
            },
        ]

        annotation_content = [
            {
                "data": {"image": "somepath/pallet1.jpg"},
                "annotations": [{"result": [{"id": 1, "value": "testvalue1"}]}],
            },
            {
                "data": {"image": "somepath/pallet2.jpg"},
                "annotations": [{"result": [{"id": 2, "value": "testvalue2"}]}],
            },
        ]

        annotation_content_encoded = json.dumps(annotation_content).encode("utf-8")
        body = StreamingBody(BytesIO(annotation_content_encoded), len(annotation_content_encoded))

        self.s3_client_mock.get_object.return_value = {"Body": body}

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.get_params.return_value = json_func()
        ls_project_mock.make_request.return_value.json.return_value = [
            {"id": 0, "data": {"image": "testingprefix/pallet1.jpg"}, "annotations": [{"id": 1}]}
        ]
        self.ls_client_mock.get_project.return_value = ls_project_mock

        # Act
        num_imported_annotations = self.labeling_service.import_ls_annotation(
            project_id=1, s3_prefix="testingprefix"
        )

        self.s3_client_mock.list_objects_v2.assert_called()
        self.ls_client_mock.get_project.assert_called_with(1)
        self.s3_client_mock.get_object.assert_called()
        self.assertEqual(num_imported_annotations, 1)

        # check make_request GET call
        expected_get_request = ("GET", "/api/projects/1/tasks?page_size=-1")
        self.assertEqual(expected_get_request, ls_project_mock.make_request.call_args_list[0].args)

        # check first make_request POST call
        expected_post_request_args_1 = ("POST", "/api/tasks/0/annotations")
        expected_post_request_kwargs_1 = {
            "json": {
                "result": [{"id": 1, "value": "testvalue1"}],
                "was_cancelled": False,
                "ground_truth": False,
                "lead_time": None,
                "task": 0,
                "parent_prediction": None,
                "parent_annotation": None,
            }
        }
        actual_post_request_kwargs_1 = ls_project_mock.make_request.call_args_list[1].kwargs
        self.assertEqual(expected_post_request_kwargs_1, actual_post_request_kwargs_1)

        # check call count
        self.assertEqual(ls_project_mock.make_request.call_count, 2)

    def test_import_ls_annotation_ls_format(self):
        """
        Verifies the successful import of annotations in the ls format
        """

        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "label_config": "<View>"
                '<Header value="Select the label and fit a bounding box around every'
                ' matching object. Leave a very tiny margin around the actual object."/>'
                '<Image name="image" value="$image" zoom="true" zoomControl="true"/>'
                '<RectangleLabels name="label" toName="image">'
                '<Label value="testvalue1"/><Label value="testvalue2"/>'
                "</RectangleLabels>"
                "</View>",
            }

        # Setup s3 client
        self.s3_client_mock.list_objects_v2.side_effect = [
            {"Contents": [{"Key": "testingprefix/ls_json.json"}]},
            {
                "Contents": [
                    {"Key": "testingprefix/pallet1.jpg"},
                    {"Key": "testingprefix/pallet_2.jpg"},
                    {"Key": "testingprefix/pallet_invalid.jpg"},
                    {"Key": "testingprefix/"},
                ]
            },
        ]

        annotation_content = [
            {
                "data": {"image": "somepath/pallet1.jpg"},
                "annotations": [{"result": [{"id": 1, "value": "testvalue1"}]}],
            },
            {
                "data": {"image": "somepath/pallet_2.jpg"},
                "annotations": [{"result": [{"id": 2, "value": "testvalue2"}]}],
            },
            {
                "data": {"image": "somepath/pallet_invalid.jpg"},
                "annotations": [{"result": [{"id": 2, "value": "testvalue_invalid"}]}],
            },
        ]

        annotation_content_encoded = json.dumps(annotation_content).encode("utf-8")
        body = StreamingBody(BytesIO(annotation_content_encoded), len(annotation_content_encoded))

        self.s3_client_mock.get_object.return_value = {"Body": body}

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.get_params.return_value = json_func()
        ls_project_mock.make_request.return_value.json.return_value = [
            {
                "id": 0,
                "data": {"image": "testingprefix/pallet1.jpg"},
                "annotations": [{"id": 1, "value": "testvalue1"}],
            },
            {
                "id": 1,
                "data": {"image": "testingprefix/pallet_2.jpg"},
                "annotations": [{"id": 2, "value": "testvalue2"}],
            },
        ]
        self.ls_client_mock.get_project.return_value = ls_project_mock

        num_imported_annotations = self.labeling_service.import_ls_annotation(
            project_id=1, s3_prefix="testingprefix"
        )

        self.s3_client_mock.list_objects_v2.assert_called()
        self.ls_client_mock.get_project.assert_called_with(1)
        self.s3_client_mock.get_object.assert_called()
        self.assertEqual(num_imported_annotations, 2)

        # check make_request GET call
        expected_get_request = ("GET", "/api/projects/1/tasks?page_size=-1")
        self.assertEqual(expected_get_request, ls_project_mock.make_request.call_args_list[0].args)

        # check first make_request POST call
        expected_post_request_args_1 = ("POST", "/api/tasks/0/annotations")
        expected_post_request_kwargs_1 = {
            "json": {
                "result": [{"id": 1, "value": "testvalue1"}],
                "was_cancelled": False,
                "ground_truth": False,
                "lead_time": None,
                "task": 0,
                "parent_prediction": None,
                "parent_annotation": None,
            }
        }
        actual_post_request_kwargs_1 = ls_project_mock.make_request.call_args_list[1].kwargs
        # delete key because it is randomly generated
        self.assertEqual(
            expected_post_request_args_1, ls_project_mock.make_request.call_args_list[1].args
        )
        self.assertEqual(expected_post_request_kwargs_1, actual_post_request_kwargs_1)

        # check second make_request POST call
        expected_post_request_args_2 = ("POST", "/api/tasks/1/annotations")
        expected_post_request_kwargs_2 = {
            "json": {
                "result": [{"id": 2, "value": "testvalue2"}],
                "was_cancelled": False,
                "ground_truth": False,
                "lead_time": None,
                "task": 1,
                "parent_prediction": None,
                "parent_annotation": None,
            }
        }

        actual_post_request_kwargs_2 = ls_project_mock.make_request.call_args_list[2].kwargs
        # delete key because it is randomly generated
        self.assertEqual(
            expected_post_request_args_2, ls_project_mock.make_request.call_args_list[2].args
        )
        self.assertEqual(expected_post_request_kwargs_2, actual_post_request_kwargs_2)

        # check call count
        self.assertEqual(ls_project_mock.make_request.call_count, 3)

    def test_import_ls_annotation_coco_format(self):
        """
        Verifies the successful import of annotations in the coco format
        """

        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "label_config": "<View>"
                '<Header value="Select the label and fit a bounding box around every'
                ' matching object. Leave a very tiny margin around the actual object."/>'
                '<Image name="image" value="$image" zoom="true" zoomControl="true"/>'
                '<RectangleLabels name="label" toName="image">'
                '<Label value="pallet"/><Label value="human"/>'
                "</RectangleLabels>"
                "</View>",
            }

        # Setup s3 client
        self.s3_client_mock.list_objects_v2.side_effect = [
            {"Contents": [{"Key": "testingprefix/coco_json.json"}]},
            {
                "Contents": [
                    {"Key": "testingprefix/pallet1.jpg"},
                    {"Key": "testingprefix/pallet_2.jpg"},
                    {"Key": "testingprefix/pallet_invalid.jpg"},
                    {"Key": "testingprefix/"},
                ]
            },
        ]

        annotation_content = {
            "images": [
                {
                    "width": 1920,
                    "height": 1080,
                    "id": 0,
                    "file_name": "somepath/pallet1.jpg",
                },
                {
                    "width": 1920,
                    "height": 1080,
                    "id": 1,
                    "file_name": "somepath/pallet_2.jpg",
                },
                {
                    "width": 1920,
                    "height": 1080,
                    "id": 2,
                    "file_name": "somepath/pallet_invalid.jpg",
                },
            ],
            "categories": [
                {"id": 0, "name": "pallet"},
                {"id": 1, "name": "human"},
            ],
            "annotations": [
                {
                    "id": 0,
                    "image_id": 0,
                    "category_id": 0,
                    "bbox": [0, 0, 0, 0],
                    "segmentation": [],
                },
                {
                    "id": 1,
                    "image_id": 1,
                    "category_id": 0,
                    "bbox": [0, 0, 0, 0],
                },
                {
                    "id": 2,
                    "image_id": 0,
                    "category_id": 1,
                    "bbox": [0, 0, 0, 0],
                },
            ],
        }

        annotation_content_encoded = json.dumps(annotation_content).encode("utf-8")
        body = StreamingBody(BytesIO(annotation_content_encoded), len(annotation_content_encoded))

        self.s3_client_mock.get_object.return_value = {"Body": body}

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.get_params.return_value = json_func()
        ls_project_mock.make_request.return_value.json.return_value = [
            {"id": 0, "data": {"image": "testingprefix/pallet1.jpg"}, "annotations": [{"id": 1}]},
            {"id": 1, "data": {"image": "testingprefix/pallet_2.jpg"}, "annotations": [{"id": 2}]},
        ]
        self.ls_client_mock.get_project.return_value = ls_project_mock

        num_imported_annotations = self.labeling_service.import_ls_annotation(
            project_id=1, s3_prefix="testingprefix"
        )

        self.s3_client_mock.list_objects_v2.assert_called()
        self.ls_client_mock.get_project.assert_called_with(1)
        self.s3_client_mock.get_object.assert_called()
        self.assertEqual(num_imported_annotations, 2)

        # check make_request GET call
        expected_get_request = ("GET", "/api/projects/1/tasks?page_size=-1")
        self.assertEqual(expected_get_request, ls_project_mock.make_request.call_args_list[0].args)

        # check first make_request POST call
        expected_post_request_args_1 = ("POST", "/api/tasks/0/annotations")
        expected_post_request_kwargs_1 = {
            "json": {
                "result": [
                    {
                        "type": "rectanglelabels",
                        "value": {
                            "x": 0.0,
                            "y": 0.0,
                            "width": 0.0,
                            "height": 0.0,
                            "rotation": 0,
                            "rectanglelabels": ["pallet"],
                        },
                        "to_name": "image",
                        "from_name": "label",
                        "image_rotation": 0,
                        "original_width": 1920,
                        "original_height": 1080,
                    },
                    {
                        "type": "rectanglelabels",
                        "value": {
                            "x": 0.0,
                            "y": 0.0,
                            "width": 0.0,
                            "height": 0.0,
                            "rotation": 0,
                            "rectanglelabels": ["human"],
                        },
                        "to_name": "image",
                        "from_name": "label",
                        "image_rotation": 0,
                        "original_width": 1920,
                        "original_height": 1080,
                    },
                ],
                "was_cancelled": False,
                "ground_truth": False,
                "lead_time": None,
                "task": 0,
                "parent_prediction": None,
                "parent_annotation": None,
            }
        }
        actual_post_request_kwargs_1 = ls_project_mock.make_request.call_args_list[1].kwargs
        # delete key because it is randomly generated
        actual_post_request_kwargs_1["json"]["result"][0].pop("id")
        actual_post_request_kwargs_1["json"]["result"][1].pop("id")
        self.assertEqual(
            expected_post_request_args_1, ls_project_mock.make_request.call_args_list[1].args
        )
        self.assertEqual(expected_post_request_kwargs_1, actual_post_request_kwargs_1)

        # check second make_request POST call
        expected_post_request_args_2 = ("POST", "/api/tasks/1/annotations")
        expected_post_request_kwargs_2 = {
            "json": {
                "result": [
                    {
                        "type": "rectanglelabels",
                        "value": {
                            "x": 0.0,
                            "y": 0.0,
                            "width": 0.0,
                            "height": 0.0,
                            "rotation": 0,
                            "rectanglelabels": ["pallet"],
                        },
                        "to_name": "image",
                        "from_name": "label",
                        "image_rotation": 0,
                        "original_width": 1920,
                        "original_height": 1080,
                    },
                ],
                "was_cancelled": False,
                "ground_truth": False,
                "lead_time": None,
                "task": 1,
                "parent_prediction": None,
                "parent_annotation": None,
            }
        }

        actual_post_request_kwargs_2 = ls_project_mock.make_request.call_args_list[2].kwargs
        # delete key because it is randomly generated
        actual_post_request_kwargs_2["json"]["result"][0].pop("id")
        self.assertEqual(
            expected_post_request_args_2, ls_project_mock.make_request.call_args_list[2].args
        )
        self.assertEqual(expected_post_request_kwargs_2, actual_post_request_kwargs_2)

        # check call count
        self.assertEqual(ls_project_mock.make_request.call_count, 3)

    def test_import_ls_annotation_azure_format(self):
        """
        Verifies the successful import of annotations in the azure format
        """

        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "label_config": "<View>"
                '<Header value="Select the label and fit a bounding box around every'
                ' matching object. Leave a very tiny margin around the actual object."/>'
                '<Image name="image" value="$image" zoom="true" zoomControl="true"/>'
                '<RectangleLabels name="label" toName="image">'
                '<Label value="Testklasse1"/><Label value="Testklasse2"/>'
                "</RectangleLabels>"
                "</View>",
            }

        # Setup s3 client
        self.s3_client_mock.list_objects_v2.side_effect = [
            {"Contents": [{"Key": "testingprefix/azure_csv.csv"}]},
            {
                "Contents": [
                    {"Key": "testingprefix/pallet1.jpeg"},
                    {"Key": "testingprefix/pallet_2.jpeg"},
                    {"Key": "testingprefix/pallet_invalid.jpeg"},
                    {"Key": "testingprefix/"},
                ]
            },
        ]

        annotation_content = BytesIO(
            b"""\
            Path,Label,LabelConfidence
            somepath/pallet1.jpeg,Testklasse1,1
            somepath/pallet_invalid.jpeg,Testklasse_invalid,1
            somepath/pallet_2.jpeg,\"Testklasse1,Testklasse2\",\"1,1\""""
        )

        # annotation_content_encoded = bytes(annotation_content)
        body = StreamingBody(annotation_content, annotation_content.getbuffer().nbytes)

        self.s3_client_mock.get_object.return_value = {"Body": body}

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.get_params.return_value = json_func()
        ls_project_mock.make_request.return_value.json.return_value = [
            {"id": 0, "data": {"image": "testingprefix/pallet1.jpeg"}, "annotations": [{"id": 1}]},
            {
                "id": 1,
                "data": {"image": "testingprefix/pallet_2.jpeg"},
                "annotations": [{"id": 2}],
            },
        ]
        self.ls_client_mock.get_project.return_value = ls_project_mock

        num_imported_annotations = self.labeling_service.import_ls_annotation(
            project_id=1, s3_prefix="testingprefix"
        )

        self.s3_client_mock.list_objects_v2.assert_called()
        self.ls_client_mock.get_project.assert_called_with(1)
        self.s3_client_mock.get_object.assert_called()
        self.assertEqual(num_imported_annotations, 2)
        ls_project_mock.make_request.assert_any_call("GET", "/api/projects/1/tasks?page_size=-1")
        ls_project_mock.make_request.assert_any_call(
            "POST",
            "/api/tasks/0/annotations",
            json={
                "result": [
                    {
                        "value": {"choices": ["Testklasse1"]},
                        "from_name": "choice",
                        "to_name": "image",
                        "type": "choices",
                        "origin": "manual",
                    }
                ],
                "was_cancelled": False,
                "ground_truth": False,
                "lead_time": None,
                "task": 0,
                "parent_prediction": None,
                "parent_annotation": None,
            },
        )
        ls_project_mock.make_request.assert_any_call(
            "POST",
            "/api/tasks/1/annotations",
            json={
                "result": [
                    {
                        "value": {"choices": ["Testklasse1", "Testklasse2"]},
                        "from_name": "choice",
                        "to_name": "image",
                        "type": "choices",
                        "origin": "manual",
                    }
                ],
                "was_cancelled": False,
                "ground_truth": False,
                "lead_time": None,
                "task": 1,
                "parent_prediction": None,
                "parent_annotation": None,
            },
        )
        self.assertEqual(ls_project_mock.make_request.call_count, 3)

    def test_import_ls_annotation_ls_format(self):
        """
        Verifies the successful import of annotations in the ls format
        """

        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "label_config": "<View>"
                '<Header value="Select the label and fit a bounding box around every'
                ' matching object. Leave a very tiny margin around the actual object."/>'
                '<Image name="image" value="$image" zoom="true" zoomControl="true"/>'
                '<RectangleLabels name="label" toName="image">'
                '<Label value="testvalue1"/><Label value="testvalue2"/>'
                "</RectangleLabels>"
                "</View>",
            }

        # Setup s3 client
        self.s3_client_mock.list_objects_v2.side_effect = [
            {"Contents": [{"Key": "testingprefix/ls_json.json"}]},
            {
                "Contents": [
                    {"Key": "testingprefix/pallet1.jpg"},
                    {"Key": "testingprefix/pallet_2.jpg"},
                    {"Key": "testingprefix/pallet_invalid.jpg"},
                    {"Key": "testingprefix/"},
                ]
            },
        ]

        annotation_content = [
            {
                "data": {"image": "somepath/pallet1.jpg"},
                "annotations": [{"result": [{"id": 1, "value": "testvalue1"}]}],
            },
            {
                "data": {"image": "somepath/pallet_2.jpg"},
                "annotations": [{"result": [{"id": 2, "value": "testvalue2"}]}],
            },
            {
                "data": {"image": "somepath/pallet_invalid.jpg"},
                "annotations": [{"result": [{"id": 3, "value": "testvalue_invalid"}]}],
            },
        ]

        annotation_content_encoded = json.dumps(annotation_content).encode("utf-8")
        body = StreamingBody(BytesIO(annotation_content_encoded), len(annotation_content_encoded))

        self.s3_client_mock.get_object.return_value = {"Body": body}

        # Setup Label Studio Client
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "Testing Dataset"
        ls_project_mock.get_params.return_value = json_func()
        ls_project_mock.make_request.return_value.json.return_value = [
            {
                "id": 0,
                "data": {"image": "testingprefix/pallet1.jpg"},
                "annotations": [{"id": 1, "value": "testvalue1"}],
            },
            {
                "id": 1,
                "data": {"image": "testingprefix/pallet_2.jpg"},
                "annotations": [{"id": 2, "value": "testvalue2"}],
            },
        ]
        self.ls_client_mock.get_project.return_value = ls_project_mock

        num_imported_annotations = self.labeling_service.import_ls_annotation(
            project_id=1, s3_prefix="testingprefix"
        )

        self.s3_client_mock.list_objects_v2.assert_called()
        self.ls_client_mock.get_project.assert_called_with(1)
        self.s3_client_mock.get_object.assert_called()
        self.assertEqual(num_imported_annotations, 2)


class TestLabelingServiceExternalURL(unittest.TestCase):
    """
    Tests management of presigned url generation for dataset files
    """

    def setUp(self) -> None:
        self.ls_client_mock = mock.MagicMock(spec=Client)
        self.s3_client_mock = mock.MagicMock(spec=S3Client)
        self.sts_client_mock = mock.MagicMock(spec=STSClient)
        self.labeling_service = LabelingService(
            self.ls_client_mock,
            self.s3_client_mock,
            self.sts_client_mock,
            "testingbucketname",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

    def test_replaces_endpoint(self):
        """
        Verifies that the internal endpoint
        is replaced with the externally accessible endpoint
        """
        internal_url = "http://s3:9000/bucket/dir/file?param=testvalue"
        external_url = self.labeling_service.make_external_presigned_url(internal_url)

        self.assertEqual(
            external_url,
            "https://s3.example.com:443/bucket/dir/file?param=testvalue",
        )

    def test_replaces_endpoint_with_warning(self):
        """
        Verifies that the internal endpoint
        is replaced with the externally accessible endpoint.
        The passed url doesn't equal the expected internal endpoint,
        therefore a warning should be logged
        but the url should be replaced as normal.
        """
        internal_url = "http://s3-docker:123/bucket/dir/file?param=testvalue"
        external_url = self.labeling_service.make_external_presigned_url(internal_url)

        self.assertEqual(
            external_url,
            "https://s3.example.com:443/bucket/dir/file?param=testvalue",
        )
        # TODO: assert that warning is logged


class TestLabelingServiceZipExport(unittest.TestCase):
    """
    Tests the Annotation Zip-Export
    """

    def setUp(self):
        self.ls_client_mock = mock.MagicMock(spec=Client)
        self.s3_client_mock = mock.MagicMock(spec=S3Client)
        self.sts_client_mock = mock.MagicMock(spec=STSClient)
        self.labeling_service = LabelingService(
            self.ls_client_mock,
            self.s3_client_mock,
            self.sts_client_mock,
            "testingbucketname",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

    def test_export_coco_annotations_to_s3(self):
        """
        Verifies the export of annotations in the "COCO" format
        """

        script_dir = Path(os.path.dirname(os.path.abspath(__file__)))

        # we need to wrap this test in a try / finally block
        # to clean up "uploaded" files in any case
        try:

            def download_file_from_s3_mock(
                Bucket, Key, Filename
            ):  # pylint: disable=unused-argument,invalid-name
                """
                Helper method to mock downloading of a file from s3
                to the local file system.
                We simply copy the file from our test data directory
                to the desired download location.
                """
                file_dir = script_dir.joinpath("../../test_data/label_studio_export/")

                if "pallet1.jpg" in Key:
                    file_path = file_dir / "pallet1.jpg"
                elif "pallet2.jpg" in Key:
                    file_path = file_dir / "pallet2.jpg"
                else:
                    raise NotImplementedError()

                shutil.copy(file_path, Path(Filename))

            uploaded_files = []

            def upload_file_to_s3_mock(
                outzipfile, Bucket, Key
            ):  # pylint: disable=unused-argument,invalid-name
                mock_upload_file = tempfile.TemporaryFile()
                mock_upload_file.write(outzipfile.read())
                uploaded_files.append(mock_upload_file)

            # Open the test data for zip archive from Label Studio
            ls_json_path = script_dir.joinpath(
                "../../test_data/label_studio_export/ls_json/test_project_ls.json"
            )
            with open(ls_json_path, "rb") as ls_json_file:
                response = mock.Mock()
                response.content = ls_json_file.read()

                # Setup Label Studio Client
                ls_project_mock = mock.MagicMock(spec=Project)
                ls_project_mock.id = 1
                ls_project_mock.title = "Testing Dataset"
                ls_project_mock.make_request.return_value = response
                self.ls_client_mock.get_project.return_value = ls_project_mock

                # Setup S3 client for file downloads and uploads
                self.s3_client_mock.download_file.side_effect = download_file_from_s3_mock
                self.s3_client_mock.upload_fileobj.side_effect = upload_file_to_s3_mock
                self.s3_client_mock.generate_presigned_url.return_value = (
                    "http://some-servie:9000/project/data/annotations.zip"
                )

                # Act
                with mock.patch(
                    "gts_backend.services.labeling.service."
                    + "LabelingService.validate_s3_import_settings"
                ) as validate_import_settings_mock, mock.patch(
                    "gts_backend.services.labeling.service."
                    + "LabelingService.validate_s3_export_settings"
                ) as validate_export_settings_mock:
                    validate_import_settings_mock.return_value = True
                    validate_export_settings_mock.return_value = True

                    presigned_url = self.labeling_service.export_annotations_to_s3(
                        project_id=1,
                        s3_prefix="testing_s3_prefix",
                        filename="latest",
                    )

            # Assert
            self.ls_client_mock.get_project.assert_called_once_with(1)
            ls_project_mock.make_request.assert_called_once_with(
                method="GET",
                url="/api/projects/1/export"
                + "?exportType=JSON&download_resources=true&download_all_tasks=false",
            )
            self.s3_client_mock.download_file.assert_called()
            self.s3_client_mock.generate_presigned_url.assert_called_once_with(
                "get_object",
                Params={
                    "Bucket": "testingbucketname",
                    "Key": "testing_s3_prefix/annotated/latest.zip",
                },
                ExpiresIn=3600,
            )
            self.assertEqual(
                presigned_url,
                "https://s3.example.com:443/project/data/annotations.zip",
            )

            # Assert that the created zip file contains the images
            self.assertEqual(len(uploaded_files), 1)

            with ZipFile(uploaded_files[0]) as zipfile:
                info = zipfile.infolist()
                with zipfile.open("testing_s3_prefix_ls.json") as resultfile:
                    result = json.load(resultfile)

                    self.assertEqual(result[0]["data"]["image"], "s3://test-bucket/pallet1.jpg")
                    self.assertEqual(result[1]["data"]["image"], "s3://test-bucket/pallet2.jpg")

                    self.assertIn("images/pallet2.jpg", zipfile.namelist())
                    self.assertIn("images/pallet1.jpg", zipfile.namelist())

        finally:
            # manually clean up all uploaded files from our file system
            for uploaded_file in uploaded_files:
                uploaded_file.close()


if __name__ == "__main__":
    unittest.main()
