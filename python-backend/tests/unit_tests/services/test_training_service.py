# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the TrainingService."""
from datetime import datetime
from unittest.mock import MagicMock
import pytest


from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.services.jobs.training.job import TrainingJob
from gts_backend.services.jobs.training.structs import TrainingJobData
from gts_backend.services.training.training_service import TrainingService


@pytest.fixture(name="training_params")
def get_training_params():
    """Fixture to get training params."""
    return {
        "epochs": 5,
        "trainLabelStudioProjectIds": ["1", "2"],
        "evalLabelStudioProjectIds": ["1", "2"],
        "model": "yolox_nano",
        "classes": ["test_class", "other_class"],
        "batchSize": 2,
        "modelName": "Test Model",
        "description": "This is just for tests.",
        "endpoint": "http://dummy-train-service.com",
    }


@pytest.fixture(name="training_job_config")
def get_training_job_config():
    """Fixture to get training job config."""
    job_config_mock = MagicMock(spec=TrainingJobData)
    job_config_mock.model_config = {
        "model": "yolox_nano",
        "epochs": 5,
        "batch_size": 2,
        "class_mapping": {
            "model_classes": [
                {"class_name": "test_class", "class_id": 0},
                {"class_name": "other_class", "class_id": 1},
            ]
        },
    }
    return job_config_mock


@pytest.fixture(name="dataset_model")
def dataset_mock(mocker):
    """Mock a dataset model."""
    mocker.patch(
        "gts_backend.services.training.training_service.DatasetModelSQL",
        return_value=DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        ),
    )


@pytest.fixture(name="training_job")
def training_job_fixture(mocker):
    """Mock a training job"""
    training_job_mock = MagicMock(spec=TrainingJob)
    training_job_config_mock = MagicMock(spec=TrainingJobData)
    training_job_mock.run.return_value = "123"
    training_job_config_mock.model_config = {
        "model": "yolox_nano",
        "epochs": 5,
        "batch_size": 2,
        "class_mapping": {
            "model_classes": [
                {"class_name": "test_class", "class_id": 0},
                {"class_name": "other_class", "class_id": 1},
            ]
        },
    }
    training_job_mock.create_job_data.return_value = training_job_config_mock

    mocker.patch(
        "gts_backend.services.training.training_service.TrainingJob",
        return_value=training_job_mock,
    )


def test_start_training(training_params, dataset_model, training_job):
    """Test expected return value of the start_training method."""
    training_service = TrainingService(training_params)
    job_id = training_service.start_training()
    assert job_id == "123"


def test_retrieve_model_config(training_params, dataset_model, training_job):
    """Test expected return value of retrieve_model_config method."""
    training_service = TrainingService(training_params)
    model_config = training_service.retrieve_model_config()
    assert model_config == {
        "model": "yolox_nano",
        "epochs": 5,
        "batch_size": 2,
        "class_mapping": {
            "model_classes": [
                {"class_name": "test_class", "class_id": 0},
                {"class_name": "other_class", "class_id": 1},
            ]
        },
    }
