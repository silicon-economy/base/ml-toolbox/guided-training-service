# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Pytest fixtures for the job tests.
"""
import os
from datetime import datetime
from unittest.mock import MagicMock

import cv2
import pytest
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.adapter import S3Adapter
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation, S3Tuple
from requests import Response

from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.services.jobs.prediction.structs import PredictionJobData


@pytest.fixture(name="mlflow_credentials")
def fixture_mlflow_credentials(mocker):
    """Mock the MLFlow credentials."""
    mocker.patch(
        "mlcvzoo_util.adapters.mlflow.credentials.MlflowCredentials.init_from_os_environment",
        return_value=MlflowCredentials(
            server_uri="http://test_server_uri",
            s3_region_name="eu-central-1",
            s3_endpoint_url="http://test_endpoint",
            s3_artifact_access_key="test_s3_artifact_access_key",
            s3_artifact_access_id="test_s3_artifact_access_id",
            run_id=None,
        ),
    )


@pytest.fixture(name="mlflow_run_id")
def fixture_mlflow_run_id():
    """Fixture for the MLflow run ID."""
    return "abc123"


@pytest.fixture(name="s3_credentials")
def fixture_s3_credentials():
    """Mock the S3 credentials."""
    return S3Credentials(
        s3_region_name="eu-central-1",
        s3_endpoint_url="http://test_endpoint",
        s3_artifact_access_key="test_s3_artifact_access_key",
        s3_artifact_access_id="test_s3_artifact_access_id",
    )


@pytest.fixture(name="s3_adapter")
def fixture_s3_adapter(s3_credentials, encoded_image):
    """Mock the S3Adapter."""
    adapter = MagicMock(spec=S3Adapter)
    adapter.credentials = s3_credentials
    adapter.list_files.return_value = [
        S3Tuple("test_bucket", "test_prefix/test_img_one.jpg"),
        S3Tuple("test_bucket", "test_prefix/test_img_two.jpg"),
    ]
    adapter.get_file_as_bytes.return_value = encoded_image
    adapter.generate_presigned_urls.return_value = [
        "http://test_prefix/predicted/unique_registered_test_model/test_img_one_predicted.jpg",
        "http://test_prefix/predicted/unique_registered_test_model/test_img_two_predicted.jpg",
    ]
    return adapter


@pytest.fixture(name="os_environ", autouse=True)
def fixture_os_environ():
    """Set the requrired environment variables."""
    vars = {
        "AWS_SECRET_ACCESS_KEY": "test_secret_access_key",
        "AWS_ACCESS_KEY_ID": "test_access_key_id",
        "S3_ENDPOINT": "http://test_endpoint",
        "S3_BUCKET": "test_bucket",
        "S3_ACCESS_KEY_ID": "test_access_key_id",
        "S3_SECRET_ACCESS_KEY": "test_secret_access_key",
        "S3_ENDPOINT_EXTERNAL": "http://test_endpoint",
        "S3_REGION_NAME": "eu-central-1",
        "MLFLOW_S3_ENDPOINT_URL": "http://test_endpoint",
        "MLFLOW_SERVER_URL": "http://test_server_uri",
        "MLFLOW_DEFAULT_EXPERIMENT": "test_experiment",
        "AWS_DEFAULT_REGION": "eu-central-1",
    }
    for key, value in vars.items():
        os.environ[key] = value
    yield
    for key in vars.keys():
        del os.environ[key]


@pytest.fixture(name="boto_3")
def fixture_boto_3(mocker):
    """Mock the MLFlowAdapter."""
    mocker.patch("boto3.client")


@pytest.fixture(name="prediction_job_data")
def fixture_prediction_job_data(predictions):
    """Return the prediction job data."""
    return PredictionJobData(
        image_locations=[],
        checkpoint_location=S3DataLocation(
            uri="test_uri/test_bucket/test_key/test_ckpt.pth",
            credentials={
                "s3_endpoint_url": "http:/test-endpoint",
                "s3_region_name": "test-region",
                "s3_artifact_access_key": "test-access-key",
                "s3_artifact_access_id": "test-access-id",
            },
        ),
        model_config={
            "param": "string",
            "list": ["list", "list_2"],
            "tuple": {"__is_tuple__": True, "content": [1, 2]},
        },
        model_type_name="",
    )


@pytest.fixture(name="encoded_image")
def fixture_encoded_image(project_root):
    """Return the encoded image."""
    test_image = cv2.imread(
        os.path.join(
            project_root,
            "tests/test_data/data/test_inference_image.jpg",
        )
    )
    _, encoded_image = cv2.imencode(".jpg", test_image)
    return encoded_image.tobytes()


@pytest.fixture(name="time_sleep", autouse=True)
def fixture_time_sleep(mocker):
    """Mock the time sleep."""
    return mocker.patch("time.sleep", return_value=None)


@pytest.fixture(name="dataset")
def fixture_dataset():
    """Return the dataset."""
    return DatasetModelSQL(
        ls_project_id=1,
        name="test_dataset",
        s3_prefix="project_1_uuid",
        task_type="object_detection",
        labels='["random_class", "dummy_class"]',
        last_modified=datetime.fromisoformat("2022-01-01T12:12:12"),
        tags="private",
    )


@pytest.fixture(name="post_request")
def fixture_post_request(mocker, mlflow_run_id):
    """Mock the post request."""
    post_response = Response()
    post_response.status_code = 201
    post_response._content = bytes(f'{{"job_id": "{mlflow_run_id}"}}', "utf-8")
    return mocker.patch("requests.post", return_value=post_response)


@pytest.fixture(name="bounding_boxes")
def fixture_bboxes():
    """Return bounding boxes."""
    test_box_1 = BoundingBox(
        box=Box(
            xmin=int(0),
            ymin=int(0),
            xmax=int(100),
            ymax=int(100),
        ),
        class_identifier=ClassIdentifier(
            class_id=0,
            class_name="random_class",
        ),
        score=1,
        difficult=False,
        occluded=False,
        content="",
        model_class_identifier=ClassIdentifier(
            class_id=0,
            class_name="random_class",
        ),
    ).to_dict()
    test_box_2 = BoundingBox(
        box=Box(
            xmin=int(0),
            ymin=int(0),
            xmax=int(42),
            ymax=int(42),
        ),
        class_identifier=ClassIdentifier(
            class_id=1,
            class_name="dummy_class",
        ),
        score=1,
        difficult=False,
        occluded=False,
        content="",
        model_class_identifier=ClassIdentifier(
            class_id=1,
            class_name="dummy_class",
        ),
    ).to_dict()
    return [test_box_1, test_box_2]
