# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the TrainingJob.
"""
import os
from datetime import datetime
from unittest import mock

import pytest
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.mlflow.structs import TrainingStatus
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation
from requests import Response

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
    ConfigCreatorDatasetConfig,
    ModelParameterConfig,
)
from gts_backend.services.configuration_creator.structs import StringReplacements
from gts_backend.services.jobs.base.configuration import BaseJobConfig
from gts_backend.services.jobs.training.configuration import TrainingJobConfig
from gts_backend.services.jobs.training.job import TrainingJob
from gts_backend.services.jobs.training.structs import DataRootDirs, TrainingJobData
from tests.unit_tests.jobs.job_fixtures import (
    fixture_boto_3,
    fixture_dataset,
    fixture_mlflow_credentials,
    fixture_mlflow_run_id,
    fixture_os_environ,
    fixture_post_request,
    fixture_s3_adapter,
    fixture_s3_credentials,
)


@pytest.fixture(name="datetime_mock")
def datetime_now_fixture(mocker):
    """Fixture for the current time."""
    mock_date = mocker.patch("gts_backend.services.jobs.training.job.datetime")
    fixed_time = datetime(2021, 1, 1, 0, 0, 0)
    mock_date.now.return_value = fixed_time

    yield


@pytest.fixture(name="mlflow_adapter")
def fixture_mlflow_adapter(mlflow_credentials):
    """Mock for the MLflowAdapter."""
    adapter = mock.MagicMock(spec=MlflowAdapter)
    adapter.credentials = MlflowCredentials(
        server_uri="http://test_server_uri",
        s3_region_name="eu-central-1",
        s3_endpoint_url="http://test_endpoint",
        s3_artifact_access_key="test_s3_artifact_access_key",
        s3_artifact_access_id="test_s3_artifact_access_id",
        run_id=None,  # should be set during runtime
    )
    # adapter.register_mlflow_model.return_value = ""
    return adapter


@pytest.fixture(name="mlflow_run_id_mock")
def fixture_mlflow_run_id_mock(mocker, mlflow_run_id):
    """Fixture for the MLflow run ID."""
    return mocker.patch(
        "mlcvzoo_util.adapters.mlflow.adapter.MlflowAdapter.create_run_id",
        return_value=mlflow_run_id,
    )


@pytest.fixture(name="dataset_config")
def fixture_dataset_config():
    """Fixture for the dataset configuration."""
    return ConfigCreatorDatasetConfig(
        class_mapping=ClassMappingConfig(
            model_classes=[
                ClassMappingModelClassesConfig(class_name=c, class_id=i)
                for i, c in enumerate(["human", "car", "dog"])
            ],
            mapping=[
                ClassMappingMappingConfig(
                    input_class_name="cat",
                    output_class_name="dog",
                )
            ],
            number_classes=3,
        ),
    )


@pytest.fixture(name="base_config")
def fixture_base_config():
    """Fixture for the base configuration."""
    return BaseJobConfig(server_endpoint="https://dummy-endpoint")


@pytest.fixture(name="model_version")
def fixture_model_version():
    """Fixture for the model version."""
    return "some_version"


@pytest.fixture(name="model_parameter_config")
def fixture_model_parameter_config(request, model_version):
    """Fixture for the model parameter configuration."""
    return ModelParameterConfig(
        epochs=10,
        batch_size=1,
        model=request.param,
        model_version=model_version,
        dataset_names=["test_dataset", "test_dataset_2"],
        nms_threshold=0.5,
        score_threshold=0.5,
    )


@pytest.fixture(name="fixed_model_parameter_config")
def fixture_fixed_model_parameter_config(model_version):
    """Fixture for the model parameter configuration."""
    return ModelParameterConfig(
        epochs=10,
        batch_size=1,
        model="yolox__nano",
        model_version=model_version,
        dataset_names=["test_dataset", "test_dataset_2"],
        nms_threshold=0.5,
        score_threshold=0.5,
    )


@pytest.mark.parametrize(
    "model_parameter_config",
    [
        "yolox__nano",
        "mmdetection_object_detection__yolov3_d53_mstrain-608_273e_coco",
        "mmdetection_object_detection__htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco",
        "mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc",
        "mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e",
        "mmdetection_object_detection__rtmdet_m_8xb32-300e_coco_20220719_112220-229f527c",
        "mmdetection_object_detection__rtmdet_l_8xb32-300e_coco_20220719_112030-5a0be7c4",
        "mmdetection_object_detection__rtmdet_x_8xb32-300e_coco_20220715_230555-cc79b9ae",
        "mmdetection_segmentation__yolact_r50_8x8_coco_20200908-ca34f5db",
        "mmdetection_segmentation__yolact_r101_1x8_coco_20200908-4cbe9101",
        "mmrotate__rotated_rtmdet_tiny-3x-dota_ms-f12286ff",
        "mmrotate__rotated_rtmdet_l-coco_pretrain-3x-dota_ms-06d248a2",
        "mmocr_text_recognition__abinet_20e_st-an_mj_20221005_012617-ead8c139",
        "mmpretrain__efficientnet-b0_3rdparty_8xb32_in1k_20220119-a7e2a0b1",
        "mmpretrain__efficientnet-b4_3rdparty-ra-noisystudent_in1k_20221103-16ba8a2d",
        "mmpretrain__efficientnet-b8_3rdparty_8xb32-aa-advprop_in1k_20220119-297ce1b7",
    ],
    indirect=True,
)
def test_training_job_run(
    mlflow_adapter,
    mlflow_run_id,
    mlflow_run_id_mock,
    dataset,
    datetime_mock,
    boto_3,
    post_request,
    base_config,
    model_parameter_config,
    # fixed_model_parameter_config,
    dataset_config,
    model_version,
):
    """Test the TrainingJob for all implemented models.

    As the main interface method heavily relies on other services which are tested in their own
    unit tests, we only test the run method here and mock the external services. To test the full
    functionality anyway, we also test the correct usage of the external services i.e. their
    respective mocks.
    """
    config_creator_config = ConfigCreatorConfig(
        parameters=model_parameter_config, dataset=dataset_config
    )
    job_config = TrainingJobConfig(creator=config_creator_config, base=base_config)

    job = TrainingJob(
        config=job_config, training_datasets=[dataset], evaluating_datasets=[dataset]
    )
    job.mlflow_adapter = mlflow_adapter

    job_id = job.run()

    # expected return value
    assert job_id == mlflow_run_id

    # expected calls
    # mlflow_adapter
    job.mlflow_adapter.register_mlflow_model.assert_called_once_with(
        run_id=mlflow_run_id, unique_name=model_version
    )
    job.mlflow_adapter.log_params.assert_called_once_with(
        run_id=mlflow_run_id,
        params={
            str(TrainingStatus.NEW.name): "2021-01-01 00:00:00",
            "batch_size": job.configuration.creator.parameters.batch_size,
            "datasets": [dataset.name for dataset in job.training_datasets],
            "evaluation_datasets": [dataset.name for dataset in job.evaluating_datasets],
            "epochs": job.configuration.creator.parameters.epochs,
            "description": job.description,
            "model_type_name": job.configuration.creator.parameters.model.split("__")[0],
            "model": job.configuration.creator.parameters.model,
        },
    )


@pytest.mark.parametrize(
    "model_parameter_config",
    [
        "unsupported_framework__unsupported_model",
        "mmdetection_object_detection__yolov3_unsupported",
        "mmdetection_segmentation__yolact_r50_unsupported",
        "mmrotate__rotated_rtmdet_unsupported",
        "mmocr_text_recognition__abinet_unsupported",
        "mmpretrain__efficientnet-unsupported",
    ],
    indirect=True,
)
def test_initialization_of_training_job_invalid_models(
    mlflow_run_id_mock,
    boto_3,
    dataset,
    base_config,
    model_parameter_config,
    dataset_config,
):
    """Test the initialization of the training job.

    It is tested, if invalid/ not implemented models raise a ValueError.
    """
    config_creator_config = ConfigCreatorConfig(
        parameters=model_parameter_config, dataset=dataset_config
    )
    job_config = TrainingJobConfig(creator=config_creator_config, base=base_config)

    with pytest.raises(ValueError):
        job = TrainingJob(
            config=job_config, training_datasets=[dataset], evaluating_datasets=[dataset]
        )


def test_mask_sensitive_data(boto_3, fixed_model_parameter_config, dataset_config, base_config):
    """Test the masking of sensitive data."""
    data = TrainingJobData(
        image_locations=[
            S3DataLocation(
                uri="test_uri/test_bucket/test_key/test_image.jpg",
                credentials={
                    "s3_endpoint_url": "http:/test-endpoint",
                    "s3_region_name": "test-region",
                    "s3_artifact_access_key": "test-access-key",
                    "s3_artifact_access_id": "test-access-id",
                },
            )
        ],
        annotation_locations=[
            S3DataLocation(
                uri="test_uri/test_bucket/test_key/test_annotation_file.json",
                credentials={
                    "s3_endpoint_url": "http:/test-endpoint",
                    "s3_region_name": "test-region",
                    "s3_artifact_access_key": "test-access-key",
                    "s3_artifact_access_id": "test-access-id",
                },
            )
        ],
        checkpoint_location=S3DataLocation(
            uri="test_uri/test_bucket/test_key/test_ckpt.pth",
            credentials={
                "s3_endpoint_url": "http:/test-endpoint",
                "s3_region_name": "test-region",
                "s3_artifact_access_key": "test-access-key",
                "s3_artifact_access_id": "test-access-id",
            },
        ),
        model_config={
            "param": "string",
            "list": ["list", "list_2"],
            "tuple": {"__is_tuple__": True, "content": [1, 2]},
        },
        eval_config={
            "param": "string",
            "list": ["list", "list_2"],
            "tuple": {"__is_tuple__": True, "content": [1, 2]},
        },
        model_type_name="",
        mlflow_credentials=MlflowCredentials(
            server_uri="http://test_server_uri",
            s3_region_name="eu-central-1",
            s3_endpoint_url="http://test_endpoint",
            s3_artifact_access_key="test_s3_artifact_access_key",
            s3_artifact_access_id="test_s3_artifact_access_id",
            run_id=None,  # should be set during runtime
        ),
    )

    replacement = "***"
    masked_data = TrainingJob.mask_sensitive_request_data(
        data=data, credential_replacement=replacement
    )

    # check if the sensitive data is masked
    assert masked_data.image_locations[0].credentials.s3_artifact_access_id == replacement
    assert masked_data.image_locations[0].credentials.s3_artifact_access_key == replacement
    assert masked_data.checkpoint_location.credentials.s3_artifact_access_id == replacement
    assert masked_data.checkpoint_location.credentials.s3_artifact_access_key == replacement
    assert masked_data.annotation_locations[0].credentials.s3_artifact_access_id == replacement
    assert masked_data.annotation_locations[0].credentials.s3_artifact_access_key == replacement
    assert masked_data.mlflow_credentials.s3_artifact_access_id == replacement
    assert masked_data.mlflow_credentials.s3_artifact_access_key == replacement


def test_create_job_data(
    mlflow_run_id_mock,
    mlflow_adapter,
    boto_3,
    dataset,
    base_config,
    fixed_model_parameter_config,
    dataset_config,
):
    """Test the creation of the job data.

    Expected data consists of:
        - image_locations for training and eval datasets
        - annotation_locations for training and eval datasets
        - checkpoint_location for model checkpoint
        - model_type_name for the model_type of the used model (i.e. its model family)
        - model_config for the configuration of the model
        - eval_config for the configuration of the evaluation of the model
        - mlflow_credentials for mlflow access
    """
    config_creator_config = ConfigCreatorConfig(
        parameters=fixed_model_parameter_config, dataset=dataset_config
    )
    job_config = TrainingJobConfig(creator=config_creator_config, base=base_config)
    job = TrainingJob(
        config=job_config, training_datasets=[dataset, dataset], evaluating_datasets=[dataset]
    )
    job.mlflow_adapter = mlflow_adapter
    data = job.create_job_data()

    expected_bucket_name = os.getenv("S3_BUCKET")
    expected_image_location = S3DataLocation(
        uri=f"{job.s3_credentials.s3_endpoint_url}/"
        f"{expected_bucket_name}/"
        f"{dataset.s3_prefix}/"
        f"data",
        location_id=StringReplacements.MEDIA_DIR,
        credentials=job.s3_credentials,
    )
    expected_annotation_location = S3DataLocation(
        uri=f"{job.s3_credentials.s3_endpoint_url}/"
        f"{expected_bucket_name}/"
        f"{dataset.s3_prefix}/"
        f"tasks",
        location_id=StringReplacements.MEDIA_DIR,
        credentials=job.s3_credentials,
    )
    expected_checkpoint_location = S3DataLocation(
        uri=f"{job.s3_credentials.s3_endpoint_url}/{expected_bucket_name}/"
        f"{DataRootDirs.INITIAL_CHECKPOINT_DIR}/"
        f"{fixed_model_parameter_config.model}.pth",
        location_id=StringReplacements.BASELINE_MODEL_DIR,
        credentials=job.s3_credentials,
    )
    expected_eval_config = {
        "annotation_handler_config": job.creator.create_annotation_handler_config(
            bucket_name=expected_bucket_name,
            s3_prefixes=[dataset.s3_prefix for dataset in job.evaluating_datasets],
        ).to_dict(),
        "iou_thresholds": [0.5],  # fixed threshold
    }

    assert data.image_locations == [
        expected_image_location,
        expected_image_location,
        expected_image_location,
    ]  # one location for each training and eval dataset
    assert data.annotation_locations == [
        expected_annotation_location,
        expected_annotation_location,
        expected_annotation_location,
    ]  # one location for each training and eval dataset
    assert data.checkpoint_location == expected_checkpoint_location
    assert data.model_type_name == fixed_model_parameter_config.model.split("__")[0]
    assert data.model_config == job.creator.create_model_config(
        bucket_name=expected_bucket_name,
        s3_prefixes=[dataset.s3_prefix for dataset in job.training_datasets],
    ).to_dict(retain_collection_types=True)
    assert data.eval_config == expected_eval_config
    assert data.mlflow_credentials == job.mlflow_adapter.credentials


def test_training_job_run_no_mlflow_run(
    boto_3,
    fixed_model_parameter_config,
    dataset_config,
    base_config,
    dataset,
    mlflow_adapter,
    mlflow_run_id_mock,
):
    """Test if a RuntimeError is raised if no MLflow run ID is set."""
    config_creator_config = ConfigCreatorConfig(
        parameters=fixed_model_parameter_config, dataset=dataset_config
    )
    job_config = TrainingJobConfig(creator=config_creator_config, base=base_config)

    job = TrainingJob(
        config=job_config, training_datasets=[dataset], evaluating_datasets=[dataset]
    )
    job.current_run_id = None
    with pytest.raises(RuntimeError):
        job.run()


def test_training_job_run_unsuccessful_response(
    mocker,
    boto_3,
    fixed_model_parameter_config,
    dataset_config,
    base_config,
    dataset,
    mlflow_adapter,
    mlflow_run_id_mock,
):
    """Test if a RuntimeError is raised if the response from the training server
    indicates an error."""
    config_creator_config = ConfigCreatorConfig(
        parameters=fixed_model_parameter_config, dataset=dataset_config
    )
    job_config = TrainingJobConfig(creator=config_creator_config, base=base_config)

    job = TrainingJob(
        config=job_config, training_datasets=[dataset], evaluating_datasets=[dataset]
    )
    job.mlflow_adapter = mlflow_adapter

    response = mock.MagicMock(spec=Response)
    response.status_code = 500
    with mocker.patch("requests.post", return_value=response):
        with pytest.raises(RuntimeError):
            job.run()
