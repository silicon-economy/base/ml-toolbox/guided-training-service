# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the AutoAnnotationJob.
"""
import json
from datetime import datetime
from unittest.mock import MagicMock

import cv2
import numpy as np
import pytest
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation
from mlflow.entities.run import Run
from requests import Response

from gts_backend.model.structs import S3ProjectFolders
from gts_backend.services.jobs.auto_annotation.job import AutoAnnotationJob
from gts_backend.services.jobs.base.configuration import BaseJobConfig
from gts_backend.services.jobs.prediction.configuration import (
    PredictionJobBaseConfig,
    PredictionJobConfig,
)
from gts_backend.services.labeling.service import LabelingService
from tests.unit_tests.jobs.job_fixtures import (
    fixture_bboxes,
    fixture_boto_3,
    fixture_dataset,
    fixture_encoded_image,
    fixture_mlflow_credentials,
    fixture_os_environ,
    fixture_prediction_job_data,
    fixture_s3_adapter,
    fixture_s3_credentials,
)


@pytest.fixture(name="mlflow_adapter")
def fixture_mlflow_adapter():
    """Mock the MLFlowAdapter."""
    mlflow_mock = MagicMock(spec=MlflowAdapter)
    mlflow_run = MagicMock(spec=Run)
    mlflow_run.data.params = {"model_type_name": "test_model_type_name"}
    mlflow_mock.get_run_of_registered_model.return_value = mlflow_run
    mlflow_mock.get_model_checkpoint_location.return_value = S3DataLocation(
        uri="test_uri/test_bucket/test_key/test_ckpt.pth",
        credentials={
            "s3_endpoint_url": "http:/test-endpoint",
            "s3_region_name": "test-region",
            "s3_artifact_access_key": "test-access-key",
            "s3_artifact_access_id": "test-access-id",
        },
    )
    mlflow_mock.get_model_config_dict_from_run.return_value = {"test": "config"}
    return mlflow_mock


@pytest.fixture(name="segmentations")
def fixture_segmentations():
    """Return a segmentation."""
    test_seg_1 = Segmentation(
        polygon=[(0, 0), (1, 1), (2, 2)],
        class_identifier=ClassIdentifier(
            class_id=1,
            class_name="dummy_class",
        ),
        score=1,
        difficult=False,
        occluded=False,
        content="",
        model_class_identifier=ClassIdentifier(
            class_id=1,
            class_name="dummy_class",
        ),
    )
    return test_seg_1.to_dict()


@pytest.fixture(name="predictions")
def fixture_predictions(bounding_boxes, segmentations):
    """Return the predictions."""
    box_1, box_2 = bounding_boxes
    return {
        "test_bucket/test_prefix/test_img_one.jpg": [
            {"type": "BoundingBox", "data": box_1},
            {"type": "BoundingBox", "data": box_2},
        ],
        "test_bucket/test_prefix/test_img_two.jpg": [
            {"type": "BoundingBox", "data": box_1},
            {"type": "Segmentation", "data": segmentations},
        ],
    }


@pytest.fixture(name="get_request")
def fixture_get_request(mocker, predictions):
    """Mock the post request."""
    first = Response()
    first._content = b'{"job_status": "RUNNING"}'
    second = Response()
    second._content = json.dumps({"job_status": "FINISHED", "job_results": predictions}).encode(
        "utf-8"
    )

    return mocker.patch("requests.get", side_effect=[first, second])


@pytest.fixture(name="post_request")
def fixture_post_request(mocker):
    """Mock the post request."""
    post_response = Response()
    post_response.status_code = 201
    post_response._content = b'{"job_id": "1234567"}'
    return mocker.patch("requests.post", return_value=post_response)


@pytest.fixture(name="time_sleep")
def fixture_time_sleep(mocker):
    """Mock the time sleep."""
    return mocker.patch("time.sleep", return_value=None)


@pytest.fixture(name="labeling_service")
def fixture_labeling_service(mocker):
    """Mock the LabelingService."""
    ls_mock = mocker.MagicMock(spec=LabelingService)
    ls_mock.import_ls_annotation.return_value = (
        3  # same as num_returned_bboxes in predictions fixture
    )
    return ls_mock


def test_auto_annotation_job_run(
    mlflow_credentials,
    s3_adapter,
    mlflow_adapter,
    boto_3,
    post_request,
    get_request,
    predictions,
    time_sleep,
    labeling_service,
    dataset,
    bounding_boxes,
    segmentations,
):
    """Test the AutoAnnotationJob run method for BoundingBox predictions and segmentations.

    As this method heavily relies on other services which are tested in their own unit tests, we
    only test the run method here and mock the external services. To test the full functionality
    anyway, we also test the correct usage of the external services i.e. their respective mocks.
    """
    job_config = PredictionJobConfig(
        job=PredictionJobBaseConfig(model_name="unique_registered_test_model"),
        base=BaseJobConfig(server_endpoint="http://test_server_endpoint"),
    )
    # from gts_backend.services.jobs.auto_annotation.job import AutoAnnotationJob

    job = AutoAnnotationJob(
        config=job_config, datasets=[dataset], labeling_service=labeling_service
    )
    job.mlflow_adapter = mlflow_adapter
    job.s3_adapter = s3_adapter
    imported_annotations = job.run()

    assert imported_annotations == 3  # same as num_returned_bboxes in predictions fixture

    # Check if the correct methods were called
    # labeling_service
    labeling_service.import_ls_annotation.assert_called_once_with(
        project_id=dataset.ls_project_id, s3_prefix=dataset.s3_prefix
    )

    # s3_adapter
    box_1, box_2 = bounding_boxes
    expected_coco_dict = {
        "info": {
            "year": datetime.now().year,
            "version": "1.0",
            "description": "",
            "contributor": "Label Studio",
            "url": "",
        },
        "categories": [{"id": 0, "name": "random_class"}, {"id": 1, "name": "dummy_class"}],
        "images": [
            {"width": 3648, "height": 2736, "id": 0, "file_name": "image/test_img_one.jpg"},
            {"width": 3648, "height": 2736, "id": 1, "file_name": "image/test_img_two.jpg"},
        ],
        "annotations": [
            {
                "id": 0,
                "image_id": 0,
                "category_id": 0,
                "segmentation": [],
                "bbox": [
                    box_1["box"]["xmin"],
                    box_1["box"]["ymin"],
                    box_1["box"]["xmax"],
                    box_1["box"]["ymax"],
                ],
                "ignore": 0,
                "iscrowd": 0,
                "area": 10000.0,
            },
            {
                "id": 1,
                "image_id": 0,
                "category_id": 1,
                "segmentation": [],
                "bbox": [
                    box_2["box"]["xmin"],
                    box_2["box"]["ymin"],
                    box_2["box"]["xmax"],
                    box_2["box"]["ymax"],
                ],
                "ignore": 0,
                "iscrowd": 0,
                "area": 1764.0,
            },
            {
                "id": 2,
                "image_id": 1,
                "category_id": 0,
                "segmentation": [],
                "bbox": [
                    box_1["box"]["xmin"],
                    box_1["box"]["ymin"],
                    box_1["box"]["xmax"],
                    box_1["box"]["ymax"],
                ],
                "ignore": 0,
                "iscrowd": 0,
                "area": 10000.0,
            },
            {
                "id": 3,
                "image_id": 1,
                "category_id": 1,
                "segmentation": [[p for point in segmentations["polygon"] for p in point]],
                "bbox": cv2.boundingRect(np.array(segmentations["polygon"])),
                "ignore": 0,
                "iscrowd": 0,
            },
        ],
    }
    expected_put_call = (
        f"{dataset.s3_prefix}/{S3ProjectFolders.PREANNOTATION}"
        f"/{job.COCO_ANNOTATION_FILE_DEFAULT_NAME}"
    )
    s3_adapter.put_file.assert_called_once_with(
        file=json.dumps(expected_coco_dict),
        prefix=expected_put_call,
        bucket="test_bucket",
    )
    s3_adapter.list_files.assert_called_once_with(
        bucket="test_bucket",
        prefix=f"{dataset.s3_prefix}/{S3ProjectFolders.DATA.value}",
        filter_empty_files=True,
    )
    assert s3_adapter.get_file_as_bytes.call_args_list[0].kwargs == {
        "bucket": "test_bucket",
        "prefix": "test_prefix/test_img_one.jpg",
    }  # same bucket/ prefix as returned by s3_adapter.list_files
    assert s3_adapter.get_file_as_bytes.call_args_list[1].kwargs == {
        "bucket": "test_bucket",
        "prefix": "test_prefix/test_img_two.jpg",
    }  # same bucket/ prefix as returned by s3_adapter.list_files
