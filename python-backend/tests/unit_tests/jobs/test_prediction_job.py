# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the PredictionJob.
"""
import json
import os
from unittest.mock import MagicMock

import pytest
from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation, S3Tuple
from mlflow.entities.param import Param
from mlflow.entities.run import Run
from mlflow.entities.run_data import RunData
from mlflow.entities.run_info import RunInfo
from requests import Response

from gts_backend.services.jobs.base.configuration import BaseJobConfig
from gts_backend.services.jobs.prediction.configuration import (
    PredictionJobBaseConfig,
    PredictionJobConfig,
)
from gts_backend.services.jobs.prediction.job import PredictionJob
from gts_backend.services.jobs.prediction.structs import PredictionJobData
from tests.unit_tests.jobs.job_fixtures import (
    fixture_bboxes,
    fixture_boto_3,
    fixture_dataset,
    fixture_encoded_image,
    fixture_mlflow_credentials,
    fixture_mlflow_run_id,
    fixture_os_environ,
    fixture_post_request,
    fixture_prediction_job_data,
    fixture_s3_adapter,
    fixture_s3_credentials,
    fixture_time_sleep,
)


@pytest.fixture(name="get_request")
def fixture_get_request(mocker, predictions):
    """Mock a successful request."""
    first = Response()
    first._content = b'{"job_status": "RUNNING"}'
    second = Response()
    second._content = json.dumps({"job_status": "FINISHED", "job_results": predictions}).encode(
        "utf-8"
    )

    return mocker.patch("requests.get", side_effect=[first, second])


@pytest.fixture(name="get_request_no_finished_state")
def fixture_get_request_no_finished(mocker, predictions):
    """Mock an time limit exceeding request."""
    first = Response()
    first._content = b'{"job_status": "RUNNING"}'

    return mocker.patch("requests.get", return_value=first)


@pytest.fixture(name="get_request_crashed_state")
def fixture_get_request_crashed(mocker, predictions):
    """Mock a crashed request."""
    first = Response()
    first._content = b'{"job_status": "CRASHED"}'

    return mocker.patch("requests.get", return_value=first)


@pytest.fixture(name="get_request_error")
def fixture_get_request_error(mocker):
    """Mock the post request."""
    get_response = Response()
    response_dict = {
        "job_status": "CRASHED",
        "job_results": {},
    }
    get_response._content = json.dumps(response_dict).encode("utf-8")
    return mocker.patch("requests.get", return_value=get_response)


@pytest.fixture(name="create_job_data")
def fixture_create_job_data(mocker, prediction_job_data):
    """Mock the create_job_data method."""
    return mocker.patch.object(PredictionJob, "create_job_data", return_value=prediction_job_data)


@pytest.fixture(name="predictions")
def fixture_predictions(bounding_boxes):
    """Return the predictions."""
    box_1, box_2 = bounding_boxes
    return {
        "test_bucket/test_prefix/test_img_one.jpg": [
            {"type": "BoundingBox", "data": box_1},
            {"type": "BoundingBox", "data": box_2},
        ],
        "test_bucket/test_prefix/test_img_two.jpg": [
            {"type": "BoundingBox", "data": box_1},
        ],
    }


@pytest.fixture(name="prediction_job")
def fixture_prediction_job(dataset):
    """Return a PredictionJob."""
    return PredictionJob(
        config=PredictionJobConfig(
            job=PredictionJobBaseConfig(model_name="unique_registered_test_model"),
            base=BaseJobConfig(server_endpoint="http://test_server_endpoint"),
        ),
        datasets=[dataset],
    )


@pytest.fixture(name="prediction_job_multiple")
def fixture_prediction_job_multiple(dataset):
    """Return a PredictionJob."""
    return PredictionJob(
        config=PredictionJobConfig(
            job=PredictionJobBaseConfig(
                model_name="unique_registered_test_model", second_model_name="second_test_model"
            ),
            base=BaseJobConfig(server_endpoint="http://test_server_endpoint"),
        ),
        datasets=[dataset, dataset],
    )


@pytest.fixture(name="image_uri")
def fixture_image_uri():
    """Return the image uri."""
    return "http://test_bucket/test_prefix/test_img_one.jpg"


@pytest.fixture(name="prediction_job_dataset_subset")
def fixture_prediction_job_dataset_subset(image_uri):
    """Return a PredictionJob."""
    return PredictionJob(
        config=PredictionJobConfig(
            job=PredictionJobBaseConfig(model_name="unique_registered_test_model"),
            base=BaseJobConfig(server_endpoint="http://test_server_endpoint"),
        ),
        datasets=[],
        dataset_subset=[image_uri, image_uri],
    )


@pytest.fixture(name="mlflow_run")
def fixture_mlflow_run():
    """Mock the MLFlow run."""
    return Run(
        run_info=RunInfo(
            run_uuid="",
            experiment_id="",
            user_id="",
            status="",
            start_time="",
            end_time="",
            lifecycle_stage="",
            run_id="123456",
            artifact_uri="s3://dummy_bucket/dummy_prefix_part_1/dummy_prefix_part_2",
        ),
        run_data=RunData(
            metrics={},
            params=[
                Param(key="best_checkpoint", value="another_test_ckpt.pth"),
                Param(key="model_type_name", value="mmdetection"),
            ],
        ),
    )


@pytest.fixture(name="model_checkpoint_location")
def fixture_model_checkpoint_location(s3_credentials):
    """Return the model checkpoint location."""
    return S3DataLocation(
        uri="http://test_endpoint/test_bucket/test_key/test_ckpt.pth",
        credentials=s3_credentials,
    )


@pytest.fixture(name="model_config")
def fixture_model_config():
    """Return the model config."""
    return {
        "param": "string",
        "list": ["list", "list_2"],
        "tuple": {"__is_tuple__": True, "content": [1, 2]},
    }


@pytest.fixture(name="mlflow_adapter")
def fixture_mlflow_adapter(mlflow_run, model_checkpoint_location, model_config):
    """Mock the MLFlowAdapter."""
    mlflow_mock = MagicMock(spec=MlflowAdapter)
    mlflow_mock.get_run_of_registered_model.return_value = mlflow_run
    mlflow_mock.get_model_checkpoint_location.return_value = model_checkpoint_location
    mlflow_mock.get_model_config_dict_from_run.return_value = model_config
    return mlflow_mock


def test_prediction_job(
    boto_3,
    get_request,
    post_request,
    create_job_data,
    s3_adapter,
    prediction_job,
    prediction_job_data,
    mlflow_run_id,
):
    """Test the PredictionJob.

    As the main interface method heavily relies on other services which are tested in their own unit tests, we
    only test the run method here and mock the external services. To test the full functionality
    anyway, we also test the correct usage of the external services i.e. their respective mocks.
    """
    job = prediction_job
    job.s3_adapter = s3_adapter
    predicted_images = job.run()

    # expected return value
    assert len(predicted_images) == 2
    assert (
        "http://test_prefix/predicted/unique_registered_test_model/test_img_one_predicted.jpg"
        in predicted_images
    )  # same as s3_adapter fixture returns
    assert (
        "http://test_prefix/predicted/unique_registered_test_model/test_img_two_predicted.jpg"
        in predicted_images
    )  # same as s3_adapter fixture returns

    # check if the correct methods were called
    # s3_adapter
    s3_adapter.generate_presigned_urls.assert_called_once_with(
        files=[
            S3Tuple(
                "test_bucket",
                "test_prefix/predicted/unique_registered_test_model/test_img_one.jpg",  # prefix logic is defined in PredictionJob.__get_new_image_prefix
            ),
            S3Tuple(
                "test_bucket",
                "test_prefix/predicted/unique_registered_test_model/test_img_two.jpg",  # prefix logic is defined in PredictionJob.__get_new_image_prefix
            ),
        ]
    )

    # check correct post request
    expected_post_data = json.dumps(prediction_job_data.to_prediction_server_data())
    assert post_request.call_args.kwargs["data"] == expected_post_data

    # check correct get request
    assert (
        get_request.call_args.kwargs["url"] == f"http://test_server_endpoint/jobs/{mlflow_run_id}"
    )  # url logic is defined in PredictionJob._check_prediction_tasks
    assert get_request.call_count == 2  # same as no. of get_request fixture returns


def test_prediction_job_timelimit_exceeds(
    boto_3,
    get_request_no_finished_state,
    post_request,
    create_job_data,
    s3_adapter,
    prediction_job,
    prediction_job_data,
):
    """Test if an RuntimeError is raised if the time limit for a successful response from the
    server is exceeded."""
    job = prediction_job
    job.s3_adapter = s3_adapter
    with pytest.raises(RuntimeError):
        predicted_images = job.run()


def test_prediction_job_crashed(
    boto_3,
    get_request_crashed_state,
    post_request,
    create_job_data,
    s3_adapter,
    prediction_job,
):
    """Test if an RuntimeError is raised if the time limit for a successful response from the
    server is exceeded."""
    job = prediction_job
    job.s3_adapter = s3_adapter
    with pytest.raises(RuntimeError):
        _ = job.run()


def test_create_job_data(
    boto_3,
    prediction_job,
    mlflow_adapter,
    dataset,
    model_checkpoint_location,
    mlflow_run,
    model_config,
):
    """Test if the create_job_data method creates the correct job data.

    In this test, only one dataset and one model is used.
    """
    job = prediction_job
    job.mlflow_adapter = mlflow_adapter
    data = job.create_job_data()

    expected_image_locations = [
        S3DataLocation(
            uri=f"{job.s3_adapter.credentials.s3_endpoint_url}/{os.getenv('S3_BUCKET')}/"
            f"{dataset.s3_prefix}/data",
            credentials=job.s3_adapter.credentials,
            location_id="MEDIA_DIR",
        ),
    ]

    assert data.image_locations == expected_image_locations
    assert data.checkpoint_location == model_checkpoint_location
    assert data.model_type_name == mlflow_run.data.params["model_type_name"]
    assert data.model_config == model_config


def test_create_job_data_multiple_datasets_and_checkpoints(
    boto_3,
    prediction_job_multiple,
    mlflow_adapter,
    dataset,
    model_checkpoint_location,
    mlflow_run,
    model_config,
):
    """Test if the create_job_data method creates the correct job data if there are multiple
    datasets and two model checkpoints."""
    job = prediction_job_multiple
    job.mlflow_adapter = mlflow_adapter
    data = job.create_job_data()

    expected_image_location = S3DataLocation(
        uri=f"{job.s3_adapter.credentials.s3_endpoint_url}/{os.getenv('S3_BUCKET')}/"
        f"{dataset.s3_prefix}/data",
        credentials=job.s3_adapter.credentials,
        location_id="MEDIA_DIR",
    )

    assert data.image_locations == [expected_image_location, expected_image_location]
    assert data.checkpoint_location == model_checkpoint_location
    assert data.model_type_name == mlflow_run.data.params["model_type_name"]
    assert data.model_config == model_config

    # values should be the same as for the first model as we do not mock a second model
    # anyway the attributes from the second model must be parsed correctly
    assert data.second_model_type_name == mlflow_run.data.params["model_type_name"]
    assert data.second_model_config == model_config
    assert data.second_checkpoint_location == model_checkpoint_location


def test_create_job_data_dataset_subset(
    boto_3,
    prediction_job_dataset_subset,
    mlflow_adapter,
    model_checkpoint_location,
    mlflow_run,
    model_config,
    image_uri,
):
    """Test if the create_job_data method creates the correct job data if a dataset subset is
    used."""
    job = prediction_job_dataset_subset
    job.mlflow_adapter = mlflow_adapter
    data = job.create_job_data()

    expected_image_location = S3DataLocation(
        uri=image_uri,
        credentials=job.s3_adapter.credentials,
        location_id="MEDIA_DIR",
    )

    assert data.image_locations == [expected_image_location, expected_image_location]


def test_mask_sensitive_data(boto_3, prediction_job):
    """Test if the mask_sensitive_request_data method masks sensitive data.

    Data to mask:
    - image_locations
    - checkpoint_location
    - second_checkpoint_location
    """
    job = prediction_job
    data = PredictionJobData(
        image_locations=[
            S3DataLocation(
                uri="test_uri/test_bucket/test_key/test_image.jpg",
                credentials={
                    "s3_endpoint_url": "http:/test-endpoint",
                    "s3_region_name": "test-region",
                    "s3_artifact_access_key": "test-access-key",
                    "s3_artifact_access_id": "test-access-id",
                },
            )
        ],
        checkpoint_location=S3DataLocation(
            uri="test_uri/test_bucket/test_key/test_ckpt.pth",
            credentials={
                "s3_endpoint_url": "http:/test-endpoint",
                "s3_region_name": "test-region",
                "s3_artifact_access_key": "test-access-key",
                "s3_artifact_access_id": "test-access-id",
            },
        ),
        second_checkpoint_location=S3DataLocation(
            uri="test_uri/test_bucket/test_key/test_ckpt_2.pth",
            credentials={
                "s3_endpoint_url": "http:/test-endpoint",
                "s3_region_name": "test-region",
                "s3_artifact_access_key": "test-access-key",
                "s3_artifact_access_id": "test-access-id",
            },
        ),
        model_config={
            "param": "string",
            "list": ["list", "list_2"],
            "tuple": {"__is_tuple__": True, "content": [1, 2]},
        },
        model_type_name="",
    )
    replacement = "***"
    masked_data = job.mask_sensitive_request_data(data=data, credential_replacement=replacement)

    # check if the sensitive data is masked
    assert masked_data.image_locations[0].credentials.s3_artifact_access_id == replacement
    assert masked_data.image_locations[0].credentials.s3_artifact_access_key == replacement
    assert masked_data.checkpoint_location.credentials.s3_artifact_access_id == replacement
    assert masked_data.checkpoint_location.credentials.s3_artifact_access_key == replacement
    assert masked_data.second_checkpoint_location.credentials.s3_artifact_access_id == replacement
    assert masked_data.second_checkpoint_location.credentials.s3_artifact_access_key == replacement
