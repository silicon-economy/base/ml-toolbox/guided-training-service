# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the /models REST Endpoints
"""
import json
import unittest
from unittest import mock

from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlflow.exceptions import RestException

from gts_backend.app import Server
from gts_backend.services.models.model_service import ModelNotFoundError


class TestModels(unittest.TestCase):
    def setUp(self) -> None:
        self.server = Server()
        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    @mock.patch("gts_backend.api.routes.models.ModelService.load_models")
    def test_get_no_models(self, mock_service, mock_adapter):
        """Test if an empty list of model information is returned."""
        mock_service.return_value = {"total_items": 0, "models": []}

        mocked_credentials = MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="test",
            s3_artifact_access_id="test",
            s3_endpoint_url="test",
            s3_region_name="test",
        )

        with mock.patch(
            "gts_backend.api.routes.models.MlflowCredentials.init_from_os_environment",
            return_value=mocked_credentials,
        ):
            with self.app.app_context():
                response = self.client.get("models/")
                data = json.loads(response.get_data(as_text=True))
                self.assertEqual(200, response.status_code)
                self.assertEqual(0, data["total_items"])
                self.assertEqual([], data["models"])

    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    @mock.patch("gts_backend.api.routes.models.ModelService.load_models")
    def test_get_models(self, mock_get_models, mock_adapter):
        """Test if list of model information is returned."""

        model_info = {
            "run_id": "123",
            "name": "A model",
            "accuracy": 0.9,
            "dataset": ["dummy dataset"],
            "description": "dummy_description",
            "model": "object_detection_xyz_model",
            "epochs": 42,
            "batch_size": 2,
            "evaluation_datasets": ["dummy eval dataset"],
            "training_state": "finished",
            "algorithm_type": "object_detection",
            "creation_date": "2023-08-02 00:00:00",
            "mlflow_url": "/test-url/mlflow",
        }

        model_info_2 = {
            "run_id": "456",
            "name": "B model",
            "accuracy": 0.9,
            "dataset": ["dummy dataset"],
            "description": "dummy_description",
            "model": "classification_xyz_model",
            "epochs": 24,
            "batch_size": 1,
            "evaluation_datasets": ["dummy eval dataset"],
            "training_state": "finished",
            "algorithm_type": "classification",
            "creation_date": "2023-08-02 00:00:00",
            "mlflow_url": "/test-url/mlflow",
        }

        mock_get_models.return_value = {
            "total_items": 2,
            "models": [model_info, model_info_2],
        }

        mocked_credentials = MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="test",
            s3_artifact_access_id="test",
            s3_endpoint_url="test",
            s3_region_name="test",
        )

        with mock.patch(
            "gts_backend.api.routes.models.MlflowCredentials.init_from_os_environment",
            return_value=mocked_credentials,
        ):
            with self.app.app_context():
                response = self.client.get("models/")
                data = json.loads(response.get_data(as_text=True))
                self.assertEqual(200, response.status_code)
                self.assertEqual(2, data["total_items"])
                self.assertEqual(model_info, data["models"][0])
                self.assertEqual(model_info_2, data["models"][1])

    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    @mock.patch("gts_backend.api.routes.models.ModelService.load_models")
    def test_returns_multiple_filtered_datasets(self, mock_get_models, mock_mlflow):
        """Test if list of model information is returned filtered and without duplicates."""

        model_info = {
            "run_id": "123",
            "name": "A model",
            "accuracy": 0.9,
            "dataset": ["dummy dataset"],
            "description": "dummy_description",
            "model": "object_detection_xyz_model",
            "epochs": 42,
            "batch_size": 2,
            "evaluation_datasets": ["dummy eval dataset"],
            "training_state": "finished",
            "algorithm_type": "object_detection",
            "creation_date": "2023-08-02 00:00:00",
            "mlflow_url": "/test-url/mlflow",
        }

        model_info_2 = {
            "run_id": "456",
            "name": "B model",
            "accuracy": 0.9,
            "dataset": ["dummy dataset"],
            "description": "dummy_description",
            "model": "classification_xyz_model",
            "epochs": 24,
            "batch_size": 1,
            "evaluation_datasets": ["dummy eval dataset"],
            "training_state": "finished",
            "algorithm_type": "classification",
            "creation_date": "2023-08-02 00:00:00",
            "mlflow_url": "/test-url/mlflow",
        }

        model_info_3 = {
            "run_id": "789",
            "name": "B model",
            "accuracy": 1.0,
            "dataset": ["dummy dataset"],
            "description": "dummy_description",
            "model": "segmentation_xyz_model",
            "epochs": 1,
            "batch_size": 1,
            "evaluation_datasets": ["dummy eval dataset"],
            "training_state": "finished",
            "algorithm_type": "segmentation",
            "creation_date": "2023-08-02 00:00:00",
            "mlflow_url": "/test-url/mlflow",
        }

        mock_get_models.return_value = {
            "total_items": 3,
            "models": [
                model_info,
                model_info_2,
                model_info_3,
            ],
        }

        mocked_credentials = MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="test",
            s3_artifact_access_id="test",
            s3_endpoint_url="test",
            s3_region_name="test",
        )

        with mock.patch(
            "gts_backend.api.routes.models.MlflowCredentials.init_from_os_environment",
            return_value=mocked_credentials,
        ):
            with self.app.app_context():
                response = self.client.get("models/?filter=dummy")
                data = json.loads(response.get_data(as_text=True))
                self.assertEqual(200, response.status_code)
                self.assertEqual(3, data["total_items"])
                self.assertEqual(model_info, data["models"][0])
                self.assertEqual(model_info_2, data["models"][1])
                self.assertEqual(model_info_3, data["models"][2])

    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    @mock.patch("gts_backend.api.routes.models.ModelService.load_model")
    def test_get_single_model(self, mock_get_model, mock_adapter):
        """Test if single model info is returned by MLFlowConnector."""
        model_info = {
            "run_id": "123",
            "name": "A model",
            "accuracy": 0.9,
            "dataset": ["dummy dataset"],
            "description": "dummy_description",
            "model": "object_detection_xyz_model",
            "epochs": 42,
            "batch_size": 2,
            "evaluation_datasets": ["dummy eval dataset"],
            "training_state": "finished",
            "algorithm_type": "object_detection",
            "creation_date": "2023-08-02 00:00:00",
            "mlflow_url": "/test-url/mlflow",
        }

        mock_get_model.return_value = {"total_items": 1, "models": [model_info]}

        mocked_credentials = MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="test",
            s3_artifact_access_id="test",
            s3_endpoint_url="test",
            s3_region_name="test",
        )

        with mock.patch(
            "gts_backend.api.routes.models.MlflowCredentials.init_from_os_environment",
            return_value=mocked_credentials,
        ):
            with self.app.app_context():
                response = self.client.get("models/dummy%20name%201")
                data = json.loads(response.get_data(as_text=True))
                self.assertEqual(200, response.status_code)
                self.assertEqual(1, data["total_items"])
                self.assertEqual(model_info, data["models"][0])

    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    @mock.patch("gts_backend.api.routes.models.ModelService.load_model")
    def test_get_single_model_not_found_error(self, mock_model, mock_adapter):
        """Test if status code 404 is returned if the model is not found."""
        mock_model.side_effect = ModelNotFoundError("Model not found")

        mocked_credentials = MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="test",
            s3_artifact_access_id="test",
            s3_endpoint_url="test",
            s3_region_name="test",
        )

        with mock.patch(
            "gts_backend.api.routes.models.MlflowCredentials.init_from_os_environment",
            return_value=mocked_credentials,
        ):
            with self.app.app_context():
                # with self.assertRaises(Exception):
                response = self.client.get("models/dummy%20name%201")
                self.assertEqual(404, response.status_code)

    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    def test_delete_model(self, mock_delete):
        """Test if a single model is successfully deleted from the model registry and 200 is
        returned as the status code."""

        mocked_credentials = MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="test",
            s3_artifact_access_id="test",
            s3_endpoint_url="test",
            s3_region_name="test",
        )

        with mock.patch(
            "gts_backend.api.routes.models.MlflowCredentials.init_from_os_environment",
            return_value=mocked_credentials,
        ):
            with self.app.app_context():
                response = self.client.delete("models/dummy_model")
                self.assertEqual(200, response.status_code)

    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    def test_delete_single_model_not_found_error(self, mock_delete):
        """Test if status code 404 is returned if the model is not found."""
        mock_delete.side_effect = ModelNotFoundError("Model not found")

        mocked_credentials = MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="test",
            s3_artifact_access_id="test",
            s3_endpoint_url="test",
            s3_region_name="test",
        )

        with mock.patch(
            "gts_backend.api.routes.models.MlflowCredentials.init_from_os_environment",
            return_value=mocked_credentials,
        ):
            with self.app.app_context():
                # with self.assertRaises(Exception):
                response = self.client.delete("models/dummy_model")
                self.assertEqual(404, response.status_code)

    @mock.patch.object(
        MlflowCredentials,
        "init_from_os_environment",
        return_value=MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
        ),
    )
    @mock.patch.object(
        S3Credentials,
        "init_from_os_environment",
        return_value=S3Credentials(
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="s3://endpoint.com/bucket-name/key/",
            s3_region_name="region",
        ),
    )
    @mock.patch("boto3.client")
    @mock.patch("gts_backend.api.routes.models.MlflowAdapter.get_download_link_for_model")
    def test_get_model_download(self, mock_connector, mock_client, mock_s3, mock_mlflow_cred):
        """Test if a download link is provided and status code 200 is returned if the model exists."""
        mock_connector.return_value = "/dummy/download_path/1.txt"
        with self.app.app_context():
            response = self.client.get("models/dummy_model/download")
            self.assertEqual({"url": "/dummy/download_path/1.txt"}, response.get_json())
            self.assertEqual(200, response.status_code)

    @mock.patch.object(
        MlflowCredentials,
        "init_from_os_environment",
        return_value=MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
        ),
    )
    @mock.patch.object(
        S3Credentials,
        "init_from_os_environment",
        return_value=S3Credentials(
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
        ),
    )
    @mock.patch("boto3.client")
    @mock.patch("gts_backend.api.routes.models.MlflowAdapter.get_download_link_for_model")
    def test_get_model_download_exception(
        self, mock_connector, mock_client, mock_s3, mock_mlflow_cred
    ):
        """Test if 404 status code is returned if the model is not found."""
        mock_connector.side_effect = RestException(json={"message": "Model not found"})
        with self.app.app_context():
            response = self.client.get("models/dummy_model/download")
            self.assertEqual(404, response.status_code)


class TestModelMetrics(unittest.TestCase):
    def setUp(self) -> None:
        self.server = Server()
        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

    @mock.patch.object(
        MlflowCredentials,
        "init_from_os_environment",
        return_value=MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
        ),
    )
    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    def test_get_model_metrics(self, mock_metrics, mock_mlflow_cred):
        """Test if status code 200 and expected model metrics are returned if the request succeeds."""
        metrics = {
            "0_loading_unit": {
                "TP": "10.0",
                "FP": "5.0",
                "AP": "0.99",
                "F1": "0.92",
                "FN": "0.0",
                "PR": "0.85",
                "RC": "1.0",
                "COUNT": "60.0",
            },
            "1_pallet": {
                "TP": "10.0",
                "FP": "5.0",
                "AP": "0.99",
                "F1": "0.92",
                "FN": "0.0",
                "PR": "0.85",
                "RC": "1.0",
                "COUNT": "60.0",
            },
        }
        mock_metrics.return_value.get_class_metrics.return_value = metrics

        with self.app.app_context():
            response = self.client.get("models/dummy_model/metrics")
            self.assertEqual(metrics, response.get_json())
            self.assertEqual(200, response.status_code)

    @mock.patch.object(
        MlflowCredentials,
        "init_from_os_environment",
        return_value=MlflowCredentials(
            server_uri="mlflow",
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
        ),
    )
    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    def test_get_model_metrics_no_model_found(self, mock_adapter, mock_creds):
        """Test if 404 status code is returned if model is not found."""
        mock_adapter.side_effect = RestException(json={"message": "Model not found"})
        with self.app.app_context():
            response = self.client.get("models/dummy_model/metrics")
            self.assertEqual(404, response.status_code)


class TestModelFalsePositiveFalseNegativeImages(unittest.TestCase):
    def setUp(self) -> None:
        self.server = Server()
        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

    @mock.patch.object(
        MlflowCredentials,
        "init_from_os_environment",
        return_value=MlflowCredentials(
            server_uri="http://localhost:5050",
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
        ),
    )
    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    @mock.patch("gts_backend.api.routes.models.ModelService.get_fp_fn_images")
    def test_get_fp_fn_images(
        self,
        model_service_mock,
        mock_adapter,
        mlflow_credentials_mock,
    ):
        """Test if fp fn images with status code 200 are returned from endpoint."""
        model_service_mock.return_value = {
            "0_test_class": ["path/to/image_1.jpg", "path/to/image_2.jpg"],
            "1_another_class": ["path/to/image_3.jpg", "path/to/image_4.jpg"],
        }

        with self.app.app_context():
            response = self.client.get("models/dummy_model/fp-fn-images")

            # check return values
            expected_response = {
                "images": {
                    "0_test_class": ["path/to/image_1.jpg", "path/to/image_2.jpg"],
                    "1_another_class": ["path/to/image_3.jpg", "path/to/image_4.jpg"],
                }
            }

            self.assertEqual(expected_response, response.get_json())
            self.assertEqual(200, response.status_code)

    @mock.patch.object(
        MlflowCredentials,
        "init_from_os_environment",
        return_value=MlflowCredentials(
            server_uri="http://localhost:5050",
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
        ),
    )
    @mock.patch("gts_backend.api.routes.models.MlflowAdapter")
    @mock.patch("gts_backend.api.routes.models.ModelService.get_fp_fn_images")
    def test_get_fp_fn_images_model_not_found(
        self, model_service_mock, mock_adapter, mlflow_credentials_mock
    ):
        """Test if 404 status code is returned if model is not found."""
        model_service_mock.side_effect = ModelNotFoundError("Model not found")
        with self.app.app_context():
            response = self.client.get("models/dummy_model/fp-fn-images")
            self.assertEqual(404, response.status_code)
