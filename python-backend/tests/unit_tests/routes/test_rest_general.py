# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the /general REST Endpoints
"""

import unittest

from gts_backend.app import Server
from gts_backend.model.dummy_sql import DummyModelSQL
from gts_backend.services.dbmanager import db


class TestRestGeneral(unittest.TestCase):
    def setUp(self):
        self.server = Server
        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

        with self.app.app_context():
            db.session.execute("PRAGMA foreign_keys = ON;")
            db.session.commit()

    def tearDown(self) -> None:
        with self.app.app_context():
            db.session.remove()
            db.session.commit()

    def test_reset_db(self):
        # prepare
        with self.app.app_context():
            d1 = DummyModelSQL(description="test_dummy_1")
            d2 = DummyModelSQL(description="test_dummy_2")
            db.session.add(d1)
            db.session.add(d2)
            db.session.commit()

            dummys = db.session.query(DummyModelSQL).all()
            self.assertEqual(len(dummys), 2)

            # act
            response = self.client.delete("general/")

            # assert
            self.assertEqual(200, response.status_code)
            dummys = db.session.query(DummyModelSQL).all()
            self.assertEqual(len(dummys), 0)


if __name__ == "__main__":
    unittest.main()
