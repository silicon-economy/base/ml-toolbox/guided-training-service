# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import json
import os
import unittest
from datetime import datetime
from unittest import mock

from gts_backend.app import Server
from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.services.dbmanager import db


class TestTrainService(unittest.TestCase):
    def setUp(self):
        self.server = Server()
        self.app = self.server.create_app(config="test")
        self.app.config["TESTING"] = True
        self.client = self.app.test_client()
        self.params = {
            "epochs": 10,
            "trainLabelStudioProjectIds": ["1"],
            "evalLabelStudioProjectIds": ["1"],
            "model": "yolox__nano",
            "modelName": "Test Model",
            "description": "This is just for tests.",
            "classes": ["test_class"],
            "batchSize": 2,
            "model_config": "",
            "endpoint": "http://dummy-train-service.com",
        }
        self.dummy_dataset = DatasetModelSQL(
            ls_project_id=1,
            name="test_dataset",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:12:12"),
            tags="private",
        )

        # Set dummy variables for test cases
        self.credentials = (
            "AWS_SECRET_ACCESS_KEY",
            "AWS_ACCESS_KEY_ID",
            "AWS_DEFAULT_REGION",
            "S3_ENDPOINT",
            "S3_BUCKET",
            "S3_REGION_NAME",
            "S3_ACCESS_KEY_ID",
            "S3_SECRET_ACCESS_KEY",
            "S3_ENDPOINT_EXTERNAL",
            "MLFLOW_S3_ENDPOINT_URL",
            "MLFLOW_SERVER_URL",
        )

        # save value, if credentials are already set
        self.values = []
        for credential in self.credentials:
            value = os.getenv(credential)
            if value is not None:
                self.values.append(value)
            else:
                self.values.append(None)

            # Temporarily set dummy value for test case
            os.environ[credential] = "test"
            if credential == "S3_ENDPOINT":
                os.environ[credential] = "s3://endpoint.com/bucket/key/"

        with self.app.app_context():
            db.session.execute("PRAGMA foreign_keys = ON;")
            db.session.commit()

    def mock_completed_training(self, *args):
        return None

    def mock_failed_training(self, *args):
        raise MemoryError

    def __init_s3_client(self):
        pass

    @mock.patch("boto3.client")
    @mock.patch("gts_backend.services.training.training_service.TrainingJob.run")
    @mock.patch("gts_backend.services.training.training_service.DatasetModelSQL")
    def test_post_correct_params(self, mock_dataset_model, mock_completed_training, mock_boto3):
        """
        Tests if complete input params are parsed correctly.
        """
        mock_completed_training.return_value = "123"
        with mock.patch(
            "mlcvzoo_util.adapters.mlflow.adapter.MlflowAdapter.create_run_id",
            return_value="123",
        ):
            with self.app.app_context():
                response = self.client.post("training/", json=self.params)
                data = json.loads(response.get_data(as_text=True))

            assert {
                "id": "123",
                "message": "Successfully started training on training server",
            } == data

    @mock.patch("boto3.client")
    @mock.patch("gts_backend.services.training.training_service.TrainingJob.run")
    @mock.patch("gts_backend.services.training.training_service.DatasetModelSQL")
    def test_post_incorrect_params(self, mock_dataset_model, mock_completed_training, mock_boto3):
        """
        Tests if incomplete params results in error code 400
        """
        incomplete_params = {"epochs": 10}

        with self.app.app_context():
            response = self.client.post("training/", json=incomplete_params)
            data = json.loads(response.get_data(as_text=True))
            self.assertEqual(400, response.status_code)

            assert {
                "errors": {
                    "batchSize": "'batchSize' is a required property",
                    "description": "'description' is a required property",
                    "endpoint": "'endpoint' is a required property",
                    "model": "'model' is a required property",
                    "modelName": "'modelName' is a required property",
                },
                "message": "Input payload validation failed",
            } == data

    @mock.patch("boto3.client")
    @mock.patch("gts_backend.services.training.training_service.TrainingJob.run")
    @mock.patch("gts_backend.services.training.training_service.DatasetModelSQL")
    def test_post_failed_training(self, mock_dataset_model, mock_completed_training, mock_boto3):
        """
        Tests if a failed training raises an error.
        """
        # Due to the missing mock of the MlflowAdapter and the missing environment variables,
        # for the init of the MlflowAdapter, this test will raise an error.
        with self.app.app_context():
            response = self.client.post("training/", json=self.params)
            data = json.loads(response.get_data(as_text=True))

            assert {
                "message": "Failed to start training because of internal error: "
                "Necessary environment variable 'MLFLOW_DEFAULT_EXPERIMENT' for defining "
                "the Mlflow default experiment is not set!"
            } == data

    @mock.patch("gts_backend.api.routes.training.TrainingService")
    def test_get_model_config_success(self, mock_training_config):
        """Test if model config is returned if request params are correct."""
        mock_training_config.return_value.retrieve_model_config.return_value = {"config": "mocked"}
        params = {
            "epochs": 5,
            "trainLabelStudioProjectIds": "1",
            "evalLabelStudioProjectIds": "1",
            "model": "yolox_nano",
            "classes": "test_class",
            "batchSize": 2,
            "modelName": "Test Model",
            "description": "This is just for tests.",
        }
        with self.app.app_context():
            response = self.client.get("training/config", query_string=params)
            data = json.loads(response.get_data(as_text=True))
            self.assertEqual(200, response.status_code)
            self.assertEqual({"config": "mocked"}, data)

            # the params be low are stringified in the query string and should be passed as lists to
            # the TrainingService
            expected_params = params
            expected_params["trainLabelStudioProjectIds"] = ["1"]
            expected_params["evalLabelStudioProjectIds"] = ["1"]
            expected_params["classes"] = ["test_class"]

            self.assertEqual(mock_training_config.call_args.kwargs["params"], params)

    @mock.patch("gts_backend.api.routes.training.TrainingService")
    def test_get_model_config_success_multiple_datasets_and_classes(self, mock_training_config):
        """Test if model config is returned if request params are correct and consist of multiple datasets and classes."""
        mock_training_config.return_value.retrieve_model_config.return_value = {"config": "mocked"}
        params = {
            "epochs": 5,
            "trainLabelStudioProjectIds": "1,2",
            "evalLabelStudioProjectIds": "1,2",
            "model": "yolox_nano",
            "classes": "test_class,other_class",
            "batchSize": 2,
            "modelName": "Test Model",
            "description": "This is just for tests.",
        }
        with self.app.app_context():
            response = self.client.get("training/config", query_string=params)
            data = json.loads(response.get_data(as_text=True))
            self.assertEqual(200, response.status_code)
            self.assertEqual({"config": "mocked"}, data)

            # the params be low are stringified in the query string and should be passed as lists to
            # the TrainingService
            expected_params = params
            expected_params["trainLabelStudioProjectIds"] = ["1", "2"]
            expected_params["evalLabelStudioProjectIds"] = ["1", "2"]
            expected_params["classes"] = ["test_class", "other_class"]

            self.assertEqual(mock_training_config.call_args.kwargs["params"], params)

    def test_get_model_config_fails_missing_param(self):
        """Test if correct status code is returned because of missing parameter."""
        params = {
            "epochs": 5,
        }
        with self.app.app_context():
            response = self.client.get("training/config", query_string=params)
            data = json.loads(response.get_data(as_text=True))

            assert {
                "message": "The browser (or proxy) sent a request that this server could not "
                "understand."
            } == data
            self.assertEqual(400, response.status_code)

    @mock.patch("gts_backend.api.routes.training.TrainingService")
    def test_get_model_config_fails_internal_error(self, mock_training_service):
        """Test if correct status code is returned in case of an internal server error."""
        mock_training_service.side_effect = Exception()
        params = {
            "epochs": 5,
            "trainLabelStudioProjectIds": "1,2",
            "evalLabelStudioProjectIds": "1,2",
            "model": "yolox_nano",
            "classes": "test_class,other_class",
            "batchSize": 2,
            "modelName": "Test Model",
            "description": "This is just for tests.",
        }
        with self.app.app_context():
            with self.assertRaises(Exception):
                response = self.client.get("training/config", query_string=params)
                self.assertEqual(500, response.status_code)

    def tearDown(self):
        # Reset variables to initial values
        for pair in zip(self.credentials, self.values):
            if pair[1] is not None:
                os.environ[pair[0]] = pair[1]
            else:
                del os.environ[pair[0]]


if __name__ == "__main__":
    unittest.main()
