# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the /model_testing REST Endpoints."""

import os
import unittest
from unittest import mock

from gevent import monkey
from mlflow.exceptions import RestException
from sqlalchemy.orm.exc import NoResultFound

from gts_backend.api.containers import Container
from gts_backend.app import Server
from gts_backend.services.labeling.service import LabelingService

monkey.patch_all = mock.MagicMock


class TestModelTestingAPI(unittest.TestCase):
    def setUp(self):
        self.server = Server()
        self.app = self.server.create_app(config="test")
        self.app.config["TESTING"] = True
        self.client = self.app.test_client()

        # Set dummy variables for test cases
        self.credentials = (
            "AWS_SECRET_ACCESS_KEY",
            "AWS_ACCESS_KEY_ID",
            "AWS_DEFAULT_REGION",
            "S3_ENDPOINT",
            "S3_BUCKET",
            "S3_REGION_NAME",
            "S3_ACCESS_KEY_ID",
            "S3_SECRET_ACCESS_KEY",
            "S3_ENDPOINT_EXTERNAL",
            "MLFLOW_S3_ENDPOINT_URL",
            "MLFLOW_SERVER_URL",
        )

        # save value, if credentials are already set
        self.values = []
        for credential in self.credentials:
            value = os.getenv(credential)
            if value is not None:
                self.values.append(value)
            else:
                self.values.append(None)

            # Temporarily set dummy value for test case
            os.environ[credential] = credential.lower()
            if credential == "S3_ENDPOINT":
                os.environ[credential] = "s3://endpoint.com/bucket-name/key/"

    @mock.patch("boto3.client")
    @mock.patch("gts_backend.services.jobs.prediction.job.PredictionJob.run")
    @mock.patch("gts_backend.api.routes.model_testing.DatasetModelSQL")
    def test_post_model_testing(self, mock_client, mock_job, mock_dataset_query):
        """Test if the POST request returns the correct response if posted data is correct."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.get_preview_images.return_value = [
            {"url": "image1.jpg?random-presigned-url-text12345"},
            {"url": "image2.jpg?random-presigned-url-text678910"},
        ]
        mock_job.return_value = ["image1_predicted.jpg", "image2_predicted.jpg"]

        post_request_data = {
            "model_name": "unqiue_test_model",
            "endpoint": "http://test_endpoint",
            "labelStudioProjectId": "123",
        }

        container = Container()

        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            response = self.client.post("/model_testing/", json=post_request_data)
            data = response.get_json()

            self.assertEqual(response.status_code, 200)
            expected_response = {
                "labelStudioProjectId": "123",
                "modelName": "unqiue_test_model",
                "images": [
                    {"url": "image1_predicted.jpg"},
                    {"url": "image2_predicted.jpg"},
                ],
            }
            self.assertEqual(data, expected_response)

    @mock.patch("boto3.client")
    @mock.patch("gts_backend.services.jobs.prediction.job.PredictionJob.run")
    @mock.patch("gts_backend.api.routes.model_testing.DatasetModelSQL")
    def test_post_model_testing_no_model(self, mock_client, mock_job, mock_dataset_query):
        """Test if the status code 404 is returned if the provided model is not found."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.get_preview_images.return_value = [
            {"url": "image1.jpg?random-presigned-url-text12345"},
            {"url": "image2.jpg?random-presigned-url-text678910"},
        ]
        mock_job.side_effect = RestException(json={})

        post_request_data = {
            "model_name": "unqiue_test_model",
            "endpoint": "http://test_endpoint",
            "labelStudioProjectId": "123",
        }

        container = Container()

        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            response = self.client.post("/model_testing/", json=post_request_data)

            self.assertEqual(
                404,
                response.status_code,
            )

    def test_post_model_testing_no_dataset(self):
        """Test if status code 404 is returned if the provided dataset is not found."""
        post_request_data = {
            "model_name": "unqiue_test_model",
            "endpoint": "http://test_endpoint",
            "labelStudioProjectId": "non existing id",
        }
        labeling_service_mock = mock.MagicMock(spec=LabelingService)

        container = Container()
        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            mock_query = mock.Mock()
            mock_query.filter.return_value.one.side_effect = NoResultFound()

            with mock.patch("gts_backend.model.dataset.DatasetModelSQL.query", new=mock_query):
                response = self.client.post("model_testing/", json=post_request_data)

                self.assertEqual(404, response.status_code)

    def test_post_model_testing_missing_param(self):
        """Test if status code 400 is returned if a parameter is missing."""
        post_request_data = {
            "model": "unqiue_test_model",
            "endpoint": "http://test_endpoint",  # missing dataset ID
        }

        with self.app.app_context():
            response = self.client.post("model_testing/", json=post_request_data)

            self.assertEqual(response.status_code, 400)

    def test_post_model_testing_general_exception(self):
        """Test if status code 500 is returned if an error occurs."""
        post_request_data = {
            "model": "unqiue_test_model",
            "endpoint": "http://test_endpoint",
            "labelStudioProjectId": "123",
        }
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.get_preview_images.return_value = [
            {"image_url": "image1.jpg"},
            {"image_url": "image2.jpg"},
        ]
        container = Container()

        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            mock_query = mock.Mock()
            mock_query.filter.return_value.one.side_effect = Exception("Test Exception")

            with mock.patch("gts_backend.model.dataset.DatasetModelSQL.query", new=mock_query):
                with self.assertRaises(Exception):
                    response = self.client.post("model_testing/", json=post_request_data)
                    self.assertEqual(response.status_code, 500)
                    data = response.get_json()
                    self.assertIn("Test Exception", data["message"])

    def tearDown(self):
        # Reset variables to initial values
        for pair in zip(self.credentials, self.values):
            if pair[1] is not None:
                os.environ[pair[0]] = pair[1]
            else:
                del os.environ[pair[0]]


if __name__ == "__main__":
    unittest.main()
