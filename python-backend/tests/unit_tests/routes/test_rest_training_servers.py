# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the /training_server REST Endpoints
"""

import json
import unittest
from unittest import mock

from gts_backend.app import Server
from gts_backend.services.dbmanager import db


class TestRestTrainingServer(unittest.TestCase):
    def setUp(self) -> None:
        self.server = Server
        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

        with self.app.app_context():
            db.session.execute("PRAGMA foreign_keys = ON;")
            db.session.commit()

    def tearDown(self) -> None:
        with self.app.app_context():
            db.session.remove()
            db.session.commit()

    @mock.patch("gts_backend.api.routes.training_servers.TrainingServerModelSQL")
    def test_get_servers_error(self, mock_sql):
        """
        Tests if status code 500 is returned for GET request if database query throws an exception
        """

        query_mock = mock_sql.query.all
        query_mock.side_effect = Exception()

        with self.app.app_context():
            with self.assertRaises(Exception):
                response = self.client.get("training-servers/")
                self.assertEqual(500, response.status_code)

    def test_get_servers_with_no_servers_added(self):
        """
        Tests if empty list is returned when no training server has previously been added
        """

        with self.app.app_context():
            response = self.client.get("training-servers/")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(200, response.status_code)
            self.assertEqual([], data)

    @mock.patch("gts_backend.api.routes.training_servers.TrainingServerModelSQL")
    def test_post_server_error(self, mock_sql):
        """
        Tests if status code 500 is returned for POST request if database query throws an exception
        """

        query_mock = mock_sql.query.with_entities.order_by.all
        query_mock.side_effect = Exception()

        server = {
            "endpoint": "https://dummy-server-1.com",
            "name": "First dummy server",
        }

        with self.app.app_context():
            with self.assertRaises(Exception):
                response = self.client.post("training-servers/", json=server)
                self.assertEqual(500, response.status_code)

    def test_add_single_server(self):
        """
        Tests if a single server is successfully added to the database. The following criteria must
        be met for a successful addition:
            - Response code equals 200 (For POST and GET request)
            - ServerInfo can be requested via GET request and contains information as expected
            - ServerID equals 0 since it is the first server added to the database
        """

        server = {
            "endpoint": "https://dummy-server-1.com",
            "name": "First dummy server",
        }

        server_info = {
            "id": 1,
            "endpoint": "https://dummy-server-1.com",
            "name": "First dummy server",
        }

        with self.app.app_context():
            response = self.client.post("training-servers/", json=server)
            self.assertEqual(201, response.status_code)

            response = self.client.get("training-servers/")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(200, response.status_code)
            self.assertEqual([server_info], data)

    def test_add_multiple_servers(self):
        """
        Tests if multiple servers are successfully added to the database. The following criteria
        must be met for this:
            - Equal number of added servers and received ServerInformation
            - Received ServerIDs must match number of added servers
        """

        server_1 = {
            "endpoint": "https://dummy-server-1.com",
            "name": "First dummy server",
        }

        server_2 = {
            "endpoint": "https://another-test-server.com",
            "name": "Another test server",
        }

        server_3 = {
            "endpoint": "https://also-a-test.com",
            "name": "Also a test server",
        }

        with self.app.app_context():
            for server in (server_1, server_2, server_3):
                response = self.client.post("training-servers/", json=server)

                self.assertEqual(201, response.status_code)

            response = self.client.get("training-servers/")
            data = json.loads(response.get_data(as_text=True))

            # Check that we got three ServerInfos for three servers added
            self.assertEqual(3, len(data))

            # Check that the ServerIDs match the number of added servers
            for index, server_info in enumerate(data):
                self.assertEqual(index + 1, server_info["id"])

    def test_get_information_for_added_server(self):
        """
        Tests if ServerInfo for single added server is returned. For this the following criteria
        must be met:
            - Server is successfully added to database
            - GET request for added server returns the expected ServerInfomn
        """

        server = {
            "endpoint": "https://dummy-server-1.com",
            "name": "First dummy server",
        }

        server_info = {
            "id": 1,
            "endpoint": "https://dummy-server-1.com",
            "name": "First dummy server",
        }

        with self.app.app_context():
            response = self.client.post("training-servers/", json=server)
            self.assertEqual(201, response.status_code)

            response = self.client.get("training-servers/1")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(server_info, data)

    def test_delete_server_from_database(self):
        """
        Tests if a single server is successfully deleted from database. For this the following
        criteria must be met:
            - Initially server must be added to database
            - List of added servers must contain one added server
            - Server must be deleted via POST request
            - List of added servers must be empty afterwards
        """

        server = {
            "endpoint": "https://dummy-server-1.com",
            "name": "First dummy server",
        }

        with self.app.app_context():
            response = self.client.post("training-servers/", json=server)
            self.assertEqual(201, response.status_code)

            response = self.client.get("training-servers/")
            self.assertEqual(1, len(json.loads(response.get_data(as_text=True))))

            response = self.client.delete("training-servers/1")
            self.assertEqual(200, response.status_code)

            response = self.client.get("training-servers/")
            self.assertEqual(0, len(json.loads(response.get_data(as_text=True))))

    def test_get_server_no_result_found_error(self):
        """
        Tests if status code 404 is returned for GET request if database query throws an
        NoResultFoundError
        """

        with self.app.app_context():
            response = self.client.get("training-servers/0")

            self.assertEqual(404, response.status_code)

    @mock.patch("gts_backend.api.routes.training_servers.TrainingServerModelSQL")
    def test_delete_server_exception_error(self, mock_sql):
        """
        Tests if status code 500 is returned for DELETE request if database query throws an
        exception
        """

        query_mock = mock_sql.query.filter
        query_mock.side_effect = Exception()

        with self.app.app_context():
            with self.assertRaises(Exception):
                response = self.client.delete("training-servers/0")
                self.assertEqual(500, response.status_code)


if __name__ == "__main__":
    unittest.main()
