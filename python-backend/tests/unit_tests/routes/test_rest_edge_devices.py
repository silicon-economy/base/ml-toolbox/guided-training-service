# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the edge device endpoints."""
from datetime import datetime

import pytest

from gts_backend.model.edge_device import EdgeDeviceModelSQL


@pytest.fixture(name="test_app")
def test_app_fixture(single_edge_device):
    """Fixture for a test app."""
    from gts_backend.app import Server

    app = Server.create_app(config="test")

    yield app

    del app


@pytest.fixture(name="single_edge_device")
def single_edge_device_fixture():
    """Fixture for a single edge device."""
    return EdgeDeviceModelSQL(
        id=1,
        is_recording=False,
        uri="test_uri",
        name="test_name",
        device_type="test_device_type",
        creation_date=datetime.now(),
    )


@pytest.fixture(name="multiple_edge_devices")
def multiple_edge_devices_fixture(single_edge_device):
    """Fixture for multiple edge devices."""
    return [
        single_edge_device,
        EdgeDeviceModelSQL(
            id=2,
            is_recording=True,
            uri="test_uri_2",
            name="test_name_2",
            device_type="test_device_type_2",
            creation_date=datetime.now(),
        ),
    ]


def test_get_edge_devices(test_app, multiple_edge_devices, mocker):
    """Test the get edge devices endpoint."""
    client = test_app.test_client()
    with test_app.app_context():

        mocker.patch(
            "gts_backend.services.edge_devices.edge_device_service.EdgeDeviceService.load_edge_devices",
            return_value=[multiple_edge_devices],
        )

        response = client.get("/edge-devices/")

    expected_device_1 = {
        "uri": "test_uri",
        "name": "test_name",
        "deviceType": "test_device_type",
        "creationDate": multiple_edge_devices[0].creation_date.isoformat(),
        "is_recording": False,
        "stream": None,
        "id": 1,
    }
    expected_device_2 = {
        "uri": "test_uri_2",
        "name": "test_name_2",
        "deviceType": "test_device_type_2",
        "creationDate": multiple_edge_devices[1].creation_date.isoformat(),
        "is_recording": True,
        "stream": None,
        "id": 2,
    }

    assert response.status_code == 200
    assert response.json[0][0] == expected_device_1
    assert response.json[0][1] == expected_device_2


def test_add_edge_device(test_app, mocker, single_edge_device):
    """Test the add edge device endpoint."""
    client = test_app.test_client()
    with test_app.app_context():

        mocker.patch(
            "gts_backend.services.edge_devices.edge_device_service.EdgeDeviceService.add_edge_device",
            return_value=single_edge_device,
        )

        response = client.post(
            "/edge-devices/",
            json={
                "uri": single_edge_device.uri,
                "name": single_edge_device.name,
                "deviceType": single_edge_device.device_type,
            },
        )

    expected_device = {
        "uri": "test_uri",
        "name": "test_name",
        "deviceType": "test_device_type",
        "creationDate": single_edge_device.creation_date.isoformat(),
        "is_recording": False,
        "stream": None,
        "id": 1,
    }

    assert response.status_code == 201
    assert response.json == expected_device
