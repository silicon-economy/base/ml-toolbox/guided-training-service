# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Tests for the /datasets REST Endpoints
"""
import asyncio
import json
import os
import unittest
from datetime import datetime
from unittest import mock

import pytest
import requests
from gevent import monkey
from label_studio_sdk import Client, Project
from mypy_boto3_s3 import S3Client
from mypy_boto3_sts import STSClient

from gts_backend.api.containers import Container
from gts_backend.api.routes.datasets import (
    _merge_dataset_and_ls_project,
    get_all_ls_projects,
)
from gts_backend.model.dataset import DatasetModelSQL, dataset_schema
from gts_backend.services.dbmanager import db
from gts_backend.services.labeling.service import LabelingService

monkey.patch_all = mock.MagicMock
# Put this import below the above patch to resolve RecursionError
from gts_backend.app import Server


class TestRestDatasetsList(unittest.TestCase):
    def set_os_env_values(self):
        credentials = ("LABEL_STUDIO_API_KEY", "LABEL_STUDIO_ENDPOINT")
        old_values = {}
        for credential in credentials:
            value = os.getenv(credential)
            if value is not None:
                old_values[credential] = value
            else:
                old_values[credential] = None

            # Temporarily set dummy value for test case
            os.environ[credential] = credential.lower()

        return old_values

    def reset_os_env_values(self, old_values):
        for cred, value in old_values.items():
            if value is not None:
                os.environ[cred] = value
            else:
                del os.environ[cred]

    def setUp(self):
        self.server = Server()

        self.container = Container()
        self.container.init_resources()

        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

        with self.app.app_context():
            db.session.execute("PRAGMA foreign_keys = ON;")
            db.session.commit()

        self.old_values = self.set_os_env_values()

    def tearDown(self):
        self.reset_os_env_values(self.old_values)
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def _get_test_datasets(self) -> list[DatasetModelSQL]:
        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        dataset2 = DatasetModelSQL(
            ls_project_id=2,
            name="dataset2",
            s3_prefix="project_2_uuid",
            task_type="classification",
            labels="classification",
            last_modified=datetime.fromisoformat("2022-05-05T12:00:00"),
            tags="public",
        )

        return [dataset1, dataset2]

    def test_returns_empty_response_for_no_datasets_in_db(self):
        """Test if status code 200 and expected response is returned if there is no dataset."""
        ls_client_mock = mock.MagicMock(spec=Client)

        with self.app.app_context(), self.container.ls_client.override(ls_client_mock):
            response = self.client.get("datasets/")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["page"], 1)
            self.assertEqual(data["page_count"], 0)
            self.assertEqual(data["total_items"], 0)
            self.assertEqual(data["per_page"], 10)
            self.assertEqual(data["datasets"], [])
            ls_client_mock.get_project.assert_not_called()

    @mock.patch("gts_backend.api.routes.datasets.get_all_ls_projects")
    def test_returns_single_dataset(self, mock_get_projects):
        """Test if status code 200 and a single dataset is returned if there is only one dataset."""
        mock_get_projects.return_value = [
            {"id": 1, "title": "dataset1", "task_number": 5, "num_tasks_with_annotations": 4},
        ]

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            db.session.add(self._get_test_datasets()[0])
            db.session.commit()

            response = self.client.get("datasets/")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["page"], 1)
            self.assertEqual(data["page_count"], 1)
            self.assertEqual(data["total_items"], 1)
            self.assertEqual(data["per_page"], 10)

            expected_datasets = [
                {
                    "ls_project_id": "1",
                    "name": "dataset1",
                    "type": "object_detection",
                    "labels": "object_detection",
                    "tags": "private",
                    "last_modified": "2022-01-01T12:00:00",
                    "s3_prefix": "project_1_uuid",
                    "total_images": 5,
                    "annotated_images": 4,
                    "preview_images": None,
                    "ls_url": "https://example.org/projects/1/data",
                }
            ]
            self.assertEqual(data["datasets"], expected_datasets)

    @mock.patch("gts_backend.api.routes.datasets.get_all_ls_projects")
    def test_returns_multiple_sorted_datasets_by_last_modified_date(self, mock_get_projects):
        """Test if status code 200 and multiple sorted datasets by last modified date are returned."""
        mock_get_projects.return_value = [
            {"id": 1, "title": "dataset1", "task_number": 5, "num_tasks_with_annotations": 4},
            {"id": 2, "title": "dataset2", "task_number": 3, "num_tasks_with_annotations": 3},
        ]

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            for ds in self._get_test_datasets():
                db.session.add(ds)
            db.session.commit()

            response = self.client.get("datasets/")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["page"], 1)
            self.assertEqual(data["page_count"], 1)
            self.assertEqual(data["total_items"], 2)
            self.assertEqual(data["per_page"], 10)

            expected_datasets = [
                {
                    "ls_project_id": "1",
                    "name": "dataset1",
                    "type": "object_detection",
                    "labels": "object_detection",
                    "tags": "private",
                    "last_modified": "2022-01-01T12:00:00",
                    "s3_prefix": "project_1_uuid",
                    "total_images": 5,
                    "annotated_images": 4,
                    "preview_images": None,
                    "ls_url": "https://example.org/projects/1/data",
                },
                {
                    "ls_project_id": "2",
                    "name": "dataset2",
                    "type": "classification",
                    "labels": "classification",
                    "tags": "public",
                    "last_modified": "2022-05-05T12:00:00",
                    "s3_prefix": "project_2_uuid",
                    "total_images": 3,
                    "annotated_images": 3,
                    "preview_images": None,
                    "ls_url": "https://example.org/projects/2/data",
                },
            ]
            self.assertEqual(data["datasets"], expected_datasets)

    @mock.patch("gts_backend.api.routes.datasets.get_all_ls_projects")
    def test_returns_multiple_sorted_datasets_by_name_descending(self, mock_get_projects):
        """Test if status code 200 multiple sorted datasets descending by name are returned."""
        mock_get_projects.return_value = [
            {"id": 2, "title": "dataset2", "task_number": 3, "num_tasks_with_annotations": 3},
            {"id": 1, "title": "dataset1", "task_number": 5, "num_tasks_with_annotations": 4},
        ]

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            for ds in self._get_test_datasets():
                db.session.add(ds)
            db.session.commit()

            response = self.client.get("datasets/?order_by=name&direction=desc")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["page"], 1)
            self.assertEqual(data["page_count"], 1)
            self.assertEqual(data["total_items"], 2)
            self.assertEqual(data["per_page"], 10)

            expected_datasets = [
                {
                    "ls_project_id": "2",
                    "name": "dataset2",
                    "type": "classification",
                    "labels": "classification",
                    "tags": "public",
                    "last_modified": "2022-05-05T12:00:00",
                    "s3_prefix": "project_2_uuid",
                    "total_images": 3,
                    "annotated_images": 3,
                    "preview_images": None,
                    "ls_url": "https://example.org/projects/2/data",
                },
                {
                    "ls_project_id": "1",
                    "name": "dataset1",
                    "type": "object_detection",
                    "labels": "object_detection",
                    "tags": "private",
                    "last_modified": "2022-01-01T12:00:00",
                    "s3_prefix": "project_1_uuid",
                    "total_images": 5,
                    "annotated_images": 4,
                    "preview_images": None,
                    "ls_url": "https://example.org/projects/1/data",
                },
            ]
            self.assertEqual(data["datasets"], expected_datasets)

    @mock.patch("gts_backend.api.routes.datasets.get_all_ls_projects")
    def test_returns_multiple_sorted_filterd_datasets(self, mock_get_projects):
        """Test if status code 200 and single dataset matching the filter are returned."""
        mock_get_projects.return_value = [
            {"id": 2, "title": "dataset2", "task_number": 3, "num_tasks_with_annotations": 3},
        ]

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            for ds in self._get_test_datasets():
                db.session.add(ds)
            db.session.commit()

            response = self.client.get("datasets/?filter=public")  # match public
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["page"], 1)
            self.assertEqual(data["page_count"], 1)
            self.assertEqual(data["total_items"], 1)
            self.assertEqual(data["per_page"], 10)

            expected_datasets = [
                {
                    "ls_project_id": "2",
                    "name": "dataset2",
                    "type": "classification",
                    "labels": "classification",
                    "tags": "public",
                    "last_modified": "2022-05-05T12:00:00",
                    "s3_prefix": "project_2_uuid",
                    "total_images": 3,
                    "annotated_images": 3,
                    "preview_images": None,
                    "ls_url": "https://example.org/projects/2/data",
                }
            ]
            self.assertEqual(data["datasets"], expected_datasets)

    @mock.patch("gts_backend.api.routes.datasets.get_all_ls_projects")
    def test_returns_multiple_sorted_paginated_datasets(self, mock_get_projects):
        """Test if status code 200 and a single dataset are returned if only one dataset should be
        displayed per page."""
        mock_get_projects.return_value = [
            {"id": 2, "title": "dataset2", "task_number": 3, "num_tasks_with_annotations": 3},
        ]

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            for ds in self._get_test_datasets():
                db.session.add(ds)
            db.session.commit()

            response = self.client.get("datasets/?page=2&per_page=1")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["page"], 2)
            self.assertEqual(data["page_count"], 2)
            self.assertEqual(data["total_items"], 2)
            self.assertEqual(data["per_page"], 1)

            expected_datasets = [
                {
                    "ls_project_id": "2",
                    "name": "dataset2",
                    "type": "classification",
                    "labels": "classification",
                    "tags": "public",
                    "last_modified": "2022-05-05T12:00:00",
                    "s3_prefix": "project_2_uuid",
                    "total_images": 3,
                    "annotated_images": 3,
                    "preview_images": None,
                    "ls_url": "https://example.org/projects/2/data",
                },
            ]
            self.assertEqual(data["datasets"], expected_datasets)
            response = self.client.get("datasets/?page=2&per_page=1")
            data = json.loads(response.get_data(as_text=True))

    def test_get_datasets_internal_error(self):
        """Test if status code 500 is returned if an error is raised."""
        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.get_preview_images.return_value = []
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        old_value = os.environ["LABEL_STUDIO_API_KEY"]
        del os.environ["LABEL_STUDIO_API_KEY"]

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            for ds in self._get_test_datasets():
                db.session.add(ds)
            db.session.commit()

            with self.assertRaises(ValueError):
                response = self.client.get("datasets/?page=2&per_page=1")

                self.assertEqual(response.status_code, 500)

        os.environ["LABEL_STUDIO_API_KEY"] = old_value


class TestAsyncFunctions(unittest.IsolatedAsyncioTestCase):
    async def mock_get_response(self, *args, **kwargs):
        response = mock.AsyncMock()
        response.text.return_value = '{"id": 1, "title": "Test Dataset 1", "task_number": 5, "num_tasks_with_annotations": 4}'
        return response

    @pytest.mark.asyncio
    @mock.patch("aiohttp.ClientSession")
    def test_get_all_ls_projects(self, mock_client_session):
        """Test if all LabelStudio projects are get successfully."""
        mock_session_instance = mock.Mock()
        mock_client_session.return_value.__aenter__.return_value = mock_session_instance
        mock_session_instance.get.side_effect = self.mock_get_response

        project_ids = [1, 2, 3]
        auth_token = "test-token"
        ls_url = "http://test-ls-url"

        project_infos = asyncio.run(get_all_ls_projects(project_ids, auth_token, ls_url))

        self.assertEqual(len(project_infos), 3)
        self.assertEqual(project_infos[0]["id"], 1)
        self.assertEqual(project_infos[0]["title"], "Test Dataset 1")
        self.assertEqual(project_infos[0]["task_number"], 5)
        self.assertEqual(project_infos[0]["num_tasks_with_annotations"], 4)
        self.assertTrue(project_infos[0] == project_infos[1] == project_infos[2])

    async def mock_get_corrupt_response(self, *args, **kwargs):
        response = mock.AsyncMock()
        response.text.return_value = '{"status_code": 404}'
        return response

    @pytest.mark.asyncio
    @mock.patch("aiohttp.ClientSession")
    def test_get_all_ls_projects_corrupt_dataset(self, mock_client_session):
        """Test if all corrupt datasets are returned correctly."""
        mock_session_instance = mock.Mock()
        mock_client_session.return_value.__aenter__.return_value = mock_session_instance
        mock_session_instance.get.side_effect = self.mock_get_corrupt_response

        project_ids = [1]
        auth_token = "test-token"
        ls_url = "http://test-ls-url"

        project_infos = asyncio.run(get_all_ls_projects(project_ids, auth_token, ls_url))

        self.assertEqual(len(project_infos), 1)
        self.assertEqual(project_infos[0]["id"], 1)
        self.assertEqual(project_infos[0]["title"], "CORRUPTED DATASET")
        self.assertEqual(project_infos[0]["task_number"], -1)
        self.assertEqual(project_infos[0]["num_tasks_with_annotations"], -1)


class TestRestDatasets(unittest.TestCase):
    def setUp(self):
        self.server = Server()

        self.container = Container()
        self.container.init_resources()

        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

        with self.app.app_context():
            db.session.execute("PRAGMA foreign_keys = ON;")
            db.session.commit()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def test_merge_dataset_and_ls_project(self):
        def json_func():
            return {
                "task_number": 1,
                "num_tasks_with_annotations": 0,
                "id": 1,
                "title": "dataset1",
            }

        ls_project_mock = mock.MagicMock(Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "dataset1"
        ls_project_mock.get_params.return_value = json_func()

        s3_client_mock = mock.MagicMock(S3Client)
        s3_client_mock.list_objects_v2.return_value = {
            "Contents": [
                {
                    "Key": "guided-training-service/image1.jpg",
                },
                {
                    "Key": "guided-training-service/image2.jpg",
                },
            ]
        }
        s3_client_mock.generate_presigned_url.side_effect = [
            "http://s3:9000/guided-training-service/image1.jpg",
            "http://s3:9000/guided-training-service/image2.jpg",
        ]

        labeling_service_mock = LabelingService(
            mock.Mock(Client),
            s3_client_mock,
            mock.Mock(STSClient),
            "guided-training-service",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )
        labeling_service_mock.ls_client.url = "https://example.org"

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            merged_dataset = _merge_dataset_and_ls_project(
                dataset_schema.dump(dataset1),
                ls_project_mock.get_params(),
                labeling_service_mock,
                preview_images=True,
            )

            self.assertEqual(merged_dataset["name"], "dataset1")
            self.assertEqual(merged_dataset["total_images"], 1)
            self.assertEqual(merged_dataset["annotated_images"], 0)
            self.assertEqual(
                merged_dataset["preview_images"],
                [
                    {"url": "https://s3.example.com:443/guided-training-service/image1.jpg"},
                    {"url": "https://s3.example.com:443/guided-training-service/image2.jpg"},
                ],
            )

    def test_get_returns_single_dataset(self):
        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "id": 1,
                "title": "dataset1",
            }

        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "dataset1"
        ls_project_mock.get_params.return_value = json_func()

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.get_project.return_value = ls_project_mock
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.get_preview_images.return_value = []
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get("datasets/1")
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)

            expected_dataset = {
                "ls_project_id": "1",
                "name": "dataset1",
                "type": "object_detection",
                "labels": "object_detection",
                "tags": "private",
                "last_modified": "2022-01-01T12:00:00",
                "s3_prefix": "project_1_uuid",
                "total_images": 0,
                "annotated_images": 0,
                "preview_images": [],
                "ls_url": "https://example.org/projects/1/data",
            }

            self.assertEqual(data, expected_dataset)
            ls_client_mock.get_project.assert_called_once_with(1)

    def test_get_returns_404_if_dataset_is_not_found(self):
        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "id": 1,
                "title": "dataset1",
            }

        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "dataset1"
        ls_project_mock.make_request.return_value = mock.Mock(status_code=200, json=json_func)

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.get_project.return_value = ls_project_mock

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.get_preview_images.return_value = []

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get("datasets/321")

            self.assertEqual(response.status_code, 404)
            ls_client_mock.get_project.assert_not_called()

    def test_deletes_dataset(self):
        labeling_service_mock = mock.MagicMock(spec=LabelingService)

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.delete("datasets/1")

            self.assertEqual(response.status_code, 200)
            labeling_service_mock.delete_project.assert_called_once_with(1, "project_1_uuid")
            self.assertEqual(
                db.session.query(DatasetModelSQL)
                .where(DatasetModelSQL.ls_project_id == 1)
                .count(),
                0,
            )

    def test_delete_returns_404_if_dataset_is_not_found(self):
        labeling_service_mock = mock.MagicMock(spec=LabelingService)

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.delete("datasets/321")

            self.assertEqual(response.status_code, 404)
            labeling_service_mock.delete_project.assert_not_called()
            self.assertEqual(
                db.session.query(DatasetModelSQL)
                .where(DatasetModelSQL.ls_project_id == 1)
                .count(),
                1,
            )

    # TODO: add update of existing dataset via put

    @mock.patch("gts_backend.api.routes.datasets.get_all_ls_projects")
    def test_update_dataset_with_label_config(self, get_all_ls_projects_mock):
        """Test if status code 200 and the updated dataset are returned if the update succeeds."""

        def json_func():
            return {
                "label_config": "some_config",
                "id": 1,
                "task_number": 0,
                "num_tasks_with_annotations": 0,
            }

        presigned_url = "http://some_url"
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "dataset1"
        ls_project_mock.get_params.return_value = json_func()

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.url = "http://test.com"
        ls_client_mock.get_project.return_value = ls_project_mock
        get_all_ls_projects_mock.return_value = [
            {"id": 1, "title": "dataset1", "task_number": 0, "num_tasks_with_annotations": 0},
        ]

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.get_preview_images.return_value = None
        response_set_framerate = requests.Response()
        response_set_framerate.status_code = 200
        labeling_service_mock.set_framerate_labelstudio_project_template.return_value = (
            response_set_framerate
        )
        labeling_service_mock.ls_client = ls_client_mock
        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_tracking",
            labels="label1",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):
            db.session.add(dataset1)
            db.session.commit()

            body = {"url": presigned_url}
            response = self.client.post(
                "datasets/1/label-config-update",
                data=json.dumps(body),
                content_type="application/json",
            )
            data = json.loads(response.get_data(as_text=True))

            expected_dataset = {
                "ls_project_id": "1",
                "name": "dataset1",
                "type": "object_tracking",
                "labels": "label1",
                "tags": "private",
                "last_modified": "2022-01-01T12:00:00",
                "s3_prefix": "project_1_uuid",
                "total_images": 0,
                "annotated_images": 0,
                "preview_images": None,
                "ls_url": "http://test.com/projects/1/data",
            }

            self.assertEqual(response.status_code, response_set_framerate.status_code)
            self.assertEqual(data, expected_dataset)
            labeling_service_mock.set_framerate_labelstudio_project_template.assert_called_once_with(
                project_id=ls_project_mock.id, presigned_url_video_file=presigned_url
            )
            get_all_ls_projects_mock.assert_called_once()

    @mock.patch("gts_backend.api.routes.datasets.get_all_ls_projects")
    def test_update_dataset_with_label_config_no_dataset(self, get_all_ls_projects_mock):
        """Test if status code 404 is returned if the dataset with given ID is not found."""

        def json_func():
            return {
                "label_config": "some_config",
                "id": 1,
                "task_number": 0,
                "num_tasks_with_annotations": 0,
            }

        presigned_url = "http://some_url"
        ls_project_mock = mock.MagicMock(spec=Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "dataset1"
        ls_project_mock.get_params.return_value = json_func()

        ls_client_mock = mock.MagicMock(spec=Client)

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.ls_client = ls_client_mock

        with self.app.app_context(), self.container.ls_client.override(
            ls_client_mock
        ), self.container.labeling_service.override(labeling_service_mock):

            body = {"url": presigned_url}
            response = self.client.post(
                "datasets/23/label-config-update",
                data=json.dumps(body),
                content_type="application/json",
            )

            self.assertEqual(response.status_code, 404)

    def test_update_dataset_with_label_config_missing_parameter(
        self,
    ):
        """Test if status code 400 is returned if a parameter is missing."""

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_tracking",
            labels="label1",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        labeling_service_mock = mock.MagicMock(spec=LabelingService)

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.post(
                "datasets/1/label-config-update",
                data=json.dumps({}),  # no parameter
                content_type="application/json",
            )

            self.assertEqual(response.status_code, 400)

    def test_post_creates_new_dataset(self):
        def json_func():
            return {
                "task_number": 0,
                "num_tasks_with_annotations": 0,
                "id": 1,
                "title": "dataset1",
            }

        ls_project_mock = mock.MagicMock(Project)
        ls_project_mock.id = 1
        ls_project_mock.title = "dataset1"
        ls_project_mock.get_params.return_value = json_func()

        ls_client_mock = mock.MagicMock(spec=Client)
        ls_client_mock.get_project.return_value = ls_project_mock
        ls_client_mock.configure_mock(url="https://example.org")

        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.initialize_project.return_value = (
            ls_project_mock,
            "project_uuid",
        )
        labeling_service_mock.attach_mock(ls_client_mock, "ls_client")

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            body = {
                "name": "dataset1",
                "type": "object_detection",
                "labels": '["test_label"]',
                "tags": "private",
            }

            response = self.client.post(
                "datasets/", data=json.dumps(body), content_type="application/json"
            )
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 201)
            labeling_service_mock.initialize_project.assert_called_with(
                dataset_name="dataset1", task_type="object_detection", labels=["test_label"]
            )
            self.assertEqual(data["name"], "dataset1")
            self.assertEqual(data["total_images"], 0)
            self.assertEqual(data["annotated_images"], 0)
            self.assertEqual(data["preview_images"], None)

    def test_generate_url_post(self):
        """Test if status code 200 and a presigned url are returned if the generation succeeds."""
        s3_client_mock = mock.MagicMock(S3Client)
        s3_client_mock.generate_presigned_url.return_value = (
            "http://s3:9000/guided-training-service/image.jpg"
        )
        labeling_service_mock = LabelingService(
            mock.Mock(Client),
            s3_client_mock,
            mock.Mock(STSClient),
            "guided-training-service",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.post(
                "datasets/1/data/url-generation",
                data=json.dumps({"file_name": "image.jpg"}),
                content_type="application/json",
            )
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                data["url"],
                "https://s3.example.com:443/guided-training-service/image.jpg",
            )

    def test_generate_url_fails_missing_parameter(self):
        """Test if status code 400 is returned if a parameter is missing."""
        s3_client_mock = mock.MagicMock(S3Client)
        s3_client_mock.generate_presigned_url.return_value = (
            "http://s3:9000/guided-training-service/image.jpg"
        )
        labeling_service_mock = LabelingService(
            mock.Mock(Client),
            s3_client_mock,
            mock.Mock(STSClient),
            "guided-training-service",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.post(
                "datasets/1/data/url-generation",
                data=json.dumps({}),  # no parameter
                content_type="application/json",
            )

            self.assertEqual(response.status_code, 400)

    def test_generate_url_post_returns_404_if_dataset_is_not_found(self):
        """Test if status code 404 is returned if the given dataset is not found."""
        s3_client_mock = mock.MagicMock(S3Client)
        s3_client_mock.generate_presigned_url.return_value = (
            "http://s3:9000/guided-training-service/image.jpg"
        )
        labeling_service_mock = LabelingService(
            mock.Mock(Client),
            s3_client_mock,
            mock.Mock(STSClient),
            "guided-training-service",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.post(
                "datasets/123/data/url-generation",
                data=json.dumps({"file_name": "image.jpg"}),
                content_type="application/json",
            )

            self.assertEqual(response.status_code, 404)

    # TODO: refactor copy code for url generation to avoid code duplication
    def test_generate_url_get(self):
        """Test if status code 200 and a presigned url are returned if the generation succeeds.."""
        s3_client_mock = mock.MagicMock(S3Client)
        s3_client_mock.generate_presigned_url.return_value = (
            "http://s3:9000/guided-training-service/image.jpg"
        )
        labeling_service_mock = LabelingService(
            mock.Mock(Client),
            s3_client_mock,
            mock.Mock(STSClient),
            "guided-training-service",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get(
                "datasets/1/data/url-generation/image.jpg",
            )
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                data["url"],
                "https://s3.example.com:443/guided-training-service/image.jpg",
            )

    def test_generate_url_get_returns_404_if_dataset_is_not_found(self):
        """Test if status code 404 is returned if the given dataset is not found."""
        s3_client_mock = mock.MagicMock(S3Client)
        s3_client_mock.generate_presigned_url.return_value = (
            "http://s3:9000/guided-training-service/image.jpg"
        )
        labeling_service_mock = LabelingService(
            mock.Mock(Client),
            s3_client_mock,
            mock.Mock(STSClient),
            "guided-training-service",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            False,
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get(
                "datasets/123/data/url-generation/image.jpg",
            )

            self.assertEqual(response.status_code, 404)

    def test_export_annotation_object_detection(self):
        """Test if status code 200 and expected object detection response is returned if
        export succeeds."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.export_annotations_to_s3.return_value = (
            "http://s3:9000/guided-training-service/annotated-data.zip"
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get(
                "datasets/1/annotation-export",
            )
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                data["url"],
                "http://s3:9000/guided-training-service/annotated-data.zip",
            )
            labeling_service_mock.export_annotations_to_s3.assert_called_once_with(
                project_id=1,
                s3_prefix="project_1_uuid",
                filename="dataset1_" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            )

    def test_export_annotation_classification(self):
        """Test if status code 200 and expected classification response is returned if export
        succeeds."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.export_annotations_to_s3.return_value = (
            "http://s3:9000/guided-training-service/annotated-data.zip"
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="classification",
            labels="classification",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            # act: object_detection
            response = self.client.get(
                "datasets/1/annotation-export",
            )
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                data["url"],
                "http://s3:9000/guided-training-service/annotated-data.zip",
            )
            labeling_service_mock.export_annotations_to_s3.assert_called_once_with(
                project_id=1,
                s3_prefix="project_1_uuid",
                filename="dataset1_" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            )

    def test_export_annotation_returns_404_if_dataset_is_not_found(self):
        """Test if status code 404 is returned if dataset is not found."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.export_annotations_to_s3.return_value = (
            "http://s3:9000/guided-training-service/annotated-data.zip"
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get(
                "datasets/123/annotation-export",
            )

            self.assertEqual(response.status_code, 404)
            labeling_service_mock.export_annotations_to_s3.assert_not_called()

    def test_sync_storage(self):
        """Test if status code 200 and expected response is returned if synchronization succeeds."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.sync_all_s3_cloud_storages.return_value = 42

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get(
                "datasets/1/synchronization",
            )
            data = json.loads(response.get_data(as_text=True))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["imported_items"], 42)
            labeling_service_mock.extract_all_images_from_zips.assert_called_once_with(
                "project_1_uuid"
            )
            labeling_service_mock.sync_all_s3_cloud_storages.assert_called_once_with(1)

    def test_import_annotation(self):
        """Test if status code 200 is returned if annotation import succeeds."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.import_ls_annotation.return_value = None

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get("datasets/1/annotation-import")

            self.assertEqual(response.status_code, 200)
            labeling_service_mock.import_ls_annotation.assert_called_once_with(1, "project_1_uuid")

    def test_import_annotation_returns_404_if_dataset_is_not_found(self):
        """Test if status code 404 is returned if annotation import fails because the dataset
        is not found."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)
        labeling_service_mock.import_ls_annotation.return_value = None

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.get("datasets/123/annotation-import")

            self.assertEqual(response.status_code, 404)
            labeling_service_mock.import_ls_annotation.assert_not_called()

    def test_remove_file(self):
        """Test if status code 200 is returned if file removal succeeds."""
        labeling_service_mock = mock.MagicMock(spec=LabelingService)

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.delete("datasets/1/data/image.jpg/file-removal")

            self.assertEqual(response.status_code, 200)
            labeling_service_mock.delete_file_from_s3.assert_called_once_with(
                s3_prefix="project_1_uuid", s3_folder="data", file_name="image.jpg"
            )

    def test_remove_file_returns_404_if_dataset_is_not_found(self):
        "Test if status code 404 is returned if dataset to remove file from is not found."
        labeling_service_mock = mock.MagicMock(spec=LabelingService)

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.delete("datasets/123/data/image.jpg/file-removal")

            self.assertEqual(response.status_code, 404)
            labeling_service_mock.delete_file_from_s3.assert_not_called()


class TestRestAutoAnnotateDatasets(unittest.TestCase):
    def setUp(self):
        self.server = Server()

        self.container = Container()
        self.container.init_resources()

        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

        with self.app.app_context():
            db.session.execute("PRAGMA foreign_keys = ON;")
            db.session.commit()

    def set_os_env_values(self):
        credentials = (
            "AWS_SECRET_ACCESS_KEY",
            "AWS_ACCESS_KEY_ID",
            "S3_ENDPOINT",
            "S3_BUCKET",
            "S3_ACCESS_KEY_ID",
            "S3_SECRET_ACCESS_KEY",
            "S3_ENDPOINT_EXTERNAL",
            "S3_REGION_NAME",
            "MLFLOW_S3_ENDPOINT_URL",
            "MLFLOW_SERVER_URL",
            "MLFLOW_DEFAULT_EXPERIMENT",
            "AWS_DEFAULT_REGION",
        )
        old_values = {}
        for credential in credentials:
            value = os.getenv(credential)
            if value is not None:
                old_values[credential] = value
            else:
                old_values[credential] = None

            # Temporarily set dummy value for test case
            os.environ[credential] = credential.lower()
            if credential == "S3_ENDPOINT":
                os.environ[credential] = "s3://s3-endpoint.com/bucket/key/"

        return old_values

    def reset_os_env_values(self, old_values):
        for cred, value in old_values.items():
            if value is not None:
                os.environ[cred] = value
            else:
                del os.environ[cred]

    @mock.patch("boto3.client")
    @mock.patch("gts_backend.api.routes.datasets.AutoAnnotationJob.run")
    def test_post_auto_annotation(self, mock_job, mock_client):
        """Test if status code 200 and expected response is returned if auto annotation succeeds."""
        # temporarily set env variables for the test; save the old ones to reset later
        old_envs = self.set_os_env_values()

        labeling_service_mock = LabelingService(
            ls_client=mock.Mock(spec=Client),
            s3_client=mock.MagicMock(S3Client),
            sts_client=mock.Mock(STSClient),
            s3_bucket_name="test_bucket",
            s3_endpoint_url="test_url",
            s3_endpoint_url_external="test_url",
            s3_region_name="eu-central-1",
            s3_use_presigned_urls=False,
        )
        mock_job.return_value = 3

        post_request_data = {
            "model": "unique test model",
            "endpoint": "http://awesome-endpoint",
        }
        test_dataset = DatasetModelSQL(
            ls_project_id=1,
            name="test_dataset",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )
        container = Container()

        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            db.session.add(test_dataset)
            db.session.commit()

            response = self.client.post("/datasets/1/auto-annotation", json=post_request_data)
            data = response.get_json()

            self.assertEqual(200, response.status_code)
            expected_response = {"generated_annotations": 3}
            self.assertEqual(expected_response, data)

        self.reset_os_env_values(old_values=old_envs)

    def test_post_auto_annotation_fails_because_of_no_dataset(self):
        """Test if status code 404 is returned because the dataset with given ID was not found."""
        labeling_service_mock = LabelingService(
            ls_client=mock.Mock(spec=Client),
            s3_client=mock.MagicMock(S3Client),
            sts_client=mock.Mock(STSClient),
            s3_bucket_name="test_bucket",
            s3_endpoint_url="test_url",
            s3_endpoint_url_external="test_url",
            s3_region_name="eu-central-1",
            s3_use_presigned_urls=False,
        )

        post_request_data = {
            "model": "unique test model",
            "endpoint": "http://awesome-endpoint",
        }
        container = Container()

        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            response = self.client.post("/datasets/1/auto-annotation", json=post_request_data)

            self.assertEqual(404, response.status_code)

    def test_post_auto_annotation_fails_because_of_missing_parameter(self):
        """Test if status code 400 is returned because a parameter in the request is missing."""
        labeling_service_mock = LabelingService(
            ls_client=mock.Mock(spec=Client),
            s3_client=mock.MagicMock(S3Client),
            sts_client=mock.Mock(STSClient),
            s3_bucket_name="test_bucket",
            s3_endpoint_url="test_url",
            s3_endpoint_url_external="test_url",
            s3_region_name="eu-central-1",
            s3_use_presigned_urls=False,
        )

        post_request_data = {
            "model": "unique test model",  # missing endpoint
        }
        container = Container()

        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            response = self.client.post("/datasets/1/auto-annotation", json=post_request_data)

            self.assertEqual(400, response.status_code)

    @mock.patch("boto3.client")
    @mock.patch("gts_backend.api.routes.datasets.AutoAnnotationJob.run")
    def test_post_auto_annotation_fails_because_of_undefined_error(self, mock_job, mock_client):
        """Test if status code 500 is returned because an Exception is raised."""
        labeling_service_mock = LabelingService(
            ls_client=mock.Mock(spec=Client),
            s3_client=mock.MagicMock(S3Client),
            sts_client=mock.Mock(STSClient),
            s3_bucket_name="test_bucket",
            s3_endpoint_url="test_url",
            s3_endpoint_url_external="test_url",
            s3_region_name="eu-central-1",
            s3_use_presigned_urls=False,
        )
        mock_job.side_effect = Exception("Test exception")
        test_dataset = DatasetModelSQL(
            ls_project_id=1,
            name="test_dataset",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        post_request_data = {
            "model": "unique test model",
            "endpoint": "http://awesome-endpoint",
        }
        container = Container()

        with self.app.app_context(), container.labeling_service.override(labeling_service_mock):

            db.session.add(test_dataset)
            db.session.commit()

            with self.assertRaises(Exception):
                response = self.client.post("/datasets/1/autoa-nnotation", json=post_request_data)

                self.assertEqual(500, response.status_code)


class TestLabelstudioProjectUpdateImportExportSettingsS3Storage(unittest.TestCase):
    """Tests for updating the S3 storage credentials for a LabelStudio project."""

    def setUp(self):
        self.server = Server()

        self.container = Container()
        self.container.init_resources()

        self.app = self.server.create_app(config="test")
        self.client = self.app.test_client()

        with self.app.app_context():
            db.session.execute("PRAGMA foreign_keys = ON;")
            db.session.commit()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def test_update_import_export_settings_project_exists_and_has_s3_settings_configured(self):
        """Test if status code 200 is returned if update succeeds."""
        # setup
        sts_client_mock = mock.Mock(STSClient)
        sts_client_mock.assume_role.return_value = {
            "Credentials": {
                "AccessKeyId": "someId",
                "SecretAccessKey": "someKey",
                "SessionToken": "someToken",
            }
        }

        def return_import_export_response():
            return mock.MagicMock(
                status_code=200,
                json=[
                    {
                        "id": 1,
                        "type": "s3",
                        "presign": False,
                        "title": "S3 data storage",
                        "description": "A custom S3 raw data storage shared by the services.",
                        "created_at": "2023-09-26T11:30:59.003271Z",
                        "last_sync": "2023-09-26T11:30:59.611632Z",
                        "last_sync_count": 1,
                        "last_sync_job": None,
                        "bucket": "bucket-name",
                        "prefix": "project_1__6e871d64-ee3c-42df-9274-ecc3058dc2ec/data",
                        "regex_filter": None,
                        "use_blob_urls": True,
                        "aws_session_token": "someToken",
                        "region_name": "eu-central-1",
                        "s3_endpoint": "http://localhost:9005",
                        "presign_ttl": 1,
                        "recursive_scan": False,
                        "project": 1,
                    }
                ],
            )

        ls_project_mock = mock.MagicMock(Project)
        ls_project_mock.id = 1
        ls_client_mock = mock.Mock(Client)
        ls_client_mock.get_project.return_value = ls_project_mock
        ls_client_mock.make_request.side_effect = return_import_export_response()

        s3_client_mock = mock.MagicMock(S3Client)

        labeling_service_mock = LabelingService(
            ls_client_mock,
            s3_client_mock,
            sts_client_mock,
            "project_1__6e871d64-ee3c-42df-9274-ecc3058dc2ec/data",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            True,
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.put(
                "datasets/1/s3-auth-update",
            )

            self.assertEqual(response.status_code, 200)

    def test_patch_import_export_settings_project_exists_and_has_no_s3_settings_configured(self):
        """Test if status code 200 is returned if updating succeeds with non-existing S3 settings."""
        # setup
        sts_client_mock = mock.Mock(STSClient)
        sts_client_mock.assume_role.return_value = {
            "Credentials": {
                "AccessKeyId": "someId",
                "SecretAccessKey": "someKey",
                "SessionToken": "someToken",
            }
        }

        def return_import_export_response():
            return mock.MagicMock(
                status_code=200,
                json=[],
            )

        ls_project_mock = mock.MagicMock(Project)
        ls_project_mock.id = 1
        ls_client_mock = mock.Mock(Client)
        ls_client_mock.get_project.return_value = ls_project_mock
        ls_client_mock.make_request.side_effect = return_import_export_response()

        s3_client_mock = mock.MagicMock(S3Client)

        labeling_service_mock = LabelingService(
            ls_client_mock,
            s3_client_mock,
            sts_client_mock,
            "project_1__6e871d64-ee3c-42df-9274-ecc3058dc2ec/data",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            True,
        )

        dataset1 = DatasetModelSQL(
            ls_project_id=1,
            name="dataset1",
            s3_prefix="project_1_uuid",
            task_type="object_detection",
            labels="object_detection",
            last_modified=datetime.fromisoformat("2022-01-01T12:00:00"),
            tags="private",
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):
            db.session.add(dataset1)
            db.session.commit()

            response = self.client.put(
                "datasets/1/s3-auth-update",
            )

            self.assertEqual(response.status_code, 200)

    def test_patch_import_export_settings_404_project_does_not_exist(self):
        """Test if status code 404 is returned if the given project is not found."""
        # Setup
        sts_client_mock = mock.Mock(STSClient)

        def return_import_export_response():
            return mock.MagicMock(status_code=404, json=[])

        ls_client_mock = mock.Mock(Client)
        ls_client_mock.make_request.side_effect = return_import_export_response()

        s3_client_mock = mock.MagicMock(S3Client)

        labeling_service_mock = LabelingService(
            ls_client_mock,
            s3_client_mock,
            sts_client_mock,
            "project_1__6e871d64-ee3c-42df-9274-ecc3058dc2ec/data",
            "http://s3:9000",
            "https://s3.example.com:443",
            "eu-central-1",
            True,
        )

        with self.app.app_context(), self.container.labeling_service.override(
            labeling_service_mock
        ):

            response = self.client.put(
                "datasets/1/s3-auth-update",
            )

            self.assertEqual(response.status_code, 404)


if __name__ == "__main__":
    unittest.main()
