### Usage
Parts of the default.conf are used to configure parts of the labelstudio-nginx server. The resulting labelstudio-nginx server configuration can be found under ./helm/label-studio/templates/configmap.yaml
### Original project
https://github.com/HumanSignal/label-studio
### Original file
https://github.com/HumanSignal/label-studio/blob/a39f614fb5d2bbd67dbf7d542e3a61fb4110c68f/deploy/default.conf