# Guided Training Service (GTS): Python backend

The Python backend contains the main logic of the GTS. For a detailed description of the backend see the documentation.

## Dependencies
This project uses Poetry for dependency management.
For a full list of the dependencies see the pyproject.toml.

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically or which have
  a different naming convention e.g. BSD-3-Clause instead of BSD License.
  The content of this file is maintained manually.

### Generating third-party license reports

This project uses [pip-licenses](https://pypi.org/project/pip-licenses/) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated with the following script ./scripts/generate_third_party_license_file.sh.
```shell
sh ./scripts/generate_third_party_license_file.sh
```
**Note**: Sometimes the script adds this project
(e.g. "gts.guided-training-service.python-backend") as a dependency, too. In this case,
simply delete the entry in the generated third-party-licenses.csv.