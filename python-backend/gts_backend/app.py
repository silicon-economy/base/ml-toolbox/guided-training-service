# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module to start the flask and socketIO server.

Initializes flask and socketIO server and loggers.
"""

# Flack-SocketIO docs:
# it is recommended that you apply the monkey patching at the top of your main script,
# even above your imports.
# pylint: disable=wrong-import-position
# isort: off
from gevent import monkey

monkey.patch_all()

# pylint: disable=wrong-import-order
# isort: on
import logging  # noqa: E402
import os  # noqa: E402

from flask import Flask  # noqa: E402
from flask_cors import CORS  # noqa: E402
from flask_socketio import SocketIO  # noqa: E402

from gts_backend.api import api_bp  # noqa: E402
from gts_backend.api.containers import Container  # noqa: E402
from gts_backend.api.socketio.socket import SocketNamespace  # noqa: E402
from gts_backend.services.dbmanager import db, ma  # noqa: E402


class Server:
    """Defines the logic to set up this service."""

    BACKEND_DEBUG_MODE_PARAM_NAME = "BACKEND_DEBUG_MODE"
    backend_debug = os.getenv(BACKEND_DEBUG_MODE_PARAM_NAME, "False") == "True"  # don't use bool()

    logger = logging.getLogger(__name__)

    def __init__(self) -> None:
        """Inits the server."""
        Server.init_loggers()

    def start(self, config: str = "production") -> None:
        """Creates an app instance for this server and runs it.

        Args:
            config: Config of the server.
        """
        app = Server.create_app(config)
        self.backend_debug = (
            os.getenv(self.BACKEND_DEBUG_MODE_PARAM_NAME, "False") == "True"
        )  # don't use bool()
        self.logger.info("Starting Python backend")
        sio = Server.add_socketio(app)

        self.run(app, sio)

    def run(self, app: Flask, sio: SocketIO) -> None:
        """Runs the actual app on the specified port and host.

        Args:
            app: App to run.
            sio: SocketIO instance to run.
        """
        self.logger.debug("Running socketio in debug mode: %s", str(self.backend_debug))
        self.logger.info("Running socketio on port 5000 and all interfaces 0.0.0.0")
        sio.run(app, debug=self.backend_debug, port=5000, host="0.0.0.0")

    @staticmethod
    def create_app(config: str) -> Flask:
        """Creates flask app.

        Registers blueprint for api and removes CORS filter

        Returns:
            The flask app.
        """
        flask_app = Flask(__name__)

        if (
            "ENV" in flask_app.config and flask_app.config["ENV"] == "development"
        ) or config == "development":
            flask_app.config.from_object("gts_backend.config.DevelopmentConfig")
        elif config == "test":
            flask_app.config.from_object("gts_backend.config.TestConfig")
        elif config == "production":
            flask_app.config.from_object("gts_backend.config.ProductionConfig")

        CORS(flask_app)
        Server.register_blueprints(flask_app)
        Server.configure_extensions(flask_app)
        return flask_app

    @staticmethod
    def register_blueprints(flask_app: Flask) -> None:
        """Register all blueprints used in the python-backend.

        Args:
            flask_app: THe app for which the blueprint should be registered.
        """
        flask_app.register_blueprint(api_bp)

    @staticmethod
    def configure_extensions(flask_app: Flask) -> None:
        """Configure all extensions like PostgreSQL.

        Args:
            flask_app: The app for which the extensions should be configured.
        """
        db.init_app(flask_app)
        ma.init_app(flask_app)

        with flask_app.app_context():
            db.create_all()

    @staticmethod
    def add_socketio(flask_app: Flask) -> SocketIO:
        """Adds Socketio to the flask sever.

        Args:
            flask_app: The flask app.

        Returns:
            The socketIO server to send and receive socketio messages.
        """
        socketio = SocketIO(
            flask_app,
            cors_allowed_origins="*",
            engineio_logger=False,
            logger=False,
            async_mode="gevent",
        )
        Server.register_socketio_namespaces(socketio)
        return socketio

    @staticmethod
    def register_socketio_namespaces(socketio: SocketIO) -> None:
        """Register all socketio namespaces used in the python-backend.

        Args:
            socketio: The server for which the namespaces should be registered.
        """
        socketio.on_namespace(
            SocketNamespace(namespace="/")
        )  # todo: register as root namespace for now

    @staticmethod
    def init_loggers() -> None:
        """Initializes loggers of used libraries/frameworks."""
        logging.getLogger("socketio").setLevel(logging.INFO)
        logging.getLogger("gevent").setLevel(logging.INFO)
        logging.getLogger("engineio").setLevel(logging.INFO)
        logging.getLogger("geventwebsocket").setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)


if __name__ == "__main__":  # pragma: no cover
    # init dependency injection container before flask app
    container = Container()
    container.init_resources()

    Server().start()
