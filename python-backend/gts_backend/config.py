# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

# pylint: disable=too-few-public-methods

"""Defining the configuration for the Guided-Training-Service backend."""

import os
from urllib.parse import quote_plus

# TODO Merge this file with the config.yml from DI-Containers

POSTGRESQL_HOSTNAME = "POSTGRESQL_HOSTNAME"
POSTGRESQL_PORT = "POSTGRESQL_PORT"
POSTGRESQL_DB = "POSTGRESQL_DB"
POSTGRESQL_USERNAME = "POSTGRESQL_USERNAME"
POSTGRESQL_PASSWORD = "POSTGRESQL_PASSWORD"


class BaseConfig:
    """Base configuration - allow specific configurations to override settings."""

    DEBUG = False
    TESTING = False
    SECRET_KEY = "secret!"
    SESSION_COOKIE_SECURE = True

    SQLALCHEMY_DATABASE_URI = ""


class ProductionConfig(BaseConfig):
    """Production configuration.

    Configuration is the same as in the base object, which should be safe as default.
    You at least need to override the secret key for production.
    """

    postgresql_hostname = os.environ.get(POSTGRESQL_HOSTNAME, "")
    postgresql_port = os.environ.get(POSTGRESQL_PORT, "")
    postgresql_database = quote_plus(os.environ.get(POSTGRESQL_DB, ""))
    postgresql_user = quote_plus(os.environ.get(POSTGRESQL_USERNAME, ""))
    postgresql_password = quote_plus(os.environ.get(POSTGRESQL_PASSWORD, ""))
    SQLALCHEMY_DATABASE_URI = "postgresql://{}:{}@{}:{}/{}".format(
        postgresql_user,
        postgresql_password,
        postgresql_hostname,
        postgresql_port,
        postgresql_database,
    )


class DevelopmentConfig(ProductionConfig):
    """Production used for local development."""

    DEBUG = True
    SESSION_COOKIE_SECURE = False


class TestConfig(BaseConfig):
    """Configuration used for tests."""

    TESTING = True
    SESSION_COOKIE_SECURE = False

    SQLALCHEMY_DATABASE_URI = "sqlite://"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
