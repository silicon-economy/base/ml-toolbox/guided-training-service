# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module for handling the SocketIO connections."""

import logging

from dependency_injector.wiring import inject
from flask_socketio import Namespace, emit


class SocketNamespace(Namespace):
    """Defines the socket namespace."""

    logger = logging.getLogger(__name__)

    @inject
    def __init__(self, namespace: str | None = None) -> None:
        """Inits the namespace."""
        super().__init__(namespace=namespace)
        self.connections = 0

    def on_connect(self) -> None:
        """Called when client connects."""
        self.connections += 1
        self.logger.info("Client connected. Connection count %d", self.connections)
        emit("message", {"data": "Connected", "count": self.connections}, broadcast=True)

    def on_disconnect(self) -> None:
        """Called when client disconnects."""
        self.connections -= 1
        self.logger.info("Client disconnected. Connection count %d", self.connections)
