# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""API specification.

All resources are added in their own namespaces
"""

from flask import Blueprint
from flask_restx import Api

from gts_backend.api.routes.datasets import api as dataset_api
from gts_backend.api.routes.edge_devices import api as devices_api
from gts_backend.api.routes.flask_restx_models import api as models_api
from gts_backend.api.routes.general import api as general_api
from gts_backend.api.routes.model_testing import api as model_testing_api
from gts_backend.api.routes.models import api as model_api
from gts_backend.api.routes.training import api as train_api
from gts_backend.api.routes.training_servers import api as training_server_api

# create the api endpoint
api_bp = Blueprint("api", __name__)
api = Api(api_bp, version="1.0", title="Guided-Training-Service API")


# add namespace here
api.add_namespace(models_api)
api.add_namespace(general_api)
api.add_namespace(dataset_api)
api.add_namespace(train_api)
api.add_namespace(training_server_api)
api.add_namespace(model_api)
api.add_namespace(model_testing_api)
api.add_namespace(devices_api)

__all__ = ["api_bp"]
