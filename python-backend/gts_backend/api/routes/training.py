# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""REST API for training management."""

import json
import logging
from typing import Any

from config_builder.json_encoding import TupleEncoder
from flask import request
from flask_restx import Namespace, Resource

from gts_backend.api.routes.flask_restx_models import (
    training_params_dao,
    training_start_response_dao,
)
from gts_backend.services.training.training_service import TrainingService

api = Namespace("training", description="Endpoint to start model trainings")

logger = logging.getLogger(__name__)


@api.route("/")
class Training(Resource):
    """Start a training on a remote training server."""

    @api.doc("start_training")
    @api.expect(training_params_dao, validate=True)
    @api.response(
        code=200,
        model=training_start_response_dao,
        description="Successfully started training on training server",
    )
    @api.response(code=400, description="Failed to start training because of missing parameters")
    @api.response(code=500, description="Failed to start training because of internal error")
    def post(self) -> Any:
        """Start a training on a remote training server."""
        try:
            service = TrainingService(params=request.get_json())
            job_id = service.start_training()

            message = "Successfully started training on training server"
            logger.info(message)
            return {
                "message": message,
                "id": job_id,
            }, 200
        except Exception as exception:
            message = f"Failed to start training because of internal error: {exception}"
            logger.warning(message)
            return {
                "message": message,
            }, 500


@api.route("/config")
class ModelConfig(Resource):
    """Endpoint for creating model configs."""

    @api.doc("Get a model config")
    @api.response(
        code=200,
        description="Successfully retrieved model config",
    )
    @api.response(
        code=400,
        description="Failed to retrieve model config due to missing or invalid parameters",
    )
    @api.response(code=500, description="Failed to retrieve model config due to internal error")
    def get(self) -> Any:
        """Retrieve a model config."""
        # Parameter validation is automatically done by parsing request.args
        # If the key does not exist, a 400 is returned.

        params = {
            "model": request.args["model"],
            "modelName": request.args["modelName"],
            "classes": request.args["classes"].split(","),
            "epochs": int(request.args["epochs"]),
            "batchSize": int(request.args["batchSize"]),
            "trainLabelStudioProjectIds": request.args["trainLabelStudioProjectIds"].split(","),
            "evalLabelStudioProjectIds": request.args["evalLabelStudioProjectIds"].split(","),
            "description": request.args["description"],
        }

        service = TrainingService(params=params)
        model_config = service.retrieve_model_config()

        # We do not use a response dao here because we do not
        # know the exact structure of the model config
        return json.loads(TupleEncoder().encode(model_config)), 200
