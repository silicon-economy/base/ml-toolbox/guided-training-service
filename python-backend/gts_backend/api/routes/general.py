# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3


"""HTTP REST API for general actions."""

from typing import Any

from flask_restx import Namespace, Resource, abort

from gts_backend.model.dummy_sql import DummyModelSQL
from gts_backend.services.dbmanager import db

api = Namespace("general", description="General API Actions.")


@api.route("/")
class General(Resource):
    """Implements some general API management actions."""

    @api.doc("reset_db")
    @api.response(code=200, description="Successfully reset the database tables")
    @api.response(code=500, description="Error resetting the database tables")
    def delete(self) -> Any:
        """Resets all data entries in the database.

        Returns:
            200: Successfully deleted all entries.
            500: Failed to delete all data entries.
        """
        try:
            dummy_rows = db.session.query(DummyModelSQL).delete()
            db.session.commit()

            return "Ok. Deleted " + str(dummy_rows) + " rows.", 200
        except Exception as exc:
            db.session.rollback()
            return abort(500, str(exc))

    @api.doc("init_demonstrator")
    @api.response(code=201, description="Successfully initialized the database")
    @api.response(code=500, description="Error initializing the database")
    def post(self) -> Any:
        """Creates the default structure used in the database.

        Returns:
            201: Successfully created the default structure.
            500: Failed to create the default structure.
        """
        try:
            db.drop_all()
            db.create_all()

            dummy = DummyModelSQL(description="This is a dummy object.")
            db.session.add(dummy)
            db.session.commit()
            return "Created", 201
        except Exception as exc:
            db.session.rollback()
            return abort(500, str(exc))


@api.route("/edge_device")
class GeneralEdgeDevice(Resource):
    """Manage the edge device database."""

    @api.doc("reset_db")
    @api.response(code=200, description="Successfully reset the database table")
    @api.response(code=500, description="Error resetting the database table")
    def delete(self) -> Any:
        """Resets all data entries in the database.

        Usage: Delete the old table. Afterwards, update the DatabaseModel, restart the backend
        and add a new database entry.

        Returns:
            200: Successfully deleted all entries.
            500: Failed to delete all data entries.
        """
        try:
            db.session.execute("DROP TABLE IF EXISTS edge_device")
            db.session.commit()

            return "Deleted the table.", 200
        except Exception as exc:
            db.session.rollback()
            return abort(500, str(exc))
