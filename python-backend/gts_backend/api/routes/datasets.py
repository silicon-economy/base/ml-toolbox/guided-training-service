# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""REST API for datasets management."""

import asyncio
import json
import os
from datetime import datetime
from operator import or_
from typing import Any

import aiohttp
import requests
from dependency_injector.wiring import Provide, inject
from flask import request
from flask_restx import Namespace, Resource, abort
from label_studio_sdk import Client
from sqlalchemy.orm.exc import NoResultFound

from gts_backend.api.containers import Container
from gts_backend.api.routes.flask_restx_models import (
    dataset_auto_annotate_dao,
    dataset_creation_dao,
    dataset_dao,
    dataset_id_response,
    dataset_sync_response,
    dataset_update_dao,
    datasets_paginated_dao,
    file_presigned_url_dao,
    file_presigned_url_response,
    generated_annotations_response,
)
from gts_backend.model.dataset import DatasetModelSQL, dataset_schema
from gts_backend.model.structs import DatasetDefaultValues, DatasetSortingTypes
from gts_backend.services.dbmanager import db
from gts_backend.services.jobs.auto_annotation.job import AutoAnnotationJob
from gts_backend.services.jobs.base.configuration import BaseJobConfig
from gts_backend.services.jobs.prediction.configuration import (
    PredictionJobBaseConfig,
    PredictionJobConfig,
)
from gts_backend.services.labeling.service import LabelingService
from gts_backend.services.labeling.structs import (
    LabelStudioProjectInformationAttributes,
)

api = Namespace("datasets", description="The datasets.")

filter_parser = api.parser()
filter_parser.add_argument("filter", type=str, help="search string")
filter_parser.add_argument("page", type=int, help="page of the pagination")
filter_parser.add_argument("per_page", type=int, help="Elements per page for the pagination")
filter_parser.add_argument("order_by", type=str, help="Column for sorting")
filter_parser.add_argument("direction", type=str, help="direction of sorting")


def _merge_dataset_and_ls_project(
    dataset: dict[str, Any],
    project: dict[str, Any],
    labeling_service: LabelingService = Provide[Container.labeling_service],
    preview_images: bool = False,
) -> Any:
    """Merge a dataset (dataset information from the GTS SQL database) and its associated LabelStudio project.

    Each is represented by a dictionary containing the relevant information.

    Args:
        dataset: Dataset information to merge.
        project: Associated LabelStudio project information to merge.
        labeling_service: LabelingService instance.
        preview_images: Bool indication if preview images should be provided or not.


    Returns:
        Merged information.
    """
    assert (
        dataset["ls_project_id"] == project["id"]
    ), f"Trying to merge dataset with id {dataset['ls_project_id']} with project {project['id']}"

    dataset["name"] = project["title"]
    dataset["total_images"] = int(project["task_number"])
    dataset["annotated_images"] = int(project["num_tasks_with_annotations"])
    if preview_images:
        dataset["preview_images"] = labeling_service.get_preview_images(
            s3_prefix=dataset["s3_prefix"]
        )
    dataset[
        "ls_url"
    ] = f"{labeling_service.ls_client.url}/projects/{dataset['ls_project_id']}/data"

    return dataset


async def get_all_ls_projects(
    project_ids: list[int], auth_token: str, ls_url: str
) -> list[dict[str, Any]]:
    """Get information for all given LabelStudio project IDs.

    Args:
        project_ids: LabelStudio project IDs.
        auth_token: Authentication token to access the LabelStudio server.
        ls_url: Endpoint to access the LabelStudio server.

    Returns:
        List with project information.
    """
    async with aiohttp.ClientSession() as session:
        tasks = []
        headers = {"Authorization": f"Token {auth_token}"}
        for id in project_ids:
            url = f"{ls_url}/api/projects/{id}"
            task = asyncio.ensure_future(session.get(url, headers=headers))
            tasks.append(task)
        responses = await asyncio.gather(*tasks)

        project_infos = []
        for index, response in enumerate(responses):
            content = await response.text()
            content_dict = json.loads(content)

            if "status_code" in content_dict and content_dict["status_code"] == 404:
                project_infos.append(
                    {
                        "id": project_ids[index],
                        "title": "CORRUPTED DATASET",
                        "task_number": -1,
                        "num_tasks_with_annotations": -1,
                    }
                )
                continue

            project_infos.append(
                {
                    key: content_dict[key]
                    for key in [
                        attribute.value for attribute in LabelStudioProjectInformationAttributes
                    ]
                }
            )

        return project_infos


@api.route("/")
class DatasetList(Resource):
    """Get the list of datasets and add datasets to it."""

    @inject
    @api.doc("list_datasets")
    @api.expect(filter_parser)
    @api.marshal_with(
        fields=datasets_paginated_dao,
        code=200,
        description="Successfully retrieved datasets",
    )
    @api.response(code=500, description="Error retrieving datasets")
    def get(self) -> tuple[dict[str, Any], int] | Any:
        """Endpoint for GET all datasets."""
        # parse args for filters and sorting
        args = filter_parser.parse_args()

        if args["order_by"]:
            if args["order_by"] == DatasetSortingTypes.TAGS.lower():
                order_column = DatasetModelSQL.tags
            elif args["order_by"] == DatasetSortingTypes.NAME.lower():
                order_column = DatasetModelSQL.name
            elif args["order_by"] == DatasetSortingTypes.TYPE.lower():
                order_column = DatasetModelSQL.task_type
            else:
                order_column = DatasetModelSQL.last_modified
        else:
            order_column = DatasetModelSQL.last_modified

        if args["direction"] and args["direction"] == DatasetSortingTypes.DESC.lower():
            order_column = order_column.desc()
        else:
            order_column = order_column.asc()

        if args["per_page"]:
            per_page = (
                int(args["per_page"])
                if int(args["per_page"]) > 0
                else DatasetDefaultValues.DATASETS_PER_PAGE
            )
        else:
            per_page = DatasetDefaultValues.DATASETS_PER_PAGE

        if args["page"]:
            page = (
                int(args["page"])
                if int(args["page"]) > 0
                else int(DatasetDefaultValues.DEFAULT_PAGE)
            )
        else:
            page = DatasetDefaultValues.DEFAULT_PAGE

        if args["filter"]:
            filter_str = f'%{args["filter"]}%'
            filter_query = or_(
                DatasetModelSQL.name.ilike(filter_str),
                DatasetModelSQL.tags.ilike(filter_str),
            )
            paginated = (
                DatasetModelSQL.query.filter(filter_query)
                .order_by(order_column)
                .paginate(page=page, per_page=per_page)
            )
        else:
            paginated = DatasetModelSQL.query.order_by(order_column).paginate(
                page=page, per_page=per_page
            )

        datasets = paginated.items
        datasets_info = [dataset_schema.dump(dataset) for dataset in datasets]

        auth_token = os.getenv("LABEL_STUDIO_API_KEY")
        ls_url = os.getenv("LABEL_STUDIO_ENDPOINT")

        if auth_token is None or ls_url is None:
            raise ValueError(
                "LABEL_STUDIO_API_KEY and/or LABEL_STUDIO_ENDPOINT "
                "environment variables are not set!"
            )

        projects_info = asyncio.run(
            get_all_ls_projects(
                project_ids=[dataset.ls_project_id for dataset in datasets],
                auth_token=auth_token,
                ls_url=ls_url,
            )
        )

        assert len(datasets_info) == len(projects_info), (
            f"Found {len(datasets_info)} datasets in our database "
            f"and {len(projects_info)} projects in Label-Studio."
        )

        response = {
            "page": page,
            "page_count": paginated.pages,
            "total_items": paginated.total,
            "per_page": paginated.per_page,
            "datasets": list(map(_merge_dataset_and_ls_project, datasets_info, projects_info)),
        }

        return response, 200

    @inject
    @api.doc("create_dataset")
    @api.expect(dataset_creation_dao, validate=True)
    @api.marshal_with(fields=dataset_dao, code=201, description="Successfully created dataset")
    @api.response(code=400, description="Failed to create dataset because of missing parameters")
    @api.response(code=500, description="Failed to create dataset because of internal error")
    def post(self, labeling_service: LabelingService = Provide[Container.labeling_service]) -> Any:
        """Create a new dataset."""
        body = api.payload
        name = body["name"]
        task_type = body["type"]
        labels = body["labels"]
        tags = body.get("tags", [])

        # add dataset to Label Studio
        ls_project, s3_prefix = labeling_service.initialize_project(
            dataset_name=name, task_type=task_type, labels=json.loads(labels)
        )

        # add dataset to local DB
        dataset = DatasetModelSQL(
            ls_project_id=ls_project.id,
            name=name,
            s3_prefix=s3_prefix,
            task_type=task_type,
            labels=labels,
            tags=json.dumps(tags),
            last_modified=datetime.now(),
        )

        db.session.add(dataset)
        db.session.commit()

        dataset = _merge_dataset_and_ls_project(
            dataset_schema.dump(dataset), ls_project.get_params()
        )

        return dataset, 201


@api.route("/<int:ls_project_id>")
@api.param("ls_project_id", description="The ID of the dataset")
class Dataset(Resource):
    """Show a single dataset, update and delete it."""

    @api.doc("get_dataset")
    @api.marshal_with(
        fields=dataset_dao,
        code=200,
        description="Successfully retrieved dataset",
    )
    @api.response(
        code=404, description="Failed to retrieve dataset because the given ID was not found"
    )
    @api.response(code=500, description="Failed to retrieve dataset because of an internal error")
    def get(self, ls_project_id: int, ls_client: Client = Provide[Container.ls_client]) -> Any:
        """Get a single dataset."""
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()
            project = ls_client.get_project(ls_project_id)

            dataset = _merge_dataset_and_ls_project(
                dataset_schema.dump(dataset), project.get_params(), preview_images=True
            )

            return dataset, 200

        except NoResultFound:
            return abort(code=404, message="The dataset with the given ID was not found")

    @api.doc("update_dataset")
    @api.expect(dataset_update_dao, validate=True)
    @api.response(code=200, model=dataset_dao, description="Successfully updated dataset")
    @api.marshal_with(
        fields=dataset_dao,
        code=200,
        description="Successfully updated dataset",
    )
    @api.response(code=400, description="Failed to update dataset because of missing parameters")
    @api.response(code=404, description="Failed to update dataset because the ID was not found")
    @api.response(code=500, description="Failed to update dataset because of an internal error")
    def put(
        self,
        ls_project_id: int,
        ls_client: Client = Provide[Container.ls_client],
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Update a dataset."""
        body = api.payload

        try:
            db.session.query(DatasetModelSQL).filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).update(
                {
                    "name": body["name"],
                    # "task_type": body["type"],
                    # "labels": json.dumps(body["labels"]),
                    "tags": json.dumps(body["tags"]),
                    "last_modified": datetime.now(),
                }
            )
            db.session.commit()

            project = ls_client.get_project(ls_project_id)

            project.set_params(title=body["name"])

            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()

            labeling_service.sync_all_s3_cloud_storages(ls_project_id)

            # TODO: update labeling interface in label studio Project with changes from payload.
            #  Maybe do this in the labeling service

            project = ls_client.get_project(ls_project_id)
            dataset = _merge_dataset_and_ls_project(
                dataset_schema.dump(dataset), project.get_params()
            )

            return dataset, 200

        except NoResultFound:
            return abort(code=404, message="The dataset with the given ID was not found")

    @api.doc("delete_dataset")
    @api.marshal_with(
        fields=dataset_id_response, code=200, description="Successfully deleted dataset"
    )
    @api.response(
        code=404, description="Failed to delete dataset because the given ID was not found"
    )
    @api.response(code=500, description="Failed to delete dataset because of an internal error")
    def delete(
        self,
        ls_project_id: int,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Delete a dataset."""
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()

            labeling_service.delete_project(ls_project_id, dataset.s3_prefix)

            db.session.query(DatasetModelSQL).filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).delete()
            db.session.commit()

            return str(ls_project_id), 200

        except NoResultFound:
            return abort(code=404, message="The dataset with the given ID was not found")


@api.route("/<int:ls_project_id>/label-config-update")
@api.param("ls_project_id", description="The ID of the dataset")
class DatasetFramerateUpdate(Resource):
    """Update the framerate of a dataset."""

    @inject
    @api.doc("update_label_config_tracking_task")
    @api.expect(file_presigned_url_response, validate=True)
    @api.marshal_with(
        fields=dataset_dao,
        code=200,
        description="Successfully updated label config for LabelStudio project",
    )
    @api.response(code=400, description="Failed to update because of missing parameters")
    @api.response(code=404, description="Failed to update because the given ID was not found")
    @api.response(code=500, description="Failed to update dataset because of an internal error")
    def post(
        self,
        ls_project_id: int,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Update $framerate placeholder in the label config of an object tracking project."""
        try:
            dataset: DatasetModelSQL = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()

            body: Any = api.payload
            presigned_url: str = body["url"]

            response: requests.Response = (
                labeling_service.set_framerate_labelstudio_project_template(
                    project_id=ls_project_id, presigned_url_video_file=presigned_url
                )
            )

            if response.status_code != 200:
                raise RuntimeError("The setting of the framerate was unsuccessful")

            auth_token = os.getenv("LABEL_STUDIO_API_KEY")
            ls_url = os.getenv("LABEL_STUDIO_ENDPOINT")
            dataset_info = dataset_schema.dump(dataset)
            project_info = asyncio.run(
                get_all_ls_projects(
                    project_ids=[dataset.ls_project_id],
                    auth_token=auth_token,  # type: ignore[arg-type]
                    ls_url=ls_url,  # type: ignore[arg-type]
                )
            )[0]
            # TODO: What should we return? The dataset has not changed, only the ls project label config
            return _merge_dataset_and_ls_project(dataset_info, project_info), 200

        except NoResultFound:
            return abort(code=404, message="The dataset with the given ID was not found")


@api.route("/<int:ls_project_id>/s3-auth-update")
@api.param("ls_project_id", description="The id of the LabelStudio project")
class LabelstudioProjectUpdateImportExportSettingsS3Storage(Resource):
    """Update the temporary S3 credentials for a LabelStudio's import and export storage."""

    @api.doc("update_s3_auth")
    @api.response(code=204, description="Successfully updated the S3 import/export settings")
    @api.response(
        code=404,
        description="Failed to update settings because the given ID was not found",
    )
    @api.response(code=500, description="Failed to update settings because of an internal error")
    @inject
    def put(
        self,
        ls_project_id: int,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Update the S3 storage credentials for a LabelStudio project.

        If no storage is configured for a project, the import and export storage are created for the respective
        LabelStudio project. Otherwise, only the credentials are updated.
        """
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()
            # before we create a new user with temporary access we ensure that everything is as expected in
            # the s3 bucket
            response: bool = labeling_service.update_import_export_s3_settings_for_temporary_user(
                s3_prefix=dataset.s3_prefix, ls_project_id=ls_project_id
            )
            if response:
                return 200

            else:
                raise RuntimeError("Failed to update the settings")

        except NoResultFound:
            return abort(
                code=404, message="Failed to update settings because the given ID was not found"
            )


@api.route("/<int:ls_project_id>/synchronization")
@api.param("ls_project_id", description="The ID of the dataset")
class DatasetSync(Resource):
    """Synchronize a dataset in LabelStudio and the S3 storage."""

    @inject
    @api.doc("sync_storage")
    @api.marshal_with(
        fields=dataset_sync_response, code=200, description="Successfully synced dataset"
    )
    @api.response(
        code=404, description="Failed to sync dataset because the given ID was not found"
    )
    @api.response(code=500, description="Failed to sync dataset because of an internal error")
    def get(
        self,
        ls_project_id: int,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Sync all S3 cloud storages of a dataset."""
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()
            # before we sync the data we ensure that everything is as expected in the s3 bucket
            labeling_service.extract_all_images_from_zips(dataset.s3_prefix)

            response = {
                "imported_items": labeling_service.sync_all_s3_cloud_storages(ls_project_id)
            }
            return response, 200

        except NoResultFound as exc:
            return abort(404, str(exc))


@api.route("/<int:ls_project_id>/annotation-export")
@api.param("ls_project_id", description="the ID of the dataset")
class DatasetAnnotationExport(Resource):
    """Export the annotations of a dataset."""

    @inject
    @api.doc("annotation_export")
    @api.marshal_with(
        fields=file_presigned_url_response,
        code=200,
        description="Successfully exported the annotations",
    )
    @api.response(code=404, description="Failed to export because the given ID was not found")
    @api.response(code=500, description="Failed to export because of an internal error")
    def get(
        self,
        ls_project_id: int,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Export of all available annotations to the S3 cloud storage."""
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()
            filename = dataset.name + "_" + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            url = labeling_service.export_annotations_to_s3(
                project_id=ls_project_id,
                s3_prefix=dataset.s3_prefix,
                filename=filename,
            )
            response = {"url": url}

            return response, 200

        except NoResultFound:
            return abort(code=404, message="Failed to export because the given ID was not found")


@api.route("/<int:ls_project_id>/<string:s3_folder>/url-generation/<string:file_name>")
@api.param(
    "ls_project_id",
    description="The ID  of the dataset you want to generate an url for uploading a single file",
)
@api.param("s3_folder", description="The folder the file is located in")
@api.param("file_name", description="The file name")
class DatasetTrackingPresignedUrl(Resource):
    """Generate presinged urls for object tracking datasets."""

    @api.marshal_with(
        fields=file_presigned_url_response,
        code=200,
        description="Successfully generated an url for getting the content of a file",
    )
    @api.response(code=404, description="Failed to generate an url because the ID was not found")
    @api.response(code=500, description="Failed to generate an url because of an internal error")
    @inject
    def get(
        self,
        ls_project_id: int,
        s3_folder: str,
        file_name: str,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Get a presigned url for an object tracking dataset."""
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()

            url = labeling_service.generate_s3_presigned_url_get_object(
                s3_prefix=dataset.s3_prefix,
                s3_folder=s3_folder,
                file_name=file_name,
            )
            response = {"url": url}

            return response, 200

        except NoResultFound:
            return abort(
                code=404, message="Failed to generate an url because the ID was not found"
            )


@api.route("/<int:ls_project_id>/<string:s3_folder>/url-generation")
@api.param(
    "ls_project_id",
    description="The ID of the dataset you want to generate an url for uploading a single file",
)
@api.param("s3_folder", description="The folder the file is located in")
class DatasetPresignedUrl(Resource):
    """Utility to generate presinged urls for the S3 storage."""

    # TODO: check if inject is set correctly, c.f. https://python-dependency-injector.ets-labs.org/wiring.html
    #  also check if inject decorators are missing at other Resources
    @inject
    @api.doc("generate_url_put_object_s3")
    @api.expect(file_presigned_url_dao, validate=True)
    @api.marshal_with(
        fields=file_presigned_url_response,
        code=200,
        description="Successfully generated an url for the file to upload",
    )
    @api.response(code=400, description="Failed to generate url because of missing parameters")
    @api.response(
        code=404, description="Failed to generate url because the given ID was not found"
    )
    @api.response(code=500, description="Failed to generate url because of an internal error")
    def post(
        self,
        ls_project_id: int,
        s3_folder: str,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Endpoint to generate an url for uploading a single file."""
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()

            body = api.payload

            url = labeling_service.generate_s3_presigned_url_upload(
                s3_prefix=dataset.s3_prefix,
                s3_folder=s3_folder,
                file_name=body["file_name"],
            )
            response = {"url": url}

            return response, 200

        except NoResultFound:
            return abort(
                code=404, message="Failed to generate url because the given ID was not found"
            )


@api.route("/<int:ls_project_id>/annotation-import")
@api.param(
    "ls_project_id",
    description="The ID of the dataset you want to import an annotation for",
)
class AnnotationImport(Resource):
    """Import annotations for datasets."""

    @inject
    @api.doc("import_annotation")
    @api.response(
        code=200,
        description="Successfully imported the annotations",
    )
    @api.response(
        code=404, description="Failed to import annotations because the given ID was not found"
    )
    @api.response(
        code=500, description="Failed to import annotations because of an internal error "
    )
    def get(
        self,
        ls_project_id: int,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Import annotations for a given dataset."""
        try:
            dataset: DatasetModelSQL = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()

            num_imported_annotations = labeling_service.import_ls_annotation(
                ls_project_id, dataset.s3_prefix
            )
            response = {"imported_annotations": num_imported_annotations}

            return response, 200

        except NoResultFound:
            return abort(code=404, message="")


@api.route("/<int:ls_project_id>/auto-annotation")
@api.param(
    "ls_project_id",
    description="The ID of the dataset you want tu automatically generate annotations for.",
)
class AutoAnnotation(Resource):
    """Generate annotations for datasets."""

    @inject
    @api.doc("auto_annotate_dataset")
    @api.expect(dataset_auto_annotate_dao, validate=True)
    @api.marshal_with(
        fields=generated_annotations_response,
        code=200,
        description="Successfully generated annotations for given dataset",
    )
    @api.response(code=400, description="Failed auto annotation because of missing parameters")
    @api.response(
        code=404, description="Failed auto annotation because the given ID was not found"
    )
    @api.response(code=500, description="Failed auto annotation because of an internal error")
    def post(
        self,
        ls_project_id: int,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Generate annotations for a given dataset."""
        params = request.get_json()

        try:
            model_name = params["model"]
            server_endpoint = params["endpoint"]
            dataset: DatasetModelSQL = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()

            job = AutoAnnotationJob(
                config=PredictionJobConfig(
                    base=BaseJobConfig(server_endpoint=server_endpoint),
                    job=PredictionJobBaseConfig(model_name=model_name),
                ),
                datasets=[dataset],
                labeling_service=labeling_service,
            )
            generated_annotations = job.run()
            response = {"generated_annotations": generated_annotations}

            return response, 200

        except NoResultFound:
            return abort(
                code=404, message="Failed auto annotation because the given ID was not found"
            )


@api.route("/<int:ls_project_id>/<string:s3_folder>/<string:file_name>/file-removal")
@api.param("ls_project_id", description="The ID of the dataset you want to remove a file from")
@api.param("s3_folder", description="The folder the file is located in")
@api.param("file_name", description="The name of the file you want to remove")
class RemoveFile(Resource):
    """Remove single files from datasets."""

    @inject
    @api.doc("remove_file")
    @api.response(code=204, description="Successfully removed the file")
    @api.response(code=404, description="Failed removal because the given ID was not found")
    @api.response(code=500, description="Failed removal because of an internal error")
    def delete(
        self,
        ls_project_id: int,
        s3_folder: str,
        file_name: str,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Remove a single file from a given dataset."""
        try:
            dataset = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()
            labeling_service.delete_file_from_s3(
                s3_prefix=dataset.s3_prefix, s3_folder=s3_folder, file_name=file_name
            )

            return 204

        except NoResultFound:
            return abort(code=404, message="Failed removal because of an internal error")
