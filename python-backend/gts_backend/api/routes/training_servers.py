# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3


"""REST API for training server management."""

from typing import Any

from flask_restx import Namespace, Resource, abort
from sqlalchemy.orm.exc import NoResultFound

from gts_backend.api.routes.flask_restx_models import (
    training_server_creation_dao,
    training_server_dao,
)
from gts_backend.model.training_server import TrainingServerModelSQL
from gts_backend.services.dbmanager import db

api = Namespace("training-servers", description="Endpoints for management of training servers")


@api.route("/")
class TrainingServerList(Resource):
    """Get the list of training servers and add servers to it."""

    @api.doc("get_training_servers")
    @api.marshal_list_with(
        fields=training_server_dao,
        code=200,
        description="Successfully retrieved list of training servers",
    )
    @api.response(code=500, description="Error while retrieving list of training servers")
    def get(self) -> Any:
        """Get all training servers."""
        servers = TrainingServerModelSQL.query.all()
        return servers

    @api.doc("add_training_server")
    @api.expect(training_server_creation_dao, validate=True)
    @api.marshal_with(
        fields=training_server_dao, code=201, description="Successfully added new training server"
    )
    @api.response(
        code=400, description="Failed to add new training server because of missing parameters"
    )
    @api.response(
        code=500, description="Failed to add new training server because of internal error"
    )
    def post(self) -> Any:
        """Add a new training server."""
        training_server = TrainingServerModelSQL(
            endpoint=api.payload["endpoint"],
            server_name=api.payload["name"],
        )

        db.session.add(training_server)
        db.session.commit()

        return training_server, 201


@api.route("/<int:training_server_id>")
@api.param(
    "training_server_id",
    description="The unique ID of the training server to be displayed",
)
class TrainingServer(Resource):
    """Display a single training server and lets you delete it."""

    @api.doc("get_training_server")
    @api.marshal_with(
        fields=training_server_dao,
        code=200,
        description="Successfully retrieved training server",
    )
    @api.response(code=404, description="The training server ID was not found")
    @api.response(code=500, description="Failed to retrieve training server")
    def get(self, training_server_id: int) -> Any:
        """Get a single training server."""
        try:
            training_server = TrainingServerModelSQL.query.filter(
                TrainingServerModelSQL.server_id == training_server_id
            ).one()

            return training_server, 200

        except NoResultFound:
            return abort(code=404, message="The training server ID was not found")

    @api.doc("delete_training_server")
    @api.response(code=200, description="Successfully deleted training server")
    @api.response(code=404, description="The training server ID was not found")
    @api.response(code=500, description="Failed to delete training server")
    def delete(self, training_server_id: int) -> Any:
        """Delete a training server."""
        try:
            training_server = TrainingServerModelSQL.query.filter(
                TrainingServerModelSQL.server_id == training_server_id
            ).one()

            try:
                db.session.delete(training_server)
            except Exception:
                db.session.rollback()
                raise
            else:
                db.session.commit()

            return "Successfully deleted training server", 200

        except NoResultFound:
            return abort(code=404, message="The training serer ID was not found")
