# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining flask_restx models."""

from flask_restx import Namespace
from flask_restx import fields as apifields

api = Namespace("Flask Restx Models", description="The flask restx models")


dummy_dao = api.model(
    "Dummy",
    {"description": apifields.String(required=True, description="some description of the dummy")},
)

preview_image_dao = api.model(
    "Preview Image",
    {
        "url": apifields.String(
            required=True, attribute="url", description="The url of the preview image"
        )
    },
)

dataset_dao = api.model(
    "Dataset",
    {
        "ls_project_id": apifields.String(
            required=True,
            attribute="ls_project_id",
            description="The datasets unique identifier given by the Label-Studio project id",
        ),
        "name": apifields.String(
            required=True, attribute="name", description="The name of the dataset"
        ),
        "type": apifields.String(
            required=True, attribute="task_type", description="The type of the dataset"
        ),  # in LabelStudio it is task_type, therefore we have to use this as the attribute
        "labels": apifields.String(
            required=True,
            attribute="labels",
            description="The labels/classes for the annotation of the dataset",
        ),
        "tags": apifields.String(
            required=False,
            attribute="tags",
            description="The tags assigned to the dataset",
        ),
        "last_modified": apifields.DateTime(
            required=True,
            attribute="last_modified",
            description="Timestamp the Dataset was last modified",
        ),
        "s3_prefix": apifields.String(
            required=True,
            attribute="s3_prefix",
            description="The prefix of the S3 directory of the dataset",
        ),
        "total_images": apifields.Integer(
            required=False,
            attribute="total_images",
            description="The count of Images in the dataset",
        ),
        "annotated_images": apifields.Integer(
            required=False,
            attribute="annotated_images",
            description="The count of Images in the dataset with annotations",
        ),
        "preview_images": apifields.List(
            cls_or_instance=apifields.Nested(
                preview_image_dao, required=False, description="Example images of the dataset."
            )
        ),
        "ls_url": apifields.String(
            required=False, attribute="ls_url", description="The url to the LabelStudio model"
        ),
    },
)

datasets_paginated_dao = api.model(
    "Paginated Datasets",
    {
        "page": apifields.Integer(
            required=True,
            attribute="page",
            description="The current page of the dataset pagination",
        ),
        "per_page": apifields.Integer(
            required=True,
            attribute="per_page",
            description="The number of dataset rows per page",
        ),
        "total_items": apifields.Integer(
            required=True,
            attribute="total_items",
            description="The total number of datasets with given filter",
        ),
        "page_count": apifields.Integer(
            required=True,
            attribute="page_count",
            description="The number of pages with the given filter",
        ),
        "datasets": apifields.List(apifields.Nested(dataset_dao)),
    },
)

dataset_creation_dao = api.model(
    "Dataset creation",
    {
        "name": apifields.String(
            required=True, attribute="name", description="The name of the dataset"
        ),
        "type": apifields.String(
            required=True, attribute="type", description="The type of the dataset"
        ),
        "labels": apifields.String(
            required=True,
            attribute="labels",
            description="The labels/classes that should be used for the annotation of the dataset",
        ),
        "tags": apifields.String(
            required=False,
            attribute="tags",
            description="The tags that should be assigned to the dataset",
        ),
    },
)

dataset_update_dao = api.model(
    "Dataset update",
    {
        "name": apifields.String(
            required=False, attribute="name", description="The new name of the dataset"
        ),
        "type": apifields.String(
            required=False, attribute="type", description="The new type of the dataset"
        ),
        "labels": apifields.String(
            required=False,
            attribute="labels",
            description="The new labels/classes for the annotation of the dataset",
        ),
        "tags": apifields.String(
            required=False,
            attribute="tags",
            description="The new tags that should be assigned to the dataset",
        ),
    },
)

dataset_sync_response = api.model(
    "Dataset sync response",
    {
        "imported_items": apifields.Integer(
            description="The number of newly imported tasks (e.g. images) from this sync"
            "operation."
        )
    },
)

dataset_auto_annotate_dao = api.model(
    "Dataset auto annotate",
    {
        "model": apifields.String(
            required=True,
            attribute="model_name",
            description="Model used to generate the annotations",
        ),
        "endpoint": apifields.String(
            required=True,
            attribute="server_endpoint",
            description="Server the prediction is executed on",
        ),
    },
)

image_dao = api.model(
    "image",
    {
        "image_name": apifields.String(
            required=True, attribute="image_name", description="The name of the image"
        ),
        "image_data": apifields.String(
            required=True, attribute="image_data", description="Encoded image"
        ),
    },
)

dataset_images_dao = api.model(
    "Image upload",
    {
        "images": apifields.List(apifields.Nested(image_dao)),
    },
)

file_presigned_url_dao = api.model(
    "File presigned url",
    {
        "file_name": apifields.String(
            required=True,
            attribute="file_name",
            description="The name of the file to upload",
        ),
    },
)

model_artifacts_download_link_response = api.model(
    name="Model artifacts download url",
    model={"url": apifields.String(description="Download url for model artifacts.")},
)

file_presigned_url_response = api.model(
    "File presigned url response",
    {
        "url": apifields.String(
            required=True,
            description="The url the s3 client has generated for the given project and file",
        )
    },
)

training_params_dao = api.model(
    "Training parameters",
    {
        "model": apifields.String(
            required=True,
            attribute="model",
            description="The model selected for training",
        ),
        "modelName": apifields.String(
            required=True,
            attribute="modelName",
            description="The custom name of the model",
        ),
        "classes": apifields.List(
            apifields.String(
                required=True,
                attribute="classes",
                description="The classes the model is trained to detect",
            )
        ),
        "epochs": apifields.Integer(
            required=True,
            attribute="epochs",
            description="The number of epochs the model is trained for",
        ),
        "batchSize": apifields.Integer(
            required=True,
            attribute="batchSize",
            description="The model selected for training",
        ),
        "description": apifields.String(
            required=True,
            attribute="description",
            description="The custom description of the model",
        ),
        "trainLabelStudioProjectIds": apifields.List(
            apifields.String(
                required=True,
                attribute="trainLabelStudioProjectIds",
                description="The ids of the datasets selected for training",
            )
        ),
        "evalLabelStudioProjectIds": apifields.List(
            apifields.String(
                required=True,
                attribute="evalLabelStudioProjectIds",
                description="The ids of the datasets selected for evaluation",
            )
        ),
        "model_config": apifields.String(
            required=False,
            attribute="model_config",
            description="Model configuration as dictionary. When this is parsed the config creator "
            "will use this config and does not build a new one.",
        ),
        "endpoint": apifields.String(
            required=True,
            attribute="endpoint",
            description="The API endpoint of the training server where the training is executed",
        ),
    },
)

training_start_response_dao = api.model(
    "Response for a successful training start",
    {
        "id": apifields.String(
            required=True,
            attribute="id",
            description="ID of the resulting training job",
        ),
        "message": apifields.String(
            required=True,
            attribute="message",
            description="Message regarding the success of the training start",
        ),
    },
)

training_server_creation_dao = api.model(
    "Training server creation",
    {
        "endpoint": apifields.String(
            required=True,
            attribute="endpoint",
            description="The API endpoint of the training server where the training is executed",
        ),
        "name": apifields.String(
            required=True, description="The custom name of the training server"
        ),
    },
)

edge_device_creation_dao = api.model(
    "Edge device creation",
    {
        "uri": apifields.String(
            required=True,
            attribute="uri",
            description="The URI of the edge device",
        ),
        "name": apifields.String(
            required=True,
            attribute="name",
            description="The custom name of the edge device",
        ),
        "deviceType": apifields.String(
            required=True,
            attribute="device_type",
            description="The device type",
        ),
    },
)

edge_device_deploy_dao = api.model(
    "Edge device creation",
    {
        "id": apifields.Integer(
            required=True,
            attribute="id",
            description="The ID of the edge device",
        ),
        "model": apifields.String(
            required=True,
            attribute="model",
            description="The custom name of the model to deploy",
        ),
    },
)

edge_device_dao = api.model(
    "Edge device",
    {
        "id": apifields.Integer(
            required=True,
            attribute="id",
            description="Unique ID of the edge device",
        ),
        "uri": apifields.String(
            required=True,
            attribute="uri",
            description="The URI of the edge device",
        ),
        "name": apifields.String(
            required=True,
            attribute="name",
            description="The custom name of the edge device",
        ),
        "deviceType": apifields.String(
            required=True,
            attribute="device_type",
            description="The device type",
        ),
        "creationDate": apifields.DateTime(
            required=True,
            attribute="creation_date",
            description="Timestamp the edge device was added",
        ),
        "is_recording": apifields.Boolean(
            required=False,
            attribute="is_recording",
            description="Info weather the device is currently recording or not",
        ),
        "stream": apifields.String(
            required=False,
            attribute="stream",
            description="Stream of the device",
        ),
    },
)

edge_device_recording_dao = api.model(
    "Edge device recording",
    {
        "message": apifields.String(
            required=True,
            attribute="message",
            description="A message about the successful recording",
        ),
        "download_link": apifields.String(
            required=True,
            attribute="download_link",
            description="The link to download the recorded dataset",
        ),
        "file_name": apifields.String(
            required=True,
            attribute="file_name",
            description="The file name of the recorded dataset",
        ),
    },
)

training_server_dao = api.model(
    "Training server",
    {
        "id": apifields.Integer(
            required=True,
            attribute="server_id",
            description="Unique ID of the training server",
        ),
        "endpoint": apifields.String(
            required=True,
            attribute="endpoint",
            description="The API endpoint of the training server where the training is executed",
        ),
        "name": apifields.String(
            required=True,
            attribute="server_name",
            description="The custom name of the training server",
        ),
    },
)


model_testing_dao = api.model(
    "Model Testing parameters",
    {
        "model_name": apifields.String(
            required=True,
            attribute="model_name",
            description="The registered model to be tested.",
        ),
        "endpoint": apifields.String(
            required=True,
            attribute="endpoint",
            description="The API endpoint of the prediction server where the prediction is executed",
        ),
        "labelStudioProjectId": apifields.String(
            required=True,
            attribute="labelStudioProjectId",
            description="The id of the datasets selected for prediction",
        ),
        "second_model_name": apifields.String(
            required=False,
            attribute="second_model_name",
            description="The registered model to be tested.",
        ),
    },
)
registered_model_dao = api.model(
    name="Registered model",
    model={
        "run_id": apifields.String(
            required=True,
            attribute="run_id",
            description="The Mlflow run ID",
        ),
        "description": apifields.String(
            required=True,
            attribute="description",
            description="A short description",
        ),
        "model": apifields.String(
            required=True,
            attribute="model",
            description="The model architecture",
        ),
        "name": apifields.String(
            required=True,
            attribute="name",
            description="The custom name",
        ),
        "accuracy": apifields.Float(
            required=True,
            attribute="accuracy",
            description="The overall accuracy on the evaluated datasets",
        ),
        "epochs": apifields.Integer(
            required=True,
            attribute="epochs",
            description="The number of epochs the model was trained",
        ),
        "batch_size": apifields.Integer(
            required=True,
            attribute="batch_size",
            description="The batch size used for training",
        ),
        "dataset": apifields.List(
            apifields.String,
        ),
        "training_state": apifields.String(
            required=True,
            attribute="training_state",
            description="The current training state",
        ),
        "algorithm_type": apifields.String(
            required=True,
            attribute="algorithm_type",
            description="The algorithm used to train the model",
        ),
        "evaluation_datasets": apifields.List(
            apifields.String,
        ),
        "creation_date": apifields.String(
            required=True,
            attribute="creation_date",
            description="The creation date",
        ),
        "mlflow_url": apifields.String(
            required=True,
            attribute="mlflow_url",
            description="The Mlflow url",
        ),
    },
)

registered_models_list_dao = api.model(
    name="Registered models",
    model={
        "total_items": apifields.Integer(
            required=True,
            attribute="total_items",
            description="The total number of registered models",
        ),
        "models": apifields.List(apifields.Nested(registered_model_dao)),
    },
)

model_testing_predicted_image = api.model(
    name="URL to a single tested image",
    model={
        "url": apifields.String(
            required=True,
            attribute="url",
            description="URL to the tested image",
        )
    },
)

model_testing_response = api.model(
    name="Model Testing Response",
    model={
        "labelStudioProjectId": apifields.String(
            required=True,
            attribute="labelStudioProjectId",
            description="The ID of the dataset the model was tested on",
        ),
        "modelName": apifields.String(
            required=True,
            attribute="modelName",
            description="The name of the tested model",
        ),
        "images": apifields.List(apifields.Nested(model_testing_predicted_image)),
    },
)

dataset_id_response = api.model(name="Dataset ID", model={"dataset_id": apifields.String})

imported_annotations_response = api.model(
    name="Imported annotations", model={"imported_annotations": apifields.Integer}
)

generated_annotations_response = api.model(
    name="Generated annotations", model={"generated_annotations": apifields.Integer}
)

fp_fn_image_wildcard = apifields.Wildcard(apifields.List(apifields.String()))

fp_fn_image_field = api.model(
    name="FP/FN image field",
    model={
        "*_*": fp_fn_image_wildcard
    },  # we do not know the key name but the pattern ID_CLASSNAME
    description="The false positive and false negative images of a given model.",
)


fp_fn_images_response = api.model(
    name="FP/ FN images", model={"images": apifields.Nested(fp_fn_image_field)}
)
