# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3


"""REST API for edge device management."""

import os
from typing import Any

from flask import request
from flask_restx import Namespace, Resource, abort

from gts_backend.api.routes.flask_restx_models import (
    edge_device_creation_dao,
    edge_device_dao,
    edge_device_recording_dao,
)
from gts_backend.services.edge_devices.edge_device_service import (
    EdgeDeviceNotFoundError,
    EdgeDeviceNotRecordingError,
    EdgeDeviceService,
)

api = Namespace("edge-devices", description="Endpoints for edge device management")


@api.route("/")
class EdgeDeviceList(Resource):
    """Get a list of all edge devices and add new edge devices to the device list."""

    @api.doc("get_edge_devices")
    @api.marshal_list_with(
        fields=edge_device_dao,
        code=200,
        description="Successfully retrieved list of edge devices.",
    )
    @api.response(code=500, description="Error while retrieving edge device list")
    def get(self) -> Any:
        """Get a list of all edge devices."""
        devices = EdgeDeviceService.load_edge_devices()
        return devices, 200

    @api.doc("add_edge_device")
    @api.expect(edge_device_creation_dao, validate=True)
    @api.marshal_with(
        edge_device_dao,
        code=201,
        description="Added new edge device",
    )
    @api.response(code=500, description="Failed to add new edge device")
    def post(self) -> Any:
        """Add a new edge device."""
        device = EdgeDeviceService.add_edge_device(
            uri=api.payload["uri"],
            name=api.payload["name"],
            device_type=api.payload["deviceType"],
        )

        return device, 201


@api.route("/<int:edge_device_id>")
@api.param(name="edge_device_id", description="The unique ID of the edge device.")
@api.response(code=404, description="The edge device ID was not found.")
class EdgeDevice(Resource):
    """Manage a single edge device."""

    @api.marshal_with(
        edge_device_dao,
        code=200,
        description="Successfully retrieved information for the edge device.",
    )
    @api.response(code=500, description="Error while retrieving edge device information")
    def get(self, edge_device_id: int) -> Any:
        """Get the information for a single edge device."""
        try:
            device = EdgeDeviceService().load_edge_device(device_id=edge_device_id)
            return device, 200
        except EdgeDeviceNotFoundError:
            return abort(code=404, message="The edge device ID was not found")

    @api.doc("delete_edge_device")
    @api.response(code=200, description="Deleted edge device")
    @api.response(code=404, description="The edge device ID was not found")
    @api.response(code=500, description="Failed to delete edge device")
    def delete(self, edge_device_id: int) -> Any:
        """Delete an edge device."""
        try:
            EdgeDeviceService().delete_edge_device(device_id=edge_device_id)

            return f"Deleted edge device with ID: {edge_device_id}", 200

        except EdgeDeviceNotFoundError:
            return abort(code=404, message="The edge device ID was not found")


@api.route("/<int:edge_device_id>/deployment/start")
@api.param(name="edge_device_id", description="The unique ID of the edge device.")
@api.response(code=404, description="The edge device ID was not found.")
class EdgeDeviceDeploymentStart(Resource):
    """Deploy models on an edge device."""

    @api.response(code=200, description="Successfully started the deployment.")
    @api.response(code=404, description="The edge device was not found.")
    @api.response(code=500, description="The deployment failed.")
    def post(self, edge_device_id: int) -> Any:
        """Deploy a model."""
        params = request.get_json()

        try:
            second_model_name = None
            if "second_model" in params and "name" in params["second_model"]:
                second_model_name = params["second_model"]["name"]

            EdgeDeviceService().start_deployment(
                device_id=edge_device_id,
                model_name=params["model"]["name"],
                second_model_name=second_model_name,
            )
        except EdgeDeviceNotFoundError:
            return "The edge device was not found", 404

        return "Successfully deployed model.", 200


@api.route("/<int:edge_device_id>/deployment/stop")
@api.param(name="edge_device_id", description="The unique ID of the edge device.")
@api.response(code=404, description="The edge device ID was not found.")
class EdgeDeviceDeploymentStop(Resource):
    """Stop deployed models on an edge device."""

    @api.response(code=200, description="Model on edge device successfully stopped.")
    @api.response(code=404, description="The edge device was not found.")
    @api.response(code=500, description="Failure while stopping the model.")
    def get(self, edge_device_id: int) -> Any:
        """Stop a model."""
        try:
            EdgeDeviceService().stop_deployment(device_id=edge_device_id)
        except EdgeDeviceNotFoundError:
            return "The edge device was not found", 404
        return "Successfully stopped model.", 200


@api.route("/<int:edge_device_id>/recording/start")
@api.param(name="edge_device_id", description="The unique ID of the edge device.")
@api.response(code=404, description="The edge device ID was not found.")
class EdgeDeviceRecordingStart(Resource):
    """Start a dataset recording on an edge device."""

    @api.response(code=200, description="Successfully started the recording.")
    @api.response(code=404, description="The edge device was not found.")
    @api.response(code=500, description="The start of the recording failed.")
    def get(self, edge_device_id: int) -> Any:
        """Start a recording."""
        try:
            EdgeDeviceService().start_recording(device_id=edge_device_id)
        except EdgeDeviceNotFoundError:
            return "The edge device was not found", 404

        return "Successfully started recording.", 200


@api.route("/<int:edge_device_id>/recording/stop")
@api.param(name="edge_device_id", description="The unique ID of the edge device.")
@api.response(code=404, description="The edge device ID was not found.")
class EdgeDeviceRecordingStop(Resource):
    """Stop a dataset recording on an edge device."""

    @api.marshal_with(
        edge_device_recording_dao,
        code=200,
        description="Successfully stopped the recording..",
    )
    @api.response(code=204, description="Got the request but the device is not recording.")
    @api.response(code=404, description="The edge device was not found.")
    @api.response(code=500, description="The stopping of the recording failed.")
    def get(self, edge_device_id: int) -> Any:
        """Stop a recording."""
        try:
            presigned_url = EdgeDeviceService().stop_recording(device_id=edge_device_id)
        except EdgeDeviceNotFoundError:
            return "The edge device was not found", 404
        except EdgeDeviceNotRecordingError:
            return "The device is not recording at the moment.", 204

        return {
            "message": "Successfully stopped the recording.",
            "download_link": presigned_url,
            "file_name": os.path.basename(presigned_url).split("?")[0],
        }, 200
