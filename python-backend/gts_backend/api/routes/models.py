# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3


"""REST API for model management."""
import logging
from typing import Any

from flask_restx import Namespace, Resource, abort
from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlflow.exceptions import RestException

from gts_backend.api.routes.flask_restx_models import (
    fp_fn_images_response,
    model_artifacts_download_link_response,
    registered_models_list_dao,
)
from gts_backend.model.structs import ModelDefaultValues, ModelSortingTypes
from gts_backend.services.models.model_service import ModelNotFoundError, ModelService

api = Namespace(name="models", description="Endpoints for model management")

logger = logging.getLogger(__name__)
filter_parser = api.parser()
filter_parser.add_argument("filter", type=str, help="search string")
filter_parser.add_argument("page", type=int, help="page of the pagination")
filter_parser.add_argument("per_page", type=int, help="Elements per page for the pagination")
filter_parser.add_argument("order_by", type=str, help="Column for sorting")
filter_parser.add_argument("direction", type=str, help="direction of sorting")


@api.route("/")
class Models(Resource):
    """Get the list of registered models."""

    @api.doc("get_models")
    @api.marshal_with(
        fields=registered_models_list_dao,
        code=200,
        description="Successfully retrieved the registered models",
    )
    @api.response(code=500, description="Failed to retrieve the registered models")
    def get(self) -> Any:
        """Get all registered models."""
        args = filter_parser.parse_args()

        models = ModelService(
            mlflow_adapter=MlflowAdapter(
                credentials=MlflowCredentials.init_from_os_environment(),
            )
        ).load_models(
            order=args.get("order_by", ModelSortingTypes.NAME),
            descending=args.get("direction") == "desc",
            page=args.get("page", ModelDefaultValues.DEFAULT_PAGE),
            per_page=args.get("per_page", ModelDefaultValues.MODELS_PER_PAGE),
            filter_str=args.get("filter", ""),
        )

        return models, 200


@api.route("/<string:model_name>")
@api.param(
    "model_name",
    description="The unique name of the model you want to delete from the registry",
)
class Model(Resource):
    """Manage a single model."""

    @api.doc("delete")
    @api.response(code=200, description="Successfully deleted model")
    @api.response(code=404, description="The model was not found")
    @api.response(code=500, description="Failed to delete model")
    def delete(self, model_name: str) -> Any:
        """Delete a single model."""
        try:
            adapter = MlflowAdapter(credentials=MlflowCredentials.init_from_os_environment())
            adapter.delete_model(model_name=model_name)

            return f"Successfully deleted model '{model_name}'", 200
        except ModelNotFoundError:  # Mlflow throws an RestException if a model is not found
            abort(code=404, message="The model was not found")

    @api.doc("get")
    @api.marshal_with(
        fields=registered_models_list_dao,
        code=200,
        description="Successfully retrieved the model",
    )
    @api.response(code=404, description="The model was not found")
    @api.response(code=500, description="Failed to retrieve model")
    def get(self, model_name: str) -> Any:
        """Retrieve a specific model."""
        try:
            models = ModelService(
                mlflow_adapter=MlflowAdapter(
                    credentials=MlflowCredentials.init_from_os_environment()
                )
            ).load_model(model_name=model_name)
            return models, 200

        except ModelNotFoundError:
            abort(code=404, message="The model was not found")


@api.route("/<string:model_name>/download")
@api.param(
    "model_name",
    description="The unique name of the model you want to get a download link for",
)
class ModelDownload(Resource):
    """Get a download link for a model."""

    @api.doc("get_download_link")
    @api.marshal_with(
        fields=model_artifacts_download_link_response,
        code=200,
        description="Successfully provided download link",
    )
    @api.response(
        code=404, description="Failed to get download link because the model was not found"
    )
    @api.response(
        code=500, description="Failed to get download link because of an internal server error"
    )
    def get(self, model_name: str) -> Any:
        """Get a download link for a model."""
        try:
            adapter = MlflowAdapter(credentials=MlflowCredentials.init_from_os_environment())

            download_url = adapter.get_download_link_for_model(
                model_name=model_name,
            )
            return {"url": download_url}, 200

        except RestException:  # Mlflow throws an RestException if a model is not found
            abort(code=404, message="The model was not found")


@api.route("/<string:model_name>/metrics")
@api.param(
    "model_name",
    description="The unique name of the model you want to retrieve metrics from the registry",
)
class ModelMetrics(Resource):
    """Get metrics of a single model."""

    @api.doc("model-metrics")
    @api.response(200, "Metrics loaded successfully")
    @api.response(code=404, description="The model was not found")
    @api.response(500, "Failed to load metrics")
    def get(self, model_name: str) -> Any:
        """Get all metrics of the model."""
        try:
            mlflow_adapter = MlflowAdapter(
                credentials=MlflowCredentials.init_from_os_environment()
            )

            metrics = mlflow_adapter.get_class_metrics(model_name)
            return metrics, 200
        except RestException:  # Mlflow throws an RestException if a model is not found
            abort(code=404, message="The model was not found")


@api.route("/<string:model_name>/fp-fn-images")
@api.param(
    "model_name",
    description="The unique name of the model you want to retrieve "
    "fp fn images from the S3 artifact storage",
)
class ModelFalsePositiveFalseNegativeImages(Resource):
    """Endpoint to retrieve fp fpn images for a single model."""

    @api.doc("fp-fn-images")
    @api.marshal_with(
        fields=fp_fn_images_response, code=200, description="FP/ FN images loaded successfully"
    )
    @api.response(code=404, description="The model was not found")
    @api.response(500, "Failed to load FP/ FN images")
    def get(self, model_name: str) -> Any:
        """GET all fp fn images for the given model."""
        try:
            presigned_urls = ModelService(
                mlflow_adapter=MlflowAdapter(
                    credentials=MlflowCredentials.init_from_os_environment(),
                ),
            ).get_fp_fn_images(model_name=model_name)

            return {"images": presigned_urls}, 200

        except ModelNotFoundError:
            abort(code=404, message="The model was not found")
