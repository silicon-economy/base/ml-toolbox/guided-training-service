# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""REST API for model testing."""

import logging
from typing import Any

from dependency_injector.wiring import Provide, inject
from flask import request
from flask_restx import Namespace, Resource
from mlflow.exceptions import RestException
from sqlalchemy.exc import NoResultFound

from gts_backend.api.containers import Container
from gts_backend.api.routes.flask_restx_models import (
    model_testing_dao,
    model_testing_response,
)
from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.services.jobs.base.configuration import BaseJobConfig
from gts_backend.services.jobs.prediction.configuration import (
    PredictionJobBaseConfig,
    PredictionJobConfig,
)
from gts_backend.services.jobs.prediction.job import PredictionJob
from gts_backend.services.labeling.service import LabelingService

api = Namespace("model_testing", description="Endpoints for model testing.")

logger = logging.getLogger(__name__)


@api.route("/")
class ModelTesting(Resource):
    """Test inference of a registered model on a given dataset."""

    @inject
    @api.doc("model_testing")
    @api.expect(model_testing_dao, validate=True)
    @api.marshal_with(
        fields=model_testing_response, code=200, description="Successfully tested the model"
    )
    @api.response(code=400, description="Failed model testing because of missing parameters")
    @api.response(code=404, description="Failed model testing because a resource was not found")
    @api.response(code=500, description="Failed to start prediction")
    def post(
        self,
        labeling_service: LabelingService = Provide[Container.labeling_service],
    ) -> Any:
        """Start model testing with the given dataset."""
        params = request.get_json()

        try:
            ls_project_id = params["labelStudioProjectId"]
            model_name = params["model_name"]
            second_model_name = params.get("second_model_name", None)
            server_endpoint = params["endpoint"]

            dataset: DatasetModelSQL = DatasetModelSQL.query.filter(
                DatasetModelSQL.ls_project_id == ls_project_id
            ).one()
            images_data = labeling_service.get_preview_images(s3_prefix=dataset.s3_prefix)

            dataset_subset = []
            for image_data in images_data:
                # each image is represented by a dict with {'url': presigned_url}
                for url, presigned_url in image_data.items():
                    # split the presigned url into the image uri and the credentials
                    dataset_subset.append(presigned_url.split("?")[0])

            job = PredictionJob(
                config=PredictionJobConfig(
                    base=BaseJobConfig(server_endpoint=server_endpoint),
                    job=PredictionJobBaseConfig(
                        model_name=model_name, second_model_name=second_model_name
                    ),
                ),
                datasets=[dataset],
                dataset_subset=dataset_subset,
            )
            predicted_images = job.run()

            response_data = {
                "labelStudioProjectId": ls_project_id,
                "modelName": model_name,
                "images": [{"url": predicted_image} for predicted_image in predicted_images],
            }
            return response_data, 200

        except NoResultFound:
            return "Failed to start model testing because the given dataset was not found", 404

        except RestException:  # mlflow throws this exception if the given model is not found
            return "Failed to start model testing because the given model was not found", 404
