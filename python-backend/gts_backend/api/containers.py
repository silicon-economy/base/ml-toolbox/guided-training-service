# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining containers used for dependency injection."""

import logging
import os
import sys
from pathlib import Path

import boto3.session
from dependency_injector import containers, providers
from label_studio_sdk import Client
from mypy_boto3_s3 import S3Client
from mypy_boto3_sts import STSClient

from gts_backend.services.labeling.service import LabelingService


class Container(containers.DeclarativeContainer):
    """Main Container bundling all dependencies used for dependency injection in other services."""

    wiring_config = containers.WiringConfiguration(
        packages=["gts_backend.api.routes", "gts_backend.services"]
    )

    # Config
    config = providers.Configuration(
        yaml_files=[
            os.environ.get(
                "PYTHON_BACKEND_CONFIG",
                os.path.abspath(os.path.join(Path(__file__).parent.parent.parent, "config.yml")),
            )
        ]
    )

    logging = providers.Resource(
        logging.basicConfig,
        stream=sys.stdout,
        level=config.log.level,
        format=config.log.format,
    )

    # Gateways
    session = providers.Resource(
        boto3.session.Session,
        aws_access_key_id=config.s3_storage.access_key_id,
        aws_secret_access_key=config.s3_storage.secret_access_key,
        region_name=config.s3_storage.region_name,
    )

    s3_storage_client: providers.Resource[S3Client] = providers.Resource(
        session.provided.client.call(),
        service_name="s3",
        endpoint_url=config.s3_storage.endpoint,
    )

    s3_sts_client: providers.Resource[STSClient] = providers.Resource(
        session.provided.client.call(),
        service_name="sts",
        endpoint_url=config.s3_storage.endpoint,
    )

    ls_client: providers.Factory[Client] = providers.Factory(
        Client, url=config.label_studio.url, api_key=config.label_studio.api_key
    )

    # Services
    labeling_service = providers.Singleton(
        LabelingService,
        ls_client=ls_client,
        s3_client=s3_storage_client,
        sts_client=s3_sts_client,
        s3_bucket_name=config.s3_storage.bucket,
        s3_endpoint_url=config.s3_storage.endpoint,
        s3_endpoint_url_external=config.s3_storage.external_endpoint,
        s3_region_name=config.s3_storage.region_name,
        s3_use_presigned_urls=config.label_studio.cloud_storage_use_presigned_urls,
    )
