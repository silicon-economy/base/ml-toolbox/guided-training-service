# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining a database model of a TrainingServer and an associated schema."""

from flask_sqlalchemy import BaseQuery

from gts_backend.services.dbmanager import db, ma


class TrainingServerModelSQL(db.Model):
    """Python data model (ORM) for a training server stored in an SQL database.

    Attributes:
        __tablename__: Name of the database model.
        server_id: Unique ID of the training server.
        endpoint: URL endpoint to reach the training server.
        server_name: Custom name.
    """

    query: BaseQuery  # for type hints
    __tablename__ = "training_server"
    server_id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    endpoint = db.Column(db.String(400), nullable=False)
    server_name = db.Column(db.String(200), nullable=False)


class TrainingServerModelSchema(ma.SQLAlchemyAutoSchema):
    """SQL Alchemy Schema for a training server."""

    class Meta:
        """Meta class.

        Attributes:
            model: Training server model.
        """

        model = TrainingServerModelSQL


training_server_schema = TrainingServerModelSchema()
training_servers_schema = TrainingServerModelSchema(many=True)
