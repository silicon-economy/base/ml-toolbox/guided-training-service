# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining a database model of an edge device and an associated schema."""

from flask_sqlalchemy import BaseQuery

from gts_backend.services.dbmanager import db, ma


class EdgeDeviceModelSQL(db.Model):
    """Python data model (ORM) for an edge device stored in an SQL database.

    Attributes:
        __tablename__: Name of the database model.
        id: Unique ID of the edge device.
        uri: URI of the edge device.
        name: Custom name.
        type: Type of the device.
        creation_date: Date when the device was added to the GTS.
    """

    query: BaseQuery  # for type hints
    __tablename__ = "edge_device"
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    uri = db.Column(db.String(400), nullable=False)
    name = db.Column(db.String(200), nullable=False)
    device_type = db.Column(db.String(200), nullable=False)
    creation_date = db.Column(db.DateTime)
    is_recording = db.Column(db.BOOLEAN, default=False)


class EdgeDeviceModelSchema(ma.SQLAlchemyAutoSchema):
    """SQL Alchemy Schema for an edge device."""

    class Meta:
        """Meta class.

        Attributes:
            model: Edge device model.
        """

        model = EdgeDeviceModelSQL


edge_device_schema = EdgeDeviceModelSchema()
edge_devices_schema = EdgeDeviceModelSchema(many=True)
