# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining a database model of a dataset and an associated schema."""

from datetime import datetime

from flask_sqlalchemy import BaseQuery

from gts_backend.services.dbmanager import db, ma


class DatasetModelSQL(db.Model):
    """Python data model (ORM) for a dataset stored in the SQL Database.

    Attributes:
        __tablename__: Name of the database model.
        ls_project_id: ID of the associated LabelStudio project.
        name: Name of the dataset.
        s3_prefix: S3 prefix of the dataset.
        task_type: Use case of the dataset e.g. classification or object detection.
        labels: Occurring labels in the dataset. Also commonly called classes.
        tags: Associated tags.
        last_modified: Date when dataset was last modified.
    """

    query: BaseQuery  # for type hints
    __tablename__ = "datasets"
    ls_project_id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(200), nullable=False)
    s3_prefix = db.Column(db.String(200), nullable=False)
    task_type = db.Column(db.String(200), nullable=False)
    labels = db.Column(db.String, nullable=False)
    tags = db.Column(db.String, nullable=False)
    last_modified = db.Column(db.DateTime)

    def __init__(
        self,
        ls_project_id: int,
        name: str,
        s3_prefix: str,
        task_type: str,
        labels: str,
        last_modified: datetime,
        tags: str,
    ):
        """Inits a DatasetModelSQL.

        Args:
            ls_project_id: ID of the associated LabelStudio project.
            name: Name of the dataset.
            s3_prefix: S3 prefix of the dataset.
            task_type: Use case of the dataset e.g. classification or object detection.
            labels: Occurring labels in the dataset. Also commonly called classes.
            tags: Associated tags.
            last_modified: Date when dataset was last modified.
        """
        self.ls_project_id = ls_project_id
        self.name = name
        self.s3_prefix = s3_prefix
        self.task_type = task_type
        self.labels = labels
        self.tags = tags
        self.last_modified = last_modified


class DatasetModelSchema(ma.SQLAlchemyAutoSchema):
    """SQL Alchemy Schema for a dataset."""

    class Meta:
        """Meta class.

        Attributes:
            model: Dataset model.
        """

        model = DatasetModelSQL


dataset_schema = DatasetModelSchema()
datasets_schema = DatasetModelSchema(many=True)
