# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module for defining recurring structs."""

from gts_backend.utils.struct_utils import StrEnum, autostr


class DatasetSortingTypes(StrEnum):
    """Specifies possibilities to order datasets."""

    TAGS = autostr()

    NAME = autostr()

    TYPE = autostr()

    DESC = autostr()


class DatasetDefaultValues:
    """Defines default values for Dataset API."""

    DATASETS_PER_PAGE = 10

    DEFAULT_PAGE = 1


class DatasetTaskType(StrEnum):
    """Possible dataset task types."""

    CLASSIFICATION = autostr()

    OBJECT_DETECTION = autostr()

    ROTATED_OBJECT_DETECTION = autostr()

    INSTANCE_SEGMENTATION = autostr()

    TEXT_RECOGNITION = autostr()

    OBJECT_TRACKING = autostr()


class DatasetExportTypes(StrEnum):
    """Specifies different dataset export types."""

    COCO = autostr()

    JSON_MIN = autostr()

    JSON = autostr()


class S3ProjectFolders(StrEnum):
    """All folders that are part of a s3 project."""

    DATA = "data"

    TASKS = "tasks"

    ANNOTATED = "annotated"

    PREANNOTATION = "preannotation"


class ModelSortingTypes:
    """Class for specifying possibilities to order datasets."""

    NAME = "name"

    ACCURACY = "accuracy"

    EPOCHS = "epochs"

    BATCH_SIZE = "batch_size"

    TRAINING_STATE = "training_state"

    ALGORITHM_TYPE = "algorithm_type"

    DESC = "desc"

    CREATION_DATE = "creation_date"


class ModelDefaultValues:
    """Class for defining default values for Dataset API."""

    MODELS_PER_PAGE = 10

    DEFAULT_PAGE = 1
