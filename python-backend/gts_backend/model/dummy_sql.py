# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining a dummy database model and an associated schema."""

from gts_backend.services.dbmanager import db, ma


class DummyModelSQL(db.Model):
    """Python data model (ORM) for a dummy model stored in the SQL Database.

    Attributes:
        __tablename__: Name of the database model.
        dummy_id: Unique ID of the model.
        description: Description text.
    """

    __tablename__ = "dummy"
    dummy_id = db.Column(db.Integer, primary_key=True, unique=True)
    description = db.Column(db.String(200))

    def __init__(self, description: str) -> None:
        """Inits a DummyModelSQL.

        Args:
            description: Description text.
        """
        self.description = description


class DummyModelSchema(ma.SQLAlchemyAutoSchema):
    """SQL Alchemy Schema for a dummy model."""

    class Meta:
        """Meta class.

        Attributes:
            model: Dummy model.
            exclude: Attribute to exclude.
        """

        model = DummyModelSQL
        exclude = ("dummy_id",)


tag_schema = DummyModelSchema()
tags_schema = DummyModelSchema(many=True)
