# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""TrainingService for starting training jobs and retrieving model configurations."""

import json
from typing import Any, Dict, List

from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingModelClassesConfig,
)

from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
    ConfigCreatorDatasetConfig,
    ModelParameterConfig,
)
from gts_backend.services.jobs.base.configuration import BaseJobConfig
from gts_backend.services.jobs.training.configuration import TrainingJobConfig
from gts_backend.services.jobs.training.job import TrainingJob


class TrainingService:
    """Service for starting training jobs and retrieving model configurations."""

    def __init__(self, params: dict[str, Any]):
        """Initialize the TrainingService."""
        self.params = params
        self.job = self.__init_training_job()

    def start_training(self) -> Any:
        """Start the training job.

        Returns:
            str: The ID of the started training job.
        """
        job_id = self.job.run()
        return job_id

    def retrieve_model_config(self) -> Dict[str, Any]:
        """Retrieve a model config for the training job.

        Returns:
            The model config as a dict.
        """
        training_job_data = self.job.create_job_data()
        return training_job_data.model_config

    def __init_training_job(self) -> TrainingJob:
        """Initialize a training job.

        Returns:
            The TrainingJob.
        """
        training_datasets = self.__load_datasets(
            dataset_ids=self.params["trainLabelStudioProjectIds"]
        )
        evaluation_datasets = self.__load_datasets(
            dataset_ids=self.params["evalLabelStudioProjectIds"]
        )

        # Advanced Mode - use model config from frontend editor:
        # If a model config is provided, parse it. The ConfigCreator
        # will use this model-config instead of building a new one.
        model_config_string = self.params.get("modelConfig", None)
        if model_config_string is not None and model_config_string != "":
            model_config = json.loads(model_config_string)
        else:
            model_config = None

        job = TrainingJob(
            config=TrainingJobConfig(
                creator=ConfigCreatorConfig(
                    parameters=ModelParameterConfig(
                        epochs=int(self.params["epochs"]),
                        batch_size=int(self.params["batchSize"]),
                        dataset_names=[dataset.name for dataset in training_datasets],
                        model=self.params["model"],
                        model_version=self.params["modelName"],
                        nms_threshold=0.5,
                        score_threshold=0.5,
                    ),
                    dataset=ConfigCreatorDatasetConfig(
                        class_mapping=ClassMappingConfig(
                            model_classes=[
                                ClassMappingModelClassesConfig(class_name=c, class_id=i)
                                for i, c in enumerate(list(self.params["classes"]))
                            ],
                            mapping=[],
                            number_model_classes=len(list(self.params["classes"])),
                        ),
                    ),
                    model_config=model_config,
                ),
                base=BaseJobConfig(
                    server_endpoint=self.params.get("endpoint", "")
                ),  # Set empty string if you just want to get the advanced config
            ),
            training_datasets=training_datasets,
            evaluating_datasets=evaluation_datasets,
            description=self.params["description"],
        )

        return job

    @staticmethod
    def __load_datasets(dataset_ids: List[str]) -> List[DatasetModelSQL]:
        """Load datasets from the database.

        Args:
            dataset_ids: The IDs of the datasets to load.

        Returns:
            The loaded datasets.
        """
        datasets = [
            DatasetModelSQL.query.where(DatasetModelSQL.ls_project_id == int(dataset_id)).one()
            for dataset_id in dataset_ids
        ]

        return datasets
