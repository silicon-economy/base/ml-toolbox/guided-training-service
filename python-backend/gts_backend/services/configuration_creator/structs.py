# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines common structs for the different ConfigCreators.

The defined Enums are used to iterate over module specific, recurring strings.
"""


class StringReplacements:
    """Replacement strings for model configurations.

    During training runtime, the strings are replaced with the actual absolute paths.
    """

    BASELINE_MODEL_DIR = "BASELINE_MODEL_DIR"
    TRAINING_OUTPUT_DIR = "TRAINING_OUTPUT_DIR"
    MEDIA_DIR = "MEDIA_DIR"
    PROJECT_ROOT_DIR = "PROJECT_ROOT_DIR"
    MMDETECTION_DIR = "MMDETECTION_DIR"


class ImplementedModels:
    """All supported models of the GTS."""

    def __init__(self) -> None:
        """Init ImplementedModels and collect all implemented models."""
        self.all = self.__collect_models()

    @staticmethod
    def __collect_models() -> set[str]:
        """Collect all implemented models from given model families and return them as a set.

        Returns:
            All implemented models as a set.
        """
        all_models: set[str] = set()
        for model_family in [
            MMDetectionModels,
            MMDetectionSegmentationModels,
            MMRotateModels,
            MMOCRModels,
            MMPretrainModels,
            YOLOXModels,
        ]:
            family_models = {
                value for key, value in model_family.__dict__.items() if not key.startswith("__")
            }
            all_models = all_models.union(family_models)
        return all_models


class YOLOXModels:
    """All supported YOLOX models."""

    S = "yolox__s"
    M = "yolox__m"
    L = "yolox__l"
    X = "yolox__x"
    TINY = "yolox__tiny"
    NANO = "yolox__nano"


class MMDetectionModels:
    """All supported models of the mlcvzoo-mmdetection package."""

    HTC = "mmdetection_object_detection__htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco"

    YOLOV3 = "mmdetection_object_detection__yolov3_d53_mstrain-608_273e_coco"

    RTMDET_TINY = (
        "mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc"
    )
    RTMDET_S = "mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e"
    RTMDET_M = "mmdetection_object_detection__rtmdet_m_8xb32-300e_coco_20220719_112220-229f527c"
    RTMDET_L = "mmdetection_object_detection__rtmdet_l_8xb32-300e_coco_20220719_112030-5a0be7c4"
    RTMDET_X = "mmdetection_object_detection__rtmdet_x_8xb32-300e_coco_20220715_230555-cc79b9ae"


class MMDetectionSegmentationModels:
    """All supported segmentation models of the mmcvzoo-mmdetection package."""

    YOLACT_R50 = "mmdetection_segmentation__yolact_r50_8x8_coco_20200908-ca34f5db"
    YOLACT_R101 = "mmdetection_segmentation__yolact_r101_1x8_coco_20200908-4cbe9101"


class MMRotateModels:
    """All supported models of the mlcvzoo-mmorotate package."""

    ROTATED_RTMDET_TINY = "mmrotate__rotated_rtmdet_tiny-3x-dota_ms-f12286ff"
    ROTATED_RTMDET_L = "mmrotate__rotated_rtmdet_l-coco_pretrain-3x-dota_ms-06d248a2"


class MMOCRModels:
    """All supported models of the mlcvzoo-mmocr package."""

    ABINET = "mmocr_text_recognition__abinet_20e_st-an_mj_20221005_012617-ead8c139"


class MMPretrainModels:
    """All supported models of the mmcvzoo-mmpretrain package."""

    EFFICIENTNET_B0 = "mmpretrain__efficientnet-b0_3rdparty_8xb32_in1k_20220119-a7e2a0b1"
    EFFICIENTNET_B4 = "mmpretrain__efficientnet-b4_3rdparty-ra-noisystudent_in1k_20221103-16ba8a2d"
    EFFICIENTNET_B8 = (
        "mmpretrain__efficientnet-b8_3rdparty_8xb32-aa-advprop_in1k_20220119-297ce1b7"
    )
