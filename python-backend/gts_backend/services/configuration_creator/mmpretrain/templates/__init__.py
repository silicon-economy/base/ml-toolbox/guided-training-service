# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines the ConfigCreator for the mlcvzoo-mmpretrain package."""

from typing import Any

from gts_backend.services.configuration_creator.mmpretrain.templates import (
    efficientnet_templates,
)
from gts_backend.services.configuration_creator.structs import MMPretrainModels

MMPRETRAIN_CONFIG_TEMPLATES: dict[str, Any] = {
    MMPretrainModels.EFFICIENTNET_B0: efficientnet_templates.create_efficientnet_b0_config,
    MMPretrainModels.EFFICIENTNET_B4: efficientnet_templates.create_efficientnet_b4_config,
    MMPretrainModels.EFFICIENTNET_B8: efficientnet_templates.create_efficientnet_b8_config,
}
