# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for specific models of the mlcvzoo-mmpretrain package."""

from typing import Any


def create_efficientnet_base_config(
    unique_name: str,
    num_classes: int,
    epochs: int,
    batch_size: int,
    initial_checkpoint: str,
) -> dict[str, Any]:
    """Create a base configuration dictionary for mmpretrain efficientnet.

    Args:
        unique_name: Unique name of the model.
        epochs: Number of training epochs.
        num_classes: Number of classes to predict.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.

    Returns:
        The mmpretrain efficientnet base config params as a dictionary.
    """
    return {
        "default_scope": "mmpretrain",
        "default_hooks": {
            "timer": {"type": "IterTimerHook"},
            "logger": {"type": "LoggerHook", "interval": 1},
            "param_scheduler": {
                "type": "ParamSchedulerHook",
            },
            "checkpoint": {
                "type": "CheckpointHook",
                "interval": 1,
                "filename_tmpl": str(unique_name) + "_{:04d}.pth",
            },
            "sampler_seed": {"type": "DistSamplerSeedHook"},
            "visualization": {"type": "VisualizationHook", "enable": False},
        },
        "env_cfg": {
            "cudnn_benchmark": False,
            "mp_cfg": {"mp_start_method": "fork", "opencv_num_threads": 0},
            "dist_cfg": {"backend": "nccl"},
        },
        "vis_backends": [{"type": "LocalVisBackend"}],
        "visualizer": {
            "type": "UniversalVisualizer",
            "vis_backends": [{"type": "LocalVisBackend"}],
        },
        "log_level": "INFO",
        "load_from": initial_checkpoint,
        "resume": False,
        "randomness": {
            "seed": 0,
            "deterministic": False,
        },
        "dataset_type": "ImageNet",
        "data_preprocessor": {
            "num_classes": num_classes,
            "mean": [123.675, 116.28, 103.53],
            "std": [58.395, 57.12, 57.375],
            "to_rgb": True,
        },
        "optim_wrapper": {
            "type": "OptimWrapper",
            "optimizer": {
                "type": "SGD",
                "lr": 0.1,
                "momentum": 0.9,
                "weight_decay": 0.0001,
            },
        },
        "param_scheduler": [
            {
                "type": "MultiStepLR",
                "by_epoch": True,
                "milestones": [int(epochs * 0.8), int(epochs * 0.9)],
                "gamma": 0.1,
            },
        ],
        "train_cfg": {
            "type": "EpochBasedTrainLoop",
            "max_epochs": epochs,
            "val_interval": 1,
        },
        "val_cfg": {},
        "test_cfg": {},
        "auto_scale": {"base_batch_size": batch_size},
        "train_pipeline": [
            {"type": "LoadImageFromFile"},
            {"type": "EfficientNetRandomCrop", "scale": 224},
            {"type": "RandomFlip", "prob": 0.5, "direction": "horizontal"},
            {"type": "PackInputs"},
        ],
        "test_pipeline": [
            {"type": "LoadImageFromFile"},
            {"type": "EfficientNetCenterCrop", "crop_size": 224},
            {"type": "PackInputs"},
        ],
        "train_dataloader": {
            "batch_size": batch_size,
            "num_workers": 10,
            "dataset": {
                "type": "CustomDataset",
                # Will be overwritten in mlcvzoo-mmpretrain with the information
                # from the yaml mlcvzoo-mmpretrain configuration.
                "data_root": "",
                # Will be overwritten in mlcvzoo-mmpretrain with the information
                # from the yaml mlcvzoo-mmpretrain configuration.
                "ann_file": "",
                "data_prefix": "",
                "with_label": True,
                # Will be overwritten by the class_mapping
                # from the mlcvzoo MMOCR configuration
                "classes": [],
                "pipeline": [
                    {"type": "LoadImageFromFile"},
                    {"type": "EfficientNetRandomCrop", "scale": 224},
                    {"type": "RandomFlip", "prob": 0.5, "direction": "horizontal"},
                    {"type": "PackInputs"},
                ],
            },
        },
        "val_dataloader": {
            "dataset": {
                "type": "CustomDataset",
                # Will be overwritten in mlcvzoo-mmpretrain with the information
                # from the yaml mlcvzoo-mmpretrain configuration.
                "data_root": "",
                # Will be overwritten in mlcvzoo-mmpretrain with the information
                # from the yaml mlcvzoo-mmpretrain configuration.
                "ann_file": "",
                "data_prefix": "",
                "with_label": True,
                # Will be overwritten by the class_mapping
                # from the mlcvzoo MMOCR configuration
                "classes": [],
                "pipeline": [
                    {"type": "LoadImageFromFile"},
                    {"type": "EfficientNetRandomCrop", "scale": 224},
                    {"type": "RandomFlip", "prob": 0.5, "direction": "horizontal"},
                    {"type": "PackInputs"},
                ],
            }
        },
        "val_evaluator": {"type": "Accuracy", "topk": (1, num_classes)},
        "test_dataloader": {
            "dataset": {
                "type": "CustomDataset",
                # Will be overwritten in mlcvzoo-mmpretrain with the information
                # from the yaml mlcvzoo-mmpretrain configuration.
                "data_root": "",
                # Will be overwritten in mlcvzoo-mmpretrain with the information
                # from the yaml mlcvzoo-mmpretrain configuration.
                "ann_file": "",
                "data_prefix": "",
                "with_label": True,
                # Will be overwritten by the class_mapping
                # from the mlcvzoo MMOCR configuration
                "classes": [],
                "pipeline": [
                    {"type": "LoadImageFromFile"},
                    {"type": "EfficientNetRandomCrop", "scale": 224},
                    {"type": "RandomFlip", "prob": 0.5, "direction": "horizontal"},
                    {"type": "PackInputs"},
                ],
            }
        },
        "test_evaluator": {"type": "Accuracy", "topk": (1, num_classes)},
    }


def create_efficientnet_b0_config(
    unique_name: str,
    num_classes: int,
    epochs: int,
    batch_size: int,
    initial_checkpoint: str,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmpretrain efficientnet b0.

    Args:
        unique_name: Unique name of the model.
        epochs: Number of maximum training epochs.
        num_classes: Number of classes to predict.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.

    Returns:
        The mmpretrain imagenet b0 config params as a dictionary.
    """
    config = create_efficientnet_base_config(
        unique_name, num_classes, epochs, batch_size, initial_checkpoint
    )
    config["model"] = {
        "type": "ImageClassifier",
        "backbone": {"type": "EfficientNet", "arch": "b0"},
        "neck": {
            "type": "GlobalAveragePooling",
        },
        "head": {
            "type": "LinearClsHead",
            "num_classes": num_classes,
            "in_channels": 1280,
            "loss": {"type": "CrossEntropyLoss", "loss_weight": 1.0},
            "topk": [1, 5],
        },
    }

    return config


def create_efficientnet_b4_config(
    unique_name: str,
    num_classes: int,
    epochs: int,
    batch_size: int,
    initial_checkpoint: str,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmpretrain efficientnet b4.

    Args:
        unique_name: Unique name of the model.
        epochs: Number of maximum training epochs.
        num_classes: Number of classes to predict.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.

    Returns:
        The mmpretrain imagenet b0 config params as a dictionary.
    """
    config = create_efficientnet_base_config(
        unique_name, num_classes, epochs, batch_size, initial_checkpoint
    )
    config["model"] = {
        "type": "ImageClassifier",
        "backbone": {"type": "EfficientNet", "arch": "b4"},
        "neck": {
            "type": "GlobalAveragePooling",
        },
        "head": {
            "type": "LinearClsHead",
            "num_classes": num_classes,
            "in_channels": 1792,
            "loss": {"type": "CrossEntropyLoss", "loss_weight": 1.0},
            "topk": [1, 5],
        },
    }

    return config


def create_efficientnet_b8_config(
    unique_name: str,
    num_classes: int,
    epochs: int,
    batch_size: int,
    initial_checkpoint: str,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmpretrain efficientnet b8.

    Args:
        unique_name: Unique name of the model.
        epochs: Number of maximum training epochs.
        num_classes: Number of classes to predict.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.

    Returns:
        The mmpretrain imagenet b0 config params as a dictionary.
    """
    config = create_efficientnet_base_config(
        unique_name=unique_name,
        num_classes=num_classes,
        epochs=epochs,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
    )
    config["model"] = {
        "type": "ImageClassifier",
        "backbone": {"type": "EfficientNet", "arch": "b8"},
        "neck": {
            "type": "GlobalAveragePooling",
        },
        "head": {
            "type": "LinearClsHead",
            "num_classes": num_classes,
            "in_channels": 2816,
            "loss": {"type": "CrossEntropyLoss", "loss_weight": 1.0},
            "topk": [1, 5],
        },
    }

    return config
