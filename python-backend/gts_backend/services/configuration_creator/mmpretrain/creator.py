# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines the MMPretrainConfigCreator class.

The class is used to create model configs for models that can be trained with the mlcvzoo-mmpretrain
package.
"""

import logging
from typing import Any, Optional

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_mmpretrain.configuration import (
    MMConfig,
    MMPretrainConfig,
    MMPretrainInferenceConfig,
    MMPretrainTrainArgparseConfig,
    MMPretrainTrainConfig,
    MMPretrainTrainDatasetConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
)
from gts_backend.services.configuration_creator.base.creator import ConfigCreator
from gts_backend.services.configuration_creator.mmpretrain.templates import (
    MMPRETRAIN_CONFIG_TEMPLATES,
)

logger = logging.getLogger(__name__)


class MMPretrainConfigCreator(ConfigCreator[MMPretrainConfig]):
    """Creator for a config to train a model with the mlcvzoo-mmpretrain package.

    Attributes:
        configuration: Configuration of the MMPretrainConfigCreator.
    """

    def __init__(
        self,
        configuration: ConfigCreatorConfig,
    ):
        """Inits MMPretrainConfigCreator with its configuration.

        Args:
            configuration: Configuration for the MMPretrainConfigCreator.
        """
        ConfigCreator.__init__(self, configuration=configuration)

    def create_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        s3_eval_prefixes: Optional[list[str]] = None,
    ) -> MMPretrainConfig:
        """Create MMPretrain model config with given S3 objects as training data.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of training data.
            s3_eval_prefixes: List of S3 prefixes of evaluation data.

        Returns:
            An MMPretrain model config.
        """
        unique_name = self._create_unique_name()

        logger.debug("Create MMPretrain-specific model config for model %s", unique_name)

        if not s3_eval_prefixes:
            raise ValueError("s3_eval_prefixes is mandatory for mmocr models.")

        return self.__create_base_model_config(
            bucket_name=bucket_name,
            s3_prefixes=s3_prefixes,
            s3_eval_prefixes=s3_eval_prefixes,
            unique_name=unique_name,
        )

    def __create_base_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        s3_eval_prefixes: list[str],
        unique_name: str,
    ) -> MMPretrainConfig:
        """Create MMPretrain model config with given S3 objects as training data from its base parameters.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of training data.

        Returns:
            A MMPretrainConfig model config.
        """
        annotation_handler_config: AnnotationHandlerConfig = self.create_annotation_handler_config(
            bucket_name, s3_prefixes
        )

        eval_annotation_handler_config: AnnotationHandlerConfig = (
            self.create_annotation_handler_config(bucket_name, s3_eval_prefixes)
        )

        mm_config = MMConfig(
            config_dict=self.__get_mmpretrain_template(
                epochs=self.configuration.parameters.epochs,
                unique_name=unique_name,
                num_classes=self.configuration.dataset.class_mapping.get_number_classes(),
                batch_size=self.configuration.parameters.batch_size,
                initial_checkpoint=f"{self.relative_initial_checkpoint_path(bucket_name=bucket_name)}/"
                f"{self.configuration.parameters.model}.pth",
            )
        )

        return MMPretrainConfig(
            unique_name=unique_name,
            class_mapping=self.configuration.dataset.class_mapping,
            inference_config=MMPretrainInferenceConfig(
                checkpoint_path="",
                score_threshold=self.configuration.parameters.score_threshold,
            ),
            train_config=MMPretrainTrainConfig(
                argparse_config=MMPretrainTrainArgparseConfig(
                    work_dir=self._create_training_output_dir()
                ),
                best_metric_name="val/accuracy/top1",
                dataset_config=MMPretrainTrainDatasetConfig(
                    train_annotation_handler_config=annotation_handler_config,
                    val_annotation_handler_config=eval_annotation_handler_config,
                    test_annotation_handler_config=eval_annotation_handler_config,
                ),
            ),
            mm_config=mm_config,
        )

    def __get_mmpretrain_template(
        self,
        unique_name: str,
        num_classes: int,
        epochs: int,
        batch_size: int,
        initial_checkpoint: str,
    ) -> dict[str, Any]:
        """Infer a template config dictionary for a specific mmpretrain model from a given model name.

        Args:
            unique_name: Unique name of the model.
            epochs: Number of maximum training epochs.
            num_classes: Number of classes to predict.
            batch_size: Number of samples in a batch.
            initial_checkpoint: The initial model checkpoint from which to resume training.

        Returns:
            template_dict: The template config dictionary.

        Raises:
            ValueError: No valid mmpretrain model config template could be
                        inferred from the given name.

        """
        template = next(
            (
                _template
                for valid_model, _template in MMPRETRAIN_CONFIG_TEMPLATES.items()
                if valid_model == self.configuration.parameters.model
            ),
            None,
        )

        if template is None:
            raise ValueError(
                "Could not find a template for model=%s", self.configuration.parameters.model
            )

        return template(  # type: ignore[no-any-return]
            unique_name=unique_name,
            num_classes=num_classes,
            epochs=epochs,
            batch_size=batch_size,
            initial_checkpoint=initial_checkpoint,
        )
