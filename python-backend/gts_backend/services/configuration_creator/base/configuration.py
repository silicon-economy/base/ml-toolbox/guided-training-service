# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the configuration of the base ConfigCreator class."""

from typing import Any, Dict, Optional

import related
from attr import define
from config_builder import BaseConfigClass
from mlcvzoo_base.configuration.class_mapping_config import ClassMappingConfig


@define
class ModelParameterConfig(BaseConfigClass):
    """Class for parsing the basic parameters every user must provide."""

    __related_strict__ = True

    epochs: int = related.IntegerField()

    batch_size: int = related.IntegerField()

    dataset_names: list[str] = related.SequenceField(cls=str)

    # TODO: Rename to baseline_model
    model: str = related.StringField()

    # TODO: Rename to unique_name
    model_version: str = related.StringField()

    nms_threshold: float = related.FloatField()

    score_threshold: float = related.FloatField()


@define
class ConfigCreatorDatasetConfig(BaseConfigClass):
    """Class for parsing the basic dataset related parameters every user must provide."""

    __related_strict__ = True

    class_mapping: ClassMappingConfig = related.ChildField(cls=ClassMappingConfig)


@define
class ConfigCreatorConfig(BaseConfigClass):
    """Class for parsing all parameters provided by user."""

    __related_strict__ = True

    parameters: ModelParameterConfig = related.ChildField(cls=ModelParameterConfig, required=True)

    dataset: ConfigCreatorDatasetConfig = related.ChildField(
        cls=ConfigCreatorDatasetConfig, required=True
    )

    model_config: Optional[Dict[str, Any]] = related.ChildField(
        cls=dict, required=False, default=None
    )
