# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines the base abstract ConfigCreator class.

The class is used to inherit MLCVZoo package-specific Creators e.g. a YoloxCreator for
the mlcvzoo-yolox package.
"""

import logging
import os
from abc import ABC, abstractmethod
from typing import Generic, Optional, TypeVar

from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.configuration.annotation_handler_config import (
    AnnotationHandlerConfig,
    AnnotationHandlerLabelStudioInputDataConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
)
from gts_backend.services.configuration_creator.structs import StringReplacements
from gts_backend.services.jobs.training.structs import DataRootDirs

logger = logging.getLogger(__name__)


ConfigurationType = TypeVar("ConfigurationType", bound=ModelConfiguration)


class ConfigCreator(ABC, Generic[ConfigurationType]):
    """Abstract base creator for creating MLCVZoo model configurations.

    Attributes:
        configuration: Configuration of the ConfigCreator.
    """

    def __init__(
        self,
        configuration: ConfigCreatorConfig,
    ):
        """Inits ConfigCreator with its configuration.

        Args:
            configuration: Configuration for the ConfigCreator.
        """
        self.configuration: ConfigCreatorConfig = configuration

    @staticmethod
    def relative_initial_checkpoint_path(bucket_name: str) -> str:
        """Returns a relative initial checkpoint path.

        Args:
            bucket_name: Name of the bucket in which the initial checkpoint is stored.

        Returns:
            Relative path to the initial checkpoint.
        """
        return (
            f"{StringReplacements.BASELINE_MODEL_DIR}/{bucket_name}/"
            f"{DataRootDirs.INITIAL_CHECKPOINT_DIR}"
        )

    @abstractmethod
    def create_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        s3_eval_prefixes: Optional[list[str]] = None,
    ) -> ConfigurationType:
        """Creates model config with given S3 objects as training data.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of the training data.
            s3_eval_prefixes: List of S3 prefixes of the evaluation data.

        Returns:
            A model config.
        """
        raise NotImplementedError("Must be implemented by sub-class: create_model_config(...)!")

    def _create_unique_name(self) -> str:
        return self.configuration.parameters.model_version.rstrip()

    def _create_training_output_dir(self) -> str:
        return os.path.join(
            f"{StringReplacements.TRAINING_OUTPUT_DIR}",
            f"{self._create_unique_name()}",
        )

    def create_annotation_handler_config(
        self, bucket_name: str, s3_prefixes: list[str]
    ) -> AnnotationHandlerConfig:
        """Generate instance of an AnnotationHandlerConfig.

        Args:
            bucket_name: S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of the training data.

        Returns:
            The AnnotationHandlerConfig.
        """
        logger.debug("Generate instance of AnnotationHandlerConfig")

        ls_input_data: list[AnnotationHandlerLabelStudioInputDataConfig] = []
        for prefix in s3_prefixes:
            ls_input_data.append(
                AnnotationHandlerLabelStudioInputDataConfig(
                    input_image_dir=f"{StringReplacements.MEDIA_DIR}/"
                    f"{bucket_name}/{prefix}/data",
                    input_annotation_dir=f"{StringReplacements.MEDIA_DIR}/"
                    f"{bucket_name}/{prefix}/tasks",
                    image_format=".jpg|.png|.jpeg",
                )
            )

        # TODO: Use correct image format
        config = AnnotationHandlerConfig(
            class_mapping=self.configuration.dataset.class_mapping,
            label_studio_input_data=ls_input_data,
        )

        return config
