# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines the MMDetectionConfigCreator class.

The class is used to create model configs for models that can be trained with the mlcvzoo-mmdetection
package.
"""

import logging
from typing import Any, Optional

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_mmdetection.configuration import (
    MMConfig,
    MMDetectionConfig,
    MMDetectionInferenceConfig,
    MMDetectionTrainArgparseConfig,
    MMDetectionTrainConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
)
from gts_backend.services.configuration_creator.base.creator import ConfigCreator
from gts_backend.services.configuration_creator.mmdetection.templates import (
    MMDETECTION_CONFIG_TEMPLATES,
    MMDETECTION_SEGMENTATION_CONFIG_TEMPLATES,
    MMROTATE_CONFIG_TEMPLATES,
)

logger = logging.getLogger(__name__)


class MMDetectionConfigCreator(ConfigCreator[MMDetectionConfig]):
    """Creator for a config to train a model with the mlcvzoo-mmdetection package.

    Attributes:
        configuration: Configuration of the MMDetectionConfigCreator
    """

    def __init__(
        self,
        configuration: ConfigCreatorConfig,
    ):
        """Inits MMDetectionConfigCreator with its configuration.

        Args:
            configuration: Configuration for the MMDetectionConfigCreator.
        """
        ConfigCreator.__init__(self, configuration=configuration)

    def create_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        s3_eval_prefixes: Optional[list[str]] = None,
    ) -> MMDetectionConfig:
        """Create MMDetection model config with given S3 objects as training data.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of training data.
            s3_eval_prefixes: List of S3 prefixes of the evaluation data.

        Returns:
            A MMDetection model config.
        """
        unique_name = self._create_unique_name()

        logger.debug("Create MMDetection-specific model config for model %s", unique_name)

        return self.__create_base_model_config(
            bucket_name=bucket_name,
            s3_prefixes=s3_prefixes,
            unique_name=unique_name,
        )

    def __create_base_model_config(
        self, bucket_name: str, s3_prefixes: list[str], unique_name: str
    ) -> MMDetectionConfig:
        """Create MMDetection model config with given S3 objects as training data from its base parameters.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of training data.

        Returns:
            A MMDetection model config.
        """
        annotation_handler_config: AnnotationHandlerConfig = self.create_annotation_handler_config(
            bucket_name, s3_prefixes
        )

        mm_config = MMConfig(
            config_dict=self.__get_mm_template(
                epochs=self.configuration.parameters.epochs,
                unique_name=unique_name,
                num_classes=self.configuration.dataset.class_mapping.get_number_classes(),
                nms_threshold=self.configuration.parameters.nms_threshold,
                batch_size=self.configuration.parameters.batch_size,
                initial_checkpoint=f"{self.relative_initial_checkpoint_path(bucket_name=bucket_name)}/"
                f"{self.configuration.parameters.model}.pth",
                # overwrite with selected labelstudio dataset
                annotation_handler_config=annotation_handler_config,
            )
        )

        return MMDetectionConfig(
            unique_name=unique_name,
            class_mapping=self.configuration.dataset.class_mapping,
            inference_config=MMDetectionInferenceConfig(
                checkpoint_path="",
                score_threshold=self.configuration.parameters.score_threshold,
            ),
            train_config=MMDetectionTrainConfig(
                argparse_config=MMDetectionTrainArgparseConfig(
                    work_dir=self._create_training_output_dir()
                ),
            ),
            mm_config=mm_config,
        )

    def __get_mm_template(
        self,
        unique_name: str,
        epochs: int,
        num_classes: int,
        nms_threshold: float,
        batch_size: int,
        initial_checkpoint: str,
        annotation_handler_config: AnnotationHandlerConfig,
    ) -> dict[str, Any]:
        """Infer a template config dictionary for a specific mmdetection model from a given model name.

        Args:
            unique_name: Unique name of the model.
            epochs: Number of maximum training epochs.
            num_classes: Number of classes to predict.
            nms_threshold: The nms threshold for class predictions.
            batch_size: Number of samples in a batch.
            initial_checkpoint: The initial model checkpoint from which to resume training.
            annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

        Returns:
            template_dict: The template config dictionary.

        Raises:
            ValueError: No valid mmdetection model config template could be inferred from the given
                        name.

        """
        model = self.configuration.parameters.model

        valid_models = {
            **MMDETECTION_CONFIG_TEMPLATES,
            **MMDETECTION_SEGMENTATION_CONFIG_TEMPLATES,
            **MMROTATE_CONFIG_TEMPLATES,
        }

        if template := valid_models.get(model):
            return template(  # type: ignore[no-any-return]
                unique_name=unique_name,
                epochs=epochs,
                num_classes=num_classes,
                nms_threshold=nms_threshold,
                batch_size=batch_size,
                initial_checkpoint=initial_checkpoint,
                annotation_handler_config=annotation_handler_config,
            )

        raise ValueError("Could not find a template for model=%s", model)
