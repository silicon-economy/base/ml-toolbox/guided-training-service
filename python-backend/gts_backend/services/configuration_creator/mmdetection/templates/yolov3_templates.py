# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for specific models of the mlcvzoo-mmdetection package."""
from typing import Any

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig


def create_mm_yolov3_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection yolov3.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection yolov3 config params as a dictionary.
    """
    return {
        "train_cfg": {
            "type": "EpochBasedTrainLoop",
            "max_epochs": epochs,
            "val_interval": 100,
        },
        "val_cfg": None,
        "test_cfg": {"type": "TestLoop"},
        "param_scheduler": [
            {
                "type": "LinearLR",
                "start_factor": 0.1,
                "by_epoch": False,
                "begin": 0,
                "end": 2000,
            },
            {
                "type": "MultiStepLR",
                "by_epoch": True,
                "milestones": [int(epochs * 0.8), int(epochs * 0.9)],
                "gamma": 0.1,
            },
        ],
        "optim_wrapper": {
            "type": "OptimWrapper",
            "optimizer": {
                "type": "SGD",
                "lr": 0.001,
                "momentum": 0.9,
                "weight_decay": 0.0005,
            },
            "clip_grad": {"max_norm": 35, "norm_type": 2},
        },
        "auto_scale_lr": {"enable": False, "base_batch_size": 64},
        "default_scope": "mmdet",
        "default_hooks": {
            "timer": {"type": "IterTimerHook"},
            "logger": {"type": "LoggerHook", "interval": 1},
            "param_scheduler": {"type": "ParamSchedulerHook"},
            "checkpoint": {
                "type": "CheckpointHook",
                "interval": 1,
                "filename_tmpl": str(unique_name) + "_{:04d}.pth",
            },
            "sampler_seed": {"type": "DistSamplerSeedHook"},
            "visualization": {"type": "DetVisualizationHook"},
        },
        "env_cfg": {
            "cudnn_benchmark": False,
            "mp_cfg": {"mp_start_method": "fork", "opencv_num_threads": 0},
            "dist_cfg": {"backend": "nccl"},
        },
        "vis_backends": [{"type": "LocalVisBackend"}],
        "visualizer": {
            "type": "DetLocalVisualizer",
            "vis_backends": [{"type": "LocalVisBackend"}],
            "name": "visualizer",
        },
        "log_processor": {"type": "LogProcessor", "window_size": 50, "by_epoch": True},
        "log_level": "INFO",
        "resume": None,
        # https://download.openmmlab.com/mmdetection/v2.0/yolo/yolov3_d53_mstrain-608_273e_coco/yolov3_d53_mstrain-608_273e_coco_20210518_115020-a2c3acb8.pth
        "load_from": initial_checkpoint,
        "data_preprocessor": {
            "type": "DetDataPreprocessor",
            "mean": [0, 0, 0],
            "std": [255.0, 255.0, 255.0],
            "bgr_to_rgb": True,
            "pad_size_divisor": 32,
        },
        "model": {
            "type": "YOLOV3",
            "data_preprocessor": {
                "type": "DetDataPreprocessor",
                "mean": [0, 0, 0],
                "std": [255.0, 255.0, 255.0],
                "bgr_to_rgb": True,
                "pad_size_divisor": 32,
            },
            "backbone": {
                "type": "Darknet",
                "depth": 53,
                "out_indices": [3, 4, 5],
                "init_cfg": {
                    "type": "Pretrained",
                    "checkpoint": "open-mmlab://darknet53",
                },
            },
            "neck": {
                "type": "YOLOV3Neck",
                "num_scales": 3,
                "in_channels": [1024, 512, 256],
                "out_channels": [512, 256, 128],
            },
            "bbox_head": {
                "type": "YOLOV3Head",
                "num_classes": num_classes,
                "in_channels": [512, 256, 128],
                "out_channels": [1024, 512, 256],
                "anchor_generator": {
                    "type": "YOLOAnchorGenerator",
                    "base_sizes": [
                        [[116, 90], [156, 198], [373, 326]],
                        [[30, 61], [62, 45], [59, 119]],
                        [[10, 13], [16, 30], [33, 23]],
                    ],
                    "strides": [32, 16, 8],
                },
                "bbox_coder": {"type": "YOLOBBoxCoder"},
                "featmap_strides": [32, 16, 8],
                "loss_cls": {
                    "type": "CrossEntropyLoss",
                    "use_sigmoid": True,
                    "loss_weight": 1.0,
                    "reduction": "sum",
                },
                "loss_conf": {
                    "type": "CrossEntropyLoss",
                    "use_sigmoid": True,
                    "loss_weight": 1.0,
                    "reduction": "sum",
                },
                "loss_xy": {
                    "type": "CrossEntropyLoss",
                    "use_sigmoid": True,
                    "loss_weight": 2.0,
                    "reduction": "sum",
                },
                "loss_wh": {"type": "MSELoss", "loss_weight": 2.0, "reduction": "sum"},
            },
            "train_cfg": {
                "assigner": {
                    "type": "GridAssigner",
                    "pos_iou_thr": 0.5,
                    "neg_iou_thr": 0.5,
                    "min_pos_iou": 0,
                }
            },
            "test_cfg": {
                "nms_pre": 1000,
                "min_bbox_size": 0,
                "score_thr": 0.05,
                "conf_thr": 0.005,
                "nms": {"type": "nms", "iou_threshold": nms_threshold},
                "max_per_img": 100,
            },
        },
        "dataset_type": "CocoDataset",
        "data_root": "data/coco/",
        "backend_args": None,
        "train_pipeline": [
            {"type": "LoadImageFromFile", "backend_args": None},
            {"type": "LoadAnnotations", "with_bbox": True},
            {
                "type": "Expand",
                "mean": [0, 0, 0],
                "to_rgb": True,
                "ratio_range": [1, 2],
            },
            {
                "type": "MinIoURandomCrop",
                "min_ious": [0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
                "min_crop_size": 0.3,
            },
            {
                "type": "RandomResize",
                "scale": [(320, 320), (608, 608)],
                "keep_ratio": True,
            },
            {"type": "RandomFlip", "prob": 0.5},
            {"type": "PhotoMetricDistortion"},
            {"type": "PackDetInputs"},
        ],
        "test_pipeline": [
            {"type": "LoadImageFromFile", "backend_args": None},
            {"type": "Resize", "scale": (608, 608), "keep_ratio": True},
            {"type": "LoadAnnotations", "with_bbox": True},
            {
                "type": "PackDetInputs",
                "meta_keys": [
                    "img_id",
                    "img_path",
                    "ori_shape",
                    "img_shape",
                    "scale_factor",
                ],
            },
        ],
        "train_dataloader": {
            "batch_size": batch_size,
            "num_workers": 2,
            "persistent_workers": True,
            "sampler": {"type": "DefaultSampler", "shuffle": True},
            "batch_sampler": {"type": "AspectRatioBatchSampler"},
            "dataset": {
                "type": "MLCVZooMMDetDataset",
                "annotation_handler_config": annotation_handler_config.to_dict(),
                "filter_cfg": {"filter_empty_gt": True, "min_size": 32},
                "pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "LoadAnnotations", "with_bbox": True},
                    {
                        "type": "Expand",
                        "mean": [0, 0, 0],
                        "to_rgb": True,
                        "ratio_range": [1, 2],
                    },
                    {
                        "type": "MinIoURandomCrop",
                        "min_ious": [0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
                        "min_crop_size": 0.3,
                    },
                    {
                        "type": "RandomResize",
                        "scale": [(320, 320), (608, 608)],
                        "keep_ratio": True,
                    },
                    {"type": "RandomFlip", "prob": 0.5},
                    {"type": "PhotoMetricDistortion"},
                    {"type": "PackDetInputs"},
                ],
                "backend_args": None,
            },
        },
        "val_dataloader": None,
        "test_dataloader": {
            "batch_size": 1,
            "num_workers": 2,
            "persistent_workers": True,
            "drop_last": False,
            "sampler": {"type": "DefaultSampler", "shuffle": False},
            "dataset": {
                "type": "CocoDataset",
                "data_root": "data/coco/",
                "ann_file": "annotations/instances_val2017.json",
                "data_prefix": {"img": "val2017/"},
                "test_mode": True,
                "pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "Resize", "scale": (608, 608), "keep_ratio": True},
                    {"type": "LoadAnnotations", "with_bbox": True},
                    {
                        "type": "PackDetInputs",
                        "meta_keys": [
                            "img_id",
                            "img_path",
                            "ori_shape",
                            "img_shape",
                            "scale_factor",
                        ],
                    },
                ],
                "backend_args": None,
            },
        },
        "val_evaluator": None,
        "test_evaluator": {
            "type": "CocoMetric",
            "ann_file": "data/coco/annotations/instances_val2017.json",
            "metric": "bbox",
            "backend_args": None,
        },
    }
