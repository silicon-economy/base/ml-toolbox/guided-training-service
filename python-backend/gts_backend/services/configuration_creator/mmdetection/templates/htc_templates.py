# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for specific models of the mlcvzoo-mmdetection package."""
from typing import Any

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig


def create_mm_htc_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection htc model.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection htc config params as a dictionary.
    """
    return {
        "dataset_type": "CocoDataset",
        "data_root": "data/coco/",
        "backend_args": None,
        "train_pipeline": [
            {"type": "LoadImageFromFile"},
            {
                "type": "LoadAnnotations",
                "with_bbox": True,
                "with_mask": True,
                "with_seg": True,
            },
            {
                "type": "RandomResize",
                "scale": [(1600, 400), (1600, 1400)],
                "keep_ratio": True,
            },
            {"type": "RandomFlip", "prob": 0.5},
            {"type": "PackDetInputs"},
        ],
        "test_pipeline": [
            {"type": "LoadImageFromFile", "backend_args": None},
            {"type": "Resize", "scale": (1333, 800), "keep_ratio": True},
            {"type": "LoadAnnotations", "with_bbox": True, "with_mask": True},
            {
                "type": "PackDetInputs",
                "meta_keys": (
                    "img_id",
                    "img_path",
                    "ori_shape",
                    "img_shape",
                    "scale_factor",
                ),
            },
        ],
        "train_dataloader": {
            "batch_size": batch_size,
            "num_workers": 1,
            "persistent_workers": True,
            "sampler": {"type": "DefaultSampler", "shuffle": True},
            "batch_sampler": {"type": "AspectRatioBatchSampler"},
            "dataset": {
                "type": "MLCVZooMMDetDataset",
                "data_root": "data/coco/",
                "ann_file": "annotations/instances_train2017.json",
                "data_prefix": {
                    "img": "train2017/",
                    "seg": "stuffthingmaps/train2017/",
                },
                "filter_cfg": {"filter_empty_gt": True, "min_size": 32},
                "pipeline": [
                    {"type": "LoadImageFromFile"},
                    {
                        "type": "LoadAnnotations",
                        "with_bbox": True,
                        "with_mask": False,
                        "with_seg": False,
                    },
                    {
                        "type": "RandomResize",
                        "scale": [(1600, 400), (1600, 1400)],
                        "keep_ratio": True,
                    },
                    {"type": "RandomFlip", "prob": 0.5},
                    {"type": "PackDetInputs"},
                ],
                "backend_args": None,
                "annotation_handler_config": annotation_handler_config.to_dict(),
            },
        },
        "val_dataloader": None,
        "test_dataloader": {
            "batch_size": batch_size,
            "num_workers": 2,
            "persistent_workers": True,
            "drop_last": False,
            "sampler": {"type": "DefaultSampler", "shuffle": False},
            "dataset": {
                "type": "CocoDataset",
                "data_root": "data/coco/",
                "ann_file": "annotations/instances_val2017.json",
                "data_prefix": {"img": "val2017/"},
                "test_mode": True,
                "pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "Resize", "scale": (1333, 800), "keep_ratio": True},
                    {"type": "LoadAnnotations", "with_bbox": True, "with_mask": True},
                    {
                        "type": "PackDetInputs",
                        "meta_keys": (
                            "img_id",
                            "img_path",
                            "ori_shape",
                            "img_shape",
                            "scale_factor",
                        ),
                    },
                ],
                "backend_args": None,
            },
        },
        "val_evaluator": None,
        "test_evaluator": {
            "type": "CocoMetric",
            "ann_file": "data/coco/annotations/instances_val2017.json",
            "metric": ["bbox", "segm"],
            "format_only": False,
            "backend_args": None,
        },
        "train_cfg": {
            "type": "EpochBasedTrainLoop",
            "max_epochs": epochs,
            "val_interval": 1,
        },
        "val_cfg": None,
        "test_cfg": {"type": "TestLoop"},
        "param_scheduler": [
            {
                "type": "LinearLR",
                "start_factor": 0.001,
                "by_epoch": False,
                "begin": 0,
                "end": 500,
            },
            {
                "type": "MultiStepLR",
                "begin": 0,
                "end": 20,
                "by_epoch": True,
                "milestones": [int(epochs * 0.8), int(epochs * 0.9)],
                "gamma": 0.1,
            },
        ],
        "optim_wrapper": {
            "type": "OptimWrapper",
            "optimizer": {
                "type": "SGD",
                "lr": 0.02,
                "momentum": 0.9,
                "weight_decay": 0.0001,
            },
        },
        "auto_scale_lr": None,
        "default_scope": "mmdet",
        "default_hooks": {
            "timer": {"type": "IterTimerHook"},
            "logger": {"type": "LoggerHook", "interval": 1},
            "param_scheduler": {"type": "ParamSchedulerHook"},
            "checkpoint": {
                "type": "CheckpointHook",
                "interval": 1,
                "filename_tmpl": str(unique_name) + "_{:04d}.pth",
            },
            "sampler_seed": {"type": "DistSamplerSeedHook"},
            "visualization": {"type": "DetVisualizationHook"},
        },
        "env_cfg": {
            "cudnn_benchmark": False,
            "mp_cfg": {"mp_start_method": "fork", "opencv_num_threads": 0},
            "dist_cfg": {"backend": "nccl"},
        },
        "vis_backends": [{"type": "LocalVisBackend"}],
        "visualizer": {
            "type": "DetLocalVisualizer",
            "vis_backends": [{"type": "LocalVisBackend"}],
            "name": "visualizer",
        },
        "log_processor": {"type": "LogProcessor", "window_size": 50, "by_epoch": True},
        "log_level": "INFO",
        # https://download.openmmlab.com/mmdetection/v2.0/htc/htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco/htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco_20200312-946fd751.pth
        "load_from": initial_checkpoint,
        "resume": False,
        "model": {
            "type": "HybridTaskCascade",
            "data_preprocessor": {
                "type": "DetDataPreprocessor",
                "mean": [123.675, 116.28, 103.53],
                "std": [58.395, 57.12, 57.375],
                "bgr_to_rgb": True,
                "pad_size_divisor": 32,
                "pad_seg": True,
            },
            "backbone": {
                "type": "ResNeXt",
                "depth": 101,
                "num_stages": 4,
                "out_indices": (0, 1, 2, 3),
                "frozen_stages": 1,
                "norm_cfg": {"type": "BN", "requires_grad": True},
                "norm_eval": True,
                "style": "pytorch",
                "init_cfg": {
                    "type": "Pretrained",
                    "checkpoint": "open-mmlab://resnext101_64x4d",
                },
                "groups": 64,
                "base_width": 4,
                "dcn": {"type": "DCN", "deform_groups": 1, "fallback_on_stride": False},
                "stage_with_dcn": (False, True, True, True),
            },
            "neck": {
                "type": "FPN",
                "in_channels": [256, 512, 1024, 2048],
                "out_channels": 256,
                "num_outs": 5,
            },
            "rpn_head": {
                "type": "RPNHead",
                "in_channels": 256,
                "feat_channels": 256,
                "anchor_generator": {
                    "type": "AnchorGenerator",
                    "scales": [8],
                    "ratios": [0.5, 1.0, 2.0],
                    "strides": [4, 8, 16, 32, 64],
                },
                "bbox_coder": {
                    "type": "DeltaXYWHBBoxCoder",
                    "target_means": [0.0, 0.0, 0.0, 0.0],
                    "target_stds": [1.0, 1.0, 1.0, 1.0],
                },
                "loss_cls": {
                    "type": "CrossEntropyLoss",
                    "use_sigmoid": True,
                    "loss_weight": 1.0,
                },
                "loss_bbox": {
                    "type": "SmoothL1Loss",
                    "beta": 0.1111111111111111,
                    "loss_weight": 1.0,
                },
            },
            "roi_head": {
                "type": "HybridTaskCascadeRoIHead",
                "interleaved": True,
                "mask_info_flow": True,
                "num_stages": 3,
                "stage_loss_weights": [1, 0.5, 0.25],
                "bbox_roi_extractor": {
                    "type": "SingleRoIExtractor",
                    "roi_layer": {
                        "type": "RoIAlign",
                        "output_size": 7,
                        "sampling_ratio": 0,
                    },
                    "out_channels": 256,
                    "featmap_strides": [4, 8, 16, 32],
                },
                "bbox_head": [
                    {
                        "type": "Shared2FCBBoxHead",
                        "in_channels": 256,
                        "fc_out_channels": 1024,
                        "roi_feat_size": 7,
                        "num_classes": num_classes,
                        "bbox_coder": {
                            "type": "DeltaXYWHBBoxCoder",
                            "target_means": [0.0, 0.0, 0.0, 0.0],
                            "target_stds": [0.1, 0.1, 0.2, 0.2],
                        },
                        "reg_class_agnostic": True,
                        "loss_cls": {
                            "type": "CrossEntropyLoss",
                            "use_sigmoid": False,
                            "loss_weight": 1.0,
                        },
                        "loss_bbox": {
                            "type": "SmoothL1Loss",
                            "beta": 1.0,
                            "loss_weight": 1.0,
                        },
                    },
                    {
                        "type": "Shared2FCBBoxHead",
                        "in_channels": 256,
                        "fc_out_channels": 1024,
                        "roi_feat_size": 7,
                        "num_classes": num_classes,
                        "bbox_coder": {
                            "type": "DeltaXYWHBBoxCoder",
                            "target_means": [0.0, 0.0, 0.0, 0.0],
                            "target_stds": [0.05, 0.05, 0.1, 0.1],
                        },
                        "reg_class_agnostic": True,
                        "loss_cls": {
                            "type": "CrossEntropyLoss",
                            "use_sigmoid": False,
                            "loss_weight": 1.0,
                        },
                        "loss_bbox": {
                            "type": "SmoothL1Loss",
                            "beta": 1.0,
                            "loss_weight": 1.0,
                        },
                    },
                    {
                        "type": "Shared2FCBBoxHead",
                        "in_channels": 256,
                        "fc_out_channels": 1024,
                        "roi_feat_size": 7,
                        "num_classes": num_classes,
                        "bbox_coder": {
                            "type": "DeltaXYWHBBoxCoder",
                            "target_means": [0.0, 0.0, 0.0, 0.0],
                            "target_stds": [0.033, 0.033, 0.067, 0.067],
                        },
                        "reg_class_agnostic": True,
                        "loss_cls": {
                            "type": "CrossEntropyLoss",
                            "use_sigmoid": False,
                            "loss_weight": 1.0,
                        },
                        "loss_bbox": {
                            "type": "SmoothL1Loss",
                            "beta": 1.0,
                            "loss_weight": 1.0,
                        },
                    },
                ],
                "mask_roi_extractor": None,
                "mask_head": None,
                "semantic_roi_extractor": None,
                "semantic_head": None,
            },
            "train_cfg": {
                "rpn": {
                    "assigner": {
                        "type": "MaxIoUAssigner",
                        "pos_iou_thr": 0.7,
                        "neg_iou_thr": 0.3,
                        "min_pos_iou": 0.3,
                        "ignore_iof_thr": -1,
                    },
                    "sampler": {
                        "type": "RandomSampler",
                        "num": 256,
                        "pos_fraction": 0.5,
                        "neg_pos_ub": -1,
                        "add_gt_as_proposals": False,
                    },
                    "allowed_border": 0,
                    "pos_weight": -1,
                    "debug": False,
                },
                "rpn_proposal": {
                    "nms_pre": 2000,
                    "max_per_img": 2000,
                    "nms": {"type": "nms", "iou_threshold": nms_threshold},
                    "min_bbox_size": 0,
                },
                "rcnn": [
                    {
                        "assigner": {
                            "type": "MaxIoUAssigner",
                            "pos_iou_thr": 0.5,
                            "neg_iou_thr": 0.5,
                            "min_pos_iou": 0.5,
                            "ignore_iof_thr": -1,
                        },
                        "sampler": {
                            "type": "RandomSampler",
                            "num": 512,
                            "pos_fraction": 0.25,
                            "neg_pos_ub": -1,
                            "add_gt_as_proposals": True,
                        },
                        "mask_size": 28,
                        "pos_weight": -1,
                        "debug": False,
                    },
                    {
                        "assigner": {
                            "type": "MaxIoUAssigner",
                            "pos_iou_thr": 0.6,
                            "neg_iou_thr": 0.6,
                            "min_pos_iou": 0.6,
                            "ignore_iof_thr": -1,
                        },
                        "sampler": {
                            "type": "RandomSampler",
                            "num": 512,
                            "pos_fraction": 0.25,
                            "neg_pos_ub": -1,
                            "add_gt_as_proposals": True,
                        },
                        "mask_size": 28,
                        "pos_weight": -1,
                        "debug": False,
                    },
                    {
                        "assigner": {
                            "type": "MaxIoUAssigner",
                            "pos_iou_thr": 0.7,
                            "neg_iou_thr": 0.7,
                            "min_pos_iou": 0.7,
                            "ignore_iof_thr": -1,
                        },
                        "sampler": {
                            "type": "RandomSampler",
                            "num": 512,
                            "pos_fraction": 0.25,
                            "neg_pos_ub": -1,
                            "add_gt_as_proposals": True,
                        },
                        "mask_size": 28,
                        "pos_weight": -1,
                        "debug": False,
                    },
                ],
            },
            "test_cfg": {
                "rpn": {
                    "nms_pre": 1000,
                    "max_per_img": 1000,
                    "nms": {"type": "nms", "iou_threshold": nms_threshold},
                    "min_bbox_size": 0,
                },
                "rcnn": {
                    "score_thr": 0.001,
                    "nms": {"type": "nms", "iou_threshold": nms_threshold},
                    "max_per_img": 100,
                    "mask_thr_binary": 0.5,
                },
            },
        },
        "max_epochs": epochs,
        "lr_config": {"step": [1]},
        "evaluation": None,
    }
