# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for the MMDetectionConfigCreator."""

from typing import Any

from gts_backend.services.configuration_creator.mmdetection.templates import (
    htc_templates,
    rotated_rtmdet_templates,
    rtmdet_templates,
    yolact_templates,
    yolov3_templates,
)
from gts_backend.services.configuration_creator.structs import (
    MMDetectionModels,
    MMDetectionSegmentationModels,
    MMRotateModels,
)

MMDETECTION_CONFIG_TEMPLATES: dict[str, Any] = {
    MMDetectionModels.YOLOV3: yolov3_templates.create_mm_yolov3_config,
    MMDetectionModels.HTC: htc_templates.create_mm_htc_config,
    MMDetectionModels.RTMDET_L: rtmdet_templates.create_mm_rtmdet_l_config,
    MMDetectionModels.RTMDET_M: rtmdet_templates.create_mm_rtmdet_m_config,
    MMDetectionModels.RTMDET_X: rtmdet_templates.create_mm_rtmdet_x_config,
    MMDetectionModels.RTMDET_S: rtmdet_templates.create_mm_rtmdet_s_config,
    MMDetectionModels.RTMDET_TINY: rtmdet_templates.create_mm_rtmdet_tiny_config,
    # add additional model templates here
}

MMROTATE_CONFIG_TEMPLATES: dict[str, Any] = {
    MMRotateModels.ROTATED_RTMDET_TINY: rotated_rtmdet_templates.create_rotated_rtmdet_tiny_config,
    MMRotateModels.ROTATED_RTMDET_L: rotated_rtmdet_templates.create_rotated_rtmdet_l_config,
    # add additional model templates here
}

MMDETECTION_SEGMENTATION_CONFIG_TEMPLATES: dict[str, Any] = {
    MMDetectionSegmentationModels.YOLACT_R50: yolact_templates.create_mm_yolact_resnet_50_config,
    MMDetectionSegmentationModels.YOLACT_R101: yolact_templates.create_mm_yolact_resnet_101_config
    # add additional model templates here
}
