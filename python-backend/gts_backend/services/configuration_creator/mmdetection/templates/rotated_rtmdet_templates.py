# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for specific models of the mlcvzoo-mmdetection package."""
from typing import Any

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig


def create_rotated_rtmdet_l_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmrotate RotatedRTMDet (L).

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmrotate RTMDet config params as a dictionary.
    """
    train_pipeline = [
        {"type": "mmdet.LoadImageFromFile", "file_client_args": {"backend": "disk"}},
        {"type": "mmdet.LoadAnnotations", "with_bbox": True, "box_type": "qbox"},
        {"type": "mmrotate.ConvertBoxType", "box_type_mapping": {"gt_bboxes": "rbox"}},
        {"type": "mmdet.Resize", "scale": (1024, 1024), "keep_ratio": True},
        {
            "type": "mmdet.RandomFlip",
            "prob": 0.75,
            "direction": ["horizontal", "vertical", "diagonal"],
        },
        {
            "type": "mmrotate.RandomRotate",
            "prob": 0.5,
            "angle_range": 180,
            "rect_obj_labels": [9, 11],
        },
        {
            "type": "mmdet.Pad",
            "size": (1024, 1024),
            "pad_val": {"img": (114, 114, 114)},
        },
        {"type": "mmdet.PackDetInputs"},
    ]

    val_pipeline = [
        {"type": "mmdet.LoadImageFromFile", "file_client_args": {"backend": "disk"}},
        {"type": "mmdet.Resize", "scale": (1024, 1024), "keep_ratio": True},
        {"type": "mmdet.LoadAnnotations", "with_bbox": True, "box_type": "qbox"},
        {"type": "mmrotate.ConvertBoxType", "box_type_mapping": {"gt_bboxes": "rbox"}},
        {"type": "mmdet.Pad", "size": (1024, 1024), "pad_val": {"img": (114, 114, 114)}},
        {
            "type": "mmdet.PackDetInputs",
            "meta_keys": ("img_id", "img_path", "ori_shape", "img_shape", "scale_factor"),
        },
    ]

    return {
        "default_scope": "mmrotate",
        "default_hooks": {
            "timer": {"type": "IterTimerHook"},
            "logger": {"type": "LoggerHook", "interval": 50},
            "param_scheduler": {"type": "ParamSchedulerHook"},
            "checkpoint": {
                "type": "CheckpointHook",
                "interval": 2,
                "filename_tmpl": str(unique_name) + "_epoch_{:04d}.pth",
            },
            "sampler_seed": {"type": "DistSamplerSeedHook"},
            "visualization": {"type": "mmdet.DetVisualizationHook"},
        },
        "env_cfg": {
            "cudnn_benchmark": False,
            "mp_cfg": {"mp_start_method": "fork", "opencv_num_threads": 0},
            "dist_cfg": {"backend": "nccl"},
        },
        "vis_backends": [{"type": "LocalVisBackend"}],
        "visualizer": {
            "type": "RotLocalVisualizer",
            "vis_backends": [{"type": "LocalVisBackend"}],
            "name": "visualizer",
        },
        "log_processor": {"type": "LogProcessor", "window_size": 50, "by_epoch": True},
        "log_level": "INFO",
        "load_from": initial_checkpoint,
        "resume": False,
        "custom_hooks": [
            {"type": "mmdet.NumClassCheckHook"},
            {
                "type": "EMAHook",
                "ema_type": "mmdet.ExpMomentumEMA",
                "momentum": 0.0002,
                "update_buffers": True,
                "priority": 49,
            },
        ],
        "train_cfg": {"type": "EpochBasedTrainLoop", "max_epochs": epochs, "val_interval": 2},
        "test_cfg": {"type": "TestLoop"},
        "param_scheduler": [
            {
                "type": "LinearLR",
                "start_factor": 1e-05,
                "by_epoch": False,
                "begin": 0,
                "end": 1000,
            },
            {
                "type": "CosineAnnealingLR",
                "eta_min": 0.0002,
                "begin": epochs // 2,
                "end": epochs,
                "T_max": epochs // 2,
                "by_epoch": True,
                "convert_to_iter_based": True,
            },
        ],
        "optim_wrapper": {
            "type": "OptimWrapper",
            "optimizer": {"type": "AdamW", "lr": 0.00025, "weight_decay": 0.05},
            "paramwise_cfg": {
                "norm_decay_mult": 0,
                "bias_decay_mult": 0,
                "bypass_duplicate": True,
            },
        },
        "train_pipeline": train_pipeline,
        # Test pipeline is just for inference
        "test_pipeline": [
            {"type": "mmdet.LoadImageFromFile", "file_client_args": {"backend": "disk"}},
            {"type": "mmdet.Resize", "scale": (1024, 1024), "keep_ratio": True},
            {"type": "mmdet.Pad", "size": (1024, 1024), "pad_val": {"img": (114, 114, 114)}},
            {
                "type": "mmdet.PackDetInputs",
                "meta_keys": ("img_id", "img_path", "ori_shape", "img_shape", "scale_factor"),
            },
        ],
        "train_dataloader": {
            "batch_size": batch_size,
            "num_workers": 4,
            "persistent_workers": True,
            "sampler": {"type": "DefaultSampler", "shuffle": True},
            "batch_sampler": None,
            "pin_memory": False,
            "dataset": {
                "type": "MLCVZooMMDetDataset",
                "annotation_handler_config": annotation_handler_config.to_dict(),
                "filter_cfg": {"filter_empty_gt": True},
                "pipeline": train_pipeline,
            },
        },
        "test_dataloader": {
            "batch_size": 1,
            "num_workers": 2,
            "persistent_workers": True,
            "drop_last": False,
            "sampler": {"type": "DefaultSampler", "shuffle": False},
            "dataset": {
                "type": "MLCVZooMMDetDataset",
                "annotation_handler_config": annotation_handler_config.to_dict(),
                "test_mode": True,
                # Pipeline for the test-dataloader needs also annotations, which is then
                # the same as for validation during the training.
                "pipeline": val_pipeline,
            },
        },
        "val_pipeline": None,
        "val_cfg": None,
        "val_dataloader": None,
        "val_evaluator": None,
        "test_evaluator": {"type": "DOTAMetric", "metric": "mAP"},
        "model": {
            "type": "mmdet.RTMDet",
            "data_preprocessor": {
                "type": "mmdet.DetDataPreprocessor",
                "mean": [103.53, 116.28, 123.675],
                "std": [57.375, 57.12, 58.395],
                "bgr_to_rgb": False,
                "boxtype2tensor": False,
                "batch_augments": None,
            },
            "backbone": {
                "type": "mmdet.CSPNeXt",
                "arch": "P5",
                "expand_ratio": 0.5,
                "deepen_factor": 1,
                "widen_factor": 1,
                "channel_attention": True,
                "norm_cfg": {"type": "SyncBN"},
                "act_cfg": {"type": "SiLU"},
                "init_cfg": None,
            },
            "neck": {
                "type": "mmdet.CSPNeXtPAFPN",
                "in_channels": [256, 512, 1024],
                "out_channels": 256,
                "num_csp_blocks": 3,
                "expand_ratio": 0.5,
                "norm_cfg": {"type": "SyncBN"},
                "act_cfg": {"type": "SiLU"},
            },
            "bbox_head": {
                "type": "RotatedRTMDetSepBNHead",
                "num_classes": num_classes,
                "in_channels": 256,
                "stacked_convs": 2,
                "feat_channels": 256,
                "angle_version": "le90",
                "anchor_generator": {
                    "type": "mmdet.MlvlPointGenerator",
                    "offset": 0,
                    "strides": [8, 16, 32],
                },
                "bbox_coder": {"type": "DistanceAnglePointCoder", "angle_version": "le90"},
                "loss_cls": {
                    "type": "mmdet.QualityFocalLoss",
                    "use_sigmoid": True,
                    "beta": 2.0,
                    "loss_weight": 1.0,
                },
                "loss_bbox": {"type": "RotatedIoULoss", "mode": "linear", "loss_weight": 2.0},
                "with_objectness": False,
                "exp_on_reg": True,
                "share_conv": True,
                "pred_kernel_size": 1,
                "use_hbbox_loss": False,
                "scale_angle": False,
                "loss_angle": None,
                "norm_cfg": {"type": "SyncBN"},
                "act_cfg": {"type": "SiLU"},
                "train_cfg": {
                    "assigner": {
                        "type": "mmdet.DynamicSoftLabelAssigner",
                        "iou_calculator": {"type": "RBboxOverlaps2D"},
                        "topk": 13,
                    },
                    "allowed_border": -1,
                    "pos_weight": -1,
                    "debug": False,
                },
                "test_cfg": {
                    "nms_pre": 2000,
                    "min_bbox_size": 0,
                    "score_thr": 0.05,
                    "nms": {"type": "nms_rotated", "iou_threshold": nms_threshold},
                    "max_per_img": 2000,
                },
            },
            "train_cfg": {
                "assigner": {
                    "type": "mmdet.DynamicSoftLabelAssigner",
                    "iou_calculator": {"type": "RBboxOverlaps2D"},
                    "topk": 13,
                },
                "allowed_border": -1,
                "pos_weight": -1,
                "debug": False,
            },
            "test_cfg": {
                "nms_pre": 2000,
                "min_bbox_size": 0,
                "score_thr": 0.05,
                "nms": {"type": "nms_rotated", "iou_threshold": 0.01},
                "max_per_img": 2000,
            },
        },
    }


def create_rotated_rtmdet_tiny_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmrotate RTMDet_X.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmrotate RTMDet config params as a dictionary.
    """
    cfg = create_rotated_rtmdet_l_config(
        epochs=epochs,
        unique_name=unique_name,
        num_classes=num_classes,
        nms_threshold=nms_threshold,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
        annotation_handler_config=annotation_handler_config,
    )
    cfg["model"]["backbone"]["deepen_factor"] = 0.167
    cfg["model"]["backbone"]["widen_factor"] = 0.375
    cfg["model"]["neck"]["in_channels"] = [96, 192, 384]
    cfg["model"]["neck"]["out_channels"] = 96
    cfg["model"]["neck"]["num_csp_blocks"] = 1
    cfg["model"]["bbox_head"]["in_channels"] = 96
    cfg["model"]["bbox_head"]["feat_channels"] = 96
    cfg["model"]["bbox_head"]["exp_on_reg"] = False
    cfg["model"]["bbox_head"]["loss_bbox"] = dict(
        type="RotatedIoULoss", mode="linear", loss_weight=2.0
    )
    return cfg
