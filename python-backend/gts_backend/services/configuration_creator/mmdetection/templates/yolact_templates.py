# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for specific models of the mlcvzoo-mmdetection package."""
from typing import Any

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig


def create_mm_yolact_resnet_101_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection yolact ResNet-101.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    return {
        "default_scope": "mmdet",
        "default_hooks": {
            "timer": {"type": "IterTimerHook"},
            "logger": {"type": "LoggerHook", "interval": 50},
            "param_scheduler": {"type": "ParamSchedulerHook"},
            "checkpoint": {
                "type": "CheckpointHook",
                "interval": 1,
                "filename_tmpl": str(unique_name) + "_{:04d}.pth",
            },
            "sampler_seed": {"type": "DistSamplerSeedHook"},
            "visualization": {"type": "DetVisualizationHook"},
        },
        "env_cfg": {
            "cudnn_benchmark": False,
            "mp_cfg": {"mp_start_method": "fork", "opencv_num_threads": 0},
            "dist_cfg": {"backend": "nccl"},
        },
        "vis_backends": [{"type": "LocalVisBackend"}],
        "visualizer": {
            "type": "DetLocalVisualizer",
            "vis_backends": [{"type": "LocalVisBackend"}],
            "name": "visualizer",
        },
        "log_processor": {"type": "LogProcessor", "window_size": 50, "by_epoch": True},
        "log_level": "INFO",
        "load_from": initial_checkpoint,
        "resume": False,
        "param_scheduler": [
            {"type": "LinearLR", "start_factor": 0.1, "by_epoch": False, "begin": 0, "end": 500},
            {
                "type": "MultiStepLR",
                "begin": 0,
                "end": epochs,
                "by_epoch": True,
                # Originally the epochs are 55, therefore scale the parameter accordingly
                "milestones": [
                    20 / 55 * epochs,
                    42 / 55 * epochs,
                    49 / 55 * epochs,
                    52 / 55 * epochs,
                ],
                "gamma": 0.1,
            },
        ],
        "backend_args": None,
        "train_cfg": {"type": "EpochBasedTrainLoop", "max_epochs": epochs, "val_interval": 1},
        "train_pipeline": [
            {"type": "LoadImageFromFile"},
            {"type": "LoadAnnotations", "with_bbox": True, "with_mask": True},
            {"type": "FilterAnnotations", "min_gt_bbox_wh": [4.0, 4.0]},
            {
                "type": "Expand",
                "mean": [123.68, 116.78, 103.94],
                "to_rgb": True,
                "ratio_range": [1, 4],
            },
            {
                "type": "MinIoURandomCrop",
                "min_ious": [0.1, 0.3, 0.5, 0.7, 0.9],
                "min_crop_size": 0.3,
            },
            {"type": "Resize", "img_scale": [550, 550], "keep_ratio": False},
            {"type": "RandomFlip", "flip_ratio": 0.5},
            {
                "type": "PhotoMetricDistortion",
                "brightness_delta": 32,
                "contrast_range": [0.5, 1.5],
                "saturation_range": [0.5, 1.5],
                "hue_delta": 18,
            },
            {
                "type": "Normalize",
                "mean": [123.68, 116.78, 103.94],
                "std": [58.4, 57.12, 57.38],
                "to_rgb": True,
            },
            {"type": "DefaultFormatBundle"},
            {"type": "Collect", "keys": ["img", "gt_bboxes", "gt_labels", "gt_masks"]},
        ],
        "train_dataloader": {
            "batch_size": batch_size,
            "num_workers": 2,
            "persistent_workers": True,
            "sampler": {"type": "DefaultSampler", "shuffle": True},
            "batch_sampler": None,
            "dataset": {
                "type": "MLCVZooMMDetDataset",
                "filter_cfg": {"filter_empty_gt": True, "min_size": 32},
                "pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "LoadAnnotations", "with_bbox": True, "with_mask": True},
                    {"type": "FilterAnnotations", "min_gt_bbox_wh": [4.0, 4.0]},
                    {
                        "type": "Expand",
                        "mean": [123.68, 116.78, 103.94],
                        "to_rgb": True,
                        "ratio_range": [1, 4],
                    },
                    {
                        "type": "MinIoURandomCrop",
                        "min_ious": [0.1, 0.3, 0.5, 0.7, 0.9],
                        "min_crop_size": 0.3,
                    },
                    {"type": "Resize", "scale": [550, 550], "keep_ratio": False},
                    {"type": "RandomFlip", "prob": 0.5},
                    {
                        "type": "PhotoMetricDistortion",
                        "brightness_delta": 32,
                        "contrast_range": [0.5, 1.5],
                        "saturation_range": [0.5, 1.5],
                        "hue_delta": 18,
                    },
                    {"type": "PackDetInputs"},
                ],
                "backend_args": None,
                "annotation_handler_config": annotation_handler_config.to_dict(),
            },
        },
        "test_cfg": {"type": "TestLoop"},
        "test_pipeline": [
            {"type": "LoadImageFromFile"},
            {
                "type": "MultiScaleFlipAug",
                "img_scale": [550, 550],
                "flip": False,
                "transforms": [
                    {"type": "Resize", "keep_ratio": False},
                    {
                        "type": "Normalize",
                        "mean": [123.68, 116.78, 103.94],
                        "std": [58.4, 57.12, 57.38],
                        "to_rgb": True,
                    },
                    {"type": "ImageToTensor", "keys": ["img"]},
                    {"type": "Collect", "keys": ["img"]},
                ],
            },
        ],
        "test_dataloader": {
            "batch_size": 1,
            "num_workers": 2,
            "persistent_workers": True,
            "drop_last": False,
            "sampler": {"type": "DefaultSampler", "shuffle": False},
            "dataset": {
                "type": "MLCVZooMMDetDataset",
                "test_mode": True,
                "pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "Resize", "scale": [550, 550], "keep_ratio": False},
                    {"type": "LoadAnnotations", "with_bbox": True, "with_mask": True},
                    {
                        "type": "PackDetInputs",
                        "meta_keys": [
                            "img_id",
                            "img_path",
                            "ori_shape",
                            "img_shape",
                            "scale_factor",
                        ],
                    },
                ],
                "backend_args": None,
                "annotation_handler_config": annotation_handler_config.to_dict(),
            },
        },
        "test_evaluator": {
            "type": "CocoMetric",
            "metric": ["bbox", "segm"],
            "format_only": False,
            "backend_args": None,
        },
        "val_cfg": None,  # {"type": "ValLoop"},
        "val_dataloader": None,
        "val_evaluator": None,
        "model": {
            "type": "YOLACT",
            "data_preprocessor": {
                "type": "DetDataPreprocessor",
                "mean": [123.68, 116.78, 103.94],
                "std": [58.4, 57.12, 57.38],
                "bgr_to_rgb": True,
                "pad_mask": True,
            },
            "backbone": {
                "type": "ResNet",
                "depth": 101,
                "num_stages": 4,
                "out_indices": [0, 1, 2, 3],
                "frozen_stages": -1,
                "norm_cfg": {"type": "BN", "requires_grad": True},
                "norm_eval": False,
                "zero_init_residual": False,
                "style": "pytorch",
                "init_cfg": None,
            },
            "neck": {
                "type": "FPN",
                "in_channels": [256, 512, 1024, 2048],
                "out_channels": 256,
                "start_level": 1,
                "add_extra_convs": "on_input",
                "num_outs": 5,
                "upsample_cfg": {"mode": "bilinear"},
            },
            "bbox_head": {
                "type": "YOLACTHead",
                "num_classes": num_classes,
                "in_channels": 256,
                "feat_channels": 256,
                "anchor_generator": {
                    "type": "AnchorGenerator",
                    "octave_base_scale": 3,
                    "scales_per_octave": 1,
                    "base_sizes": [8, 16, 32, 64, 128],
                    "ratios": [0.5, 1.0, 2.0],
                    "strides": [
                        7.971014492753623,
                        15.714285714285714,
                        30.555555555555557,
                        61.111111111111114,
                        110.0,
                    ],
                    "centers": [
                        [3.9855072463768115, 3.9855072463768115],
                        [7.857142857142857, 7.857142857142857],
                        [15.277777777777779, 15.277777777777779],
                        [30.555555555555557, 30.555555555555557],
                        [55.0, 55.0],
                    ],
                },
                "bbox_coder": {
                    "type": "DeltaXYWHBBoxCoder",
                    "target_means": [0.0, 0.0, 0.0, 0.0],
                    "target_stds": [0.1, 0.1, 0.2, 0.2],
                },
                "loss_cls": {
                    "type": "CrossEntropyLoss",
                    "use_sigmoid": False,
                    "reduction": "none",
                    "loss_weight": 1.0,
                },
                "loss_bbox": {"type": "SmoothL1Loss", "beta": 1.0, "loss_weight": 1.5},
                "num_head_convs": 1,
                "num_protos": 32,
                "use_ohem": True,
            },
            "mask_head": {
                "type": "YOLACTProtonet",
                "in_channels": 256,
                "num_protos": 32,
                "num_classes": num_classes,
                "max_masks_to_train": 100,
                "loss_mask_weight": 6.125,
                "with_seg_branch": True,
                "loss_segm": {"type": "CrossEntropyLoss", "use_sigmoid": True, "loss_weight": 1.0},
            },
            "train_cfg": {
                "assigner": {
                    "type": "MaxIoUAssigner",
                    "pos_iou_thr": 0.5,
                    "neg_iou_thr": 0.4,
                    "min_pos_iou": 0.0,
                    "ignore_iof_thr": -1,
                    "gt_max_assign_all": False,
                },
                "sampler": {"type": "PseudoSampler"},
                "allowed_border": -1,
                "pos_weight": -1,
                "neg_pos_ratio": 3,
                "debug": False,
            },
            "test_cfg": {
                "nms_pre": 1000,
                "min_bbox_size": 0,
                "score_thr": 0.05,
                "mask_thr": 0.5,
                "iou_thr": 0.5,
                "top_k": 200,
                "max_per_img": 100,
                "mask_thr_binary": 0.5,
                "nms": {"iou_threshold": nms_threshold},
            },
        },
        "optim_wrapper": {
            "type": "OptimWrapper",
            "optimizer": {"type": "SGD", "lr": 0.001, "momentum": 0.9, "weight_decay": 0.0005},
        },
        "custom_hooks": [{"type": "CheckInvalidLossHook", "interval": 50, "priority": "VERY_LOW"}],
        "auto_scale_lr": None,
        "optimizer": {"type": "SGD", "lr": 0.001, "momentum": 0.9, "weight_decay": 0.0005},
        "optimizer_config": {},
        "lr_config": {
            "policy": "step",
            "warmup": "linear",
            "warmup_iters": 500,
            "warmup_ratio": 0.1,
            "step": [1],
        },
        "runner": {"type": "EpochBasedRunner", "max_epochs": epochs},
        "cudnn_benchmark": True,
        "evaluation": None,
        "launcher": "none",
    }


def create_mm_yolact_resnet_50_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection yolact ResNet-101.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    yolact_resnet_50_config = create_mm_yolact_resnet_101_config(
        epochs=epochs,
        unique_name=unique_name,
        num_classes=num_classes,
        nms_threshold=nms_threshold,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
        annotation_handler_config=annotation_handler_config,
    )

    yolact_resnet_50_config["model"]["backbone"]["depth"] = 50

    return yolact_resnet_50_config
