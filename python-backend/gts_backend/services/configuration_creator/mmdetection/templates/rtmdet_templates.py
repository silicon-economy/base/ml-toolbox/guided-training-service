# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for specific models of the mlcvzoo-mmdetection package."""
from typing import Any

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig


def create_mm_rtmdet_base_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection RTMDet (L).

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    return {
        "default_scope": "mmdet",
        "default_hooks": {
            "timer": {"type": "IterTimerHook"},
            "logger": {"type": "LoggerHook", "interval": 50},
            "param_scheduler": {"type": "ParamSchedulerHook"},
            "checkpoint": {
                "type": "CheckpointHook",
                "interval": 2,
                "filename_tmpl": str(unique_name) + "_epoch_{:04d}.pth",
            },
            "sampler_seed": {"type": "DistSamplerSeedHook"},
            "visualization": {"type": "DetVisualizationHook"},
        },
        "env_cfg": {
            "cudnn_benchmark": False,
            "mp_cfg": {"mp_start_method": "fork", "opencv_num_threads": 0},
            "dist_cfg": {"backend": "nccl"},
        },
        "vis_backends": [{"type": "LocalVisBackend"}],
        "visualizer": {
            "type": "DetLocalVisualizer",
            "vis_backends": [{"type": "LocalVisBackend"}],
            "name": "visualizer",
        },
        "log_processor": {"type": "LogProcessor", "window_size": 50, "by_epoch": True},
        "log_level": "INFO",
        "load_from": initial_checkpoint,
        "resume": False,
        "train_cfg": {
            "type": "EpochBasedTrainLoop",
            "max_epochs": epochs,
            "val_interval": 10,
            "dynamic_intervals": [(280, 1)],
        },
        "val_cfg": None,  # {"type": "ValLoop"},
        "test_cfg": {"type": "TestLoop"},
        "param_scheduler": [
            {
                "type": "LinearLR",
                "start_factor": 1e-05,
                "by_epoch": False,
                "begin": 0,
                "end": 1000,
            },
            {
                "type": "CosineAnnealingLR",
                "eta_min": 0.0002,
                "begin": epochs // 2,  # ToDo: discuss in review, default:=150
                "end": epochs,  # ToDo: discuss in review, default:=300
                "T_max": epochs // 2,  # ToDo: discuss in review, default:=150
                "by_epoch": True,
                "convert_to_iter_based": True,
            },
        ],
        "optim_wrapper": {
            "type": "OptimWrapper",
            "optimizer": {"type": "AdamW", "lr": 0.004, "weight_decay": 0.05},
            "paramwise_cfg": {
                "norm_decay_mult": 0,
                "bias_decay_mult": 0,
                "bypass_duplicate": True,
            },
        },
        "auto_scale_lr": {"enable": False, "base_batch_size": 16},
        "dataset_type": "CocoDataset",
        "data_root": "data/coco/",
        "backend_args": None,
        "train_pipeline": [
            {"type": "LoadImageFromFile", "backend_args": None},
            {"type": "LoadAnnotations", "with_bbox": True},
            {"type": "CachedMosaic", "img_scale": (640, 640), "pad_val": 114.0},
            {
                "type": "RandomResize",
                "scale": (1280, 1280),
                "ratio_range": (0.1, 2.0),
                "keep_ratio": True,
            },
            {"type": "RandomCrop", "crop_size": (640, 640)},
            {"type": "YOLOXHSVRandomAug"},
            {"type": "RandomFlip", "prob": 0.5},
            {"type": "Pad", "size": (640, 640), "pad_val": {"img": (114, 114, 114)}},
            {
                "type": "CachedMixUp",
                "img_scale": (640, 640),
                "ratio_range": (1.0, 1.0),
                "max_cached_images": 20,
                "pad_val": (114, 114, 114),
            },
            {"type": "PackDetInputs"},
        ],
        "test_pipeline": [
            {"type": "LoadImageFromFile", "backend_args": None},
            {"type": "Resize", "scale": (640, 640), "keep_ratio": True},
            {"type": "Pad", "size": (640, 640), "pad_val": {"img": (114, 114, 114)}},
            {"type": "LoadAnnotations", "with_bbox": True},
            {
                "type": "PackDetInputs",
                "meta_keys": (
                    "img_id",
                    "img_path",
                    "ori_shape",
                    "img_shape",
                    "scale_factor",
                ),
            },
        ],
        "train_dataloader": {
            "batch_size": batch_size,
            "num_workers": 2,
            "persistent_workers": True,
            "sampler": {"type": "DefaultSampler", "shuffle": True},
            "batch_sampler": None,
            "dataset": {
                "type": "MLCVZooMMDetDataset",
                "filter_cfg": {"filter_empty_gt": True, "min_size": 32},
                "pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "LoadAnnotations", "with_bbox": True},
                    {"type": "CachedMosaic", "img_scale": (640, 640), "pad_val": 114.0},
                    {
                        "type": "RandomResize",
                        "scale": (1280, 1280),
                        "ratio_range": (0.1, 2.0),
                        "keep_ratio": True,
                    },
                    {"type": "RandomCrop", "crop_size": (640, 640)},
                    {"type": "YOLOXHSVRandomAug"},
                    {"type": "RandomFlip", "prob": 0.5},
                    {
                        "type": "Pad",
                        "size": (640, 640),
                        "pad_val": {"img": (114, 114, 114)},
                    },
                    {
                        "type": "CachedMixUp",
                        "img_scale": (640, 640),
                        "ratio_range": (1.0, 1.0),
                        "max_cached_images": 20,
                        "pad_val": (114, 114, 114),
                    },
                    {"type": "PackDetInputs"},
                ],
                "backend_args": None,
                "annotation_handler_config": annotation_handler_config.to_dict(),  # added
            },
            "pin_memory": True,
        },
        "val_dataloader": None,
        "test_dataloader": {
            "batch_size": 1,
            "num_workers": 2,  # 10
            "persistent_workers": True,
            "drop_last": False,
            "sampler": {"type": "DefaultSampler", "shuffle": False},
            "dataset": {
                "type": "CocoDataset",
                "data_root": "data/coco/",
                "ann_file": "annotations/instances_val2017.json",
                "data_prefix": {"img": "val2017/"},
                "test_mode": True,
                "pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "Resize", "scale": (640, 640), "keep_ratio": True},
                    {
                        "type": "Pad",
                        "size": (640, 640),
                        "pad_val": {"img": (114, 114, 114)},
                    },
                    {"type": "LoadAnnotations", "with_bbox": True},
                    {
                        "type": "PackDetInputs",
                        "meta_keys": (
                            "img_id",
                            "img_path",
                            "ori_shape",
                            "img_shape",
                            "scale_factor",
                        ),
                    },
                ],
                "backend_args": None,
            },
        },
        "val_evaluator": None,
        "test_evaluator": {
            "type": "CocoMetric",
            "ann_file": "data/coco/annotations/instances_val2017.json",
            "metric": "bbox",
            "format_only": False,
            "backend_args": None,
            "proposal_nums": (100, 1, 10),
        },
        "tta_model": {
            "type": "DetTTAModel",
            "tta_cfg": {
                "nms": {"type": "nms", "iou_threshold": 0.6},
                "max_per_img": 100,
            },
        },
        "img_scales": [(640, 640), (320, 320), (960, 960)],
        "tta_pipeline": [
            {"type": "LoadImageFromFile", "backend_args": None},
            {
                "type": "TestTimeAug",
                "transforms": [
                    [
                        {"type": "Resize", "scale": (640, 640), "keep_ratio": True},
                        {"type": "Resize", "scale": (320, 320), "keep_ratio": True},
                        {"type": "Resize", "scale": (960, 960), "keep_ratio": True},
                    ],
                    [
                        {"type": "RandomFlip", "prob": 1.0},
                        {"type": "RandomFlip", "prob": 0.0},
                    ],
                    [
                        {
                            "type": "Pad",
                            "size": (960, 960),
                            "pad_val": {"img": (114, 114, 114)},
                        }
                    ],
                    [{"type": "LoadAnnotations", "with_bbox": True}],
                    [
                        {
                            "type": "PackDetInputs",
                            "meta_keys": (
                                "img_id",
                                "img_path",
                                "ori_shape",
                                "img_shape",
                                "scale_factor",
                                "flip",
                                "flip_direction",
                            ),
                        }
                    ],
                ],
            },
        ],
        "model": {
            "type": "RTMDet",
            "data_preprocessor": {
                "type": "DetDataPreprocessor",
                "mean": [103.53, 116.28, 123.675],
                "std": [57.375, 57.12, 58.395],
                "bgr_to_rgb": False,
                "batch_augments": None,
            },
            "backbone": {
                "type": "CSPNeXt",
                "arch": "P5",
                "expand_ratio": 0.5,
                "deepen_factor": 1,
                "widen_factor": 1,
                "channel_attention": True,
                "norm_cfg": {"type": "SyncBN"},
                "act_cfg": {"type": "SiLU", "inplace": True},
            },
            "neck": {
                "type": "CSPNeXtPAFPN",
                "in_channels": [256, 512, 1024],
                "out_channels": 256,
                "num_csp_blocks": 3,
                "expand_ratio": 0.5,
                "norm_cfg": {"type": "SyncBN"},
                "act_cfg": {"type": "SiLU", "inplace": True},
            },
            "bbox_head": {
                "type": "RTMDetSepBNHead",
                "num_classes": num_classes,
                "in_channels": 256,
                "stacked_convs": 2,
                "feat_channels": 256,
                "anchor_generator": {
                    "type": "MlvlPointGenerator",
                    "offset": 0,
                    "strides": [8, 16, 32],
                },
                "bbox_coder": {"type": "DistancePointBBoxCoder"},
                "loss_cls": {
                    "type": "QualityFocalLoss",
                    "use_sigmoid": True,
                    "beta": 2.0,
                    "loss_weight": 1.0,
                },
                "loss_bbox": {"type": "GIoULoss", "loss_weight": 2.0},
                "with_objectness": False,
                "exp_on_reg": True,
                "share_conv": True,
                "pred_kernel_size": 1,
                "norm_cfg": {"type": "SyncBN"},
                "act_cfg": {"type": "SiLU", "inplace": True},
            },
            "train_cfg": {
                "assigner": {"type": "DynamicSoftLabelAssigner", "topk": 13},
                "allowed_border": -1,
                "pos_weight": -1,
                "debug": False,
            },
            "test_cfg": {
                "nms_pre": 30000,
                "min_bbox_size": 0,
                "score_thr": 0.001,
                "nms": {"type": "nms", "iou_threshold": nms_threshold},
                "max_per_img": 300,
            },
        },
        "train_pipeline_stage2": [
            {"type": "LoadImageFromFile", "backend_args": None},
            {"type": "LoadAnnotations", "with_bbox": True},
            {
                "type": "RandomResize",
                "scale": (640, 640),
                "ratio_range": (0.1, 2.0),
                "keep_ratio": True,
            },
            {"type": "RandomCrop", "crop_size": (640, 640)},
            {"type": "YOLOXHSVRandomAug"},
            {"type": "RandomFlip", "prob": 0.5},
            {"type": "Pad", "size": (640, 640), "pad_val": {"img": (114, 114, 114)}},
            {"type": "PackDetInputs"},
        ],
        "max_epochs": epochs,
        "stage2_num_epochs": 20,
        "base_lr": 0.004,
        "interval": 10,
        "custom_hooks": [
            {
                "type": "EMAHook",
                "ema_type": "ExpMomentumEMA",
                "momentum": 0.0002,
                "update_buffers": True,
                "priority": 49,
            },
            {
                "type": "PipelineSwitchHook",
                "switch_epoch": 280,
                "switch_pipeline": [
                    {"type": "LoadImageFromFile", "backend_args": None},
                    {"type": "LoadAnnotations", "with_bbox": True},
                    {
                        "type": "RandomResize",
                        "scale": (640, 640),
                        "ratio_range": (0.1, 2.0),
                        "keep_ratio": True,
                    },
                    {"type": "RandomCrop", "crop_size": (640, 640)},
                    {"type": "YOLOXHSVRandomAug"},
                    {"type": "RandomFlip", "prob": 0.5},
                    {
                        "type": "Pad",
                        "size": (640, 640),
                        "pad_val": {"img": (114, 114, 114)},
                    },
                    {"type": "PackDetInputs"},
                ],
            },
        ],
    }


def create_mm_rtmdet_l_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection RTMDet_L.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    return create_mm_rtmdet_base_config(
        epochs=epochs,
        unique_name=unique_name,
        num_classes=num_classes,
        nms_threshold=nms_threshold,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
        annotation_handler_config=annotation_handler_config,
    )


def create_mm_rtmdet_m_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection RTMDet_M.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    cfg = create_mm_rtmdet_base_config(
        epochs=epochs,
        unique_name=unique_name,
        num_classes=num_classes,
        nms_threshold=nms_threshold,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
        annotation_handler_config=annotation_handler_config,
    )
    cfg["model"]["backbone"]["deepen_factor"] = 0.67
    cfg["model"]["backbone"]["widen_factor"] = 0.75
    cfg["model"]["neck"]["in_channels"] = [192, 384, 768]
    cfg["model"]["neck"]["out_channels"] = 192
    cfg["model"]["neck"]["num_csp_blocks"] = 2
    cfg["model"]["bbox_head"]["in_channels"] = 192
    cfg["model"]["bbox_head"]["feat_channels"] = 192
    return cfg


def create_mm_rtmdet_x_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection RTMDet_X.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    cfg = create_mm_rtmdet_base_config(
        epochs=epochs,
        unique_name=unique_name,
        num_classes=num_classes,
        nms_threshold=nms_threshold,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
        annotation_handler_config=annotation_handler_config,
    )
    cfg["model"]["backbone"]["deepen_factor"] = 1.33
    cfg["model"]["backbone"]["widen_factor"] = 1.25
    cfg["model"]["neck"]["in_channels"] = [320, 640, 1280]
    cfg["model"]["neck"]["out_channels"] = 320
    cfg["model"]["neck"]["num_csp_blocks"] = 4
    cfg["model"]["bbox_head"]["in_channels"] = 320
    cfg["model"]["bbox_head"]["feat_channels"] = 320
    return cfg


def create_mm_rtmdet_s_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection RTMDet_S.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    cfg = create_mm_rtmdet_base_config(
        epochs=epochs,
        unique_name=unique_name,
        num_classes=num_classes,
        nms_threshold=nms_threshold,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
        annotation_handler_config=annotation_handler_config,
    )
    cfg["train_pipeline"][3]["ratio_range"] = (0.5, 2.0)
    cfg["train_dataloader"]["dataset"]["pipeline"][3]["ratio_range"] = (0.5, 2.0)
    cfg["model"]["backbone"]["deepen_factor"] = 0.33
    cfg["model"]["backbone"]["widen_factor"] = 0.5
    cfg["model"]["backbone"]["init_cfg"] = {
        "type": "Pretrained",
        "prefix": "backbone.",
        "checkpoint": initial_checkpoint,
    }
    cfg["model"]["neck"]["in_channels"] = [128, 256, 512]
    cfg["model"]["neck"]["out_channels"] = 128
    cfg["model"]["neck"]["num_csp_blocks"] = 1
    cfg["model"]["bbox_head"]["in_channels"] = 128
    cfg["model"]["bbox_head"]["feat_channels"] = 128
    cfg["model"]["bbox_head"]["exp_on_reg"] = False
    cfg["train_pipeline_stage2"][2]["ratio_range"] = (0.5, 2.0)
    cfg["custom_hooks"][1]["switch_pipeline"][2]["ratio_range"] = (0.5, 2.0)
    cfg["checkpoint"] = initial_checkpoint
    return cfg


def create_mm_rtmdet_tiny_config(
    epochs: int,
    unique_name: str,
    num_classes: int,
    nms_threshold: float,
    batch_size: int,
    initial_checkpoint: str,
    annotation_handler_config: AnnotationHandlerConfig,
) -> dict[str, Any]:
    """Create a static configuration dictionary for mmdetection RTMDet_Tiny.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        num_classes: Number of classes to predict.
        nms_threshold: The nms threshold for class predictions.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.
        annotation_handler_config: The MLCVZoo AnnotationHandlerConfig of the model.

    Returns:
        The mmdetection RTMDet config params as a dictionary.
    """
    cfg = create_mm_rtmdet_s_config(
        epochs=epochs,
        unique_name=unique_name,
        num_classes=num_classes,
        nms_threshold=nms_threshold,
        batch_size=batch_size,
        initial_checkpoint=initial_checkpoint,
        annotation_handler_config=annotation_handler_config,
    )
    cfg["train_pipeline"][2].update(
        {
            "max_cached_images": 20,
            "random_pop": False,
        }
    )
    cfg["train_pipeline"][8]["max_cached_images"] = 10
    cfg["train_pipeline"][8].update(
        {
            "random_pop": False,
            "prob": 0.5,
        }
    )
    cfg["train_dataloader"]["dataset"]["pipeline"][2].update(
        {
            "max_cached_images": 20,
            "random_pop": False,
        }
    )
    cfg["train_dataloader"]["dataset"]["pipeline"][8]["max_cached_images"] = 10
    cfg["train_dataloader"]["dataset"]["pipeline"][8].update(
        {
            "random_pop": False,
            "prob": 0.5,
        }
    )
    cfg["model"]["backbone"]["deepen_factor"] = 0.167
    cfg["model"]["backbone"]["widen_factor"] = 0.375
    cfg["model"]["neck"]["in_channels"] = [96, 192, 384]
    cfg["model"]["neck"]["out_channels"] = 96
    cfg["model"]["bbox_head"]["in_channels"] = 96
    cfg["model"]["bbox_head"]["feat_channels"] = 96

    return cfg
