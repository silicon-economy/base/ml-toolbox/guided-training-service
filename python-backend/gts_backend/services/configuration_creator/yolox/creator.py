# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines the YOLOXConfigCreator class.

The class is used to create model configs for models that can be trained with the mlcvzoo-yolox
package.
"""

import logging
from typing import Optional

from mlcvzoo_yolox.configuration import (
    YOLOXConfig,
    YOLOXExperimentConfig,
    YOLOXInferenceConfig,
    YOLOXTrainArgparseConfig,
    YOLOXTrainConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
)
from gts_backend.services.configuration_creator.base.creator import ConfigCreator

logger = logging.getLogger(__name__)


class YOLOXConfigCreator(ConfigCreator[YOLOXConfig]):
    """Creator for a config to train a model with the mlcvzoo-yolox package.

    Attributes:
        configuration: Configuration of the YOLOXConfigCreator
    """

    def __init__(
        self,
        configuration: ConfigCreatorConfig,
    ):
        """Inits YOLOXConfigCreator with its configuration.

        Args:
            configuration: Configuration for the YOLOXConfigCreator.
        """
        ConfigCreator.__init__(self, configuration=configuration)

    def create_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        s3_eval_prefixes: Optional[list[str]] = None,
    ) -> YOLOXConfig:
        """Creates YOLOX model config with given S3 objects as training data.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of the training data.
            s3_eval_prefixes: List of S3 prefixes of the evaluation data.

        Returns:
            A YOLOX model config.
        """
        unique_name = self._create_unique_name()

        logger.debug("Create YOLOX-specific model config for model %s", unique_name)

        if (
            "nano" in self.configuration.parameters.model
            or "tiny" in self.configuration.parameters.model
        ):
            default_input_size = [416, 416]
        else:
            default_input_size = [640, 640]

        return self.__create_base_model_config(
            bucket_name=bucket_name,
            s3_prefixes=s3_prefixes,
            unique_name=unique_name,
            default_input_size=default_input_size,
        )

    def __create_base_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        unique_name: str,
        default_input_size: list[int],
    ) -> YOLOXConfig:
        """Creates YOLOX model config with given S3 objects as training data.

        Creates a config from the base parameters.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of the training data.
            unique_name: Unique name for the YOLOX model.
            default_input_size: Input size of the model.

        Returns:
            A YOLOX model config.
        """
        return YOLOXConfig(
            class_mapping=self.configuration.dataset.class_mapping,
            experiment_config=YOLOXExperimentConfig(
                exp_type=self.configuration.parameters.model.split("__")[1].upper(),
                attribute_overwrite={
                    "max_epoch": self.configuration.parameters.epochs,
                    "input_size": default_input_size,
                    "test_size": default_input_size,
                    "output_dir": self._create_training_output_dir(),
                },
            ),
            train_config=YOLOXTrainConfig(
                argparse_config=YOLOXTrainArgparseConfig(
                    batch_size=self.configuration.parameters.batch_size,
                    ckpt=f"{self.relative_initial_checkpoint_path(bucket_name=bucket_name)}/"
                    f"{self.configuration.parameters.model}.pth",
                    devices=1,
                ),
                train_annotation_handler_config=self.create_annotation_handler_config(
                    bucket_name=bucket_name, s3_prefixes=s3_prefixes
                ),
                test_annotation_handler_config=self.create_annotation_handler_config(
                    bucket_name=bucket_name, s3_prefixes=s3_prefixes
                ),
            ),
            inference_config=YOLOXInferenceConfig(
                checkpoint_path="",
                nms_threshold=self.configuration.parameters.nms_threshold,
                score_threshold=self.configuration.parameters.score_threshold,
                input_dim=default_input_size,
                device="gpu",
                gpu_fp16=False,
                fuse=False,
            ),
            unique_name=unique_name,
        )
