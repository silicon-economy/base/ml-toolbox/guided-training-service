# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines the MMOCRConfigCreator class.

The class is used to create model configs for models that can be trained with the mlcvzoo-mmocr
package.
"""

import logging
from typing import Any, Optional

from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_mmdetection.configuration import MMConfig
from mlcvzoo_mmocr.configuration import (
    MMOCRConfig,
    MMOCRInferenceConfig,
    MMOCRTrainArgparseConfig,
    MMOCRTrainConfig,
    MMOCRTrainDatasetConfig,
)

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
)
from gts_backend.services.configuration_creator.base.creator import ConfigCreator
from gts_backend.services.configuration_creator.mmocr.templates import (
    MMOCR_CONFIG_TEMPLATES,
)

logger = logging.getLogger(__name__)


class MMOCRConfigCreator(ConfigCreator[MMOCRConfig]):
    """Creator for a config to train a model with the mlcvzoo-mmocr package.

    Attributes:
        configuration: Configuration of the MMOCRConfigCreator
    """

    def __init__(
        self,
        configuration: ConfigCreatorConfig,
    ):
        """Inits MMOCRConfigCreator with its configuration.

        Args:
            configuration: Configuration for the MMOCRConfigCreator.
        """
        ConfigCreator.__init__(self, configuration=configuration)

    def create_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        s3_eval_prefixes: Optional[list[str]] = None,
    ) -> MMOCRConfig:
        """Create MMOCR model config with given S3 objects as training data.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of training data.
            s3_eval_prefixes: List of S3 prefixes of the evaluation data.

        Returns:
            An MMOCR model config.
        """
        unique_name = self._create_unique_name()

        logger.debug("Create MMOCR-specific model config for model %s", unique_name)

        if not s3_eval_prefixes:
            raise ValueError("s3_eval_prefixes is mandatory for mmocr models.")

        return self.__create_base_model_config(
            bucket_name=bucket_name,
            s3_prefixes=s3_prefixes,
            s3_eval_prefixes=s3_eval_prefixes,
            unique_name=unique_name,
        )

    def __create_base_model_config(
        self,
        bucket_name: str,
        s3_prefixes: list[str],
        s3_eval_prefixes: list[str],
        unique_name: str,
    ) -> MMOCRConfig:
        """Create MMOCR model config with given S3 objects as training data from its base parameters.

        Args:
            bucket_name: Name of the S3 bucket in which the training data is stored.
            s3_prefixes: List of S3 prefixes of training data.

        Returns:
            An MMOCR model config.
        """
        train_annotation_handler_config: AnnotationHandlerConfig = (
            self.create_annotation_handler_config(bucket_name, s3_prefixes)
        )

        eval_annotation_handler_config: AnnotationHandlerConfig = (
            self.create_annotation_handler_config(bucket_name, s3_eval_prefixes)
        )

        mm_config = MMConfig(
            config_dict=self.__get_mmocr_template(
                epochs=self.configuration.parameters.epochs,
                unique_name=unique_name,
                batch_size=self.configuration.parameters.batch_size,
                initial_checkpoint=f"{self.relative_initial_checkpoint_path(bucket_name=bucket_name)}/"
                f"{self.configuration.parameters.model}.pth",
            )
        )

        return MMOCRConfig(
            unique_name=unique_name,
            class_mapping=self.configuration.dataset.class_mapping,
            inference_config=MMOCRInferenceConfig(
                checkpoint_path="",
                score_threshold=self.configuration.parameters.score_threshold,
            ),
            train_config=MMOCRTrainConfig(
                argparse_config=MMOCRTrainArgparseConfig(
                    work_dir=self._create_training_output_dir()
                ),
                best_metric_name="val/recog/word_acc_ignore_case",
                dataset_config=MMOCRTrainDatasetConfig(
                    train_annotation_handler_config=train_annotation_handler_config,
                    val_annotation_handler_config=eval_annotation_handler_config,
                    test_annotation_handler_config=eval_annotation_handler_config,
                ),
            ),
            mm_config=mm_config,
        )

    def __get_mmocr_template(
        self,
        unique_name: str,
        epochs: int,
        batch_size: int,
        initial_checkpoint: str,
    ) -> dict[str, Any]:
        """Infer a template config dictionary for a specific mmocr model from a given model name.

        Args:
            unique_name: Unique name of the model.
            epochs: Number of maximum training epochs.
            batch_size: Number of samples in a batch.
            initial_checkpoint: The initial model checkpoint from which to resume training.

        Returns:
            template_dict: The template config dictionary.

        Raises:
            ValueError: No valid mmocr model config template could be inferred from the given name.

        """
        model = self.configuration.parameters.model

        valid_models: dict[Any, Any] = {}
        valid_models.update(MMOCR_CONFIG_TEMPLATES.items())

        template = MMOCR_CONFIG_TEMPLATES.get(model)

        if template is None:
            raise ValueError("Could not find a template for model=%s", model)

        return template(  # type: ignore[no-any-return]
            unique_name=unique_name,
            epochs=epochs,
            batch_size=batch_size,
            initial_checkpoint=initial_checkpoint,
        )
