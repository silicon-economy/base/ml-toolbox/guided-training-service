# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for MMOCR Abinet model."""

from typing import Any, Dict


def create_mmocr_abinet_config(
    unique_name: str,
    epochs: int,
    batch_size: int,
    initial_checkpoint: str,
) -> Dict[str, Any]:
    """Create a static configuration dictionary for mmocr ABINet.

    Args:
        epochs: Number of maximum training epochs.
        unique_name: Unique name of the model.
        batch_size: Number of samples in a batch.
        initial_checkpoint: The initial model checkpoint from which to resume training.

    Returns:
        The mmocr ABINet config params as a dictionary.
    """
    test_dataloader = {
        "batch_size": 1,
        "num_workers": 4,
        "persistent_workers": True,
        "drop_last": False,
        "sampler": {"type": "DefaultSampler", "shuffle": False},
        "dataset": {
            "type": "RecogTextDataset",
            "data_root": "",  # Will be overwritten in mlcvzoo-mmocr with the information
            # from the yaml mlcvzoo-mmocr configuration.
            "ann_file": "",  # Will be overwritten in mlcvzoo-mmocr with the information
            # from the yaml mlcvzoo-mmocr configuration.
            "pipeline": [
                {"type": "LoadImageFromFile"},
                {"type": "Resize", "scale": (128, 32)},
                {"type": "LoadOCRAnnotations", "with_text": True},
                {
                    "type": "PackTextRecogInputs",
                    "meta_keys": ("img_path", "ori_shape", "img_shape", "valid_ratio"),
                },
            ],
            "parser_cfg": {"type": "LineStrParser", "separator": ";"},
        },
    }

    return {
        "default_scope": "mmocr",
        "env_cfg": {
            "cudnn_benchmark": False,
            "mp_cfg": {"mp_start_method": "fork", "opencv_num_threads": 0},
            "dist_cfg": {"backend": "nccl"},
        },
        "randomness": {"seed": None},
        "default_hooks": {
            "timer": {"type": "IterTimerHook"},
            "logger": {"type": "LoggerHook", "interval": 1},
            "param_scheduler": {"type": "ParamSchedulerHook"},
            "checkpoint": {
                "type": "CheckpointHook",
                "interval": 1,
                "filename_tmpl": str(unique_name) + "_epoch_{:04d}.pth",  # added
            },
            "sampler_seed": {"type": "DistSamplerSeedHook"},
            "sync_buffer": {"type": "SyncBuffersHook"},
            "visualization": {
                "type": "VisualizationHook",
                "interval": 1,
                "enable": False,
                "show": False,
                "draw_gt": False,
                "draw_pred": False,
            },
        },
        "log_level": "INFO",
        "log_processor": {"type": "LogProcessor", "window_size": 10, "by_epoch": True},
        "load_from": initial_checkpoint,
        "resume": False,
        "vis_backends": [{"type": "LocalVisBackend"}],
        "visualizer": {
            "type": "TextRecogLocalVisualizer",
            "name": "visualizer",
            "vis_backends": [{"type": "LocalVisBackend"}],
        },
        "tta_model": {"type": "EncoderDecoderRecognizerTTAModel"},
        "optim_wrapper": {"type": "OptimWrapper", "optimizer": {"type": "Adam", "lr": 0.0001}},
        "train_cfg": {"type": "EpochBasedTrainLoop", "max_epochs": epochs, "val_interval": 2},
        # ValLoop is needed in order to produce and log metrics on validation data
        "val_cfg": {"type": "ValLoop"},
        "test_cfg": {"type": "TestLoop"},
        #  Original values from mmocr repo:
        #      max_epochs = 20
        #      LinearLR: end=2 -> 0.1 of max_epochs
        #      MultiStepLR: milestones=[16, 18] -> 0.8 and 0.9 of max_epochs
        "param_scheduler": [
            {
                "type": "LinearLR",
                "end": max(int(epochs * 0.1), 1),
                "start_factor": 0.001,
                "convert_to_iter_based": True,
            },
            {
                "type": "MultiStepLR",
                "milestones": [int(epochs * 0.8), int(epochs * 0.9)],
                "end": epochs,
            },
        ],
        "dictionary": {
            "type": "Dictionary",
            "dict_file": "MMOCR_DIR/dicts/lower_english_digits.txt",
            "with_start": True,
            "with_end": True,
            "same_start_end": True,
            "with_padding": False,
            "with_unknown": False,
        },
        "model": {
            "type": "ABINet",
            "backbone": {"type": "ResNetABI"},
            "encoder": {
                "type": "ABIEncoder",
                "n_layers": 3,
                "n_head": 8,
                "d_model": 512,
                "d_inner": 2048,
                "dropout": 0.1,
                "max_len": 256,
            },
            "decoder": {
                "type": "ABIFuser",
                "vision_decoder": {
                    "type": "ABIVisionDecoder",
                    "in_channels": 512,
                    "num_channels": 64,
                    "attn_height": 8,
                    "attn_width": 32,
                    "attn_mode": "nearest",
                    "init_cfg": {"type": "Xavier", "layer": "Conv2d"},
                },
                "module_loss": {"type": "ABIModuleLoss", "letter_case": "lower"},
                "postprocessor": {"type": "AttentionPostprocessor"},
                "dictionary": {
                    "type": "Dictionary",
                    "dict_file": "MMOCR_DIR/dicts/lower_english_digits.txt",
                    "with_start": True,
                    "with_end": True,
                    "same_start_end": True,
                    "with_padding": False,
                    "with_unknown": False,
                },
                "max_seq_len": 26,
                "d_model": 512,
                "num_iters": 3,
                "language_decoder": {
                    "type": "ABILanguageDecoder",
                    "d_model": 512,
                    "n_head": 8,
                    "d_inner": 2048,
                    "n_layers": 4,
                    "dropout": 0.1,
                    "detach_tokens": True,
                    "use_self_attn": False,
                },
            },
            "data_preprocessor": {
                "type": "TextRecogDataPreprocessor",
                "mean": [123.675, 116.28, 103.53],
                "std": [58.395, 57.12, 57.375],
            },
        },
        "train_pipeline": [
            {"type": "LoadImageFromFile", "ignore_empty": True, "min_size": 2},
            {"type": "LoadOCRAnnotations", "with_text": True},
            {"type": "Resize", "scale": (128, 32)},
            {
                "type": "RandomApply",
                "prob": 0.5,
                "transforms": [
                    {
                        "type": "RandomChoice",
                        "transforms": [
                            {"type": "RandomRotate", "max_angle": 15},
                            {
                                "type": "TorchVisionWrapper",
                                "op": "RandomAffine",
                                "degrees": 15,
                                "translate": (0.3, 0.3),
                                "scale": (0.5, 2.0),
                                "shear": (-45, 45),
                            },
                            {
                                "type": "TorchVisionWrapper",
                                "op": "RandomPerspective",
                                "distortion_scale": 0.5,
                                "p": 1,
                            },
                        ],
                    }
                ],
            },
            {
                "type": "RandomApply",
                "prob": 0.25,
                "transforms": [
                    {"type": "PyramidRescale"},
                    {
                        "type": "mmdet.Albu",
                        "transforms": [
                            {"type": "GaussNoise", "var_limit": (20, 20), "p": 0.5},
                            {"type": "MotionBlur", "blur_limit": 7, "p": 0.5},
                        ],
                    },
                ],
            },
            {
                "type": "RandomApply",
                "prob": 0.25,
                "transforms": [
                    {
                        "type": "TorchVisionWrapper",
                        "op": "ColorJitter",
                        "brightness": 0.5,
                        "saturation": 0.5,
                        "contrast": 0.5,
                        "hue": 0.1,
                    }
                ],
            },
            {
                "type": "PackTextRecogInputs",
                "meta_keys": ("img_path", "ori_shape", "img_shape", "valid_ratio"),
            },
        ],
        "test_pipeline": [
            {"type": "LoadImageFromFile"},
            {"type": "Resize", "scale": (128, 32)},
            {"type": "LoadOCRAnnotations", "with_text": True},
            {
                "type": "PackTextRecogInputs",
                "meta_keys": ("img_path", "ori_shape", "img_shape", "valid_ratio"),
            },
        ],
        "tta_pipeline": [
            {"type": "LoadImageFromFile"},
            {
                "type": "TestTimeAug",
                "transforms": [
                    [
                        {
                            "type": "ConditionApply",
                            "true_transforms": [
                                {
                                    "type": "ImgAugWrapper",
                                    "args": [{"cls": "Rot90", "k": 0, "keep_size": False}],
                                }
                            ],
                            "condition": "results['img_shape'][1]<results['img_shape'][0]",
                        },
                        {
                            "type": "ConditionApply",
                            "true_transforms": [
                                {
                                    "type": "ImgAugWrapper",
                                    "args": [{"cls": "Rot90", "k": 1, "keep_size": False}],
                                }
                            ],
                            "condition": "results['img_shape'][1]<results['img_shape'][0]",
                        },
                        {
                            "type": "ConditionApply",
                            "true_transforms": [
                                {
                                    "type": "ImgAugWrapper",
                                    "args": [{"cls": "Rot90", "k": 3, "keep_size": False}],
                                }
                            ],
                            "condition": "results['img_shape'][1]<results['img_shape'][0]",
                        },
                    ],
                    [{"type": "Resize", "scale": (128, 32)}],
                    [{"type": "LoadOCRAnnotations", "with_text": True}],
                    [
                        {
                            "type": "PackTextRecogInputs",
                            "meta_keys": ("img_path", "ori_shape", "img_shape", "valid_ratio"),
                        }
                    ],
                ],
            },
        ],
        "train_dataloader": {
            "batch_size": batch_size,
            "num_workers": 10,
            "persistent_workers": True,
            "sampler": {"type": "DefaultSampler", "shuffle": True},
            "dataset": {
                "type": "RecogTextDataset",
                "data_root": "",  # Will be overwritten in mlcvzoo-mmocr with the information
                # from the yaml mlcvzoo-mmocr configuration.
                "ann_file": "",  # Will be overwritten in mlcvzoo-mmocr with the information
                # from the yaml mlcvzoo-mmocr configuration.
                "pipeline": [
                    {"type": "LoadImageFromFile", "ignore_empty": True, "min_size": 2},
                    {"type": "LoadOCRAnnotations", "with_text": True},
                    {"type": "Resize", "scale": (128, 32)},
                    {
                        "type": "RandomApply",
                        "prob": 0.5,
                        "transforms": [
                            {
                                "type": "RandomChoice",
                                "transforms": [
                                    {"type": "RandomRotate", "max_angle": 15},
                                    {
                                        "type": "TorchVisionWrapper",
                                        "op": "RandomAffine",
                                        "degrees": 15,
                                        "translate": (0.3, 0.3),
                                        "scale": (0.5, 2.0),
                                        "shear": (-45, 45),
                                    },
                                    {
                                        "type": "TorchVisionWrapper",
                                        "op": "RandomPerspective",
                                        "distortion_scale": 0.5,
                                        "p": 1,
                                    },
                                ],
                            }
                        ],
                    },
                    {
                        "type": "RandomApply",
                        "prob": 0.25,
                        "transforms": [
                            {"type": "PyramidRescale"},
                            {
                                "type": "mmdet.Albu",
                                "transforms": [
                                    {"type": "GaussNoise", "var_limit": (20, 20), "p": 0.5},
                                    {"type": "MotionBlur", "blur_limit": 7, "p": 0.5},
                                ],
                            },
                        ],
                    },
                    {
                        "type": "RandomApply",
                        "prob": 0.25,
                        "transforms": [
                            {
                                "type": "TorchVisionWrapper",
                                "op": "ColorJitter",
                                "brightness": 0.5,
                                "saturation": 0.5,
                                "contrast": 0.5,
                                "hue": 0.1,
                            }
                        ],
                    },
                    {
                        "type": "PackTextRecogInputs",
                        "meta_keys": ("img_path", "ori_shape", "img_shape", "valid_ratio"),
                    },
                ],
                "parser_cfg": {"type": "LineStrParser", "separator": ";"},
            },
        },
        "test_dataloader": test_dataloader,
        "test_evaluator": [
            {"type": "WordMetric", "mode": ["exact", "ignore_case", "ignore_case_symbol"]},
            {"type": "CharMetric"},
        ],
        "val_dataloader": test_dataloader,
        "val_evaluator": [
            {"type": "WordMetric", "mode": ["exact", "ignore_case", "ignore_case_symbol"]},
            {"type": "CharMetric"},
        ],
        "auto_scale_lr": {"base_batch_size": batch_size},
    }
