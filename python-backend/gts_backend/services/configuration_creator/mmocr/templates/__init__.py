# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines templates for the MMOCRConfigCreator."""

from typing import Any, Dict

from gts_backend.services.configuration_creator.mmocr.templates import abinet_template
from gts_backend.services.configuration_creator.structs import MMOCRModels

MMOCR_CONFIG_TEMPLATES: Dict[str, Any] = {
    MMOCRModels.ABINET: abinet_template.create_mmocr_abinet_config,
    # add additional model templates here
}
