# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3


"""EdgeDeviceService to provide functionality related to registered and to-be registered edge devices."""

from datetime import datetime

import requests
from config_builder.json_encoding import TupleEncoder
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.adapter import S3Adapter
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3Data, S3DataLocation

from gts_backend.model.edge_device import EdgeDeviceModelSQL
from gts_backend.services.dbmanager import db


class EdgeDeviceNotFoundError(Exception):
    """Exception raised when an edge device is not found in the edge device database."""

    pass


class EdgeDeviceNotRecordingError(Exception):
    """Exception raised when an edge device is not recording at the moment."""

    pass


class EdgeDeviceService:
    """Service to provide functionality related to registered and to-be registered edge devices."""

    @staticmethod
    def load_edge_devices() -> list[EdgeDeviceModelSQL]:
        """Load the edge devices from the edge device database.

        Returns:
            The loaded edge devices.
        """
        devices: list[EdgeDeviceModelSQL] = EdgeDeviceModelSQL.query.all()
        return devices

    @staticmethod
    def add_edge_device(uri: str, name: str, device_type: str) -> EdgeDeviceModelSQL:
        """Add an edge device to the edge device database.

        Args:
            uri: The uri of the edge device.
            name: The name of the edge device.
            device_type: The type of the edge device.

        Returns:
            The added edge device.
        """
        device: EdgeDeviceModelSQL = EdgeDeviceModelSQL(
            uri=uri,
            name=name,
            device_type=device_type,
            creation_date=datetime.now(),
        )
        db.session.add(device)
        db.session.commit()

        return device

    @staticmethod
    def __load_single_edge_device(device_id: int) -> EdgeDeviceModelSQL:
        """Load the edge device from the edge device database.

        Args:
            device_id: The id of the edge device to load.

        Raises:
            EdgeDeviceNotFoundError: If the edge device is not found.

        Returns:
            The loaded edge device.
        """
        device: EdgeDeviceModelSQL = EdgeDeviceModelSQL.query.get(device_id)
        if device:
            return device

        raise EdgeDeviceNotFoundError(
            f"Edge device with id '{device_id}' not found in the edge device database."
        )

    def load_edge_device(self, device_id: int) -> dict[str, str | int | bool | datetime]:
        """Load the edge device from the edge device database.

        Args:
            device_id: The id of the edge device to load.

        Returns:
            The loaded edge device.
        """
        device = self.__load_single_edge_device(device_id)
        return {
            "id": device.id,
            "uri": device.uri,
            "name": device.name,
            "device_type": device.device_type,
            "creation_date": device.creation_date,
            "stream": f"{device.uri}/stream/",
            "is_recording": device.is_recording,
        }

    def delete_edge_device(self, device_id: int) -> None:
        """Delete the edge device from the edge device database.

        Args:
            device_id: The id of the edge device to delete.

        Raises:
            Exception: If the edge device deletion was unsuccessful.
        """
        device = self.__load_single_edge_device(device_id)
        try:
            db.session.delete(device)
        except Exception:
            db.session.rollback()
            raise
        else:
            db.session.commit()

    def start_deployment(
        self, device_id: int, model_name: str, second_model_name: str | None = None
    ) -> None:
        """Start a deployment on the edge device.

        Args:
            device_id: The id of the edge device to start the deployment for.
            model_name: The name of the model to deploy.
            second_model_name: The name of the second model to deploy.

        Raises:
            requests.exceptions.HTTPError: If the deployment request was unsuccessful.
        """
        device = self.__load_single_edge_device(device_id)
        credentials = MlflowCredentials.init_from_os_environment()
        credential_dict = credentials.to_dict()
        # The MlflowCredentials for the deployment of models in the mlcvzoo-edge-service don't
        # expect a 'run_id'
        credential_dict.pop("run_id")

        if second_model_name is None:
            data = {
                "unique_name": model_name,
                "mlflow_credentials": credential_dict,
            }
        else:
            data = {
                "unique_name": model_name,
                "second_unique_name": second_model_name,
                "mlflow_credentials": credential_dict,
            }

        data_encoded = TupleEncoder().encode(data)

        response = requests.post(
            url=f"{device.uri}/deploy/",
            data=data_encoded,
            headers={"Content-Type": "application/json"},
        )
        response.raise_for_status()

    def stop_deployment(self, device_id: int) -> None:
        """Stop a deployment on the edge device.

        Args:
            device_id: The id of the edge device to stop the deployment for.

        Raises:
            requests.exceptions.HTTPError: If the deployment stop request was unsuccessful.
        """
        device = self.__load_single_edge_device(device_id)
        response = requests.post(
            url=f"{device.uri}/deploy/stop",
        )
        response.raise_for_status()

    def start_recording(self, device_id: int) -> None:
        """Start a recording on the edge device.

        Args:
            device_id: The id of the edge device to start the recording for.

        Raises:
            requests.exceptions.HTTPError: If the recording request was successful.
        """
        device = self.__load_single_edge_device(device_id)
        credentials = S3Credentials.init_from_os_environment()

        zip_location = S3DataLocation(
            uri=f"{credentials.s3_endpoint_url}/edge-devices/{device_id}",
            location_id="",
            credentials=credentials,
        )

        data = {
            "zip_location": zip_location.to_dict(),
            "recording_interval": 1,
        }

        data_encoded = TupleEncoder().encode(data)

        response = requests.put(
            url=f"{device.uri}/data_recording/start",
            data=data_encoded,
            headers={"Content-Type": "application/json"},
        )
        response.raise_for_status()

        device.is_recording = True
        db.session.commit()

    def stop_recording(self, device_id: int) -> str:
        """Stop a recording on the edge device.

        Args:
            device_id: The id of the edge device to stop the recording for.

        Returns
            The presigned URL of the recorded data.

        Raises:
            EdgeDeviceNotRecordingError: If the edge device is not recording.
        """
        device = self.__load_single_edge_device(device_id)

        if not device.is_recording:
            raise EdgeDeviceNotRecordingError("The device is not recording at the moment.")

        response = requests.put(
            url=f"{device.uri}/data_recording/stop",
        )
        response.raise_for_status()

        device.is_recording = False
        db.session.commit()

        s3_adapter = S3Adapter(credentials=S3Credentials.init_from_os_environment())

        # URI needs cleanup from nested strings ' and "
        s3_data = S3Data(uri=response.text.rstrip().replace("'", "").replace('"', ""))
        presigned_url = s3_adapter.generate_presigned_url(
            bucket=s3_data.bucket, prefix=s3_data.prefix
        )
        return presigned_url
