# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""ModelService to provide functionality related to registered and to-be registered models."""

from dataclasses import asdict
from operator import itemgetter
from typing import Any

from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation
from mlflow.exceptions import RestException

from gts_backend.model.structs import ModelDefaultValues, ModelSortingTypes
from gts_backend.services.configuration_creator.structs import StringReplacements


class ModelNotFoundError(Exception):
    """Exception raised when a model is not found in the model registry."""

    pass


class ModelService:
    """Service to provide functionality related to registered and to-be registered models."""

    def __init__(self, mlflow_adapter: MlflowAdapter) -> None:
        """Initialize the ModelService."""
        self.__mlflow_adapter = mlflow_adapter

    def load_models(
        self,
        order: str = ModelSortingTypes.NAME,
        descending: bool = False,
        page: int = ModelDefaultValues.DEFAULT_PAGE,
        per_page: int = ModelDefaultValues.MODELS_PER_PAGE,
        filter_str: str = "",
    ) -> dict[str, int | list[dict[str, Any]]]:
        """Load the models from the model registry.

        The returned models are ordered by the given criteria.

        Args:
            order: The column to order by.
            descending: Whether to order the models in descending order or not.
            page: The page of the pagination.
            per_page: The number of elements per page.
            filter_str: The filter string.

        Returns:
            The loaded models.
        """
        if not getattr(ModelSortingTypes, order.upper(), False):
            order = ModelSortingTypes.NAME

        if per_page < 1:
            per_page = ModelDefaultValues.MODELS_PER_PAGE

        if page < 1:
            page = ModelDefaultValues.DEFAULT_PAGE

        # filter models by search string
        filter_query = f"{ModelSortingTypes.NAME} ILIKE '%{filter_str}%'" if filter_str else None

        model_infos = self.__mlflow_adapter.get_model_infos_from_registry(
            filter_query=filter_query
        )

        total_items = len(model_infos)

        model_response_list = [asdict(info) for info in model_infos]

        model_response_list = self.__unify_model_information(models=model_response_list)

        # sort by values of the key with order direction
        sorted_model_response_list = sorted(
            model_response_list, key=itemgetter(order), reverse=descending
        )

        models_per_page = self.__filter_models_per_page(
            models=sorted_model_response_list, page=page, per_page=per_page
        )

        return {
            "total_items": total_items,
            "models": models_per_page,
        }

    @staticmethod
    def __unify_model_information(models: list[dict[str, Any]]) -> list[dict[str, Any]]:
        """Unify the given model information.

        Some models have alternative keys, e.g. "datasets" instead of "dataset".

        Args:
            models: The models information to parse.

        Returns:
            The unified models information.
        """
        for model in models:
            model["dataset"] = model["datasets"]
            model["accuracy"] = model["score"]
        return models

    @staticmethod
    def __filter_models_per_page(
        models: list[dict[str, Any]], page: int, per_page: int
    ) -> list[dict[str, Any]]:
        """Filter the models by the given page and per page values.

        Given the desired page and the models per page, the models are filtered accordingly.

        Args:
            models: The models to filter.
            page: The desired page.
            per_page: The number of models per page.

        Returns:
            The filtered models.
        """
        start = (page - 1) * per_page
        end = start + per_page
        return models[start:end]

    def load_model(self, model_name: str) -> dict[str, int | list[dict[str, Any]]]:
        """Load the model from the model registry.

        Args:
            model_name: The name of the model to load.

        Raises:
            RestException: If the model is not found. Raised by the MlflowAdapter.

        Returns:
            The loaded model.
        """
        try:
            model_info = self.__mlflow_adapter.get_model_infos_from_registry(
                f"{ModelSortingTypes.NAME} = '{model_name}'"
            )[0]
            model_dict = self.__unify_model_information(models=[asdict(model_info)])[0]
            return {"total_items": 1, "models": [model_dict]}

        except (RestException, IndexError):
            raise ModelNotFoundError(f"Model '{model_name}' not found in the model registry.")

    def get_fp_fn_images(self, model_name: str) -> dict[str, list[str]]:
        """Get the false positive and false negative images of a given model.

        An image is represented by a presigned url which equals a link to download the image.

        Args:
            model_name: The name of the model to get the images for.

        Returns:
            A dictionary with the presigned urls of the false positive and false negative images.
        """
        model_classes = self.__mlflow_adapter.get_model_config_dict(model_name=model_name)[
            "class_mapping"
        ]["model_classes"]

        model_artifact_location: S3DataLocation = (
            self.__mlflow_adapter.get_model_artifact_location(
                model_name=model_name, location_id=StringReplacements.BASELINE_MODEL_DIR
            )
        )

        image_prefixes = self.__parse_fp_fn_image_prefixes(
            model_prefix=model_artifact_location.prefix,
            model_classes=model_classes,
        )

        return self.__get_fp_fn_presigned_urls(
            bucket=model_artifact_location.bucket, image_prefixes=image_prefixes
        )

    @staticmethod
    def __parse_fp_fn_image_prefixes(
        model_prefix: str, model_classes: list[dict[str, str]]
    ) -> list[tuple[str, str]]:
        """Parse the false positive and false negative image prefixes from the model classes info.

        An image prefix has the structure "{MODEL_PREFIX}/fp-fn-images/{CLASS_ID}_{CLASS_NAME}".

        Args:
            model_classes: The classes of the model.

        Returns:
            Image prefixes and the corresponding class info (ID + name).
        """
        if not model_prefix.endswith("/"):
            model_prefix = model_prefix + "/"

        return [
            (
                f"{model_prefix}fp_fn_images/{model_class['class_id']}_{model_class['class_name']}",
                f"{model_class['class_id']}_{model_class['class_name']}",
            )
            for model_class in model_classes
        ]

    def __get_fp_fn_presigned_urls(
        self, bucket: str, image_prefixes: list[tuple[str, str]]
    ) -> dict[str, list[str]]:
        """Get the presigned urls for the false positive and false negative images.

        Args:
            bucket: The bucket where the images are stored.
            image_prefixes: List of prefixes and the associated class info (ID + name)
            of the false positive and false negative images.

        Returns:
            A list of presigned urls.
        """
        presigned_urls = {}
        for prefix, class_info in image_prefixes:

            if files := self.__mlflow_adapter.s3_adapter.list_files(bucket=bucket, prefix=prefix):
                presigned_urls[
                    class_info
                ] = self.__mlflow_adapter.s3_adapter.generate_presigned_urls(files=files)

        return presigned_urls
