# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the LabelingService.

The Service is used to interact with LabelStudio and makes use of its API. Additionally, it
integrates LabelStudio with a S3 storage.
"""

import json
import logging
import os
import tempfile
import uuid
import xml.etree.ElementTree as etree
from collections.abc import Generator
from datetime import timedelta
from hashlib import md5
from io import BytesIO
from pathlib import Path
from string import Template
from time import sleep
from typing import Any, Union
from urllib.parse import urlparse
from zipfile import ZipFile, is_zipfile

import cv2
import requests
from label_studio_converter.imports.coco import convert_coco_to_ls
from label_studio_sdk import Client, Project
from mypy_boto3_s3 import S3Client
from mypy_boto3_s3.type_defs import DeleteTypeDef, ObjectIdentifierTypeDef
from mypy_boto3_sts import STSClient
from mypy_boto3_sts.type_defs import CredentialsTypeDef

from gts_backend.model.structs import (
    DatasetExportTypes,
    DatasetTaskType,
    S3ProjectFolders,
)
from gts_backend.services.labeling.converters.azure_to_ls import convert_azure_to_ls
from gts_backend.services.labeling.structs import LabelingServiceConstants
from gts_backend.services.labeling.templates import (
    TEMPLATE_CLASSIFICATION_CLASS,
    TEMPLATE_CLASSIFICATION_MAIN,
    TEMPLATE_DETECTION_LABEL,
    TEMPLATE_DETECTION_MAIN,
    TEMPLATE_SEGMENTATION_MAIN,
    TEMPLATE_TEXT_RECOGNITION_MAIN,
    TEMPLATE_TRACKING_MAIN,
)
from gts_backend.utils.exceptions import WrongFormatException


class LabelingService:
    """Service wrapper around Label-Studio and a custom S3 storage.

    Attributes:
        ls_client: LabelStudio client.
        s3_client: S3 client.
        sts_client: Client for a AWS Security Token Service.
        s3_bucket_name: Name of S3 bucket.
        s3_endpoint_url: Endpoint to access S3 storage.
        s3_endpoint_url_external: External endpoint to access S3 storage.
        s3_region_name: Region of S3 storage.
        s3_use_presigned_urls: Defines if pre-signed urls should be used.
    """

    def __init__(
        self,
        ls_client: Client,
        s3_client: S3Client,
        sts_client: STSClient,
        s3_bucket_name: str,
        s3_endpoint_url: str,
        s3_endpoint_url_external: str,
        s3_region_name: str,
        s3_use_presigned_urls: str | bool,
    ) -> None:
        """Initialize a LabelingService.

        Args:
            ls_client: LabelStudio client.
            s3_client: S3 client.
            sts_client: Client for a AWS Security Token Service.
            s3_bucket_name: Name of S3 bucket.
            s3_endpoint_url: Endpoint to access S3 storage.
            s3_endpoint_url_external: External endpoint to access S3 storage.
            s3_region_name: Region of S3 storage.
            s3_use_presigned_urls: Defines if pre-signed urls should be used.
        """
        self._logger = logging.getLogger(
            f"{__name__}.{self.__class__.__name__}",
        )
        self.ls_client = ls_client
        self.s3_client = s3_client
        self.sts_client = sts_client
        self.s3_bucket_name = s3_bucket_name
        self.s3_endpoint_url = s3_endpoint_url
        self.s3_endpoint_url_external = s3_endpoint_url_external
        self.s3_region_name = s3_region_name
        self.s3_use_presigned_urls = str(s3_use_presigned_urls).lower() == "true"

    @staticmethod
    def validate_s3_import_settings(ls_project: Project, **kwargs: Any) -> bool:
        """Validate the LabelStudio cloud storage import settings.

        Validate given settings to match a given LabelStudio project. Validation is made by
        sending a request to LabelStudio.

        Args:
            ls_project: Project for which settings should be validated
            kwargs: Settings to validate.

        Returns:
            Bool indicating if validation was successful or not.
        """
        kwargs["project"] = ls_project.id
        if "presign_ttl" not in kwargs:
            kwargs["presign_ttl"] = 1

        try:
            response: requests.Response = ls_project.make_request(
                "POST", "/api/storages/s3/validate", json=kwargs
            )
        except requests.exceptions.HTTPError:
            return False
        return int(response.status_code) == 200

    @staticmethod
    def validate_s3_export_settings(ls_project: Project, **kwargs: str) -> bool:
        """Validate the Label Studio Cloud Storage export settings.

        Validate given settings to match a given LabelStudio project. Validation is made by
        sending a request to LabelStudio.

        Args:
            ls_project: Project for which settings should be validated
            kwargs: Settings to validate.

        Returns:
            Bool indicating if validation was successful or not.
        """
        kwargs["project"] = ls_project.id

        try:
            response: requests.Response = ls_project.make_request(
                "POST", "/api/storages/export/s3/validate", json=kwargs
            )
        except requests.exceptions.HTTPError:
            return False
        return int(response.status_code) == 200

    def __assert_correctness_of_s3_path(
        self, s3_path: str, expected_s3_prefix: str, s3_file_key: str
    ) -> None:
        """Assert that the given S3 path is a valid path to an image inside a S3 bucket.

        Args:
            s3_path: Potentially valid path to image inside S3 bucket.
            expected_s3_prefix: Expected prefix of the S3 bucket where the image is stored.
            s3_file_key: Actual prefix of the S3 bucket where the image is stored.
        """
        path_parts = Path(s3_path).parts

        assert s3_path[:5] == "s3://", "Expected the file_name to start with s3://"

        assert (
            path_parts[1] == self.s3_bucket_name
        ), f"Expected the file_name to be a path in the '{self.s3_bucket_name}' S3 bucket"

        assert (
            f"{path_parts[2]}/{path_parts[3]}" == f"{expected_s3_prefix}/{S3ProjectFolders.DATA}"
        ), (
            f"Expected the S3 file key to start with the "
            f"'{expected_s3_prefix}/{S3ProjectFolders.DATA}' project prefix, got: "
            f"'{s3_file_key}' instead"
        )

    def __parse_annotations_from_labelstudio(
        self, export_type: str, project: Project, project_id: int
    ) -> Union[dict[str, Any], list[dict[str, Any]]]:
        """Parse annotations from LabelStudio.

        Args:
            export_type: Expected annotation format.
            project: LabelStudio project from where the annotations should be parsed.
            project_id: ID of the LabelStudio project.

        Return:
            Dictionary containing annotations for all images of the LabelStudio project.
        """
        # download annotations from Label-Studio as zip archive
        response: requests.Response = project.make_request(
            method="GET",
            url=f"/api/projects/{project_id}/export?"
            f"exportType={export_type}&download_resources=true&download_all_tasks=false",
        )

        self._logger.info("Downloading annotations as zip archive from Label-Studio.")

        if export_type == DatasetExportTypes.COCO.upper():
            with ZipFile(BytesIO(response.content)) as zipfile, zipfile.open(
                "result.json"
            ) as resultfile:
                return json.load(resultfile)  # type: ignore[no-any-return]
        elif export_type == DatasetExportTypes.JSON.upper():
            # parse annotations
            return json.loads(response.content)  # type: ignore[no-any-return]
        elif export_type == DatasetExportTypes.JSON_MIN.upper():
            # wrap list with "images" for further handling of the json
            return {"images": json.load(BytesIO(response.content))}

        self._logger.warning(
            "Unsupported export type '%s' for parsing annotations from Label-Studio.", export_type
        )
        return {}

    def __validate_generated_zip_file(self, zip_path: Path) -> None:
        """Validate a generated zip file.

        Validate the given path to a zip file. Valid means, that the path points to an
        existing zip file and the corruption of the zip file got successfully checked by
        calculating the CRC.

        Args:
            zip_path: Path to the zip file which should be validated.

        Raises:
            AssertionError: The zip file is not a file or zip file.
            ValueError: The CRC check failed.
        """
        assert zip_path.is_file(), f"The generated zip file {zip_path} is not a file."
        assert is_zipfile(zip_path), f"The generated zip file {zip_path} is not a zip file."
        # pragma: no cover
        with ZipFile(zip_path) as tmp_outzip:
            validation_error = tmp_outzip.testzip()
            if validation_error is not None:  # pragma: no cover
                # pylint: disable=logging-not-lazy
                self._logger.error(
                    "The created zip file '%s' with annotations "
                    "and images is invalid. The file '%s' is invalid.",
                    str(zip_path),
                    validation_error,
                )
                raise ValueError(
                    f"The created zip file {zip_path} with annotations"
                    f"and images is invalid. The file '{validation_error}' is invalid."
                )

    @staticmethod
    def __create_template_specific_configuration(
        labels: list[str],
        label_template: Template,
        main_template: Template,
    ) -> str:
        """Create a template specific LabelStudio interface configuration.

        The interface configuration depends on the provided templates the LabelStudio project is
        created for e.g. classification or object detection.

        Args:
            labels: List with labels present in the project.
            label_template: Template used for the configuration of the labels.
            main_template: Template used for the configuration of the task.

        Returns:
            Template specific configuration as string.
        """
        labelitemsconfig = ""
        for label in labels:
            labelitemsconfig = labelitemsconfig + label_template.substitute(label=label)
        return main_template.substitute(labels=labelitemsconfig)

    def create_config(
        self,
        task_type: str = DatasetTaskType.OBJECT_DETECTION.lower(),
        labels: list[str] = [],
    ) -> str:
        """Create a LabelStudio labeling interface configuration.

         The created interface configuration depends on the task type of the dataset
         i.e. classification, object detection and object tracking.

         Args:
            task_type: Task type of dataset.
            labels: List of labels present in dataset.

        Returns:
            LabelStudio interface configuration as string.
        """
        self._logger.info("Creating labels for %s task", task_type)

        if (
            task_type == DatasetTaskType.OBJECT_DETECTION.lower()
            or task_type == DatasetTaskType.ROTATED_OBJECT_DETECTION.lower()
        ):
            return self.__create_template_specific_configuration(
                labels=labels,
                label_template=TEMPLATE_DETECTION_LABEL,
                main_template=TEMPLATE_DETECTION_MAIN,
            )

        elif task_type == DatasetTaskType.INSTANCE_SEGMENTATION.lower():
            return self.__create_template_specific_configuration(
                labels=labels,
                label_template=TEMPLATE_DETECTION_LABEL,
                main_template=TEMPLATE_SEGMENTATION_MAIN,
            )

        elif task_type == DatasetTaskType.CLASSIFICATION.lower():
            return self.__create_template_specific_configuration(
                labels=labels,
                label_template=TEMPLATE_CLASSIFICATION_CLASS,
                main_template=TEMPLATE_CLASSIFICATION_MAIN,
            )

        elif task_type == DatasetTaskType.OBJECT_TRACKING.lower():
            # REMARK: This template is not complete on creation of the labelstudio project,
            #         as the framerate has to be loaded dynamically when the video has been
            #         stored in the s3
            return self.__create_template_specific_configuration(
                labels=labels,
                label_template=TEMPLATE_DETECTION_LABEL,
                main_template=TEMPLATE_TRACKING_MAIN,
            )  # type: ignore[unused-ignore]

        elif task_type == DatasetTaskType.TEXT_RECOGNITION.lower():
            return self.__create_template_specific_configuration(
                labels=labels,
                label_template=TEMPLATE_DETECTION_LABEL,
                main_template=TEMPLATE_TEXT_RECOGNITION_MAIN,
            )

        else:
            raise NotImplementedError(f"The task type '{task_type}' is not supported yet.")

    def initialize_project(
        self,
        dataset_name: str,
        task_type: str,
        labels: list[str],
    ) -> tuple[Project, str]:
        """Initialize a LabelStudio project.

        Initialize a project for a dataset by creating a corresponding labeling config, the
        label studio project itself and the cloud storage connections to the S3 storage.

        Args:
            dataset_name: Name of dataset.
            task_type: Task type of dataset.
            labels: List of labels present in dataset.

        Returns:
             The project and the randomly generated s3_prefix
        """
        project = self.ls_client.start_project(
            title=dataset_name, label_config=self.create_config(task_type, labels)
        )

        s3_prefix = f"project_{project.id}__{uuid.uuid4()}"
        self._init_s3_bucket(s3_prefix=s3_prefix)

        self._connect_cloud_storage(
            project=project,
            s3_prefix=s3_prefix,
            s3_credentials=self._get_credentials_for_label_studio(s3_prefix=s3_prefix),
        )

        return project, s3_prefix

    def _connect_cloud_storage(
        self, project: Project, s3_prefix: str, s3_credentials: CredentialsTypeDef
    ) -> None:
        """Connect a LabelStudio project to a cloud storage.

        Connect the project with the specified S3 storage by setting up a
        LabelStudio cloud storage integration for the import and export.

        Args:
            project: LabelStudio project.
            s3_prefix: Prefix of S3 bucket where dataset is stored.
            s3_credentials: Credentials to gain access to S3 bucket.
        """
        s3_endpoint_url = (
            self.s3_endpoint_url_external if self.s3_use_presigned_urls else self.s3_endpoint_url
        )

        self._logger.info(
            "Connecting cloud storage %s presigned urls to S3 at '%s'",
            "with" if self.s3_use_presigned_urls else "without",
            s3_endpoint_url,
        )

        import_settings: dict[str, Any] = {
            "s3_endpoint": s3_endpoint_url,
            "region_name": self.s3_region_name,
            "bucket": self.s3_bucket_name,
            "prefix": f"{s3_prefix}/{S3ProjectFolders.DATA}",
            "title": "Custom S3 data storage",
            "description": "A custom S3 raw data storage shared by the services.",
            "presign": self.s3_use_presigned_urls,
            "use_blob_urls": True,
            "aws_access_key_id": s3_credentials["AccessKeyId"],
            "aws_secret_access_key": s3_credentials["SecretAccessKey"],
            "aws_session_token": s3_credentials["SessionToken"],
        }

        export_settings = {
            "s3_endpoint": s3_endpoint_url,
            "region_name": self.s3_region_name,
            "bucket": self.s3_bucket_name,
            "prefix": f"{s3_prefix}/{S3ProjectFolders.TASKS}",
            "title": "Custom S3 data storage",
            "description": "A custom S3 storage for label studio annotation tasks.",
            "aws_access_key_id": s3_credentials["AccessKeyId"],
            "aws_secret_access_key": s3_credentials["SecretAccessKey"],
            "aws_session_token": s3_credentials["SessionToken"],
        }
        import_validation = self.validate_s3_import_settings(project, **import_settings)

        export_validation = self.validate_s3_export_settings(project, **export_settings)

        if not (import_validation and export_validation):
            self._logger.error(
                "Failed to validate custom S3 storage cloud connection to Label-Studio. Aborting!"
            )
            return

        s3_import_storage = project.connect_s3_import_storage(**import_settings)

        s3_export_storage = project.connect_s3_export_storage(**export_settings)

        project.sync_storage("s3", s3_import_storage["id"])
        project.sync_storage("export/s3", s3_export_storage["id"])

    def _init_s3_bucket(self, s3_prefix: str) -> None:
        """Initialize a S3 bucket with the necessary directory structure.

        The storage is to be used as a project's data storage.

        Args:
            s3_prefix: Prefix of S3 bucket where data is stored.
        """
        # init an empty "Directory"

        self.s3_client.put_object(Bucket=self.s3_bucket_name, Body="", Key=f"{s3_prefix}/")

        # setup data structure
        self.s3_client.put_object(
            Bucket=self.s3_bucket_name,
            Body="",
            Key=f"{s3_prefix}/{S3ProjectFolders.DATA}/",
        )
        self.s3_client.put_object(
            Bucket=self.s3_bucket_name,
            Body="",
            Key=f"{s3_prefix}/{S3ProjectFolders.TASKS}/",
        )

    def _get_credentials_for_label_studio(self, s3_prefix: str) -> CredentialsTypeDef:
        """Get STS credentials for the S3 bucket of the LabelStudio project.

        Helper method to get STS credentials for the S3 bucket to use with a LabelStudio cloud storage connection.
        The permissions of the credentials are limited for the specific project's data given by the s3_prefix.

        Args:
            s3_prefix: Prefix of S3 bucket where the dataset is stored.

        Returns:
            Dict with credentials for S3 bucket.
        """
        self._logger.info("Generating credentials for Label Studio")
        policy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "ListWholeBucket",
                    "Effect": "Allow",
                    "Action": ["s3:ListBucket"],
                    "Resource": f"arn:aws:s3:::{self.s3_bucket_name}",
                },
                {
                    "Sid": "GetObj",
                    "Effect": "Allow",
                    "Action": ["s3:GetObject"],
                    "Resource": f"arn:aws:s3:::{self.s3_bucket_name}/{s3_prefix}/*",
                },
                {
                    "Sid": "PutDelObj",
                    "Effect": "Allow",
                    "Action": ["s3:PutObject", "s3:DeleteObject"],
                    "Resource": f"arn:aws:s3:::{self.s3_bucket_name}/{s3_prefix}/tasks/*",
                },
            ],
        }
        response = self.sts_client.assume_role(
            RoleArn="arn:xxx:xxx:xxx:xxxx",
            RoleSessionName="whatever",
            Policy=json.dumps(policy, separators=(",", ":")),
            DurationSeconds=31536000,  # 365 days
        )

        assert (
            "Credentials" in response
        ), "Missing credentials in response from custom S3 storage Security Token Service"
        assert response["Credentials"].keys() >= {
            "AccessKeyId",
            "SecretAccessKey",
            "SessionToken",
        }, "Expected to find AccessKeyId, SecretAccessKey, and SessionToken in credentials"

        self._logger.info(
            "Generated credentials with AccessKeyId %s for Label Studio",
            response["Credentials"]["AccessKeyId"],
        )

        return response["Credentials"]

    # TODO: this is only a temporary fix to resolve issues to expired sts user access via the ls s3 client connection
    #  the function should be removed asap as it is not clear what security implications such an update function has
    def update_import_export_s3_settings_for_temporary_user(
        self, s3_prefix: str, ls_project_id: int
    ) -> bool:
        """Update a LabelStudio project's S3 storage import/export settings with new temporary credentials.

        The credentials are generated via the STS client.

        Args:
            s3_prefix: S3 bucket prefix.
            ls_project_id: ID of the LabelStudio project.

        Returns:
            True/False if the update was successful or not.
        """
        try:
            ls_project: Project = self.ls_client.get_project(ls_project_id)
            s3_credentials: CredentialsTypeDef = self._get_credentials_for_label_studio(
                s3_prefix=s3_prefix
            )
            url_import: str = "/api/storages/s3"
            url_export: str = "/api/storages/export/s3"
            params: dict[str, int] = {"project": ls_project_id}

            response_import_get: requests.Response = self.ls_client.make_request(
                method="GET", url=url_import, params=params
            )
            response_import_get_json: list[dict[str, Any]] = response_import_get.json()
            self._logger.info(
                f"Import settings for S3 storage of project '{ls_project_id}' are: {response_import_get_json}",
            )
            response_export_get: requests.Response = self.ls_client.make_request(
                method="GET", url=url_export, params=params
            )
            response_export_get_json: list[dict[str, Any]] = response_export_get.json()
            self._logger.info(
                f"Export settings for S3 storage of project '{ls_project_id}' are: {response_export_get_json}",
            )

            # an import and export storage has been already configured for the given ls project
            # we assume that only one storage is configured per project
            if (
                (response_import_get.status_code == 200) and (len(response_import_get_json) == 1)
            ) and (
                (response_export_get.status_code == 200) and (len(response_export_get_json) == 1)
            ):
                id_import_storage: int = response_import_get_json[0]["id"]
                id_export_storage: int = response_export_get_json[0]["id"]
                updated_access_credentials: dict[str, str] = {
                    "aws_access_key_id": s3_credentials["AccessKeyId"],
                    "aws_secret_access_key": s3_credentials["SecretAccessKey"],
                    "aws_session_token": s3_credentials["SessionToken"],
                }
                response_import_patch: requests.Response = self.ls_client.make_request(
                    method="PATCH",
                    url=url_import + "/" + str(id_import_storage),
                    json=updated_access_credentials,
                )
                response_export_patch: requests.Response = self.ls_client.make_request(
                    method="PATCH",
                    url=url_export + "/" + str(id_export_storage),
                    json=updated_access_credentials,
                )
                if (
                    response_export_patch.status_code == 200
                    and response_import_patch.status_code == 200
                ):
                    self._logger.info(
                        f"The import/export S3 settings in LabelStudio project '{ls_project_id}' have been updated.",
                    )
                    ls_project.sync_storage("s3", id_import_storage)
                    ls_project.sync_storage("export/s3", id_export_storage)
                    return True
                self._logger.error(
                    f"The import/export S3 settings in LabelStudio project '{ls_project_id}' could not be updated.",
                )
                return False

            # no import / export s3 configuration could be detected and has to be created
            self._logger.info(
                f"No import/export configuration for LabelStudio project '{ls_project_id}' could be detected. "
                f"Creating new configuration."
            )
            self._connect_cloud_storage(
                project=ls_project,
                s3_prefix=s3_prefix,
                s3_credentials=s3_credentials,
            )
            self._logger.info(
                f"The import/export S3 settings in LabelStudio project '{ls_project_id}' have been created.",
            )
            return True

        except Exception as e:
            self._logger.error(e)
            self._logger.error(
                f"The import/export S3 settings in LabelStudio project '{ls_project_id}' could not be updated. "
                f"An error occurred during the update process.",
            )
            return False

    def extract_all_images_from_zips(self, s3_prefix: str) -> None:
        """Ensure that all data in the S3ProjectFolders.DATA directory are as expected.

        Extract all image files from zip archives in the S3 bucket and upload them
        to the data directory. The method also removes the original zip archives
        after the extraction.

        Args:
            s3_prefix: S3 bucket prefix.
        """
        dataset_s3_root = f"{s3_prefix}/{S3ProjectFolders.DATA}"

        file_names = self._list_all_s3_objects(
            bucket_name=self.s3_bucket_name,
            prefix=dataset_s3_root,
        )

        for file_name in file_names:
            if not file_name.endswith(".zip"):
                continue

            zip_file_key = f"{dataset_s3_root}/{file_name}"
            self._logger.info("Extracting images from zip '%s'", zip_file_key)

            with tempfile.TemporaryDirectory() as tempdir:
                # download zip archive
                archive_path = f"{tempdir}/{file_name}"
                self.s3_client.download_file(
                    Bucket=self.s3_bucket_name, Key=zip_file_key, Filename=archive_path
                )

                # unpack archive
                with ZipFile(archive_path, "r") as archive:
                    archive.extractall(tempdir)

                image_paths = (
                    [str(image_path) for image_path in Path(tempdir).glob("**/*.jpg")]
                    + [str(image_path) for image_path in Path(tempdir).glob("**/*.jpeg")]
                    + [str(image_path) for image_path in Path(tempdir).glob("**/*.png")]
                )
                self._logger.info(
                    "Found %s images in %s for uploading to dataset at " "%s",
                    len(image_paths),
                    file_name,
                    dataset_s3_root,
                )

                # upload image files to S3
                for image_path in image_paths:
                    with open(image_path, "rb") as image_file:
                        key = f"{dataset_s3_root}/{os.path.basename(image_path)}"
                        self._logger.debug(
                            "Uploading image '%s' to bucket '%s'", key, self.s3_bucket_name
                        )
                        self.s3_client.upload_fileobj(
                            image_file,
                            self.s3_bucket_name,
                            key,
                        )

            # remove the zip archive
            self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=zip_file_key)

    def sync_all_s3_cloud_storages(self, project_id: int) -> int:
        """Sync all S3 storages of the LabelStudio project.

        Trigger the sync_storage operation for all S3 cloud storages
        configured for the project with the given project_id.

        Args:
            project_id: ID of the project.

        Returns:
            The total number of imported tasks.
        """
        self._logger.info("Syncing all Cloud Storages for project id=%d.", project_id)
        project = self.ls_client.get_project(project_id)

        params = {"project": project.id}
        total_imported_task_count = 0

        # import S3 storages
        response = project.make_request("GET", "/api/storages/s3", params=params)
        for storage_id in [
            storage["id"] for storage in response.json() if storage["type"] == "s3"
        ]:
            res = project.sync_storage("s3", storage_id)
            total_imported_task_count += int(res["last_sync_count"])

        # export S3 storages
        response = project.make_request("GET", "/api/storages/export/s3", params=params)
        for storage_id in [
            storage["id"] for storage in response.json() if storage["type"] == "s3"
        ]:
            project.sync_storage("export/s3", storage_id)
        return total_imported_task_count

    def export_annotations_to_s3(
        self,
        project_id: int,
        s3_prefix: str,
        filename: str,
    ) -> str:
        """Export the annotation data of a LabelStudio project to the S3 storage.

        Export the annotation data of a LabelStudio project
        together with the images into a single zip archive.
        The annotations are fetched from LabelStudio directly.
        Using the annotations file we download the images from S3 based on the file name specified.
        Afterwards, we zip everything together and upload that archive to S3.
        Finally, a pre-signed URL for that archive in S3 storage is generated and returned

        Args:
            project_id: The ID of the LabelStudio project you wish to export annotations for.
            s3_prefix: The prefix of the S3 bucket where the project's data is located.
            filename: The name of the exported file.
            export_type: The format of the generated annotations, e.g., COCO, YOLO.

        Returns:
            A pre-signed URL pointing to the zip archive in S3 storage.
        """
        export_type = DatasetExportTypes.JSON.upper()

        self._logger.info(
            "Exporting annotations for project id=%d using format %s.",
            project_id,
            export_type,
        )
        if s3_prefix is None:
            raise ValueError("S3 prefix not found")

        if export_type not in [ex_type.upper() for ex_type in DatasetExportTypes]:
            raise NotImplementedError(f"The export type {export_type} is not supported")

        results = self.__parse_annotations_from_labelstudio(
            export_type=export_type,
            project=self.ls_client.get_project(project_id),
            project_id=project_id,
        )

        if not isinstance(results, list):
            raise ValueError("Unsupported type for exporting annoations to s3: %s", type(results))

        # We need to store the final zip file in the file system
        # because it's required for the upload later on.
        # We use a temporary directory  (tmp_outdir)to do so
        # because we don't care about the data after uploading.
        # Using a temporary file would also be an option
        # with the downside of resetting the file pointer all the time
        # because we access the file multiple times
        with tempfile.TemporaryDirectory() as tmp_outdir:
            outzip_path = Path(tmp_outdir) / "annotations_with_images.zip"
            with ZipFile(outzip_path, mode="w") as tmp_outzip:
                with tempfile.TemporaryDirectory() as tmp_images_dir:
                    images_dir_path = os.path.join(tmp_images_dir, "images")
                    os.makedirs(images_dir_path)

                    # download all referenced images from S3 storage to that directory
                    for result in results:
                        image_path = result["data"]["image"]

                        new_image_path = os.path.join(
                            images_dir_path, os.path.basename(image_path)
                        )
                        self._logger.info(
                            "Downloading image from %s to %s.",
                            image_path,
                            new_image_path,
                        )

                        bucket_path = "s3://" + self.s3_bucket_name + "/"

                        s3_file_key = image_path.replace(bucket_path, "")

                        self.s3_client.download_file(
                            Bucket=self.s3_bucket_name,
                            Key=s3_file_key,
                            Filename=new_image_path,
                        )

                        assert os.path.isfile(
                            new_image_path
                        ), f"There is no downloaded file from S3 at '{image_path}'."

                        self._logger.info("Adding image %s to zip", new_image_path)
                        tmp_outzip.write(
                            str(new_image_path),
                            arcname=os.path.relpath(new_image_path, tmp_images_dir),
                        )

                # write the output zip file
                tmp_outzip.writestr(
                    f"{s3_prefix}_ls.json",
                    json.dumps(results, indent=2),
                )

            self.__validate_generated_zip_file(zip_path=outzip_path)

            # upload everything to s3
            self._logger.info(
                "Storing exported annotations for project id=%d in S3 storage.",
                project_id,
            )
            with open(outzip_path, "rb") as outzipfile:
                self.s3_client.upload_fileobj(
                    outzipfile,
                    Bucket=self.s3_bucket_name,
                    Key=f"{s3_prefix}/annotated/{filename}.zip",
                )

        self._logger.info(
            "Creating presigned url for '%s'",
            s3_prefix + "/annotated/" + filename + ".zip",
        )
        annotations_zip_presigned_url = self.s3_client.generate_presigned_url(
            "get_object",
            Params={
                "Bucket": self.s3_bucket_name,
                "Key": f"{s3_prefix}/annotated/{filename}.zip",
            },
            ExpiresIn=int(timedelta(hours=1).total_seconds()),
        )

        annotations_zip_presigned_external_url = self.make_external_presigned_url(
            annotations_zip_presigned_url
        )

        return annotations_zip_presigned_external_url

    def make_external_presigned_url(self, url_str: str) -> str:
        """Change presigned url to be accessible externally.

        Change hostname of a (presigned s3) url from a local endpoint
        to an externally accessible endpoint.
        That new endpoint is normally a (NGINX) reverse proxy,
        forwarding the request to the original S3 endpoint.
        It updates the protocol (e.g. HTTP to HTTPS),
        the hostname (e.g. s3_storage to localhost),
        and the port (e.g. 9000 to 443)

        Example:
          url = make_external_url("http://s3_storage-internal-name:9000/bucket/file")
          # url is now "https://s3_storage.example.com/bucket/file"

        Args:
            url_str: A (pre-signed) URL string pointing to a file.

        Returns:
            A pre-signed URL pointing to the same file with externally accessible hostname.
        """
        url = urlparse(url_str)
        internal_endpoint = urlparse(self.s3_endpoint_url)

        if url.scheme != internal_endpoint.scheme or url.netloc != internal_endpoint.netloc:
            self._logger.warning(
                "Expected to get an interal presigned url with endpoint '%s' but got '%s'.",
                internal_endpoint.geturl(),
                url.geturl(),
            )

        external_endpoint = urlparse(self.s3_endpoint_url_external)
        external_url = url._replace(
            scheme=external_endpoint.scheme, netloc=external_endpoint.netloc
        )

        return external_url.geturl()

    def delete_files_from_s3_folder(self, s3_prefix: str, folder: str) -> None:
        """Delete files from given S3 folder.

        Args:
            s3_prefix: Prefix of S3 bucket.
            folder: Folder from which all files should be deleted.
        """
        paginator = self.s3_client.get_paginator("list_objects_v2")
        pages = paginator.paginate(Bucket=self.s3_bucket_name, Prefix=f"{s3_prefix}/{folder}")

        file_keys: list[ObjectIdentifierTypeDef] = []
        for item in pages.search("Contents"):
            if item is not None:
                file_keys.append(dict(Key=item["Key"]))

                # delete files now if number of files reaches S3 limit of 1000 files
                if len(file_keys) == 1000:
                    self.s3_client.delete_objects(
                        Bucket=self.s3_bucket_name, Delete=dict(Objects=file_keys)
                    )
                    file_keys.clear()

        # delete remaining files
        if file_keys:
            self.s3_client.delete_objects(
                Bucket=self.s3_bucket_name, Delete=dict(Objects=file_keys)
            )

    def delete_project(self, project_id: int, s3_prefix: str) -> None:
        """Delete all files related to a labeling project from S3 storage.

        Args:
            project_id: ID of the labeling project.
            s3_prefix: Prefix of the S3 bucket.
        """
        self._logger.info("Delete project id=%d.", project_id)

        # delete the images from s3
        self.delete_files_from_s3_folder(s3_prefix=s3_prefix, folder=S3ProjectFolders.DATA.value)
        # delete the tasks and exported annotations from s3
        self.delete_files_from_s3_folder(s3_prefix=s3_prefix, folder=S3ProjectFolders.TASKS.value)
        self.delete_files_from_s3_folder(
            s3_prefix=s3_prefix, folder=S3ProjectFolders.ANNOTATED.value
        )

        # delete the folders from s3
        objects = self.s3_client.list_objects(Bucket=self.s3_bucket_name, Prefix=s3_prefix)
        if objects.get("Contents", []):
            # delete all remaining objects from prefix if there are any
            keys: DeleteTypeDef = {
                "Objects": [
                    {"Key": k} for k in [obj["Key"] for obj in objects.get("Contents", [])]
                ]
            }
            self.s3_client.delete_objects(Bucket=self.s3_bucket_name, Delete=keys)

        # TODO: Remove try / except block and move first line to the start of the method
        #       when adding a sync database operation
        # delete project from label studio
        try:
            project = self.ls_client.get_project(project_id)
            project.make_request("DELETE", f"/api/projects/{project_id}/")
        except requests.exceptions.HTTPError:
            self._logger.warning("Could not find labelstudio project id=%d.", project_id)

    def delete_file_from_s3(self, s3_prefix: str, s3_folder: str, file_name: str) -> None:
        """Delete a single file from S3 storage.

        Args:
            s3_prefix: Prefix of S3 bucket.
            s3_folder: Folder inside S3 bucket.
            file_name: File to delete.
        """
        if file_name and s3_prefix:
            self._logger.info("Delete file '%s' from S3 storage", file_name)
            self.s3_client.delete_object(
                Bucket=self.s3_bucket_name, Key=f"{s3_prefix}/{s3_folder}/{file_name}"
            )

    def generate_s3_presigned_url_upload(
        self, s3_prefix: str, s3_folder: str, file_name: str
    ) -> str:
        """Generate a presigned URL to be used by the frontend to upload a file to S3.

        Args:
            s3_prefix: Prefix of S3 bucket.
            s3_folder: Folder inside S3 bucket.
            file_name: File to generate presigned url for.

        Returns:
            Presigned url.
        """
        if file_name and s3_prefix:
            self._logger.info(f"Generate S3 presigned url (upload) for '{file_name}'")
            file_name = self.__replace_whitespaces(file_name)
            url = self.s3_client.generate_presigned_url(
                ClientMethod="put_object",
                Params={
                    "Bucket": self.s3_bucket_name,
                    "Key": f"{s3_prefix}/{s3_folder}/{file_name}",
                },
                ExpiresIn=int(timedelta(minutes=5).total_seconds()),
            )
            external_url = self.make_external_presigned_url(url)
            return external_url

        raise ValueError(
            "One or more of the provided parameters are empty strings. Please "
            "provide valid parameters!"
        )

    def generate_s3_presigned_url_get_object(
        self, s3_prefix: str, s3_folder: str, file_name: str
    ) -> str:
        """Generate a presigned URL to be used by the frontend to read a file object from S3.

        Args:
            s3_prefix: Prefix of S3 bucket.
            s3_folder: Folder inside S3 bucket.
            file_name: File to generate presigned url for.

        Returns:
            Presigned url.
        """
        if file_name and s3_prefix:
            self._logger.info(f"Generate S3 presigned url (download) for '{file_name}'")
            file_name = self.__replace_whitespaces(file_name)
            url = self.s3_client.generate_presigned_url(
                ClientMethod="get_object",
                Params={
                    "Bucket": self.s3_bucket_name,
                    "Key": f"{s3_prefix}/{s3_folder}/{file_name}",
                },
                ExpiresIn=int(timedelta(minutes=5).total_seconds()),
            )
            external_url = self.make_external_presigned_url(url)
            return external_url

        raise ValueError(
            "One or more of the provided parameters are empty strings. Please "
            "provide valid parameters!"
        )

    def set_framerate_labelstudio_project_template(
        self,
        project_id: int,
        presigned_url_video_file: str,
    ) -> requests.Response:
        """Set the framerate in a LabelStudio project template for tracking tasks.

        Set the $framerate placeholder in the label config of an already created LabelStudio project with the actual
        framerate of the uploaded video from its presigned url.

        Args:
            project_id: ID of the labeling project.
            presigned_url_video_file: The url with read access to the video file on the S3 storage.

        Returns:
            The HTTP response of the LabelStudio client's patch request.
        """
        project: Project = self.ls_client.get_project(id=project_id)
        video: cv2.VideoCapture = self._load_video(presigned_url=presigned_url_video_file)
        framerate: float = LabelingService._get_framerate(video=video)
        label_config_old: str = project.get_params()["label_config"]
        label_config_new: str = label_config_old.replace("$framerate", str(framerate))
        response: requests.Response = project.make_request(
            method="PATCH",
            url=f"/api/projects/{project_id}/",
            params={"id": project_id},
            data={"label_config": label_config_new},
        )
        return response

    def get_preview_images(self, s3_prefix: str) -> list[dict[str, str]]:
        """Get preview images for a dataset associated to a LabelStudio project.

        Generate presigned urls for a subset of all images in the project's data. The
        subset will later on be used as preview images to be shown in the frontend.

        Args:
            s3_prefix: Prefix of S3 bucket.

        Returns:
            List with presigned urls of images. Each url is stored inside a dict with key:
            'url' and value: presigned_url
        """
        files = self.s3_client.list_objects_v2(
            Bucket=self.s3_bucket_name,
            Prefix=f"{s3_prefix}/{S3ProjectFolders.DATA}/",
            MaxKeys=5,
            EncodingType="url",
        )
        urls = []
        for file in files.get("Contents", []):
            file_name = file["Key"].split("/")[-1]
            if file_name.endswith(("jpg", "jpeg", "png")):
                url = self.s3_client.generate_presigned_url(
                    ClientMethod="get_object",
                    Params={
                        "Bucket": self.s3_bucket_name,
                        "Key": f"{s3_prefix}/data/{file_name}",
                    },
                    ExpiresIn=int(timedelta(minutes=5).total_seconds()),
                )
                external_url = self.make_external_presigned_url(url)
                urls.append({"url": external_url})
        return urls

    def _check_annotation_file_for_errors(self, s3_prefix: str) -> tuple[str, str]:
        """Check the annotation file for the given project for mistakes.

        Args:
            s3_prefix: Prefix of S3 bucket.
        """
        annotation_files = self.s3_client.list_objects_v2(
            Bucket=self.s3_bucket_name,
            Prefix=f"{s3_prefix}/{S3ProjectFolders.PREANNOTATION}/",
        )
        if len(annotation_files["Contents"]) > 1:
            self._logger.warning(
                "More than one annotation file found in S3. Using the latest one for annotation"
            )
            sorted_files = sorted(
                annotation_files["Contents"], key=lambda x: x["LastModified"], reverse=True
            )
            annotation_files["Contents"] = sorted_files

        annotation_filename = annotation_files["Contents"][0]["Key"].split("/")[-1]
        annotation_format = annotation_filename.split(".")[0]

        if annotation_format not in ("ls_json", "coco_json", "azure_csv"):
            raise WrongFormatException("Wrong annotation format was provided.")

        return annotation_filename, annotation_format

    def _convert_annotation_to_ls_json(
        self, annotation_filename: str, annotation_format: str, s3_prefix: str
    ) -> list[dict[str, Any]]:
        """Convert several annotation file formats to the LabelStudio JSON format.

        Args:
            annotation_filename: Name of the annotation file
            annotation_format: Format of the annotation file
            s3_prefix: Prefix of S3 bucket

        Returns:
            List of converted annotations. The converted annotations are in the
            LabelStudio-specific JSON format.
        """
        s3_annotation_file = self.s3_client.get_object(
            Bucket=self.s3_bucket_name,
            Key=f"{s3_prefix}/{S3ProjectFolders.PREANNOTATION}/{annotation_filename}",
        )
        annotation_bytes = s3_annotation_file["Body"].read()

        annotation_content_ls = []

        # We need to differentiate between JSON files and CSV files,
        # because they will be processed differently
        if annotation_format in ("ls_json", "coco_json"):
            # In case we get a JSON file, we can read the content from
            # the byte object
            annotation_json = json.loads(annotation_bytes)
            # The LabelStudio JSON format does not need any conversion
            if annotation_format == "ls_json":
                annotation_content_ls = annotation_json
            # The coco file format needs to be parsed to the LabelStudio
            # JSON format
            elif annotation_format == "coco_json":
                # In case the coco file contains empty segmentations we need to
                # remove them from the annotations to prevent errors in the
                # LabelStudio converter
                for annotation in annotation_json["annotations"]:
                    if "segmentation" in annotation and len(annotation.get("segmentation")) == 0:
                        annotation.pop("segmentation")

                # The coco format stores a bounding-box for every segmentation. For now skip this
                # bounding-box if a segmentation is present, since we primarily want to import
                # segmentation if this one present.
                for annotation in annotation_json["annotations"]:
                    if "segmentation" in annotation and "bbox" in annotation:
                        annotation.pop("bbox")
                try:
                    # The LabelStudio converter only accepts files:
                    # Write the JSON request payload into a file
                    with tempfile.NamedTemporaryFile(
                        mode="w", delete=False, suffix=".json"
                    ) as tmp_coco_file:
                        json.dump(annotation_json, tmp_coco_file)
                    # Create a result file
                    tmp_result_file = tempfile.NamedTemporaryFile(
                        mode="w", delete=False, suffix=".json"
                    )
                    # Convert the COCO file content to LabelStudio JSON
                    # and write it to the result file
                    convert_coco_to_ls(
                        input_file=tmp_coco_file.name,
                        out_file=tmp_result_file.name,
                        to_name="image",
                        from_name="label",
                    )
                    # Read the result as JSON
                    with open(tmp_result_file.name) as result_file:
                        annotation_content_ls = json.load(result_file)
                    # Close the temporary files
                    tmp_coco_file.close()
                    tmp_result_file.close()
                    # Correct the annotations from_name to fit our labeling config
                    for annotation in annotation_content_ls:
                        for idx in range(0, len(annotation["annotations"][0]["result"])):
                            annotation["annotations"][0]["result"][idx]["from_name"] = "label"
                finally:
                    # Unlink the temporary files to completely remove them
                    os.unlink(tmp_coco_file.name)
                    os.unlink(tmp_result_file.name)
        elif annotation_format == "azure_csv":
            # The azure format needs to be converted to the LabelStudio JSON format
            annotation_content_ls = convert_azure_to_ls(annotation_bytes)

        return annotation_content_ls

    def import_ls_annotation(self, project_id: int, s3_prefix: str) -> int:
        """Import a given annotation file for the specified LabelStudio project.

        Args:
            project_id: ID of the project.
            s3_prefix: Prefix of S3 bucket.

        Returns:
            Number of imported annotations.
        """
        self._logger.info("Importing annotation for project id=%d.", project_id)
        project = self.ls_client.get_project(project_id)

        annotation_filename, annotation_format = self._check_annotation_file_for_errors(s3_prefix)
        ls_annotation_content = self._convert_annotation_to_ls_json(
            annotation_filename, annotation_format, s3_prefix
        )

        # Load the annotation files of the corresponding s3 bucket
        s3_filenames = set(
            self._list_all_s3_objects(
                bucket_name=self.s3_bucket_name,
                prefix=f"{s3_prefix}/{S3ProjectFolders.DATA}/",
            )
        )
        self._logger.info("Received %d filenames from S3", len(s3_filenames))

        # Remove existing annotations
        self.delete_files_from_s3_folder(s3_prefix=s3_prefix, folder="tasks")

        # Remove annotations that do not have a corresponding image in the s3 bucket
        ls_annotation_content[:] = [
            annotation
            for annotation in ls_annotation_content
            if os.path.basename(annotation["data"]["image"]) in s3_filenames
        ]
        self._logger.info(
            "Processing %d annotations after filtering images", len(ls_annotation_content)
        )

        labels = self.__extract_labels(project.get_params().get("label_config"))
        self._logger.info(labels)
        ls_annotation_content = self.__filter_annotation_for_all_labels(
            ls_annotation_content, labels
        )
        self._logger.info(
            "Processing %d annotations after filtering labels", len(ls_annotation_content)
        )

        # Get the labelstudio tasks of the project
        ls_tasks = project.make_request(
            "GET", f"/api/projects/{project_id}/tasks?page_size=-1"
        ).json()
        self._logger.info("Received %d labeling tasks", len(ls_tasks))

        # Create a dictionary that maps the image file name to its annotation content
        # Replace spaces in image name with underscores in the annotation paths to avoid
        # displaying errors
        annotation_content_dict = {
            os.path.basename(annotation["data"]["image"]).replace(" ", "_"): annotation
            for annotation in ls_annotation_content
        }
        num_imported_annotations = 0
        for ls_task in ls_tasks:
            for _annotation in ls_task["annotations"]:
                self.ls_client.make_request(
                    method="DELETE", url=f"/api/annotations/{_annotation['id']}"
                )

            image_name = os.path.basename(ls_task["data"]["image"])
            # Check if the task image name is in the annotation_content_dict
            if image_name in annotation_content_dict:
                # A match between tasks in LabelStudio and the annotations
                # that should be uploaded was found
                annotation = annotation_content_dict[image_name]

                data = {
                    "result": [_result for _result in annotation["annotations"][0]["result"]],
                    "was_cancelled": annotation["annotations"][0].get("was_cancelled", False),
                    "ground_truth": annotation["annotations"][0].get("ground_truth", False),
                    "lead_time": annotation["annotations"][0].get("lead_time", None),
                    "task": ls_task["id"],
                    "parent_prediction": annotation["annotations"][0].get(
                        "parent_prediction", None
                    ),
                    "parent_annotation": annotation["annotations"][0].get(
                        "parent_annotation", None
                    ),
                }

                # Post the annotation to LabelStudio
                project.make_request("POST", f"/api/tasks/{ls_task['id']}/annotations", json=data)
                num_imported_annotations += 1
                # Remove the annotation object from the list
                # since it does not need to be imported again
                del annotation_content_dict[image_name]

        return num_imported_annotations

    def _list_all_s3_objects(self, bucket_name: str, prefix: str) -> Generator[str, None, None]:
        """List all S3 objects in a given bucket and prefix and return the names of the files.

        Args:
            bucket_name: Name of the S3 bucket
            prefix: Prefix to filter the S3 objects

        Yields:
            str: Names of the files.
        """
        continuation_token = None
        while True:
            if continuation_token:
                response = self.s3_client.list_objects_v2(
                    Bucket=bucket_name,
                    Prefix=prefix,
                    ContinuationToken=continuation_token,
                )
            else:
                response = self.s3_client.list_objects_v2(Bucket=bucket_name, Prefix=prefix)
            for file in response.get("Contents", []):
                file_name = file["Key"].split("/")[-1]
                if file_name != "":
                    yield file_name
            if "NextContinuationToken" in response:
                continuation_token = response["NextContinuationToken"]
            else:
                break

    def _load_video(self, presigned_url: str) -> cv2.VideoCapture:
        """Load a video file located on the S3 storage from its presigned url with data verification via md5 checksum.

        Args:
            presigned_url: The video files location on the S3 as a presigned url.

        Returns:
            An VideoCaputure instance from the loaded video.

        Raises:
            IOError: The tempfile can not be created.
        """
        md5_checksums_not_equal = True
        self._logger.info("Waiting for sync between video file upload and S3 storage.")
        parse_results = urlparse(presigned_url)
        _, bucket, key = parse_results.path.split("/", 2)
        s3_eid_md5 = self.s3_client.get_object(Bucket=bucket, Key=f"{key}")["ETag"].replace(
            '"', ""
        )
        i = 0
        while md5_checksums_not_equal and i < LabelingServiceConstants.MAX_NUM_CHECKS:
            i += 1
            try:
                self._logger.info("Checking file integrity with md5...")
                hash_md5 = md5()
                video: bytes = requests.get(presigned_url).content
                hash_md5.update(video)
                if hash_md5.hexdigest() == s3_eid_md5:
                    md5_checksums_not_equal = False
                    self._logger.info("Checksums match, loading video file now.")
                self._logger.info("Checksums do not match.")

            except (self.s3_client.exceptions.ClientError, self.s3_client.exceptions.NoSuchKey):
                self._logger.info("File has not been uploaded to S3 yet, waiting...")
                sleep(LabelingServiceConstants.WAIT_TIME_SECONDS)

        try:
            # cv2 only loads VideoCapture from filepath or indexed datasource, so we need
            # a named temporary file to load from, not only the file handler
            with tempfile.NamedTemporaryFile() as f:
                f.write(video)
                return cv2.VideoCapture(f.name)
        except IOError as e:
            self._logger.error("The temporary video file could not be created.")
            raise e

    @staticmethod
    def _get_framerate(video: cv2.VideoCapture) -> float:
        """Get the framerate of a loaded cv2 video file.

        Args:
            video: A cv2.VideoCapture instance.

        Returns:
            The video's framerate.
        """
        return video.get(cv2.CAP_PROP_FPS)

    def __replace_whitespaces(self, file_name: str) -> str:
        """Replace whitespaces in file names to build correct presigned urls for file up-/download on S3 storage.

        Args:
            file_name: Name of the file.

        Returns:
            The file name without whitespaces.
        """
        if file_name.__contains__(" "):
            self._logger.warning("Replaced spaces with underscores in filename '%s'", file_name)
            file_name = file_name.replace(" ", "_")
        return file_name

    def __extract_labels(self, xml_string: str) -> list[str | None]:
        """Extract all labels from the input field in the data upload of the guided training service.

        Args:
            xml_string: Label config data given as xml tree.

        Raises:
            ParseError: Xml string not in xml format is invalid.

        Returns:
            List of all labels in the config.

        """
        label_names: list[str | None] = []
        if xml_string is None:
            return label_names
        try:
            root = etree.fromstring(xml_string)
            rectangle_labels = root.find(".//RectangleLabels[@name='label']")

            if rectangle_labels is not None:
                labels = rectangle_labels.findall(".//Label[@value]")
                return [label.get("value") for label in labels]

            polygon_labels = root.find(".//PolygonLabels[@name='label']")

            if polygon_labels is not None:
                labels = polygon_labels.findall(".//Label[@value]")
                return [label.get("value") for label in labels]

        except etree.ParseError as e:
            self._logger.error(f"Error parsing XML: {e}")

        return label_names

    def __filter_annotation_for_all_labels(
        self, ls_annotation_content: list[dict[str, Any]], labels: list[str | None]
    ) -> list[dict[str, Any]]:
        """Filter the label studio annotation content by the given labels.

        Args:
            ls_annotation_content: Content of the annotation data in label studio format.
            labels: List of allowed labels.

        Returns:
            Content of the annotation data containing only labels from labels.

        """
        filtered_ls_annotation_content = []
        for item in ls_annotation_content:
            filtered_annotations = []
            for annotation in item.get("annotations", []):
                filtered_results = []
                for result in annotation.get("result", []):
                    filtered_results.extend(self.__check_item_for__labels(result, labels))
                if filtered_results:
                    annotation["result"] = filtered_results
                    filtered_annotations.append(annotation)

            if filtered_annotations:
                item["annotations"] = filtered_annotations
                filtered_ls_annotation_content.append(item)
        return filtered_ls_annotation_content

    def __check_item_for__labels(
        self, result: Any, labels: list[str | None]
    ) -> list[dict[str, Any]]:
        """Check the result of an annotation containing only labels listed in labels.

        The check differentiates between "rectanglelabels" used in object detection and classification labels
        given by "choices" or "value" in the annotation data depending on the original format.

        Args:
            result: Content of the annotation.
            labels: List of allowed labels.

        Returns:
            Annotation content limited to the allows labels.

        """
        filtered_results = []
        if "value" in result:
            if "rectanglelabels" in result["value"]:
                rectangle_labels = result["value"]["rectanglelabels"]
                if all(label in labels for label in rectangle_labels):
                    filtered_results.append(result)
            elif "polygonlabels" in result["value"]:
                polygon_labels = result["value"]["polygonlabels"]
                if all(label in labels for label in polygon_labels):
                    filtered_results.append(result)
            elif "labels" in result["value"]:
                _labels = result["value"]["labels"]
                if all(label in labels for label in _labels):
                    filtered_results.append(result)
            elif "choices" in result["value"]:
                choices = result["value"]["choices"]
                if all(label in labels for label in choices):
                    filtered_results.append(result)
            else:
                # value = result["value"]
                # if value in labels:
                filtered_results.append(result)

        return filtered_results
