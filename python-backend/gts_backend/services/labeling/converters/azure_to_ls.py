# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines functions to convert data structures."""

import io
import re
from typing import Any


def create_json(filepath: str, annotations: list[str]) -> dict[str, Any]:
    """Creates JSON with fixed keys.

    Sets the given filepath and the annotations as values of specific keys.

    Args:
        filepath: Path of file.
        annotations: List of annotations.

    Returns:
        The created JSON as dict.
    """
    json_res: dict[str, Any] = {
        "data": {"image": ""},
        "annotations": [
            {
                "result": [
                    {
                        "value": {"choices": []},
                        "from_name": "choice",
                        "to_name": "image",
                        "type": "choices",
                        "origin": "manual",
                    }
                ]
            }
        ],
    }
    json_res["data"]["image"] = filepath
    json_res["annotations"][0]["result"][0]["value"]["choices"] = annotations
    return json_res


def get_filepath_and_annotations(line: str) -> tuple[str, list[str]]:
    """Gets a filepath and a list of annotations from a given line.

    Args:
        line: String from which to get the filepath and the annotations.

    Returns:
        Tuple where the first element is the filepath and the second element is the list of
        annotations.
    """
    splitted = line.split(",")

    # parse filename
    filepath = splitted[0]

    annotations = []
    # get annotations
    annotations = re.findall('"([^"]*)"', f"SetVariables {line}'")

    if len(annotations) < 1:
        annotations.append(splitted[1])
    annotations = annotations[0].split(",")
    return filepath, annotations


def convert_azure_to_ls(file_bytes: bytes) -> list[dict[str, Any]]:
    """Converts bytes into a list of dicts in a LabelStudio-specific JSON format.

    Args:
        file_bytes: Bytes to convert.

    Returns:
        List of dicts. Each dict is in the LabelStudio-specific JSON format.
    """
    json_out = []
    file_formats = [".jpg", ".jpeg"]
    file = io.StringIO(file_bytes.decode())
    for line in file:
        if not any(format in line for format in file_formats):
            continue
        filename, annotations = get_filepath_and_annotations(line.strip())
        json_out.append(create_json(filename, annotations))
    return json_out
