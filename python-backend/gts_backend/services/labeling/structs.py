"""Module for structs used in labeling service."""

from enum import Enum


class LabelingServiceConstants:
    """Store constants for labeling service."""

    MAX_NUM_CHECKS = 10
    WAIT_TIME_SECONDS = 0.5


class LabelStudioProjectInformationAttributes(Enum):
    """Relevant information attributes about a LabelStudio project to be displayed in DataManagement tab."""

    ID = "id"
    TITLE = "title"
    TASK_NUMBER = "task_number"
    NUM_TASK_WITH_ANNOTATIONS = "num_tasks_with_annotations"
