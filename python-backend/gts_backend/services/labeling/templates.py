# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""String Templates used for the generation of Label Studio labeling interface specifications."""

# disable flake8 for file because of line length
# flake8: noqa
from string import Template

TEMPLATE_DETECTION_MAIN = Template(
    """
    <View>
      <RectangleLabels name="label" toName="image">
        $labels
      </RectangleLabels>
      <Image name="image" value="$$image" zoom="true" zoomControl="true" crosshair="true" defaultZoom="fit"/>
      <Header value="Select the label and fit a bounding box around every matching object. Leave a very tiny margin around the actual object."/>
    </View>
    """
)

TEMPLATE_SEGMENTATION_MAIN = Template(
    """
    <View>
      <PolygonLabels name="label" toName="image">
        $labels
      </PolygonLabels>
      <Image name="image" value="$$image" zoom="true" zoomControl="true" crosshair="true" defaultZoom="fit"/>
      <Header value="Select the label and draw the polygon around every matching object. Leave a very tiny margin around the actual object."/>
    </View>
    """
)

TEMPLATE_DETECTION_LABEL = Template("""<Label value="$label"/>""")

TEMPLATE_CLASSIFICATION_MAIN = Template(
    """
    <View>
      <Header value="Select the best matching class for the image."/>
      <Image name="image" value="$$image" zoom="true" zoomControl="true"/>
      <Choices name="choice" toName="image">
        $labels
      </Choices>
    </View>
    """
)

TEMPLATE_CLASSIFICATION_CLASS = Template("""<Choice value="$label"/>""")

TEMPLATE_TRACKING_MAIN = Template(
    """
    <View>
      <Header>Select the label and fit a bounding box around every matching object in each video frame. Leave a very tiny margin around the actual object.</Header>
      <Video name="video" value="$$video" framerate="$$framerate"/>
      <VideoRectangle name="box" toName="video"/>
      <Labels name="videoLabels" toName="video">
        $labels
      </Labels>
    </View>
    """
)

TEMPLATE_TEXT_RECOGNITION_MAIN = Template(
    """
    <View>
        <Labels name="label" toName="image">
            $labels
        </Labels>
        <Rectangle name="bbox" toName="image" strokeWidth="3"/>
        <Polygon name="poly" toName="image" strokeWidth="3"/>
        <TextArea name="transcription" toName="image"
                editable="true"
                perRegion="true"
                required="false"
                maxSubmissions="1"
                rows="5"
                placeholder="Recognized Text"
                displayMode="region-list"
                />
        <Image name="image" value="$$image" zoom="true" zoomControl="true" crosshair="true" defaultZoom="fit"/>
        <Header value="Select the label and fit a bounding box around every matching object. Leave a very tiny margin around the actual object."/>
    </View>
    """
)
