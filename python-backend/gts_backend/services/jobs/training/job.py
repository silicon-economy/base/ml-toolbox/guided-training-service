# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the TrainingJob.

The TrainingJob is used to run training jobs.
"""

import copy
import logging
from datetime import datetime
from typing import Any

import requests
from config_builder.json_encoding import TupleEncoder
from mlcvzoo_util.adapters.mlflow.structs import TrainingStatus
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.services.configuration_creator.base.creator import ConfigCreator
from gts_backend.services.configuration_creator.mmdetection.creator import (
    MMDetectionConfigCreator,
)
from gts_backend.services.configuration_creator.mmocr.creator import MMOCRConfigCreator
from gts_backend.services.configuration_creator.mmpretrain.creator import (
    MMPretrainConfigCreator,
)
from gts_backend.services.configuration_creator.structs import (
    ImplementedModels,
    StringReplacements,
)
from gts_backend.services.configuration_creator.yolox.creator import YOLOXConfigCreator
from gts_backend.services.jobs.base.job import BaseJob
from gts_backend.services.jobs.training.configuration import TrainingJobConfig
from gts_backend.services.jobs.training.structs import (
    DataRootDirs,
    ImplementedConfigCreators,
    TrainingJobData,
    TrainingRunServerEndpoint,
)

logger = logging.getLogger(__name__)


class TrainingJob(BaseJob):
    """Run training jobs."""

    def __init__(
        self,
        config: TrainingJobConfig,
        training_datasets: list[DatasetModelSQL],
        evaluating_datasets: list[DatasetModelSQL],
        description: str | None = None,
    ):
        """Init a TrainingJob.

        Args:
            config: Configuration of the TrainingJob.
            training_datasets: Dataset on which the training should be executed.
            evaluating_datasets: Datasets on which the model should be evaluated on.
            description: Description of the model.
        """
        BaseJob.__init__(self, config=config.base)
        self.configuration: TrainingJobConfig = config
        self.training_datasets: list[DatasetModelSQL] = training_datasets
        self.evaluating_datasets: list[DatasetModelSQL] = evaluating_datasets
        self.creator: ConfigCreator = self.__build_config_creator()  # type: ignore[type-arg]
        self.description: str | None = description
        self.current_run_id: str = self.mlflow_adapter.create_run_id()

        self.s3_credentials: S3Credentials = S3Credentials.init_from_os_environment()

        logger.debug(
            "Init instance of TrainingJob with following configuration: %s",
            self.configuration,
        )

    def create_job_data(self) -> TrainingJobData:
        """Create all data necessary to run a training job.

        Necessary data depends on the specified model and includes model specific configs,
        training data locations (images + annotations) and the location of the initial training
        checkpoint.

        Returns:
            Necessary data for a training job.
        """
        bucket_name = self.get_s3_bucket_name()

        image_locations, annotation_locations = self.__create_dataset_locations(
            bucket_name=bucket_name
        )
        # pylint: disable=no-member
        mlflow_credentials = self.mlflow_adapter.credentials
        # pylint: enable=no-member
        mlflow_credentials.run_id = self.current_run_id

        s3_training_prefixes = [dataset.s3_prefix for dataset in self.training_datasets]
        s3_eval_prefixes = [dataset.s3_prefix for dataset in self.evaluating_datasets]

        if self.creator.configuration.model_config is None:
            model_config = self.creator.create_model_config(
                bucket_name=bucket_name,
                s3_prefixes=s3_training_prefixes,
                s3_eval_prefixes=s3_eval_prefixes,
            )
            model_config_dict = model_config.to_dict(retain_collection_types=True)
        else:
            model_config_dict = TupleEncoder.decode(self.creator.configuration.model_config)

        return TrainingJobData(
            image_locations=image_locations,
            annotation_locations=annotation_locations,
            checkpoint_location=self.__create_initial_model_checkpoint_location(
                bucket_name=bucket_name
            ),
            model_type_name=self.__get_model_type_name(),
            model_config=model_config_dict,
            eval_config={
                "annotation_handler_config": self.creator.create_annotation_handler_config(
                    bucket_name=bucket_name, s3_prefixes=s3_eval_prefixes
                ).to_dict(),
                "iou_thresholds": [0.5],
            },
            mlflow_credentials=mlflow_credentials,
        )

    def run(self) -> str:
        """Run a training job."""
        data = self.create_job_data()

        if self.current_run_id is not None:
            self.mlflow_adapter.register_mlflow_model(
                run_id=self.current_run_id,
                unique_name=self.creator.configuration.parameters.model_version,
            )
        else:
            raise RuntimeError("No Mlflow run ID set for training job!")

        training_url = (
            f"{self.configuration.base.server_endpoint}/"
            f"{TrainingRunServerEndpoint.TRAINING_ENDPOINT}/"
        )

        # Remove sensitive data from the log
        log_data: TrainingJobData = copy.deepcopy(data)
        log_data = self.mask_sensitive_request_data(log_data, "***")

        logger.info(
            "Post training on '%s' with data: \n%s"
            "===============================================",
            training_url,
            log_data.__dict__,
        )

        if self.current_run_id is not None:
            self.__log_training_run_data(
                params={
                    str(TrainingStatus.NEW.name): datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    "batch_size": self.configuration.creator.parameters.batch_size,
                    "datasets": [dataset.name for dataset in self.training_datasets],
                    "evaluation_datasets": [dataset.name for dataset in self.evaluating_datasets],
                    "epochs": self.configuration.creator.parameters.epochs,
                    "description": self.description,
                    "model_type_name": self.__get_model_type_name(),
                    "model": self.configuration.creator.parameters.model,
                }
            )
        else:
            raise RuntimeError("No Mlflow run ID set for training job!")

        request = requests.post(
            url=training_url,
            data=TupleEncoder().encode(data.to_training_server_data()),
            headers={"Content-Type": "application/json"},
        )

        if request.status_code != 201:
            raise RuntimeError(
                "Training run failed! Check the connection to the training"
                f"server and try again! status-code='{request.status_code}'"
            )
        job_id = str(request.json()["job_id"])
        logger.info("Successfully started training job with ID: %s", job_id)

        return job_id

    @staticmethod
    def mask_sensitive_request_data(
        data: TrainingJobData, credential_replacement: str
    ) -> TrainingJobData:
        """Replace sensitive data in order to securely log it.

        Args:
            data: The data that contains sensitive information.
            credential_replacement: The string with which the sensitive data is to be replaced.

        Returns:
            Masked data.
        """
        if data.image_locations:
            for location in data.image_locations:
                if location.credentials:
                    location.credentials.s3_artifact_access_id = credential_replacement
                    location.credentials.s3_artifact_access_key = credential_replacement

        if data.annotation_locations:
            for location in data.annotation_locations:
                if location.credentials:
                    location.credentials.s3_artifact_access_id = credential_replacement
                    location.credentials.s3_artifact_access_key = credential_replacement

        if data.checkpoint_location and data.checkpoint_location.credentials:
            data.checkpoint_location.credentials.s3_artifact_access_id = credential_replacement
            data.checkpoint_location.credentials.s3_artifact_access_key = credential_replacement

        if data.mlflow_credentials:
            data.mlflow_credentials.s3_artifact_access_id = credential_replacement
            data.mlflow_credentials.s3_artifact_access_key = credential_replacement

        return data

    def __build_config_creator(
        self,
    ) -> MMDetectionConfigCreator | MMOCRConfigCreator | MMPretrainConfigCreator | YOLOXConfigCreator:
        """Init a ConfigCreator object.

        Depending on the specified model an associated ConfigCreator is initialized
        (e.g. a YOLOXConfigCreator if a yolox model is specified).

        Returns:
            The initialized ConfigCreator.

        Raises:
            ValueError: If the specified model is not supported.
        """
        baseline_model = self.configuration.creator.parameters.model
        baseline_model_type = self.__get_model_type_name()

        model_type_config_creator_mapping = {
            ImplementedConfigCreators.YOLOX: YOLOXConfigCreator,
            ImplementedConfigCreators.MMDETECTION: MMDetectionConfigCreator,
            ImplementedConfigCreators.MMROTATE: MMDetectionConfigCreator,
            ImplementedConfigCreators.MMPRETRAIN: MMPretrainConfigCreator,
            ImplementedConfigCreators.MMOCR: MMOCRConfigCreator,
            ImplementedConfigCreators.MMSEGMENTATION: MMDetectionConfigCreator,
        }

        config_creator_callable = model_type_config_creator_mapping.get(baseline_model_type)
        if config_creator_callable is None:
            raise ValueError(
                f"No configuration creator could be found for the specified model_type "
                f"'{baseline_model_type}'. Supported model_types are "
                f"{model_type_config_creator_mapping.keys()}"
            )

        if baseline_model not in ImplementedModels().all:
            raise ValueError(
                f"The specified model '{baseline_model}' is not supported. Supported models are "
                f"{ImplementedModels().all}"
            )

        return config_creator_callable(configuration=self.configuration.creator)  # type: ignore[return-value]

    def __create_dataset_locations(
        self, bucket_name: str
    ) -> tuple[list[S3DataLocation], list[S3DataLocation]]:
        """Create data locations.

        The created data locations contain the information about the annotations and images used
        for the training. Each S3DataLocation stores the information about a single image or a
        single annotation file

        Args:
            bucket_name: S3 bucket which stores the annotations and images.

        Returns:
            Two lists of S3DataLocation objects. One List for annotations and one for images.
        """
        logger.info(
            f"\n\n====================================\n"
            f"Read data from bucket='{bucket_name}' "
            f"and s3_prefixes='{(dataset.s3_prefix for dataset in self.training_datasets + self.evaluating_datasets)}'"
            f"\n\n====================================\n"
        )

        annotation_locations = []
        image_locations = []

        for dataset in self.training_datasets + self.evaluating_datasets:
            image_locations.append(
                S3DataLocation(
                    uri=f"{self.s3_credentials.s3_endpoint_url}/"
                    f"{bucket_name}/"
                    f"{dataset.s3_prefix}/"
                    f"data",
                    location_id=StringReplacements.MEDIA_DIR,
                    credentials=self.s3_credentials,
                )
            )

            annotation_locations.append(
                S3DataLocation(
                    uri=f"{self.s3_credentials.s3_endpoint_url}/"
                    f"{bucket_name}/"
                    f"{dataset.s3_prefix}/"
                    f"tasks",
                    location_id=StringReplacements.MEDIA_DIR,
                    credentials=self.s3_credentials,
                )
            )

        return image_locations, annotation_locations

    def __log_training_run_data(self, params: dict[str, Any]) -> None:
        """Log the training data in the associated run.

        Args:
            params: Training parameters to log.

        """
        self.mlflow_adapter.log_params(run_id=self.current_run_id, params=params)

    def __create_initial_model_checkpoint_location(self, bucket_name: str) -> S3DataLocation:
        """Create data location of the initial model checkpoint.

        Args:
            bucket_name: S3 bucket which stores the initial model checkpoint.

        Returns:
            Single S3DataLocation object with information about the initial checkpoint.
        """
        return S3DataLocation(
            uri=f"{self.s3_credentials.s3_endpoint_url}/{bucket_name}/"
            f"{DataRootDirs.INITIAL_CHECKPOINT_DIR}/"
            f"{self.configuration.creator.parameters.model}.pth",
            location_id=StringReplacements.BASELINE_MODEL_DIR,
            credentials=self.s3_credentials,
        )

    def __get_model_type_name(self) -> str:
        """Get the model type from the configuration.

        Gets the model type name (resp. the framework) of the model to train e.g.
        MMDetection for YoloV3 and HTC and Yolox for Yolox_nano, etc.

        The naming convention is "MODELTYPE__*", as can be seen in
        angular-frontend/src/app/features/pages/04-model-management-page/
        train-new-model-dialog/train-new-model-dialog.component.ts

        Returns:
            The model type name.
        """
        return self.configuration.creator.parameters.model.split("__")[0]
