# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the configuration of the TrainingJob."""

import related
from attr import define
from config_builder import BaseConfigClass

from gts_backend.services.configuration_creator.base.configuration import (
    ConfigCreatorConfig,
)
from gts_backend.services.jobs.base.configuration import BaseJobConfig


@define
class TrainingJobConfig(BaseConfigClass):
    """Defines all parameters provided by user."""

    __related_strict__ = True

    creator: ConfigCreatorConfig = related.ChildField(cls=ConfigCreatorConfig, required=True)

    base: BaseJobConfig = related.ChildField(
        cls=BaseJobConfig,
        required=True,
    )
