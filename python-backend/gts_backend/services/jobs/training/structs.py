# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining common structures used by the TrainingJob."""

from dataclasses import dataclass
from typing import Any

from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from gts_backend.services.jobs.base.structs import BaseJobData, BaseJobRequestKeys


class ImplementedConfigCreators:
    """Define the implemented ConfigCreators."""

    YOLOX = "yolox"
    MMDETECTION = "mmdetection_object_detection"
    MMSEGMENTATION = "mmdetection_segmentation"
    MMROTATE = "mmrotate"
    MMOCR = "mmocr_text_recognition"
    MMPRETRAIN = "mmpretrain"


@dataclass
class TrainingJobData(BaseJobData):
    """Define all data necessary to start a training job."""

    annotation_locations: list[S3DataLocation]

    eval_config: dict[str, Any]

    mlflow_credentials: MlflowCredentials

    def __getitem__(self, item: Any) -> Any:
        """Get named attribute from object."""
        return getattr(self, item)

    def to_training_server_data(self) -> dict[str, Any]:
        """Convert dataclass object into a format which is understandable by the training server.

        Returns:
            Dictionary as parameters for the training job REST request
        """
        data = {
            str(TrainingJobRequestKeys.IMAGE_LOCATIONS): [
                {"data": location.to_dict(), "adapter_type": StorageAdapters.S3}
                for location in self.image_locations
            ],
            str(TrainingJobRequestKeys.ANNOTATION_LOCATIONS): [
                {"data": location.to_dict(), "adapter_type": StorageAdapters.S3}
                for location in self.annotation_locations
            ],
            # pylint: disable=line-too-long
            str(TrainingJobRequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION): {
                "data": self.checkpoint_location.to_dict(),
                # pylint: enable=line-too-long
                "adapter_type": StorageAdapters.S3,
            },
            str(TrainingJobRequestKeys.MODEL_TYPE_NAME): self.model_type_name,
            # pylint: disable=no-member
            str(TrainingJobRequestKeys.MODEL_CONFIG_DATA): self.model_config,
            # pylint: enable=no-member
            str(TrainingJobRequestKeys.EVAL_CONFIG): self.eval_config,
            str(TrainingJobRequestKeys.MLFLOW_CREDENTIALS): {
                "data": self.mlflow_credentials.to_dict(),
                "adapter_type": StorageAdapters.MLFLOW,
            },
        }
        return data


class TrainingJobRequestKeys(BaseJobRequestKeys):
    """Define all needed keys to make a POST request to start a training on a training server."""

    ANNOTATION_LOCATIONS = "ANNOTATION_LOCATIONS"
    EVAL_CONFIG = "EVAL_CONFIG"
    MLFLOW_CREDENTIALS = "MLFLOW_CREDENTIALS"


class StorageAdapters:
    """Define all supported storage adapters."""

    S3 = "S3"
    MLFLOW = "MLFLOW"
    CONFIG = "CONFIG"


# TODO: Directory structure of the stored initial checkpoints?
#       -> First draft: All checkpoints stored in bucket / path
#                       guided-training-service/initial-checkpoints/xxx.pth


class DataRootDirs:
    """Define values for common data directories."""

    INITIAL_CHECKPOINT_DIR = "initial-checkpoints"
    LABELSTUDIO_DATA_DIR = "data"
    LABELSTUDIO_ANNOTATION_DIR = "task"


class TrainingRunServerEndpoint:
    """Define the endpoint of the training server to start a training job."""

    TRAINING_ENDPOINT = "training"
