# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the AutoAnnotationJob.

The AutoAnnotationJob runs auto annotation jobs.
"""
import ast
import json
from datetime import datetime
from logging import getLogger
from typing import Any

import cv2
import numpy as np
from mlcvzoo_util.adapters.s3.data_location import S3Tuple

from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.model.structs import S3ProjectFolders
from gts_backend.services.jobs.prediction.configuration import PredictionJobConfig
from gts_backend.services.jobs.prediction.job import PredictionJob
from gts_backend.services.labeling.service import LabelingService

logger = getLogger(__file__)


class AutoAnnotationJob(PredictionJob):
    """Run an auto annotation job.

    The AutoAnnotationJob is a subclass of the PredictionJob. It also triggers a prediction
    job on a specified prediction server. It diverges from the PredictionJob in the way the
    resulting predictions are processed.
    """

    COCO_ANNOTATION_FILE_DEFAULT_NAME = "coco_json.json"
    AUTO_ANNOTATION_CHECK_DURATION = 1800  # 30 minutes

    class __CocoAnnotation:
        """Define a default COCO annotation."""

        def __init__(
            self,
            categories: list[dict[str, Any]],
            images: list[dict[str, int | str]],
            annotations: list[dict[str, Any]],
        ):
            """Init a CocoAnnotation object.

            The attributes are based on the standardized COCO annotation format.

            Args:
                categories: The categories (i.e. classes) of a dataset.
                images: The images of a dataset.
                annotations: The annotations of a dataset.
            """
            self.info = {
                "year": datetime.now().year,
                "version": "1.0",
                "description": "",
                "contributor": "Label Studio",
                "url": "",
            }
            self.categories = categories
            self.images = images
            self.annotations = annotations

    def __init__(
        self,
        config: PredictionJobConfig,
        datasets: list[DatasetModelSQL],
        labeling_service: LabelingService,
    ):
        """Init an AutoAnnotationJob.

        Args:
            config: Config of the job.
            datasets: Datasets on which the job is executed.
            labeling_service: LabelingService instance.
        """
        PredictionJob.__init__(self, config=config, datasets=datasets)
        self.labeling_service = labeling_service

    def run(self) -> int:  # type: ignore[override]
        """Run an auto annotation job.

        Use a registered model to predict an un-annotated dataset. Process the predictions,
        generate an annotation file and use it to annotate the dataset.

        Returns:
            The number of auto-generated annotations.
        """
        job_id = self._start_prediction_job()

        predictions = self._check_prediction_task(
            job_id=job_id, check_duration=self.AUTO_ANNOTATION_CHECK_DURATION
        )
        self.__create_annotation_file(predictions=predictions)

        return self.labeling_service.import_ls_annotation(
            project_id=self.datasets[0].ls_project_id, s3_prefix=self.datasets[0].s3_prefix
        )

    def __create_annotation_file(self, predictions: dict[str, Any]) -> None:
        """Create an annotation file with the given predictions in the COCO annotation format.

        The created annotation file is uploaded under the same prefix as its associated dataset.

        Args:
            predictions: Predictions to create the annotation file with.
        """
        classes = ast.literal_eval(self.datasets[0].labels)  # in LabelStudio Labels == Classes
        bucket = self.get_s3_bucket_name()
        images_prefix = f"{self.datasets[0].s3_prefix}/{S3ProjectFolders.DATA.value}"
        annotation_prefix = (
            f"{self.datasets[0].s3_prefix}/"
            f"{S3ProjectFolders.PREANNOTATION.value}/"
            f"{self.COCO_ANNOTATION_FILE_DEFAULT_NAME}"
        )

        inverted_categories = self.__get_inverted_coco_categories(classes=classes)
        # TODO: Use .list_files(...) of s3_adapter and not private method
        images, image_ids = self.__get_coco_image_values(
            images=self.s3_adapter.list_files(
                bucket=bucket,
                prefix=images_prefix,
                filter_empty_files=True,
            ),
        )

        coco_annotation = self.__CocoAnnotation(
            categories=self.__get_coco_categories_values(classes=classes),
            images=images,
            annotations=self.__get_coco_annotation_values(
                predictions=predictions, image_ids=image_ids, category_ids=inverted_categories
            ),
        )

        self.s3_adapter.put_file(
            file=json.dumps(coco_annotation.__dict__), bucket=bucket, prefix=annotation_prefix
        )

    @staticmethod
    def __get_coco_categories_values(classes: list[str]) -> list[dict[str, Any]]:
        """Get the categories (i.e. classes) of a COCO annotation file.

        Args:
            classes: The classes of a dataset.

        Returns:
            The categories.
        """
        return [{"id": index, "name": class_} for index, class_ in enumerate(classes)]

    @staticmethod
    def __get_inverted_coco_categories(classes: list[str]) -> dict[str, Any]:
        """Get the inverted categories (i.e. classes) of a COCO annotation file.

        In contrast to the non-inverted categories, the class is the key and the ID is the value.

        Args:
            classes: The classes of a dataset.

        Returns:
            The categories.
        """
        return {class_: index for index, class_ in enumerate(classes)}

    def __get_coco_image_values(
        self, images: list[S3Tuple]
    ) -> tuple[list[dict[str, int | str]], dict[str, int]]:
        """Get the images of a COCO annotation file and dict with the mapping of the images and ids.

        Args:
            images: The images of a dataset. Each image is represented by a tuple of its bucket
            and its unique prefix.

        Returns:
            The images and a dict with the mapping of images and ids.
        """
        coco_images: list[dict[str, int | str]] = []
        image_ids: dict[str, int] = {}
        for index, image in enumerate(images):
            # necessary to get the shape of the image
            file = self.s3_adapter.get_file_as_bytes(bucket=image.bucket, prefix=str(image.prefix))
            decoded_file = cv2.imdecode(np.asarray(bytearray(file), dtype=np.uint8), -1)
            height = decoded_file.shape[0]
            width = decoded_file.shape[1]

            file_name = image.prefix.split("/")[-1]
            image_ids[file_name] = index
            coco_images.append(
                {"width": width, "height": height, "id": index, "file_name": f"image/{file_name}"}
            )

        return coco_images, image_ids

    def __get_coco_annotation_values(
        self, predictions: dict[str, Any], image_ids: dict[str, int], category_ids: dict[str, Any]
    ) -> list[dict[str, Any]]:
        """Get the annotations of a COCO annotation file.

        Args:
            predictions: Predictions for each image.
            image_ids: IDs of each image in the COCO file.
            category_ids: IDs of each category (i.e. class).

        Returns:
            The annotations.
        """
        annotations: list[dict[str, str | int | list[float]]] = []
        for image, prediction in predictions.items():
            bucket, key = image.split("/", 1)
            file_name = key.split("/")[-1]

            for single_prediction in prediction:
                if single_prediction.get("type") == "Segmentation":
                    annotation = self.__process_segmentation(
                        input_segmentation=single_prediction,
                        annotation_id=len(annotations),
                        file_name=file_name,
                        image_ids=image_ids,
                        category_ids=category_ids,
                    )
                    if annotation:
                        annotations.append(annotation)

                elif single_prediction.get("type") == "BoundingBox":
                    annotation = self.__process_prediction(
                        input_bbox=single_prediction,
                        annotation_id=len(annotations),
                        file_name=file_name,
                        image_ids=image_ids,
                        category_ids=category_ids,
                    )
                    if annotation:
                        annotations.append(annotation)

        return annotations

    @staticmethod
    def __process_segmentation(
        input_segmentation: dict[str, Any],
        annotation_id: int,
        file_name: str,
        image_ids: dict[str, int],
        category_ids: dict[str, Any],
    ) -> dict[str, Any] | None:
        """Process an input segmentation and convert it into a more informative annotation.

        Args:
            input_segmentation: The input segmentation.
            annotation_id: The annotation ID.
            file_name: The file name of the image.
            image_ids: The mapping of the image IDs.
            category_ids: The mapping of the category IDs.

        Returns:
            The annotation or None if the annotation is skipped.
        """
        try:
            segmentation: list[float] = []
            for point in input_segmentation["data"]["polygon"]:
                segmentation.extend([point[0], point[1]])

            annotation = {
                "id": annotation_id,
                "image_id": image_ids[file_name],
                "category_id": category_ids[
                    input_segmentation["data"]["class_identifier"]["class_name"]
                ],
                "segmentation": [segmentation],
                "bbox": cv2.boundingRect(np.array(input_segmentation["data"]["polygon"])),
                "ignore": 0,
                "iscrowd": 0,
            }
            return annotation

        except Exception as error:
            logger.error("%s, annotation will be skipped", error)
            return None

    @staticmethod
    def __process_prediction(
        input_bbox: dict[str, Any],
        annotation_id: int,
        file_name: str,
        image_ids: dict[str, int],
        category_ids: dict[str, Any],
    ) -> dict[str, Any] | None:
        """Process an input prediction and convert it into a more informative annotation.

        Args:
            input_bbox: The input prediction given as a bounding box.
            annotation_id: The annotation ID.
            file_name: The file name of the image.
            image_ids: The mapping of the image IDs.
            category_ids: The mapping of the category IDs.

        Returns:
            The annotation or None if the annotation is skipped.
        """
        xmin = float(input_bbox["data"]["box"]["xmin"])
        xmax = float(input_bbox["data"]["box"]["xmax"])
        ymin = float(input_bbox["data"]["box"]["ymin"])
        ymax = float(input_bbox["data"]["box"]["ymax"])
        box_width = xmax - xmin
        box_height = ymax - ymin

        try:
            annotation = {
                "id": annotation_id,
                "image_id": image_ids[file_name],
                "category_id": category_ids[input_bbox["data"]["class_identifier"]["class_name"]],
                "segmentation": [],
                "bbox": [
                    xmin,
                    ymin,
                    box_width,
                    box_height,
                ],
                "ignore": 0,
                "iscrowd": 0,
                "area": box_height * box_width,
            }
            return annotation

        except Exception as error:
            logger.error("%s, annotation will be skipped", error)
            return None
