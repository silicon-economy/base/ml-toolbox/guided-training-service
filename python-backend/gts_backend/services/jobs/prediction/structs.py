# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines common structs for the PredictionJob."""

from dataclasses import dataclass
from typing import Any, Optional

from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from gts_backend.services.jobs.base.structs import BaseJobData, BaseJobRequestKeys
from gts_backend.services.jobs.training.structs import StorageAdapters


@dataclass
class PredictionJobData(BaseJobData):
    """Define all data necessary to run a prediction job."""

    second_checkpoint_location: Optional[S3DataLocation] = None
    second_model_type_name: Optional[str] = None
    second_model_config: Optional[dict[str, Any]] = None

    def __getitem__(self, item: Any) -> Any:
        """Get named attribute from object."""
        return getattr(self, item)

    def to_prediction_server_data(self) -> dict[str, Any]:
        """Convert dataclass object into a format which is understandable by the prediction server.

        Returns:
            Dictionary as parameters for the prediction job REST request.
        """
        if (
            self.second_checkpoint_location is not None
            and self.second_model_type_name is not None
            and self.second_model_config is not None
        ):
            data = {
                str(BaseJobRequestKeys.IMAGE_LOCATIONS): [
                    {"data": location.to_dict(), "adapter_type": StorageAdapters.S3}
                    for location in self.image_locations
                ],
                str(BaseJobRequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION): {
                    "data": self.checkpoint_location.to_dict(),
                    "adapter_type": StorageAdapters.S3,
                },
                str(BaseJobRequestKeys.MODEL_TYPE_NAME): self.model_type_name,
                str(BaseJobRequestKeys.MODEL_CONFIG_DATA): self.model_config,
                str(BaseJobRequestKeys.SECOND_MODEL_CHECKPOINT_LOCATION): {
                    "data": self.second_checkpoint_location.to_dict(),
                    "adapter_type": StorageAdapters.S3,
                },
                str(BaseJobRequestKeys.SECOND_MODEL_TYPE_NAME): self.second_model_type_name,
                str(BaseJobRequestKeys.SECOND_MODEL_CONFIG_DATA): self.second_model_config,
            }
        else:
            data = {
                str(BaseJobRequestKeys.IMAGE_LOCATIONS): [
                    {"data": location.to_dict(), "adapter_type": StorageAdapters.S3}
                    for location in self.image_locations
                ],
                str(BaseJobRequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION): {
                    "data": self.checkpoint_location.to_dict(),
                    "adapter_type": StorageAdapters.S3,
                },
                str(BaseJobRequestKeys.MODEL_TYPE_NAME): self.model_type_name,
                str(BaseJobRequestKeys.MODEL_CONFIG_DATA): self.model_config,
            }
        return data


class PredictionJobPredictionTaskCheckValues:
    """Define values to frequently check the state of a prediction job."""

    MAXIMUM_CHECK_DURATION = 120
    CHECK_INTERVAL = 5


class PredictionRunServerEndpoint:
    """Define the endpoint of the prediction server to start a prediction job."""

    PREDICTION_ENDPOINT = "prediction"
