# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the configuration of the PredictionJob."""


from typing import Optional

import related
from attr import define
from config_builder import BaseConfigClass

from gts_backend.services.jobs.base.configuration import BaseJobConfig


@define
class PredictionJobBaseConfig(BaseConfigClass):
    """Define unique parameters of the PredictionJob."""

    __related_strict__ = True

    model_name: str = related.StringField()
    second_model_name: Optional[str] = related.ChildField(cls=Optional[str], default=None)


@define
class PredictionJobConfig(BaseConfigClass):
    """Define all parameters for a prediction job.

    Consists of the base parameters which are common to all jobs and PredictionJob-specific
    parameters.
    """

    __related_strict__ = True

    base: BaseJobConfig = related.ChildField(cls=BaseJobConfig, required=True)

    job: PredictionJobBaseConfig = related.ChildField(
        cls=PredictionJobBaseConfig,
        required=True,
    )
