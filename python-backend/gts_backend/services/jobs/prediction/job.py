# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the PredictionJob.

The PredictionJob runs prediction jobs i.e. a model predicting a dataset.
"""
import copy
import os
import time
from logging import getLogger
from typing import Any

import cv2
import numpy as np
import requests
from config_builder.json_encoding import TupleEncoder
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.classification import Classification
from mlcvzoo_base.api.data.data_registry import DataRegistry
from mlcvzoo_base.api.data.geometric_classifiction import GeometricClassification
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.utils.draw_utils import draw_on_image, generate_detector_colors
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation, S3Tuple

from gts_backend.model.dataset import DatasetModelSQL
from gts_backend.services.configuration_creator.structs import StringReplacements
from gts_backend.services.jobs.base.job import BaseJob
from gts_backend.services.jobs.base.structs import BaseJobRequestKeys
from gts_backend.services.jobs.prediction.configuration import PredictionJobConfig
from gts_backend.services.jobs.prediction.structs import (
    PredictionJobData,
    PredictionJobPredictionTaskCheckValues,
    PredictionRunServerEndpoint,
)

logger = getLogger(__file__)


class PredictionJob(BaseJob):
    """Run prediction jobs."""

    def __init__(
        self,
        config: PredictionJobConfig,
        datasets: list[DatasetModelSQL],
        dataset_subset: list[str] | None = None,
    ):
        """Init a PredictionJob.

        Args:
            config: Config of the PredictionJob.
            datasets: Datasets on which the run is executed.
            dataset_subset: Subset of a dataset i.e. a list of image uris part of a single dataset.
        """
        BaseJob.__init__(self, config=config.base)
        self.datasets = datasets
        self.configuration = config
        self.dataset_subset = dataset_subset

    def run(self) -> list[str]:
        """Start a prediction job.

        Starts the prediction job on the prediction server, processes the results and saves the
        predicted images to S3. A predicted image contains the drawn predictions of the model.

        Returns:
            List of presigned URLs of the predicted images.
        """
        job_id = self._start_prediction_job()

        predictions = self._check_prediction_task(job_id=job_id)

        logger.info(f"Got prediction results: {predictions}")

        predicted_images = self.__draw_predictions_on_images(predictions=predictions)

        return self.s3_adapter.generate_presigned_urls(files=predicted_images)

    def _start_prediction_job(self) -> str:
        """Start a prediction job on the specified prediction server.

        Returns:
            The ID of the prediction job.
        """
        data = self.create_job_data()
        prediction_url = (
            f"{self.configuration.base.server_endpoint}/"
            f"{PredictionRunServerEndpoint.PREDICTION_ENDPOINT}/"
        )

        log_data: PredictionJobData = copy.deepcopy(data)
        log_data = self.mask_sensitive_request_data(data=log_data, credential_replacement="***")
        logger.info(
            "Run prediction on '%s' with data: \n%s",
            prediction_url,
            log_data.to_prediction_server_data(),
        )

        data_encoded = TupleEncoder().encode(data.to_prediction_server_data())
        response = requests.post(
            url=prediction_url,
            data=data_encoded,
            headers={"Content-Type": "application/json"},
        )

        if response.status_code != 201:
            raise RuntimeError(
                "Start of prediction run failed! Check the connection to the prediction "
                f"server and try again! status-code='{response.status_code}'"
            )

        job_id: str = response.json()["job_id"]
        logger.info("Successfully started prediction job with ID: %s", job_id)

        return job_id

    def __parse_model_information(
        self, model_name: str
    ) -> tuple[str, S3DataLocation, dict[str, Any]]:
        """Parse the relevant information for a given model.

        Args:
            model_name: The name of the model.

        Returns:
            The relevant model information as a tuple containing:
            (model_type_name, checkpoint_location, model_configuration)
        """
        run = self.mlflow_adapter.get_run_of_registered_model(model_name=model_name)
        model_type_name = run.data.params[BaseJobRequestKeys.MODEL_TYPE_NAME.lower()]

        checkpoint_location = self.mlflow_adapter.get_model_checkpoint_location(
            run=run, location_id=StringReplacements.MEDIA_DIR
        )
        model_configuration = self.mlflow_adapter.get_model_config_dict_from_run(run=run)

        return model_type_name, checkpoint_location, model_configuration

    def create_job_data(self) -> PredictionJobData:
        """Create all data necessary for the prediction run.

        The created data consists of the locations of the datasets to predict, the location of the
        checkpoint used for prediction, the type and the configuration of the used model.

        Returns:
            PredictionRunData containing the dataset locations, the checkpoint location, the model
            type and the configuration.
        """
        image_locations = self.__create_dataset_locations(bucket_name=self.get_s3_bucket_name())

        model_type_name, checkpoint_location, model_configuration = self.__parse_model_information(
            model_name=self.configuration.job.model_name,
        )

        if second_model := self.configuration.job.second_model_name:
            (
                second_model_type_name,
                second_checkpoint_location,
                second_model_configuration,
            ) = self.__parse_model_information(
                model_name=second_model,
            )

            return PredictionJobData(
                image_locations=image_locations,
                checkpoint_location=checkpoint_location,
                model_type_name=model_type_name,
                model_config=model_configuration,
                second_checkpoint_location=second_checkpoint_location,
                second_model_type_name=second_model_type_name,
                second_model_config=second_model_configuration,
            )

        return PredictionJobData(
            image_locations=image_locations,
            checkpoint_location=checkpoint_location,
            model_type_name=model_type_name,
            model_config=model_configuration,
        )

    def __create_dataset_locations(self, bucket_name: str) -> list[S3DataLocation]:
        """Create data locations for the prediction task.

        The created locations contain information about the images to predict. Each location
        either represents one whole dataset or if a dataset subset is provided a single image from a
        dataset.

        Args:
            bucket_name: S3 bucket which stores the images.

        Returns:
            List of data locations.
        """
        image_locations = []

        if self.dataset_subset is not None:
            # element in dataset subset is the uri of an image
            for image_uri in self.dataset_subset:
                image_locations.append(
                    S3DataLocation(
                        uri=image_uri,
                        location_id=StringReplacements.MEDIA_DIR,
                        credentials=self.s3_adapter.credentials,
                    )
                )
        else:
            for dataset in self.datasets:
                image_locations.append(
                    S3DataLocation(
                        uri=f"{self.s3_adapter.credentials.s3_endpoint_url}/"
                        f"{bucket_name}/"
                        f"{dataset.s3_prefix}/"
                        f"data",
                        location_id=StringReplacements.MEDIA_DIR,
                        credentials=self.s3_adapter.credentials,
                    )
                )
        return image_locations

    def _check_prediction_task(
        self,
        job_id: str,
        check_duration: int = PredictionJobPredictionTaskCheckValues.MAXIMUM_CHECK_DURATION,
        check_interval: int = PredictionJobPredictionTaskCheckValues.CHECK_INTERVAL,
    ) -> dict[str, Any]:
        """Frequently check if the prediction task is finished.

        Args:
            job_id: Task to check.

        Returns:
            Dictionary containing the name of the images and the predictions for each image.

        Raises:
            RuntimeError if the prediction takes to long.
        """
        url = f"{self.configuration.base.server_endpoint}/jobs/{job_id}"

        for i in range(
            0,
            check_duration,
            check_interval,
        ):
            response = requests.get(url=url).json()
            job_status = response["job_status"].lower()

            if job_status == "finished":
                return response["job_results"]  # type: ignore[no-any-return]
            if job_status == "crashed":
                raise RuntimeError(
                    "Prediction run crashed! Check the logs of the prediction server "
                    "for detailed information!"
                )

            logger.info("Waiting for prediction results...")
            time.sleep(check_interval)

        raise RuntimeError(
            f"Maximum time for prediction run exceeded! You can check the run"
            f"on the prediction server manually with ID: '{job_id}"
        )

    def __draw_predictions_on_images(self, predictions: dict[str, Any]) -> list[S3Tuple]:
        """Draw predictions to the associated images and save the drawn images to S3.

        Args:
            predictions: Images and the associated predictions.

        Returns:
            List of S3 paths of drawn images. Each drawn image has a bucket and a S3 prefix.
        """
        bbox_colors = generate_detector_colors(num_classes=1000)
        drawn_images: list[S3Tuple] = []

        if "crashed" in predictions:
            logger.warning(
                "Can not draw predictions, "
                "because the prediction job crashed with message: %s" % predictions.get("message")
            )
            return drawn_images

        data_registry = DataRegistry()

        for image_path, prediction_entry in predictions.items():
            # split up image path into relevant parts
            bucket, prefix = image_path.split(sep="/", maxsplit=1)
            image_extension = os.path.splitext(image_path)[1]

            # get the image from s3 as ndarray
            image_encoded = self.s3_adapter.get_file_as_bytes(bucket=bucket, prefix=prefix)
            image_decoded = cv2.imdecode(np.asarray(bytearray(image_encoded), dtype=np.uint8), -1)

            # draw the predicted bounding boxes into the image
            classification_predictions: list[Classification] = []
            geometric_predictions: list[GeometricClassification] = []

            for encoded_prediction in prediction_entry:
                prediction = data_registry.decode(data=encoded_prediction)
                if isinstance(prediction, BoundingBox):
                    geometric_predictions.append(prediction)
                elif isinstance(prediction, Segmentation):
                    geometric_predictions.append(prediction)
                # Segmentation and BoundingBox is a subclass of Classification,
                # therefore check for Classification last
                elif isinstance(prediction, Classification):
                    classification_predictions.append(prediction)

            drawn_image = image_decoded
            if predictions is not None:
                drawn_image = draw_on_image(
                    frame=image_decoded,
                    rgb_colors=bbox_colors,
                    predictions=geometric_predictions,
                    classifications=classification_predictions,
                    draw_caption=True,
                    include_content_in_caption=True,
                )

            # save the drawn images to S3
            drawn_image_encoded = np.array(cv2.imencode(ext=image_extension, img=drawn_image)[1])
            new_prefix = self.__get_new_image_prefix(prefix=prefix)
            self.s3_adapter.put_file(
                file=drawn_image_encoded.tobytes(),
                bucket=bucket,
                prefix=new_prefix,
            )
            drawn_images.append(S3Tuple(bucket, new_prefix))

        return drawn_images

    def __get_new_image_prefix(self, prefix: str) -> str:
        """Get a new prefix based on the internal logic used to save predicted images.

        Predicted images are saved under: LABEL_STUDIO_PROJECT_ID / predicted / MODEL_NAME / IMAGE_NAME

        Args:
            prefix: Prefix of the original image.

        Returns:
            The new prefix.
        """
        ls_project_id, s3_key = prefix.split("/", 1)
        s3_key = s3_key.replace("data/", "")
        return f"{ls_project_id}/predicted/{self.configuration.job.model_name}/{s3_key}"

    @staticmethod
    def mask_sensitive_request_data(
        data: PredictionJobData, credential_replacement: str
    ) -> PredictionJobData:
        """Replace sensitive prediction run data in order to securely log it.

        Args:
            data: The data that contains sensitive information.
            credential_replacement: The string with which the sensitive data is to be replaced.

        Returns:
            Masked data.
        """
        if data.image_locations:
            for location in data.image_locations:
                if location.credentials:
                    location.credentials.s3_artifact_access_id = credential_replacement
                    location.credentials.s3_artifact_access_key = credential_replacement
        if data.checkpoint_location and data.checkpoint_location.credentials:
            data.checkpoint_location.credentials.s3_artifact_access_id = credential_replacement
            data.checkpoint_location.credentials.s3_artifact_access_key = credential_replacement
        if data.second_checkpoint_location and data.second_checkpoint_location.credentials:
            data.second_checkpoint_location.credentials.s3_artifact_access_id = (
                credential_replacement
            )
            data.second_checkpoint_location.credentials.s3_artifact_access_key = (
                credential_replacement
            )
        return data
