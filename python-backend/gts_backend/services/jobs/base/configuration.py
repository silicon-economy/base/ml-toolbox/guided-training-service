# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the configuration of the BaseJob."""


import related
from attr import define
from config_builder import BaseConfigClass


@define
class BaseJobConfig(BaseConfigClass):
    """Define basic parameters necessary for every Job."""

    __related_strict__ = True

    server_endpoint: str = related.StringField()
