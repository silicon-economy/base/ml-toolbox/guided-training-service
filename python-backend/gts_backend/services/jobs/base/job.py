# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the BaseJob.

The BaseJob is an abstract class to inherit from for different types of jobs.
"""
import os
from abc import ABC, abstractmethod
from typing import Any

from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.adapter import S3Adapter
from mlcvzoo_util.adapters.s3.credentials import S3Credentials

from gts_backend.services.jobs.base.structs import BaseJobData


class BaseJob(ABC):
    """Define an abstract base job to inherit different jobs from."""

    def __init__(
        self,
        config: Any,
    ):
        """Init a BaseJob.

        Args:
            config: Configuration of the BaseJob.
        """
        self.configuration = config
        self.mlflow_adapter = MlflowAdapter(
            credentials=MlflowCredentials.init_from_os_environment()
        )
        self.s3_adapter: S3Adapter = S3Adapter(
            credentials=S3Credentials.init_from_os_environment()
        )

    @staticmethod
    def get_s3_bucket_name() -> str:
        """Get the name of the S3 bucket from the environment variables.

        Returns:
            Name of the s3 bucket which stores the dataset used for training.
        """
        s3_bucket_env_name = "S3_BUCKET"

        bucket_name: str | None = os.getenv(s3_bucket_env_name)

        if bucket_name is None:
            raise ValueError(
                f"Necessary environment variable '{s3_bucket_env_name}' "
                f"for accessing training data is not set!"
            )

        return bucket_name

    @abstractmethod
    def create_job_data(self) -> BaseJobData:
        """Create data necessary to start a job.

        Returns:
            Created data.
        """
        raise NotImplementedError("Must be implemented by sub-class: create_job_data(...)!")

    @abstractmethod
    def run(self) -> str | list[str]:
        """Run a job."""
        raise NotImplementedError("Must be implemented by sub-class: run(...)!")

    @staticmethod
    @abstractmethod
    def mask_sensitive_request_data(data: Any, credential_replacement: str) -> Any:
        """Replace sensitive data in order to securely log it.

        Args:
            data: The data that contains sensitive information.
            credential_replacement: The string with which the sensitive data is to be replaced.

        Returns:
            Masked data.
        """
        raise NotImplementedError(
            "Must be implemented by sub-class: mask_sensitive_request_data(...)!"
        )
