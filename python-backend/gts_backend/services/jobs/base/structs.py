# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining common structs for all Jobs."""

from dataclasses import dataclass
from typing import Any

from mlcvzoo_util.adapters.s3.data_location import S3DataLocation


@dataclass
class BaseJobData:
    """Define necessary data common for all jobs."""

    image_locations: list[S3DataLocation]

    checkpoint_location: S3DataLocation

    model_type_name: str

    model_config: dict[str, Any]


class BaseJobRequestKeys:
    """Define all keys needed to make a POST request to start any form of job."""

    IMAGE_LOCATIONS = "IMAGE_LOCATIONS"

    INITIAL_MODEL_CHECKPOINT_LOCATION = "INITIAL_MODEL_CHECKPOINT_LOCATION"

    MODEL_TYPE_NAME = "MODEL_TYPE_NAME"

    MODEL_CONFIG_DATA = "MODEL_CONFIG_DATA"

    SECOND_MODEL_CHECKPOINT_LOCATION = "SECOND_MODEL_CHECKPOINT_LOCATION"

    SECOND_MODEL_TYPE_NAME = "SECOND_MODEL_TYPE_NAME"

    SECOND_MODEL_CONFIG_DATA = "SECOND_MODEL_CONFIG_DATA"
