# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining mypy compatible StrEnum."""

from enum import Enum, _auto_null, auto
from typing import Any


class autostr(auto):
    """Defines an autostr.

    Attributes:
        value: Value of the autostr.
    """

    value: str = _auto_null

    def upper(self) -> str:
        """Returns value in uppercase.

        Returns:
            Value in uppercase.
        """
        return self.value.upper()

    def lower(self) -> str:
        """Returns value in lowercase.

        Returns:
            Value in lowercase.
        """
        return self.value.lower()


class StrEnum(str, Enum):
    """Defines a StrEnum."""

    @staticmethod
    def _generate_next_value_(name: str, start: int, count: int, last_values: list[Any]) -> str:
        """Generate the next value when not given.

        Attributes:
            name: the name of the member
            start: the initial start value or None
            count: the number of existing members
            last_values: the last value assigned or None

        Returns:
            The generated value
        """
        return name
