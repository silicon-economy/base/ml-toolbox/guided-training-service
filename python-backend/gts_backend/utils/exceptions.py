# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
"""Module defining common exceptions."""


class TooManyAnnotationFilesException(Exception):
    """Raised when too many annotation files are imported."""

    pass


class WrongFormatException(Exception):
    """Raised when an annotation file has the wrong annotation format."""

    pass
