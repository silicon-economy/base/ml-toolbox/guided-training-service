# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining the main logic of the Guided-Training-Service backend."""

import os

__version__ = "8.0.0" + os.environ.get("PACKAGE_VERSION_EXTENSION", "")
