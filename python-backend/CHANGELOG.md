# Guided Training Service - python backend Versions:

8.0.0 (2025-01-28):
-------------------
Integrate new algorithms and highlight features:
- Algorithms:
  - OCR (Text Recogntion) via mlcvzoo-mmocr
  - Image Classification via mlcvzoo-mmpretrain
  - Rotated Object Detection via mlcvzoo-mmrotate
  - Instance Segmentation via mlcvzoo-mmdetection
- Allow to modify the model configuration in an editor before training
- Refactor and introduce several components

7.0.0 (2024-07-19):
-------------------
Update utilized mlcvzoo-service endpoints to latest mlcvzoo-service version
- Additionally: Fix filtering of models in the model overview page

6.3.0 (2024-07-16):
-------------------
Add functionality to get False Positive (FP)/ False Negative (FN) images for a model:
- Add a REST endpoint to get the images
- Add a ModelService as a utility service for the model endpoints

6.2.0 (2024-07-04):
-------------------
Add an API endpoint to retrieve a single dataset by name

6.1.1 (2024-06-24):
-------------------
Upgrade MLflow version to 2.11.3 (coming from 1.30.0)

6.1.0 (2024-06-13):
-------------------
Allow users to overwrite annotations for an already annotated project

6.0.3 (2024-06-13):
-------------------
Adapt python package management:
* Replace poetry by uv as dependency resolver and installer
* Replace poetry by the python native build package for building the python package

6.0.2 (2024-05-06):
-------------------
Fix a crash when trying to load an unknown project from LabelStudio

6.0.1 (2024-04-25):
-------------------
Update python to 3.10

6.0.0 (2024-04-23):
-------------------
Improve interaction with Mlflow server:
* Remove direct interaction with the Mlflow server outside of the MlflowConnector
* Make the MlflowConnector the only interface for interacting with the Mlflow server

5.0.0 (2024-04-23):
-------------------
Improve REST API to be consistent with best practises
- Use correct status code for all endpoints
- Remove redundant exception handling
- Unify Swagger documentation
- Apply naming conventions to routes
- Unify request and response handling

4.2.0 (2024-03-26):
-------------------
Implement functionality to manage edge devices and deploy models
- Register and manage edge devices
- Deploy models on registered edge devices
- Start and stop dataset recordings on registered edge devices

4.1.0 (2024-03-15):
-------------------
Add functionality to retrieve metrics of a single model

4.0.0 (2024-02-22):
-------------------
Refactor backend for consistent and organized module structure
* New module structure
* New module names
* Matching class and file names

3.4.0 (2024-02-21):
-------------------
Add RTMDet models as trainable models

3.3.0 (2024-02-13):
-------------------
Add filter for non matching labels from annotation data upload
* The labels from the annotation file must match the dataset labels
* Non matching labels are not considered

3.2.0 (2024-02-07):
-------------------
Add functionality to create TrackingTasks
* Add functionality to create ObjectTracking datasets in the GTS
* Add functionality to annotate tracking tasks in LabelStudio

3.1.0 (2024-01-11):
-------------------
Implement changes from mlcvzoo-service update (v2.0.0)

3.0.0 (2024-01-08):
-------------------
Improve the LabelStudio Deployment
- Provide a helm chart for a LabelStudio deployment with a version supporting parallel requests
- Implement async feature for parallel requests from backend to LabelStudio

2.5.0 (2024-01-04)
-------------------
Add training status
- Read status of a training run from mlflow
- Log new status of training run during initialization

2.4.0 (2023-12-18):
-------------------
Change in the provision of preview images:
- Remove preview images from endpoint to get all datasets
- Add preview images to endpoint to get a single dataset

2.3.0 (2023-12-01):
-------------------
Implement functionality to auto annotate datasets:
- Implementation of new AutoAnnotationRunner to generate annotations with registered models
- Add REST endpoint to use registered models for auto annotation
- Add action button in "Data Management" tab to start the auto annotation
- Allow to overwrite existing annotations when uploading a new annotation file or auto annotating

2.2.0 (2023-11-22):
-------------------
Hotfix for updating expired S3 client credentials to visualize dataset images in frontend

2.1.0 (2023-11-21):
-------------------
Send Mlflow and LabelStudio URLs to the frontend instead of the frontend generating them

2.0.0 (2023-11-03):
-------------------
Implement model test component with following changes to code base:
- Implementation of new PredictionRunner to execute predictions with registered models
- Create abstract BaseRunner class to inherit from and refactor TrainingRunner class
- Add REST endpoint to use registered models for prediction on created datasets
- Refactor functions/ components accessing Mlflow and/ or S3 servers which are related to the PredictionRunner class

1.2.1 (2023-10-31):
-------------------
Fix backward compatibility: Add default value for evaluation datasets if none are provided

1.2.0 (2023-10-30):
-------------------
Allow the frontend to show what datasets were used to evaluate a model

1.1.0 (2023-10-26):
-------------------
The description of a trained model will now be stored in MLflow

1.0.0 (2023-10-20):
-------------------
Split up evaluation and training datasets when training a new model

0.1.0 (2023-09-23):
-------------------
Initial release of the package