# Guided Training Service - angular frontend Versions:

8.0.0 (2025-01-28):
-------------------
Integrate new algorithms and highlight features:
- Algorithms:
  - OCR (Text Recogntion)
  - Image Classification 
  - Rotated Object Detection
  - Instance Segmentation
- Allow to modify the model configuration in an editor before training

8.0.0 (2024-11-12):
-------------------
Remove unused dashboard

7.7.1 (2024-08-22):
-------------------
Improved validators for name inputs:
- Added validation to prohibit datasets with duplicate names.
- Updated validation to prevent leading and trailing spaces in dataset and model names.
- Fixed dataset management not working properly with unknown dataset types.

7.7.0 (2024-07-16):
-------------------
Add a page to display False Positive (FP)/ False Negative (FN) images in the model detail view

7.6.0 (2024-07-08):
-------------------
Improve functionality of the model detail page by opening the detail page in a new tab

7.5.0 (2024-07-04):
-------------------
Hide unselected steps in train new model dialog

* Remove text labels next to unselected steps due to text wrapping issues
* Add custom step icons for each step
* Fix a bug where the icon next to each step would not have a checkmark when the step was completed

7.4.0 (2024-06-26):
-------------------
Allow files to be uploaded in chunks

- Allow files to be uploaded in chunks of 25 files instead of waiting for each individual file
  - The batch size can be changed using the "UPLOAD_BATCH_SIZE" environment variable
  - This should speed up the upload of files significantly
- Remove legacy LabelStudio environment variables from environment file
- Edit table headers in the "Edit Dataset" Dialog to be consistent with the "Add new Dataset" Dialog

7.3.1 (2024-06-12):
-------------------
Update nodejs to v20

7.3.0 (2024-06-04):
-------------------
Add functionality to duplicate a dataset
- Added dropdown menu in dataset creation dialog
- Fixed bug in dataset creation dialog: Don't disable 'Create' button, when confirming classes by hitting enter

7.2.0 (2024-05-28):
-------------------
Optimized data upload with status indicators
- Merge the "Upload" and "Save" button in the "edit Dataset" dialog.
- Deactivate UI elements when uploading annotations
- Add indicator for upload data:
  * Remove check icon from the table
  * Add status icons for each file in the table
  * Add upload indicator to the 'Upload&Save-Button' in the form of e.g. (5/60)

7.1.0 (2024-05-23):
-------------------
Added a tooltip explaining the different status icons on the model management page

7.0.0 (2024-05-21):
-------------------
Split up dataset upload:
- Remove the ability to upload annotations in the "Add new Dataset" dialog.
- Disable the "Abort" Button in the "Add new Dataset" dialog when uploading images.
- Merge the "Upload" and "Save" button in the "Add new Dataset" dialog.
- You now have to use the "Edit Dataset" (Gear) Icon to upload annotations.
- Fixed an issue in the "Edit dataset" dialog where it would report the incorrect number of annotated images.

6.1.0 (2024-05-07):
-------------------
Add loading spinner when starting a training

6.0.4 (2024-05-03):
-------------------
Disabled most dataset buttons when encountering a corrupted dataset
Added a warning to how the dataset might have been corrupted

6.0.3 (2024-04-29):
-------------------
Remove 0x prefix from page names
Fix image on the deployment page being too large 

6.0.2 (2024-04-25):
-------------------
Update Angular to v17.3.4

6.0.1 (2024-04-23):
-------------------
Implement updates from backend REST API

6.0.0 (2024-03-28):
-------------------
Implement functionality for edge device deployment
- A management page for edge devices
  - Added functionality to add and remove devices
  - Implemented the ability to start/stop deployment of models on devices
- A page to view camera streams on the registered edge devices

5.0.0 (2024-03-15):
-------------------
Create an overview for a single model
- Introduce a new page for individual model overview.
- Directly link the model's overview to its name.
- Move the MLFlow link to the model name in the model overview.
- Include details such as 
  - status, 
  - datasets, 
  - evaluation datasets, 
  - parameters and 
  - metrics on the model's overview.

4.0.0 (2024-03-05):
-------------------
Create Server Management Page
- Add a dedicated page to add and remove servers. This feature will be expanded upon in the future.
- Add a reload button next to server selection boxes to reload available servers.

3.3.0 (2024-02-21):
-------------------
Add RTMDet models as selectable models to the train-new-model dialog

3.2.0 (2024-02-07):
-------------------
Add functionality to upload a video for object-tracking
- Add Object Tracking as dataset type
- Enable video uploads for a dataset

3.1.0 (2024-01-30)
-------------------
Enhance functionality to save servers in the following components:
- Auto-Annotate:
  - Integrate server selection for existing servers
  - Implement a dialog for creating new servers
- Model-Testing:
  - Integrate server selection for existing servers
  - Implement a dialog for creating new servers
  
3.0.0 (2024-01-12)
-------------------
Refactor datamanagement UX:
- Set the labelstudio link to the dataset name
- Remove unnecessary 'annotate' action button
- Change icon for download
- Enable to upload annotations afterwards
- Set classes as required field
- Remove dropdown elements for tags

2.1.0 (2024-01-04)
-------------------
Add training status

- Display status of a training run

2.0.0 (2023-12-18):
-------------------
Revision of preview images and classes:
- Move preview images and classes to a dialog
- Info icon opens dialog for preview images and classes

1.1.3 (2023-12-14):
-------------------
Ensure that files have uploaded correctly
Disable "Upload" button in "Add-new-dataset" dialogue until all files have been uploaded correctly

1.1.2 (2023-12-14):
-------------------
Resolve missing data in the "Upload Data" dialog within the "Add New Dataset" process

1.1.1 (2023-12-04):
-------------------
Optimize dataset selection:
- Fixed missing datasets in "Train-new-model" and "Model-testing" dialogs
- Added loading spinner to the "Model-test-page" on dataset form field

1.1.0 (2023-11-29):
-------------------
Add a functionality to automatically annotate datasets 
- Add action button to data management tab to start the auto annotation

1.0.0 (2023-11-22):
-------------------
Restructure Models and Training page:
- Removed info column
- Replaced the gear icon with an info icon

0.4.0 (2023-11-21):
-------------------
- Clicking on the name of a model in the Model and Training page opens the models MLflow page
- Removed the LabelStudio environment variables
  - Instead, the URLs are provided by the backend

0.3.0 (2023-11-03):
-------------------
Add possibility to specify prediction endpoint used for model testing feature

0.2.0 (2023-10-30):
-------------------
Show evaluation datasets when hovering over the info button and fix bug in 'Train-new-model-dialog':
- Enhancements in Models and Training page:
    - Information on evaluation datasets displayed when hovering over the info button.
- Enhancements in 'Train-new-model-dialog':
    - Implemented consistent display of green checkmarks on the overview screen.

0.1.0 (2023-01-20):
-------------------
Split up evaluation and training datasets when training a new model

0.0.2 (2023-10-18):
-------------------
Fix bug in AddNewDataset when deleting uploaded files

0.0.1 (2023-09-23):
-------------------
Initial release of the package
