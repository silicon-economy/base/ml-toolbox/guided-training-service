/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from 'src/app/app.module';
import {environment} from 'src/environments/environment';

/**
 * Enables production mode to improve performance by disabling Angular-specific debugging features,
 * if the application is running in production mode.
 */
if (environment.production) {
  enableProdMode();
}

/**
 * Bootstraps the Angular application by initializing the main application module (`AppModule`).
 * Catches and logs any errors that occur during bootstrapping.
 */
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
