/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ErrorPageComponent} from 'src/app/core/components/error-page/error-page.component';
import {ImprintLegalPageComponent} from 'src/app/features/pages/imprint-legal-page/imprint-legal-page.component';
import {PrivacyPageComponent} from 'src/app/features/pages/privacy-page/privacy-page.component';
import {
  DataManagementPageComponent
} from 'src/app/features/pages/data-management-page/data-management-page.component';
import {
  ModelManagementPageComponent
} from 'src/app/features/pages/model-management-page/model-management-page.component';
import {ModelTestingPageComponent} from 'src/app/features/pages/model-testing-page/model-testing-page.component';
import {DeploymentPageComponent} from 'src/app/features/pages/deployment-page/deployment-page.component';
import {ServerManagementComponent} from "src/app/features/pages/server-management/server-management.component";
import {
  DetailDeploymentComponent
} from "src/app/features/pages/deployment-page/detail-deployment/detail-deployment.component";

import {
  ModelDetailPageComponent
} from 'src/app/features/pages/model-management-page/model-detail-page/model-detail-page.component';
import {
  MisclassificationViewerPageComponent
} from "src/app/features/pages/model-management-page/misclassification-viewer-page/misclassification-viewer-page.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'data-management'},

  {path: 'data-management', component: DataManagementPageComponent},
  {path: 'models-and-training', component: ModelManagementPageComponent},
  {path: 'model-testing', component: ModelTestingPageComponent},
  {path: 'deployment', component: DeploymentPageComponent},
  {path: 'model-detail/:data', component: ModelDetailPageComponent},
  {path: 'misclassification-viewer/:runId/:className/:modelName', component: MisclassificationViewerPageComponent},
  {path: 'server-management', component: ServerManagementComponent},
  {path: 'detail-deployment/:data', component: DetailDeploymentComponent},

  {path: 'privacy', component: PrivacyPageComponent},
  {path: 'imprint-legal', component: ImprintLegalPageComponent},
  {path: '**', component: ErrorPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
