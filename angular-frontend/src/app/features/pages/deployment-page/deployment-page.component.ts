/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";

import {merge, Subscription, tap} from "rxjs";

import {DevicesService} from 'src/app/core/services/load-devices/devices.service';
import {EdgeDevice} from 'src/app/core/models/deployment';
import {
  AddDeviceDialogComponent
} from 'src/app/features/pages/deployment-page/add-device-dialog/add-device-dialog.component';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {
  DeleteDeviceDialogComponent
} from 'src/app/features/pages/deployment-page/delete-device-dialog/delete-device-dialog.component';
import {
  StopDeploymentDialogComponent
} from 'src/app/features/pages/deployment-page/stop-deployment-dialog/stop-deployment-dialog.component';
import {
  DeployDeviceDialogComponent
} from 'src/app/features/pages/deployment-page/start-deployment-dialog/start-deployment-dialog-component';

/**
 * Component for managing the deployment page.
 */
@Component({
  selector: 'app-deployment-page',
  templateUrl: './deployment-page.component.html',
  styleUrls: ['./deployment-page.component.scss'],
})
export class DeploymentPageComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  devices: EdgeDevice[] = [];
  displayedColumns: string[] = ['name', 'uri', 'creationDate', 'deviceType', 'action'];
  dataSource: EdgeDevice[] = [];
  totalDevices = 0;

  /**
   * Subscriptions for managing resources and avoiding memory leaks
   */
  private readonly subscriptions = new Subscription();

  /**
   * Initializes required services and injects them into the component.
   *
   * @param deviceService - Service to handle device-related backend interactions.
   * @param dialog - Material Dialog service for opening dialogs.
   * @param toasterService - Service to display notifications.
   * @param router - Router service for navigation.
   */
  constructor(private deviceService: DevicesService,
              public dialog: MatDialog,
              public toasterService: ToasterService,
              private router: Router) {
  }

  /**
   * Lifecycle hook that initializes the component and loads devices.
   */
  ngOnInit(): void {
    this.loadDevices();
  }

  /**
   * Loads devices from the service and updates the table's data source.
   */
  loadDevices(): void {
    this.deviceService.getDevices().subscribe({
        next: (response: EdgeDevice[]) => {
          this.devices = response;
          this.totalDevices = response.length;
          this.dataSource = response;
        },
        error: () => this.toasterService.error(`Devices could not be loaded.`, 'Failed to load Devices')
      }
    );
  }

  /**
   * Opens a dialog to confirm the deletion of a device.
   *
   * If confirmed, deletes the device and reloads the list.
   *
   * @param device - The device to be deleted.
   */
  deleteDevice(device: EdgeDevice): void {
    const dialogRef = this.dialog.open(DeleteDeviceDialogComponent, {
      data: device,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deviceService.delete(device).subscribe({
          next: () => {
            this.toasterService.success(`Device ${device.name} was deleted.`, 'Device deleted');
          },
          error: () => this.toasterService.error(`Device ${device.name} could not be deleted.`, 'Failed to delete Device'),
          complete: () => this.loadDevices()
        });
      }
    });
  }

  /**
   * Opens a dialog to add a new device.
   *
   * Reloads the list after adding a new device.
   */
  openAddNewDeviceDialog(): void {
    const dialogRef = this.dialog.open(AddDeviceDialogComponent, {
      height: '80%',
      width: '50%',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadDevices();
    });
  }

  /**
   * Opens a dialog to deploy a device.
   *
   * Reloads the list after the deployment dialog is closed.
   *
   * @param device - The device to be deployed.
   */
  openDeployDeviceDialog(device: EdgeDevice): void {
    const dialogRef = this.dialog.open(DeployDeviceDialogComponent, {
      width: '500px',
      disableClose: true,
      data: device
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadDevices();
    });
  }

  /**
   * Opens a dialog to confirm stopping the deployment of a device.
   *
   * If confirmed, stops deployment and reloads the list.
   *
   * @param device - The device for which the deployment should be stopped.
   */
  openStopDeploymentDialog(device: EdgeDevice): void {
    const dialogRef = this.dialog.open(StopDeploymentDialogComponent, {
      data: device,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deviceService.deployStop(device).subscribe({
          next: () => {
            this.toasterService.success(`Deployment was successfully stopped.`, 'Stopped deployment');
          },
          error: () => this.toasterService.error(`Failed during stop the deployment`, 'Failed to stop deployment'),
          complete: () => this.loadDevices()
        });
      }
    });
  }

  /**
   * Navigates to the device detail page to view its streaming data.
   *
   * @param device - The device to view in detail.
   */
  openStreamOfDevice(device: EdgeDevice): void {
    this.router.navigate(['/detail-deployment', device.id]);
  }

  /**
   * Lifecycle hook that initializes sorting and pagination observers.
   *
   * Resets the paginator to page 0 on each sort change.
   */
  ngAfterViewInit() {
    this.subscriptions.add(
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)
    );

    // Reloads data on sort or pagination change
    this.subscriptions.add(
      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.ngOnInit())
        )
        .subscribe()
    );
  }
}
