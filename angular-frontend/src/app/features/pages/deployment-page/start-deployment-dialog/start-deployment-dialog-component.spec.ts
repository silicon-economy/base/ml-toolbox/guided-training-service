/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from "@angular/common/http/testing";

import {of, throwError} from 'rxjs';

import {DeployDeviceDialogComponent} from "src/app/features/pages/deployment-page/start-deployment-dialog/start-deployment-dialog-component";
import {DevicesService} from "src/app/core/services/load-devices/devices.service";
import {ToasterService} from "src/app/core/services/toaster/toaster.service";
import {ModelManagementService} from "src/app/core/backend/model-management.service";
import {EdgeDevice} from "src/app/core/models/deployment";
import {Model} from "src/app/core/models/model";

describe('DeployDeviceDialogComponent', () => {
  let component: DeployDeviceDialogComponent;
  let fixture: ComponentFixture<DeployDeviceDialogComponent>;
  let devicesService: DevicesService;
  let toasterService: ToasterService;
  let modelManagementService: ModelManagementService;

  const dummy_device: EdgeDevice = {
    id: 1,
    name: 'Test Device 1',
    uri: 'http://example.com/device1',
    deviceType: 'Type 1',
    creationDate: '',
    stream: ''
  };
  const dummy_model: Model = {
    run_id: '0',
    name: 'Dummy-Model',
    description: 'This is a really long description for the model. The model\'s name is "Dummy-Model" and it uses the "Dummy-Dataset" dataset',
    dataset: ['Dummy-Dataset'],
    evaluation_datasets: ['Dummy-Dataset'],
    model: 'modelpath',
    epochs: 30,
    batch_size: 6,
    annotation_task: [],
    training_state: 'running',
    accuracy: 0.8,
    algorithm_type: 'detection',
    creation_date: new Date("2023-08-01T00:00:00.000Z"),
    mlflow_url: "https://example.org/"
  };

  beforeEach(async () => {
    const dialogRefSpyObj = jasmine.createSpyObj('MatDialogRef', ['close']);

    await TestBed.configureTestingModule({
      declarations: [DeployDeviceDialogComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [
        DevicesService,
        ToasterService,
        ModelManagementService,
        {provide: MatDialogRef, useValue: dialogRefSpyObj},
        {provide: MAT_DIALOG_DATA, useValue: dummy_device}
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeployDeviceDialogComponent);
    component = fixture.componentInstance;
    devicesService = TestBed.inject(DevicesService);
    toasterService = TestBed.inject(ToasterService);
    modelManagementService = TestBed.inject(ModelManagementService);
    spyOn(modelManagementService, 'getModels').and.returnValue(of({
      models: [dummy_model], totalModels: 1
    }));
    spyOn(toasterService, 'success').and.stub();
    spyOn(toasterService, 'error').and.stub();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load models on ngOnInit', () => {
    expect(component.models.length).toBe(1);
  });

  it('should set selected model on modelSelected', () => {
    // Act
    component.modelSelected(dummy_model);
    // Verify
    expect(component.selectedModel).toEqual(dummy_model);
  });

  it('should set selected second model on secondModelSelected', () => {
    // Act
    component.secondModelSelected(dummy_model);
    // Verify
    expect(component.secondModel).toEqual(dummy_model);
  });

  it('should deploy model on device', () => {
    // Prepare
    spyOn(devicesService, 'deployStart').and.returnValue(of(null));
    component.selectedModel = dummy_model;
    component.secondModel = dummy_model;

    // Act
    component.deployModel();
    // Verify
    expect(devicesService.deployStart).toHaveBeenCalledWith(component.selectedModel, component.selectedModel, dummy_device);
    expect(toasterService.success).toHaveBeenCalledWith(`Model ${component.selectedModel.name} deployed on ${dummy_device.name}`, 'Successfully deployed Model');
  });

  it('should handle error when deploying model on device', () => {
    // Prepare
    spyOn(devicesService, 'deployStart').and.returnValue(throwError(() => new Error()))
    component.selectedModel = dummy_model;
    // Act
    component.deployModel();
    // Verify
    expect(toasterService.error).toHaveBeenCalledWith(`Model ${component.selectedModel.name} could not be deployed.`, 'Failed to deploy Model');
  });
});
