/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

import {Model} from "src/app/core/models/model";
import {DevicesService} from "src/app/core/services/load-devices/devices.service";
import {EdgeDevice} from "src/app/core/models/deployment";
import {ToasterService} from "src/app/core/services/toaster/toaster.service";
import {ModelManagementService} from "src/app/core/backend/model-management.service";

/**
 * Component to start the deployment of models on an edge device.
 */
@Component({
  selector: 'app-start-deployment-dialog',
  templateUrl: './start-deployment-dialog-component.html',
  styleUrls: ['./start-deployment-dialog-component.scss']
})
export class DeployDeviceDialogComponent implements OnInit {
  models: Model[] = [];
  selectedModel: Model;
  secondModel: Model;

  deployDeviceGroup = new FormGroup({
    modelCtrl: new FormControl([], [
      Validators.required
    ]),
    secondModelCtrl: new FormControl([]),
  });

  /**
   * Constructs the dialog component with injected dependencies and device data.
   *
   * @param deviceService - Service to handle device actions, such as starting deployment.
   * @param data - Data on the target edge device for deployment, injected from parent component.
   * @param dialogRef - Reference to the dialog instance for handling dialog actions.
   * @param toasterService - Service to provide user feedback on the result of deployment.
   * @param modelManagementService - Service to fetch and manage models for deployment.
   */
  constructor(
    private deviceService: DevicesService,
    @Inject(MAT_DIALOG_DATA) public data: EdgeDevice,
    public dialogRef: MatDialogRef<DeployDeviceDialogComponent>,
    public toasterService: ToasterService,
    public modelManagementService: ModelManagementService,
  ) {
  }

  /**
   * Initializes the component by fetching the list of available models.
   */
  ngOnInit() {
    this.modelManagementService.getModels(
      '',
      'creation_data',
      'desc',
      0,
      500
    ).pipe().subscribe({
      next: (res) => {
        this.models = this.modelManagementService.sortModelList(res.models);
      }
    });
  }

  /**
   * Sets the primary model selected by the user.
   *
   * @param model - The selected primary model for deployment.
   */
  modelSelected(model: Model) {
    this.selectedModel = model;
  }

  /**
   * Sets the secondary model selected by the user.
   *
   * @param model - The selected secondary model for deployment.
   */
  secondModelSelected(model: Model) {
    this.secondModel = model;
  }

  /**
   * Initiates the deployment of the selected models to the specified device.
   */
  deployModel(): void {
    this.deviceService.deployStart(this.selectedModel, this.secondModel, this.data).subscribe({
        next: () => {
          this.toasterService.success(`Model ${this.selectedModel.name} deployed on ${this.data.name}`, 'Successfully deployed Model');
          this.dialogRef.close(true);
        },
        error: () => this.toasterService.error(`Model ${this.selectedModel.name} could not be deployed.`, 'Failed to deploy Model')
      }
    )
  }
}


