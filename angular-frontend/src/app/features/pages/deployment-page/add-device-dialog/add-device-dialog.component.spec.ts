/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed, tick, fakeAsync} from '@angular/core/testing';
import {MatDialogRef} from '@angular/material/dialog';
import {ReactiveFormsModule} from '@angular/forms';
import {of, throwError} from 'rxjs';

import {
  AddDeviceDialogComponent
} from 'src/app/features/pages/deployment-page/add-device-dialog/add-device-dialog.component';
import {DevicesService} from 'src/app/core/services/load-devices/devices.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

describe('AddDeviceDialogComponent', () => {
  let component: AddDeviceDialogComponent;
  let fixture: ComponentFixture<AddDeviceDialogComponent>;
  let deviceServiceSpy: jasmine.SpyObj<DevicesService>;
  let dialogRefSpy: jasmine.SpyObj<MatDialogRef<AddDeviceDialogComponent>>;
  let toasterServiceSpy: jasmine.SpyObj<ToasterService>;

  beforeEach(async () => {
    const deviceServiceSpyObj = jasmine.createSpyObj('DevicesService', ['save']);
    const dialogRefSpyObj = jasmine.createSpyObj('MatDialogRef', ['close']);
    const toasterServiceSpyObj = jasmine.createSpyObj('ToasterService', ['success', 'error']);

    await TestBed.configureTestingModule({
      declarations: [AddDeviceDialogComponent],
      imports: [ReactiveFormsModule],
      providers: [
        {provide: DevicesService, useValue: deviceServiceSpyObj},
        {provide: MatDialogRef, useValue: dialogRefSpyObj},
        {provide: ToasterService, useValue: toasterServiceSpyObj}
      ]
    })
      .compileComponents();

    deviceServiceSpy = TestBed.inject(DevicesService) as jasmine.SpyObj<DevicesService>;
    dialogRefSpy = TestBed.inject(MatDialogRef) as jasmine.SpyObj<MatDialogRef<AddDeviceDialogComponent>>;
    toasterServiceSpy = TestBed.inject(ToasterService) as jasmine.SpyObj<ToasterService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeviceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add device and close dialog on success', fakeAsync(() => {
    // Prepare
    deviceServiceSpy.save.and.returnValue(of(null));

    // Act
    component.deviceFormGroup.get('nameFormCtrl').setValue('Test Device');
    component.deviceFormGroup.get('uriFormCtrl').setValue('http://example.com');
    component.deviceFormGroup.get('typeFormCtrl').setValue('Test Type');
    component.addDevice();
    tick();

    // Verify
    expect(deviceServiceSpy.save).toHaveBeenCalledOnceWith({
      id: null,
      name: 'Test Device',
      uri: 'http://example.com',
      deviceType: 'Test Type',
      creationDate: null,
      stream: ''
    });
    expect(toasterServiceSpy.success).toHaveBeenCalledOnceWith('Created new Device Test Device', 'Created Device');
    expect(dialogRefSpy.close).toHaveBeenCalledOnceWith(true);
  }));

  it('should handle error when adding device', fakeAsync(() => {
    // Prepare
    deviceServiceSpy.save.and.returnValue(throwError('Error'));

    // Act
    component.deviceFormGroup.get('nameFormCtrl').setValue('Test Device');
    component.deviceFormGroup.get('uriFormCtrl').setValue('http://example.com');
    component.deviceFormGroup.get('typeFormCtrl').setValue('Test Type');
    component.addDevice();
    tick();

    // Verify
    expect(deviceServiceSpy.save).toHaveBeenCalled();
    expect(toasterServiceSpy.error).toHaveBeenCalledOnceWith('Device Test Device could not be created.', 'Failed to create Device');
    expect(dialogRefSpy.close).toHaveBeenCalledOnceWith(true);
  }));

  it('should reset form', () => {
    // Act
    component.deviceFormGroup.get('nameFormCtrl').setValue('Test Device');
    component.deviceFormGroup.get('uriFormCtrl').setValue('http://example.com');
    component.deviceFormGroup.get('typeFormCtrl').setValue('Test Type');
    component.reset();

    // Verify
    expect(component.deviceFormGroup.get('nameFormCtrl').value).toBeNull();
    expect(component.deviceFormGroup.get('uriFormCtrl').value).toBeNull();
    expect(component.deviceFormGroup.get('typeFormCtrl').value).toBeNull();
  });
});
