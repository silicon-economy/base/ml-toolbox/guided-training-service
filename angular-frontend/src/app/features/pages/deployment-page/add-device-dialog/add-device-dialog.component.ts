/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

import {DevicesService} from 'src/app/core/services/load-devices/devices.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

/**
 * Dialog component for adding a new edge device.
 */
@Component({
  selector: 'app-add-device-dialog',
  templateUrl: './add-device-dialog.component.html',
  styleUrls: ['./add-device-dialog.component.scss']
})
export class AddDeviceDialogComponent {

  deviceFormGroup = new FormGroup({
    nameFormCtrl: new FormControl('', [
      Validators.required
    ]),
    uriFormCtrl: new FormControl('', [
      Validators.required
    ]),
    typeFormCtrl: new FormControl('', [
      Validators.required
    ])
  });

  finished = false;

  /**
   * Initializes necessary services for adding devices and displaying dialog and toaster notifications.
   *
   * @param deviceService - Service to handle device-related backend interactions.
   * @param dialogRef - Reference to the Material dialog to manage dialog actions.
   * @param toasterService - Service to display notifications upon success or failure of device addition.
   */
  constructor(
    private deviceService: DevicesService,
    public dialogRef: MatDialogRef<AddDeviceDialogComponent>,
    private toasterService: ToasterService
  ) {
  }

  /**
   * Adds a new device by calling the device service with the form data.
   *
   * The device data sent to the service includes:
   * - `id`: set to `null` to allow the backend to assign an ID.
   * - `creationDate`: initialized as `null` to be set by the backend.
   * - `stream`: initialized as an empty string for future use in detailed views.
   *
   * Upon successful save, displays a success message. Otherwise, shows an error message.
   */
  addDevice(): void {
    this.deviceService.save({
      id: null,
      name: this.deviceFormGroup.get('nameFormCtrl').value,
      uri: this.deviceFormGroup.get('uriFormCtrl').value,
      deviceType: this.deviceFormGroup.get('typeFormCtrl').value,
      creationDate: null,
      stream: ''
    }).subscribe({
      next: () => {
        this.toasterService.success(`Created new Device ${this.deviceFormGroup.get('nameFormCtrl').value}`, 'Created Device');
      },
      error: () => this.toasterService.error(`Device ${this.deviceFormGroup.get('nameFormCtrl').value} could not be created.`, 'Failed to create Device')
    })
    this.dialogRef.close(true);
  }

  /**
   * Resets the form controls for device name, URI, and type to their default values (null).
   */
  reset(): void {
    this.deviceFormGroup.get('nameFormCtrl').setValue(null);
    this.deviceFormGroup.get('uriFormCtrl').setValue(null);
    this.deviceFormGroup.get('typeFormCtrl').setValue(null);
  }
}
