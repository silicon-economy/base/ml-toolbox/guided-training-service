/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

import {EdgeDevice} from 'src/app/core/models/deployment';

/**
 * Dialog component for confirming the deletion of an edge device.
 */
@Component({
  selector: 'app-delete-device-dialog',
  templateUrl: './delete-device-dialog.component.html',
  styleUrls: ['./delete-device-dialog.component.scss']
})
export class DeleteDeviceDialogComponent {

  /**
   * Initializes the dialog with the injected data for the device to be deleted.
   *
   * @param data - The edge device details passed into the dialog.
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: EdgeDevice) {
  }

}
