/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

import {EdgeDevice} from "src/app/core/models/deployment";

/**
 * Component to stop the deployment of models on an edge device.
 */
@Component({
  selector: 'app-stop-deployment-dialog',
  templateUrl: './stop-deployment-dialog.component.html',
  styleUrls: ['./stop-deployment-dialog.component.scss']
})
export class StopDeploymentDialogComponent {
  /**
   * The edge device for which the deployment is being stopped.
   * This data is injected from the parent component when opening the dialog.
   *
   * @param data - The edge device object that contains deployment information.
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: EdgeDevice) {
  }
}
