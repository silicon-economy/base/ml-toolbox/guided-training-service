/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {DevicesService} from 'src/app/core/services/load-devices/devices.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {EdgeDevice} from 'src/app/core/models/deployment';

/**
 * Component for managing the deployment of a model on an edge device.
 * It allows the user to view the device's stream, start and stop recording, adjust resolution,
 * and toggle neural network prediction overlays.
 */
@Component({
  selector: 'app-detail-deployment',
  templateUrl: './detail-deployment.component.html',
  styleUrls: ['./detail-deployment.component.scss']
})
export class DetailDeploymentComponent implements OnInit, AfterViewInit {

  device: EdgeDevice;
  deviceId: number;
  streamUrl = '';
  neural = false;

  // Available resolution options for the video stream.
  resolutions = [
    {"name": "2560x1440", "checked": false},
    {"name": "1920x1080", "checked": true},
    {"name": "1280x720", "checked": false}
  ];
  resolution = this.resolutions[1];  // Default resolution is 1920x1080

  seconds = 0;
  timer: any;
  isRecording = false;
  isDownloading = false;
  downloadLink = '';
  fileName = '';

  /**
   * Initializes the component by retrieving the device ID from route parameters.
   *
   * @param route - The route service to retrieve route parameters.
   * @param devicesService - The service to fetch device data.
   * @param toasterService - The service to display notifications.
   */
  constructor(private route: ActivatedRoute, private devicesService: DevicesService, private toasterService: ToasterService) {
  }

  /**
   * Lifecycle hook that is called when the component is initialized.
   * Retrieves the device ID from route parameters.
   */
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.deviceId = params['data']; // Access data from route parameters. data = device
    });
  }

  /**
   * Lifecycle hook that is called after the view has been initialized.
   * Retrieves the stream details and initializes parameters.
   */
  ngAfterViewInit() {
    this.getStream();
  }

  /**
   * Retrieves the device stream and sets relevant parameters like neural display and resolution.
   */
  getStream() {
    this.streamUrl = '';
    this.devicesService.getDeviceDetails(this.deviceId).subscribe({
        next: (device) => {
          this.device = device;
          this.streamUrl = device.stream;
          this.setNeural();
          this.setResolution();
        },
        error: () => {
          this.toasterService.error(`Failed during loading device stream.`, 'Failed loading stream')
        }
      }
    );
  }

  /**
   * Sets the base parameters for the device stream URL.
   *
   * Currently, 3 parameters can be appended to the url:
   * - resolution
   * - neural
   * - fps
   * The first parameter starts with '/?' and all others are appended with '&'.
   */
  setParamsUrl() {
    let paramsString;
    if (this.streamUrl.includes('/?')) {
      paramsString = '&';
    } else {
      paramsString = '/?';
    }
    this.streamUrl += paramsString;
  }

  /**
   * Sets the neural flag in the stream URL to enable prediction drawing if selected.
   */
  setNeural() {
    if (this.neural) {
      this.streamUrl += this.streamUrl.includes('?') ? '&draw_predictions=true' : '?draw_predictions=true';
    }
  }

  /**
   * Sets the resolution in the stream URL.
   */
  setResolution() {
    this.streamUrl += this.streamUrl.includes('?') ? `&resolution=${this.resolution.name}` : `?resolution=${this.resolution.name}`;
  }

  /**
   * Starts recording the device stream and sets up a timer to track recording duration.
   */
  startRecording() {
    this.downloadLink = '';
    this.devicesService.startRecording(this.deviceId).subscribe({
      next: (response: string) => {
        this.isRecording = true;
        this.startTimer();
        this.toasterService.success(response, 'Successfully start the recording');
      },
      error: () => {
        this.toasterService.error('Failed during start the recording', 'Failed recording stream');
      }
    })
  }

  /**
   * Stops the recording of the device stream and prepares the download link.
   */
  stopRecording() {
    this.isDownloading = true;
    this.isRecording = false;
    this.resetTimer();
    this.devicesService.stopRecording(this.deviceId).subscribe({
      next: (response: any) => {
        this.downloadLink = response['download_link'];
        this.fileName = response['file_name'];
        this.toasterService.success(response['message'], 'Successfully stop the recording');
      },
      error: () => {
        this.toasterService.error('Failed during stop the recording', 'Failed to stop the recording');
      },
      complete: () => {
        this.isDownloading = false;
      }
    })
  }

  /**
   * Starts the timer to track the recording duration.
   */
  startTimer() {
    this.seconds = 0;
    this.timer = setInterval(() => {
      this.seconds++;
    }, 1000);
  }

  /**
   * Resets the recording timer to zero.
   */
  resetTimer() {
    this.seconds = 0;
  }
}
