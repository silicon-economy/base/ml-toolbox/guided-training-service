/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {ActivatedRoute} from '@angular/router';
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {of, throwError} from 'rxjs';

import {DevicesService} from 'src/app/core/services/load-devices/devices.service';
import {DetailDeploymentComponent} from 'src/app/features/pages/deployment-page/detail-deployment/detail-deployment.component';
import {EdgeDevice} from 'src/app/core/models/deployment';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

describe('DetailDeploymentComponent', () => {
  let devicesService: DevicesService;
  let toasterService: ToasterService;
  let component: DetailDeploymentComponent;
  let fixture: ComponentFixture<DetailDeploymentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [DetailDeploymentComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({data: 1})
          }
        },
        {provide: DevicesService}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailDeploymentComponent);
    component = fixture.componentInstance;
    devicesService = TestBed.inject(DevicesService);
    toasterService = TestBed.inject(ToasterService);
    spyOn(toasterService, 'success');
    spyOn(toasterService, 'error');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set stream URL and device details on successful subscription', () => {
    // Prepare
    const mockDevice: EdgeDevice = {
      id: 1,
      name: 'Test Device 1',
      uri: 'http://example.com/device1',
      deviceType: 'Type 1',
      creationDate: '',
      stream: 'http://test.de'
    };
    spyOn(devicesService, 'getDeviceDetails').and.returnValue(of(mockDevice));
    spyOn(component, 'setNeural');
    spyOn(component, 'setResolution');
    // Act
    component.getStream();
    // Verify
    expect(component.device).toEqual(mockDevice);
    expect(component.streamUrl).toEqual(mockDevice.stream);
    expect(component.setNeural).toHaveBeenCalled();
    expect(component.setResolution).toHaveBeenCalled();
  });

  it('should call error toaster on subscription error', () => {
    // Prepare
    spyOn(devicesService, 'getDeviceDetails').and.returnValue(throwError(() => {
      new Error
    }));
    // Act
    component.getStream();
    // Verify
    expect(toasterService.error).toHaveBeenCalledWith('Failed during loading device stream.', 'Failed loading stream');
  });

  it('should start recording', () => {
    // Prepare
    const response = 'Recording started successfully';
    const devicesServiceSpy = spyOn(devicesService, 'startRecording').and.returnValue(of(response));
    // Act
    component.startRecording();
    // Verify
    expect(devicesServiceSpy).toHaveBeenCalledOnceWith(component.deviceId);
    expect(component.isRecording).toBeTrue();
  });

  it('should handle error when start recording', () => {
    // Prepare
    spyOn(devicesService, 'startRecording').and.returnValue(throwError(() => {
      new Error
    }));
    // Act
    component.startRecording();
    // Verify
    expect(toasterService.error).toHaveBeenCalledWith(`Failed during start the recording`, 'Failed recording stream');
  });

  it('should stop recording', () => {
    // Prepare
    const response = {message: 'Recording stopped successfully', download_link: 'some_link', file_name: 'some_name'};
    const devicesServiceSpy = spyOn(devicesService, 'stopRecording').and.returnValue(of(response));
    // Act
    component.stopRecording();
    // Verify
    expect(devicesServiceSpy).toHaveBeenCalledOnceWith(component.deviceId);
    expect(component.isRecording).toBeFalse();
    expect(component.downloadLink).toEqual(response.download_link);
    expect(component.fileName).toEqual(response.file_name);
  });

  it('should handle error when stop recording', () => {
    // Prepare
    spyOn(devicesService, 'stopRecording').and.returnValue(throwError(() => {
      new Error
    }));
    // Act
    component.stopRecording();
    // Verify
    expect(toasterService.error).toHaveBeenCalledWith(`Failed during stop the recording`, 'Failed to stop the recording');
  });

  it('should start timer', () => {
    // Prepare
    spyOn(window, 'setInterval').and.callThrough();
    // Act
    component.startTimer();
    // Verify
    expect(component.seconds).toEqual(0);
    expect(window.setInterval).toHaveBeenCalled();
  });

  it('should reset timer', () => {
    // Prepare
    component.seconds = 10;
    // Act
    component.resetTimer();
    // Verify
    expect(component.seconds).toEqual(0);
  });

  it('should set stream URL with existing query parameters and draw_predictions=true', () => {
    // Prepare
    component.streamUrl = 'http://example.com/device1?resolution=1920x1080';
    component.neural = true;
    // Act
    component.setNeural();
    // Verify
    expect(component.streamUrl).toEqual('http://example.com/device1?resolution=1920x1080&draw_predictions=true');
  });

  it('should set stream URL with resolution parameter', () => {
    // Prepare
    component.streamUrl = 'http://example.com/device1';
    component.resolution = {name: '1920x1080', checked: true};
    // Act
    component.setResolution();
    // Verify
    expect(component.streamUrl).toEqual('http://example.com/device1?resolution=1920x1080');
  });

  it('should append resolution parameter to existing URL', () => {
    // Prepare
    component.streamUrl = 'http://example.com/device1?draw_predictions=true';
    component.resolution = {name: '1280x720', checked: true};
    // Act
    component.setResolution();
    // Verify
    expect(component.streamUrl).toEqual('http://example.com/device1?draw_predictions=true&resolution=1280x720');
  });

  it('should append "&" to the URL if "/?" is already present', () => {
    component.streamUrl = 'http://example.com/device1/?resolution=1920x1080';

    component.setParamsUrl();

    expect(component.streamUrl).toEqual('http://example.com/device1/?resolution=1920x1080&');
  });

  it('should append "/?" to the URL if no parameters are present', () => {
    component.streamUrl = 'http://example.com/device1';

    component.setParamsUrl();

    expect(component.streamUrl).toEqual('http://example.com/device1/?');
  });

  it('should append "&" to the URL if no "/" is present but parameters are present', () => {
    component.streamUrl = 'http://example.com/device1/?resolution=1920x1080';

    component.setParamsUrl();

    expect(component.streamUrl).toEqual('http://example.com/device1/?resolution=1920x1080&');
  });
});
