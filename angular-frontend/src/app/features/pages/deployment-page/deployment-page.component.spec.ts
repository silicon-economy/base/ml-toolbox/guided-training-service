/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router} from '@angular/router';

import {of, throwError} from 'rxjs';

import {DeploymentPageComponent} from 'src/app/features/pages/deployment-page/deployment-page.component';
import {DevicesService} from 'src/app/core/services/load-devices/devices.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {ModelManagementService} from 'src/app/core/backend/model-management.service';
import {EdgeDevice} from 'src/app/core/models/deployment';
import {
  DeleteDeviceDialogComponent
} from 'src/app/features/pages/deployment-page/delete-device-dialog/delete-device-dialog.component';
import {
  AddDeviceDialogComponent
} from 'src/app/features/pages/deployment-page/add-device-dialog/add-device-dialog.component';
import {
  StopDeploymentDialogComponent
} from 'src/app/features/pages/deployment-page/stop-deployment-dialog/stop-deployment-dialog.component';
import {
  DeployDeviceDialogComponent
} from 'src/app/features/pages/deployment-page/start-deployment-dialog/start-deployment-dialog-component';


describe('DeploymentPageComponent', () => {
  let component: DeploymentPageComponent;
  let fixture: ComponentFixture<DeploymentPageComponent>;
  let router: Router;
  let devicesService: DevicesService;
  let toasterService: ToasterService;

  const dummy_devices: EdgeDevice[] = [
    {
      id: 1,
      name: 'Test Device 1',
      uri: 'http://example.com/device1',
      deviceType: 'Type 1',
      creationDate: '',
      stream: ''
    },
    {
      id: 2,
      name: 'Test Device 2',
      uri: 'http://example.com/device2',
      deviceType: 'Type 2',
      creationDate: '',
      stream: ''
    }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DeploymentPageComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        MatDialogModule,
        MatPaginatorModule,
        MatSortModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      providers: [
        DevicesService,
        ToasterService,
        ModelManagementService
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeploymentPageComponent);
    component = fixture.componentInstance;
    devicesService = TestBed.inject(DevicesService);
    toasterService = TestBed.inject(ToasterService);
    router = TestBed.inject(Router);
    spyOn(router, 'navigate').and.stub();
    spyOn(toasterService, 'success');
    spyOn(toasterService, 'error');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should load devices on ngOnInit', () => {
    // Prepare
    spyOn(devicesService, 'getDevices').and.returnValue(of(dummy_devices));
    // Act
    component.ngOnInit();
    // Verify
    expect(component.devices.length).toBe(2);
    expect(component.totalDevices).toBe(2);
    expect(component.dataSource.length).toBe(2);
  });

  it('should handle error when loading devices on ngOnInit', () => {
    // Prepare
    spyOn(devicesService, 'getDevices').and.returnValue(throwError(() => new Error()));
    // Act
    component.ngOnInit();
    // Verify
    expect(toasterService.error).toHaveBeenCalledOnceWith(`Devices could not be loaded.`, 'Failed to load Devices');
  });

  it('should delete device', inject([MatDialog, DevicesService], (matdialog: MatDialog, devicesService: DevicesService) => {
    // Prepare
    const loadDevicesSpy = spyOn(component, 'loadDevices');
    const deleteDeviceSpy = spyOn(devicesService, 'delete').and.returnValue(of({}));
    const dialogSpy = spyOn(matdialog, 'open').and
      .returnValue(<MatDialogRef<typeof DeleteDeviceDialogComponent>>{afterClosed: () => of(true)});
    // Act
    component.deleteDevice(dummy_devices[0]);
    // Verify
    expect(devicesService.delete).toHaveBeenCalledWith(dummy_devices[0]);
    expect(dialogSpy).toHaveBeenCalledOnceWith(DeleteDeviceDialogComponent, {data: dummy_devices[0]});
    expect(deleteDeviceSpy).toHaveBeenCalledOnceWith(dummy_devices[0]);
    expect(toasterService.success).toHaveBeenCalledOnceWith(`Device ${dummy_devices[0].name} was deleted.`, 'Device deleted');
    expect(loadDevicesSpy).toHaveBeenCalled();
  }));

  it('should handle error when deleting device', inject([MatDialog, DevicesService], (matdialog: MatDialog, devicesService: DevicesService) => {
    // Prepare
    spyOn(devicesService, 'delete').and.returnValue(throwError(() => new Error()));
    spyOn(matdialog, 'open').and
      .returnValue(<MatDialogRef<typeof DeleteDeviceDialogComponent>>{afterClosed: () => of(true)});
    // Act
    component.deleteDevice(dummy_devices[0]);
    // Verify
    expect(toasterService.error).toHaveBeenCalledOnceWith(`Device ${dummy_devices[0].name} could not be deleted.`, 'Failed to delete Device');
  }));

  it('should open add new device dialog', inject([MatDialog], (matdialog: MatDialog) => {
    // Prepare
    const loadDevicesSpy = spyOn(component, 'loadDevices');
    const dialogSpy = spyOn(matdialog, 'open').and
      .returnValue(<MatDialogRef<typeof AddDeviceDialogComponent>>{afterClosed: () => of(true)});
    // Act
    component.openAddNewDeviceDialog();
    // Verify
    expect(dialogSpy).toHaveBeenCalledOnceWith(AddDeviceDialogComponent, {
      height: '80%',
      width: '50%',
      disableClose: true
    });
    expect(loadDevicesSpy).toHaveBeenCalled();
  }));

  it('should open deploy device dialog', inject([MatDialog], (matdialog: MatDialog) => {
    // Prepare
    const loadDevicesSpy = spyOn(component, 'loadDevices');
    const dialogSpy = spyOn(matdialog, 'open').and
      .returnValue(<MatDialogRef<typeof AddDeviceDialogComponent>>{afterClosed: () => of(true)});
    // Act
    component.openDeployDeviceDialog(dummy_devices[0]);
    // Verify
    expect(dialogSpy).toHaveBeenCalledOnceWith(DeployDeviceDialogComponent, {
      width: '500px',
      disableClose: true,
      data: dummy_devices[0]
    });
    expect(loadDevicesSpy).toHaveBeenCalled();
  }));

  it('should open stop deployment dialog', inject([MatDialog, DevicesService], (matdialog: MatDialog, devicesService: DevicesService) => {
    // Prepare
    const loadDevicesSpy = spyOn(component, 'loadDevices');
    const stopDeploymentSpy = spyOn(devicesService, 'deployStop').and.returnValue(of({}));
    const dialogSpy = spyOn(matdialog, 'open').and
      .returnValue(<MatDialogRef<typeof StopDeploymentDialogComponent>>{afterClosed: () => of(true)});
    // Act
    component.openStopDeploymentDialog(dummy_devices[0]);
    // Verify
    expect(devicesService.deployStop).toHaveBeenCalledWith(dummy_devices[0]);
    expect(dialogSpy).toHaveBeenCalledOnceWith(StopDeploymentDialogComponent, {data: dummy_devices[0]});
    expect(stopDeploymentSpy).toHaveBeenCalledOnceWith(dummy_devices[0]);
    expect(toasterService.success).toHaveBeenCalledOnceWith(`Deployment was successfully stopped.`, 'Stopped deployment');
    expect(loadDevicesSpy).toHaveBeenCalled();
  }));

  it('should handle error when stopping deployment', inject([MatDialog, DevicesService], (matdialog: MatDialog, devicesService: DevicesService) => {
    // Prepare
    const dialogRefMock = jasmine.createSpyObj('MatDialogRef', ['afterClosed']);
    dialogRefMock.afterClosed.and.returnValue(of(true));
    const stopDeploymentSpy = spyOn(devicesService, 'deployStop').and.returnValue(throwError(() => new Error()));
    spyOn(matdialog, 'open').and.returnValue(dialogRefMock);
    // Act
    component.openStopDeploymentDialog(dummy_devices[0]);
    // Verify
    expect(stopDeploymentSpy).toHaveBeenCalledWith(dummy_devices[0]);
    expect(toasterService.error).toHaveBeenCalledOnceWith(`Failed during stop the deployment`, 'Failed to stop deployment');
  }));


  it('should open stream of device', () => {
    // Act
    component.openStreamOfDevice(dummy_devices[0]);
    // Verify
    expect(router.navigate).toHaveBeenCalledWith(['/detail-deployment', dummy_devices[0].id]);
  });
});

