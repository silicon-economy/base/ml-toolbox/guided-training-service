/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {HttpClient} from "@angular/common/http";
import {environment} from "src/environments/environment";
import {Observable} from "rxjs";

/**
 * Dialog component for adding a new training server.
 */
@Component({
  selector: 'app-auto-annotate-dialog',
  templateUrl: './add-server-dialog.component.html',
  styleUrls: ['./add-server-dialog.component.scss']
})
export class AddServerDialogComponent implements OnInit {

  /**
   * API endpoint for creating a new training server.
   * Constructed using values from the environment configuration.
   * TODO: Move this to the service
   */
  private readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  /**
   * Reactive form group for the server name and endpoint inputs.
   */
  addServerGroup = new FormGroup({
    serverNameFormCtrl: new FormControl('', [
      Validators.required
    ]),
    serverEndpointFormCtrl: new FormControl('', [
      Validators.required,
      Validators.pattern('^(http|https)://.*$')
    ])
  });
  finished = false;

  /**
   * Constructor for AddServerDialogComponent.
   *
   * @param data Injected data for dialog (optional).
   * @param dialogRef Reference to the dialog, allowing it to close with a result.
   * @param toasterService Toaster service for displaying notifications.
   * @param http HttpClient for making HTTP requests.
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddServerDialogComponent>,
    public toasterService: ToasterService,
    private http: HttpClient
  ) {
  }

  /**
   * Initializes the dialog component. A placeholder for any deep copy initialization if needed.
   */
  ngOnInit() {
    // Placeholder: Deep copy initialization logic could go here if necessary
  }

  /**
   * Sends a POST request to create a new training server.
   *
   * @param name The name of the new server.
   * @param endpoint The endpoint URL of the new server.
   *
   * @returns Observable of the server creation HTTP request.
   */
  addServer(name: string, endpoint: string): Observable<object> {
    const body = {
      'name': name,
      'endpoint': endpoint,
    }
    return this.http.post(`${this.API_URL}/training-servers/`, body);
  }

  /**
   * Saves the server by retrieving form data, sending it via `addServer`, and handling the result.
   * On success, closes the dialog and shows a success message; on error, shows an error message.
   */
  saveServer(): void {
    const serverName = this.addServerGroup.value.serverNameFormCtrl
    const serverEndpoint = this.addServerGroup.value.serverEndpointFormCtrl

    this.addServer(serverName, serverEndpoint).subscribe({
      next: () => {
        this.toasterService.success(`Created new server '${serverName}'`, 'Created server');
        this.dialogRef.close(true);
      },
      error: () => this.toasterService.error(`Failed to create the training server '${serverEndpoint}'.`, 'Error creating training server')
    });
  }
}
