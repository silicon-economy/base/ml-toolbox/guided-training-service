/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {of, throwError} from 'rxjs';
import {
  AddServerDialogComponent
} from 'src/app/features/pages/server-management/add-server-dialog/add-server-dialog.component';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {TrainingServerService} from 'src/app/core/backend/training-server.service';
import {TrainingServer} from 'src/app/core/models/trainingserver';
import {environment} from 'src/environments/environment';

describe('AddServerDialogComponent', () => {
  let component: AddServerDialogComponent;
  let fixture: ComponentFixture<AddServerDialogComponent>;
  let toasterService: ToasterService;
  let service: TrainingServerService;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddServerDialogComponent],
      imports: [
        BrowserAnimationsModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        FormsModule,
        MatTooltipModule,
        MatIconModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      providers: [
        ToasterService,
        TrainingServerService,
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {
            }
          }
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }
      ]
    }).compileComponents();
    service = TestBed.inject(TrainingServerService);
    httpMock = TestBed.inject(HttpTestingController);
    toasterService = TestBed.inject(ToasterService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddServerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form controls', () => {
    expect(component.addServerGroup.get('serverNameFormCtrl')).toBeTruthy();
    expect(component.addServerGroup.get('serverEndpointFormCtrl')).toBeTruthy();
  });

  it('should call addServer and close dialog on saveServer', () => {
    const serverName = 'TestServer';
    const serverEndpoint = 'http://test-server';

    const componentSpy = spyOn(component, 'addServer').and.returnValue(of({}));
    spyOn(component.dialogRef, 'close');

    component.addServerGroup.setValue({
      serverNameFormCtrl: serverName,
      serverEndpointFormCtrl: serverEndpoint
    });
    component.saveServer();

    expect(componentSpy).toHaveBeenCalledWith(serverName, serverEndpoint);
    expect(component.dialogRef.close).toHaveBeenCalledWith(true);
  });

  it('should call addServer on saveServer and get toaster error', async () => {
    const serverName = 'TestServer';
    const serverEndpoint = 'invalid-url';

    const componentSpy = spyOn(component, 'addServer').and.returnValue(throwError(() => new Error('Error creating training server')));
    const toastErrorSpy = spyOn(toasterService, 'error');

    component.addServerGroup.setValue({
      serverNameFormCtrl: serverName,
      serverEndpointFormCtrl: serverEndpoint
    });
    expect(component.addServerGroup.invalid).toBeTrue();
    expect(component.addServerGroup.get('serverEndpointFormCtrl').hasError('pattern')).toBeTrue();

    await component.saveServer();

    expect(componentSpy).toHaveBeenCalledWith(serverName, serverEndpoint);
    expect(toastErrorSpy).toHaveBeenCalledOnceWith(`Failed to create the training server '${serverEndpoint}'.`, 'Error creating training server');
  });

  it('should send a POST request to add a server', () => {
    const serverName = 'TestServer';
    const serverEndpoint = 'http://test-server';
    const newServer: TrainingServer = {
      name: serverName,
      endpoint: serverEndpoint,
      id: 0,
      inEditing: false,
      selected: false
    };

    service.addTrainingServer(newServer).subscribe(response => {
      expect(response).toBeTruthy();
    });

    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/training-servers/`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({'name': serverName, 'endpoint': serverEndpoint});

    req.flush({});
    httpMock.verify();
  });
});
