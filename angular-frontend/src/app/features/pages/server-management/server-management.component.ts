/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, OnInit} from '@angular/core';
import {TrainingServer} from "src/app/core/models/trainingserver";
import {TrainingServerService} from "src/app/core/backend/training-server.service";
import {ToasterService} from "src/app/core/services/toaster/toaster.service";
import {ServerDataSource} from "src/app/core/backend/server.datasource";
import {
  AddServerDialogComponent
} from "src/app/features/pages/server-management/add-server-dialog/add-server-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {
  RemoveServerDialogComponent
} from "src/app/features/pages/server-management/remove-server-dialog/remove-server-dialog.component";

/**
 * Component for managing training servers in the application.
 */
@Component({
  selector: 'app-server-management',
  templateUrl: './server-management.component.html',
  styleUrls: ['./server-management.component.scss']
})
export class ServerManagementComponent implements OnInit {

  dataSource: ServerDataSource;
  servers: TrainingServer[];
  displayedColumns: string[] = ['name', 'endpoint', 'action'];

  /**
   * Constructor to inject required services.
   * @param serverService Service for managing server data.
   * @param toasterService Service for displaying notifications.
   * @param dialog Material dialog service for opening dialogs.
   */
  public constructor(public serverService: TrainingServerService, public toasterService: ToasterService, public dialog: MatDialog) {
  }

  /**
   * Initializes the component and loads the servers into the data source.
   */
  ngOnInit(): void {
    this.dataSource = new ServerDataSource(this.serverService);
    this.dataSource.loadServers()
  }

  /**
   * Opens a dialog to add a new training server.
   * After the dialog closes, the server list is reloaded.
   */
  openAddServer(): void {
    const dialogRef = this.dialog.open(AddServerDialogComponent, {
      width: '500px',
      data: {customName: '', endpointUrl: ''},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      this.dataSource.loadServers()
    });
  }

  /**
   * Opens a dialog to confirm server removal. If confirmed, deletes the server and
   * displays a success or error notification.
   * @param server The server instance to be deleted.
   */
  removeServer(server: TrainingServer) {
    const dialogRef = this.dialog.open(RemoveServerDialogComponent, {
      width: '500px', data: server, disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.serverService.deleteTrainingServer(server)
          .subscribe({
            next: () => {
              this.toasterService.success(`Server '${server.name}' was deleted successfully`, 'Server deleted');
              this.dataSource.loadServers();
            },
            error: () => this.toasterService.error(`Server ${server.name} could not be deleted.`, 'Failed to remove Server')
          });
      }
    });
  }
}

