/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ServerManagementComponent} from "src/app/features/pages/server-management/server-management.component";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {of, throwError} from "rxjs";
import {
  AddServerDialogComponent
} from "src/app/features/pages/server-management/add-server-dialog/add-server-dialog.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MatTableModule} from "@angular/material/table";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSortModule} from "@angular/material/sort";
import {MatChipsModule} from "@angular/material/chips";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatIconModule} from "@angular/material/icon";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {TimePastPipe} from "ng-time-past-pipe";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {
  RemoveServerDialogComponent
} from "src/app/features/pages/server-management/remove-server-dialog/remove-server-dialog.component";
import {TrainingServer} from "src/app/core/models/trainingserver";

export class MdDialogMock {

  open() {
    return {
      afterClosed: () => of(true)
    };
  }
}


describe('ServerManagementComponent', () => {
  let component: ServerManagementComponent;
  let fixture: ComponentFixture<ServerManagementComponent>;
  let matDialog: MatDialog;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSortModule,
        MatChipsModule,
        MatProgressBarModule,
        MatIconModule,
        MatPaginatorModule,
        MatTooltipModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        TimePastPipe,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      declarations: [ServerManagementComponent],
      providers: [
        {
          provide: MatDialog,
          useClass: MdDialogMock
        }
      ]
    }).compileComponents();
    matDialog = TestBed.inject(MatDialog);
  })

  beforeEach(async () => {
    fixture = await TestBed.createComponent(ServerManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open AddServerDialog and reload servers when openAddServer is called', () => {
    // Mock for the MatDialog open method
    const dialogRefSpyObj = jasmine.createSpyObj({afterClosed: of({}), close: null});
    spyOn(matDialog, 'open').and.returnValue(dialogRefSpyObj);

    // Mock for the loadServer method
    const loadServerSpy = spyOn(component.dataSource, 'loadServers');

    // Call the method
    component.openAddServer();

    // Expect that MatDialog open method was called with the correct parameters
    expect(matDialog.open).toHaveBeenCalledWith(AddServerDialogComponent, {
      width: '500px',
      data: {customName: '', endpointUrl: ''},
      disableClose: true
    });
    // Expect that the server has been loaded correctly
    expect(loadServerSpy).toHaveBeenCalled()
  });

  it('should open RemoveServerDialog and reload servers when removeServer is called', () => {
    // Mock for the MatDialog open method
    const dialogRefSpyObj = jasmine.createSpyObj({afterClosed: of({}), close: null});
    spyOn(matDialog, 'open').and.returnValue(dialogRefSpyObj);
    const deleteServerSpy = spyOn(component.serverService, 'deleteTrainingServer').and.returnValue(of(<TrainingServer[]>[]))

    // Mock for the loadServer method
    const loadServerSpy = spyOn(component.dataSource, 'loadServers');

    // Mock dummy server
    const server = <TrainingServer>{
      id: 0,
      selected: true,
      inEditing: false,
      name: "test",
      endpoint: "http://localhost",
    }

    // Call the method
    component.removeServer(server);

    // Expect that MatDialog open method was called with the correct parameters
    expect(matDialog.open).toHaveBeenCalledWith(RemoveServerDialogComponent, {
      width: '500px',
      data: server,
      disableClose: true
    });
    // Expect that the server has been loaded correctly
    expect(deleteServerSpy).toHaveBeenCalledWith(server)
    expect(loadServerSpy).toHaveBeenCalled()
  });

  it('should show error toaster when server deletion fails', () => {
    // Mock for the MatDialog open method
    const dialogRefSpyObj = jasmine.createSpyObj({afterClosed: of({}), close: null});
    spyOn(matDialog, 'open').and.returnValue(dialogRefSpyObj);

    // Mock for the deleteTrainingServer method to return an error Observable
    const deleteServerSpy = spyOn(component.serverService, 'deleteTrainingServer').and.returnValue(throwError('Delete server failed'));

    // Mock for the error method of the toasterService
    const toasterErrorSpy = spyOn(component.toasterService, 'error');

    // Mock Dummy Server
    const server = <TrainingServer>{
      id: 0,
      selected: true,
      inEditing: false,
      name: "test",
      endpoint: "http://localhost",
    };

    // Call the method
    component.removeServer(server);

    // Expect that MatDialog open method was called with the correct parameters
    expect(matDialog.open).toHaveBeenCalledWith(RemoveServerDialogComponent, {
      width: '500px',
      data: server,
      disableClose: true
    });

    // // Expect that deleteTrainingServer method was called with the server
    expect(deleteServerSpy).toHaveBeenCalledWith(server);

    // // Expect that the error is shown in the toaster service
    expect(toasterErrorSpy).toHaveBeenCalledWith(`Server ${server.name} could not be deleted.`, 'Failed to remove Server');
  });
});
