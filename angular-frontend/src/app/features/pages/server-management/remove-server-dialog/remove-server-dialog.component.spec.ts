/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {
  RemoveServerDialogComponent
} from 'src/app/features/pages/server-management/remove-server-dialog/remove-server-dialog.component';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

describe('DeleteServerDialogComponent', () => {
  let component: RemoveServerDialogComponent;
  let fixture: ComponentFixture<RemoveServerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RemoveServerDialogComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(RemoveServerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
