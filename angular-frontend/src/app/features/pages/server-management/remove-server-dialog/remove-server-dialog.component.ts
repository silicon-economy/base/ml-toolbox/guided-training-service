/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

/**
 * Dialog component for confirming the removal of a training server.
 */
@Component({
  selector: 'app-remove-server-dialog',
  templateUrl: './remove-server-dialog.component.html',
  styleUrls: ['./remove-server-dialog.component.scss']
})
export class RemoveServerDialogComponent {
  /**
   * Constructor that injects data into the dialog.
   * @param data The data for the server to be removed, injected via `MAT_DIALOG_DATA`.
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}
