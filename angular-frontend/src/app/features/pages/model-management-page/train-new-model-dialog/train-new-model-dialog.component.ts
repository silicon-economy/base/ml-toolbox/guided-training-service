/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {ARCHITECTURES, ModelManagementService} from 'src/app/core/backend/model-management.service';
import {MatChipInputEvent} from '@angular/material/chips';
import {finalize} from 'rxjs';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {MenuItem} from 'src/app/core/models/item';
import {DataManagementService} from 'src/app/core/backend/data-management.service';
import {Dataset} from 'src/app/core/models/dataset';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {environment} from 'src/environments/environment';
import {TrainingServer} from 'src/app/core/models/trainingserver';
import {TrainingServerService} from 'src/app/core/backend/training-server.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import * as ace from "ace-builds";
import {HttpClient} from '@angular/common/http';

/**
 * Represents an object for chip rows.
 *
 * @property {string} value - The value of the chip.
 * @property {string} styleClass - The CSS class applied to the chip.
 */
export interface ChipRowObject {
  value: string;
  styleClass: string;
}

/**
 * Dialog component for training a new model.
 * Handles the form and interaction logic for setting up a new model training.
 */
@Component({
  selector: 'app-train-new-model-dialog',
  templateUrl: './train-new-model-dialog.component.html',
  styleUrls: ['./train-new-model-dialog.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class TrainNewModelDialogComponent implements OnInit, AfterViewInit {
  contentHeight = '30vh';
  contentWidth = '60%';
  contentTop = '';
  contentLeft = '';
  contentTransform = '';
  toggleSizeIcon = 'zoom_out_map';
  advanced = false;
  modelConfig: string = '';
  //DB-Data
  datasets: Dataset[] = [];
  applicationArchitectureMenuItems: MenuItem[] = [];
  //FormGroups
  basicInfoGroup = new FormGroup({
    modelName: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(200),
      Validators.pattern(/^\S(.*\S)?$/),
    ]),
    modelNameDescription: new FormControl('', [
      Validators.minLength(1),
      Validators.maxLength(200)
    ]),
    modelApplication: new FormControl('', [
      Validators.required
    ]),
    modelArchitecture: new FormControl('', [
      Validators.required
    ]),
    tagControl: new FormControl('', [])
  });
  datasetParameterGroup = new FormGroup({
    trainDataSetCtrl: new FormControl([], [
      Validators.required,
    ]),
    evalDataSetCtrl: new FormControl([], [
      Validators.required,
    ]),
    classesControl: new FormControl('', [])
  });
  trainingParameterGroup = new FormGroup({
    epochsControl: new FormControl('', [
      this.requiredValidator.bind(this),
      Validators.pattern('^(?:[1-9]\\d{0,2}|1000)$')
    ]),
    batchSizeControl: new FormControl('', [
      this.requiredValidator.bind(this),
      Validators.pattern('^(?:[1-9]\\d{0,2}|1000)$')
    ])
  });
  trainingServerGroup = new FormGroup({
    ctrlServerNameChange: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(200)
    ]),
    ctrlServerEndpointChange: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(200),
      Validators.pattern('^(http|https)://.*$')
    ])
  });
  //Training Server Table
  selectedTrainingServer: TrainingServer = null;
  displayedTrainingServiceColumns = ['selected', 'name', 'endpoint'];
  trainingServer: TrainingServer[];
  trainingServerName = '';
  trainingsServerEndpoint = '';
  //Mat-Chip
  separatorKeysCodes: number[] = [ENTER, COMMA];
  autoCompleteClasses: string[] = [];
  chipRowClasses: ChipRowObject[] = [];
  chipRowTag: ChipRowObject[] = [];

  aceEditor: ace.Ace.Editor;

  // TODO: Move this to service
  private readonly backendUrl: string = `${environment.apiHost}:${environment.apiPort}/training/`;
  startingTraining = false;
  selectedTrainingDatasets: Dataset[] = [];
  selectedTrainingDatasetsClasses: string[] = [];
  selectedEvaluationDatasets: Dataset[] = [];
  removedClasses: string[] = [];
  trainLabelStudioProjectIds: number[] = [];
  evalLabelStudioProjectIds: number[] = [];

  constructor(
    public dialogRef: MatDialogRef<TrainNewModelDialogComponent>,
    public modelManagementService: ModelManagementService,
    public dataManagementService: DataManagementService,
    public trainingServerService: TrainingServerService,
    public toasterService: ToasterService,
    private http: HttpClient
  ) {
  }

  /**
   * Initializes the dialog with default data.
   * Fetches datasets and training server information.
   */
  ngOnInit(): void {
    this.dataManagementService.getDatasets('', 'lastModified', 'desc', 0, 50).subscribe(res => {
      this.datasets = this.dataManagementService.sortDatasetList(res.datasets);
    });
    this.applicationArchitectureMenuItems = ARCHITECTURES;
    this.loadTrainingServer();
  }

  @ViewChild("editor", {static: true}) private editor: ElementRef<HTMLElement>;

  /**
   * Sets up the ACE editor for YAML configuration.
   */
  ngAfterViewInit(): void {
    ace.config.set("fontSize", "14px");
    ace.config.set('basePath', 'https://unpkg.com/ace-builds@1.4.12/src-noconflict');

    this.aceEditor = ace.edit(this.editor.nativeElement);

    this.aceEditor.setTheme('ace/theme/github');
    this.aceEditor.session.setMode('ace/mode/yaml');
  }

  /**
   * Toggles the dialog size between default and expanded.
   */
  toggleWindowSize() {
    if (this.contentHeight === '30vh') {
      this.contentHeight = '90%';
      this.contentWidth = '99%';
      this.contentTop = '50%';
      this.contentLeft = '50%';
      this.contentTransform = 'translate(-50%, -54%)';
      this.toggleSizeIcon = 'zoom_in_map';
    } else {
      this.contentHeight = '30vh';
      this.contentWidth = '60%';
      this.contentTop = '';
      this.contentLeft = '';
      this.contentTransform = '';
      this.toggleSizeIcon = 'zoom_out_map';
    }
  }

  /**
   * Resets the text editor to its original state
   */
  resetAdvancedMode() {
    this.aceEditor.session.setValue(this.modelConfig);
  }

  /**
   * Retrieves the default advanced mode config for a given model and sets it in the text editor
   * 
   * @param event The event that called the method
   */
  onAdvancedToggle(event: any) {
    // If advanced mode is toggled, include advanced configuration
    if (event.checked) {
      this.advanced = true;

      // Collect dataset and classes info
      this.trainLabelStudioProjectIds = [];
      this.selectedTrainingDatasets.forEach(dts => this.trainLabelStudioProjectIds.push(dts.labelStudioProjectId));

      this.evalLabelStudioProjectIds = [];
      this.selectedEvaluationDatasets.forEach(dts => this.evalLabelStudioProjectIds.push(dts.labelStudioProjectId));

      const selectedClasses = this.chipRowClasses.map(chip => chip.value);

      // Call the backend API to get a template with advanced configuration
      this.modelManagementService.retrieveModelConfig(
        this.basicInfoGroup.controls.modelArchitecture.value,
        this.basicInfoGroup.controls.modelName.value,
        this.trainLabelStudioProjectIds,
        this.evalLabelStudioProjectIds,
        Number(this.trainingParameterGroup.controls.epochsControl.value),
        Number(this.trainingParameterGroup.controls.batchSizeControl.value),
        selectedClasses,
        this.basicInfoGroup.controls.modelNameDescription.value
      ).subscribe({
        next: response => {
          this.modelConfig = JSON.stringify(response, null, 2);
          this.aceEditor.session.setValue(this.modelConfig);
        },
        error: () => {
          // Return an error message and close advanced mode automatically
          this.toasterService.error('Error creating template with advanced configuration');
          this.advanced = false;
        }
      });
    }
  }

  /**
   * Custom validator to enforce field requirement based on advanced-checkbox status.
   * If advanced mode is disabled and the control value is empty, it returns a required error.
   *
   * @param control The form control to validate.
   *
   * @returns Validation error if the field is empty and advanced mode is off, otherwise null.
   */
  requiredValidator(control: FormControl) {
    if (!this.advanced && !control.value) {
      return {required: true};
    }
    return null;
  }

  /**
   * Handles the selection of training datasets. Updates selected datasets and corresponding classes.
   *
   * @param datasets The selected datasets to update the list.
   */
  trainingDatasetSelected(datasets: Dataset[]) {
    const remove_datasets = this.selectedTrainingDatasets.filter(dts => !datasets.includes(dts));
    const add_datasets = datasets.filter(dts => !this.selectedTrainingDatasets.includes(dts));
    this.selectedTrainingDatasets = datasets;

    // Remove classes associated with removed datasets.
    for (const dataset of remove_datasets) {
      for (const cls of dataset.classes) {
        const idx = this.selectedTrainingDatasetsClasses.indexOf(cls);
        if (idx !== -1) {
          this.selectedTrainingDatasetsClasses.splice(idx, 1);
        }
      }
    }

    // Add classes from newly selected datasets and handle the default selection behavior.
    for (const dataset of add_datasets) {
      this.selectedTrainingDatasetsClasses.push(...dataset.classes);
      for (const cls of dataset.classes) {
        // Default: All classes from a newly selected dataset get selected (if they have not been manually removed).
        if (this.chipRowClasses.filter(chip => chip.value === cls).length === 0 && this.removedClasses.indexOf(cls) === -1) {
          this.chipRowClasses.push({
            value: cls,
            styleClass: 'orange-chip'
          });
        }
      }
    }

    // Filter duplicates for the auto-complete list.
    this.autoCompleteClasses = this.selectedTrainingDatasetsClasses.filter((item, idx, self) => (idx === self.indexOf(item)));

    // Remove classes from the selected list that got removed by removing their dataset.
    this.chipRowClasses = this.chipRowClasses.filter(chip => {
      return this.selectedTrainingDatasetsClasses.indexOf(chip.value) !== -1;
    });
  }

  /**
   * Handles the selection of evaluation datasets.
   *
   * @param datasets The selected evaluation datasets.
   */
  evaluationDatasetSelected(datasets: Dataset[]) {
    this.selectedEvaluationDatasets = datasets;
  }

  /**
   * Loads the training server data by making an API call.
   * Initializes the training server list and resets the selection.
   */
  loadTrainingServer(): void {
    this.trainingServerService.getAllTrainingsServers().subscribe({
      next: (res) => {
        this.selectedTrainingServer = null;
        this.trainingServerName = '';
        this.trainingsServerEndpoint = '';
        this.trainingServerGroup.reset();
        this.trainingServer = res;
      }, error: () => {
        this.toasterService.error('An error occurred while loading the training servers.', 'Failed to load server');
      }
    })
  }

  /**
   * Handles the selection of a specific training server from the list.
   *
   * @param selection The training server selected by the user.
   */
  trainingServerSelected(selection: TrainingServer): void {
    let newSelection = false;
    this.trainingServer.forEach((server: TrainingServer) => {
      if (!selection.selected && selection.name == server.name && selection.endpoint == server.endpoint) {
        server.selected = true;
        this.selectedTrainingServer = server;
        this.trainingServerName = server.name;
        this.trainingsServerEndpoint = server.endpoint;
        newSelection = true;
      } else {
        server.selected = false;
      }
    });
    if (!newSelection) {
      this.selectedTrainingServer = null;
      this.trainingServerName = '';
      this.trainingsServerEndpoint = '';
    }
  }

  /**
   * Starts the training process with the provided parameters.
   * Sends a request to start the training job and shows appropriate messages on success or failure.
   */
  startTraining() {
    this.startingTraining = true;
    const classArray: string[] = [];
    this.chipRowClasses.forEach(classes => classArray.push(classes.value));
    this.trainLabelStudioProjectIds = [];
    this.selectedTrainingDatasets.forEach(dts => this.trainLabelStudioProjectIds.push(dts.labelStudioProjectId));
    this.evalLabelStudioProjectIds = [];
    this.selectedEvaluationDatasets.forEach(dts => this.evalLabelStudioProjectIds.push(dts.labelStudioProjectId));

    let modelConfig = "";
    if (this.advanced === true) {
      modelConfig = this.aceEditor.getValue();
    }

    this.modelManagementService.startTraining(
      Number(this.trainingParameterGroup.controls.epochsControl.value),
      this.trainLabelStudioProjectIds,
      this.evalLabelStudioProjectIds,
      this.basicInfoGroup.controls.modelArchitecture.value,
      this.basicInfoGroup.controls.modelName.value,
      this.basicInfoGroup.controls.modelNameDescription.value,
      classArray,
      Number(this.trainingParameterGroup.controls.batchSizeControl.value),
      modelConfig,
      this.selectedTrainingServer.endpoint,
      this.backendUrl,
    ).pipe(finalize(() => {
      this.startingTraining = false;
    }))
      .subscribe({
        next: trainingJob => {
          this.toasterService.success(`The training job '${trainingJob.id}' was started successfully.`, 'Training started');
          this.dialogRef.close();
        },
        error: () => this.toasterService.error('Failed to start the training job.', 'Training error')
      });
  }

  /**
   * Removes a tag from the list of tags.
   *
   * @param tag The tag to remove.
   */
  removeTag(tag: string) {
    this.chipRowTag.forEach((chipTag, index) => {
      if (chipTag.value === tag) {
        this.chipRowTag.splice(index, 1);
      }
    });
  }

  /**
   * Adds a tag to the list of tags if it doesn't already exist.
   *
   * @param $event The event triggered when a new tag is added.
   */
  addTag($event: MatChipInputEvent) {
    if (this.chipRowTag.filter(tag => tag.value === $event.value).length === 0) {
      this.chipRowTag.push({
        value: $event.value,
        styleClass: 'orange-chip'
      });
      $event.chipInput.clear();
    }
  }

  /**
   * Adds a class to the list of selected classes.
   *
   * @param $event The event triggered when a new class is added.
   */
  addClass($event: MatChipInputEvent): void {
    const removeIdx = this.removedClasses.indexOf($event.value);
    if (removeIdx !== -1) {
      this.removedClasses.splice(removeIdx, 1);
    }
    if (this.chipRowClasses.filter(className => className.value === $event.value).length === 0) {
      this.chipRowClasses.push({
        value: $event.value,
        styleClass: 'orange-chip'
      });
      $event.chipInput.clear();
    }
  }

  /**
   * Removes a class from the list of selected classes.
   *
   * @param className The name of the class to remove.
   */
  removeClass(className: string) {
    this.removedClasses.push(className);
    this.chipRowClasses.forEach((classNameChip, index) => {
      if (classNameChip.value === className) {
        this.chipRowClasses.splice(index, 1);
      }
    });
  }

  /**
   * Handles the selection of a class from the auto-complete list.
   *
   * @param event The selection event triggered when a class is selected from the list.
   */
  onClassSelected(event: MatAutocompleteSelectedEvent): void {
    const selectedClass = event.option.viewValue;
    const removeIdx = this.removedClasses.indexOf(selectedClass);
    if (removeIdx !== -1) {
      this.removedClasses.splice(removeIdx, 1);
    }
    if (this.chipRowClasses.filter(className => className.value === selectedClass).length === 0) {
      this.chipRowClasses.push({
        value: selectedClass,
        styleClass: 'orange-chip'
      });
    }
  }
}
