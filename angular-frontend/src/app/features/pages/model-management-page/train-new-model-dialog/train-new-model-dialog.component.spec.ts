/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule, FormsModule, FormGroup} from '@angular/forms';
import {MatAutocompleteModule, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent, MatChipsModule} from '@angular/material/chips';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatOptionModule} from '@angular/material/core';
import {ModelManagementService} from 'src/app/core/backend/model-management.service';

import {
  TrainNewModelDialogComponent
} from 'src/app/features/pages/model-management-page/train-new-model-dialog/train-new-model-dialog.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {Dataset} from 'src/app/core/models/dataset';
import {of, Subject, throwError} from 'rxjs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {TrainingServerService} from 'src/app/core/backend/training-server.service';
import {TrainingServer} from 'src/app/core/models/trainingserver';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {TrainingJobSubmitted} from 'src/app/core/models/trainingjob';
import {environment} from 'src/environments/environment';
import {DataManagementService, DatasetResponse} from "src/app/core/backend/data-management.service";

import * as ace from 'ace-builds';

const DUMMY_SERVER: TrainingServer[] = [
  {
    id: 1,
    name: 'server1',
    selected: false,
    endpoint: 'server1:8080',
    inEditing: false
  },
  {
    id: 2,
    name: 'server2',
    selected: false,
    endpoint: 'server2:8080',
    inEditing: false
  },
  {
    id: 3,
    name: 'notValid',
    selected: false,
    endpoint: '',
    inEditing: false
  }
];

const DUMMY_DATASETS: Dataset[] = [
  {
    labelStudioProjectId: 123,
    name: 'Test Dataset 1',
    datasetType: {
      value: 'Value',
      viewValue: 'ViewValue'
    },
    annotatedImages: 10,
    totalImages: 100,
    lastModified: new Date(),
    previewImages: ['image1.jpg', 'image2.jpg'],
    tags: [
      {
        name: 'tag',
        colorClass: 'orange-chip'
      }],
    classes: ['Class A', 'Class B', 'Class C'],
    labelStudioUrl: "",
  },
  {
    labelStudioProjectId: 456,
    name: 'Test Dataset 2',
    datasetType: {
      value: 'Value',
      viewValue: 'ViewValue'
    },
    annotatedImages: 10,
    totalImages: 100,
    lastModified: new Date(),
    previewImages: ['image1.jpg', 'image2.jpg'],
    tags: [
      {
        name: 'tag',
        colorClass: 'orange-chip'
      }],
    classes: ['Class C', 'Class D', 'Class E'],
    labelStudioUrl: "",
  },
];

describe('TrainNewModelDialogComponent', () => {
  let component: TrainNewModelDialogComponent;
  let fixture: ComponentFixture<TrainNewModelDialogComponent>;
  let modelManagementServiceSpy: jasmine.SpyObj<ModelManagementService>;
  let dataManagementService: DataManagementService;
  let trainingServerServiceSpy: jasmine.SpyObj<TrainingServerService>;
  beforeEach(async () => {
    modelManagementServiceSpy = jasmine.createSpyObj<ModelManagementService>('ModelManagementServiceSpy',
      ['getAllLabelTags', 'startTraining', 'retrieveModelConfig']
    );
    trainingServerServiceSpy = jasmine.createSpyObj<TrainingServerService>('TrainingServerServiceSpy',
      ['getAllTrainingsServers']
    );
    trainingServerServiceSpy.getAllTrainingsServers.and.returnValue(of([]));
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatDialogModule,
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatInputModule,
        MatTooltipModule,
        MatSelectModule,
        MatCheckboxModule,
        MatStepperModule,
        MatOptionModule,
        MatInputModule,
        MatTableModule,
        MatSlideToggleModule,
        MatIconModule,
        MatCardModule,
        MatDividerModule,
        MatListModule,
        HttpClientTestingModule
      ],
      declarations: [TrainNewModelDialogComponent],
      providers: [
        ToasterService,
        {provide: MAT_DIALOG_DATA, useValue: {}},
        {
          provide: MatDialogRef, useValue: {
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            close: () => {
            }
          }
        },
        {
          provide: ModelManagementService,
          useValue: modelManagementServiceSpy
        },
        {
          provide: DataManagementService
        },
        {
          provide: TrainingServerService,
          useValue: trainingServerServiceSpy
        }
      ]
    }).compileComponents();
    dataManagementService = TestBed.inject(DataManagementService);
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(TrainNewModelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('multiple datasets selected', () => {
    component.trainingDatasetSelected(DUMMY_DATASETS);

    expect(component.selectedTrainingDatasets).toEqual(DUMMY_DATASETS);
    expect(component.selectedTrainingDatasetsClasses).toEqual(['Class A', 'Class B', 'Class C', 'Class C', 'Class D', 'Class E']);
    expect(component.autoCompleteClasses).toEqual(['Class A', 'Class B', 'Class C', 'Class D', 'Class E']);
    expect(component.chipRowClasses).toEqual([
      {value: 'Class A', styleClass: 'orange-chip'},
      {value: 'Class B', styleClass: 'orange-chip'},
      {value: 'Class C', styleClass: 'orange-chip'},
      {value: 'Class D', styleClass: 'orange-chip'},
      {value: 'Class E', styleClass: 'orange-chip'},
    ]);
  });

  it('should remove classes from deselected dataset', () => {
    component.trainingDatasetSelected(DUMMY_DATASETS);

    expect(component.selectedTrainingDatasets).toEqual(DUMMY_DATASETS);
    expect(component.selectedTrainingDatasetsClasses).toEqual(['Class A', 'Class B', 'Class C', 'Class C', 'Class D', 'Class E']);
    expect(component.autoCompleteClasses).toEqual(['Class A', 'Class B', 'Class C', 'Class D', 'Class E']);
    expect(component.chipRowClasses).toEqual([
      {value: 'Class A', styleClass: 'orange-chip'},
      {value: 'Class B', styleClass: 'orange-chip'},
      {value: 'Class C', styleClass: 'orange-chip'},
      {value: 'Class D', styleClass: 'orange-chip'},
      {value: 'Class E', styleClass: 'orange-chip'},
    ]);

    component.trainingDatasetSelected([DUMMY_DATASETS[0]]);

    expect(component.selectedTrainingDatasets).toEqual([DUMMY_DATASETS[0]]);
    expect(component.selectedTrainingDatasetsClasses).toEqual(['Class A', 'Class B', 'Class C']);
    expect(component.autoCompleteClasses).toEqual(['Class A', 'Class B', 'Class C']);
    expect(component.chipRowClasses).toEqual([
      {value: 'Class A', styleClass: 'orange-chip'},
      {value: 'Class B', styleClass: 'orange-chip'},
      {value: 'Class C', styleClass: 'orange-chip'},
    ]);
  });

  it('should sort datasets by datasetType.value and then by name', () => {
    const datasets: Dataset[] = [
      {
        labelStudioProjectId: 1,
        name: 'Dataset B',
        datasetType: {value: 'Type1', viewValue: 'Type1'},
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      },
      {
        labelStudioProjectId: 2,
        name: 'Dataset A',
        datasetType: {value: 'Type2', viewValue: 'Type2'},
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      },
      {
        labelStudioProjectId: 3,
        name: 'Dataset C',
        datasetType: {value: 'Type1', viewValue: 'Type1'},
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      },
      {
        labelStudioProjectId: 4,
        name: 'Dataset A',
        datasetType: {value: 'Type1', viewValue: 'Type1'},
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      }
    ];

    // Mock the service to return the test datasets
    const testDatasetsResponse: DatasetResponse = {
      totalDatasets: 50,
      datasets: datasets
    };
    spyOn(dataManagementService, 'getDatasets').and.returnValue(of(testDatasetsResponse));

    // Trigger the component's ngOnInit or any method that fetches datasets
    component.ngOnInit();

    // Validate sorting by `datasetType.value` and `name`
    expect(component.datasets[0].datasetType.value).toBe('Type1');
    expect(component.datasets[0].name).toBe('Dataset A');
    expect(component.datasets[1].datasetType.value).toBe('Type1');
    expect(component.datasets[1].name).toBe('Dataset B');
    expect(component.datasets[2].datasetType.value).toBe('Type1');
    expect(component.datasets[2].name).toBe('Dataset C');
    expect(component.datasets[3].datasetType.value).toBe('Type2');
    expect(component.datasets[3].name).toBe('Dataset A');
  });

  describe('basicInfoGroup', () => {
    let form: FormGroup;
    beforeEach(() => {
      form = component.basicInfoGroup;
    });

    it('should be valid when all fields are filled correctly', () => {
      form.setValue({
        modelName: 'Test',
        modelNameDescription: 'Description',
        modelApplication: 'Application',
        modelArchitecture: 'Architecture',
        tagControl: ''
      });
      expect(form.valid).toBeTruthy();
    });

    it('should be invalid when required fields are empty', () => {
      form.setValue({
        modelName: '',
        modelNameDescription: '',
        modelApplication: '',
        modelArchitecture: '',
        tagControl: ''
      });
      expect(form.valid).toBeFalsy();
    });

    it('should be invalid when fields are too long', () => {
      form.setValue({
        modelName: 'a'.repeat(201),
        modelNameDescription: 'b'.repeat(201),
        modelApplication: 'c'.repeat(201),
        modelArchitecture: 'd'.repeat(201),
        tagControl: ''
      });
      expect(form.valid).toBeFalsy();
    });

    it('should validate modelName form control correctly', () => {
      const control = component.basicInfoGroup.get('modelName');

      // Test: Empty value (should be invalid due to required validation)
      control.setValue('');
      expect(control.valid).toBeFalsy();
      expect(control.hasError('required')).toBeTruthy();

      // Test: Value with only spaces (should be invalid due to pattern validation)
      control.setValue(' ');
      expect(control.valid).toBeFalsy();
      expect(control.hasError('pattern')).toBeTruthy();

      // Test: Value shorter than minLength (should be valid since minLength is 1)
      control.setValue('A');
      expect(control.valid).toBeTruthy();
      expect(control.hasError('minlength')).toBeFalsy();

      // Test: Value with trailing space (should be invalid due to pattern validation)
      control.setValue('ModelName ');
      expect(control.valid).toBeFalsy();
      expect(control.hasError('pattern')).toBeTruthy();

      // Test: Value with leading space (should be invalid due to pattern validation)
      control.setValue(' ModelName');
      expect(control.valid).toBeFalsy();
      expect(control.hasError('pattern')).toBeTruthy();

      // Test: Valid value within length limits and without leading/trailing spaces
      control.setValue('ValidModelName');
      expect(control.valid).toBeTruthy();
      expect(control.hasError('required')).toBeFalsy();
      expect(control.hasError('minlength')).toBeFalsy();
      expect(control.hasError('maxlength')).toBeFalsy();
      expect(control.hasError('pattern')).toBeFalsy();
    });

  });

  describe('datasetParameterGroup', () => {
    let form: FormGroup;
    beforeEach(() => {
      form = component.datasetParameterGroup;
    });
    it('should be valid when all fields are filled correctly', () => {
      form.setValue({
        trainDataSetCtrl: ['Test'],
        evalDataSetCtrl: ['Test'],
        classesControl: 'Description'
      });
      expect(form.valid).toBeTruthy();
    });

    it('should be invalid when required fields are empty', () => {
      form.setValue({
        trainDataSetCtrl: '',
        evalDataSetCtrl: '',
        classesControl: ''
      });
      expect(form.valid).toBeFalsy();
    });
  });

  describe('trainingParameterGroup', () => {
    let form: FormGroup;
    beforeEach(() => {
      form = component.trainingParameterGroup;
    });
    it('should be valid when all fields are filled correctly', () => {
      form.setValue({
        epochsControl: '1',
        batchSizeControl: '1'
      });
      expect(form.valid).toBeTruthy();
    });

    it('should be invalid when required fields are empty', () => {
      form.setValue({
        epochsControl: '',
        batchSizeControl: ''
      });
      expect(form.valid).toBeFalsy();
    });

    it('should be invalid when required fields are char', () => {
      form.setValue({
        epochsControl: 'a',
        batchSizeControl: 'b'
      });
      expect(form.valid).toBeFalsy();
    });

    it('should be invalid when fields are too long', () => {
      form.setValue({
        epochsControl: '100000000000',
        batchSizeControl: '100000000000'
      });
      expect(form.valid).toBeFalsy();
    });

    it('should reset advanced mode and set aceEditor session value', () => {
      // Mock aceEditor and its session with type assertion
      component.aceEditor = {
        session: {
          setValue: jasmine.createSpy('setValue'),
        } as unknown as ace.Ace.EditSession // Type assertion for the session
      } as unknown as ace.Ace.Editor; // Type assertion for the editor

      // Set the modelConfig property to test value
      component.modelConfig = 'test config';

      // Call the method
      component.resetAdvancedMode();

      // Expect setValue to have been called with the correct value
      expect(component.aceEditor.session.setValue).toHaveBeenCalledWith('test config');
    });
  });

  describe('Matchip-Row', () => {

    it('Add Class', () => {
      component.chipRowClasses.push({value: 'class1', styleClass: 'orange-chip'});
      component.removedClasses.push('new-class');
      expect(component.chipRowClasses.length).toEqual(1);
      expect(component.removedClasses.length).toEqual(1);
      const event = {
        value: 'new-class', chipInput: {
          clear: () => {
            //
          }
        }
      } as MatChipInputEvent;
      component.addClass(event);
      expect(component.chipRowClasses.length).toEqual(2);
      expect(component.removedClasses.length).toEqual(0);
    });

    it('Remove Class', () => {
      component.chipRowClasses.push({value: 'class1', styleClass: 'orange-chip'});
      component.chipRowClasses.push({value: 'class2', styleClass: 'orange-chip'});
      expect(component.chipRowClasses.length).toEqual(2);
      expect(component.removedClasses.length).toEqual(0);
      component.removeClass('class1');
      expect(component.chipRowClasses.length).toEqual(1);
      expect(component.removedClasses.length).toEqual(1);
    });

    it('should add selected class to chipRowClasses array', () => {
      component.removedClasses.push('selectedClass');
      expect(component.chipRowClasses.length).toEqual(0);
      expect(component.removedClasses.length).toEqual(1);
      const event: MatAutocompleteSelectedEvent = {
        option: {
          viewValue: 'selectedClass'
        }
      } as MatAutocompleteSelectedEvent;
      component.onClassSelected(event);
      expect(component.chipRowClasses.length).toEqual(1);
      expect(component.removedClasses.length).toEqual(0);
    });

    it('Add Tag', () => {
      component.chipRowTag.push({value: 'tag1', styleClass: 'orange-chip'});
      expect(component.chipRowTag.length).toEqual(1);
      const event = {
        value: 'new-tag', chipInput: {
          clear: () => {
            //
          }
        }
      } as MatChipInputEvent;
      component.addTag(event);
      expect(component.chipRowTag.length).toEqual(2);
    });

    it('Remove Tag', () => {
      component.chipRowTag.push({value: 'tag1', styleClass: 'orange-chip'});
      component.chipRowTag.push({value: 'tag2', styleClass: 'orange-chip'});
      expect(component.chipRowTag.length).toEqual(2);
      component.removeTag('tag1');
      expect(component.chipRowTag.length).toEqual(1);
    });

  });

  describe('Management Service', () => {

    beforeEach(() => {
      const dummyDataset = <Dataset>{
        labelStudioProjectId: 12
      };
      component.trainingParameterGroup.controls.epochsControl.setValue('20');
      component.datasetParameterGroup.get('trainDataSetCtrl').setValue([String(dummyDataset.labelStudioProjectId)]);
      component.datasetParameterGroup.get('evalDataSetCtrl').setValue([String(dummyDataset.labelStudioProjectId)]);
      component.basicInfoGroup.controls.modelArchitecture.setValue('yolox_tiny');
      component.basicInfoGroup.controls.modelName.setValue('Testing object detection model');
      component.basicInfoGroup.controls.modelNameDescription.setValue('A model for unit testing');
      component.chipRowClasses = [];
      component.trainingParameterGroup.controls.batchSizeControl.setValue('16');
      component.selectedTrainingServer = DUMMY_SERVER[0];
      component.advanced = false;
      component.modelConfig = '';
      spyOn(component.dialogRef, 'close');
    });

    it('should display a loading spinner while starting the training job and close the dialog on success and display a success toast',
      inject([ToasterService], (toasterService: ToasterService) => {
        const toastSucessSpy = spyOn(toasterService, 'success');
        const dummyJob = <TrainingJobSubmitted>{
          id: 'job-1',
          message: 'Training started!'
        };
        const testSubject = new Subject<TrainingJobSubmitted>();
        modelManagementServiceSpy.startTraining.and.returnValue(testSubject);
        component.startTraining();
        expect(component.startingTraining).withContext('The component should be waiting for a response now').toBeTrue();
        expect(modelManagementServiceSpy.startTraining).toHaveBeenCalledOnceWith(
          20,
          [],
          [],
          'yolox_tiny',
          'Testing object detection model',
          'A model for unit testing',
          [],
          16,
          '',
          DUMMY_SERVER[0].endpoint,
          `${environment.apiHost}:${environment.apiPort}/training/`,
        );
        testSubject.next(dummyJob);
        testSubject.complete();
        expect(component.startingTraining).withContext('The component should no longer wait for a response').toBeFalse();
        expect(component.dialogRef.close).toHaveBeenCalled();
        expect(toastSucessSpy).toHaveBeenCalledOnceWith(`The training job '${dummyJob.id}' was started successfully.`, 'Training started');
      }));

    it('should display a loading spinner while starting the training job in advanced mode and close the dialog on success and display a success toast',
      inject([ToasterService], (toasterService: ToasterService) => {
        component.advanced = true;
        component.aceEditor.session.setValue('{"test_entry": "test_value"}');

        const toastSucessSpy = spyOn(toasterService, 'success');
        const dummyJob = <TrainingJobSubmitted>{
          id: 'job-1',
          message: 'Training started!'
        };
        const testSubject = new Subject<TrainingJobSubmitted>();
        modelManagementServiceSpy.startTraining.and.returnValue(testSubject);
        component.startTraining();
        expect(component.startingTraining).withContext('The component should be waiting for a response now').toBeTrue();
        expect(modelManagementServiceSpy.startTraining).toHaveBeenCalledOnceWith(
          20,
          [],
          [],
          'yolox_tiny',
          'Testing object detection model',
          'A model for unit testing',
          [],
          16,
          '{"test_entry": "test_value"}',
          DUMMY_SERVER[0].endpoint,
          `${environment.apiHost}:${environment.apiPort}/training/`,
        );
        testSubject.next(dummyJob);
        testSubject.complete();
        expect(component.startingTraining).withContext('The component should no longer wait for a response').toBeFalse();
        expect(component.dialogRef.close).toHaveBeenCalled();
        expect(toastSucessSpy).toHaveBeenCalledOnceWith(`The training job '${dummyJob.id}' was started successfully.`, 'Training started');
      }));

    it('should keep the dialog open on error and display an error toast', inject([ToasterService], (toasterService: ToasterService) => {
      const toastErrorSpy = spyOn(toasterService, 'error');
      modelManagementServiceSpy.startTraining.and
        .returnValue(throwError(() => new Error('Something went wrong while starting the training.')));
      component.startTraining();
      expect(modelManagementServiceSpy.startTraining).toHaveBeenCalled();
      expect(component.dialogRef.close).toHaveBeenCalledTimes(0);
      expect(toastErrorSpy).toHaveBeenCalledOnceWith('Failed to start the training job.', 'Training error');
      expect(component.startingTraining).withContext('The component should no longer wait for a response').toBeFalse();
    }));

    it('should toggle window size correctly', () => {
      // Initial state
      expect(component.contentHeight).toBe('30vh');
      expect(component.contentWidth).toBe('60%');
      expect(component.contentTop).toBe('');
      expect(component.contentLeft).toBe('');
      expect(component.contentTransform).toBe('');
      expect(component.toggleSizeIcon).toBe('zoom_out_map');

      // Call toggleWindowSize() to change state
      component.toggleWindowSize();

      // Check if state has changed correctly
      expect(component.contentHeight).toBe('90%');
      expect(component.contentWidth).toBe('99%');
      expect(component.contentTop).toBe('50%');
      expect(component.contentLeft).toBe('50%');
      expect(component.contentTransform).toBe('translate(-50%, -54%)');
      expect(component.toggleSizeIcon).toBe('zoom_in_map');

      // Call toggleWindowSize() again to revert state
      component.toggleWindowSize();

      // Check if state has reverted correctly
      expect(component.contentHeight).toBe('30vh');
      expect(component.contentWidth).toBe('60%');
      expect(component.contentTop).toBe('');
      expect(component.contentLeft).toBe('');
      expect(component.contentTransform).toBe('');
      expect(component.toggleSizeIcon).toBe('zoom_out_map');
    });
  });

  describe('Training Server', () => {

    it('should load training servers and select the correct training server when selectedTrainingServer is set', () => {
      const mockTrainingServers: TrainingServer[] = [
        {id: 1, name: 'server1', endpoint: 'http://server1.com', selected: true, inEditing: false},
        {id: 2, name: 'server2', endpoint: 'http://server2.com', selected: false, inEditing: false}
      ];
      trainingServerServiceSpy.getAllTrainingsServers.and.returnValue(of(mockTrainingServers));
      component.loadTrainingServer();
      expect(component.trainingServer[0].selected).toBe(true);
      expect(component.trainingServer[1].selected).toBe(false);
    });

    it('should select a training server', () => {
      // Create an TrainingServer object
      const server: TrainingServer = {id: 1, name: '', endpoint: '', inEditing: false, selected: false};

      // Set the component's trainingServer property
      component.trainingServer = [server];

      // Call the trainingServerSelected method with a selection
      component.trainingServerSelected(component.trainingServer[0]);

      // Expect that the selected server is now marked as selected
      expect(component.trainingServer[0].selected).toBe(true);

      // Expect that the component's selectedTrainingServer property has been updated
      expect(component.selectedTrainingServer).toBe(component.trainingServer[0]);

      // Expect that the component's trainingServerName and trainingsServerEndpoint properties have been updated
      expect(component.trainingServerName).toBe(component.trainingServer[0].name);
      expect(component.trainingsServerEndpoint).toBe(component.trainingServer[0].endpoint);

      // Expect that the other servers are marked as not selected
      for (const server of component.trainingServer) {
        if (server !== component.trainingServer[0]) {
          expect(server.selected).toBe(false);
        }
      }
    });

    it('should deselect a training server', () => {
      // Create an TrainingServer object
      const server: TrainingServer = {
        id: 1,
        name: 'Server1',
        endpoint: 'http://server1.com',
        inEditing: false,
        selected: true
      };
      const server2: TrainingServer = {
        id: 2,
        name: 'Server2',
        endpoint: 'http://server2.com',
        inEditing: false,
        selected: false
      };

      // Set the component's trainingServer property
      component.trainingServer = [server, server2];

      // Call the trainingServerSelected method with a deselection
      component.trainingServerSelected(component.trainingServer[0]);

      // Expect that the selected server is now marked as not selected
      expect(component.trainingServer[0].selected).toBe(false);

      // Expect that the component's selectedTrainingServer property has been set to null
      expect(component.selectedTrainingServer).toBeNull();

      // Expect that the component's trainingServerName and trainingsServerEndpoint properties have been cleared
      expect(component.trainingServerName).toBe('');
      expect(component.trainingsServerEndpoint).toBe('');

      // Expect that the other server remains not selected
      expect(component.trainingServer[1].selected).toBe(false);
    });

    it('display an error toast',
      inject([ToasterService], (toasterService: ToasterService) => {
        const toastErrorSpy = spyOn(toasterService, 'error');
        trainingServerServiceSpy.getAllTrainingsServers.and
          .returnValue(throwError(() => new Error('Failed to load server')));
        component.loadTrainingServer();
        expect(toastErrorSpy).toHaveBeenCalledOnceWith('An error occurred while loading the training servers.', 'Failed to load server');
      }));
  });

  describe('onAdvancedToggle', () => {

    const event = {checked: true};
    const mockResponse = {advancedTemplate: 'template data'};

    const setupComponent = () => {
      component.selectedTrainingDatasets = DUMMY_DATASETS;
      component.selectedEvaluationDatasets = DUMMY_DATASETS;
      component.chipRowClasses = [{value: 'Class A', styleClass: 'orange-chip'}];
      component.basicInfoGroup.controls.modelArchitecture.setValue('Yolo');
      component.basicInfoGroup.controls.modelName.setValue('Test Model');
      component.trainingParameterGroup.controls.epochsControl.setValue('1');
      component.trainingParameterGroup.controls.batchSizeControl.setValue('1');
      component.basicInfoGroup.controls.modelNameDescription.setValue('Test Description');
    };

    beforeEach(() => {
      modelManagementServiceSpy.retrieveModelConfig.and.returnValue(of(mockResponse)); // Mocking successful response

      // Setup initial component state
      setupComponent();
    });

    it('should not call retrieveModelConfig when event.checked is false', () => {
      // Arrange
      const uncheckedEvent = {checked: false};

      // Act
      component.onAdvancedToggle(uncheckedEvent);
      fixture.detectChanges();

      // Assert: Check if the service method was not called
      expect(modelManagementServiceSpy.retrieveModelConfig).not.toHaveBeenCalled();
    });

    it('should handle empty datasets and classes correctly', () => {
      // Arrange: Empty datasets and classes
      component.selectedTrainingDatasets = [];
      component.selectedEvaluationDatasets = [];
      component.chipRowClasses = [];

      // Act
      component.onAdvancedToggle(event);
      fixture.detectChanges();

      // Assert: Ensure empty arrays are passed and advanced config is updated
      expect(modelManagementServiceSpy.retrieveModelConfig).toHaveBeenCalledWith(
        'Yolo',
        'Test Model',
        [],
        [],
        1,
        1,
        [],
        'Test Description'
      );
      expect(component.modelConfig).toBe(JSON.stringify(mockResponse, null, 2));
    });
  });

});
