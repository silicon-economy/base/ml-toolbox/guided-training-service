/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Model} from 'src/app/core/models/model';

/**
 * Component for displaying a dialog to confirm the deletion of a model.
 */
@Component({
  selector: 'app-delete-model-dialog',
  templateUrl: './delete-model-dialog.component.html',
  styleUrls: ['./delete-model-dialog.component.scss']
})
export class DeleteModelDialogComponent {
  /**
   * The model data passed to the dialog for deletion confirmation.
   *
   * @param data The model to be deleted.
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: Model) {
  }
}
