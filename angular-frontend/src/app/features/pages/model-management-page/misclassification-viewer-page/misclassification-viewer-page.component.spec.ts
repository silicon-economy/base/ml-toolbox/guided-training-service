/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed, ComponentFixture, tick, fakeAsync} from '@angular/core/testing';
import {
  MisclassificationViewerPageComponent
} from 'src/app/features/pages/model-management-page/misclassification-viewer-page/misclassification-viewer-page.component';
import {MatDialog} from '@angular/material/dialog';
import {ModelManagementService} from 'src/app/core/backend/model-management.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {ActivatedRoute} from '@angular/router';
import {of, throwError} from 'rxjs';

describe('MisclassificationViewerPageComponent', () => {
  let component: MisclassificationViewerPageComponent;
  let fixture: ComponentFixture<MisclassificationViewerPageComponent>;
  let mockModelManagementService: jasmine.SpyObj<ModelManagementService>;
  let mockToasterService: jasmine.SpyObj<ToasterService>;
  let mockMatDialog: jasmine.SpyObj<MatDialog>;

  beforeEach(async () => {
    const modelManagementServiceSpy = jasmine.createSpyObj('ModelManagementService', ['getFnFpImages']);
    const toasterServiceSpy = jasmine.createSpyObj('ToasterService', ['error']);
    const matDialogSpy = jasmine.createSpyObj('MatDialog', ['open']);

    await TestBed.configureTestingModule({
      declarations: [MisclassificationViewerPageComponent],
      providers: [
        {provide: ModelManagementService, useValue: modelManagementServiceSpy},
        {provide: ToasterService, useValue: toasterServiceSpy},
        {provide: MatDialog, useValue: matDialogSpy},
        {
          provide: ActivatedRoute,
          useValue: {params: of({runId: '123', className: 'dummy_class', modelName: 'dummy_model'})}
        }
      ]
    })
      .compileComponents();

    mockModelManagementService = TestBed.inject(ModelManagementService) as jasmine.SpyObj<ModelManagementService>;
    mockToasterService = TestBed.inject(ToasterService) as jasmine.SpyObj<ToasterService>;
    mockMatDialog = TestBed.inject(MatDialog) as jasmine.SpyObj<MatDialog>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MisclassificationViewerPageComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should load images on initialization', fakeAsync(() => {
    const images = ['image1.jpg', 'image2.jpg'];
    component.className = 'dummy_class';
    mockModelManagementService.getFnFpImages.and.returnValue(of({'images': {[component.className]: images}}));

    fixture.detectChanges();
    tick();

    expect(component.images).toEqual(images);
    expect(component.loading).toBeFalsy();
  }));

  it('should show error toaster if loading images fails', fakeAsync(() => {
    mockModelManagementService.getFnFpImages.and.returnValue(throwError('Error'));

    fixture.detectChanges();
    tick();

    expect(mockToasterService.error).toHaveBeenCalledWith('An error occurred while loading the images', 'Failed to load images');
    expect(component.loading).toBeFalsy();
  }));

  it('should open image popup', () => {
    const imageUrl = 'image.jpg';
    component.openImagePopup(imageUrl);
    expect(mockMatDialog.open).toHaveBeenCalledWith(jasmine.any(Function), {
      maxWidth: '90vw',
      maxHeight: '90vh',
      data: {imageUrl: imageUrl, altText: 'Image', title: 'View Image'}
    });
  });
});
