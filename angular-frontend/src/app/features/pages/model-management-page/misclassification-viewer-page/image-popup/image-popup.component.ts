/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

/**
 * Component for displaying an image in a popup dialog.
 */
@Component({
  selector: 'app-image-popup',
  templateUrl: './image-popup.component.html',
  styleUrls: ['./image-popup.component.scss']
})
export class ImagePopupComponent {
  /**
   * Creates an instance of the ImagePopupComponent.
   *
   * @param data The data injected into the dialog.
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}
