/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {
  ImagePopupComponent
} from "src/app/features/pages/model-management-page/misclassification-viewer-page/image-popup/image-popup.component";
import {ModelManagementService} from "src/app/core/backend/model-management.service";
import {ToasterService} from "src/app/core/services/toaster/toaster.service";
import {finalize} from "rxjs/operators";

/**
 * Component for displaying and viewing misclassified images for a given model.
 * This component loads images related to a model's misclassifications (false negatives or false positives)
 * and allows users to view these images in a popup dialog.
 */
@Component({
  selector: 'app-misclassification-viewer-page',
  templateUrl: './misclassification-viewer-page.component.html',
  styleUrls: ['./misclassification-viewer-page.component.scss']
})
export class MisclassificationViewerPageComponent implements OnInit {
  className: string;
  runId: string;
  modelName: string;

  loading: boolean;

  images = [];

  /**
   * Creates an instance of the MisclassificationViewerPageComponent.
   *
   * @param route The activated route used to extract parameters like runId, className, and modelName.
   * @param dialog The Angular Material dialog service for opening the image popup.
   * @param modelManagementService The service responsible for interacting with the model's backend.
   * @param toasterService The service for displaying success or error messages.
   */
  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private modelManagementService: ModelManagementService,
              private toasterService: ToasterService) {
  }

  /**
   * Initializes the component by subscribing to route parameters and loading the images.
   */
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.runId = params['runId'];
      this.className = params['className'];
      this.modelName = params['modelName'];
    });
    this.loadImages();
  }

  /**
   * Loads the misclassified images for the specified model and class.
   *
   * The images are fetched from the backend and displayed in the component. If an error occurs,
   * an error message is shown using the toaster service.
   */
  loadImages() {
    this.loading = true;
    this.modelManagementService.getFnFpImages(this.modelName).pipe(finalize(() => {
      this.loading = false;
    })).subscribe({
        next: (response) => {
          // Check response if data is included
          if (response && response['images']) {
            response['images'][this.className].forEach(image => {
              this.images.push(image);
            });
          }
        }, error: () => {
          this.toasterService.error('An error occurred while loading the images', 'Failed to load images');
        }
      }
    );
  }

  /**
   * Opens a popup dialog to view the selected image.
   *
   * @param imageUrl The URL of the image to be viewed in the popup.
   */
  openImagePopup(imageUrl: string): void {
    this.dialog.open(ImagePopupComponent, {
      maxWidth: '90vw',
      maxHeight: '90vh',
      data: {imageUrl: imageUrl, altText: 'Image', title: 'View Image'}
    });
  }
}
