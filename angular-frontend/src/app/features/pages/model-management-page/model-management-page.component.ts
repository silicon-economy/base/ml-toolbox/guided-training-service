/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ModelsDataSource} from 'src/app/core/backend/models.datasource';
import {ModelManagementService} from 'src/app/core/backend/model-management.service';
import {debounceTime, distinctUntilChanged, fromEvent, merge, Subscription, tap} from 'rxjs';
import {Model} from 'src/app/core/models/model';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {
  TrainNewModelDialogComponent
} from 'src/app/features/pages/model-management-page/train-new-model-dialog/train-new-model-dialog.component';
import {
  DeleteModelDialogComponent
} from 'src/app/features/pages/model-management-page/delete-model-dialog/delete-model-dialog.component';
import {
  DownloadModelDialogComponent
} from "src/app/features/pages/model-management-page/download-model-dialog/download-model-dialog.component";
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

/**
 * Component for managing models in the application.
 * Allows viewing, deleting, adding, and downloading models.
 *
 * @component
 */
@Component({
  selector: 'app-model-management-page',
  templateUrl: './model-management-page.component.html',
  styleUrls: ['./model-management-page.component.scss']
})
export class ModelManagementPageComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns: string[] = ['name', 'dataset', 'creation_date', 'algorithm_type', 'epochs', 'batch_size', 'accuracy', 'state', 'action'];
  dataSource: ModelsDataSource;

  totalModels = 100;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filterInput') filterInput: ElementRef;

  /**
   * Stores all open subscriptions to prevent memory leaks.
   */
  private readonly subscriptions = new Subscription();

  /**
   * Creates an instance of the component.
   *
   * @param modelManagementService Service for managing models.
   * @param dialog Dialog service for opening modal dialogs.
   * @param toasterService Service for showing toast notifications.
   * @param router Router for navigating to other pages.
   */
  constructor(private modelManagementService: ModelManagementService,
              public dialog: MatDialog, private toasterService: ToasterService, private router: Router) {
  }

  /**
   * Initializes the component by setting up the data source and loading models.
   */
  ngOnInit(): void {
    this.dataSource = new ModelsDataSource(this.modelManagementService);
    this.subscriptions.add(
      this.dataSource.totalModels.subscribe(totalModels => {
          this.totalModels = totalModels;
        }
      ));
    // TODO: get this settings automatically from HTML defaults and run this.loadModels() instead
    this.dataSource.loadModels('', 'creation_date', 'desc', 0, 10);
  }

  /**
   * Initializes views and sets up event listeners for sorting and filtering.
   */
  ngAfterViewInit() {
    this.subscriptions.add(
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)
    );

    this.subscriptions.add(
      fromEvent(this.filterInput.nativeElement, 'keyup')
        .pipe(
          debounceTime(150),
          distinctUntilChanged(),
          tap(() => {
            this.paginator.pageIndex = 0;
            this.loadModels();
          })
        )
        .subscribe()
    );

    // Reload on sort or paginator change
    this.subscriptions.add(
      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.loadModels())
        )
        .subscribe()
    );
  }

  /**
   * Cleans up subscriptions when the component is destroyed.
   */
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  /**
   * Loads models with the specified filters and sorting options.
   */
  loadModels() {
    let sortName = 'creation_date';
    let sortOrder = 'desc';
    if (this.sort.active && this.sort.direction) {
      sortName = this.sort.active;
      sortOrder = this.sort.direction;
    }
    this.dataSource.loadModels(
      this.filterInput.nativeElement.value,
      sortName,
      sortOrder,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  /**
   * Clears the filter input and reloads the models.
   */
  clearFilter() {
    this.filterInput.nativeElement.value = '';
    this.paginator.pageIndex = 0;
    this.loadModels();
  }

  /**
   * Deletes a model after confirmation through a dialog.
   *
   * @param model The model to delete.
   */
  deleteModel(model: Model) {
    const dialogRef = this.dialog.open(DeleteModelDialogComponent, {
      data: model,
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.modelManagementService.deleteModel(model).subscribe({
          next: () => {
            this.toasterService.success(`Model ${model.name} was deleted.`, 'Model deleted');
            this.loadModels();
          },
          error: () => this.toasterService.error(`Model ${model.name} could not be deleted.`, 'Failed to delete Model')
        });
      }
    });
  }

  /**
   * Opens a dialog for adding a new model.
   */
  addNewModel() {
    const dialogRef = this.dialog.open(TrainNewModelDialogComponent, {
      height: '80%',
      width: '70%',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(() => {
      this.ngOnInit();
    });
  }

  /**
   * Opens the model detail page in a new tab.
   *
   * @param model The model whose details to view.
   */
  openModelPage(model: Model) {
    const name = encodeURIComponent(model.name)
    const url = this.router.serializeUrl(this.router.createUrlTree(['/model-detail', name]))
    window.open(url, '_blank')
  }

  /**
   * Downloads the model artifacts.
   *
   * @param url The URL to download the artifacts from.
   */
  _downloadFiles(url: string) {
    window.location.assign(url)
  }

  /**
   * Opens a dialog to download the model artifacts after confirmation.
   *
   * @param model The model whose artifacts to download.
   */
  downloadModel(model: Model) {
    const dialogRef = this.dialog.open(DownloadModelDialogComponent, {
      data: model
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.modelManagementService.downloadModelArtifacts(model).subscribe({
          next: (downloadUrl) => {
            this.toasterService.success(`Artifacts for model '${model.name}' were downloaded`, 'Downloaded model artifacts');
            this._downloadFiles(downloadUrl.toString())
            this.loadModels();
          },
          error: () => this.toasterService.error(`Download of artifacts for model '${model.name}' failed!`, 'Download failed')
        })
      }
    });
  }
}
