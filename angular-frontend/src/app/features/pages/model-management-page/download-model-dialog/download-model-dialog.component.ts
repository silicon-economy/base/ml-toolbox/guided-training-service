/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Model} from 'src/app/core/models/model';

/**
 * Component for displaying a dialog to confirm downloading model artifacts.
 */
@Component({
  selector: 'app-download-model-dialog',
  templateUrl: './download-model-dialog.component.html',
  styleUrls: ['./download-model-dialog.component.scss']
})
export class DownloadModelDialogComponent {
  /**
   * The model data passed to the dialog for artifact download confirmation.
   *
   * @param data The model whose artifacts are to be downloaded.
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: Model) {
  }
}
