/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {TimePastPipe} from 'ng-time-past-pipe';
import {Observable, of, throwError} from 'rxjs';
import {Model, LabelsTag} from 'src/app/core/models/model';

import {
  TrainNewModelDialogComponent
} from 'src/app/features/pages/model-management-page/train-new-model-dialog/train-new-model-dialog.component';
import {
  ModelManagementPageComponent
} from 'src/app/features/pages/model-management-page/model-management-page.component';
import {
  DeleteModelDialogComponent
} from 'src/app/features/pages/model-management-page/delete-model-dialog/delete-model-dialog.component';
import {
  DownloadModelDialogComponent
} from "src/app/features/pages/model-management-page/download-model-dialog/download-model-dialog.component";
import {MatInputModule} from '@angular/material/input';
import {ModelManagementService, ModelsResponse} from 'src/app/core/backend/model-management.service';
import {MatChipsModule} from '@angular/material/chips';
import {MatProgressBarModule} from '@angular/material/progress-bar';

import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Router} from '@angular/router';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

const LABEL_TAGS: LabelsTag[] = [
  {
    name: 'EPAL',
    colorClass: 'bright-green-chip'
  },
  {
    name: 'Gitterbox',
    colorClass: 'yellow-chip'
  },
  {
    name: 'KLT',
    colorClass: 'orange-chip'
  },
  {
    name: 'Überstand',
    colorClass: 'red-chip'
  }
];

const DUMMY_MODELS: Model[] = [
  {
    run_id: '0',
    name: 'Default Yolo-v5',
    description: 'This is a really long description for the model. The model\'s name is "Default Yolo-v5" and it uses the "Imagenet" dataset',
    dataset: ['Imagenet'],
    evaluation_datasets: ['Imagenet'],
    model: 'modelpath',
    epochs: 30,
    batch_size: 6,
    annotation_task: [LABEL_TAGS[0], LABEL_TAGS[1]],
    training_state: 'running',
    accuracy: 0.8,
    algorithm_type: 'detection',
    creation_date: new Date("2023-08-01T00:00:00.000Z"),
    mlflow_url: "https://example.org/"
  },
  {
    run_id: '1',
    name: 'Default Yolo-v5',
    description: 'Description for the model',
    dataset: ['Imagenet'],
    evaluation_datasets: ['Imagenet'],
    model: 'modelpath',
    epochs: 30,
    batch_size: 6,
    annotation_task: [LABEL_TAGS[2], LABEL_TAGS[3]],
    training_state: 'done',
    accuracy: 0.8,
    algorithm_type: 'classification',
    creation_date: new Date("2023-08-01T00:00:00.000Z"),
    mlflow_url: ""
  },
  {
    run_id: '2',
    name: 'Default Yolo-v5',
    description: 'Description for the model',
    dataset: ['Imagenet'],
    evaluation_datasets: ['Imagenet'],
    model: 'modelpath',
    epochs: 30,
    batch_size: 6,
    annotation_task: [LABEL_TAGS[2], LABEL_TAGS[3]],
    training_state: 'done',
    accuracy: 0.8,
    algorithm_type: 'classification',
    creation_date: new Date("2023-08-01T00:00:00.000Z"),
    mlflow_url: ""
  }
];

export class MdDialogMock {

  open() {
    return {
      afterClosed: () => of(true)
    };
  }
}

describe('ModelManagementPageComponent', () => {
  let component: ModelManagementPageComponent;
  let fixture: ComponentFixture<ModelManagementPageComponent>;
  let router: Router;
  let mockModelManagementService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSortModule,
        MatChipsModule,
        MatProgressBarModule,
        MatIconModule,
        MatPaginatorModule,
        MatTooltipModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        TimePastPipe,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      declarations: [ModelManagementPageComponent],
      providers: [
        ToasterService,
        {
          provide: ModelManagementService,
        },
        {
          provide: MatDialog, useClass: MdDialogMock,
        },
      ]
    })
      .compileComponents();

    mockModelManagementService = TestBed.inject(ModelManagementService);

  });

  let getModelsSpy: jasmine.Spy<(filter: string, sortName: string, sortOrder: string, pageNumber: number, pageSize: number)
    => Observable<ModelsResponse>>;

  beforeEach(
    inject([ModelManagementService], (modelManagementService: ModelManagementService) => {


      const testModelsResponse: ModelsResponse = {
        models: DUMMY_MODELS,
        totalModels: 50
      };
      getModelsSpy = spyOn(modelManagementService, 'getModels').and.returnValue(of(testModelsResponse));

      fixture = TestBed.createComponent(ModelManagementPageComponent);
      component = fixture.componentInstance;
      router = TestBed.inject(Router);
      fixture.detectChanges();
    }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initially load models from service', () => {
    const initialRequestParams = [
      '', 'creation_date', 'desc', 0, 10
    ];
    expect(mockModelManagementService.getModels).toHaveBeenCalledOnceWith(...initialRequestParams);
  });

  it('should update totalModels after initial models request', () => {
    expect(component.totalModels).toEqual(50);
  });

  it('should reset and reload models from service on clearFilter', () => {
    // Prepare
    mockModelManagementService.getModels.calls.reset();
    // The paginator's pageSize should be kept unchanged
    component.paginator.pageSize = 20;
    // The paginator's pageIndex should be reset
    component.paginator.pageIndex = 5;
    // The filter should reset
    component.filterInput.nativeElement.value = 'filter text';

    // Act
    component.clearFilter();

    // Verify
    const requestParams = [
      '', 'creation_date', 'desc', 0, 20
    ];
    expect(mockModelManagementService.getModels).toHaveBeenCalledOnceWith(...requestParams);
  });

  it('should request models from service on filter change after debounce time', fakeAsync(() => {
    // Prepare
    mockModelManagementService.getModels.calls.reset();
    // The paginator's pageSize should be kept unchanged
    component.paginator.pageSize = 20;
    // The paginator's pageIndex should be reset
    component.paginator.pageIndex = 5;

    // Act
    component.filterInput.nativeElement.value = 'filter text';
    component.filterInput.nativeElement.dispatchEvent(new KeyboardEvent('keyup'));
    fixture.detectChanges();
    tick(150); // wait for debounce
    discardPeriodicTasks(); // discard remaining tasks

    // Verify
    const requestParams = [
      'filter text', 'creation_date', 'desc', 0, 20
    ];
    expect(mockModelManagementService.getModels).toHaveBeenCalledOnceWith(...requestParams);
  }));

  it('should not immediatly request models from service on filter change', fakeAsync(() => {
    // Prepare
    mockModelManagementService.getModels.calls.reset();


    // Act
    component.filterInput.nativeElement.value = 'filter text';
    component.filterInput.nativeElement.dispatchEvent(new KeyboardEvent('keyup'));
    fixture.detectChanges();
    tick(50); // wait for less than debounce time
    discardPeriodicTasks(); // discard remaining tasks

    // Verify
    expect(mockModelManagementService.getModels).toHaveBeenCalledTimes(0);
  }));


  describe('Datatable pages', () => {
    it('should request models from service on page change', () => {
      // Prepare
      mockModelManagementService.getModels.calls.reset();


      // Act
      expect(component.paginator.hasNextPage()).toBeTrue();
      component.paginator.nextPage();
      fixture.detectChanges();

      // Verify
      const requestParams = [
        '', 'creation_date', 'desc', 1, 10
      ];
      expect(mockModelManagementService.getModels).toHaveBeenCalledOnceWith(...requestParams);
      expect(component.paginator.hasNextPage()).toBeTrue(); // in total 5 pages
    });

    it('should request models from service after navigating to last page', () => {
      // Prepare
      mockModelManagementService.getModels.calls.reset();


      // Act
      component.paginator.lastPage();
      fixture.detectChanges();
      // Verify
      const requestParams = [
        '', 'creation_date', 'desc', 4, 10
      ];
      expect(mockModelManagementService.getModels).toHaveBeenCalledOnceWith(...requestParams);
      expect(component.paginator.hasNextPage()).withContext("Datatable has no next page").toBeFalse();
    });
  });


  it('should request models from service on sort change', () => {
    // Prepare
    mockModelManagementService.getModels.calls.reset();

    // Act
    // click on one of the sort header buttons
    const sortButtons = fixture.debugElement.nativeElement.querySelectorAll('.mat-sort-header-container');
    sortButtons[2].click(); // asc
    mockModelManagementService.getModels.calls.reset();
    sortButtons[2].click(); // desc
    fixture.detectChanges();

    // Verify
    const requestParams = [
      '', 'algorithm_type', 'desc', 0, 10
    ];
    expect(mockModelManagementService.getModels).toHaveBeenCalledOnceWith(...requestParams);
  });

  it('should open add new model dialog', inject([MatDialog], (matdialog: MatDialog) => {
    const dialogSpy = spyOn(matdialog, 'open').and.callThrough();
    component.addNewModel();
    expect(dialogSpy).toHaveBeenCalledOnceWith(TrainNewModelDialogComponent, {
      height: '80%',
      width: '70%',
      disableClose: true
    });
  }));

  describe('Delete Dialog', () => {
    it('should open the dialog and delete the model if the dialog is confirmed',
      inject([MatDialog, ModelManagementService, ToasterService],
        (matdialog: MatDialog, modelManagementService: ModelManagementService, toasterService: ToasterService) => {
          const modelManagementServiceSpy = spyOn(modelManagementService, 'deleteModel').and.returnValue(of({}));
          const toastSuccessSpy = spyOn(toasterService, 'success');
          const dialogSpy = spyOn(matdialog, 'open').and
            .returnValue(<MatDialogRef<typeof DeleteModelDialogComponent>>{afterClosed: () => of(true)});

          component.deleteModel(DUMMY_MODELS[0]);

          expect(dialogSpy).toHaveBeenCalledOnceWith(DeleteModelDialogComponent, {data: DUMMY_MODELS[0]});
          expect(modelManagementServiceSpy).toHaveBeenCalledOnceWith(DUMMY_MODELS[0]);
          expect(toastSuccessSpy).toHaveBeenCalledOnceWith(`Model ${DUMMY_MODELS[0].name} was deleted.`, 'Model deleted');
        }));

    it('should not delete the model if the dialog is not confirmed',
      inject([MatDialog, ModelManagementService, ToasterService],
        (matdialog: MatDialog, modelManagementService: ModelManagementService, toasterService: ToasterService) => {
          const modelManagementServiceSpy = spyOn(modelManagementService, 'deleteModel').and.returnValue(of({}));
          const toastSuccessSpy = spyOn(toasterService, 'success');

          const dialogSpy = spyOn(matdialog, 'open').and
            .returnValue(<MatDialogRef<typeof DeleteModelDialogComponent>>{afterClosed: () => of(false)});
          component.deleteModel(DUMMY_MODELS[0]);
          expect(dialogSpy).toHaveBeenCalledOnceWith(DeleteModelDialogComponent, {data: DUMMY_MODELS[0]});

          expect(modelManagementServiceSpy).toHaveBeenCalledTimes(0);
          expect(toastSuccessSpy).toHaveBeenCalledTimes(0);
        }));


    it('should show an error if the deletion of the model fails',
      inject([MatDialog, ModelManagementService, ToasterService], (matdialog: MatDialog, modelManagementService: ModelManagementService, toasterService: ToasterService) => {
        const deleteModelSpy = spyOn(modelManagementService, 'deleteModel').and
          .returnValue(throwError(() => new Error()));
        const toastErrorSpy = spyOn(toasterService, 'error');

        const dialogSpy = spyOn(matdialog, 'open').and
          .returnValue(<MatDialogRef<typeof DeleteModelDialogComponent>>{afterClosed: () => of(true)});
        component.deleteModel(DUMMY_MODELS[0]);
        expect(dialogSpy).toHaveBeenCalledOnceWith(DeleteModelDialogComponent, {data: DUMMY_MODELS[0]});
        expect(deleteModelSpy).toHaveBeenCalledOnceWith(DUMMY_MODELS[0]);
        expect(toastErrorSpy).toHaveBeenCalledOnceWith(`Model ${DUMMY_MODELS[0].name} could not be deleted.`, 'Failed to delete Model');
      }));
  });

  describe('Model overview', () => {
    it('should open model detail page in a new tab', () => {
      // Prepare
      const model: Model = DUMMY_MODELS[0];
      const openSpy = spyOn(window, 'open');
      const name = encodeURIComponent(model.name)
      const expectedUrl = router.serializeUrl(router.createUrlTree(['/model-detail', name]))
      // Act
      component.openModelPage(model);

      // Verify
      expect(openSpy).toHaveBeenCalledWith(expectedUrl, '_blank');
    });
  });

  describe("Download dialog", () => {
    it('should open download dialog',
      inject([MatDialog, ToasterService, ModelManagementService], (matdialog: MatDialog, toasterService: ToasterService, modelManagementService: ModelManagementService) => {
        const toastSuccessSpy = spyOn(toasterService, 'success');
        const downloadModelSpy = spyOn(modelManagementService, 'downloadModelArtifacts').and.returnValue(
          of('someURL'));
        const dialogSpy = spyOn(matdialog, 'open')
          .and.returnValue(<MatDialogRef<typeof DownloadModelDialogComponent>>{afterClosed: () => of(true)})
        const downloadSpy = spyOn(component, '_downloadFiles');
        component.downloadModel(DUMMY_MODELS[0]);

        expect(dialogSpy).toHaveBeenCalledOnceWith(DownloadModelDialogComponent, {data: DUMMY_MODELS[0]});
        expect(downloadModelSpy).toHaveBeenCalledOnceWith(DUMMY_MODELS[0]);
        expect(downloadSpy).toHaveBeenCalledOnceWith('someURL');
        expect(toastSuccessSpy).toHaveBeenCalledOnceWith(`Artifacts for model '${DUMMY_MODELS[0].name}' were downloaded`, 'Downloaded model artifacts');
      }));

    it('should show an error if download fails',
      inject([MatDialog, ToasterService, ModelManagementService], (matdialog: MatDialog, toasterService: ToasterService, modelManagementService: ModelManagementService) => {
        const toastErrorSpy = spyOn(toasterService, 'error');
        const downloadModelSpy = spyOn(modelManagementService, 'downloadModelArtifacts').and.returnValue(
          throwError(() => new Error('An error happened')));
        const dialogSpy = spyOn(matdialog, 'open')
          .and.returnValue(<MatDialogRef<typeof DownloadModelDialogComponent>>{afterClosed: () => of(true)})
        component.downloadModel(DUMMY_MODELS[0]);

        expect(dialogSpy).toHaveBeenCalledOnceWith(DownloadModelDialogComponent, {data: DUMMY_MODELS[0]});
        expect(toastErrorSpy).toHaveBeenCalledOnceWith(`Download of artifacts for model '${DUMMY_MODELS[0].name}' failed!`, 'Download failed');
        expect(downloadModelSpy).toHaveBeenCalledOnceWith(DUMMY_MODELS[0]);
      }));

    it('should open the download dialog and abort',
      inject([MatDialog, ToasterService, ModelManagementService], (matdialog: MatDialog, toasterService: ToasterService, modelManagementService: ModelManagementService) => {
        const toastSuccessSpy = spyOn(toasterService, 'success');
        const downloadModelSpy = spyOn(modelManagementService, 'downloadModelArtifacts').and.returnValue(
          of('someURL'));
        const dialogSpy = spyOn(matdialog, 'open')
          .and.returnValue(<MatDialogRef<typeof DownloadModelDialogComponent>>{afterClosed: () => of(false)})
        component.downloadModel(DUMMY_MODELS[0]);

        expect(dialogSpy).toHaveBeenCalledOnceWith(DownloadModelDialogComponent, {data: DUMMY_MODELS[0]});
        expect(toastSuccessSpy).toHaveBeenCalledTimes(0);
        expect(downloadModelSpy).toHaveBeenCalledTimes(0);
      }));

  });
});
