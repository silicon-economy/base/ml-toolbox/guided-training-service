/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ModelDetailPageComponent} from './model-detail-page.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Model} from "src/app/core/models/model";
import {ModelManagementService} from "src/app/core/backend/model-management.service";
import {of, throwError} from "rxjs";
import {ToasterService} from "src/app/core/services/toaster/toaster.service";

const DUMMY_MODEL: Model = {
  run_id: '0',
  name: 'Dummy-Model',
  description: 'This is a really long description for the model. The model\'s name is "Dummy-Model"',
  dataset: [],
  evaluation_datasets: [],
  model: 'modelpath',
  epochs: 30,
  batch_size: 6,
  annotation_task: [],
  training_state: 'running',
  accuracy: 0.8,
  algorithm_type: 'detection',
  creation_date: new Date("2023-08-01T00:00:00.000Z"),
  mlflow_url: "https://example.org/"
}

describe('ModelDetailPageComponent', () => {
  let component: ModelDetailPageComponent;
  let fixture: ComponentFixture<ModelDetailPageComponent>;
  let modelManagementService: jasmine.SpyObj<ModelManagementService>; // Mocked service
  let toasterServiceSpy: jasmine.SpyObj<ToasterService>;


  beforeEach(async () => {
    modelManagementService = jasmine.createSpyObj('ModelManagementService', ['getMetrics', 'getModel']); // Create mocked service
    const toasterService = jasmine.createSpyObj('ToasterService', ['error']);

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ModelDetailPageComponent],
      providers: [
        {provide: ToasterService, useValue: toasterService},
        {provide: ModelManagementService, useValue: modelManagementService}
      ]
    }).compileComponents();

    modelManagementService.getMetrics.and.returnValue(of({}));
    modelManagementService.getModel.and.returnValue(of({totalModels: 1, models: [DUMMY_MODEL]}));
    modelManagementService = TestBed.inject(ModelManagementService) as jasmine.SpyObj<ModelManagementService>;
    toasterServiceSpy = TestBed.inject(ToasterService) as jasmine.SpyObj<ToasterService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelDetailPageComponent);
    component = fixture.componentInstance;

    history.replaceState({model: DUMMY_MODEL}, '');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set selected model and initialize dataSourceParameter correctly', () => {
    // Prepare
    spyOn(component, 'getMetricsOfRun');

    // Act
    component.ngOnInit();

    // Verify
    expect(component.selectedModel).toEqual(DUMMY_MODEL);
    expect(component.dataSourceParameter).toEqual([
      {key: 'Batch size', value: DUMMY_MODEL.batch_size},
      {key: 'Epochs', value: DUMMY_MODEL.epochs},
      {key: 'Algorithm type', value: DUMMY_MODEL.algorithm_type},
      {key: 'Accuracy (Score)', value: '80.0%'}
    ]);
    expect(component.getMetricsOfRun).toHaveBeenCalled();
  })

  it('should open a new page with mlflow', () => {
      // Prepare
      const windowOpenSpy = spyOn(window, 'open');
      component.openModelPage();

      // Act & Verify
      expect(windowOpenSpy).toHaveBeenCalledOnceWith('https://example.org/', '_blank');
    }
  );

  it('should return className value directly', () => {
    // Arrange
    const classMetrics = [
      {key: 'className', value: '0_loading_unit'},
    ];

    // Act
    const result1 = component.getMetricValue(classMetrics, 'className');

    // Assert
    expect(result1).toEqual('0_loading_unit');
  });

  it('should return metric value multiplied by 100 with two decimal places for AP, PR, or RC', () => {
    // Prepare
    const classMetrics = [
      {key: 'AP', value: 0.75},
      {key: 'PR', value: 0.85},
      {key: 'RC', value: 0.98}
    ];

    // Act & Verify
    expect(component.getMetricValue(classMetrics, 'AP')).toEqual('75.00');
    expect(component.getMetricValue(classMetrics, 'PR')).toEqual('85.00');
    expect(component.getMetricValue(classMetrics, 'RC')).toEqual('98.00');
  });

  it('should return the value without percentage for metricName not AP, PR, or RC', () => {
    const classMetrics = [
      {key: 'FP', value: 10.0},
      {key: 'FN', value: 20.0}
    ];
    expect(component.getMetricValue(classMetrics, 'FP')).toEqual('10');
    expect(component.getMetricValue(classMetrics, 'FN')).toEqual('20');
  });

  it('should return "-" if metric not found', () => {
    // Prepare
    const classMetrics = [
      {key: 'PR', value: 0.75},
      {key: 'RC', value: 0.80}
    ];
    const metricName = 'f1_score';

    // Act
    const result = component.getMetricValue(classMetrics, metricName);

    // Verify
    expect(result).toEqual('-');
  });

  it('should fetch metrics successfully and initialize dataSourceMetrics', () => {
    // Prepare
    const mockMetricsResponse = {
      class1: {PR: 0.75, RC: 0.80},
      class2: {PR: 0.85, RC: 0.90}
    };
    modelManagementService.getMetrics.and.returnValue(of(mockMetricsResponse)); // Mocking service response

    // Act
    component.getMetricsOfRun();

    // Verify
    expect(component.dataSourceMetrics).toEqual([
      [
        {key: 'PR', value: 0.75},
        {key: 'RC', value: 0.80},
        {key: 'className', value: 'class1'}
      ],
      [
        {key: 'PR', value: 0.85},
        {key: 'RC', value: 0.90},
        {key: 'className', value: 'class2'}
      ]
    ]);
  });

  it('should display error toast when loading metrics fails', () => {
    // Prepare
    modelManagementService.getMetrics.and.returnValue(throwError('Error'));

    // Act
    component.getMetricsOfRun();

    // Verify
    expect(toasterServiceSpy.error).toHaveBeenCalledWith('An error occurred while loading the metrics!', 'Failed to load metrics');
  });

  it('should return correct tooltip text for each metric', () => {
    expect(component.getMetricName('className')).toEqual('Class Identifier');
    expect(component.getMetricName('TP')).toEqual('True Positives');
    expect(component.getMetricName('FP')).toEqual('False Positives');
    expect(component.getMetricName('FN')).toEqual('False Negatives');
    expect(component.getMetricName('RC')).toEqual('Recall');
    expect(component.getMetricName('PR')).toEqual('Precision');
    expect(component.getMetricName('AP')).toEqual('Average Precision');
    expect(component.getMetricName('OtherColumn')).toEqual('');
  });

  it('should open misclassification viewer page in new tab', () => {
    spyOn(window, 'open');
    const runId = '123';
    const element = ['className', 'TP', 'FP'];
    const className = 'ClassName';
    const modelName = 'ModelName';
    spyOn(component, 'getMetricValue').and.returnValue('some-metric-value');

    component.openMisclassificationViewerPage(runId, element, className, modelName);

    const expectedUrl = '/misclassification-viewer/123/some-metric-value/ModelName';
    expect(window.open).toHaveBeenCalledWith(expectedUrl, '_blank');
  });
});
