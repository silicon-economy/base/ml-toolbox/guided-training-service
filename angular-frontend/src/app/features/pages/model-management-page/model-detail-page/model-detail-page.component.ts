/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Model} from 'src/app/core/models/model';
import {ModelManagementService} from "src/app/core/backend/model-management.service";
import {ToasterService} from "src/app/core/services/toaster/toaster.service";

/**
 * Component for displaying detailed information about a selected model.
 */
@Component({
  selector: 'app-model-detail-page',
  templateUrl: './model-detail-page.component.html',
  styleUrls: ['./model-detail-page.component.scss']
})
export class ModelDetailPageComponent implements OnInit {
  selectedModel: Model | null = null;
  displayedColumnsParameter: string[] = ['parameter', 'value'];
  dataSourceParameter = [];

  displayedColumnsMetrics: string[] = ['className', 'TP', 'FP', 'FN', 'RC', 'PR', 'AP'];
  dataSourceMetrics = [];

  protected readonly window = window;

  /**
   * Creates an instance of ModelDetailPageComponent.
   *
   * @param route The activated route to access route parameters.
   * @param modelManagementService The service to manage model data.
   * @param toasterService The service to display toaster notifications.
   * @param router The Angular router to navigate between views.
   */
  constructor(
    private route: ActivatedRoute,
    private modelManagementService: ModelManagementService,
    private toasterService: ToasterService,
    private router: Router) {
  }

  /**
   * On initialization, fetches the model details and metrics based on the route parameter.
   */
  ngOnInit() {
    const name = this.route.snapshot.params['data'];
    this.modelManagementService.getModel(name).subscribe(params => {
      this.selectedModel = params['models'][0];
      this.dataSourceParameter = [
        {key: 'Batch size', value: this.selectedModel.batch_size},
        {key: 'Epochs', value: this.selectedModel.epochs},
        {key: 'Algorithm type', value: this.selectedModel.algorithm_type},
        {
          key: 'Accuracy (Score)',
          value: `${(this.selectedModel.accuracy * 100).toFixed(1)}%`
        }
      ];
      this.getMetricsOfRun();
    })
  }

  /**
   * Opens the model page in a new tab.
   */
  openModelPage() {
    window.open(this.selectedModel.mlflow_url, '_blank');
  }

  /**
   * Fetches and processes the metrics of the selected model.
   * Organizes the metrics into a format suitable for display in the component.
   */
  getMetricsOfRun() {
    this.modelManagementService.getMetrics(this.selectedModel).subscribe({
      next: (response: any) => {
        const metrics = response;
        const dataSourceMetrics = [];
        // Iterate over each class in the metrics dictionary
        for (const className in metrics) {
          if (Object.prototype.hasOwnProperty.call(metrics, className)) {
            const classMetrics = metrics[className];
            // Convert the inner dictionary to an array of objects
            const classMetricsArray = Object.keys(classMetrics).map(metricName => ({
              key: metricName,
              value: classMetrics[metricName]
            }));
            classMetricsArray.push({key: 'className', value: className});
            // Push the class name and its metrics array to the dataSourceMetrics array
            dataSourceMetrics.push(classMetricsArray);
          }
        }
        this.dataSourceMetrics = dataSourceMetrics;
      },
      error: () => this.toasterService.error(`An error occurred while loading the metrics!`, 'Failed to load metrics')
    });
  }

  /**
   * Returns the value of a specific metric for a given class.
   *
   * @param classMetrics The metrics of the class.
   * @param metricName The name of the metric to retrieve.
   * @returns The value of the metric, or a default value if not found.
   */
  getMetricValue(classMetrics: any[], metricName: string): any {
    const metric = classMetrics.find(metric => metric.key === metricName);

    if (!metric) {
      return '-';
    }
    if (metric.key === 'className') {
      // Statement for className
      return metric.value;
    } else if (metricName === 'AP' || metricName === 'PR' || metricName === 'RC') {
      // Statement for metrics with percentage
      return (metric.value * 100).toFixed(2);
    } else {
      // Statement for rest of metrics
      return Number(metric.value).toFixed(0);
    }
  }

  /**
   * Returns the corresponding tooltip text for various metrics based on the provided column name.
   *
   * @param column The name of the column.
   *
   * @returns The tooltip text associated with the column name.
   */
  getMetricName(column: string): string {
    if (column === 'className') return 'Class Identifier'
    else if (column === 'TP') return 'True Positives'
    else if (column === 'FP') return 'False Positives'
    else if (column === 'FN') return 'False Negatives'
    else if (column === 'RC') return 'Recall'
    else if (column === 'PR') return 'Precision'
    else if (column === 'AP') return 'Average Precision'
    else return ''
  }

  /**
   * Opens the misclassification viewer page in a new tab.
   *
   * @param runId The ID of the run.
   * @param element An array containing data elements.
   * @param className The name of the class.
   * @param modelName The name of the model.
   */
  openMisclassificationViewerPage(runId: string, element: unknown[], className: string, modelName: string) {
    const name = this.getMetricValue(element, className);

    // Construct the URL for the misclassification viewer page
    const url = ['/misclassification-viewer', runId, name, modelName];
    const urlTree = this.router.createUrlTree(url);

    // Open the URL in a new tab
    window.open(this.router.serializeUrl(urlTree), '_blank');
  }
}
