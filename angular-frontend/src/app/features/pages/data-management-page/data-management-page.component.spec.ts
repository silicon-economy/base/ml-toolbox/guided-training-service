/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ComponentFixture, discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {TimePastPipe} from 'ng-time-past-pipe';
import {Observable, of, throwError} from 'rxjs';
import {DataManagementService, DATASET_TYPES, DatasetResponse} from 'src/app/core/backend/data-management.service';
import {
  ResponsiveImageCollectionComponent
} from 'src/app/core/components/responsive-image-collection/responsive-image-collection.component';
import {Dataset, DatasetTag} from 'src/app/core/models/dataset';
import {TooltiplistPipe} from 'src/app/shared/pipes/tooltiplist/tooltiplist.pipe';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {
  AddNewDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/add-new-dataset-dialog/add-new-dataset-dialog.component';

import {
  DataManagementPageComponent
} from 'src/app/features/pages/data-management-page/data-management-page.component';
import {
  DeleteDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/delete-dataset-dialog/delete-dataset-dialog.component';
import {
  DownloadDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/download-dataset-dialog/download-dataset-dialog.component';
import {
  AutoAnnotateDialogComponent
} from 'src/app/features/pages/data-management-page/auto-annotate-dialog/auto-annotate-dialog.component';
import {
  ShowPreviewImagesComponent
} from 'src/app/features/pages/data-management-page/show-preview-images/show-preview-images.component';

const DATASET_TAGS: DatasetTag[] = [
  {
    name: 'Public',
    colorClass: 'red-chip'
  },
  {
    name: 'Private',
    colorClass: 'red-chip'
  },
  {
    name: 'Annotated',
    colorClass: 'blue-chip'
  }
];

const DUMMY_DATASETS: Dataset[] = [
  {
    labelStudioProjectId: 1,
    name: 'Coco',
    datasetType: DATASET_TYPES[1],
    annotatedImages: 65000,
    totalImages: 65000,
    lastModified: new Date('2001-01-13'),
    previewImages: [
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1533473359331-0135ef1b58bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1594914778686-ef7909432545?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1592085198739-ffcad7f36b54?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1601002052861-8cd1485edf1f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1533646281814-761f53fc5483?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1587293852726-70cdb56c2866?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1587293852726-70cdb56c2866?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1587293852726-70cdb56c2866?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80'
    ],
    tags: [DATASET_TAGS[0], DATASET_TAGS[2]],
    classes: ['human', 'car', 'dog'],
    labelStudioUrl: "https://example.org/projects/1/data",
  },
  {
    labelStudioProjectId: 2,
    name: 'Loco',
    datasetType: DATASET_TYPES[0],
    annotatedImages: 12000,
    totalImages: 12000,
    lastModified: new Date('2022-03-04'),
    previewImages: [
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1592085198739-ffcad7f36b54?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1601002052861-8cd1485edf1f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1594914778686-ef7909432545?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1587293852726-70cdb56c2866?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80'
    ],
    tags: [DATASET_TAGS[0], DATASET_TAGS[2]],
    classes: ['human', 'car', 'dog'],
    labelStudioUrl: "https://example.org/projects/2/data",
  },
  {
    labelStudioProjectId: 3,
    name: 'Imagenet',
    datasetType: DATASET_TYPES[0],
    annotatedImages: 1400000,
    totalImages: 1400000,
    lastModified: new Date('2022-03-03'),
    previewImages: [
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1592085198739-ffcad7f36b54?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1601002052861-8cd1485edf1f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1594914778686-ef7909432545?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1587293852726-70cdb56c2866?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80'
    ],
    tags: [DATASET_TAGS[0]],
    classes: ['human', 'car', 'dog'],
    labelStudioUrl: "https://example.org/projects/3/data",
  },
  {
    labelStudioProjectId: 4,
    name: 'My Warehouse',
    datasetType: DATASET_TYPES[0],
    annotatedImages: 4000,
    totalImages: 4000,
    lastModified: new Date('2022-02-04'),
    previewImages: [
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1592085198739-ffcad7f36b54?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80',
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1601002052861-8cd1485edf1f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
    ],
    tags: [DATASET_TAGS[1]],
    classes: ['human', 'car', 'dog'],
    labelStudioUrl: "https://example.org/projects/4/data",
  },
  {
    labelStudioProjectId: 5,
    name: 'Transport',
    datasetType: DATASET_TYPES[1],
    annotatedImages: 500,
    totalImages: 500,
    lastModified: new Date('2022-02-01'),
    previewImages: [
      // eslint-disable-next-line max-len
      'https://images.unsplash.com/photo-1587293852726-70cdb56c2866?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80'
    ],
    tags: [DATASET_TAGS[1], DATASET_TAGS[2]],
    classes: ['human', 'car', 'dog'],
    labelStudioUrl: "https://example.org/projects/5/data",
  },
];

export class MdDialogMock {

  open() {
    return {
      afterClosed: () => of(true)
    };
  }
}

describe('DataManagementPageComponent', () => {
  let component: DataManagementPageComponent;
  let fixture: ComponentFixture<DataManagementPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatTableModule,
        MatChipsModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSortModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatCardModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatIconModule,
        TimePastPipe,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      declarations: [DataManagementPageComponent, ResponsiveImageCollectionComponent, TooltiplistPipe],
      providers: [
        ToasterService,
        DataManagementService,
        {
          provide: MatDialog, useClass: MdDialogMock,
        },
      ]
    })
      .compileComponents();
  });

  let getDatasetsSpy: jasmine.Spy<(filter: string, sortName: string, sortOrder: string, pageNumber: number, pageSize: number)
    => Observable<DatasetResponse>>;

  beforeEach(
    inject([DataManagementService], (dataManagementService: DataManagementService) => {


      // Prepare
      // By default we return a dummy set of Datasets
      const testDatasetsResponse: DatasetResponse = {
        totalDatasets: 50,
        datasets: DUMMY_DATASETS
      };
      getDatasetsSpy = spyOn(dataManagementService, 'getDatasets').and.returnValue(of(testDatasetsResponse));

      fixture = TestBed.createComponent(DataManagementPageComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      // verify that the datasets are requested upon each creation of an instance
      expect(getDatasetsSpy).withContext('should initially load datasets from service')
        .toHaveBeenCalledOnceWith('', 'lastModified', 'desc', 0, 10);
      getDatasetsSpy.calls.reset();
    }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update totalDatasets after initial datasets request',
    inject([DataManagementService], (dataManagementService: DataManagementService) => {

      expect(component.totalDatasets).toEqual(50);
    }));

  describe('Datasets table filter', () => {

    it('should reset and reload datasets from service on clearFilter',
      inject([DataManagementService], (dataManagementService: DataManagementService) => {

        // Prepare

        // The paginator's pageSize should be kept unchanged
        component.paginator.pageSize = 20;
        // The paginator's pageIndex should be reset
        component.paginator.pageIndex = 5;
        // The filter should reset
        component.filterInput.nativeElement.value = 'filter text';

        // Act
        component.clearFilter();

        // Verify
        expect(dataManagementService.getDatasets).toHaveBeenCalledOnceWith('', 'lastModified', 'desc', 0, 20);
      }));

    it('should request datasets from service on filter change after debounce time',
      fakeAsync(inject([DataManagementService], (dataManagementService: DataManagementService) => {
        // Prepare

        // The paginator's pageSize should be kept unchanged
        component.paginator.pageSize = 20;
        // The paginator's pageIndex should be reset
        component.paginator.pageIndex = 5;

        // Act
        component.filterInput.nativeElement.value = "filter text";
        component.filterInput.nativeElement.dispatchEvent(new KeyboardEvent("keyup"));
        fixture.detectChanges();
        tick(150); // wait for debounce
        discardPeriodicTasks(); // discard remaining tasks

        // Verify
        expect(dataManagementService.getDatasets).toHaveBeenCalledOnceWith("filter text", "lastModified", "desc", 0, 20)
      })));

    it('should not immediatly request datasets from service on filter change',
      fakeAsync(inject([DataManagementService], (dataManagementService: DataManagementService) => {
        // Prepare


        // Act
        component.filterInput.nativeElement.value = 'filter text';
        component.filterInput.nativeElement.dispatchEvent(new KeyboardEvent('keyup'));
        fixture.detectChanges();
        tick(50); // wait for less than debounce time
        discardPeriodicTasks(); // discard remaining tasks

        // Verify
        expect(dataManagementService.getDatasets).toHaveBeenCalledTimes(0);
      })));

  });

  describe('Datatable pages', () => {

    it('should request datasets from service on page change',
      inject([DataManagementService], (dataManagementService: DataManagementService) => {
        // Prepare
        // Act
        expect(component.paginator.hasNextPage()).withContext("Datatable has next page").toBeTrue();
        component.paginator.nextPage();
        fixture.detectChanges();
        // Verify
        expect(dataManagementService.getDatasets).toHaveBeenCalledOnceWith("", "lastModified", "desc", 1, 10)
        expect(component.paginator.hasNextPage()).withContext("Datatable has next page").toBeTrue(); // in total 5 pages
      }));

    it('should request datasets from service after navigating to last page',
      inject([DataManagementService], (dataManagementService: DataManagementService) => {
        // Prepare
        // Act
        component.paginator.lastPage();
        fixture.detectChanges();
        // Verify
        expect(dataManagementService.getDatasets).toHaveBeenCalledOnceWith("", "lastModified", "desc", 4, 10)
        expect(component.paginator.hasNextPage()).withContext("Datatable has no next page").toBeFalse();
      }));
  });


  it('should request datasets from service on sort change',
    inject([DataManagementService], (dataManagementService: DataManagementService) => {

      // Act
      // click on one of the sort header buttons
      const sortButtons = fixture.debugElement.nativeElement.querySelectorAll('.mat-sort-header-container');
      sortButtons[0].click(); // asc
      getDatasetsSpy.calls.reset();
      sortButtons[0].click(); // desc
      fixture.detectChanges();

      // Verify
      expect(dataManagementService.getDatasets).toHaveBeenCalledOnceWith("", "name", "desc", 0, 10)
    }));

  it('should open add dialog', inject([MatDialog], (matdialog: MatDialog) => {
    const dialogSpy = spyOn(matdialog, 'open').and.callThrough();
    component.addNewDataset();
    expect(dialogSpy).toHaveBeenCalledOnceWith(AddNewDatasetDialogComponent, {
      height: '80%',
      width: '50%',
      disableClose: true
    });
  }));

  it('should open dialog for preview images', inject([MatDialog], (matDialog: MatDialog) => {
    const dialogSpy = spyOn(matDialog, 'open').and.callThrough();
    component.showPreviewImages(DUMMY_DATASETS[0]);
    expect(dialogSpy).toHaveBeenCalledWith(ShowPreviewImagesComponent, {
      height: '80%',
      width: '50%',
      data: DUMMY_DATASETS[0].labelStudioProjectId,
      disableClose: true
    })
  }));

  describe('Delete Dialog', () => {
    it('should open the dialog and delte the dataset if the dialog is confirmed',
      inject([MatDialog, DataManagementService, ToasterService], (matdialog: MatDialog, dataManagementService: DataManagementService, toasterService: ToasterService) => {
        const deleteDatasetsSpy = spyOn(dataManagementService, 'deleteDataset').and.returnValue(of({}));
        const toastSuccessSpy = spyOn(toasterService, 'success');
        const dialogSpy = spyOn(matdialog, 'open').and
          .returnValue(<MatDialogRef<typeof DeleteDatasetDialogComponent>>{afterClosed: () => of(true)});

        component.deleteDataset(DUMMY_DATASETS[0]);

        expect(dialogSpy).toHaveBeenCalledOnceWith(DeleteDatasetDialogComponent, {data: DUMMY_DATASETS[0]});
        expect(deleteDatasetsSpy).toHaveBeenCalledOnceWith(DUMMY_DATASETS[0]);
        expect(toastSuccessSpy).toHaveBeenCalledOnceWith(`Dataset ${DUMMY_DATASETS[0].name} was deleted.`, 'Dataset deleted');
      }));

    it('should show an error if the deletion of the dataset fails',
      inject([MatDialog, DataManagementService, ToasterService],
        (matdialog: MatDialog, dataManagementService: DataManagementService, toasterService: ToasterService) => {
          const deleteDatasetsSpy = spyOn(dataManagementService, 'deleteDataset').and.returnValue(throwError(() => new Error()));
          const toastErrorSpy = spyOn(toasterService, 'error');

          const dialogSpy = spyOn(matdialog, 'open').and
            .returnValue(<MatDialogRef<typeof DeleteDatasetDialogComponent>>{afterClosed: () => of(true)});
          component.deleteDataset(DUMMY_DATASETS[0]);
          expect(dialogSpy).toHaveBeenCalledOnceWith(DeleteDatasetDialogComponent, {data: DUMMY_DATASETS[0]});
          expect(deleteDatasetsSpy).toHaveBeenCalledOnceWith(DUMMY_DATASETS[0]);
          expect(toastErrorSpy).toHaveBeenCalledOnceWith(`Dataset ${DUMMY_DATASETS[0].name} could not be deleted.`,
            'Failed to delete Dataset');
        }));

    it('should not delete the dataset if the dialog is not confirmed',
      inject([MatDialog, DataManagementService, ToasterService],
        (matdialog: MatDialog, dataManagementService: DataManagementService, toasterService: ToasterService) => {
          const deleteDatasetsSpy = spyOn(dataManagementService, 'deleteDataset');
          const toastSuccessSpy = spyOn(toasterService, 'success');

          const dialogSpy = spyOn(matdialog, 'open').and
            .returnValue(<MatDialogRef<typeof DeleteDatasetDialogComponent>>{afterClosed: () => of(false)});
          component.deleteDataset(DUMMY_DATASETS[0]);
          expect(dialogSpy).toHaveBeenCalledOnceWith(DeleteDatasetDialogComponent, {data: DUMMY_DATASETS[0]});
          expect(deleteDatasetsSpy).toHaveBeenCalledTimes(0);
          expect(toastSuccessSpy).toHaveBeenCalledTimes(0);
        }));
  });

  describe('Download Dataset Annotations', () => {

    it('should show an error toast message if the dataset can not be downloaded',
      fakeAsync(inject([ToasterService, DataManagementService, MatDialog],
        (toasterService: ToasterService, dataManagementService: DataManagementService, matdialog: MatDialog) => {
          const toastErrorSpy = spyOn(toasterService, 'error');
          const exportAnnotationSpy = spyOn(dataManagementService, 'exportDatasetAnnotation').and
            .returnValue(throwError(() => new Error('Something bad happend')));
          const downloadSpy = spyOn(component, '_downloadFile');
          const dialogSpy = spyOn(matdialog, 'open').and
            .returnValue(<MatDialogRef<typeof DownloadDatasetDialogComponent>>{afterClosed: () => of(true)});

          component.downloadDataset(DUMMY_DATASETS[0]);

          expect(dialogSpy).toHaveBeenCalledOnceWith(DownloadDatasetDialogComponent, {data: DUMMY_DATASETS[0]});
          expect(exportAnnotationSpy).toHaveBeenCalledOnceWith(DUMMY_DATASETS[0]);
          expect(downloadSpy).toHaveBeenCalledTimes(0);
          expect(toastErrorSpy).toHaveBeenCalledOnceWith('Failed to request download url for exported annotations.');
        })));

    it('should open a dialog asking for confirmation on annotations and start download and show a confirmation',
      fakeAsync(inject([ToasterService, DataManagementService, MatDialog],
        (toasterService: ToasterService, dataManagementService: DataManagementService, matdialog: MatDialog) => {
          const toastSucessSpy = spyOn(toasterService, 'success');
          const exportAnnotationSpy = spyOn(dataManagementService, 'exportDatasetAnnotation').and
            .returnValue(of('SomeURL'));
          const downloadSpy = spyOn(component, '_downloadFile');
          const dialogSpy = spyOn(matdialog, 'open').and
            .returnValue(<MatDialogRef<typeof DownloadDatasetDialogComponent>>{afterClosed: () => of(true)});

          component.downloadDataset(DUMMY_DATASETS[0]);

          expect(dialogSpy).toHaveBeenCalledOnceWith(DownloadDatasetDialogComponent, {data: DUMMY_DATASETS[0]});
          expect(exportAnnotationSpy).toHaveBeenCalledOnceWith(DUMMY_DATASETS[0]);
          expect(downloadSpy).toHaveBeenCalledOnceWith('SomeURL');
          expect(toastSucessSpy).toHaveBeenCalledOnceWith(`Downloading dataset annotations for '${DUMMY_DATASETS[0].name}'.`);
        })));

    it('should open a dialog asking for confirmation on annotations and abort',
      fakeAsync(inject([ToasterService, DataManagementService, MatDialog],
        (toasterService: ToasterService, dataManagementService: DataManagementService, matdialog: MatDialog) => {
          const dialogSpy = spyOn(matdialog, 'open').and
            .returnValue(<MatDialogRef<typeof DownloadDatasetDialogComponent>>{afterClosed: () => of(false)});
          const exportAnnotationSpy = spyOn(dataManagementService, 'exportDatasetAnnotation').and.returnValue(of('SomeURL'));
          const toastSucessSpy = spyOn(toasterService, 'success');
          const downloadSpy = spyOn(component, '_downloadFile');

          component.downloadDataset(DUMMY_DATASETS[0]);

          expect(dialogSpy).toHaveBeenCalledOnceWith(DownloadDatasetDialogComponent, {data: DUMMY_DATASETS[0]});
          expect(exportAnnotationSpy).toHaveBeenCalledTimes(0);
          expect(toastSucessSpy).toHaveBeenCalledTimes(0);
          expect(downloadSpy).toHaveBeenCalledTimes(0);
        })));
  });

  it('should open a new page with label studio', () => {
    const windowOpenSpy = spyOn(window, 'open');

    component.openLabelstudio(DUMMY_DATASETS[0]);

    expect(windowOpenSpy).toHaveBeenCalledOnceWith(`https://example.org/projects/${DUMMY_DATASETS[0].labelStudioProjectId}/data`, '_blank');
  });

  it('should open auto-annotate dialog', inject([MatDialog], (matDialog: MatDialog) => {
    // Mock dataset for testing
    const dataset: Dataset = {
      labelStudioProjectId: 1,
      name: 'test',
      datasetType: {value: '', viewValue: ''},
      annotatedImages: 0,
      totalImages: 0,
      lastModified: new Date(),
      previewImages: [],
      tags: [],
      classes: [],
      labelStudioUrl: "url to labelstudio",
    };

    // Create a spy for the MatDialog's open method and ensure it is called through
    const openSpy = spyOn(matDialog, 'open').and.callThrough();

    // Call the autoAnnotateDialog method with the mock dataset
    component.autoAnnotateDialog(dataset);

    // Expect the open method to have been called with the AutoAnnotateDialogComponent
    // and specific configuration options, including data with the dataset
    expect(openSpy).toHaveBeenCalledWith(AutoAnnotateDialogComponent, {
      height: '80%',
      width: '50%',
      data: {dataset},
      disableClose: true,
    });
  }))

});

