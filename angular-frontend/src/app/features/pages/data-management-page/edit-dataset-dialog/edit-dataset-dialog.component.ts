/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {Dataset, DatasetTag} from 'src/app/core/models/dataset';
import {UploadFilePickerAdapter} from 'src/app/core/services/file-upload/upload-file-picker.adapter';

import {BehaviorSubject, finalize} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatStepper} from '@angular/material/stepper';
import {MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {DataManagementService, TaskTypeAnnotationFormats} from 'src/app/core/backend/data-management.service';
import {MatTableDataSource} from '@angular/material/table';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {FileStatusService} from "src/app/shared/file-status.service";

export interface UploadDragDropFile {
  name: string;
  size: number;
  type: string;
  icon: string;
  statusIcon: string;
}

@Component({
  selector: 'app-edit-dataset-dialog',
  templateUrl: './edit-dataset-dialog.component.html',
  styleUrls: ['./edit-dataset-dialog.component.scss']
})
export class EditDatasetDialogComponent implements OnInit {

  displayedColumns: string[] = ['name', 'statusIcon', 'type', 'remove'];
  dataSource = new MatTableDataSource<UploadDragDropFile>([]);
  selectedFiles: File[] = [];
  uploadFileList: UploadDragDropFile[] = [];
  uploadIndicator: string;
  addOnBlur = true;

  datasetNames: string[];
  selectedDataset: Dataset;
  basicInfoFormGroup: FormGroup;
  duplicatedName = false;
  loadingDatasets;

  separatorKeysCodes: number[] = [ENTER, COMMA];

  tags: DatasetTag[] = [];
  classes: string[] = [];
  tagsCtrl = new FormControl();
  classCtrl = new FormControl();

  datasetSubject: BehaviorSubject<Dataset>;
  uploadingAnnotations = false;

  annotationFormatSubject: BehaviorSubject<string>;
  annotationUploadBlockedSubject = new BehaviorSubject<boolean>(false);

  annotationAdapter: UploadFilePickerAdapter;

  availableTaskTypeAnnotationFormats: TaskTypeAnnotationFormats[];

  uploadAnnotationFormGroup = new FormGroup({
    formatCtrl: new FormControl({
      value: '',
      disabled: this.annotationUploadBlockedSubject.getValue()
    }, [Validators.required])
  });


  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  @ViewChild('stepper') stepper: MatStepper;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<EditDatasetDialogComponent>,
    private _formBuilder: FormBuilder,
    private dataManagementService: DataManagementService,
    private http: HttpClient,
    private toasterService: ToasterService,
    public dialog: MatDialog,
    public fileStatusService: FileStatusService) {

    dataManagementService.getAllTaskTypeAnnotationFormats().subscribe((val) => {
      this.availableTaskTypeAnnotationFormats = val;
    });
  }

  ngOnInit(): void {
    // This is necessary to create a simple deep copy of the dataset
    // Otherwise the dataset would also be edited in the parent component
    // TODO: Use more efficient deep copy method in the future

    this.getAllDatasets();
    this.selectedDataset = JSON.parse(JSON.stringify(this.data.dataset));

    this.datasetSubject = new BehaviorSubject<Dataset>(this.selectedDataset);
    this.annotationFormatSubject = new BehaviorSubject<string>('');
    this.annotationUploadBlockedSubject = new BehaviorSubject<boolean>(false);

    this.annotationAdapter = new UploadFilePickerAdapter(this.http, this.datasetSubject, this.annotationFormatSubject,
      this.annotationUploadBlockedSubject, this.toasterService);

    this.classes = this.selectedDataset.classes;
    this.tags = this.selectedDataset.tags;

    this.basicInfoFormGroup = this._formBuilder.group({
      datasetNameCtrl: [this.selectedDataset.name, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(200),
        Validators.pattern(/^\S(.*\S)?$/),
        this.duplicateNameValidator.bind(this)]],
      classCtrl: [{value: '', disabled: true}],
      tagsCtrl: [this.selectedDataset.tags],
    });

    this.uploadAnnotationFormGroup.get('formatCtrl').valueChanges.subscribe(value => {
      this.annotationFormatSubject.next(value);
    });
    this.dataSource = new MatTableDataSource<UploadDragDropFile>(this.uploadFileList);
  }

  /**
   * Handles the file selection event.
   *
   * @param event The file selection event that triggered the function.
   */
  // Validator-function to avoid duplicate dataset names
  duplicateNameValidator(datasetNameCtrl: FormControl) {
    let isDuplicate;
    if (this.loadingDatasets === false) {
      if (datasetNameCtrl.value === this.selectedDataset.name) {
        isDuplicate = false;
      } else {
        isDuplicate = this.datasetNames.includes(datasetNameCtrl.value);
      }
    }
    this.duplicatedName = isDuplicate;
    return isDuplicate ? {duplicate: true} : null;
  }

  getAllDatasets() {
    this.loadingDatasets = true;
    this.dataManagementService.getDatasets('', 'lastModified', 'desc', 0, 500).pipe(finalize(() => {
      this.loadingDatasets = false;
    })).subscribe({
      next: (res) => {
        this.datasetNames = res.datasets.map(dataset => dataset.name);
      },
      error: () => {
        this.toasterService.error(`Failed to load datasets`, 'Error loading datasets');
      }
    });
  }

  onFileSelected(event) {
    for (let i = 0; i < event.currentTarget.files?.length; i++) {
      if (!this.checkDuplicateFile(event.currentTarget.files[i])) {
        if (event.currentTarget.files[i].type == ('application/json' || 'text/csv')) {
          this.selectedFiles.push(event.currentTarget.files[i]);
        }
      }
    }
    this.generateTableFiles();
  }

  /**
   * Prevents the default behavior of the drag event.
   *
   * @param event The drag event that triggered the function.
   */
  onDragLeave(event) {
    event.preventDefault();
  }

  /**
   * Prevents the default behavior of the drag event.
   *
   * @param event The drag event that triggered the function.
   */
  onDragOver(event) {
    event.preventDefault();
  }

  /**
   * Allows the user to drop files into the drag and drop area to upload them.
   *
   * @param event The event that fired the function.
   */
  onDrop(event) {
    for (let i = 0; i < event.dataTransfer.files?.length; i++) {
      if (!this.checkDuplicateFile(event.dataTransfer.files[i])) {
        if (event.dataTransfer.files[i].type == ('application/json' || 'text/csv')) {
          this.selectedFiles.push(event.dataTransfer.files[i]);
        }
      }
    }
    event.preventDefault();
    this.generateTableFiles();
  }

  /**
   * Removes a file from the selected files.
   *
   * @param element The file to remove.
   */
  remove(element: UploadDragDropFile | File) {
    this.selectedFiles.splice(this.selectedFiles.findIndex(file => file.name === element.name), 1);
    this.generateTableFiles();
  }

  /**
   * Generates the files table for all selected files.
   */
  generateTableFiles() {
    this.uploadFileList = [];
    // Show selected files without statusIcon
    for (let i = 0; i < this.selectedFiles?.length; i++) {
      const fileSize = Number((this.selectedFiles[i].size / 1000 / 1000).toFixed(2));
      this.uploadFileList.push({
        name: this.selectedFiles[i].name,
        size: fileSize,
        type: this.selectedFiles[i].type.split('/')[1],
        icon: 'delete',
        statusIcon: ''
      });
    }
    this.dataSource = new MatTableDataSource<UploadDragDropFile>(this.uploadFileList);
  }

  /**
   * Checks if the selected file is a duplicate of an already selected file.
   *
   * @param file The file that should be checked for duplicates.
   * @returns True if the file is a duplicate, false otherwise.
   */
  checkDuplicateFile(file: File): boolean {
    for (let i = 0; i < this.selectedFiles?.length; i++) {
      if (this.selectedFiles[i].name === file.name) {
        return true;
        // Only one annotation file is allowed, so take latest
      } else if (this.selectedFiles[i].type == ('application/json' || 'text/csv')) {
        this.selectedFiles[i] = file;
        return true;
      }
    }
    return false;
  }

  /**
   * Adds a tag to the dataset.
   *
   * @param event The event that triggered the function.
   */
  addTag(event: MatChipInputEvent): void {
    const value = event.value;

    if (value) {
      const tag = <DatasetTag>{
        name: value,
        colorClass: 'orange-chip'
      };
      this.tags.push(tag);
    }

    // Clear the input value
    event.chipInput.clear();

    this.tagsCtrl.setValue(null);
  }

  /**
   * Removes a tag from the dataset.
   *
   * @param tag The tag to remove.
   */
  removeTag(tag: DatasetTag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  /**
   * Uploads the selected files to the dataset.
   */
  async upload() {
    let fileIndex;
    this.uploadingAnnotations = true;

    for (let i = 0; i < this.selectedFiles.length; i++) {
      try {
        this.uploadIndicator = '(' + (i) + '/' + this.selectedFiles.length + ')';
        fileIndex = this.uploadFileList.findIndex(f => f.name === this.selectedFiles[i].name);
        const s3URL = await this.annotationAdapter.getRequestURL(this.selectedFiles[i]);
        await this.annotationAdapter.uploadToS3(s3URL, this.selectedFiles[i]);
        this.uploadFileList[fileIndex].statusIcon = 'checked';
      } catch {
        this.uploadFileList[fileIndex].statusIcon = 'dangerous';
      }
    }
    this.dataSource = new MatTableDataSource<UploadDragDropFile>(this.uploadFileList);
    this.uploadIndicator = 'finishing';

  }

  /**
   * Edits the dataset.
   */
  editDataset(): void {
    const datasetName: string = this.basicInfoFormGroup.get('datasetNameCtrl').value;
    this.dataManagementService.editDataset(this.selectedDataset.labelStudioProjectId, datasetName, this.tags)
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      .subscribe({
        next: dataset => {
          this.toasterService.success(`Edited dataset '${dataset['name']}'`, 'Edited dataset');
          if (this.annotationUploadBlockedSubject.getValue()) {
            this.dataManagementService.importAnnotation(this.selectedDataset).pipe(finalize(() => {
            })).subscribe({
              next: response => {
                this.toasterService.success(`Successfully imported ${response['imported_annotations']} annotations.`, 'Annotations imported');
                this.dialogRef.close(true);
              },
              error: (e) => {
                this.toasterService.error(`An error occured while importing the annotation. (Error: ${e.error.message})`,
                  'Annotation import error');
                this.dialogRef.close(true);
              }
            });
          } else {
            this.dialogRef.close(true);
          }
        },
        error: () => {
          this.toasterService.error(`Failed to edit the dataset '${this.selectedDataset.name}'.`, 'Error editing dataset');
          this.dialogRef.close(true);
        }
      });

  }

  /**
   * Uploads the selected files and saves the dataset.
   */
  async uploadAndSave() {
    // Check if valid inputs have been made
    if (!this.basicInfoFormGroup.valid) {
      this.stepper.previous();
      this.basicInfoFormGroup.markAllAsTouched();
    } else {
      this.fileStatusService.updateStatusIcon(this.selectedFiles, this.uploadFileList, 'cached');
      await this.upload();
      this.fileStatusService.updateStatusIcon(this.selectedFiles, this.uploadFileList, 'sync_alt');
      this.editDataset();
    }
  }

  /**
   * Gets the status tooltip for the given status icon.
   *
   * @param statusIcon The status icon to get the tooltip for.
   */
  getStatusTooltip(statusIcon: string): string {
    return this.fileStatusService.getStatusTooltip(statusIcon);
  }
}

