/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ENTER} from '@angular/cdk/keycodes';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatOptionModule} from '@angular/material/core';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {of, throwError} from 'rxjs';
import {
  DataManagementService,
  DATASET_TYPES,
  TaskTypeAnnotationFormats
} from 'src/app/core/backend/data-management.service';
import {Dataset, DatasetTag} from 'src/app/core/models/dataset';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {
  EditDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/edit-dataset-dialog/edit-dataset-dialog.component';
import {HttpResponse} from "@angular/common/http";
import {UploadDragDropFile} from "../add-new-dataset-dialog/add-new-dataset-dialog.component";

const DATASET_TAGS: DatasetTag[] = [
  {
    name: 'Public',
    colorClass: 'red-chip'
  },
  {
    name: 'Private',
    colorClass: 'red-chip'
  },
  {
    name: 'Annotated',
    colorClass: 'blue-chip'
  }
];

const TASK_TYPE_ANNOTATION_FORMATS: TaskTypeAnnotationFormats[] = [
  {
    taskType: 'classification',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
    ]
  },
  {
    taskType: 'object_detection',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
      {value: 'coco_json', viewValue: 'COCO JSON'},
    ]
  },
  {
    taskType: 'rotated_object_detection',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
    ]
  },
  {
    taskType: 'instance_segmentation',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
      {value: 'coco_json', viewValue: 'COCO JSON'},
    ]
  },
  {
    taskType: 'text_recognition',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
    ]
  },
];

const DUMMY_DATASET = <Dataset>{
  labelStudioProjectId: 1,
  name: 'MyDataset',
  datasetType: DATASET_TYPES[0],
  tags: [],
  classes: [],
  lastModified: new Date(),
  annotatedImages: 0,
  totalImages: 0,
  previewImages: [],
};

describe('EditDatasetDialogComponent', () => {
  let component: EditDatasetDialogComponent;
  let fixture: ComponentFixture<EditDatasetDialogComponent>;

  let dataManagementServiceSpy: jasmine.SpyObj<DataManagementService>;
  let dialogRefSpy: jasmine.SpyObj<MatDialogRef<EditDatasetDialogComponent>>;

  beforeEach(async () => {
    dataManagementServiceSpy = jasmine.createSpyObj<DataManagementService>('DataManagementServiceSpy',
      ['getAllTaskTypeAnnotationFormats', 'editDataset', 'importAnnotation', 'getDatasets']
    );
    dialogRefSpy = jasmine.createSpyObj<MatDialogRef<EditDatasetDialogComponent>>('EditDatasetDialogComponentSpy', ['close']);

    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        MatSelectModule,
        MatChipsModule,
        MatStepperModule,
        MatOptionModule,
        MatIconModule,
        MatTooltipModule,
        HttpClientTestingModule,
        MatCardModule,
        MatTableModule,


      ],
      declarations: [EditDatasetDialogComponent],
      providers: [
        ToasterService,
        {provide: MAT_DIALOG_DATA, useValue: {dataset: DUMMY_DATASET}},
        {
          provide: MatDialogRef,
          useValue: dialogRefSpy
        },
        {
          provide: DataManagementService,
          useValue: dataManagementServiceSpy
        }
      ]
    })
      .compileComponents();

    dataManagementServiceSpy.getDatasets.and.returnValue(of({datasets: [], totalDatasets: 0}));
    dataManagementServiceSpy.getAllTaskTypeAnnotationFormats.and.returnValue(of(TASK_TYPE_ANNOTATION_FORMATS));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDatasetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  //TODO: test the importAnnotationmethod
  it('should display a message after editing the dataset and close the dialog',
    inject([ToasterService], (toasterService: ToasterService) => {
      const dummyDataset = <Dataset>{
        labelStudioProjectId: 1,
        name: 'MyDataset',
        datasetType: DATASET_TYPES[0],
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
      };
      // Prepare
      dataManagementServiceSpy.editDataset.and.returnValue(of(dummyDataset));
      dataManagementServiceSpy.importAnnotation.and.returnValue(of({'imported_annotations': 37}));
      const toastSucessSpy = spyOn(toasterService, 'success');
      component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
      component.basicInfoFormGroup.controls['tagsCtrl'].setValue(DATASET_TAGS[0]);
      component.annotationUploadBlockedSubject.next(true);

      // Act
      component.editDataset();

      // Verify
      expect(dataManagementServiceSpy.editDataset).toHaveBeenCalledOnceWith(1, 'MyDataset', []);
      expect(dataManagementServiceSpy.importAnnotation).toHaveBeenCalled();
      expect(toastSucessSpy).toHaveBeenCalledWith(`Edited dataset 'MyDataset'`, 'Edited dataset');
      expect(toastSucessSpy).toHaveBeenCalledWith('Successfully imported 37 annotations.', 'Annotations imported');
      expect(dialogRefSpy.close).toHaveBeenCalledOnceWith(true);
    })
  );

  it('should display a validation error after  writing a dataset name that already exists',
    () => {
      // Prepare
      component.loadingDatasets = false;
      component.selectedDataset = <Dataset>{
        labelStudioProjectId: 1,
        name: 'SelectedDataset',
        datasetType: DATASET_TYPES[0],
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
      };
      component.datasetNames.push('MyDataset');

      // Act
      component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');

      // Verify
      expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
    });

  it('should display a validation error if a dataset name is entered with an extra space at the beginning or end',
    () => {
      // Act: Set a value with a leading space
      component.basicInfoFormGroup.get('datasetNameCtrl').setValue(' MyDataset');
      // Verify: The control should be invalid
      expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
      // Verify: There should be a pattern error
      expect(component.basicInfoFormGroup.get('datasetNameCtrl').hasError('pattern')).toBeTruthy();

      // Act: Set a value with a trailing space
      component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset ');
      // Verify: The control should be invalid
      expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
      // Verify: There should be a pattern error
      expect(component.basicInfoFormGroup.get('datasetNameCtrl').hasError('pattern')).toBeTruthy();

      // Act: Set a value without leading or trailing spaces
      component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
      // Verify: The control should be valid
      expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeTruthy();
    });

  it('should display an error if saving failed',
    inject([ToasterService], (toasterService: ToasterService) => {
      // Prepare
      const toastErrorSpy = spyOn(toasterService, 'error');
      dataManagementServiceSpy.editDataset.and.returnValue(throwError(() => new Error('Something went wrong :(')));
      component.basicInfoFormGroup.get('tagsCtrl').setValue(DATASET_TAGS[0]);

      // Act
      component.editDataset();

      // Verify
      expect(dataManagementServiceSpy.editDataset).toHaveBeenCalledOnceWith(1, 'MyDataset', []);
      expect(toastErrorSpy).toHaveBeenCalledOnceWith(`Failed to edit the dataset '${DUMMY_DATASET.name}'.`, 'Error editing dataset');
      expect(dialogRefSpy.close).toHaveBeenCalledTimes(1);
    })
  );


  describe('The Tag input', () => {

    it('should add a tag to the tags array', fakeAsync(() => {
      // Act
      component.tagInput.nativeElement.value = 'newtag';
      component.tagInput.nativeElement.dispatchEvent(new KeyboardEvent('keydown', {
        code: 'Enter', key: 'Enter', keyCode: ENTER, charCode: ENTER, bubbles: true
      }));
      tick(20); // wait for debounce
      // Verify
      expect(component.tags.length).toEqual(1);
      expect(component.tags[0].name).toEqual('newtag');
    }));

    it('should not add empty tag on enter', fakeAsync(() => {
      // Act
      component.tagInput.nativeElement.value = '';
      component.tagInput.nativeElement.dispatchEvent(new KeyboardEvent('keydown', {
        code: 'Enter', key: 'Enter', keyCode: ENTER, charCode: ENTER, bubbles: true
      }));
      tick(20); // wait for debounce
      // Verify
      expect(component.tags).toEqual([]);
    }));

    it('should remove only one tag', () => {
      component.tags = [DATASET_TAGS[0], DATASET_TAGS[2]];
      fixture.detectChanges();

      // act
      component.removeTag(DATASET_TAGS[2]);

      // verify
      expect(component.tags).toEqual([DATASET_TAGS[0]]);
    });

    it('should add files to selectedFiles array if they are not duplicates and of type application/json or text/csv', () => {
      // Prepare
      const file1 = new File(['data'], 'file1.json', {type: 'application/json'});
      const file2 = new File(['data'], 'file2.csv', {type: 'text/csv'});
      const file3 = new File(['data'], 'file3.png', {type: 'image/png'});
      const event = {
        currentTarget: {
          files: [file1, file2, file3]
        }
      };
      // Act
      component.onFileSelected(event);
      // Verify
      expect(component.selectedFiles.length).toEqual(1);
      expect(component.selectedFiles).toContain(file1);
      expect(component.selectedFiles).toContain(file2);
    });

    it('should not add files to selectedFiles array if they are duplicates', () => {
      // Prepare
      const file1 = new File(['data'], 'file1.json', {type: 'application/json'});
      const file2 = new File(['data'], 'file5.json', {type: 'application/json'});
      const event = {
        currentTarget: {
          files: [file1, file2]
        }
      };
      spyOn(component, 'checkDuplicateFile').and.returnValue(true);
      // Act
      component.onFileSelected(event);
      // Verify
      expect(component.selectedFiles.length).toEqual(0);
    });

    it('should not add files to selectedFiles array if they are not of type application/json or text/csv', () => {
      // Prepare
      const file1 = new File(['data'], 'file1.png', {type: 'image/png'});
      const event = {
        currentTarget: {
          files: [file1]
        }
      };
      // Act
      component.onFileSelected(event);
      // Verify
      expect(component.selectedFiles.length).toEqual(0);
    });

    it('should add selected file to selectedFiles array if it does not exist', () => {
      // Prepare
      const mockFile = new File([''], 'test.json', {type: 'application/json'});
      spyOn(component, 'checkDuplicateFile').and.returnValue(false);
      // Act
      component.onFileSelected({currentTarget: {files: [mockFile]}});
      // Verify
      expect(component.selectedFiles.length).toBe(1);
      expect(component.selectedFiles[0]).toEqual(mockFile);
    });

    it('should not add selected file to selectedFiles array if it already exists', () => {
      // Prepare
      const mockFile = new File([''], 'test.json', {type: 'application/json'});
      component.selectedFiles = [mockFile];
      spyOn(component, 'checkDuplicateFile').and.returnValue(true);
      // Act
      component.onFileSelected({currentTarget: {files: [mockFile]}});
      // Verify
      expect(component.selectedFiles.length).toBe(1);
    });

    it('should prevent default on drag over event', () => {
      // Prepare
      const event = new Event('dragover');
      const preventDefaultSpy = spyOn(event, 'preventDefault');
      // Act
      component.onDragOver(event);
      // Verify
      expect(preventDefaultSpy).toHaveBeenCalled();
    });

    it('should prevent default on drag leave event', () => {
      // Prepare
      const event = new Event('dragleave');
      const preventDefaultSpy = spyOn(event, 'preventDefault');
      // Act
      component.onDragLeave(event);
      // Verify
      expect(preventDefaultSpy).toHaveBeenCalled();
    });

    it('should add selected files and prevent default on drop event', () => {
      // Prepare
      const mockFile1 = new File([''], 'file1.json', {type: 'application/json'});
      const mockEvent = {dataTransfer: {files: [mockFile1]}, preventDefault: jasmine.createSpy('preventDefault')};
      spyOn(component, 'checkDuplicateFile').and.returnValue(false);
      spyOn(component, 'generateTableFiles');
      // Act
      component.onDrop(mockEvent);
      // Verify
      expect(component.selectedFiles.length).toEqual(1);
      expect(component.selectedFiles[0]).toEqual(mockFile1);
      expect(mockEvent.preventDefault).toHaveBeenCalled();
      expect(component.generateTableFiles).toHaveBeenCalled();
    });

    it('should not add duplicate file to selectedFiles array', () => {
      // Prepare
      const mockFile = new File(['test'], 'test.png', {type: 'image/png'});
      component.selectedFiles = [mockFile];
      const mockEvent = {dataTransfer: {files: [mockFile]}, preventDefault: jasmine.createSpy('preventDefault')};
      spyOn(component, 'generateTableFiles');
      // Act
      component.onDrop(mockEvent);
      // Verify
      expect(component.selectedFiles.length).toBe(1);
      expect(mockEvent.preventDefault).toHaveBeenCalled();
      expect(component.generateTableFiles).toHaveBeenCalled();
    });

    it('should detect duplicate files', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.jpg'),
        new File([''], 'file2.jpg'),
      ];
      const duplicateFile = new File([''], 'file1.jpg');
      // Act
      const result = component.checkDuplicateFile(duplicateFile);
      // Verify
      expect(result).toEqual(true);
    });

    it('should update annotation file', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.json', {type: 'application/json'}),
        new File([''], 'file2.png', {type: 'image/png'}),
      ];
      const annotationFile = new File([''], 'file3.json', {type: 'application/json'});
      // Act
      const result = component.checkDuplicateFile(annotationFile);
      // Verify
      expect(result).toEqual(true);
      expect(component.selectedFiles[0]).toEqual(annotationFile);
    });

    it('should not detect duplicate files', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.jpg'),
        new File([''], 'file2.jpg'),
      ];
      const newFile = new File([''], 'file3.jpg');
      // Act
      const result = component.checkDuplicateFile(newFile);
      // Verify
      expect(result).toEqual(false);
    });

    it('should update selectedFiles with the latest file if it is a JSON or CSV file', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.json', {type: 'application/json'}),
      ];
      const newFile = new File([''], 'file2.json', {type: 'application/json'});
      // Act
      component.checkDuplicateFile(newFile);
      // Verify
      expect(component.selectedFiles).toEqual([newFile]);
    });

    it('should remove the element from selectedFiles', () => {
      // Prepare
      const file1 = new File([], 'file1.json', {type: 'application/json'});
      component.selectedFiles = [file1];
      // Act
      component.remove(file1);
      // Verify
      expect(component.selectedFiles).toEqual([]);
    });

    it('should call generateTableFiles()', () => {
      // Prepare
      spyOn(component, 'generateTableFiles');
      const file = <UploadDragDropFile>{name: 'file1', type: 'test'}
      // Act
      component.remove(file);
      // Verify
      expect(component.generateTableFiles).toHaveBeenCalled();
    });

    it('should upload files', async () => {
      // Prepare
      spyOn(component.annotationAdapter, 'getRequestURL').and.returnValue(Promise.resolve("https://example.com"));
      spyOn(component.annotationAdapter, 'uploadToS3').and.returnValue(Promise.resolve(new HttpResponse()))
      component.selectedFiles = [new File([], 'test.png')];
      component.generateTableFiles();
      // Act
      await component.upload();
      // Verify
      expect(component.uploadingAnnotations).toBeTrue();
      expect(component.annotationAdapter.getRequestURL).toHaveBeenCalledWith(jasmine.any(File));
      expect(component.annotationAdapter.uploadToS3).toHaveBeenCalledWith(jasmine.any(String), jasmine.any(File))
    });
  });

  it('should navigate to basic information step if there are invalid inputs', async () => {
    // Prepare
    spyOn(component, 'upload');
    spyOn(component, 'editDataset');
    spyOn(component.fileStatusService, 'updateStatusIcon');
    spyOn(component.stepper, 'previous');
    spyOn(component.basicInfoFormGroup, 'markAllAsTouched');

    component.basicInfoFormGroup.controls['datasetNameCtrl'].setValue('');

    // Act
    await component.uploadAndSave();
    // Verify
    expect(component.stepper.previous).toHaveBeenCalled();
    expect(component.basicInfoFormGroup.markAllAsTouched).toHaveBeenCalled();
    expect(component.fileStatusService.updateStatusIcon).not.toHaveBeenCalled();
    expect(component.upload).not.toHaveBeenCalled();
    expect(component.fileStatusService.updateStatusIcon).not.toHaveBeenCalled();
    expect(component.editDataset).not.toHaveBeenCalled();
  });

  it('should upload files', async () => {
    // Prepare
    spyOn(component, 'upload');
    spyOn(component, 'editDataset');
    spyOn(component.fileStatusService, 'updateStatusIcon');

    component.selectedFiles = [new File([], 'test.png')];
    component.generateTableFiles();
    // Act
    await component.uploadAndSave();
    // Verify
    expect(component.fileStatusService.updateStatusIcon).toHaveBeenCalledWith(component.selectedFiles, component.uploadFileList, 'cached');
    expect(component.upload).toHaveBeenCalled();
    expect(component.fileStatusService.updateStatusIcon).toHaveBeenCalledWith(component.selectedFiles, component.uploadFileList, 'sync_alt');
    expect(component.editDataset).toHaveBeenCalled();
  });

  it('should get tooltips for status icons', () => {
    // Act
    const returnValue = component.getStatusTooltip('cached');
    // Verify
    expect(returnValue).toEqual('File is waiting to be uploaded');
  });
});



