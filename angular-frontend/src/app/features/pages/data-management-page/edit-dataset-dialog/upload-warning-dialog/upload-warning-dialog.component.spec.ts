/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';

import {
  UploadWarningDialogComponent
} from 'src/app/features/pages/data-management-page/edit-dataset-dialog/upload-warning-dialog/upload-warning-dialog.component';

describe('UploadWarningDialogComponent', () => {
  let component: UploadWarningDialogComponent;
  let fixture: ComponentFixture<UploadWarningDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UploadWarningDialogComponent],
      imports: [MatDialogModule],
      providers: [
        {provide: MatDialogRef, useValue: {}},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadWarningDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
