/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-upload-warning-dialog',
  templateUrl: './upload-warning-dialog.component.html',
  styleUrls: ['./upload-warning-dialog.component.scss']
})
export class UploadWarningDialogComponent {
  
  constructor(private dialogRef: MatDialogRef<UploadWarningDialogComponent>) { }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  onYesClick(): void {
    this.dialogRef.close(true);
  }
}
