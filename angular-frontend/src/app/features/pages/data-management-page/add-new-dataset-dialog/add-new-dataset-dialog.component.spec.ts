/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ENTER} from '@angular/cdk/keycodes';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatChipsModule} from '@angular/material/chips';
import {MatOptionModule} from '@angular/material/core';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {of, throwError} from 'rxjs';
import {DataManagementService, DATASET_TYPES} from 'src/app/core/backend/data-management.service';
import {Dataset, DatasetTag} from 'src/app/core/models/dataset';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';

import {
  AddNewDatasetDialogComponent, UploadDragDropFile
} from 'src/app/features/pages/data-management-page/add-new-dataset-dialog/add-new-dataset-dialog.component';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {MatCardModule} from '@angular/material/card';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {HttpResponse} from "@angular/common/http";

const DATASET_TAGS: DatasetTag[] = [
  {
    name: 'Public',
    colorClass: 'red-chip'
  },
  {
    name: 'Private',
    colorClass: 'red-chip'
  },
  {
    name: 'Annotated',
    colorClass: 'blue-chip'
  }
];

// Utility function to complete the first step of the dialog
function completeDatasetCreationStep(
  dataManagementServiceSpy: jasmine.SpyObj<DataManagementService>,
  component: AddNewDatasetDialogComponent) {
  const dummyDataset = <Dataset>{
    labelStudioProjectId: 1,
    name: 'MyDataset',
    datasetType: DATASET_TYPES[0],
    tags: [],
    classes: [],
    lastModified: new Date(),
    annotatedImages: 0,
    totalImages: 0,
    previewImages: [],
    labelStudioUrl: "",
  };
  dataManagementServiceSpy.createDataset.and.returnValue(of(dummyDataset));
  component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
  component.basicInfoFormGroup.get('typeCtrl').setValue(DATASET_TYPES[0].value);
  component.basicInfoFormGroup.get('classCtrl').setValue('dummyClass');
  component.basicInfoFormGroup.get('tagsCtrl').setValue('public');
  component.createDataset();
  return dummyDataset;
}

describe('AddNewDatasetDialogComponent', () => {
  let component: AddNewDatasetDialogComponent;
  let fixture: ComponentFixture<AddNewDatasetDialogComponent>;

  let dataManagementServiceSpy: jasmine.SpyObj<DataManagementService>;
  let dialogRefSpy: jasmine.SpyObj<MatDialogRef<AddNewDatasetDialogComponent>>;

  beforeEach(async () => {
    dataManagementServiceSpy = jasmine.createSpyObj<DataManagementService>('DataManagementServiceSpy',
      ['getAllDatasetTypes', 'createDataset', 'deleteDataset', 'syncDataset', 'getDatasets', 'sortDatasetList']
    );
    dialogRefSpy = jasmine.createSpyObj<MatDialogRef<AddNewDatasetDialogComponent>>('AddNewDatasetDialogComponentSpy', ['close']);
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        MatSelectModule,
        MatChipsModule,
        MatStepperModule,
        MatOptionModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatInputModule,
        HttpClientTestingModule,
        MatCardModule,
        MatTableModule
      ],
      declarations: [AddNewDatasetDialogComponent],
      providers: [
        ToasterService,
        {provide: MAT_DIALOG_DATA, useValue: {}},
        {
          provide: MatDialogRef,
          useValue: dialogRefSpy
        },
        {
          provide: DataManagementService,
          useValue: dataManagementServiceSpy
        },
      ]
    }).compileComponents();

    dataManagementServiceSpy.getAllDatasetTypes.and.returnValue(of(DATASET_TYPES));
    dataManagementServiceSpy.getDatasets.and.returnValue(of({datasets: [], totalDatasets: 0}));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewDatasetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('The basic information tab', () => {

    it('should be locked such that the user can switch to the upload tab immediately', () => {
      expect(component.stepper.linear).withContext('Steps of the stepper should be followed linearly').toBeTrue();
      expect(component.stepper.steps.get(0).editable).withContext('The basic information tab should not be clickable').toBeFalse();
      expect(component.stepper.steps.get(1).editable).withContext('The upload data tab should not be clickable').toBeFalse();
    });

    it('should reset basicInfoFormGroup',
      () => {
        // Prepare
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
        component.basicInfoFormGroup.get('typeCtrl').setValue(DATASET_TYPES[0].value);
        component.basicInfoFormGroup.get('classCtrl').setValue('dummyClass');
        component.basicInfoFormGroup.get('tagsCtrl').setValue('public');
        // Act
        component.reset();
        // Verify
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').value == null).toBeTruthy();
        expect(component.basicInfoFormGroup.get('typeCtrl').value == null).toBeTruthy();
        expect(component.basicInfoFormGroup.get('classCtrl').value == null).toBeTruthy();
        expect(component.basicInfoFormGroup.get('tagsCtrl').value == null).toBeTruthy();
      });

    it('should display validation error after writing a wrong dataset name',
      () => {
        // Prepare
        const dummy = 'dummy';
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue(dummy.repeat(201));
        // Verify
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
        // Prepare
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue('');
        // Verify
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
      });

    it('should display a validation error after writing a dataset name that already exists',
      () => {
        // Prepare
        component.datasets = component.datasets || [];
        const existingDataset = <Dataset>{
          labelStudioProjectId: 1,
          name: 'MyDataset',
          datasetType: DATASET_TYPES[0],
          tags: [],
          classes: [],
          lastModified: new Date(),
          annotatedImages: 0,
          totalImages: 0,
          previewImages: [],
        };
        component.datasets.push(existingDataset);

        // Act
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');

        // Verify
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
      });

    it('should display a validation error if a dataset name is entered with an extra space at the beginning or end',
      () => {
        // Act: Set a value with a leading space
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue(' MyDataset');
        // Verify: The control should be invalid
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
        // Verify: There should be a pattern error
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').hasError('pattern')).toBeTruthy();

        // Act: Set a value with a trailing space
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset ');
        // Verify: The control should be invalid
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeFalsy();
        // Verify: There should be a pattern error
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').hasError('pattern')).toBeTruthy();

        // Act: Set a value without leading or trailing spaces
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
        // Verify: The control should be valid
        expect(component.basicInfoFormGroup.get('datasetNameCtrl').valid).toBeTruthy();
      });

    it('should display a message after creating the dataset and switch to next tab',
      inject([ToasterService], (toasterService: ToasterService) => {
        // Prepare
        const dummyDataset = <Dataset>{
          labelStudioProjectId: 1,
          name: 'MyDataset',
          datasetType: DATASET_TYPES[0],
          tags: [],
          classes: [],
          lastModified: new Date(),
          annotatedImages: 0,
          totalImages: 0,
          previewImages: [],
        };
        dataManagementServiceSpy.createDataset.and.returnValue(of(dummyDataset));
        const toastSuccessSpy = spyOn(toasterService, 'success');
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
        component.basicInfoFormGroup.get('typeCtrl').setValue(DATASET_TYPES[0].value);
        component.basicInfoFormGroup.get('classCtrl').setValue('dummyClass');
        component.basicInfoFormGroup.get('tagsCtrl').setValue('public');
        // Act
        component.createDataset();
        // Verify
        expect(dataManagementServiceSpy.createDataset).toHaveBeenCalledOnceWith('MyDataset', DATASET_TYPES[0].value, [], []);
        expect(toastSuccessSpy).toHaveBeenCalledOnceWith('Created new dataset \'MyDataset\'', 'Created dataset');
        expect(component.createdDataset).toEqual(dummyDataset);
        expect(component.stepper.selected.label).toEqual('Upload data');
        expect(component.stepper.steps.get(0).completed).withContext('The basic information tab should be completed').toBeTrue();
        expect(component.stepper.steps.get(0).editable).withContext('The basic information tab should not be clickable').toBeFalse();
        expect(component.stepper.steps.get(1).editable).withContext('The upload data tab should not be clickable').toBeFalse();
      }));

    it('should display an error if the dataset creation failed and stay on the same tab',
      inject([ToasterService], (toasterService: ToasterService) => {
        // Prepare
        dataManagementServiceSpy.createDataset.and.returnValue(throwError(() => new Error('Something went wrong creating the dataset.')));
        const toastErrorSpy = spyOn(toasterService, 'error');
        component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
        component.basicInfoFormGroup.get('typeCtrl').setValue(DATASET_TYPES[0].value);
        component.basicInfoFormGroup.get('classCtrl').setValue('dummyClass');
        component.basicInfoFormGroup.get('tagsCtrl').setValue('public');
        // Act
        component.createDataset();
        // Verify
        expect(dataManagementServiceSpy.createDataset).toHaveBeenCalledOnceWith('MyDataset', DATASET_TYPES[0].value, [], []);
        expect(toastErrorSpy).toHaveBeenCalledOnceWith('Failed to create the dataset \'MyDataset\'.', 'Error creating dataset');
        expect(component.stepper.steps.get(0).completed).withContext('The basic information tab should not be completed').toBeFalse();
      }));

    it('should display validation errors if the form is not complete', () => {
      // Prepare
      // we don't fill the Dataset name here
      // component.basicInfoFormGroup.get('datasetNameCtrl').setValue('MyDataset');
      component.basicInfoFormGroup.get('typeCtrl').setValue(DATASET_TYPES[0].value);
      expect(component.basicInfoFormGroup.valid).toBeFalse();
      // Act
      component.createDataset();
      // Verify
      expect(dataManagementServiceSpy.createDataset).toHaveBeenCalledTimes(0);
      expect(component.stepper.steps.get(0).completed).withContext('The basic information tab should not be completed').toBeFalse();
    });

    it('should set dataset name and type', () => {
      // Prepare
      const dummyDataset = <Dataset>{
        labelStudioProjectId: 1,
        name: 'MyDataset',
        datasetType: DATASET_TYPES[0],
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      };
      // Act
      component.datasetSelected(dummyDataset);
      // Verify
      expect(component.basicInfoFormGroup.controls['datasetNameCtrl'].value).toBe(dummyDataset.name);
      expect(component.basicInfoFormGroup.controls['typeCtrl'].value).toBe(dummyDataset.datasetType.value);
    });

    it('should populate classes and tags', () => {
      // Prepare
      const dummyDataset = <Dataset>{
        labelStudioProjectId: 1,
        name: 'MyDataset',
        datasetType: DATASET_TYPES[0],
        tags: [],
        classes: ['dummyClass1', 'dummyClass2'],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      };
      // Act
      component.datasetSelected(dummyDataset);
      // Verify
      expect(component.classes).toEqual(dummyDataset.classes);
      expect(component.tags).toEqual(dummyDataset.tags);
    });
  });

  describe('The upload data tab', () => {

    it('should call toasterService.error when an error occurs during syncDataset',
      inject([ToasterService], (toasterService: ToasterService) => {
        // Prepare
        const errorMessage = 'Error saving images';
        const toastErrorSpy = spyOn(toasterService, 'error');
        dataManagementServiceSpy.syncDataset.and.returnValue(throwError({error: {message: errorMessage}}));
        // Act
        component.save();
        // Verify
        expect(toastErrorSpy).toHaveBeenCalledWith(`Failed to save images to dataset '${component.createdDataset.name}'`, 'Error saving images');
      }));

    it('should call syncDataset and close the dialog', () => {
      // Prepare
      component.createdDataset = completeDatasetCreationStep(dataManagementServiceSpy, component);
      dataManagementServiceSpy.syncDataset.and.returnValue(of(10));
      // Act
      component.save();
      // Verify
      expect(dataManagementServiceSpy.syncDataset).toHaveBeenCalledWith(component.createdDataset);
      expect(dialogRefSpy.close).toHaveBeenCalledWith(true);
    });

    it('should display a message after aborting',
      inject([ToasterService], (toasterService: ToasterService) => {
        // Prepare
        const dummyDataset = completeDatasetCreationStep(dataManagementServiceSpy, component);
        const toastSuccessSpy = spyOn(toasterService, 'success');
        dataManagementServiceSpy.deleteDataset.and.returnValue(of({}));
        // Act
        component.deleteCreatedDataset();
        // Verify
        expect(dataManagementServiceSpy.deleteDataset).toHaveBeenCalledOnceWith(dummyDataset);
        expect(toastSuccessSpy).toHaveBeenCalledOnceWith(`The dataset '${dummyDataset.name}' was deleted.`, 'Dataset deleted.');
        expect(dialogRefSpy.close).toHaveBeenCalledOnceWith();
      }));

    it('should display an error if aborting fails',
      inject([ToasterService], (toasterService: ToasterService) => {
        // Prepare
        const dummyDataset = completeDatasetCreationStep(dataManagementServiceSpy, component);
        const toastErrorSpy = spyOn(toasterService, 'error');
        dataManagementServiceSpy.deleteDataset.and.returnValue(throwError(() => new Error('Something went wrong :(')));
        // Act
        component.deleteCreatedDataset();
        // Verify
        expect(dataManagementServiceSpy.deleteDataset).toHaveBeenCalledOnceWith(dummyDataset);
        expect(toastErrorSpy).toHaveBeenCalledOnceWith(`Failed to delete the dataset '${dummyDataset.name}'.`, 'Error deleting dataset');
        expect(dialogRefSpy.close).toHaveBeenCalledTimes(0);
      }));

    it('should display a message after saving',
      inject([ToasterService], (toasterService: ToasterService) => {
        // Prepare
        const dummyDataset = completeDatasetCreationStep(dataManagementServiceSpy, component);
        const toastSuccessSpy = spyOn(toasterService, 'success');
        dataManagementServiceSpy.syncDataset.and.returnValue(of(10));
        // Act
        component.save();
        // Verify
        expect(dataManagementServiceSpy.syncDataset).toHaveBeenCalledOnceWith(dummyDataset);
        expect(toastSuccessSpy).toHaveBeenCalledOnceWith('10 Uploaded images were saved to dataset \'MyDataset\'', 'Images saved');
      }));

    it('should display an error if saving failed',
      inject([ToasterService], (toasterService: ToasterService) => {
        // Prepare
        const dummyDataset = completeDatasetCreationStep(dataManagementServiceSpy, component);
        const toastErrorSpy = spyOn(toasterService, 'error');
        dataManagementServiceSpy.syncDataset.and.returnValue(throwError(() => new Error('Something went wrong :(')));
        // Act
        component.save();
        // Verify
        expect(dataManagementServiceSpy.syncDataset).toHaveBeenCalledOnceWith(dummyDataset);
        expect(toastErrorSpy).toHaveBeenCalledOnceWith(`Failed to save images to dataset '${dummyDataset.name}'`, 'Error saving images');
        expect(dialogRefSpy.close).toHaveBeenCalledTimes(0);
      }));

    it('should display a message after uploading and saving',
      inject([ToasterService], async (toasterService: ToasterService) => {
        // Prepare
        const dummyDataset = completeDatasetCreationStep(dataManagementServiceSpy, component);
        const toastSuccessSpy = spyOn(toasterService, 'success');
        dataManagementServiceSpy.syncDataset.and.returnValue(of(10));
        // Act
        await component.uploadAndSave();
        // Verify
        expect(dataManagementServiceSpy.syncDataset).toHaveBeenCalledOnceWith(dummyDataset);
        expect(toastSuccessSpy).toHaveBeenCalledOnceWith('10 Uploaded images were saved to dataset \'MyDataset\'', 'Images saved');
      }));

    it('should add selected file to selectedFiles array if it does not exist', () => {
      // Prepare
      const mockFile = new File([''], 'test.png', {type: 'image/png'});
      spyOn(component, 'checkDuplicateFile').and.returnValue(false);
      // Act
      component.onFileSelected({currentTarget: {files: [mockFile]}});
      // Verify
      expect(component.selectedFiles.length).toBe(1);
      expect(component.selectedFiles[0]).toEqual(mockFile);
    });

    it('should not add selected file to selectedFiles array if it already exists', () => {
      // Prepare
      const mockFile = new File([''], 'test.png', {type: 'image/png'});
      component.selectedFiles = [mockFile];
      spyOn(component, 'checkDuplicateFile').and.returnValue(true);
      // Act
      component.onFileSelected({currentTarget: {files: [mockFile]}});
      // Verify
      expect(component.selectedFiles.length).toBe(1);
    });

    it('should prevent default on drag over event', () => {
      // Prepare
      const event = new Event('dragover');
      const preventDefaultSpy = spyOn(event, 'preventDefault');
      // Act
      component.onDragOver(event);
      // Verify
      expect(preventDefaultSpy).toHaveBeenCalled();
    });

    it('should prevent default on drag leave event', () => {
      // Prepare
      const event = new Event('dragleave');
      const preventDefaultSpy = spyOn(event, 'preventDefault');
      // Act
      component.onDragLeave(event);
      // Verify
      expect(preventDefaultSpy).toHaveBeenCalled();
    });

    it('should detect duplication if file is already uploaded', () => {
      // Prepare
      const mockFile = new File([''], 'test.png', {type: 'image/png'});
      component.selectedFiles = [mockFile];
      const newFile = new File([''], 'test.png', {type: 'image/png'});
      // Act
      component.checkDuplicateFile(newFile);
      // Verify
      expect(component.checkDuplicateFile).toBeTruthy;
    });

    it('should add selected files and prevent default on drop event', () => {
      // Prepare
      const mockFile = new File([''], 'file2.png', {type: 'image/png'});
      const mockEvent = {
        dataTransfer: {files: [mockFile]},
        preventDefault: jasmine.createSpy('preventDefault')
      };
      spyOn(component, 'checkDuplicateFile').and.returnValue(false);
      spyOn(component, 'generateTableFiles');
      // Act
      component.onDrop(mockEvent);
      // Verify
      expect(component.selectedFiles.length).toEqual(1);
      expect(component.selectedFiles[0]).toEqual(mockFile);
      expect(mockEvent.preventDefault).toHaveBeenCalled();
      expect(component.generateTableFiles).toHaveBeenCalled();
    });

    it('should not add duplicate file to selectedFiles array', () => {
      // Prepare
      const mockFile = new File(['test'], 'test.png', {type: 'image/png'});
      component.selectedFiles = [mockFile];
      const mockEvent = {dataTransfer: {files: [mockFile]}, preventDefault: jasmine.createSpy('preventDefault')};
      spyOn(component, 'generateTableFiles');
      // Act
      component.onDrop(mockEvent);
      // Verify
      expect(component.selectedFiles.length).toBe(1);
      expect(mockEvent.preventDefault).toHaveBeenCalled();
      expect(component.generateTableFiles).toHaveBeenCalled();
    });

    it('should not detect duplicate files', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.jpg'),
        new File([''], 'file2.jpg'),
      ];
      const newFile = new File([''], 'file3.jpg');
      // Act
      const result = component.checkDuplicateFile(newFile);
      // Verify
      expect(result).toEqual(false);
    });

    it('should remove a file', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.jpg'),
        new File([''], 'file2.jpg'),
      ];
      const fileToRemove = new File([''], 'file1.jpg');
      // Act
      component.remove(fileToRemove);
      // Verify
      expect(component.selectedFiles).toEqual([
        new File([''], 'file2.jpg'),
      ]);
    });

    it('should call generateTableFiles after removing a file', () => {
      // Prepare
      const file = <UploadDragDropFile>{name: 'test-file.json', type: 'json'};
      spyOn(component, 'generateTableFiles');
      // Act
      component.remove(file);
      // Verify
      expect(component.generateTableFiles).toHaveBeenCalled();
    });

    it('should generate table files with only selected files', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.jpg'),
        new File([''], 'file2.jpg'),
      ];
      // Act
      component.generateTableFiles();
      // Verify
      expect(component.uploadFileList.length).toBe(2); // Expect two files in the upload file list
      expect(component.uploadFileList[0].name).toBe('file1.jpg'); // Expect first file name to match
      expect(component.uploadFileList[1].name).toBe('file2.jpg'); // Expect second file name to match
      expect(component.uploadFileList[0].statusIcon).toBe(''); // Expect first file check status to be false
      expect(component.uploadFileList[1].statusIcon).toBe(''); // Expect second file check status to be false
    });

    it('should generate empty table files if no selected files', () => {
      // Act
      component.generateTableFiles();
      // Verify
      expect(component.uploadFileList.length).toBe(0); // Expect no files in the upload file list
    });

    it('should create MatTableDataSource with generated file list', () => {
      // Prepare
      component.selectedFiles = [
        new File([''], 'file1.jpg'),
        new File([''], 'file2.jpg'),
      ];
      // Act
      component.generateTableFiles();
      // Verify
      expect(component.dataSource).toBeInstanceOf(MatTableDataSource); // Expect MatTableDataSource instance
      expect(component.dataSource.data.length).toBe(2); // Expect two files in the data source
      expect(component.dataSource.data[0].name).toBe('file1.jpg'); // Expect first file name to match
      expect(component.dataSource.data[1].name).toBe('file2.jpg'); // Expect second file name to match
    });


    it('should upload files', async () => {
      // Prepare
      spyOn(component.adapter, 'getRequestURL').and.returnValue(Promise.resolve('https://example.com'));
      spyOn(component.adapter, 'uploadToS3').and.returnValue(Promise.resolve(new HttpResponse()));
      spyOn(component.adapter, 'getTrackingPresignedUrl').and.returnValue(of({url: 'your-tracking-presigned-url'}));
      spyOn(component.adapter, 'editLabelConfig');

      const videoFile = new File([], 'test.mp4', {type: 'video/*'});
      component.selectedFiles = [videoFile];
      component.generateTableFiles();
      // Act
      await component.upload();
      // Verify
      expect(component.adapter.getRequestURL).toHaveBeenCalledWith(jasmine.any(File));
      expect(component.adapter.uploadToS3).toHaveBeenCalledWith(jasmine.any(String), jasmine.any(File));
      expect(component.adapter.getTrackingPresignedUrl).toHaveBeenCalledWith(videoFile.name);
      expect(component.adapter.editLabelConfig).toHaveBeenCalledWith('your-tracking-presigned-url');
    });

    it('should set statusIcon property to `` for existing file in uploadFileList', async () => {
      // Prepare
      spyOn(component.adapter, 'getRequestURL').and.returnValue(Promise.resolve('https://example.com'));
      spyOn(component.adapter, 'uploadToS3').and.returnValue(Promise.resolve(new HttpResponse()));
      spyOn(component.adapter, 'getTrackingPresignedUrl').and.returnValue(of({url: 'your-tracking-presigned-url'}));
      spyOn(component.adapter, 'editLabelConfig');

      spyOn(component, 'generateTableFiles'); // This is needed for generating the table files
      const existingFile = {name: 'existingFile.jpg', size: 1024, type: 'image/jpeg', icon: 'delete'};
      component.uploadFileList = [{...existingFile, statusIcon: ''}]; // Add an existing file with statusIcon: ''
      const uploadedFile = new File([], 'existingFile.jpg', {type: 'image/jpeg'});
      component.selectedFiles = [uploadedFile];

      // Act
      await component.upload();

      // Verify
      expect(component.uploadFileList.length).toBe(1); // Expect only one file in the upload file list
      expect(component.uploadFileList[0].statusIcon).toBe('checked'); // Expect statusIcon property of existing file to be 'check'
    });

    it('should get tooltips for status icons', () => {
      // Act
      const returnValue = component.getStatusTooltip('cached');
      // Verify
      expect(returnValue).toEqual('File is waiting to be uploaded');
    });
  });

  describe('The class input', () => {
    it('should add selected class to array of classes',
      () => {
        // Prepare
        const Event = {
          option: {
            value: 'dummyClass'
          }
        } as MatAutocompleteSelectedEvent;
        const lengthBefore = component.classes.length;
        // Act
        component.selectedClass(Event);
        // Verify
        expect(component.classes.length > lengthBefore).toBeTruthy();
      });

    it('should remove only one class', () => {
      component.classes = ['class1', 'class2'];
      fixture.detectChanges();
      // Act
      component.removeClass('class2');
      // Verify
      expect(component.classes).toEqual(['class1']);
      expect(component.basicInfoFormGroup.get('classCtrl').value).not.toEqual(null);
    });

    it('should remove all classes and set classCtrl to null', () => {
      component.classes = ['class1'];
      fixture.detectChanges();
      // Act
      component.removeClass('class1');
      // Verify
      expect(component.classes).toEqual([]);
      expect(component.basicInfoFormGroup.get('classCtrl').value).toEqual(null);
    });

    it('should add a new class on enter', fakeAsync(() => {
      // Act
      component.classInput.nativeElement.value = 'newClass';
      component.classInput.nativeElement.dispatchEvent(new KeyboardEvent('keydown', {
        code: 'Enter', key: 'Enter', keyCode: ENTER, charCode: ENTER, bubbles: true
      }));
      tick(20); // wait for debounce

      expect(component.classes.length).toEqual(1);
      expect(component.classes[0]).toEqual('newClass');
    }));

    it('should not add empty class on enter', fakeAsync(() => {
      // Act
      component.classInput.nativeElement.value = '';
      component.classInput.nativeElement.dispatchEvent(new KeyboardEvent('keydown', {
        code: 'Enter', key: 'Enter', keyCode: ENTER, charCode: ENTER, bubbles: true
      }));
      tick(20); // wait for debounce

      expect(component.classes).toEqual([]);
    }));
  });

  describe('The Tag input', () => {

    it('should remove only one tag', () => {
      component.tags = [DATASET_TAGS[0], DATASET_TAGS[2]];
      fixture.detectChanges();
      // Act
      component.removeTag(DATASET_TAGS[2]);
      // Verify
      expect(component.tags).toEqual([DATASET_TAGS[0]]);
    });

    it('should add a new tag on enter', fakeAsync(() => {
      // Act
      component.tagInput.nativeElement.value = 'newtag';
      component.tagInput.nativeElement.dispatchEvent(new KeyboardEvent('keydown', {
        code: 'Enter', key: 'Enter', keyCode: ENTER, charCode: ENTER, bubbles: true
      }));
      tick(20); // wait for debounce

      expect(component.tags.length).toEqual(1);
      expect(component.tags[0].name).toEqual('newtag');
    }));

    it('should not add empty tag on enter', fakeAsync(() => {
      // Act
      component.tagInput.nativeElement.value = '';
      component.tagInput.nativeElement.dispatchEvent(new KeyboardEvent('keydown', {
        code: 'Enter', key: 'Enter', keyCode: ENTER, charCode: ENTER, bubbles: true
      }));
      tick(20); // wait for debounce

      expect(component.tags).toEqual([]);
    }));
  });
});
