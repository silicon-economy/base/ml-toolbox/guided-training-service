/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {BehaviorSubject, finalize} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {UploadFilePickerAdapter} from 'src/app/core/services/file-upload/upload-file-picker.adapter';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

import {MatDialogRef} from '@angular/material/dialog';
import {Dataset, DatasetTag, DatasetType} from 'src/app/core/models/dataset';
import {DataManagementService} from 'src/app/core/backend/data-management.service';
import {MatStepper} from '@angular/material/stepper';
import {MatTableDataSource} from '@angular/material/table';
import {FileStatusService} from "src/app/shared/file-status.service";
import {environment} from "src/environments/environment";

/**
 * Interface for the Drag and Drop Table.
 */
export interface UploadDragDropFile {
  /** The name of the file. */
  name: string;
  /** The size of the file. */
  size: number;
  /** The type of the file. */
  type: string;
  //TODO: Why is this icon defined here and not in the html? As far as I am aware, this is the same for all files.
  /** The delete icon??? */
  icon: string;
  /** The icon identifier for the status. */
  statusIcon: string;
}

@Component({
  selector: 'app-add-new-dataset-dialog',
  templateUrl: './add-new-dataset-dialog.component.html',
  styleUrls: ['./add-new-dataset-dialog.component.scss']
})
export class AddNewDatasetDialogComponent implements OnInit {

  visible = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  // datasets from Backend
  datasets: Dataset[] = [];
  loadingDatasets: boolean;
  duplicatedName = false;

  // Drag and Drop
  displayedColumns: string[] = ['name', 'statusIcon', 'type', 'remove'];
  dataSource = new MatTableDataSource<UploadDragDropFile>([]);
  selectedFiles: File[] = [];
  uploadFileList: UploadDragDropFile[] = [];

  // Types
  availableDatasetTypes: DatasetType[];

  // Classes
  classes: string[] = [];

  // Tags
  tags: DatasetTag[] = [];

  // FormGroups
  basicInfoFormGroup = this._formBuilder.group({
    duplicateDatasetCtrl: new FormControl([]),
    datasetNameCtrl: ['', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(200),
      Validators.pattern(/^\S(.*\S)?$/),
      this.duplicateNameValidator.bind(this)]],
    typeCtrl: new FormControl('', Validators.required),
    classCtrl: new FormControl('', Validators.required),
    tagsCtrl: new FormControl('')
  });

  // Validator-function to avoid duplicate dataset names
  duplicateNameValidator(datasetNameCtrl: FormControl) {
    const isDuplicate = Array.isArray(this.datasets) && this.datasets.length > 0 && this.datasets.some(dataset => dataset.name === datasetNameCtrl.value);
    this.duplicatedName = isDuplicate;
    return isDuplicate ? {duplicate: true} : null;
  }

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;
  @ViewChild('classInput') classInput: ElementRef<HTMLInputElement>;
  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  createdDataset: Dataset = {
    labelStudioProjectId: 0,
    name: '',
    datasetType: {
      value: '',
      viewValue: ''
    },
    annotatedImages: 0,
    totalImages: 0,
    lastModified: new Date(),
    previewImages: [],
    tags: [],
    classes: [],
    labelStudioUrl: "",
  };
  datasetSubject = new BehaviorSubject<Dataset>(this.createdDataset);

  adapter = new UploadFilePickerAdapter(this.http, this.datasetSubject, new BehaviorSubject(''), new BehaviorSubject(false), this.toasterService);

  uploadingImages = false;
  creatingDataset = false;
  uploadChunkSize = Number.parseInt(environment.uploadBatchSize);
  uploadedImages = 0;


  /**
   * Constructor for the AddNewDatasetDialogComponent.
   *
   * @param dialogRef A reference to the dialog HTML component.
   * @param _formBuilder A reference to the FormBuilder.
   * @param dataManagementService A reference to the DataManagementService.
   * @param http A reference to the HttpClient.
   * @param toasterService A reference to the ToasterService.
   * @param fileStatusService A reference to the FileStatusService.
   */
  constructor(
    private dialogRef: MatDialogRef<AddNewDatasetDialogComponent>,
    private _formBuilder: FormBuilder,
    private dataManagementService: DataManagementService,
    private http: HttpClient,
    private toasterService: ToasterService,
    public fileStatusService: FileStatusService) {
  }

  /**
   * Initializes the component and load all datasets and available dataset types from the backend.
   */
  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<UploadDragDropFile>(this.uploadFileList);
    this.getAllDatasets();
    this.dataManagementService.getAllDatasetTypes().subscribe((val) => {
      this.availableDatasetTypes = val;
    });
  }

  /**
   * Adds a tag to the dataset.
   *
   * @param event The MatChipInputEvent that triggered the function.
   */
  addTag(event: MatChipInputEvent): void {
    const tagValue = event.value;

    if ((tagValue || '').trim()) {
      const tag = <DatasetTag>{
        name: tagValue,
        colorClass: 'orange-chip'
      };
      this.tags.push(tag);
    }
    event.chipInput.clear();

    this.basicInfoFormGroup.get('tagsCtrl').setValue(null);
  }

  /**
   * Removes a tag from the dataset.
   *
   * @param tag The tag to remove.
   */
  removeTag(tag: DatasetTag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  /**
   * Adds a class to the dataset.
   *
   * @param event The MatChipInputEvent that triggered the function.
   */
  addClass(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      const classChip = String(value);
      this.classes.push(classChip);
    }
    // Clear the input value
    event.chipInput.clear();
  }

  /**
   * Removes a class from the dataset.
   *
   * @param classChip The class to remove.
   */
  removeClass(classChip: string): void {
    const index = this.classes.indexOf(classChip);
    if (index >= 0) {
      this.classes.splice(index, 1);
    }
    if (this.classes.length === 0) {
      this.basicInfoFormGroup.get('classCtrl').setValue(null);
    }
  }

  /**
   * Selects a class from the autocomplete list.
   *
   * @param event The MatAutocompleteSelectedEvent that triggered the function.
   */
  selectedClass(event: MatAutocompleteSelectedEvent): void {
    this.classes.push(event.option.value);
    this.classInput.nativeElement.value = '';
    this.basicInfoFormGroup.get('classCtrl').setValue(null);
  }

  /**
   * Resets the form and stepper.
   */
  reset(): void {
    this.stepper.reset();

    this.tags = [];
    this.basicInfoFormGroup.get('tagsCtrl').setValue(null);
    this.tagInput.nativeElement.value = '';

    this.basicInfoFormGroup.get('classCtrl').setValue(null);
    this.classInput.nativeElement.value = '';
    this.classes = [];
  }

  /**
   * Creates a new dataset with the given information.
   */
  createDataset(): void {
    if (this.basicInfoFormGroup.valid) {
      const datasetName: string = this.basicInfoFormGroup.get('datasetNameCtrl').value;
      const datasetType: string = this.basicInfoFormGroup.get('typeCtrl').value;

      this.creatingDataset = true;
      this.dataManagementService.createDataset(datasetName, datasetType, this.classes, this.tags)
        .pipe(finalize(() => {
          this.creatingDataset = false;
        }))
        .subscribe({
          next: dataset => {
            this.toasterService.success(`Created new dataset '${dataset.name}'`, 'Created dataset');

            this.createdDataset = dataset;
            this.datasetSubject.next(this.createdDataset);
            this.stepper.selected.completed = true;
            this.stepper.next();
          },
          error: () => this.toasterService.error(`Failed to create the dataset '${datasetName}'.`, 'Error creating dataset')
        });
    } else {
      this.basicInfoFormGroup.markAllAsTouched();
    }
  }

  /**
   * Deletes the created dataset.
   */
  deleteCreatedDataset(): void {
    this.uploadingImages = false
    if (this.createdDataset) {
      this.dataManagementService.deleteDataset(this.createdDataset).subscribe({
        next: () => {
          this.toasterService.success(`The dataset '${this.createdDataset.name}' was deleted.`, 'Dataset deleted.');
          this.dialogRef.close();
        },
        error: () => this.toasterService.error(`Failed to delete the dataset '${this.createdDataset.name}'.`, 'Error deleting dataset')
      });
    }
  }

  /**
   * Uploads the selected files to the dataset.
   */
  async upload() {
    this.uploadingImages = true;
    this.uploadedImages = 0;

    const chunked: File[][] = [];

    for (let i = 0; i < this.selectedFiles.length; i += this.uploadChunkSize) {
      const chunk = this.selectedFiles.slice(i, (i + this.uploadChunkSize));
      chunked.push(chunk);
    }

    for (let i = 0; i < chunked.length; i++) {
      const promises = chunked[i].map(file => this.uploadFile(file));
      await Promise.all(promises);
    }
    this.dataSource = new MatTableDataSource<UploadDragDropFile>(this.uploadFileList);
  }

  /**
   * Uploads a single file to the dataset.
   *
   * @param file The file to upload.
   */
  async uploadFile(file: File): Promise<void> {
    const index = this.uploadFileList.findIndex(f => f.name === file.name);

    try {
      const s3URL = await this.adapter.getRequestURL(file);
      await this.adapter.uploadToS3(s3URL, file);

      if (file.type.startsWith('video/')) {
        this.adapter.getTrackingPresignedUrl(file.name).subscribe({
          next: (response) => {
            this.adapter.editLabelConfig(response['url']);
          }
        });
      }
      this.uploadFileList[index].statusIcon = 'checked';
    } catch {
      this.uploadFileList[index].statusIcon = 'dangerous';
    }
    this.uploadedImages++;
  }

  /**
   * Synchronizes the uploaded images with the dataset.
   */
  save(): void {
    this.dataManagementService.syncDataset(this.createdDataset).subscribe({
      next: numSyncedImages => {
        this.toasterService.success(`${numSyncedImages} Uploaded images were saved to dataset '${this.createdDataset.name}'`,
          'Images saved');
        this.dialogRef.close(true);
      },
      error: () => this.toasterService.error(`Failed to save images to dataset '${this.createdDataset.name}'`, 'Error saving images'),
      complete: () => this.dialogRef.close(true)
    });
  }

  /**
   * Uploads and saves the selected files to the dataset.
   */
  async uploadAndSave() {
    this.fileStatusService.updateStatusIcon(this.selectedFiles, this.uploadFileList, 'cached');
    await this.upload();
    this.fileStatusService.updateStatusIcon(this.selectedFiles, this.uploadFileList, 'sync_alt');
    this.save();
  }

  /**
   * Checks for duplicate files and generates the table element.
   *
   * @param event The event that fired the function.
   */
  onFileSelected(event) {
    for (let i = 0; i < event.currentTarget.files?.length; i++) {
      if (!this.checkDuplicateFile(event.currentTarget.files[i])) {
        this.selectedFiles.push(event.currentTarget.files[i]);
      }
    }
    this.generateTableFiles();
  }

  /**
   * Prevents the default behavior of the drag event.
   *
   * @param event The event that fired the function.
   */
  onDragLeave(event) {
    event.preventDefault();
  }

  /**
   * Prevents the default behavior of the drag event.
   *
   * @param event The event that fired the function.
   */
  onDragOver(event) {
    event.preventDefault();
  }

  /**
   * Allows the user to drop files into the drag and drop area to upload them.
   *
   * @param event The event that fired the function.
   */
  onDrop(event) {
    for (let i = 0; i < event.dataTransfer.files?.length; i++) {
      if (!this.checkDuplicateFile(event.dataTransfer.files[i])) {
        this.selectedFiles.push(event.dataTransfer.files[i]);
      }
    }
    event.preventDefault();
    this.generateTableFiles();
  }

  /**
   * Removes a file from the selected files.
   *
   * @param element The file to remove
   */
  remove(element: UploadDragDropFile | File) {
    this.selectedFiles.splice(this.selectedFiles.findIndex(file => file.name === element.name), 1);
    this.generateTableFiles();
  }

  /**
   * Generates the files table for all selected files.
   */
  generateTableFiles() {
    this.uploadFileList = [];
    // Show selected files without statusIcon
    for (let i = 0; i < this.selectedFiles?.length; i++) {
      const fileSize = Number((this.selectedFiles[i].size / 1000 / 1000).toFixed(2));
      this.uploadFileList.push({
        name: this.selectedFiles[i].name,
        size: fileSize,
        type: this.selectedFiles[i].type.split('/')[1],
        icon: 'delete',
        statusIcon: ''
      });
    }
    this.dataSource = new MatTableDataSource<UploadDragDropFile>(this.uploadFileList);
  }

  /**
   * Checks if the selected file is a duplicate of an already selected file.
   *
   * @param file The file that should be checked for duplicates.
   * @returns True if the file is a duplicate, false otherwise.
   */
  checkDuplicateFile(file: File): boolean {
    for (let i = 0; i < this.selectedFiles?.length; i++) {
      if (this.selectedFiles[i].name === file.name) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets the status tooltip text for a given status icon.
   *
   * @param statusIcon The status icon to get the tooltip for.
   */
  getStatusTooltip(statusIcon: string): string {
    return this.fileStatusService.getStatusTooltip(statusIcon);
  }

  /**
   * Loads all datasets from the backend.
   */
  getAllDatasets() {
    this.loadingDatasets = true;
    this.dataManagementService.getDatasets('', 'lastModified', 'desc', 0, 500).pipe(finalize(() => {
      this.loadingDatasets = false;
    })).subscribe({
      next: (res) => {
        this.datasets = this.dataManagementService.sortDatasetList(res.datasets);
      },
      error: () => {
        this.toasterService.error(`Failed to load datasets`, 'Error loading datasets');
      }
    });
  }

  /**
   * Automatically fills the form with the selected dataset for duplication.
   *
   * @param dataset The dataset to duplicate.
   */
  datasetSelected(dataset: Dataset) {
    this.basicInfoFormGroup.controls['datasetNameCtrl'].setValue(dataset.name);
    this.basicInfoFormGroup.controls['typeCtrl'].setValue(dataset.datasetType.value);

    this.tags = [];
    dataset.tags.forEach((datasetTags) => {
      this.tags.push(datasetTags);
    });

    this.classes = [];
    dataset.classes.forEach((datasetClass) => {
      this.basicInfoFormGroup.controls['classCtrl'].setValue(datasetClass);
      this.classes.push(datasetClass);
      this.basicInfoFormGroup.controls['classCtrl'].setValue(' ');
    });
    this.basicInfoFormGroup.markAllAsTouched();
  }
}
