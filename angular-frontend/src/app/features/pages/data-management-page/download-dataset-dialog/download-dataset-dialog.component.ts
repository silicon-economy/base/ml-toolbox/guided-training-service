/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Dataset } from 'src/app/core/models/dataset';

@Component({
  selector: 'app-download-dataset-dialog',
  templateUrl: './download-dataset-dialog.component.html',
  styleUrls: ['./download-dataset-dialog.component.scss']
})
export class DownloadDatasetDialogComponent{

  constructor(@Inject(MAT_DIALOG_DATA) public data: Dataset) { }

}
