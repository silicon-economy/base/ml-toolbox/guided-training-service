/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {
  AutoAnnotateDialogComponent
} from 'src/app/features/pages/data-management-page/auto-annotate-dialog/auto-annotate-dialog.component';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ModelManagementService, ModelsResponse} from 'src/app/core/backend/model-management.service';
import {DataManagementService} from 'src/app/core/backend/data-management.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {of} from 'rxjs';
import {Dataset, TagColor} from 'src/app/core/models/dataset';
import {TrainingServer} from 'src/app/core/models/trainingserver';
import {Model} from "../../../../core/models/model";

describe('AutoAnnotateDialogComponent', () => {
  let component: AutoAnnotateDialogComponent;
  let fixture: ComponentFixture<AutoAnnotateDialogComponent>;
  let mockData;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AutoAnnotateDialogComponent],
      imports: [
        MatDialogModule,
        MatSelectModule,
        MatInputModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatCardModule],
      providers: [
        ModelManagementService,
        DataManagementService,
        ToasterService,
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockData,
        },
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {
            }
          },
        },
      ],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoAnnotateDialogComponent);
    component = fixture.componentInstance;

    // Mock the data you want to test with
    mockData = {
      dataset: <Dataset>{
        labelStudioProjectId: 123,
        name: 'Test Dataset',
        datasetType: {value: "test", viewValue: "test"},
        annotatedImages: 0,
        totalImages: 0,
        lastModified: new Date(),
        previewImages: [],
        tags: [],
        classes: []
      },
    };

    component.selectedDataset = mockData.dataset;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch models from the backend and initialize the component', () => {
    const mockModelsResponse = <ModelsResponse>{
      models: [
        {
          run_id: "1",
          name: "Default Yolo-v5",
          description: 'This is a really long description for the model. The model\'s name is "Default Yolo-v5" and it uses the "Imagenet" dataset',
          dataset: ["Imagenet"],
          model: "test",
          evaluation_datasets: ["Imagenet Test"],
          epochs: 3,
          batch_size: 3,
          annotation_task: [
            {
              name: 'EPAL',
              colorClass: 'bright-green-chip' as TagColor,
            }
          ],
          training_state: 'RUNNING',
          accuracy: 90.000,
          algorithm_type: "test",
          creation_date: new Date(),
          mlflow_url: "url to mlflow"
        }
      ],
      totalModels: 1, // Set the totalModels count as needed
    };

    spyOn(component.modelManagementService, 'getModels').and.returnValue(of(mockModelsResponse));

    component.ngOnInit();

    expect(component.models).toEqual(mockModelsResponse.models);
  });

  it('should set the selectedModel when modelSelected is called', () => {
    const mockModel = <Model>{
      run_id: "1",
      name: "SelectedModel",
      description: 'Selected Model Description',
      dataset: ["Selected Dataset"],
      model: "selected",
      evaluation_datasets: ["Selected Eval Dataset"],
      epochs: 3,
      batch_size: 3,
      annotation_task: [],
      training_state: 'RUNNING',
      accuracy: 90.000,
      algorithm_type: "selected",
      creation_date: new Date(),
      mlflow_url: "url to mlflow",
    };

    component.modelSelected(mockModel);

    expect(component.selectedModel).toEqual(mockModel);
  });

  it('should send an auto-annotate request, show success message, and close the dialog', () => {
    const mockResponse = {}; // Replace with your expected response data.
    spyOn(component.dataManagementService, 'autoAnnotate').and.returnValue(of(mockResponse));
    spyOn(component.toasterService, 'success');
    const matDialogRef = TestBed.inject(MatDialogRef);
    const closeSpy = spyOn(matDialogRef, 'close'); // Create a spy for matDialogRef.close


    component.selectedModel = <Model>{
      run_id: "1",
      name: "SelectedModel",
      description: 'Selected Model Description',
      dataset: ["Selected Dataset"],
      model: "selected",
      evaluation_datasets: ["Selected Eval Dataset"],
      epochs: 3,
      batch_size: 3,
      annotation_task: [],
      training_state: 'RUNNING',
      accuracy: 90.000,
      algorithm_type: "selected",
      creation_date: new Date(),
      mlflow_url: "url to mlflow",
    };

    component.selectedServer = {
      name: 'testServer',
      endpoint: 'http://example.com',
      id: 0,
      inEditing: false,
      selected: false
    }

    component.autoAnnotate();

    expect(component.dataManagementService.autoAnnotate).toHaveBeenCalledWith(
      component.selectedDataset.labelStudioProjectId,
      component.selectedModel.name,
      component.selectedServer.endpoint
    );
    expect(component.toasterService.success).toHaveBeenCalledWith('You have annotated a dataset', 'Successfully Annotated');
    expect(closeSpy).toHaveBeenCalledWith(true);
    expect(component.finished).toBe(false);
  });

  it('should reset form controls to null', () => {
    const modelFormCtrl = new FormControl([{
      run_id: "1",
      name: "SelectedModel",
      description: 'Selected Model Description',
      dataset: ["Selected Dataset"],
      model: "selected",
      evaluation_datasets: ["Selected Eval Dataset"],
      epochs: 3,
      batch_size: 3,
      annotation_task: [],
      training_state: 'RUNNING',
      accuracy: 90.000,
      algorithm_type: "selected",
      creation_date: new Date(),
      mlflow_url: "url to mlflow",
    }]);
    const serverFormCtrl = new FormControl('anotherValue');

    component.autoAnnotateGroup = new FormGroup({
      modelFormCtrl,
      serverFormCtrl,
    });

    expect(modelFormCtrl.value).not.toBeNull();
    expect(serverFormCtrl.value).not.toBeNull();

    component.reset();

    expect(modelFormCtrl.value).toBeNull();
    expect(serverFormCtrl.value).toBeNull();
  });

  it('should set selectedServer when serverSelected is called', () => {
    const testServer: TrainingServer = {
      name: 'TestServer',
      endpoint: 'http://test-server',
      id: 1,
      inEditing: false,
      selected: false
    };
    component.serverSelected(testServer);
    expect(component.selectedServer).toEqual(testServer);
  });
});

