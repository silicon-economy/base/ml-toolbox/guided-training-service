/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { finalize } from 'rxjs';
import { DataManagementService } from 'src/app/core/backend/data-management.service';
import { ModelManagementService } from 'src/app/core/backend/model-management.service';
import { TrainingServerService } from 'src/app/core/backend/training-server.service';
import { Dataset } from 'src/app/core/models/dataset';
import { Model } from 'src/app/core/models/model';
import { TrainingServer } from 'src/app/core/models/trainingserver';
import { ToasterService } from 'src/app/core/services/toaster/toaster.service';

@Component({
  selector: 'app-auto-annotate-dialog',
  templateUrl: './auto-annotate-dialog.component.html',
  styleUrls: ['./auto-annotate-dialog.component.scss']
})
export class AutoAnnotateDialogComponent implements OnInit {


  selectedDataset: Dataset;

  // models from Backend
  models: Model[] = [];
  // model which is picked from User
  selectedModel: Model;
  // servers from Backend
  servers: TrainingServer[];
  // server which is picked from the user
  selectedServer: TrainingServer;

  // Formcontrols
  autoAnnotateGroup = new FormGroup({
    modelFormCtrl: new FormControl([], [
      Validators.required
    ]),
    serverFormCtrl: new FormControl('', [
      Validators.required
    ])
  });
  finished = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dataManagementService: DataManagementService,
    public modelManagementService: ModelManagementService,
    public dialogRef: MatDialogRef<AutoAnnotateDialogComponent>,
    public toasterService: ToasterService,
    public serverService: TrainingServerService
  ) {
  }

  ngOnInit() {
    // This is necessary to create a simple deep copy of the dataset
    // Otherwise the dataset would also be edited in the parent component
    this.selectedDataset = JSON.parse(JSON.stringify(this.data.dataset));

    // get models from backend and put it in attribute models
    this.modelManagementService.getModels(
      '',
      'creation_data',
      'desc',
      0,
      500
    ).pipe().subscribe({
      next: (res) => {
        this.models = this.modelManagementService.sortModelList(res.models);
      }
    });
    this.loadServer();
  }

  /**
   * Sets the selected model.
   *
   * @param model The model that should be selected.
   */
  modelSelected(model: Model) {
    this.selectedModel = model;
  }

  /**
   * Resets the form.
   */
  reset(): void {
    this.autoAnnotateGroup.get('modelFormCtrl').setValue(null);
    this.autoAnnotateGroup.get('serverFormCtrl').setValue(null);
  }

  /**
   * Run the auto annotation process.
   */
  autoAnnotate() {
    this.finished = true;
    this.dataManagementService.autoAnnotate(this.selectedDataset.labelStudioProjectId, this.selectedModel.name, this.selectedServer.endpoint)
      .pipe(finalize(() => {
        this.finished = false;
      }))
      .subscribe({
        next: () => {
          this.toasterService.success('You have annotated a dataset', 'Successfully Annotated');
          this.dialogRef.close(true);
        },
        error: () => this.toasterService.error(`Annotating with the provided arguments failed.`, 'Error while annotating')
      });
  }

  /**
   * Sets the selected server.
   *
   * @param server The server that should be selected.
   */
  serverSelected(server: TrainingServer) {
    this.selectedServer = server;
  }

  /**
   * Load the servers from the backend.
   */
  loadServer() {
    this.serverService.getAllTrainingsServers().subscribe({
      next: (res) => {
        this.autoAnnotateGroup.get("serverFormCtrl").reset();
        this.selectedServer = undefined;
        this.servers = res;
        console.log('Request completed: Load server')
      }
    })
  }
}
