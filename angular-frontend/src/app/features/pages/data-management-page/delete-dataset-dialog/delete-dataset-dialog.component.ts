/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Dataset } from 'src/app/core/models/dataset';

@Component({
  selector: 'app-delete-dataset-dialog',
  templateUrl: './delete-dataset-dialog.component.html',
  styleUrls: ['./delete-dataset-dialog.component.scss']
})
export class DeleteDatasetDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: Dataset) { }

}
