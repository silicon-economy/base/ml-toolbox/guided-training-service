/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataManagementService } from 'src/app/core/backend/data-management.service';
import { Dataset } from 'src/app/core/models/dataset';

@Component({
  selector: 'app-show-preview-images',
  templateUrl: './show-preview-images.component.html',
  styleUrls: ['./show-preview-images.component.scss']
})
export class ShowPreviewImagesComponent {

  dataset: Dataset;

  loadDatasetSpinner = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: string, public dataManagementService: DataManagementService) {
    /**
     * data = labelstudioProjectId
    */
    this.loadDataset(Number(data));
  }

  /**
   * Loads the dataset for the given labelStudioProjectId
   *
   * @param labelStudioProjectId The id of the label studio project
   */
  loadDataset(labelStudioProjectId: number) {
    this.loadDatasetSpinner = true;
    this.dataManagementService.getDataset(labelStudioProjectId).subscribe({
      next: (res) => {
        this.dataset = res;
      },
      complete: () => {
        this.loadDatasetSpinner = false;
      }
    });
  }
}
