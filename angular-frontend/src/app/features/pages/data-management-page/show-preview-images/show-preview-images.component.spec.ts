/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {
  ShowPreviewImagesComponent
} from 'src/app/features/pages/data-management-page/show-preview-images/show-preview-images.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Dataset} from 'src/app/core/models/dataset';
import {DataManagementService, DATASET_TYPES} from 'src/app/core/backend/data-management.service';
import {of} from 'rxjs';

describe('ShowPreviewImagesComponent', () => {
  let component: ShowPreviewImagesComponent;
  let fixture: ComponentFixture<ShowPreviewImagesComponent>;
  let dialogRefSpy: jasmine.SpyObj<MatDialogRef<ShowPreviewImagesComponent>>;

  beforeEach(async () => {
    dialogRefSpy = jasmine.createSpyObj<MatDialogRef<ShowPreviewImagesComponent>>('ShowPreviewImagesComponent', ['close']);

    await TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatCardModule,
        MatChipsModule,
        MatButtonModule,
        MatDialogModule,
        HttpClientTestingModule
      ],
      declarations: [ShowPreviewImagesComponent],
      providers: [
        {provide: MAT_DIALOG_DATA, useValue: '23'},
        {
          provide: MatDialogRef,
          useValue: dialogRefSpy
        },
        DataManagementService
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ShowPreviewImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load dataset', () => {
    expect(component.loadDatasetSpinner).toBe(true);
    // Arrange
    const dummyDataset = <Dataset>{
      labelStudioProjectId: 1,
      name: 'MyDataset',
      datasetType: DATASET_TYPES[0],
      tags: [],
      classes: [],
      lastModified: new Date(),
      annotatedImages: 0,
      totalImages: 0,
      previewImages: [],
    };

    spyOn(component.dataManagementService, 'getDataset').and.returnValue(of(dummyDataset));
    // Act
    component.loadDataset(1);

    // Verify
    expect(component.dataManagementService.getDataset).toHaveBeenCalledOnceWith(1);
    expect(component.dataset).toEqual(dummyDataset);
    expect(component.loadDatasetSpinner).toBe(false);
  });

});
