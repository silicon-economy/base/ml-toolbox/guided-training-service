/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {AfterViewInit, Component, ViewChild, OnInit, OnDestroy, ElementRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DatasetDataSource} from 'src/app/core/backend/dataset.datasource';
import {DataManagementService} from 'src/app/core/backend/data-management.service';
import {Subscription, debounceTime, distinctUntilChanged, fromEvent, merge, tap} from 'rxjs';
import {Dataset} from 'src/app/core/models/dataset';
import {MatDialog} from '@angular/material/dialog';
import {
  AddNewDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/add-new-dataset-dialog/add-new-dataset-dialog.component';
import {
  DeleteDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/delete-dataset-dialog/delete-dataset-dialog.component';
import {
  DownloadDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/download-dataset-dialog/download-dataset-dialog.component';
import {
  EditDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/edit-dataset-dialog/edit-dataset-dialog.component';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {
  AutoAnnotateDialogComponent
} from 'src/app/features/pages/data-management-page/auto-annotate-dialog/auto-annotate-dialog.component';
import {
  ShowPreviewImagesComponent
} from 'src/app/features/pages/data-management-page/show-preview-images/show-preview-images.component';

@Component({
  selector: 'app-data-management-page',
  templateUrl: './data-management-page.component.html',
  styleUrls: ['./data-management-page.component.scss']
})
export class DataManagementPageComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns: string[] = ['name', 'type', 'annotatedImages', 'totalImages', 'lastModified', 'tags',
    'action'];
  dataSource: DatasetDataSource;
  totalDatasets = 0;

  downloadingDataset = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filterInput') filterInput: ElementRef;

  // store all open subscriptions
  private readonly subscriptions = new Subscription();

  constructor(private dataManagementService: DataManagementService, public dialog: MatDialog, private toasterService: ToasterService) {
  }

  ngOnInit(): void {
    this.dataSource = new DatasetDataSource(this.dataManagementService);
    this.subscriptions.add(
      this.dataSource.totalDatasets.subscribe(totalDatasets => {
          this.totalDatasets = totalDatasets;
        }
      ));

    // TODO: get this settings automatically from HTML defaults and run this.loadDatasets() instead
    this.dataSource.loadDatasets('', 'lastModified', 'desc', 0, 10);
  }

  ngAfterViewInit() {
    this.subscriptions.add(
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)
    );

    this.subscriptions.add(
      fromEvent(this.filterInput.nativeElement, 'keyup')
        .pipe(
          debounceTime(150),
          distinctUntilChanged(),
          tap(() => {
            this.paginator.pageIndex = 0;
            this.loadDatasets();
          })
        )
        .subscribe()
    );

    // reload on sort or paginator change
    this.subscriptions.add(
      merge(this.sort.sortChange, this.paginator.page)
        .pipe(
          tap(() => this.loadDatasets())
        )
        .subscribe()
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  /**
   * Loads the datasets from the backend.
   */
  loadDatasets() {
    // TODO: add sortName for different column names
    // TODO: set and display default sort in HTML
    let sortName = 'lastModified';
    let sortOrder = 'desc';
    if (this.sort.active && this.sort.direction) {
      sortName = this.sort.active;
      sortOrder = this.sort.direction;
    }
    this.dataSource.loadDatasets(
      this.filterInput.nativeElement.value,
      sortName,
      sortOrder,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  /**
   * Clears the filter input and resets the paginator to the first page.
   */
  clearFilter() {
    this.filterInput.nativeElement.value = '';
    this.paginator.pageIndex = 0;
    this.loadDatasets();
  }

  /**
   * Opens the EditDatasetDialogComponent to edit the given dataset.
   *
   * @param dataset The dataset that should be edited.
   */
  editDataset(dataset: Dataset) {
    const dialogRef = this.dialog.open(EditDatasetDialogComponent, {
      height: '80%',
      width: '50%',
      data: {
        dataset: dataset
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(() => {
      this.loadDatasets();
    });
  }

  /**
   * Opens Labelstudio in a new tab
   *
   * @param dataset
   */
  openLabelstudio(dataset: Dataset) {
    window.open(dataset.labelStudioUrl, '_blank');
  }

  /**
   * Opens the DeleteDatasetDialog to delete the given dataset.
   *
   * @param dataset The dataset that should be deleted.
   */
  deleteDataset(dataset: Dataset) {
    const dialogRef = this.dialog.open(DeleteDatasetDialogComponent, {
      data: dataset,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataManagementService.deleteDataset(dataset).subscribe({
          next: () => {
            this.toasterService.success(`Dataset ${dataset.name} was deleted.`, 'Dataset deleted');
            this.loadDatasets();
          },
          error: () => this.toasterService.error(`Dataset ${dataset.name} could not be deleted.`, 'Failed to delete Dataset')
        });
      }
    });
  }

  // This utility method is used to download a file from an url without causing a pop-up warning or similar
  // We require this extra method to make this component testable
  // as spying on window.location.assign is not directly possible
  /**
   * Downloads a file from the given url
   *
   * @param url The URL of the file that should be downloaded.
   */
  _downloadFile(url: string | URL) {
    window.location.assign(url);
  }

  /**
   * Opens the DownloadDatasetDialog to download the annotations of the given dataset.
   *
   * @param dataset The dataset for which the annotations should be downloaded.
   */
  downloadDataset(dataset: Dataset) {
    this.downloadingDataset = true;
    const dialogRef = this.dialog.open(DownloadDatasetDialogComponent, {
      data: dataset,
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.dataManagementService.exportDatasetAnnotation(dataset).subscribe({
          next: (downloadUrl) => {
            this.toasterService.success(`Downloading dataset annotations for '${dataset.name}'.`);
            this._downloadFile(downloadUrl.toString());
            this.downloadingDataset = false;
          },
          error: () => {
            this.toasterService.error('Failed to request download url for exported annotations.');
            this.downloadingDataset = false;
          }
        });
      }
    });
  }

  /**
   * Opens the AddNewDatasetDialogComponent to add a new dataset.
   */
  addNewDataset() {
    const dialogRef = this.dialog.open(AddNewDatasetDialogComponent, {
      height: '80%',
      width: '50%',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(() => {
      this.loadDatasets();
    });
  }

  /**
   * Opens the AutoAnnotateDialogComponent to auto annotate the given dataset.
   *
   * @param dataset The dataset that should be auto annotated.
   */
  autoAnnotateDialog(dataset: Dataset) {
    const dialogRef = this.dialog.open(AutoAnnotateDialogComponent, {
      height: '80%',
      width: '50%',
      data: {
        dataset: dataset
      },
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(() => {
      this.ngOnInit();
    });
  }

  /**
   * Opens the ShowPreviewImagesComponent to show the preview images of the given dataset.
   *
   * @param dataset The dataset for which the preview images should be shown.
   */
  showPreviewImages(dataset: Dataset) {
    this.dialog.open(ShowPreviewImagesComponent, {
      data: dataset.labelStudioProjectId,
      height: '80%',
      width: '50%',
      disableClose: true,
    });
  }
}
