/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {finalize} from 'rxjs';
import {DataManagementService} from 'src/app/core/backend/data-management.service';
import {ModelManagementService} from 'src/app/core/backend/model-management.service';
import {ModelTestingService} from 'src/app/core/backend/model-testing.service';
import {Dataset} from 'src/app/core/models/dataset';
import {Model} from 'src/app/core/models/model';
import {MatDialog} from "@angular/material/dialog";
import {TrainingServer} from "src/app/core/models/trainingserver";
import {TrainingServerService} from "src/app/core/backend/training-server.service";

@Component({
  selector: 'app-model-testing-page',
  templateUrl: './model-testing-page.component.html',
  styleUrls: ['./model-testing-page.component.scss']
})
export class ModelTestingPageComponent implements OnInit {
  // datasets from Backend
  datasets: Dataset[] = [];
  // dataset which is picked from User
  selectedDataset: Dataset;
  // models from Backend
  models: Model[] = [];
  // model which is picked from User
  selectedModel: Model;
  // second model
  secondModel: Model;
  // servers from Backend
  servers: TrainingServer[];
  // server which is picked from the user
  selectedServer: TrainingServer;

  images: string[] = [];
  currentIndex = 0; // Index of the currently displayed image

  loading: boolean;
  loadingDatasets: boolean;

  // Formcontrols
  testingParameterGroup = new FormGroup({
    dataSetCtrl: new FormControl([], [
      Validators.required
    ]),
    modelCtrl: new FormControl([], [
      Validators.required
    ]),
    secondModelCtrl: new FormControl([]),
    serverCtrl: new FormControl('', [
      Validators.required
    ])
  });

  /**
   * Constructs the ModelTestingPageComponent with the given parameters
   * @param dataManagementService The data management service
   * @param dialog A MatDialog object
   * @param modelManagementService The model management service
   * @param modelTestingService The model testing service
   * @param serverService The training server service
   */
  constructor(public dataManagementService: DataManagementService, public dialog: MatDialog, public modelManagementService: ModelManagementService, private modelTestingService: ModelTestingService, public serverService: TrainingServerService) {
  }

  ngOnInit(): void {
    this.loadingDatasets = true;
    // get datasets from backend and put it in attribute datasets
    this.dataManagementService.getDatasets('', 'lastModified', 'desc', 0, 50).pipe(finalize(() => {
      this.loadingDatasets = false;
    })).subscribe({
      next: (res) => {
        this.datasets = this.dataManagementService.sortDatasetList(res.datasets);
        this.selectedDataset = this.datasets[1];
        console.log('Request completed: Load datasets');
      }
    });
    // get models from backend and put it in attribute models
    this.modelManagementService.getModels('', 'creation_date', 'desc', 0, 1000).pipe().subscribe({
      next: (res) => {
        this.models = this.modelManagementService.sortModelList(res.models);
      }
    });
    // get server from backend and put it in attribute server
    this.loadServer();

  }

  /**
   * Loads all servers from the backend.
   */
  loadServer() {
    this.serverService.getAllTrainingsServers().subscribe({
      next: (res) => {
        this.selectedServer = undefined;
        this.testingParameterGroup.get('serverCtrl').reset();
        this.servers = res;
        console.log('Request completed: Load server')
      }
    })
  }

  /**
   * Sets the selected dataset.
   *
   * @param dataset The dataset that should be selected.
   */
  datasetSelected(dataset: Dataset) {
    this.selectedDataset = dataset;
  }

  /**
   * Selects the first model.
   *
   * @param model The first model that should be selected.
   */
  modelSelected(model: Model) {
    this.selectedModel = model;
  }

  /**
   * Selects the second model.
   *
   * @param model The second model that should be selected.
   */
  secondModelSelected(model: Model) {
    this.secondModel = model;
  }

  /**
   * Sets the selected server.
   *
   * @param server The server that should be selected.
   */
  serverSelected(server: TrainingServer) {
    this.selectedServer = server;
  }

  /**
   * Tests the model with the given parameters given in the form and displays the images from the test.
   */
  testModel() {
    this.images = [];
    this.currentIndex = 0;
    this.loading = true;
    if (this.secondModel === undefined) {
      this.secondModel = {
        run_id: '',
        name: '',
        description: '',
        dataset: [],
        model: '',
        evaluation_datasets: [],
        epochs: 0,
        batch_size: 0,
        annotation_task: [],
        training_state: '',
        accuracy: 0,
        algorithm_type: '',
        creation_date: new Date(),
        mlflow_url: ''
      };
    }
    this.modelTestingService.startModelTesting(this.selectedDataset.labelStudioProjectId, this.selectedModel.name, this.secondModel.name, this.selectedServer.endpoint).pipe(finalize(() => {
      this.loading = false;
    })).subscribe(res => {
      if (res.images && res.images.length) {
        // set images
        res.images.forEach(image => {
          this.images.push(image.url);
        });
        console.log(res);
      } else {
        console.log("No images found in the response.");
        this.images = [];
      }
    });
  }

  /**
   * Shows the previous image.
   */
  showPreviousImage() {
    this.currentIndex = (this.currentIndex - 1 + this.images.length) % this.images.length;
  }

  /**
   * Shows the next image.
   */
  showNextImage() {
    this.currentIndex = (this.currentIndex + 1) % this.images.length;
  }
}
