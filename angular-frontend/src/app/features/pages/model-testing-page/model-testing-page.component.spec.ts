/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ModelTestingPageComponent} from 'src/app/features/pages/model-testing-page/model-testing-page.component';
import {DataManagementService} from 'src/app/core/backend/data-management.service';
import {ModelManagementService} from 'src/app/core/backend/model-management.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DatasetResponse} from 'src/app/core/backend/data-management.service';
import {ModelsResponse} from 'src/app/core/backend/model-management.service';
import {ModelTestingService} from 'src/app/core/backend/model-testing.service';
import {of} from 'rxjs';
import {Dataset} from 'src/app/core/models/dataset';
import {Model} from 'src/app/core/models/model';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {TrainingServer} from 'src/app/core/models/trainingserver';
import {TrainingServerService} from 'src/app/core/backend/training-server.service';
import {DATASET_TYPES} from 'src/app/core/backend/data-management.service'

describe('ModelTestingPageComponent', () => {
  let component: ModelTestingPageComponent;
  let fixture: ComponentFixture<ModelTestingPageComponent>;
  let dataManagementService: DataManagementService;
  let modelManagementService: ModelManagementService;
  let modelTestingService: ModelTestingService;
  let trainingServerService: TrainingServerService;


  // Mock data
  const mockDatasets: Dataset[] = [
    // Mock dataset objects here
  ];
  const mockModels: Model[] = [
    // Mock model objects here
  ];
  const mockServers: TrainingServer[] = [
    // Mock server objects here
  ];

  const selectedModel: Model = {
    run_id: '1',
    name: 'test',
    description: 'test',
    dataset: ['test'],
    evaluation_datasets: ['test'],
    model: 'test',
    epochs: 2,
    batch_size: 3,
    annotation_task: [],
    training_state: 'running',
    accuracy: 54,
    algorithm_type: 'test',
    creation_date: new Date(),
    mlflow_url: '',
  };

  const selectedDataset: Dataset = {
    labelStudioProjectId: 1,
    name: 'MyDataset',
    datasetType: DATASET_TYPES[0],
    tags: [],
    classes: [],
    lastModified: new Date(),
    annotatedImages: 0,
    totalImages: 0,
    previewImages: [],
    labelStudioUrl: '',
  };

  const selectedServer: TrainingServer = {
    name: 'testServer',
    endpoint: 'http://example.com',
    id: 0,
    selected: false,
    inEditing: false
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientModule,
        MatFormFieldModule,
        MatSelectModule,
        MatTooltipModule,
        BrowserAnimationsModule,
        MatIconModule,
        MatInputModule,
        MatDialogModule
      ],
      declarations: [ModelTestingPageComponent],
      providers: [
        DataManagementService,
        ModelManagementService,
        ModelTestingService,
        TrainingServerService,
        {provide: MatDialogRef, useValue: {}}
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelTestingPageComponent);
    component = fixture.componentInstance;
    dataManagementService = TestBed.inject(DataManagementService);
    modelManagementService = TestBed.inject(ModelManagementService);
    modelTestingService = TestBed.inject(ModelTestingService);
    trainingServerService = TestBed.inject(TrainingServerService);
    const mockDatasetResponse: DatasetResponse = {
      datasets: mockDatasets,
      totalDatasets: mockDatasets.length,
    };
    const mockModelsResponse: ModelsResponse = {
      models: mockModels,
      totalModels: mockModels.length,
    };
    const mockServersResponse: TrainingServer[] = mockServers

    // Spy on service methods
    spyOn(dataManagementService, 'getDatasets').and.returnValue(of(mockDatasetResponse));
    spyOn(modelManagementService, 'getModels').and.returnValue(of(mockModelsResponse));
    spyOn(trainingServerService, 'getAllTrainingsServers').and.returnValue(of(mockServersResponse));


    fixture.detectChanges();
  });

  it('should fetch datasets and models', () => {
    // Trigger the ngOnInit method
    component.ngOnInit();

    // Expect dataManagementService.getDatasets to have been called once
    expect(dataManagementService.getDatasets).toHaveBeenCalled();

    // Expect component.datasets to have been assigned with the mock datasets
    expect(component.datasets).toEqual(mockDatasets);

    // Expect component.selectedDataset to have been assigned with the second dataset from mockDatasets
    expect(component.selectedDataset).toEqual(mockDatasets[1]);

    // Expect modelManagementService.getModels to have been called once
    expect(modelManagementService.getModels).toHaveBeenCalled();

    // Expect component.models to have been assigned with the mock models
    expect(component.models).toEqual(mockModels);

    // Expect trainingServerService.getAllTrainingsServers to have been called once
    expect(trainingServerService.getAllTrainingsServers).toHaveBeenCalled();

    // Expect component.servers to have been assigned with the mock servers
    expect(component.servers).toEqual(mockServers);

    // Expect loadingDatasets to be set to false after completion
    expect(component.loadingDatasets).toBe(false);
  });

  it('should set the selectedDataset when datasetSelected method is called', () => {
    // Call the method
    component.datasetSelected(selectedDataset);

    // Expect the selectedDataset to have been set
    expect(component.selectedDataset).toEqual(selectedDataset);
  });

  it('should set the selectedModel when modelSelected method is called', () => {
    // Call the method
    component.modelSelected(selectedModel);

    // Expect the selectedModel to have been set
    expect(component.selectedModel).toEqual(selectedModel);
  });

  it('should set selected second model on secondModelSelected', () => {
    // Act
    component.secondModelSelected(selectedModel);
    // Verify
    expect(component.secondModel).toEqual(selectedModel);
  });

  it('should switch to the previous image correctly', () => {
    // Set currentIndex to a specific value
    component.images.length = 5;
    component.currentIndex = 3;

    // Call the function
    component.showPreviousImage();

    // Expect that the currentIndex is updated correctly
    expect(component.currentIndex).toBe(2);
  });

  it('should switch to the next image correctly', () => {
    // Set currentIndex to a specific value
    component.images.length = 5;
    component.currentIndex = 2;

    // Call the function
    component.showNextImage();

    // Expect that the currentIndex is updated correctly
    expect(component.currentIndex).toBe(3);
  });

  it('should call testModel and handle the response', fakeAsync(() => {
    const mockResponse = {
      images: [
        {url: 'image1.jpg'},
        {url: 'image2.jpg'},
      ],
    };

    // Spy on the modelTestingService.startModelTesting method and return a mock response
    const startModelTestingSpy = spyOn(modelTestingService, 'startModelTesting').and.returnValue(of(mockResponse));

    // Set values in your component that are used by testModel
    component.selectedDataset = selectedDataset;
    component.selectedModel = selectedModel;
    component.secondModel = selectedModel;
    component.selectedServer = selectedServer;

    // Call the testModel method
    component.testModel();

    // Tick to simulate the passage of time, which is necessary for the asynchronous code inside testModel
    tick();

    // Expect that the component's properties were updated based on the response
    expect(component.images).toEqual(['image1.jpg', 'image2.jpg']);
    expect(component.currentIndex).toBe(0);
    expect(component.loading).toBe(false);

    // Expect that startModelTesting was called with the expected arguments
    expect(startModelTestingSpy).toHaveBeenCalledWith(1, 'test', 'test', 'http://example.com');
  }));

  it('should set selectedServer when serverSelected is called', () => {
    const testServer: TrainingServer = {
      name: 'TestServer',
      endpoint: 'http://test-server',
      id: 1,
      inEditing: false,
      selected: false
    };
    component.serverSelected(testServer);
    expect(component.selectedServer).toEqual(testServer);
  });

  afterEach(() => {
    fixture.destroy();
  });
});
