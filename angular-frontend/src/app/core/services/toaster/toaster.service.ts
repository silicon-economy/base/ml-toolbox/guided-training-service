/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Toast} from 'src/app/core/services/toaster/toast.interface';
import {ToastType} from 'src/app/core/services/toaster/toast.type';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {
  subject: BehaviorSubject<Toast>;
  toast$: Observable<Toast>;

  /**
   * Creates an instance of ToasterService.
   */
  constructor() {
    this.subject = new BehaviorSubject<Toast>(null);
    this.toast$ = this.subject.asObservable()
      .pipe(filter(toast => toast !== null));
  }

  /**
   * Shows a toast with the given type, title, body, and delay.
   *
   * @param type The type of the toast.
   * @param title The title of the toast.
   * @param body The body of the toast.
   * @param delay The amount of milliseconds the toast should be displayed for.
   */
  private show(type: ToastType, title?: string, body?: string, delay?: number) {
    this.subject.next({type, title, body, delay});
  }

  /**
   * Shows a success toast with the given body, title and delay.
   *
   * @param body The body of the toast.
   * @param title The title of the toast.
   * @param delay The amount of milliseconds the toast should be displayed for.
   */
  success(body?: string, title?: string, delay?: number) {
    this.show('success', title, body, delay);
  }

  /**
   * Shows an error toast with the given body, title and delay.
   *
   * @param body The body of the toast.
   * @param title The title of the toast.
   * @param delay The amount of milliseconds the toast should be displayed for.
   */
  error(body?: string, title?: string, delay?: number) {
    this.show('error', title, body, delay);
  }

  /**
   * Shows an info toast with the given body, title and delay.
   *
   * @param body The body of the toast.
   * @param title The title of the toast.
   * @param delay The amount of milliseconds the toast should be displayed for.
   */
  warning(body?: string, title?: string, delay?: number) {
    this.show('warning', title, body, delay);
  }

}
