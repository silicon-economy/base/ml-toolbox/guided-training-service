/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, OnInit} from '@angular/core';
import {Toast} from 'src/app/core/services/toaster/toast.interface';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

@Component({
  selector: 'app-toaster-container',
  templateUrl: './toaster-container.component.html',
  styles: []
})

export class ToasterContainerComponent implements OnInit {

  toasts: Toast[] = [];

  /**
   * Constructor for the ToasterContainerComponent.
   */
  constructor(private toaster: ToasterService) {
  }

  /**
   * Initializes the component and subscribes to the toast observable.
   */
  ngOnInit() {
    this.toaster.toast$
      .subscribe(toast => {
        this.toasts = [toast, ...this.toasts];
        setTimeout(() => this.toasts.pop(), toast.delay || 6000);
      });
  }

  /**
   * Removes the toast at a given index.
   *
   * @param index The index of the toast to remove.
   */
  remove(index: number) {
    this.toasts = this.toasts.filter((v, i) => i !== index);
  }
}
