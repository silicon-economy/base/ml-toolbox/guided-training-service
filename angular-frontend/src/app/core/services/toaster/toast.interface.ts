/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ToastType} from 'src/app/core/services/toaster/toast.type';

/**
 * The interface that defines a toast.
 */
export interface Toast {
  /** The type of Toast. */
  type: ToastType;
  /** The title of the toast. */
  title: string;
  /** The body of the toast. */
  body: string;
  /** The amount of milliseconds the toast should be displayed for. */
  delay: number;
}
