/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

describe('ToasterService', () => {
  let toasterService: ToasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToasterService]
    });
    toasterService = TestBed.inject(ToasterService);
  });

  it('should create the service', () => {
    expect(toasterService).toBeTruthy();
  });

  it('should show a success toast', () => {
    toasterService.success('Success message');
    toasterService.toast$.subscribe((toast) => {
      expect(toast.type).toBe('success');
      expect(toast.title).toBeFalsy();
      expect(toast.body).toBe('Success message');
      expect(toast.delay).toBeFalsy();
    });
  });

  it('should show an error toast', () => {
    toasterService.error('Error message');
    toasterService.toast$.subscribe((toast) => {
      expect(toast.type).toBe('error');
      expect(toast.title).toBeFalsy();
      expect(toast.body).toBe('Error message');
      expect(toast.delay).toBeFalsy();
    });
  });

  it('should show a warning toast', () => {
    toasterService.warning('Warning message');
    toasterService.toast$.subscribe((toast) => {
      expect(toast.type).toBe('warning');
      expect(toast.title).toBeFalsy();
      expect(toast.body).toBe('Warning message');
      expect(toast.delay).toBeFalsy();
    });
  });
});
