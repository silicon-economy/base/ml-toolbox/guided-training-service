/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {Mode, ModeIcon, ModeName} from 'src/app/core/models/mode';
import {Theme, ThemeName} from 'src/app/core/models/theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private readonly ModeStorageKey = 'current-mode';
  private readonly ThemeStorageKey = 'current-theme';
  private readonly Modes: Mode[] = [
    {name: 'light-theme', icon: 'light_mode'},
    {name: 'dark-theme', icon: 'dark_mode'},
  ];
  private readonly Themes: Theme[] = [
    {name: 'lightblue', label: 'Light Blue'},
    {name: 'blue', label: 'Blue'},
  ];

  private modeName$: ReplaySubject<ModeName> = new ReplaySubject<ModeName>(1);
  private modeName: ModeName = localStorage.getItem(this.ModeStorageKey) as ModeName;

  private themeName$: ReplaySubject<ThemeName> = new ReplaySubject<ThemeName>(1);
  private themeName: ThemeName = localStorage.getItem(this.ThemeStorageKey) as ThemeName;

  constructor() {
    this.modeName$.asObservable().subscribe(mode => {
      this.modeName = mode;
      this.setLocalStorageMode(mode);
    });
    this.themeName$.asObservable().subscribe(theme => {
      this.themeName = theme;
      this.setLocalStorageTheme(theme);
    });
    const tmpMode = this.getLocalStorageMode();
    this.modeName$.next(tmpMode ? tmpMode : this.Modes[0].name);

    const tmpTheme = this.getLocalStorageTheme();
    this.themeName$.next(tmpTheme ? tmpTheme : this.Themes[0].name);

  }

  /**
   * Get the current mode name.
   *
   * @returns An observable that emits the current mode name.
   */
  public getModeName$(): Observable<ModeName> {
    return this.modeName$.asObservable();
  }

  /**
   * Get the current mode name.
   *
   * @returns The current mode name.
   */
  public getModeName(): ModeName {
    return this.modeName;
  }

  /**
   * Get the current theme name.
   *
   * @returns An observable that emits the current theme name.
   */
  public getThemeName$(): Observable<ThemeName> {
    return this.themeName$.asObservable();
  }

  /**
   * Get the current theme name.
   *
   * @returns The current theme name.
   */
  public getThemeName(): ThemeName {
    return this.themeName;
  }

  /**
   * Get the currently selected mode object.
   *
   * @returns The currently selected mode object.
   */
  public getModeObj(): Mode {
    return this.Modes.find(x => x.name == this.modeName);
  }

  /**
   * Get the currently selected theme object.
   *
   * @returns The currently selected theme object.
   */
  public getThemeObj(): Theme {
    return this.Themes.find(x => x.name == this.themeName);
  }

  /**
   * Get the next mode object in the sequence. If the current mode is the last mode, the first mode will be returned.
   *
   * @returns The next mode object in the sequence.
   */
  public getNextModeObj(): Mode {
    const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeName));
    const nextIndex = (currentIndex + 1) % this.Modes.length;
    return this.Modes[nextIndex];
  }

  /**
   * Get the icon string identifier for the next mode in the sequence. If the current mode is the last mode, the first mode's icon will be returned.
   *
   * @returns The icon string identifier for the next mode in the sequence.
   */
  public getNextModeIcon(): ModeIcon {
    const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeName));
    const nextIndex = (currentIndex + 1) % this.Modes.length;
    return this.Modes[nextIndex].icon;
  }

  /**
   * Updates the mode and saves it to local storage.
   *
   * @param name The name of the mode to set.
   */
  public setMode(name: ModeName): void {
    if (this.Modes.find(x => x.name == name) === undefined) {
      console.warn('Requested mode-name does not match available mode names.');
    } else {
      this.modeName$.next(name);
    }
  }

  /**
   * Updates the theme and saves it to local storage.
   *
   * @param name The name of the theme to set.
   */
  public setTheme(name: ThemeName): void {
    if (this.Themes.find(x => x.name == name) === undefined) {
      console.warn('Requested theme-name does not match available theme names.');
    } else {
      this.themeName$.next(name);
    }
  }

  /**
   * Cycle through the available modes and themes.
   */
  public cycleMode() {
    const currentModeIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeName));
    const currentThemeIndex = this.Themes.indexOf(this.Themes.find(x => x.name == this.themeName));
    const nextModeIndex = (currentModeIndex + 1) % this.Modes.length;
    const nextThemeIndex = (currentThemeIndex + 1) % this.Themes.length;
    this.modeName$.next(this.Modes[nextModeIndex].name);
    this.themeName$.next(this.Themes[nextThemeIndex].name);
  }

  /**
   * Get all available modes.
   *
   * @returns all available modes.
   */
  public getAvailableModes(): Mode[] {
    return this.Modes;
  }

  /**
   * Get all available themes.
   *
   * @returns all available themes.
   */
  public getAvailableThemes(): Theme[] {
    return this.Themes;
  }

  /**
   * Get the class name for the current theme and mode.
   *
   * @returns The class name for the current theme and mode.
   */
  public getClassName(): string {
    return this.themeName + '-' + this.modeName;
  }

  /**
   * Get the mode name from local storage.
   *
   * @returns The mode name from local storage.
   */
  private getLocalStorageMode(): ModeName | null {
    return localStorage.getItem(this.ModeStorageKey) as ModeName;
  }

  /**
   * Get the theme name from local storage.
   *
   * @return The theme name from local storage.
   */
  private getLocalStorageTheme(): ThemeName | null {
    return localStorage.getItem(this.ThemeStorageKey) as ThemeName;
  }

  /**
   * Set the mode name in local storage.
   *
   * @param mode The mode name to set.
   */
  private setLocalStorageMode(mode: ModeName): void {
    localStorage.setItem(this.ModeStorageKey, mode);
  }

  /**
   * Set the theme name in local storage.
   *
   * @param theme The theme name to set.
   */
  private setLocalStorageTheme(theme: ThemeName): void {
    localStorage.setItem(this.ThemeStorageKey, theme);
  }
}
