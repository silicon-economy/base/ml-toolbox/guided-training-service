/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';

import {ThemeService} from 'src/app/core/services/state/theme.service';

describe('ThemeService', () => {
  let service: ThemeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThemeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should cycle through modes', () => {
    // init to known state
    const first_mode = service.getAvailableModes()[0];
    const first_theme = service.getAvailableThemes()[0];
    service.setMode(first_mode.name);
    service.setTheme(first_theme.name);
    expect(service.getModeName()).toEqual(first_mode.name);
    expect(service.getThemeName()).toEqual(first_theme.name);

    // cycle through modes
    const localStorageSetSpy = spyOn(Object.getPrototypeOf(localStorage), 'setItem');
    for (let index = 1; index < service.getAvailableModes().length; index++) {
      const mode_element = service.getAvailableModes()[index];
      const theme_element = service.getAvailableThemes()[index];

      service.cycleMode();

      expect(localStorageSetSpy).toHaveBeenCalledTimes(2);
      expect(localStorageSetSpy).toHaveBeenCalledWith('current-mode', mode_element.name);
      expect(localStorageSetSpy).toHaveBeenCalledWith('current-theme', theme_element.name);
      expect(service.getModeName()).toEqual(mode_element.name);
      expect(service.getThemeName()).toEqual(theme_element.name);
      localStorageSetSpy.calls.reset();
    }

    // should roll over to first mode after cycling through all other ones
    service.cycleMode();

    expect(localStorageSetSpy).toHaveBeenCalledTimes(2);
    expect(localStorageSetSpy).toHaveBeenCalledWith('current-mode', first_mode.name);
    expect(localStorageSetSpy).toHaveBeenCalledWith('current-theme', first_theme.name);
    expect(service.getModeName()).toEqual(first_mode.name);
    expect(service.getThemeName()).toEqual(first_theme.name);
  });

  it('should save the new theme', () => {
    const themes = service.getAvailableThemes();
    service.setTheme(themes[0].name);

    const localStorageSetSpy = spyOn(Object.getPrototypeOf(localStorage), 'setItem');

    const element = themes[themes.length - 1];
    service.setTheme(element.name);

    expect(localStorageSetSpy).toHaveBeenCalledOnceWith('current-theme', element.name);
    expect(service.getThemeName()).toEqual(element.name);
  });
});
