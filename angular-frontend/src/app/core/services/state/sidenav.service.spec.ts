/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';
import {first} from 'rxjs/operators';

import {SidenavService} from 'src/app/core/services/state/sidenav.service';

describe('SidenavService', () => {
  let service: SidenavService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SidenavService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should toggle sidenav state and save to localstore', () => {
    const localStorageSetSpy = spyOn(Object.getPrototypeOf(localStorage), 'setItem');

    let oldState = null;
    service.getSidenavState$().pipe(first()).subscribe((state) => {
      oldState = state;
    });

    service.toggleSidenavState();

    let newState = null;
    service.getSidenavState$().pipe(first()).subscribe((state) => {
      newState = state;
    });

    expect(oldState).toEqual(jasmine.any(Boolean));
    expect(newState).toEqual(jasmine.any(Boolean));
    expect(oldState).toEqual(!newState);

    expect(localStorageSetSpy).toHaveBeenCalledOnceWith('desktop-sidenav-state', newState.toString());
  });

  it('should only emit the last state', () => {

    let initialState = null;
    service.getSidenavState$().pipe(first()).subscribe((state) => {
      initialState = state;
    });

    // generate some events
    service.toggleSidenavState();
    service.toggleSidenavState();
    service.toggleSidenavState();

    const stateUpdates = [];
    service.getSidenavState$().subscribe((state) => {
      stateUpdates.push(state);
    });

    expect(initialState).toEqual(jasmine.any(Boolean));
    expect(stateUpdates).toEqual([!initialState]);
  });
});
