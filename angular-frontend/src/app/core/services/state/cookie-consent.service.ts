/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CookieConsentService {

  private readonly storageKey = 'cookieConsent';

  private cookieConsent$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  /**
   * Constructs a new `CookieConsentService` and loads the cookie consent state from local storage.
   */
  constructor() {
    this.cookieConsent$.next(localStorage.getItem(this.storageKey) === 'true');
  }

  /**
   * Get the current cookie consent state.
   *
   * @returns An observable that emits the current cookie consent state.
   */
  public getCookieConsent(): Observable<boolean> {
    return this.cookieConsent$.asObservable();
  }

  /**
   * Set the cookie consent state and store it in local storage.
   *
   * @param consent The new cookie consent state.
   */
  public setCookieConsent(consent: boolean): void {
    this.cookieConsent$.next(consent);
    localStorage.setItem(this.storageKey, consent.toString());
  }
}
