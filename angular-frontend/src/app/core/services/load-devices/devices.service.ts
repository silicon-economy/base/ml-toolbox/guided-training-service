/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {EdgeDevice} from 'src/app/core/models/deployment';
import {environment} from "src/environments/environment";
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Model} from "src/app/core/models/model";

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  private readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  /**
   * Constructs the `DevicesService` with the required `HttpClient`.
   *
   * @param http A HTTP client to make requests to the server.
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Get all known edge devices from the server.
   *
   * @returns An observable that emits an array of edge devices.
   */
  getDevices(): Observable<EdgeDevice[]> {
    return this.http.get<EdgeDevice[]>(`${this.API_URL}/edge-devices/`)
      .pipe(map((devices: EdgeDevice[]) => {
        return devices;
      }));
  }

  /**
   * Save a new edge device to the server.
   *
   * @param edgeDevice The edge device to save.
   */
  save(edgeDevice: EdgeDevice) {
    const body = {
      'name': edgeDevice.name,
      'uri': edgeDevice.uri,
      'deviceType': edgeDevice.deviceType,
    }
    return this.http.post(`${this.API_URL}/edge-devices/`, body);
  }

  /**
   * Deploy models to an edge device.
   *
   * @param model The first model to deploy.
   * @param secondModel The second model to deploy.
   * @param device The edge device to deploy to.
   */
  deployStart(model: Model, secondModel: Model, device: EdgeDevice) {
    const body = {
      'id': device.id,
      'model': model,
      'second_model': secondModel
    }
    return this.http.post(`${this.API_URL}/edge-devices/${device.id}/deployment/start`, body);
  }

  /**
   * Stop the deployment of models on an edge device.
   *
   * @param device The edge device to stop deployment on.
   */
  deployStop(device: EdgeDevice) {
    return this.http.get(`${this.API_URL}/edge-devices/${device.id}/deployment/stop`);
  }

  /**
   * Delete an edge device from the server.
   *
   * @param device The edge device to delete.
   */
  delete(device: EdgeDevice) {
    return this.http.delete(`${this.API_URL}/edge-devices/${device.id}`);
  }

  /**
   * Get the details of a specific edge device.
   *
   * @param deviceId The ID of the edge device to get details for.
   */
  getDeviceDetails(deviceId: number): Observable<EdgeDevice> {
    return this.http.get<EdgeDevice>(`${this.API_URL}/edge-devices/${deviceId}`)
  }

  /**
   * Start recording on an edge device.
   *
   * @param deviceId The ID of the edge device to start recording on.
   */
  startRecording(deviceId: number) {
    return this.http.get(`${this.API_URL}/edge-devices/${deviceId}/recording/start`);
  }

  /**
   * Stop recording on an edge device.
   *
   * @param deviceId The ID of the edge device to stop recording on.
   */
  stopRecording(deviceId: number) {
    return this.http.get(`${this.API_URL}/edge-devices/${deviceId}/recording/stop`);
  }
}
