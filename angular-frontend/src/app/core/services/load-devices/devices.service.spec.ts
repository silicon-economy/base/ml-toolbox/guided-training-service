/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {DevicesService} from 'src/app/core/services/load-devices/devices.service';
import {EdgeDevice} from 'src/app/core/models/deployment';
import {Model} from 'src/app/core/models/model';
import {environment} from "src/environments/environment";

const mockDevices: EdgeDevice[] = [
  {
    id: 1,
    name: 'test1',
    uri: 'http://test.de',
    creationDate: 'null',
    deviceType: 'Orin Nano',
    stream: '',
  }
];

describe('DevicesService', () => {
  let service: DevicesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DevicesService]
    });
    service = TestBed.inject(DevicesService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get devices', () => {
    // Act
    service.getDevices().subscribe(devices => {
      // Verify
      expect(devices.length).toBe(mockDevices.length);
      expect(devices).toEqual(mockDevices);
    });

    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/`);
    expect(req.request.method).toBe('GET');
    req.flush(mockDevices);
  });

  it('should save edge device', () => {
    // Act
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    service.save(mockDevices[0]).subscribe(() => {
    });

    // Verify
    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/`);
    expect(req.request.method).toBe('POST');
    req.flush({});
  });

  it('should start deployment', () => {
    // Prepare
    const mockModel: Model = {
      run_id: '0',
      name: 'Dummy-Model',
      description: 'This is a really long description for the model. The model\'s name is "Dummy-Model" and it uses the "Dummy-Dataset" dataset',
      dataset: ['Dummy-Dataset'],
      evaluation_datasets: ['Dummy-Dataset'],
      model: 'modelpath',
      epochs: 30,
      batch_size: 6,
      annotation_task: [],
      training_state: 'running',
      accuracy: 0.8,
      algorithm_type: 'detection',
      creation_date: new Date("2023-08-01T00:00:00.000Z"),
      mlflow_url: "https://example.org/"
    };

    // Act
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    service.deployStart(mockModel, mockModel, mockDevices[0]).subscribe(() => {
    });

    // Verify
    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/${mockDevices[0].id}/deployment/start`);
    expect(req.request.method).toBe('POST');
    req.flush({});
  });

  it('should stop deployment', () => {
    // Act
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    service.deployStop(mockDevices[0]).subscribe(() => {
    });

    // Verify
    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/${mockDevices[0].id}/deployment/stop`);
    expect(req.request.method).toBe('GET');
    req.flush({});
  });

  it('should delete edge device', () => {
    // Act
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    service.delete(mockDevices[0]).subscribe(() => {
    });

    // Verify
    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/${mockDevices[0].id}`);
    expect(req.request.method).toBe('DELETE');
    req.flush({});
  });

  it('should get device details', () => {
    // Prepare
    const deviceId = 1;

    // Act
    service.getDeviceDetails(deviceId).subscribe(device => {
      // Verify
      expect(device).toEqual(mockDevices[0]);
    });

    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/${deviceId}`);
    expect(req.request.method).toBe('GET');
    req.flush(mockDevices[0]);
  });

  it('should start recording', () => {
    // Prepare
    const deviceId = 1;

    // Act
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    service.startRecording(deviceId).subscribe(() => {
    });

    // Verify
    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/${deviceId}/recording/start`);
    expect(req.request.method).toBe('GET');
    req.flush({});
  });

  it('should stop recording', () => {
    // Prepare
    const deviceId = 1;

    // Act
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    service.stopRecording(deviceId).subscribe(() => {
    });

    // Verify
    const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/edge-devices/${deviceId}/recording/stop`);
    expect(req.request.method).toBe('GET');
    req.flush({});
  });
});
