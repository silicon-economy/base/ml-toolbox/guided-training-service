/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClient, HttpClientModule, HttpEvent, HttpEventType} from '@angular/common/http';
import {BehaviorSubject, of, throwError} from 'rxjs';
import {UploadFilePickerAdapter} from 'src/app/core/services/file-upload/upload-file-picker.adapter';
import {Dataset} from 'src/app/core/models/dataset';
import {environment} from 'src/environments/environment';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DATASET_TYPES} from "src/app/core/backend/data-management.service";

describe('UploadFilePickerAdapter', () => {
  let adapter: UploadFilePickerAdapter;
  let httpMock: jasmine.SpyObj<HttpClient>;
  let datasetSubject: BehaviorSubject<Dataset>;
  let annotationFormatSubject: BehaviorSubject<string>;
  let annotationUploadBlockedSubject: BehaviorSubject<boolean>;
  let toasterMock: jasmine.SpyObj<ToasterService>;
  const initialDataset = <Dataset>{
    labelStudioProjectId: -1
  };

  beforeEach(() => {
    httpMock = jasmine.createSpyObj<HttpClient>('HttpClient', ['delete', 'post', 'request', 'patch', 'get']);
    datasetSubject = new BehaviorSubject(initialDataset);
    annotationFormatSubject = new BehaviorSubject('');
    annotationUploadBlockedSubject = new BehaviorSubject(false);
    toasterMock = jasmine.createSpyObj<ToasterService>('ToasterService', ['error', 'success']);
    adapter = new UploadFilePickerAdapter(httpMock, datasetSubject, annotationFormatSubject, annotationUploadBlockedSubject, toasterMock);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [UploadFilePickerAdapter],
    });
  });

  it('should create', () => {
    expect(adapter).toBeTruthy();
  });

  describe('buildRequestURL', () => {
    it('should request a presigned URL and upload the file', async () => {
      httpMock.post.and.returnValue(of({url: 'http://this-is-a-mocked-presigned-url.local'}));
      httpMock.request.and.returnValue(of(<HttpEvent<any>>{
        type: HttpEventType.Response,
        body: {
          data: 'some data from backend'
        }
      }));

      const mockFile = new File([''], 'test.png', {type: 'image/png'});
      const url = await adapter.getRequestURL(mockFile);
      await adapter.uploadToS3(url, mockFile);

      datasetSubject.next(<Dataset>{
        labelStudioProjectId: 42
      });

      //const expectedUrl = `${environment.apiHost}:${environment.apiPort}` + '/datasets/42/data/generateurl';
      //expect(httpMock.post).toHaveBeenCalledOnceWith(expectedUrl, {'file_name': mockFile.name});
      expect(httpMock.request).toHaveBeenCalledTimes(1);
      expect(toasterMock.success).toHaveBeenCalledTimes(0);
      expect(toasterMock.error).toHaveBeenCalledTimes(0);
    });

    it('should get tracking presigned URL', () => {
      const mockFileName = 'test.json';
      const mockLabelStudioProjectId = 42;

      // Mock datasetSubject to return a dataset with a labelStudioProjectId
      datasetSubject.next(<Dataset>{
        labelStudioProjectId: 42
      });

      // Mock the HttpClient.get to return an observable with the expected response
      const expectedUrl = `${environment.apiHost}:${environment.apiPort}/datasets/${mockLabelStudioProjectId}/data/url-generation/${mockFileName}`;
      const mockResponse = {url: 'http://mocked-presigned-url'};
      httpMock.get.and.returnValue(of(mockResponse));

      // Call the getTrackingPresignedUrl method
      adapter.getTrackingPresignedUrl(mockFileName).subscribe(response => {
        // Verify that the HttpClient.get was called with the correct URL
        expect(httpMock.get).toHaveBeenCalledWith(expectedUrl, {responseType: 'json'});
        // Verify that the response matches the expected response
        expect(response).toEqual(mockResponse);
      });
    });
    it('should call http.patch with the correct URL and data', () => {
      const mockPresignedUrl = 'http://mocked-presigned-url';
      const expectedLabelStudioProjectId = 42;

      // Set up datasetSubject with a value
      adapter['datasetSubject'].next({
        labelStudioProjectId: expectedLabelStudioProjectId,
        name: 'MyDataset',
        datasetType: DATASET_TYPES[0],
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      });

      // Set up httpMock.patch to return a response
      httpMock.patch.and.returnValue(of({}));

      // Call the editLabelConfig method
      adapter.editLabelConfig(mockPresignedUrl);

      // Verify that http.patch was called with the correct URL and data
      const expectedUrl = `http://localhost:5000/datasets/${expectedLabelStudioProjectId}`;
      const expectedData = {url: mockPresignedUrl};
      expect(httpMock.patch).toHaveBeenCalledWith(expectedUrl, expectedData);

      // Verify other expectations (e.g., error handling)
      expect(toasterMock.error).toHaveBeenCalledTimes(0);
    });

    it('should handle errors and call toasterService.error on HTTP patch error', () => {
      const mockPresignedUrl = 'http://mocked-presigned-url';
      const expectedLabelStudioProjectId = 42;

      // Set up datasetSubject with a value
      adapter['datasetSubject'].next({
        labelStudioProjectId: expectedLabelStudioProjectId,
        name: 'MyDataset',
        datasetType: DATASET_TYPES[0],
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      });

      // Set up httpMock.patch to throw an error
      httpMock.patch.and.returnValue(throwError({message: 'HTTP patch error'}));

      // Call the editLabelConfig method
      adapter.editLabelConfig(mockPresignedUrl);

      // Verify that http.patch was called with the correct URL and data
      const expectedUrl = `http://localhost:5000/datasets/${expectedLabelStudioProjectId}`;
      const expectedData = {url: mockPresignedUrl};
      expect(httpMock.patch).toHaveBeenCalledWith(expectedUrl, expectedData);

      // Verify that toasterService.error was called with the correct message
      const expectedErrorMessage = '(Error Message: HTTP patch error)';
      expect(toasterMock.error).toHaveBeenCalledWith(expectedErrorMessage, 'File Upload Error');
    });
  });

  /*
    describe('uploadFile', () => {
      it('should request a presigned URL and upload the file', () => {
        httpMock.post.and.returnValue(of({ url: 'http://this-is-a-mocked-presigned-url.local' }));
        httpMock.request.and.returnValue(of(<HttpEvent<any>>{
          type: HttpEventType.Response,
          body: {
            data: "some data from backend"
          }
        }));

        const mockFile = new File([''], 'test.png', { type: 'image/png' });


        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        })
        adapter.uploadFile(mockFile, false);

        const expectedUrl = `${environment.apiHost}:${environment.apiPort}` + `/datasets/42/data/generateurl`;
        expect(httpMock.post).toHaveBeenCalledOnceWith(expectedUrl, { 'file_name': mockFile.name });
        expect(httpMock.request).toHaveBeenCalledTimes(1);
        expect(toasterMock.success).toHaveBeenCalledTimes(0);
        expect(toasterMock.error).toHaveBeenCalledTimes(0);
      });

      it('should upload a JSON file successfully', () => {
        httpMock.post.and.returnValue(of({ url: 'http://this-is-a-mocked-presigned-url.local' }));
        httpMock.request.and.returnValue(of(<HttpEvent<any>>{
          type: HttpEventType.Response,
          body: {
            data: "some data from backend"
          }
        }));

        const mockFile = new File([''], '.json', { type: 'application/json' });


        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        })
        adapter.uploadFile(mockFile, false);

        const expectedUrl = `${environment.apiHost}:${environment.apiPort}` + `/datasets/42/preannotation/generateurl`;
        expect(httpMock.post).toHaveBeenCalledOnceWith(expectedUrl, { 'file_name': mockFile.name });
        expect(httpMock.request).toHaveBeenCalledTimes(1);
        expect(toasterMock.success).toHaveBeenCalledTimes(0);
        expect(toasterMock.error).toHaveBeenCalledTimes(0);
      });

      it('should upload a CSV file successfully', () => {
        httpMock.post.and.returnValue(of({ url: 'http://this-is-a-mocked-presigned-url.local' }));
        httpMock.request.and.returnValue(of(<HttpEvent<any>>{
          type: HttpEventType.Response,
          body: {
            data: 'some data from backend'
          }
        }));

        const mockFile = new File([''], '.csv', { type: 'text/csv' });

        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        });

        adapter.uploadFile(mockFile, false);

        const expectedUrl = `${environment.apiHost}:${environment.apiPort}` + `/datasets/42/preannotation/generateurl`;
        expect(httpMock.post).toHaveBeenCalledOnceWith(expectedUrl, { 'file_name': mockFile.name });
        expect(httpMock.request).toHaveBeenCalledTimes(1);
        expect(toasterMock.success).toHaveBeenCalledTimes(0);
        expect(toasterMock.error).toHaveBeenCalledTimes(0);
      });

      it('should show an error if uploading the file failed', () => {
        httpMock.post.and.returnValue(of({ url: 'http://this-is-a-mocked-presigned-url.local' }));
        httpMock.request.and.returnValue(throwError(() => new Error("Some mocked Error.")));

        const mockFile = new File([''], 'test.png', { type: 'image/png' });

        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        })
        adapter.uploadFile(mockFile, false);

        const expectedUrl = `${environment.apiHost}:${environment.apiPort}` + `/datasets/42/data/generateurl`;
        expect(httpMock.post).toHaveBeenCalledOnceWith(expectedUrl, { 'file_name': mockFile.name });
        expect(httpMock.request).toHaveBeenCalledTimes(1);
        expect(toasterMock.success).toHaveBeenCalledTimes(0);
        expect(toasterMock.error).toHaveBeenCalledOnceWith(`An error occurred while uploading the file '${mockFile.name}' to the server.
           Please try again. (Error Message: Some mocked Error.)`, 'File Upload Error');
      });

      it('should request a presigned URL and upload the file', () => {
        // Set up httpMock.post to return a response
        httpMock.post.and.returnValue(of({ url: 'http://this-is-a-mocked-presigned-url.local' }));

        // Set up httpMock.request to return a response
        httpMock.request.and.returnValue(of(<HttpEvent<any>>{
          type: HttpEventType.Response,
          body: {
            data: 'some data from backend'
          }
        }));

        const mockFile = new File([''], 'test.png', { type: 'image/png' });

        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        });

        adapter.uploadFile(mockFile, false);
        const expectedUrl = `${environment.apiHost}:${environment.apiPort}` + `/datasets/42/data/generateurl`;

        // Verify that httpMock.post was called with the expected arguments
        expect(httpMock.post).toHaveBeenCalledWith(expectedUrl, { 'file_name': mockFile.name });

        // Verify other expectations
        expect(httpMock.request).toHaveBeenCalledTimes(1);
        expect(toasterMock.success).toHaveBeenCalledTimes(0);
        expect(toasterMock.error).toHaveBeenCalledTimes(0);
      });

      it('should call http.patch with the correct URL and data', () => {
        const mockPresignedUrl = 'http://mocked-presigned-url';
        const expectedLabelStudioProjectId = 42;

        // Set up datasetSubject with a value
        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        });

        // Set up httpMock.patch to return a response
        httpMock.patch.and.returnValue(of({}));

        // Call the editLabelConfig method
        adapter.editLabelConfig(mockPresignedUrl);

        // Verify that http.patch was called with the correct URL and data
        const expectedUrl = `${environment.apiHost}:${environment.apiPort}/datasets/${expectedLabelStudioProjectId}`;
        const expectedData = { url: mockPresignedUrl };
        expect(httpMock.patch).toHaveBeenCalledWith(expectedUrl, expectedData);

        // Verify other expectations (e.g., error handling)
        expect(toasterMock.error).toHaveBeenCalledTimes(0);
      });

      it('should handle errors and call toasterService.error on HTTP patch error', () => {
        const mockPresignedUrl = 'http://mocked-presigned-url';
        const expectedLabelStudioProjectId = 42;

        // Set up datasetSubject with a value
        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        });

        // Set up httpMock.patch to throw an error
        httpMock.patch.and.returnValue(throwError({ message: 'HTTP patch error' }));

        // Call the editLabelConfig method
        adapter.editLabelConfig(mockPresignedUrl);

        // Verify that http.patch was called with the correct URL and data
        const expectedUrl = `${environment.apiHost}:${environment.apiPort}/datasets/${expectedLabelStudioProjectId}`;
        const expectedData = { url: mockPresignedUrl };
        expect(httpMock.patch).toHaveBeenCalledWith(expectedUrl, expectedData);

        // Verify that toasterService.error was called with the correct message
        const expectedErrorMessage = '(Error Message: HTTP patch error)';
        expect(toasterMock.error).toHaveBeenCalledWith(expectedErrorMessage, 'File Upload Error');
      });

      it('should call getTrackingPresignedUrl and editLabelConfig if tracking is true', () => {
        httpMock.post.and.returnValue(of({ url: 'http://this-is-a-mocked-presigned-url.local' }));
        httpMock.request.and.returnValue(of(<HttpEvent<any>>{
          type: HttpEventType.Response,
          body: {
            data: "some data from backend"
          }
        }));
        const expectedUrlPost = `${environment.apiHost}:${environment.apiPort}` + `/datasets/42/data/generateurl`;
        const mockFile = new File([''], 'test.png', { type: 'image/png' });
        const mockResponse = { url: 'http://mocked-presigned-url' };
        const mockLabelStudioProjectId = 42;

        const expectedUrlGet = `${environment.apiHost}:${environment.apiPort}/datasets/${mockLabelStudioProjectId}/data/generateurl/${mockFile.name}`;
        httpMock.get.and.returnValue(of(mockResponse));

        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        })

        // Call the getTrackingPresignedUrl method
        adapter.getTrackingPresignedUrl(mockFile.name).subscribe(response => {
          // Verify that the HttpClient.get was called with the correct URL
          expect(httpMock.get).toHaveBeenCalledWith(expectedUrlGet, { responseType: 'json' });

          // Verify that the response matches the expected response
          expect(response).toEqual(mockResponse);
        });

        const mockPresignedUrl = 'http://mocked-presigned-url';

        // Set up datasetSubject with a value
        datasetSubject.next(<Dataset>{
          labelStudioProjectId: 42
        });

        // Set up httpMock.patch to return a response
        httpMock.patch.and.returnValue(of({}));

        // Verify that http.patch was called with the correct URL and data
        const expectedUrlPatch = `${environment.apiHost}:${environment.apiPort}/datasets/${mockLabelStudioProjectId}`;
        const expectedData = { url: mockPresignedUrl };

        adapter.uploadFile(mockFile, true);

        expect(httpMock.post).toHaveBeenCalledOnceWith(expectedUrlPost, { file_name: mockFile.name });
        expect(httpMock.patch).toHaveBeenCalledWith(expectedUrlPatch, expectedData);
        expect(httpMock.request).toHaveBeenCalledTimes(1);
        expect(toasterMock.success).toHaveBeenCalledTimes(0);
        expect(toasterMock.error).toHaveBeenCalledTimes(0);
      });
    });*/
})
