/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  HttpRequest,
  HttpClient,
  HttpEvent
} from '@angular/common/http';
import {BehaviorSubject, catchError, lastValueFrom, map, Observable, of} from 'rxjs';

import {environment} from 'src/environments/environment';
import {Dataset} from 'src/app/core/models/dataset';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';

/**
 * A component that provides the functionality to upload files to the server.
 */
export class UploadFilePickerAdapter {
  private readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  /**
   * Constructs the `UploadFilePickerAdapter` with the required services.
   *
   * @param http A HTTP client to make requests to the server.
   * @param datasetSubject A behavior subject that holds the current dataset.
   * @param annotationFormatSubject A behavior subject that holds the current annotation format.
   * @param annotationUploadBlockedSubject A behavior subject that holds the current upload blocked status.
   * @param toasterService A service to display toasts.
   */
  constructor(
    private http: HttpClient,
    private datasetSubject: BehaviorSubject<Dataset>,
    private annotationFormatSubject: BehaviorSubject<string>,
    private annotationUploadBlockedSubject: BehaviorSubject<boolean>,
    private toasterService: ToasterService,
  ) {
  }

  /**
   * Get the request URL for the file upload.
   *
   * @param file The file that should be uploaded.
   */
  getRequestURL(file: File) {
    let body, apiRequestUrl;
    if (file.type == 'application/json') {
      [apiRequestUrl, body] = this.buildRequestUrl(true, true, 'preannotation', `${this.annotationFormatSubject.getValue()}.json`);
    } else if (file.type == 'text/csv') {
      [apiRequestUrl, body] = this.buildRequestUrl(true, true, 'preannotation', `${this.annotationFormatSubject.getValue()}.csv`);
    } else {
      [apiRequestUrl, body] = this.buildRequestUrl(true, this.annotationUploadBlockedSubject.getValue(), 'data', file.name);
    }
    //eslint-disable-next-line @typescript-eslint/no-explicit-any
    return lastValueFrom(this.http.post<any>(apiRequestUrl, body).pipe(map(res =>
      res['url']
    )))
  }

  /**
   * Gets the presigned URL for tracking a file.
   *
   * @param file_name The name of the file to track.
   */
  getTrackingPresignedUrl(file_name: string): Observable<any> {
    const apiUrl = `${this.API_URL}/datasets/${this.datasetSubject.getValue().labelStudioProjectId}/data/url-generation/${file_name}`;

    return this.http.get(apiUrl, {responseType: 'json'});
  }

  /**
   * Edits the label configuration of the dataset.
   *
   * @param presignedUrl The presigned URL that points to the new label config.
   */
  editLabelConfig(presignedUrl: string) {
    const body = {'url': presignedUrl};
    console.log(body);
    return this.http.patch(`${this.API_URL}/datasets/${this.datasetSubject.getValue().labelStudioProjectId}`, body).pipe(
      catchError(err => {
        this.toasterService.error(`(Error Message: ${err.message})`, 'File Upload Error');
        return of(<any>{status: 'ERROR', body: err});
      })
    ).subscribe();
  }

  /**
   * Builds the request URL for the file upload.
   *
   * @param upload If the file should be uploaded. If false, the file will be removed.
   * @param formatBlocked If the format is blocking.
   * @param s3Folder The S3 folder to upload to.
   * @param bodyContent The body content of the request.
   *
   * @returns Either a tuple with the request URL and the body if upload is set to true, otherwise will just return the request URL.
   */
  //eslint-disable-next-line @typescript-eslint/no-explicit-any
  public buildRequestUrl(upload: boolean, formatBlocked: boolean, s3Folder: string, bodyContent: string): [string, any | null] {
    this.annotationUploadBlockedSubject.next(formatBlocked);
    let apiRequestUrl;
    if (upload) {
      apiRequestUrl = this.API_URL + `/datasets/${this.datasetSubject.getValue().labelStudioProjectId}/${s3Folder}/url-generation`;
      const body = {'file_name': bodyContent};
      return [apiRequestUrl, body];
    } else {
      apiRequestUrl = this.API_URL + `/datasets/${this.datasetSubject.getValue().labelStudioProjectId}
      /${s3Folder}/${bodyContent}/file-removal`;
      return apiRequestUrl;
    }
  }

  /**
   * Uploads a file to S3.
   *
   * @param presignedUrl The presigned URL to upload the file to.
   * @param file The file to upload.
   */
  //eslint-disable-next-line @typescript-eslint/no-explicit-any
  public async uploadToS3(presignedUrl: string, file: File): Promise<HttpEvent<any>> {
    const s3Url = new URL(presignedUrl);
    const req = new HttpRequest('PUT', s3Url.href, file);

    return lastValueFrom(this.http.request(req))
  }

  // TODO: Unused code, should be removed?
  public removeFile(file: File) {
    let apiRequestUrl;
    if (file.type == 'application/json') {
      apiRequestUrl = this.buildRequestUrl(false, false, 'preannotation', `${this.annotationFormatSubject.getValue()}.json`);
    } else if (file.type == 'text/csv') {
      apiRequestUrl = this.buildRequestUrl(false, false, 'preannotation', `${this.annotationFormatSubject.getValue()}.csv`);
    } else {
      apiRequestUrl = this.buildRequestUrl(false, this.annotationUploadBlockedSubject.getValue(), 'data', file.name);
    }
    return this.http.delete(apiRequestUrl).pipe(
      //eslint-disable-next-line @typescript-eslint/no-explicit-any
      map((res: any) => {
        this.toasterService.success(`Removed file '${file.name}' from the server.`, 'Removed File');
        return of(res);
      }),
      catchError(err => {
        this.toasterService.error(`An error occurred while removing the file '${file.name}' from the server.
         Please try again. (Error Message: ${err.message})`, 'Remove File Error');
        return of(err);
      })
    );
  }
}
