/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Represents a training server. */
export interface TrainingServer {
  /** The unique id of the training server. */
  id: number;
  /** Whether the training server is currently selected. */
  selected: boolean;
  /** Whether the training server is currently being edited. */
  inEditing: boolean;
  /** The name of the training server. */
  name: string;
  /** The endpoint URL of the training server. */
  endpoint: string;
}
