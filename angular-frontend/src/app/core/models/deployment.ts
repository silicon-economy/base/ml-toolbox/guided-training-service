/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Represents an edge device. */
export interface EdgeDevice {
  /** The unique identifier of the edge device. */
  id: number;
  /** The name of the edge device. */
  name: string;
  /** The URI of the edge device. */
  uri: string;
  /** The date when the edge device was created. */
  creationDate: string;
  /** The type of the edge device. */
  deviceType: string;
  /** The URL of the stream associated with the device. */
  stream: string;
}
