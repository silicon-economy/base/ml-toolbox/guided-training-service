/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Represents a config used for training. */
export class TrainConfig {
  /** The amount of epochs the model should be trained for. */
  epochs: number;
  /** An array of datasets ids used to train the model. */
  trainLabelStudioProjectIds: number[];
  /** An array of datasets ids used to evaluate the model. */
  evalLabelStudioProjectIds: number[];
  /** The model the training is based on. */
  model: string;
  /** The unique name of the model. */
  modelName: string;
  /** The description of the model. */
  description: string;
  /** An array of classes the model should be trained for. */
  classes: string[];
  /** The batch size of the training. */
  batchSize: number;
  /** The model config that has been build based on the other parameters. This
   * is relevant receiving an intermediate configuration that can be used in
   * the advanced configuration step of the model training.
   */
  modelConfig: string;
  /** The endpoint URL the model should be trained on. */
  endpoint: string;

  constructor(
    epochs: number,
    trainLabelStudioProjectIds: number[],
    evalLabelStudioProjectIds: number[],
    model: string,
    modelName: string,
    description: string,
    classes: string[],
    batchSize: number,
    modelConfig: string,
    endpoint: string,
    ) {
    this.epochs = epochs;
    this.trainLabelStudioProjectIds = trainLabelStudioProjectIds;
    this.evalLabelStudioProjectIds = evalLabelStudioProjectIds;
    this.model = model;
    this.modelName = modelName;
    this.description = description;
    this.classes = classes;
    this.batchSize = batchSize;
    this.modelConfig = modelConfig;
    this.endpoint = endpoint;
  }
}
