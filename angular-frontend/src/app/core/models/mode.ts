/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Represents available color themes. */
export type Mode =
  { name: 'dark-theme', icon: 'dark_mode' } |
  { name: 'light-theme', icon: 'light_mode' };

/** Represents the name of a mode. */
export type ModeName = 'dark-theme' | 'light-theme';

/** Represents the material symbol icon of a mode. */
export type ModeIcon = 'dark_mode' | 'light_mode';
