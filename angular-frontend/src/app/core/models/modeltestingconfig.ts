/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface ModelTestingConfig {
  /** The name of the model. */
  model_name: string;
  /** The name of the second model. */
  second_model_name: string;
  /** The Labelstudio project id. */
  labelStudioProjectId: number;
  /** The endpoint URL where the model should be tested on. */
  endpoint: string;
}
