/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Pre-defined list of valid colors for tags. */
export type TagColor =
  'red-chip'
  | 'orange-chip'
  | 'yellow-chip'
  | 'neon-yellow-chip'
  | 'bright-green-chip'
  | 'green-chip'
  | 'blue-chip'
  | 'azure-blue-chip';

/** Represents a single tag on a dataset. */
export interface DatasetTag {
  /** The name of the tag. */
  name: string;
  /** The color of the tag. */
  colorClass: TagColor;
}

/** Represents a type of dataset. */
export interface DatasetType {
  /** The internal name of the type. */
  value: string;
  /** The display value of the type. */
  viewValue: string;
}

/** Represents a single Dataset. */
export interface Dataset {
  /** The project id in Labelstudio. */
  labelStudioProjectId: number;
  /** The name of the dataset. */
  name: string;
  /** The [DatasetType] of the dataset. */
  datasetType: DatasetType;
  /** The amount of images that have been annotated. */
  annotatedImages: number;
  /** The total amount of images in the dataset. */
  totalImages: number;
  /** The last time the dataset has been edited. */
  lastModified: Date;
  /** An array of URLs to preview images of the dataset. */
  previewImages: string[];
  /** The tags of the dataset. */
  tags: DatasetTag[];
  /** The tracked classes of the dataset. */
  classes: string[];
  /** The URL to the dataset on Labelstudio. */
  labelStudioUrl: string;
}
