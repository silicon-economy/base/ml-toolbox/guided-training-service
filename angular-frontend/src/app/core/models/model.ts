/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Pre-defined list of valid colors for tags. */
export type TagColor =
  'red-chip'
  | 'orange-chip'
  | 'yellow-chip'
  | 'neon-yellow-chip'
  | 'bright-green-chip'
  | 'green-chip'
  | 'blue-chip'
  | 'azure-blue-chip';

/** Represents a label tag. */
export interface LabelsTag {
  /** The name of the label. */
  name: string;
  /** The color of the label. */
  colorClass: TagColor;
}

/** Represents a single model. */
export interface Model {
  /** The Mlflow run id of the model. */
  run_id: string;
  /** The name of the model. */
  name: string;
  /** The description of the model. */
  description: string;
  /** An array of datasets ids used to train the model. */
  dataset: string[];
  /** The model the training is based on. */
  model: string;
  /** An array of datasets ids used to evaluate the model. */
  evaluation_datasets: string[];
  /** The amount of epochs the model was trained for. */
  epochs: number;
  /** The batch size used for training of the model. */
  batch_size: number;
  //TODO: Find out if this is used?
  annotation_task: LabelsTag[];
  /**
   *  The current status of the training.
   *
   *  For example could be set to  'running', 'waiting', 'done', 'error'
   */
  training_state: string;
  // TODO: Change to score?!
  /** The overall accuracy on the evaluated datasets. */
  accuracy: number;
  /** The algorithm used to train the model. */
  algorithm_type: string;
  /** The date the model was created at. */
  creation_date: Date;
  /** The Mlflow URL to the model. */
  mlflow_url: string;
}
