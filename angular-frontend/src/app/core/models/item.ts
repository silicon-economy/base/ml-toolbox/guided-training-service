/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Represents a single item in a menu*/
export interface MenuItem {
  /** The string that should be displayed in the menu. */
  displayValue: string;
  /** The string that is sent in the backend. */
  backendValue?: string;
  /** An array of potential children that are shown below the current item. */
  children?: MenuItem[];
}
