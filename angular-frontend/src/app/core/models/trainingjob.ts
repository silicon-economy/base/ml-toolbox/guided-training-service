/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Represents a response to a training job start request. */
export interface TrainingJobSubmitted {
  /** The Mlflow id of the new training job. */
  id: string;
  /** The message regarding the success of the training start */
  message: string;
}
