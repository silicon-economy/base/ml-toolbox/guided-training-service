/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Subscription} from 'rxjs';

/**
 * A component that displays a collection of images with a responsive layout.
 * The number of images displayed in a row changes based on the screen size.
 *
 * This component listens to breakpoint changes and adjusts the number of images
 * shown according to the current screen width (XS, SM, or default).
 */
@Component({
  selector: 'app-responsive-image-collection',
  templateUrl: './responsive-image-collection.component.html',
  styleUrls: ['./responsive-image-collection.component.scss']
})
export class ResponsiveImageCollectionComponent implements OnInit, OnDestroy {

  @Input() imageUrls: string[] = [];
  @Input() imageSize = 32;
  @Input() altText: string;

  numCollectionImages = 5;
  private breakpointStateXS = false;
  private breakpointStateSM = false;

  // store all open subscriptions
  private readonly subscriptions = new Subscription();

  /**
   * Constructs the `ResponsiveImageCollectionComponent` with the required `BreakpointObserver`.
   *
   * @param breakpointObserver The service used to observe layout breakpoints.
   */
  constructor(private readonly breakpointObserver: BreakpointObserver) {
  }

  /**
   * Initializes the component by setting up subscriptions to breakpoint changes.
   * Adjusts the number of images displayed when the layout changes.
   */
  ngOnInit(): void {
    this.subscriptions.add(
      this.breakpointObserver.observe([
        Breakpoints.Medium
      ]).subscribe(result => {
        this.breakpointStateSM = result.matches;
        this.rearrangeImageCollection();
      }));

    this.subscriptions.add(
      this.breakpointObserver.observe([
        Breakpoints.XSmall, Breakpoints.Small
      ]).subscribe(result => {
        this.breakpointStateXS = result.matches;
        this.rearrangeImageCollection();
      }));
  }

  /**
   * Cleans up all subscriptions when the component is destroyed.
   */
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  /**
   * Rearranges the collection of images based on the current screen size.
   * Adjusts the `numCollectionImages` value to display a different number of images
   * depending on the screen width (1 for XS, 2 for SM, 5 for others).
   */
  rearrangeImageCollection() {
    if (this.breakpointStateXS) {
      this.numCollectionImages = 1;
    } else if (this.breakpointStateSM) {
      this.numCollectionImages = 2;
    } else {
      this.numCollectionImages = 5;
    }
  }
}
