/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {
  ResponsiveImageCollectionComponent
} from 'src/app/core/components/responsive-image-collection/responsive-image-collection.component';

describe('ResponsiveImageCollectionComponent', () => {
  let component: ResponsiveImageCollectionComponent;
  let fixture: ComponentFixture<ResponsiveImageCollectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResponsiveImageCollectionComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsiveImageCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set numCollectionImages to 1 when breakpointStateXS is true', () => {
    component['breakpointStateXS'] = true;
    component.rearrangeImageCollection();
    expect(component.numCollectionImages).toBe(1);
  });

  it('should set numCollectionImages to 2 when breakpointStateSM is true', () => {
    component['breakpointStateXS'] = false;
    component['breakpointStateSM'] = true;
    component.rearrangeImageCollection();
    expect(component.numCollectionImages).toBe(2);
  });

  it('should set numCollectionImages to 5 when neither breakpointStateXS nor breakpointStateSM are true', () => {
    component['breakpointStateXS'] = false;
    component['breakpointStateSM'] = false;
    component.rearrangeImageCollection();
    expect(component.numCollectionImages).toBe(5);
  });

  it('should unsubscribe from all subscriptions', () => {
    // Prepare
    const mockSubscription = jasmine.createSpyObj('Subscription', ['unsubscribe']);
    component['subscriptions'].add(mockSubscription);
    // Act
    component.ngOnDestroy();
    // Verify
    expect(mockSubscription.unsubscribe).toHaveBeenCalled();
  });
});
