/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map, Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {TrainingServer} from 'src/app/core/models/trainingserver';

/**
 * Service for managing training servers.
 * Provides methods to interact with the backend to fetch, delete, and add training servers.
 * This service is used by components to perform CRUD operations on training servers.
 */
@Injectable({
  providedIn: 'root'
})
export class TrainingServerService {

  /**
   * API base URL, constructed using the environment configuration.
   */
  private readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  /**
   * Constructs the TrainingServerService.
   *
   * @param http HttpClient used to make HTTP requests to the backend.
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Fetches all training servers from the backend.
   *
   * @returns An observable of an array of training servers.
   */
  getAllTrainingsServers(): Observable<TrainingServer[]> {
    return this.http.get<TrainingServer[]>(`${this.API_URL}/training-servers/`)
      .pipe(map((server: TrainingServer[]) => {
        return server;
      }));
  }

  /**
   * Deletes a specific training server from the backend.
   *
   * @param server The training server to delete.
   *
   * @returns An observable representing the HTTP delete request.
   */
  deleteTrainingServer(server: TrainingServer) {
    return this.http.delete<TrainingServer[]>(`${this.API_URL}/training-servers/${server.id}`);
  }

  /**
   * Adds a new training server to the backend.
   *
   * @param server The training server to add.
   *
   * @returns An observable representing the HTTP post request to add the server.
   */
  addTrainingServer(server: TrainingServer) {
    server.inEditing = false;
    const body = {
      'name': server.name,
      'endpoint': server.endpoint,
    };

    return this.http.post(`${this.API_URL}/training-servers/`, body);
  }

}
