/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ModelTestingService } from 'src/app/core/backend/model-testing.service';
import { environment } from 'src/environments/environment';

describe('ModelTestingService', () => {
  let service: ModelTestingService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ModelTestingService]
    });

    service = TestBed.inject(ModelTestingService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('startModelTesting', () => {
    it('should send a GET request to the correct URL', () => {
      const lsProjectId = 123;
      const modelName = 'test_model';
      const secondModelName = "second test model";
      const serverEndpoint = "http://dummy-endpoint"
      const expectedUrl = `${environment.apiHost}:${environment.apiPort}/model_testing/`;

      service.startModelTesting(lsProjectId, modelName, secondModelName, serverEndpoint).subscribe();

      const req = httpTestingController.expectOne(expectedUrl);
      expect(req.request.method).toEqual('POST');
      httpTestingController.verify();
    });

    it('should return data from the GET request', () => {
      const lsProjectId = 123;
      const modelName = 'test_model';
      const secondModelName = "second test model";
      const serverEndpoint = "http:/dummy-endpoint"
      const testData = { result: 'success' };

      service.startModelTesting(lsProjectId, modelName, secondModelName, serverEndpoint).subscribe(data => {
        expect(data).toEqual(testData);
      });

      const req = httpTestingController.expectOne(
        `${environment.apiHost}:${environment.apiPort}/model_testing/`
      );

      req.flush(testData);
      httpTestingController.verify();
    });
  });
});
