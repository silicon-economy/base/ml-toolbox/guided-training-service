/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map, Observable, of} from 'rxjs';
import {environment} from 'src/environments/environment';
import {Model, LabelsTag} from 'src/app/core/models/model';
import {TrainConfig} from 'src/app/core/models/trainconfig';
import {TrainingJobSubmitted} from 'src/app/core/models/trainingjob';

/**
 * A constant list of predefined label tags with associated color classes.
 * These tags can be used for categorizing or tagging models or datasets.
 */
const LABEL_TAGS: LabelsTag[] = [
  {
    name: 'EPAL',
    colorClass: 'bright-green-chip'
  },
  {
    name: 'Gitterbox',
    colorClass: 'yellow-chip'
  },
  {
    name: 'KLT',
    colorClass: 'orange-chip'
  },
  {
    name: 'Überstand',
    colorClass: 'red-chip'
  }
];

/**
 * A constant list of model architectures with their corresponding display names
 * and backend values for different machine learning tasks.
 */
export const ARCHITECTURES = [
  {
    displayValue: 'Classification',
    backendValue: 'classification',
    children: [
      {
        displayValue: 'EfficientNet',
        children: [
          {
            displayValue: 'EfficientNet - B0',
            backendValue: 'mmpretrain__efficientnet-b0_3rdparty_8xb32_in1k_20220119-a7e2a0b1'
          },
          {
            displayValue: 'EfficientNet - B4',
            backendValue: 'mmpretrain__efficientnet-b4_3rdparty-ra-noisystudent_in1k_20221103-16ba8a2d'
          },
          {
            displayValue: 'EfficientNet - B8',
            backendValue: 'mmpretrain__efficientnet-b8_3rdparty_8xb32-aa-advprop_in1k_20220119-297ce1b7'
          }
        ]
      }
    ]
  },
  {
    displayValue: 'Object Detection',
    backendValue: 'object_detection',
    children: [
      {
        displayValue: 'YOLOX',
        children: [
          {
            displayValue: 'YOLOX - Nano (416x416)',
            backendValue: 'yolox__nano'
          },
          {
            displayValue: 'YOLOX - Tiny (416x416)',
            backendValue: 'yolox__tiny'
          },
          {
            displayValue: 'YOLOX - S (640x640)',
            backendValue: 'yolox__s'
          },
          {
            displayValue: 'YOLOX - M (640x640)',
            backendValue: 'yolox__m'
          },
          {
            displayValue: 'YOLOX - L (640x640)',
            backendValue: 'yolox__l'
          },
          {
            displayValue: 'YOLOX - X (640x640)',
            backendValue: 'yolox__x'
          },
          {
            displayValue: 'YOLOX - Darknet (640x640)',
            backendValue: 'yolox__darknet'
          }
        ]
      },
      {
        displayValue: 'mmdetection',
        children: [
          {
            displayValue: 'YOLOV3 - Darknet53 (640x640)',
            backendValue: 'mmdetection_object_detection__yolov3_d53_mstrain-608_273e_coco'
          },
          {
            displayValue: 'HTC - X-101-64x4d-FPN',
            backendValue: 'mmdetection_object_detection__htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco'
          },
          {
            displayValue: 'RTMDet - Tiny',
            backendValue: 'mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc'
          },
          {
            displayValue: 'RTMDet - S',
            backendValue: 'mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e'
          },
          {
            displayValue: 'RTMDet - M',
            backendValue: 'mmdetection_object_detection__rtmdet_m_8xb32-300e_coco_20220719_112220-229f527c'
          },
          {
            displayValue: 'RTMDet - L',
            backendValue: 'mmdetection_object_detection__rtmdet_l_8xb32-300e_coco_20220719_112030-5a0be7c4'
          },
          {
            displayValue: 'RTMDet - X',
            backendValue: 'mmdetection_object_detection__rtmdet_x_8xb32-300e_coco_20220715_230555-cc79b9ae'
          }
        ]
      }
    ]
  },
  {
    displayValue: 'Rotated Object Detection',
    backendValue: 'rotated_object_detection',
    children: [
      {
        displayValue: 'Rotated RTMDet',
        children: [
          {
            displayValue: 'Rotated RTMDet - Tiny',
            backendValue: 'mmrotate__rotated_rtmdet_tiny-3x-dota_ms-f12286ff'
          },
          {
            displayValue: 'Rotated RTMDet - L',
            backendValue: 'mmrotate__rotated_rtmdet_l-coco_pretrain-3x-dota_ms-06d248a2'
          }
        ]
      }
    ]
  },
  {
    displayValue: 'Instance Segmentation',
    backendValue: 'instance_segmentation',
    children: [
      {
        displayValue: 'mmdetection',
        children: [
          {
            displayValue: 'YOLACT - Resnet50-FPN',
            backendValue: 'mmdetection_segmentation__yolact_r50_8x8_coco_20200908-ca34f5db'
          },
          {
            displayValue: 'YOLACT - Resnet101-FPN',
            backendValue: 'mmdetection_segmentation__yolact_r101_1x8_coco_20200908-4cbe9101'
          },
        ]
      }
    ]
  },
  {
    displayValue: 'Text Recognition',
    backendValue: 'text_recognition',
    children: [
      {
        displayValue: 'ABINet',
        children: [
          {
            displayValue: 'ABINet',
            backendValue: 'mmocr_text_recognition__abinet_20e_st-an_mj_20221005_012617-ead8c139'
          }
        ]
      }
    ]
  }
];

/**
 * Represents the response for a models request, including the total count and the list of models.
 */
export class ModelsResponse {
  totalModels: number;
  models: Model[];
}

/**
 * Service for managing models, including fetching, sorting, deleting, and starting training jobs.
 */
@Injectable({
  providedIn: 'root'
})
export class ModelManagementService {

  /**
   * API base URL, constructed using the environment configuration.
   */
  private readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  /**
   * Configuration for the training process, set when starting training
   */
  config: TrainConfig;

  /**
   * Creates an instance of the service.
   *
   * @param http The HttpClient used to make HTTP requests to the backend.
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Fetches a list of models, applying optional filters, sorting, and pagination.
   *
   * @param filter The filter criteria for fetching models (default is an empty string).
   * @param sortName The field by which to sort the models (default is 'creation_date').
   * @param sortOrder The sorting order, either 'asc' or 'desc' (default is 'desc').
   * @param pageNumber The page number (0-indexed, default is 0).
   * @param pageSize The number of items per page (default is 10).
   *
   * @returns An observable containing the response with models and the total number of models.
   */
  getModels(
    filter = '',
    sortName = 'creation_date', sortOrder = 'desc',
    pageNumber = 0, pageSize = 10): Observable<ModelsResponse> {

    return this.http.get(this.API_URL + '/models/', {
      params: new HttpParams()
        .set('filter', filter)
        .set('order_by', sortName)
        .set('direction', sortOrder)
        .set('page', pageNumber + 1) // page is specified as 0 indexed value in the backend
        .set('per_page', pageSize)
    }).pipe(
      map(res => <ModelsResponse>{
        totalModels: res['total_items'],
        models: res['models']
      })
    );
  }

  /**
   * Sorts the list of models first by `algorithm_type` and then by `name`.
   *
   * @param models The array of models to be sorted.
   *
   * @returns The sorted array of models.
   */
  sortModelList(models: Model[]) {
    models.sort((a: Model, b: Model) => {
      if (a.algorithm_type > b.algorithm_type) {
        return 1;
      } else if (a.algorithm_type < b.algorithm_type) {
        return -1;
      }

      if (a.name > b.name) {
        return 1;
      } else if (a.name < b.name) {
        return -1;
      }

      return 0;
    });
    return models
  }

  /**
   * Fetches a single model by its name.
   *
   * @param name The name of the model to retrieve.
   *
   * @returns An observable containing the model data.
   */
  getModel(name: string): Observable<ModelsResponse> {
    return this.http.get(`${this.API_URL}/models/${name}`) as Observable<ModelsResponse>;
  }

  /**
   * Deletes a model by its name.
   *
   * @param model The model object to delete.
   *
   * @returns An observable representing the delete action result.
   */

  /* istanbul ignore next */
  deleteModel(model: Model): Observable<object> {
    return this.http.delete(`${this.API_URL}/models/${model.name}`);
  }

  /**
   * Downloads the artifacts associated with a model.
   *
   * @param model The model whose artifacts are to be downloaded.
   *
   * @returns An observable containing the URL to download the artifacts.
   */
  downloadModelArtifacts(model: Model): Observable<string> {
    return this.http.get(`${this.API_URL}/models/${model.name}/download`).pipe(
      map(res => res['url'])
    );
  }

  /**
   * Retrieves all available label tags.
   *
   * @returns An observable containing the list of label tags.
   */
  getAllLabelTags(): Observable<LabelsTag[]> {
    return of(LABEL_TAGS);
  }

  /**
   * Starts a training job for a model with the specified parameters.
   *
   * @param epochs The number of epochs for training.
   * @param trainLabelStudioProjectIds The list of project IDs used for training.
   * @param evalLabelStudioProjectIds The list of project IDs used for evaluation.
   * @param model The baseline model to use for training.
   * @param modelName A unique name for the training job.
   * @param description A description of the training job.
   * @param classes The list of classes for training.
   * @param batchSize The batch size for training.
   * @param modelConfig The model config that can be edited in the advanced mode.
   * @param trainingEndpoint The endpoint used to start training.
   * @param backendUrl The backend URL to post the training request to.
   *
   * @returns An observable containing the response with the training job ID and message.
   */
  startTraining(
    epochs: number,
    trainLabelStudioProjectIds: number[],
    evalLabelStudioProjectIds: number[],
    model: string,
    modelName: string,
    description: string,
    classes: string[],
    batchSize: number,
    modelConfig: string,
    trainingEndpoint: string,
    backendUrl: string
  ): Observable<TrainingJobSubmitted> {
    this.config = new TrainConfig(
      epochs,
      trainLabelStudioProjectIds,
      evalLabelStudioProjectIds,
      model,
      modelName,
      description,
      classes,
      batchSize,
      modelConfig,
      trainingEndpoint,
    );
    console.log('Starting training with given parameters: ' + JSON.stringify(this.config));
    return this.http.post<TrainConfig>(backendUrl, this.config).pipe(
      map(res => <TrainingJobSubmitted>{
        id: res['id'],
        message: res['message'],
      })
    );
  }

  /**
   * Fetches the metrics for a model's run, sorting the metrics by their keys.
   * If no metrics are available, an empty array is returned.
   *
   * Example response structure (if metrics are available):
   * ```json
   * {
   *   "0_loading_unit": {
   *     "TP": 60,
   *     "FP": 10,
   *     "FN": 0,
   *     "COUNT": 60,
   *     "RC": 1,
   *     "PR": 0.857142857142857,
   *     "F1": 0.923076923076923,
   *     "AP": 0.999726775956284
   *   },
   *   "1_pallet": {
   *     "TP": 60,
   *     "FP": 10,
   *     "FN": 0,
   *     "COUNT": 60,
   *     "RC": 1,
   *     "PR": 0.857142857142857,
   *     "F1": 0.923076923076923,
   *     "AP": 0.999726775956284
   *   }
   * }
   * ```
   *
   * @param model The model for which to fetch metrics.
   * @returns An observable containing the sorted metrics object or an empty array if no metrics are available.
   */

  getMetrics(model: Model): Observable<any> {
    return this.http.get<any>(`${this.API_URL}/models/${model.name}/metrics`).pipe(
      map(metrics => {
        if (!metrics) {
          return [];
        }

        const sortedKeys = Object.keys(metrics).sort();
        const sortedMetrics = {};
        sortedKeys.forEach(key => {
          sortedMetrics[key] = metrics[key];
        });

        return sortedMetrics;
      })
    );
  }

  /**
   * Fetches the false positive and false negative images for a specific model.
   *
   * @param modelName The name of the model for which to fetch the images.
   *
   * @returns An observable containing an array of false positive and false negative images.
   */
  getFnFpImages(modelName: string): Observable<any> {
    return this.http.get<any[]>(`${this.API_URL}/models/${modelName}/fp-fn-images`)
  }

  // retrieves a model config with the specified parameters for the advanced mode
  retrieveModelConfig(
    model: string,
    modelName: string,
    trainLabelStudioProjectIds: number[],
    evalLabelStudioProjectIds: number[],
    epochs: number,
    batchSize: number,
    classes: string[],
    description: string
  ): Observable<any> {
    const params = new HttpParams()
      .set('model', model)
      .set('modelName', modelName)
      .set('trainLabelStudioProjectIds', trainLabelStudioProjectIds.join(','))
      .set('evalLabelStudioProjectIds', evalLabelStudioProjectIds.join(','))
      .set('epochs', epochs.toString())
      .set('batchSize', batchSize.toString())
      .set('classes', classes.join(','))
      .set('description', description);

    return this.http.get<any>(this.API_URL + '/training/config', { params });
  }
}
