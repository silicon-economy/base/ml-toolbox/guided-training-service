/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {Dataset} from 'src/app/core/models/dataset';
import {DataManagementService, DatasetResponse} from 'src/app/core/backend/data-management.service';

/**
 * A data source for managing datasets in the dataset table.
 *
 * This class handles the loading, fetching, and error handling of dataset data
 * for display in a table.
 */
export class DatasetDataSource implements DataSource<Dataset> {

  private datasetSubject = new BehaviorSubject<Dataset[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public totalDatasets = new BehaviorSubject<number>(0);

  /**
   * Creates an instance of DatasetDataSource.
   *
   * @param dataManagementService The service used to manage dataset data.
   */
  constructor(private dataManagementService: DataManagementService) {
  }

  /**
   * Loads the datasets with the provided filters and pagination.
   *
   * This method triggers the fetching of datasets from the server and updates
   * the dataset list and total count. It handles loading state and error
   * handling as well.
   *
   * @param filter The search filter to apply to the datasets.
   * @param sortName The name of the field to sort the datasets by.
   * @param sortDirection The direction of the sort (ascending or descending).
   * @param pageIndex The current page index.
   * @param pageSize The number of items per page.
   */
  loadDatasets(
    filter: string,
    sortName: string,
    sortDirection: string,
    pageIndex: number,
    pageSize: number) {

    this.loadingSubject.next(true);

    this.dataManagementService.getDatasets(filter, sortName, sortDirection,
      pageIndex, pageSize)
      .pipe(
        catchError((err) => {
          console.error(err);
          return of(<DatasetResponse>{totalDatasets: 0, datasets: <Dataset[]>[]});
        }),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(datasets => {
        this.totalDatasets.next(datasets.totalDatasets);
        this.datasetSubject.next(datasets.datasets);
      });
  }

  /**
   * Connects the data source to the collection viewer, providing the dataset data.
   *
   * @param collectionViewer The collection viewer to connect the data source to.
   *
   * @returns An observable of the dataset data.
   */
  connect(collectionViewer: CollectionViewer): Observable<Dataset[]> {
    return this.datasetSubject.asObservable();
  }

  /**
   * Disconnects the data source from the collection viewer, completing the subject.
   *
   * @param collectionViewer The collection viewer to disconnect from.
   */
  disconnect(collectionViewer: CollectionViewer): void {
    this.datasetSubject.complete();
    this.loadingSubject.complete();
  }
}
