/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {Model} from 'src/app/core/models/model';
import {ModelManagementService, ModelsResponse} from 'src/app/core/backend/model-management.service';

/**
 * DataSource for displaying models in a paginated and sortable table.
 * It integrates with the `ModelManagementService` to retrieve models from the backend and handles pagination,
 * sorting, and loading states.
 */
export class ModelsDataSource implements DataSource<Model> {

  private modelsSubject = new BehaviorSubject<Model[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public totalModels = new BehaviorSubject<number>(0);

  /**
   * Constructs the ModelsDataSource.
   *
   * @param modelManagementService Service to fetch model data from the backend.
   */
  constructor(private modelManagementService: ModelManagementService) {
  }

  /**
   * Loads models with the specified filter, sorting, and pagination parameters.
   * The data is fetched from the backend, and the models are updated accordingly.
   *
   * @param filter The search filter applied to the models.
   * @param sortName The name of the model property to sort by.
   * @param sortDirection The direction of sorting, either 'asc' or 'desc'.
   * @param pageIndex The current page index (zero-based).
   * @param pageSize The number of models to fetch per page.
   */
  loadModels(
    filter: string,
    sortName: string,
    sortDirection: string,
    pageIndex: number,
    pageSize: number) {

    this.loadingSubject.next(true);

    this.modelManagementService.getModels(filter, sortName, sortDirection,
      pageIndex, pageSize).pipe(
      catchError(() => of(<ModelsResponse>{totalModels: 0, models: <Model[]>[]})),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(models => {
        this.totalModels.next(models.totalModels);
        this.modelsSubject.next(models.models);
      });
  }

  /**
   * Connects the data source to the collection viewer (e.g., Angular Material table).
   * This will return the observable stream of model data.
   *
   * @param collectionViewer The collection viewer to which the data is bound.
   *
   * @returns Observable stream of the model data.
   */
  connect(collectionViewer: CollectionViewer): Observable<Model[]> {
    return this.modelsSubject.asObservable();
  }

  /**
   * Disconnects the data source from the collection viewer.
   * This will complete the data and loading streams.
   *
   * @param collectionViewer The collection viewer to which the data was bound.
   */
  disconnect(collectionViewer: CollectionViewer): void {
    this.modelsSubject.complete();
    this.loadingSubject.complete();
  }

}

