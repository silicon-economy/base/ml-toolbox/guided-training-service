/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {TrainingServer} from "src/app/core/models/trainingserver";
import {BehaviorSubject, catchError, finalize, Observable, of} from "rxjs";
import {TrainingServerService} from "src/app/core/backend/training-server.service";

/**
 * DataSource for displaying training servers in a paginated and sortable table.
 * It integrates with the `TrainingServerService` to retrieve training server data from the backend
 * and handles loading, pagination, and error states.
 */
export class ServerDataSource implements DataSource<TrainingServer> {

  private serversSubject = new BehaviorSubject<TrainingServer[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  public totalServers = new BehaviorSubject<number>(0);

  /**
   * Constructs the ServerDataSource.
   *
   * @param trainingServerService Service to fetch training server data from the backend.
   */
  constructor(private trainingServerService: TrainingServerService) {
  }

  /**
   * Loads training servers from the backend and updates the subject streams.
   * It updates the servers list and total server count, and handles loading and error states.
   */
  loadServers() {
    this.loadingSubject.next(true);
    this.trainingServerService.getAllTrainingsServers().pipe(
      catchError((err) => {
        console.error(err)
        return of(<TrainingServer[]>[])
      }),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(servers => {
        if (servers === undefined) {
          this.totalServers.next(0)
          this.serversSubject.next([])
        } else {
          this.totalServers.next(servers.length);
          this.serversSubject.next(servers);
        }

      }
    )
  }

  /**
   * Connects the data source to the collection viewer (e.g., Angular Material table).
   * This will return the observable stream of training server data.
   *
   * @param collectionViewer The collection viewer to which the data is bound.
   *
   * @returns Observable stream of the training server data.
   */
  connect(collectionViewer: CollectionViewer): Observable<TrainingServer[]> {
    return this.serversSubject.asObservable();
  }

  /**
   * Disconnects the data source from the collection viewer.
   * This will complete the data and loading streams.
   *
   * @param collectionViewer The collection viewer to which the data was bound.
   */
  disconnect(collectionViewer: CollectionViewer): void {
    this.serversSubject.complete()
    this.loadingSubject.complete()
  }

}
