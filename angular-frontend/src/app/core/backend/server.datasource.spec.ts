/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ServerDataSource} from 'src/app/core/backend/server.datasource';
import {TrainingServer} from 'src/app/core/models/trainingserver';
import {BehaviorSubject, of, throwError} from 'rxjs';
import {TrainingServerService} from 'src/app/core/backend/training-server.service';

describe('ServerDataSource', () => {
  let dataSource: ServerDataSource;
  let mockTrainingServerService: jasmine.SpyObj<TrainingServerService>;
  let serversSubject: BehaviorSubject<TrainingServer[]>;

  beforeEach(() => {
    mockTrainingServerService = jasmine.createSpyObj('TrainingServerService', ['getAllTrainingsServers']);
    serversSubject = new BehaviorSubject<TrainingServer[]>([]);
    dataSource = new ServerDataSource(mockTrainingServerService);
    dataSource['serversSubject'] = serversSubject;
  });

  it('should be created', () => {
    expect(dataSource).toBeTruthy();
  });

  it('should load servers successfully', () => {
    const mockServers: TrainingServer[] = [
      {id: 1, name: 'Server 1', endpoint: 'test.de', selected: false, inEditing: false},
      {id: 2, name: 'Server 2', endpoint: 'test.de', selected: false, inEditing: false}
    ];
    mockTrainingServerService.getAllTrainingsServers.and.returnValue(of(mockServers));

    dataSource.loadServers();

    expect(dataSource['loadingSubject'].value).toBe(false);
    expect(mockTrainingServerService.getAllTrainingsServers).toHaveBeenCalled();

    serversSubject.next(mockServers); // Simulate servers being loaded

    expect(dataSource['serversSubject'].value).toEqual(mockServers);
    expect(dataSource.totalServers.value).toBe(mockServers.length);
    expect(dataSource['loadingSubject'].value).toBe(false);
  });

  it('should handle error while loading servers', () => {
    const errorMessage = 'Server error';
    mockTrainingServerService.getAllTrainingsServers.and.returnValue(throwError(errorMessage));

    dataSource.loadServers();

    expect(dataSource['loadingSubject'].value).toBe(false);
    expect(mockTrainingServerService.getAllTrainingsServers).toHaveBeenCalled();

    expect(dataSource['serversSubject'].value).toEqual([]);
    expect(dataSource.totalServers.value).toBe(0);
    expect(dataSource['loadingSubject'].value).toBe(false);
  });
});
