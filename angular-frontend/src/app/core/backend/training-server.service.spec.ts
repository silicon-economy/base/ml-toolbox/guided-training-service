/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {TrainingServerService} from 'src/app/core/backend/training-server.service';
import {TrainingServer} from 'src/app/core/models/trainingserver';
import {environment} from 'src/environments/environment';

const DUMMY_SERVER: TrainingServer[] = [
  {
    id: 1,
    name: 'server1',
    selected: false,
    endpoint: 'server1:8080',
    inEditing: false
  },
];

describe('TrainingServerService', () => {
  let service: TrainingServerService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TrainingServerService]
    });
    service = TestBed.inject(TrainingServerService);
    http = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    // Verify that there are no outstanding HTTP requests
    http.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load servers successfully', fakeAsync(() => {
    // Mock servers to be returned by the service
    const mockServers: TrainingServer[] = DUMMY_SERVER;

    // Subscribe to the getAllTrainingsServers() method
    service.getAllTrainingsServers().subscribe(servers => {
      // Ensure that the returned servers match the mocked servers
      expect(servers).toEqual(mockServers);
    });

    // Expect an HTTP GET request to the correct API endpoint
    const req = http.expectOne(`${environment.apiHost}:${environment.apiPort}/training-servers/`);
    // Ensure that the request method is GET
    expect(req.request.method).toBe('GET');
    // Flush the response with the mocked servers
    req.flush(mockServers);
    // Advance the test to the point where all pending asynchronous activities are completed
    tick();
  }));

  it('should handle error when loading servers', fakeAsync(() => {
    // Define an error message to expect
    const errorMessage = 'Failed to load servers';

    // Subscribe to the getAllTrainingsServers() method and handle errors
    service.getAllTrainingsServers().subscribe(
      () => fail('Expected error, but got success'),
      error => {
        // Ensure that the error message matches the expected error message
        expect(error).toBeTruthy(errorMessage);
      }
    );

    // Expect an HTTP GET request to the correct API endpoint
    const req = http.expectOne(`${environment.apiHost}:${environment.apiPort}/training-servers/`);
    // Ensure that the request method is GET
    expect(req.request.method).toBe('GET');
    // Advance the test to the point where all pending asynchronous activities are completed
    tick();
  }));

  it('should return observable with training server', inject([TrainingServerService], () => {
    // Subscribe to the getAllTrainingsServers() method
    service.getAllTrainingsServers().subscribe(res => {
      // Ensure that the returned servers have the expected length and content
      expect(res.length).toEqual(DUMMY_SERVER.length);
      expect(res).toEqual(DUMMY_SERVER);
    });

    // Expect an HTTP GET request to the correct API endpoint
    const req = http.expectOne(`${environment.apiHost}:${environment.apiPort}/training-servers/`);
    // Ensure that the request method is GET
    expect(req.request.method).toEqual('GET');
    // Flush the response with the mocked servers
    req.flush(DUMMY_SERVER);
  }));

  it('should delete training server', inject([TrainingServerService,
    HttpTestingController, HttpClient], (backend: HttpTestingController) => {
    const server = DUMMY_SERVER[0];
    const backendUrl = `${environment.apiHost}:${environment.apiPort}/training-servers/${server.id}`;
    service.deleteTrainingServer(server).subscribe({
      next(value) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        expect(value).toEqual(server);
      },
    });
    const req = http.expectOne(backendUrl);
    //expect(req.request.method).toBe("DELETE");
  }));

  it('should add training server', inject([TrainingServerService,
    HttpTestingController, HttpClient], (backend: HttpTestingController) => {
    const server = DUMMY_SERVER[0];
    const backendUrl = `${environment.apiHost}:${environment.apiPort}/training-servers/`;
    service.addTrainingServer(server).subscribe({
      next(value) {
        expect(value).toEqual(server);
      },
    });
    const req = http.expectOne(backendUrl);
    expect(req.request.method).toBe('POST');
  }));


});
