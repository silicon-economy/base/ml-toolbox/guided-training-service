/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {ARCHITECTURES, ModelManagementService} from 'src/app/core/backend/model-management.service';
import {environment} from 'src/environments/environment';
import {Model} from "src/app/core/models/model";

describe('ModelManagementService', () => {
  let service: ModelManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ModelManagementService]
    });
    service = TestBed.inject(ModelManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return models with default parameters', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    const mockResponse = {
      total_items: 5,
      models: [
        {
          run_id: '1',
          name: 'test',
          description: 'test',
          dataset: 'test',
          model: 'test',
          evaluation_datasets: 'test',
          epochs: 1,
          batch_size: 1,
          annotation_task: [],
          training_state: 'RUNNING', // a state value with e.g. running, waiting, done, error
          accuracy: 99,
          algorithm_type: 'test',
          creation_date: Date,
          mlflow_url: 'test'
        }
      ]
    };

    service.getModels().subscribe(modelsResponse => {
      expect(modelsResponse.totalModels).toBe(5);
      expect(modelsResponse.models.length).toBeGreaterThan(0); // Check if models array is not empty
    });

    const req = httpMock.expectOne(request => request.url.includes('/models/'));
    expect(req.request.method).toBe('GET');
    expect(req.request.params.get('filter')).toBe('');
    expect(req.request.params.get('order_by')).toBe('creation_date');
    expect(req.request.params.get('direction')).toBe('desc');
    expect(req.request.params.get('page')).toBe('1');
    expect(req.request.params.get('per_page')).toBe('10');

    req.flush(mockResponse);
  }));

  it('should return observable with models', inject([ModelManagementService], () => {
    service.getModels('', '', '', 0, 10).subscribe(res => {
      expect(res.totalModels).toEqual(100);
    });
  }));

  it('should return observable with model', inject([ModelManagementService], () => {
    service.getModel('test').subscribe(res => {
      expect(res.totalModels).toEqual(1);
    });
  }));

  it('send parameters from input dialog to backend',
    inject([HttpTestingController], (httpMock: HttpTestingController) => {
      const epochs = 10;
      const labelStudioProjectIds = [42];
      const model = 'yolox_nano';
      const modelName = 'test123';
      const description = 'This is a test model';
      const classes = ['person', 'car'];
      const batchSize = 1;
      const endpoint = 'server123:8080';
      const modelConfig = 'testfile'
      const backendUrl = `${environment.apiHost}:${environment.apiPort}/training/`;
      service.startTraining(
        epochs,
        labelStudioProjectIds,
        labelStudioProjectIds,
        model,
        modelName,
        description,
        classes,
        batchSize,
        modelConfig,
        endpoint,
        backendUrl,
      ).subscribe({
        next(trainingJob) {
          expect(trainingJob.id).toEqual('1024');
          expect(trainingJob.message).toEqual('Training job started!');
        },
      });

      const mockRequest = httpMock.expectOne(backendUrl);
      expect(mockRequest.request.method).toBe('POST');

      mockRequest.flush({
        id: '1024',
        message: 'Training job started!'
      });

      httpMock.verify();
    }));

   it('should create a template with specified parameters', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    const model = 'model-1';
    const modelName = 'AdvancedModel';
    const trainLabelStudioProjectIds = [1, 2];
    const evalLabelStudioProjectIds = [3];
    const epochs = 10;
    const batch_size = 32;
    const classes = ['class1', 'class2'];
    const description = 'Test template description';

    const mockResponse = { success: true };

    service.retrieveModelConfig(model, modelName, trainLabelStudioProjectIds, evalLabelStudioProjectIds, epochs, batch_size, classes, description)
      .subscribe(response => {
        expect(response).toEqual(mockResponse);
      });

    const req = httpMock.expectOne(req => req.url === `${environment.apiHost}:${environment.apiPort}/training/config` &&
      req.params.get('model') === model &&
      req.params.get('modelName') === modelName &&
      req.params.get('trainLabelStudioProjectIds') === trainLabelStudioProjectIds.join(',') &&
      req.params.get('evalLabelStudioProjectIds') === evalLabelStudioProjectIds.join(',') &&
      req.params.get('epochs') === epochs.toString() &&
      req.params.get('batchSize') === batch_size.toString() &&
      req.params.get('classes') === classes.join(',') &&
      req.params.get('description') === description);

    expect(req.request.method).toBe('GET');

    req.flush(mockResponse); // Simulates the API response
  }));

  it('should sort models by algorithm_type and then by name', () => {
    const models: Model[] = [
      {
        run_id: '0',
        name: 'B',
        description: 'Description for the model',
        dataset: ['Imagenet'],
        evaluation_datasets: ['Imagenet'],
        model: 'modelpath',
        epochs: 30,
        batch_size: 6,
        annotation_task: [{
          name: 'EPAL',
          colorClass: 'bright-green-chip'
        }],
        training_state: 'running',
        accuracy: 0.8,
        algorithm_type: 'Algo1',
        creation_date: new Date("2023-08-01T00:00:00.000Z"),
        mlflow_url: "https://example.org/"
      },
      {
        run_id: '1',
        name: 'A',
        description: 'Description for the model',
        dataset: ['Imagenet'],
        evaluation_datasets: ['Imagenet'],
        model: 'modelpath',
        epochs: 30,
        batch_size: 6,
        annotation_task: [
          {
            name: 'EPAL',
            colorClass: 'bright-green-chip'
          }],
        training_state: 'done',
        accuracy: 0.8,
        algorithm_type: 'Algo2',
        creation_date: new Date("2023-08-01T00:00:00.000Z"),
        mlflow_url: ""
      },
      {
        run_id: '2',
        name: 'C',
        description: 'Description for the model',
        dataset: ['Imagenet'],
        evaluation_datasets: ['Imagenet'],
        model: 'modelpath',
        epochs: 30,
        batch_size: 6,
        annotation_task: [{
          name: 'EPAL',
          colorClass: 'bright-green-chip'
        }],
        training_state: 'done',
        accuracy: 0.8,
        algorithm_type: 'Algo1',
        creation_date: new Date("2023-08-01T00:00:00.000Z"),
        mlflow_url: ""
      },
      {
        run_id: '3',
        name: 'A',
        description: 'Description for the model',
        dataset: ['Imagenet'],
        evaluation_datasets: ['Imagenet'],
        model: 'modelpath',
        epochs: 30,
        batch_size: 6,
        annotation_task: [{
          name: 'EPAL',
          colorClass: 'bright-green-chip'
        }],
        training_state: 'done',
        accuracy: 0.8,
        algorithm_type: 'Algo1',
        creation_date: new Date("2023-08-01T00:00:00.000Z"),
        mlflow_url: ""
      }
    ];

    const sortedModels = service.sortModelList(models);

    expect(sortedModels[0].algorithm_type).toBe('Algo1');
    expect(sortedModels[0].name).toBe('A');
    expect(sortedModels[1].algorithm_type).toBe('Algo1');
    expect(sortedModels[1].name).toBe('B');
    expect(sortedModels[2].algorithm_type).toBe('Algo1');
    expect(sortedModels[2].name).toBe('C');
    expect(sortedModels[3].algorithm_type).toBe('Algo2');
    expect(sortedModels[3].name).toBe('A');
  });

  describe('getArchitecture Function Tests', () => {
    it('should return metrics for a model', inject([HttpTestingController], (httpMock: HttpTestingController) => {
      // Prepare
      const dummy_model = {
        run_id: '2',
        name: 'Default Yolo-v5',
        description: 'Description for the model',
        dataset: ['Imagenet'],
        evaluation_datasets: ['Imagenet'],
        model: 'modelpath',
        epochs: 30,
        batch_size: 6,
        annotation_task: [],
        training_state: 'done',
        accuracy: 0.8,
        algorithm_type: 'classification',
        creation_date: new Date("2023-08-01T00:00:00.000Z"),
        mlflow_url: ""
      }
      const mockMetricsResponse = {

        "0_loading_unit": {
          "AP": "0.99",
          "COUNT": "60.0",
          "F1": "0.92",
          "FN": "0.0",
          "FP": "10.0",
          "PR": "0.85",
          "RC": "1.0",
          "TP": "60.0"
        }
        ,

        "1_pallet": {
          "AP": "0.29",
          "COUNT": "60.0",
          "F1": "0.92",
          "FN": "5.0",
          "FP": "2.0",
          "PR": "0.23",
          "RC": "2.0",
          "TP": "70.0"
        }
      }

      // Act & Verify
      service.getMetrics(dummy_model).subscribe(metrics => {
        expect(metrics).toEqual(mockMetricsResponse);
      });
      const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/models/${dummy_model.name}/metrics`);
      expect(req.request.method).toEqual('GET');

      req.flush(mockMetricsResponse);
    }));

    it('should return fn, fp images for a model', inject([HttpTestingController], (httpMock: HttpTestingController) => {
      // Prepare
      const dummy_model_name = 'dummy_model'
      const mockMetricsResponse = [
        {
          '0_loading_unit':
            [
              'http://dummypage/test.png',
            ]
        }
      ]

      // Act & Verify
      service.getFnFpImages(dummy_model_name).subscribe(metrics => {
        expect(metrics).toEqual(mockMetricsResponse);
      });
      const req = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/models/${dummy_model_name}/fp-fn-images`);
      expect(req.request.method).toEqual('GET');

      req.flush(mockMetricsResponse);
    }));

    it('should return the expected architecture', () => {
      // Define the expected data structure
      const expectedData = [
        {
          displayValue: 'Classification',
          backendValue: 'classification',
          children: [
            {
              displayValue: 'EfficientNet',
              children: [
                {
                  displayValue: 'EfficientNet - B0',
                  backendValue: 'mmpretrain__efficientnet-b0_3rdparty_8xb32_in1k_20220119-a7e2a0b1'
                },
                {
                  displayValue: 'EfficientNet - B4',
                  backendValue: 'mmpretrain__efficientnet-b4_3rdparty-ra-noisystudent_in1k_20221103-16ba8a2d'
                },
                {
                  displayValue: 'EfficientNet - B8',
                  backendValue: 'mmpretrain__efficientnet-b8_3rdparty_8xb32-aa-advprop_in1k_20220119-297ce1b7'
                }
              ]
            }
          ]
        },
        {
          displayValue: 'Object Detection',
          backendValue: 'object_detection',
          children: [
            {
              displayValue: 'YOLOX',
              children: [
                {
                  displayValue: 'YOLOX - Nano (416x416)',
                  backendValue: 'yolox__nano'
                },
                {
                  displayValue: 'YOLOX - Tiny (416x416)',
                  backendValue: 'yolox__tiny'
                },
                {
                  displayValue: 'YOLOX - S (640x640)',
                  backendValue: 'yolox__s'
                },
                {
                  displayValue: 'YOLOX - M (640x640)',
                  backendValue: 'yolox__m'
                },
                {
                  displayValue: 'YOLOX - L (640x640)',
                  backendValue: 'yolox__l'
                },
                {
                  displayValue: 'YOLOX - X (640x640)',
                  backendValue: 'yolox__x'
                },
                {
                  displayValue: 'YOLOX - Darknet (640x640)',
                  backendValue: 'yolox__darknet'
                }
              ]
            },
            {
              displayValue: 'mmdetection',
              children: [
                {
                  displayValue: 'YOLOV3 - Darknet53 (640x640)',
                  backendValue: 'mmdetection_object_detection__yolov3_d53_mstrain-608_273e_coco'
                },
                {
                  displayValue: 'HTC - X-101-64x4d-FPN',
                  backendValue: 'mmdetection_object_detection__htc_x101_64x4d_fpn_dconv_c3-c5_mstrain_400_1400_16x1_20e_coco'
                },
                {
                  displayValue: 'RTMDet - Tiny',
                  backendValue: 'mmdetection_object_detection__rtmdet_tiny_8xb32-300e_coco_20220902_112414-78e30dcc'
                },
                {
                  displayValue: 'RTMDet - S',
                  backendValue: 'mmdetection_object_detection__rtmdet_s_8xb32-300e_coco_20220905_161602-387a891e'
                },
                {
                  displayValue: 'RTMDet - M',
                  backendValue: 'mmdetection_object_detection__rtmdet_m_8xb32-300e_coco_20220719_112220-229f527c'
                },
                {
                  displayValue: 'RTMDet - L',
                  backendValue: 'mmdetection_object_detection__rtmdet_l_8xb32-300e_coco_20220719_112030-5a0be7c4'
                },
                {
                  displayValue: 'RTMDet - X',
                  backendValue: 'mmdetection_object_detection__rtmdet_x_8xb32-300e_coco_20220715_230555-cc79b9ae'
                }
              ]
            }
          ]
        },
        {
          displayValue: 'Rotated Object Detection',
          backendValue: 'rotated_object_detection',
          children: [
            {
              displayValue: 'Rotated RTMDet',
              children: [
                {
                  displayValue: 'Rotated RTMDet - Tiny',
                  backendValue: 'mmrotate__rotated_rtmdet_tiny-3x-dota_ms-f12286ff'
                },
                {
                  displayValue: 'Rotated RTMDet - L',
                  backendValue: 'mmrotate__rotated_rtmdet_l-coco_pretrain-3x-dota_ms-06d248a2'
                }
              ]
            }
          ]
        },
        {
          displayValue: 'Instance Segmentation',
          backendValue: 'instance_segmentation',
          children: [
            {
              displayValue: 'mmdetection',
              children: [
                {
                  displayValue: 'YOLACT - Resnet50-FPN',
                  backendValue: 'mmdetection_segmentation__yolact_r50_8x8_coco_20200908-ca34f5db'
                },
                {
                  displayValue: 'YOLACT - Resnet101-FPN',
                  backendValue: 'mmdetection_segmentation__yolact_r101_1x8_coco_20200908-4cbe9101'
                },
              ]
            }
          ]
        },
        {
          displayValue: 'Text Recognition',
          backendValue: 'text_recognition',
          children: [
            {
              displayValue: 'ABINet',
              children: [
                {
                  displayValue: 'ABINet',
                  backendValue: 'mmocr_text_recognition__abinet_20e_st-an_mj_20221005_012617-ead8c139'
                }
              ]
            }
          ]
        }
      ];
      // Test if the actual data matches the expected structure
      expect(ARCHITECTURES).toEqual(expectedData);
    });
  });
});
