/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {ModelTestingConfig} from "src/app/core/models/modeltestingconfig";

/**
 * Service for handling model testing operations.
 * This service is responsible for starting model testing by sending configurations
 * to the backend and receiving the response with the test results.
 */
@Injectable({
  providedIn: 'root'
})
export class ModelTestingService {

  /**
   * API base URL, constructed using the environment configuration.
   */
  private readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  /**
   * Configuration for the model testing process, set when starting the testing.
   */
  config: ModelTestingConfig

  /**
   * Creates an instance of the ModelTestingService.
   *
   * @param http The HttpClient used to communicate with the backend.
   */
  constructor(private http: HttpClient) { }

  /**
   * Starts the model testing process by submitting a configuration to the backend.
   * This method sends the necessary configuration details for the model testing, including
   * the primary model name, the second model name, and the associated LabelStudio project ID.
   *
   * @param ls_project_id The ID of the Label Studio project associated with the model testing.
   * @param model_name The name of the first model to be tested.
   * @param second_model_name The name of the second model to be tested.
   * @param server_endpoint The server endpoint where the testing request is sent.
   *
   * @returns An observable that emits the response from the backend, which contains the testing status or results.
   */
  startModelTesting(ls_project_id: number, model_name: string, second_model_name: string, server_endpoint: string): Observable<any> {
    this.config = <ModelTestingConfig>{
      "model_name": model_name,
      "second_model_name": second_model_name,
      "labelStudioProjectId": ls_project_id,
      "endpoint": server_endpoint
    }
    const url = `${this.API_URL}/model_testing/`;
    return this.http.post<ModelTestingConfig>(url, this.config);
  }
}
