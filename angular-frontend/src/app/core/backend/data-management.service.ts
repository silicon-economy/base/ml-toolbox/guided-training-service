/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map, Observable, of} from 'rxjs';
import {environment} from 'src/environments/environment';
import {Dataset, DatasetTag, DatasetType} from 'src/app/core/models/dataset';

/**
 * Represents the available dataset types with their value and view label.
 */
export const DATASET_TYPES: DatasetType[] = [
  {value: 'classification', viewValue: 'Classification'},
  {value: 'object_detection', viewValue: 'Object Detection'},
  {value: 'rotated_object_detection', viewValue: 'Rotated Object Detection'},
  {value: 'instance_segmentation', viewValue: 'Instance Segmentation'},
  {value: 'object_tracking', viewValue: 'Object Tracking'},
  {value: 'text_recognition', viewValue: 'Text Recognition'},
];

/**
 * The default dataset type to be used when the type is unknown.
 */
const UNKNOWN_DATASET_TYPE: DatasetType = {value: 'unknown', viewValue: 'UNKNOWN'}

/**
 * Represents task types and their associated annotation formats.
 */
const TASK_TYPE_ANNOTATION_FORMATS: TaskTypeAnnotationFormats[] = [
  {
    taskType: 'classification',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
    ]
  },
  {
    taskType: 'object_detection',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
      {value: 'coco_json', viewValue: 'COCO JSON'},
    ]
  },
  {
    taskType: 'rotated_object_detection',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
    ]
  },
  {
    taskType: 'instance_segmentation',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
      {value: 'coco_json', viewValue: 'COCO JSON'},
    ]
  },
  {
    taskType: 'text_recognition',
    annotationFormats: [
      {value: 'ls_json', viewValue: 'LabelStudio JSON'},
    ]
  },
];

/**
 * Represents the response structure for a dataset request.
 */
export class DatasetResponse {
  datasets: Dataset[];
  totalDatasets: number;
}

/**
 * Interface for task types and their associated annotation formats.
 */
export interface TaskTypeAnnotationFormats {
  taskType: string;
  annotationFormats: AnnotationFormat[];
}

/**
 * Interface for annotation format details.
 */
export interface AnnotationFormat {
  value: string;
  viewValue: string;
}

/**
 * Service for managing datasets, including retrieval, sorting, and CRUD operations.
 */
@Injectable({
  providedIn: 'root'
})
export class DataManagementService {

  /**
   * API base URL, constructed using the environment configuration.
   */
  private readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  constructor(private http: HttpClient) {
  }

  /**
   * Parses a raw dataset object, converting its properties into a strongly-typed `Dataset` object.
   *
   * @param dataset The raw dataset object to parse.
   *
   * @returns A parsed `Dataset` instance.
   */
  private parseDataset(dataset: any): Dataset {
    let parsedTags = [];
    try {
      // We need to call json parse twice because the input string is "over-stringified"
      parsedTags = JSON.parse(JSON.parse(dataset['tags']));
    } catch (exp) {
      console.log('No tags given.');
    }
    let parsedLabels = [];
    try {
      parsedLabels = JSON.parse(dataset['labels']);
    } catch (exp) {
      console.log('No labels given.');
    }

    return <Dataset>{
      labelStudioProjectId: dataset['ls_project_id'],
      name: dataset['name'],
      datasetType: DATASET_TYPES.filter(t => t.value == dataset['type'])[0] || UNKNOWN_DATASET_TYPE,
      tags: parsedTags,
      classes: parsedLabels,
      lastModified: new Date(dataset['last_modified'] + 'Z'),
      annotatedImages: dataset['annotated_images'],
      totalImages: dataset['total_images'],
      previewImages: dataset['preview_images']?.map(previewImage => previewImage['url']) || [],
      labelStudioUrl: dataset['ls_url']
    };
  }

  /**
   * Retrieves a dataset by its LabelStudio project ID.
   *
   * @param labelStudioProjectId The LabelStudio project ID.
   *
   * @returns An observable of the dataset.
   */
  getDataset(labelStudioProjectId: number): Observable<Dataset> {
    return this.http.get(`${this.API_URL}/datasets/${labelStudioProjectId}`).pipe(
      map((data: any) => this.parseDataset(data))
    );
  }

  /**
   * Retrieves a paginated list of datasets based on filter and sorting parameters.
   *
   * @param filter The filter term for the datasets.
   * @param sortName The field to sort by.
   * @param sortOrder The sorting order (ascending or descending).
   * @param pageNumber The page number.
   * @param pageSize The number of items per page.
   *
   * @returns An observable of the dataset response.
   */
  getDatasets(
    filter: string,
    sortName: string, sortOrder: string,
    pageNumber: number, pageSize: number): Observable<DatasetResponse> {

    const sortNameMapping = {
      'name': 'name',
      'type': 'type',
      'annotatedImages': 'annotated_images',
      'totalImages': 'total_images',
      'lastModified': 'last_modified'
    };

    return this.http.get(`${this.API_URL}/datasets/`, {
      params: new HttpParams()
        .set('filter', filter)
        .set('order_by', sortNameMapping[sortName])
        .set('direction', sortOrder)
        .set('page', pageNumber + 1) // page is specified as 0 indexed value in the backend
        .set('per_page', pageSize)
    }).pipe(
      map(res => <DatasetResponse>{
        totalDatasets: res['total_items'],
        datasets: res['datasets'].map((dataset) => this.parseDataset(dataset))
      })
    );
  }

  /**
   * Sorts the list of datasets first by type and then by `name`.
   *
   * @param datasets The list of datasets to sort.
   *
   * @returns The sorted dataset list.
   */
  sortDatasetList(datasets: Dataset[]) {
    datasets.sort((a: Dataset, b: Dataset) => {
      if (a.datasetType.value > b.datasetType.value) {
        return 1;
      } else if (a.datasetType.value < b.datasetType.value) {
        return -1;
      }

      if (a.name > b.name) {
        return 1;
      } else if (a.name < b.name) {
        return -1;
      }

      return 0;
    });
    return datasets
  }

  /**
   * Creates a new dataset with the specified properties.
   *
   * @param name The name of the new dataset.
   * @param type The type of the new dataset.
   * @param labels The labels associated with the dataset.
   * @param tags The tags associated with the dataset.
   *
   * @returns An observable of the created dataset.
   */
  createDataset(name: string, type: string, labels: string[], tags: DatasetTag[]) {
    const body = {
      'name': name,
      'type': type,
      'labels': JSON.stringify(labels),
      'tags': JSON.stringify(tags)
    };
    return this.http.post(`${this.API_URL}/datasets/`, body).pipe(
      map(this.parseDataset)
    );
  }

  /**
   * Edits an existing dataset by its ID.
   *
   * @param id The ID of the dataset to edit.
   * @param name The new name for the dataset.
   * @param tags The new tags for the dataset.
   *
   * @returns An observable of the updated dataset.
   */
  editDataset(id: number, name: string, tags: DatasetTag[]) {
    const body = {
      'name': name,
      'tags': JSON.stringify(tags)
    };
    return this.http.put(`${this.API_URL}/datasets/${id}`, body);
  }

  /**
   * Deletes the specified dataset.
   *
   * @param dataset The dataset to delete.
   *
   * @returns An observable for the delete operation.
   */
  /* istanbul ignore next */
  deleteDataset(dataset: Dataset): Observable<object> {
    return this.http.delete(`${this.API_URL}/datasets/${dataset.labelStudioProjectId}`);
  }

  /**
   * Exports the annotations of the specified dataset.
   *
   * @param dataset The dataset to export annotations for.
   *
   * @returns An observable of the export URL.
   */
  exportDatasetAnnotation(dataset: Dataset): Observable<string> {
    return this.http.get(`${this.API_URL}/datasets/${dataset.labelStudioProjectId}/annotation-export`).pipe(
      map(res => res['url'])
    );
  }

  /**
   * Imports annotations for the specified dataset.
   *
   * @param dataset The dataset to import annotations for.
   *
   * @returns An observable for the import operation.
   */
  importAnnotation(dataset: Dataset): Observable<any> {
    return this.http.get(`${this.API_URL}/datasets/${dataset.labelStudioProjectId}/annotation-import`);
  }

  /**
   * Synchronizes the specified dataset.
   *
   * @param dataset The dataset to synchronize.
   *
   * @returns An observable with the number of imported items.
   */
  syncDataset(dataset: Dataset): Observable<number> {
    return this.http.get(`${this.API_URL}/datasets/${dataset.labelStudioProjectId}/synchronization`).pipe(
      map(res => res['imported_items'])
    );
  }

  /**
   * Retrieves all available dataset types.
   *
   * @returns An observable that emits the list of dataset types.
   */
  getAllDatasetTypes(): Observable<DatasetType[]> {
    return of(DATASET_TYPES);
  }

  /**
   * Retrieves all available task type annotation formats.
   *
   * @returns An observable that emits the list of task type annotation formats.
   */
  getAllTaskTypeAnnotationFormats(): Observable<TaskTypeAnnotationFormats[]> {
    return of(TASK_TYPE_ANNOTATION_FORMATS);
  }

  /**
   * Automatically annotates a dataset using a specified model and server endpoint.
   *
   * @param ls_project_id The ID of the LabelStudio project for the dataset.
   * @param model_name The name of the model to use for auto-annotation.
   * @param server_endpoint The endpoint of the server for auto-annotation.
   *
   * @returns An observable of the HTTP response.
   */
  autoAnnotate(ls_project_id: number, model_name: string, server_endpoint: string): Observable<any> {
    const body = {
      'model': model_name,
      'endpoint': server_endpoint,
    };
    return this.http.post(`${this.API_URL}/datasets/${ls_project_id}/auto-annotation`, body);
  }
}
