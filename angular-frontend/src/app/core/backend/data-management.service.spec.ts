/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from 'src/environments/environment';
import {Dataset} from 'src/app/core/models/dataset';
import {DataManagementService} from 'src/app/core/backend/data-management.service';

describe('DataManagementService', () => {
  let service: DataManagementService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DataManagementService]
    });
    service = TestBed.inject(DataManagementService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', inject([DataManagementService], () => {
    expect(service).toBeTruthy();
  }));

  it('should return observable with datasets', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    service.getDatasets('someFilter', 'name', 'asc', 0, 15).subscribe({
      next: (res) => {
        expect(res.totalDatasets).toEqual(100);
        expect(res.datasets.length).toEqual(3);

        expect(res.datasets[0].labelStudioProjectId).toEqual(12);
        expect(res.datasets[0].datasetType).toEqual({value: 'classification', viewValue: 'Classification'});
        expect(res.datasets[0].previewImages.length).toBe(2);
        expect(res.datasets[0].previewImages).toEqual(['https://s3.example.com:443/image1', 'https://s3.example.com:443/image2']);

        expect(res.datasets[1].labelStudioProjectId).toEqual(13);
        expect(res.datasets[1].datasetType).toEqual({value: 'object_detection', viewValue: 'Object Detection'});
        expect(res.datasets[1].previewImages.length).toBe(1);
        expect(res.datasets[1].previewImages).toEqual(['https://s3.example.com:443/image3']);

        expect(res.datasets[2].labelStudioProjectId).toEqual(14);
        expect(res.datasets[2].datasetType).toEqual({value: 'unknown', viewValue: 'UNKNOWN'});
        expect(res.datasets[2].previewImages.length).toBe(1);
        expect(res.datasets[2].previewImages).toEqual(['https://s3.example.com:443/image4']);
      }
    });

    const mockRequest = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}`
      + '/datasets/?filter=someFilter&order_by=name&direction=asc&page=1&per_page=15');
    expect(mockRequest.request.method).toEqual('GET');

    mockRequest.flush({
      total_items: 100,
      datasets: [
        {
          ls_project_id: 12,
          type: 'classification',
          preview_images: [
            {url: 'https://s3.example.com:443/image1'},
            {url: 'https://s3.example.com:443/image2'}
          ]
        },
        {
          ls_project_id: 13,
          type: 'object_detection',
          preview_images: [
            {url: 'https://s3.example.com:443/image3'}
          ]
        },
        {
          ls_project_id: 14,
          type: 'not_a_real_type',
          preview_images: [
            {url: 'https://s3.example.com:443/image4'}
          ]
        }
      ]
    });

    httpMock.verify();
  }));

  it('should sort datasets by datasetType.value and then by name', () => {
    const datasets: Dataset[] = [
      {
        labelStudioProjectId: 1,
        name: 'Dataset B',
        datasetType: { value: 'Type1', viewValue:'Type1' },
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      },
      {
        labelStudioProjectId: 2,
        name: 'Dataset A',
        datasetType: { value: 'Type2', viewValue:'Type2' },
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      },
      {
        labelStudioProjectId: 3,
        name: 'Dataset C',
        datasetType: { value: 'Type1', viewValue:'Type1' },
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      },
      {
        labelStudioProjectId: 4,
        name: 'Dataset A',
        datasetType: { value: 'Type1', viewValue:'Type1' },
        tags: [],
        classes: [],
        lastModified: new Date(),
        annotatedImages: 0,
        totalImages: 0,
        previewImages: [],
        labelStudioUrl: "",
      }
    ];

    const sortedDatasets = service.sortDatasetList(datasets);

    // Validate sorting by `datasetType.value`
    expect(sortedDatasets[0].datasetType.value).toBe('Type1');
    expect(sortedDatasets[0].name).toBe('Dataset A');
    expect(sortedDatasets[1].datasetType.value).toBe('Type1');
    expect(sortedDatasets[1].name).toBe('Dataset B');
    expect(sortedDatasets[2].datasetType.value).toBe('Type1');
    expect(sortedDatasets[2].name).toBe('Dataset C');
    expect(sortedDatasets[3].datasetType.value).toBe('Type2');
    expect(sortedDatasets[3].name).toBe('Dataset A');
  });

  it('createDataset should create a valid request', inject([HttpTestingController], (httpMock: HttpTestingController) => {

    service.createDataset('DatasetName', 'Classification', ['label1', 'label2'], []).subscribe({
      next(value) {
        expect(value.labelStudioProjectId).toEqual(42);
        expect(value.datasetType).toEqual({value: 'classification', viewValue: 'Classification'});
      },
    });

    const mockRequest = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}` + '/datasets/');
    expect(mockRequest.request.method).toEqual('POST');
    expect(mockRequest.request.body).toEqual({
      'name': 'DatasetName',
      'type': 'Classification',
      'labels': JSON.stringify(['label1', 'label2']),
      'tags': JSON.stringify([])
    });

    mockRequest.flush(
      {
        ls_project_id: 42,
        type: 'classification'
      }
    );

    httpMock.verify();

  }));

  it('editDataset should create a valid request', inject([HttpTestingController], (httpMock: HttpTestingController) => {

    service.editDataset(42, 'DatasetName', []).subscribe({
      next(value) {
        expect(value['name']).toEqual('DatasetName');
      },
    });

    const mockRequest = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}/datasets/42`);
    expect(mockRequest.request.method).toEqual('PUT');
    expect(mockRequest.request.body).toEqual({
      'name': 'DatasetName',
      'tags': JSON.stringify([])
    });

    mockRequest.flush(
      {
        name: 'DatasetName'
      }
    );

    httpMock.verify();

  }));

  it('should request Annotations for the correct project', inject([HttpTestingController], (httpMock: HttpTestingController) => {

    const dataset = <Dataset>{
      labelStudioProjectId: 42
    };
    service.exportDatasetAnnotation(dataset).subscribe({
      next(value) {
        expect(value).toEqual('http://some-url-to-download-annotation.example.com');
      },
    });

    const mockRequest = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}` + '/datasets/42/annotation-export');
    expect(mockRequest.request.method).toEqual('GET');

    mockRequest.flush({
      url: 'http://some-url-to-download-annotation.example.com'
    });

    httpMock.verify();
  }));

  it('should request synchronization for the correct project and return the number of synchronized items',
    inject([HttpTestingController], (httpMock: HttpTestingController) => {
      const dataset = <Dataset>{
        labelStudioProjectId: 42
      };
      service.syncDataset(dataset).subscribe({
        next(value) {
          expect(value).toEqual(21);
        },
      });

      const mockRequest = httpMock.expectOne(`${environment.apiHost}:${environment.apiPort}` + '/datasets/42/synchronization');
      expect(mockRequest.request.method).toEqual('GET');

      mockRequest.flush({
        imported_items: 21
      });

      httpMock.verify();
    }));
});
