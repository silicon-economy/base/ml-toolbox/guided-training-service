/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToasterContainerComponent} from 'src/app/core/services/toaster/toaster-container.component';
import {CookieDialogComponent} from 'src/app/core/components/dialogs/cookie-dialog/cookie-dialog.component';
import {ErrorPageComponent} from 'src/app/core/components/error-page/error-page.component';
import {
  ResponsiveImageCollectionComponent
} from 'src/app/core/components/responsive-image-collection/responsive-image-collection.component';
import {SidenavService} from 'src/app/core/services/state/sidenav.service';
import {ThemeService} from 'src/app/core/services/state/theme.service';
import {ToasterService} from 'src/app/core/services/toaster/toaster.service';
import {ToasterComponent} from 'src/app/core/services/toaster/toaster.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    ToasterComponent,
    ToasterContainerComponent,
    CookieDialogComponent,
    ErrorPageComponent,
    ResponsiveImageCollectionComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatButtonModule
  ],
  exports: [
    ToasterContainerComponent,
    CookieDialogComponent,
    ErrorPageComponent,
    ResponsiveImageCollectionComponent
  ],
  providers: [
    SidenavService,
    ThemeService,
    ToasterService
  ]
})
export class CoreModule {
}
