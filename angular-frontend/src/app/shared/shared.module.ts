/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TooltiplistPipe} from 'src/app/shared/pipes/tooltiplist/tooltiplist.pipe';

@NgModule({
  declarations: [
    TooltiplistPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    TooltiplistPipe,
  ]
})
export class SharedModule {
}
