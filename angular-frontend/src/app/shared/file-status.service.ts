/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {
  UploadDragDropFile
} from "src/app/features/pages/data-management-page/add-new-dataset-dialog/add-new-dataset-dialog.component";

@Injectable({
  providedIn: 'root',
})
export class FileStatusService {

  /**
   * Updates the status icon for all new files with the given status
   *
   * @param selectedFiles All files selected in the dialog
   * @param uploadFileList The files that needed to be uploaded
   * @param status The new status that the files should be given
   */
  updateStatusIcon(selectedFiles: File[], uploadFileList: UploadDragDropFile[], status: string) {
    for (const file of selectedFiles) {
      const fileIndex = uploadFileList.findIndex(f => f.name === file.name);
      if (fileIndex !== -1) {
        uploadFileList[fileIndex].statusIcon = status;
      }
    }
  }

  /**
   * Gets the correct Tooltip for a given icon
   *
   * @param statusIcon The current status icon
   */
  getStatusTooltip(statusIcon: string): string {
    switch (statusIcon) {
      case 'cached':
        return 'File is waiting to be uploaded';
      case 'checked':
        return 'File has been uploaded';
      case 'dangerous':
        return 'File could not be uploaded due to an error';
      case 'sync_alt':
        return 'There is currently a synchronization between S3 and Labelstudio';
      default:
        return '';
    }
  }
}
