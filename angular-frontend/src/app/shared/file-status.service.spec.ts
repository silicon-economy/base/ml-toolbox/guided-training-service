/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';
import {FileStatusService} from './file-status.service';
import {
  UploadDragDropFile
} from 'src/app/features/pages/data-management-page/add-new-dataset-dialog/add-new-dataset-dialog.component';

describe('FileStatusService', () => {
  let service: FileStatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileStatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update statusIcon of the selected files', () => {
    const selectedFiles: File[] = [
      new File([''], 'file1'),
      new File([''], 'file2')
    ];
    const uploadFileList: UploadDragDropFile[] = [
      {name: 'file1', statusIcon: '', icon: 'remove', size: 0, type: ''},
      {name: 'file2', statusIcon: '', icon: 'remove', size: 0, type: ''},
      {name: 'file3', statusIcon: '', icon: 'remove', size: 0, type: ''}
    ];

    service.updateStatusIcon(selectedFiles, uploadFileList, 'checked');

    expect(uploadFileList[0].statusIcon).toBe('checked');
    expect(uploadFileList[1].statusIcon).toBe('checked');
    expect(uploadFileList[2].statusIcon).toBe('');
  });

  it('should return correct tooltip for statusIcon', () => {
    expect(service.getStatusTooltip('cached')).toBe('File is waiting to be uploaded');
    expect(service.getStatusTooltip('checked')).toBe('File has been uploaded');
    expect(service.getStatusTooltip('dangerous')).toBe('File could not be uploaded due to an error');
    expect(service.getStatusTooltip('sync_alt')).toBe('There is currently a synchronization between S3 and Labelstudio');
    expect(service.getStatusTooltip('unknown')).toBe('');
  });
});
