/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'tooltiplist'
})
export class TooltiplistPipe implements PipeTransform {

  /**
   * Transforms a list of strings into a multi line text of bullet points
   *
   * @example
   *
   * ```ts
   * transform(['hello', 'world'])
   * ```
   * would return `• hello\n • world`
   *
   * @param lines An array of strings that should be transformed into a tooltip string
   * @returns the tooltip as a single string
   */

  transform(lines: string[]): string {
    return lines.map(l => '• ' + l).join('\n');
  }

}
