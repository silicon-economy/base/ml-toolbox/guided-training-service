/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TooltiplistPipe} from 'src/app/shared/pipes/tooltiplist/tooltiplist.pipe';

describe('TooltiplistPipe', () => {
  it('create an instance', () => {
    const pipe = new TooltiplistPipe();
    expect(pipe).toBeTruthy();
  });
});
