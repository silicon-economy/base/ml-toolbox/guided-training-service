/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'a, button'
})
export class FocusBlurDirective {
  /**
   * Creates a new object that loses focus when clicked
   *
   * @param element HTML element that should lose focus when clicked
   */
  constructor(private element: ElementRef) {
  }

  /**
   * Let element lose focus when clicked
   */
  @HostListener('click')
  onClick() {
    this.element.nativeElement.blur();
  }
}
