/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from 'src/app/app.component';
import {AppRoutingModule} from 'src/app/app-routing.module';
import {FocusBlurDirective} from 'src/app/shared/directives/focus-blur.directive';
import {ImprintLegalPageComponent} from 'src/app/features/pages/imprint-legal-page/imprint-legal-page.component';
import {PrivacyPageComponent} from 'src/app/features/pages/privacy-page/privacy-page.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatChipsModule} from '@angular/material/chips';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ScrollRestorationService} from 'src/app/core/services/state/scroll-restoration.service';
import {
  DataManagementPageComponent
} from 'src/app/features/pages/data-management-page/data-management-page.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TimePastPipe} from 'ng-time-past-pipe';
import {
  AddNewDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/add-new-dataset-dialog/add-new-dataset-dialog.component';
import {
  DeleteDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/delete-dataset-dialog/delete-dataset-dialog.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatMenuModule} from '@angular/material/menu';
import {
  ModelManagementPageComponent
} from 'src/app/features/pages/model-management-page/model-management-page.component';
import {
  TrainNewModelDialogComponent
} from 'src/app/features/pages/model-management-page/train-new-model-dialog/train-new-model-dialog.component';
import {
  DeleteModelDialogComponent
} from 'src/app/features/pages/model-management-page/delete-model-dialog/delete-model-dialog.component';
import {
  DownloadModelDialogComponent
} from "src/app/features/pages/model-management-page/download-model-dialog/download-model-dialog.component";
import {MatTooltipModule} from '@angular/material/tooltip';
import {ModelTestingPageComponent} from 'src/app/features/pages/model-testing-page/model-testing-page.component';
import {DeploymentPageComponent} from 'src/app/features/pages/deployment-page/deployment-page.component';
import {
  DownloadDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/download-dataset-dialog/download-dataset-dialog.component';
import {
  EditDatasetDialogComponent
} from 'src/app/features/pages/data-management-page/edit-dataset-dialog/edit-dataset-dialog.component';
import {CoreModule} from 'src/app/core/core.module';
import {
  UploadWarningDialogComponent
} from 'src/app/features/pages/data-management-page/edit-dataset-dialog/upload-warning-dialog/upload-warning-dialog.component';
import {
  AutoAnnotateDialogComponent
} from 'src/app/features/pages/data-management-page/auto-annotate-dialog/auto-annotate-dialog.component';
import {
  ShowPreviewImagesComponent
} from 'src/app/features/pages/data-management-page/show-preview-images/show-preview-images.component';
import {
  AddServerDialogComponent
} from "src/app/features/pages/server-management/add-server-dialog/add-server-dialog.component";
import {ServerManagementComponent} from "src/app/features/pages/server-management/server-management.component";
import {
  RemoveServerDialogComponent
} from 'src/app/features/pages/server-management/remove-server-dialog/remove-server-dialog.component';
import {
  ModelDetailPageComponent
} from 'src/app/features/pages/model-management-page/model-detail-page/model-detail-page.component';
import {
  AddDeviceDialogComponent
} from "src/app/features/pages/deployment-page/add-device-dialog/add-device-dialog.component";
import {
  DeleteDeviceDialogComponent
} from "src/app/features/pages/deployment-page/delete-device-dialog/delete-device-dialog.component";
import {
  DetailDeploymentComponent
} from 'src/app/features/pages/deployment-page/detail-deployment/detail-deployment.component';
import {MatRadioModule} from '@angular/material/radio';
import {
  StopDeploymentDialogComponent
} from 'src/app/features/pages/deployment-page/stop-deployment-dialog/stop-deployment-dialog.component';
import {
  DeployDeviceDialogComponent
} from "src/app/features/pages/deployment-page/start-deployment-dialog/start-deployment-dialog-component";
import {
  MisclassificationViewerPageComponent
} from 'src/app/features/pages/model-management-page/misclassification-viewer-page/misclassification-viewer-page.component';
import {
  ImagePopupComponent
} from 'src/app/features/pages/model-management-page/misclassification-viewer-page/image-popup/image-popup.component';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/lang/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    FocusBlurDirective,
    ImprintLegalPageComponent,
    PrivacyPageComponent,
    DataManagementPageComponent,
    AddNewDatasetDialogComponent,
    DeleteDatasetDialogComponent,
    ModelManagementPageComponent,
    TrainNewModelDialogComponent,
    DeleteModelDialogComponent,
    ModelTestingPageComponent,
    DeploymentPageComponent,
    DownloadDatasetDialogComponent,
    DownloadModelDialogComponent,
    EditDatasetDialogComponent,
    UploadWarningDialogComponent,
    AutoAnnotateDialogComponent,
    ShowPreviewImagesComponent,
    AddServerDialogComponent,
    ServerManagementComponent,
    RemoveServerDialogComponent,
    ModelDetailPageComponent,
    AddDeviceDialogComponent,
    DeployDeviceDialogComponent,
    DeleteDeviceDialogComponent,
    DetailDeploymentComponent,
    StopDeploymentDialogComponent,
    MisclassificationViewerPageComponent,
    ImagePopupComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,

    // Custom Modules:
    CoreModule,
    SharedModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    CommonModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    MatChipsModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatCheckboxModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    MatAutocompleteModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatMenuModule,
    TimePastPipe,
    HttpClientModule,
    MatRadioModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgOptimizedImage,
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ScrollRestorationService,
    {provide: 'scrollContainerSelector', useValue: '#sidenav-content-area'}
  ]
})
export class AppModule {
}
