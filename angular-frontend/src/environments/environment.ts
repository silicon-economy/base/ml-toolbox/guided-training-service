/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Contains variables.
 * Environment variables are loaded from asset folder to be editable after build.
 * If no environment variables are available default values will be loaded.
 * Attention: New environment variables also need to be added to assets/env.js and assets/env.template.js
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */
export const environment = {
  // Check if fields are defined before reading them, they are not in e.g. headless chrome
  production: window['env'] && window['env']['production'] ? window['env']['production'] : false,
  // API_HOST has to include the protocol since we can not rely on servers redirecting us to https when hardcoding http
  apiHost: window['env'] && window['env']['API_HOST'] ? window['env']['API_HOST'] : 'http://localhost',
  apiPort: window['env'] && window['env']['API_PORT'] ? window['env']['API_PORT'] : '5000',
  uploadBatchSize: window['env'] && window['env']['UPLOAD_BATCH_SIZE'] ? window['env']['UPLOAD_BATCH_SIZE'] : '25',
};
