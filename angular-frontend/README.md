
<!--
Workaround for the license-checker tool and this project's license:

The license-checker tool performs different steps to determine the license of npm packages.
When generating a third-party license report for this project, the tool tries to determine this project's license, too.
Since the Open Logistics Foundation License is currently not contained in the SPDX License List, the license-checker tool fails and ultimately falls back to scanning the README.md file for some license information.
Strangely, the tool then chooses the first URL it can find as the license for this project.
Therefore, this comment is used to have a reliable place for the first link in this document.
In the generated license report a URL as a license makes no sense, of course, but for this project this information is not really relevant anyway.

https://openlogisticsfoundation.org

This is a known issue with the license-checker tool: https://github.com/davglass/license-checker/issues/125
 -->


# Guided-Training-Service Frontend

The front-end of the Guided-Training-Service allows users to train the models for use on the smart cameras in an intuitive visual way. In addition to training, the frontend also includes the management of data sets and models, as well as exporting and testing the models on a smart camera. 

# Development
## Versions

* NodeJS 	20.12.2 	as used by the trion/ng-cli-karma:17.3.4
* NPM 	8.7.0 	as used by given NodeJS version
* Angular CLI 	17.3.4

## Docker and NGINX
The frontend will be deployed with NGINX. NGINX configurations can found in `/nginx/default.conf`.

## Environment Variables
Followed mostly this [guide](https://pumpingco.de/blog/environment-variables-angular-docker/
) to load environment-variables <br>
To add an environment variable you have to add it to:
* /src/assets/env.js
* /src/assets/env.template.js
* /src/environments/environment.ts

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

# Licenses of third-party dependencies
The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports
This project uses the [license-checker](https://www.npmjs.com/package/license-checker) to generate a file containing the licenses used by the third-party dependencies. 
We only care about production level dependencies and ignore any dev dependencies. 
Therefore, the dependencies should be installed using: 

`npm install --omit=dev`

The `third-party-licenses/third-party-licenses.csv` file can be generated using the following commands:

`npx license-checker --unknown --csv --out ./third-party-licenses/third-party-licenses.csv`

Third-party dependencies for which the licenses cannot be determined automatically, have an "UNKNOWN" license in the `third-party-licenses/third-party-licenses.csv` file and need to be checked manually.

The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:

`npx license-checker --unknown --summary > ./third-party-licenses/third-party-licenses-summary.txt`
